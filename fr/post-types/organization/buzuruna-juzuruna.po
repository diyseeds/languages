# Copyright (C) 2021 Maxime Lecoq
# This file is distributed under the GNU General Public License v2 or later.
msgid ""
msgstr ""
"Project-Id-Version: Diyseeds 2.0\n"
"POT-Creation-Date: 2021-07-10T00:00:00+02:00\n"
"PO-Revision-Date: 2021-09-17 14:13+0000\n"
"Last-Translator: Martina Widmer <martinawidmer@yahoo.fr>\n"
"Language-Team: French <http://translate.diyseeds.org/projects/"
"diyseeds-org-organizations/buzuruna-juzuruna/fr/>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"
"X-Generator: Weblate 4.8\n"
"WPOT-Type: post-types/organization\n"
"WPOT-Origin: buzuruna-juzuruna\n"
"X-Domain: wpot\n"

#. This title will be displayed into the browser's window title bar.
msgctxt "browser-title"
msgid "Buzuruna Juzuruna"
msgstr "Buzuruna Juzuruna"

#. The slug is the last part of the URL (www.example.org/slug-is-here). Only alphanumerical characters are allowed, use dash to replace space. The slug should be short and nice and it must reflect the content it refers to. It is usually made from the title. If you don't provide it, a slug will be auto-generated.
msgctxt "slug"
msgid "buzuruna-juzuruna"
msgstr "buzuruna-juzuruna"

msgctxt "name"
msgid "Buzuruna Juzuruna"
msgstr "Buzuruna Juzuruna"

msgctxt "description"
msgid "A collective of Syrian, Lebanese and French farmers who have set up a training centre for agroecology in Lebanon. They want to use these techniques in today’s and tomorrow’s Middle East to resist corporate control of agricultural markets in Lebanon and Syria."
msgstr "C’est un collectif d’agriculteurs et agricultrices syrien·nes, libanais·es et français·es qui a mis en place un centre de formation à l’agroécologie au Liban. Ils et elles veulent utiliser ces techniques au Moyen-Orient d’aujourd’hui et de demain pour résister au contrôle des entreprises sur les marchés agricoles au Liban et en Syrie."

#. This title will be displayed by search engines like Google. 60 characters max.
msgctxt "seo-title"
msgid "The Buzuruna Juzuruna collective"
msgstr "Le collectif Buzuruna Juzuruna"

#. This description will be displayed below the title on search engine results. 160 characters max.
msgctxt "seo-description"
msgid "A collective of Syrian, Lebanese and French farmers who have set up a training centre for agroecology in Lebanon."
msgstr ""
"Collectif d’agriculteurs et agricultrices syrien·nes, libanais·es et "
"français·es qui a mis en place au Liban un centre de formation à "
"l’agroécologie."

#. The tags are used by search engines to reference the content. Use commas to separate tags.
msgctxt "seo-tags"
msgid "collective, agriculture, seeds, courses, training, agroecology, Syria, Lebanon, France"
msgstr "collectif, agriculture, semences, formation, apprentissage, agroécologie, Syrie, Liban, France"
