# Copyright (C) 2021 Maxime Lecoq
# This file is distributed under the GNU General Public License v2 or later.
msgid ""
msgstr ""
"Project-Id-Version: Diyseeds 2.0\n"
"POT-Creation-Date: 2021-07-10T00:00:00+02:00\n"
"PO-Revision-Date: 2021-09-16 13:37+0000\n"
"Last-Translator: Martina Widmer <martinawidmer@yahoo.fr>\n"
"Language-Team: French <http://translate.diyseeds.org/projects/"
"diyseeds-org-articles/plant-selection-in-the-industrial-age/fr/>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"
"X-Generator: Weblate 4.8\n"
"WPOT-Type: post\n"
"WPOT-Origin: plant-selection-in-the-industrial-age-simplified\n"
"X-Domain: wpot\n"

#. This title will be displayed into the browser's window title bar.
msgctxt "browser-title"
msgid "Plant selection in the industrial age"
msgstr "La sélection des plantes à l’âge industriel"

#. The slug is the last part of the URL (www.example.org/slug-is-here). Only alphanumerical characters are allowed, use dash to replace space. The slug should be short and nice and it must reflect the content it refers to. It is usually made from the title. If you don't provide it, a slug will be auto-generated.
msgctxt "slug"
msgid "plant-selection-in-the-industrial-age"
msgstr "la-selection-des-plantes-a-l-age-industriel"

#. Title of a page's section.
msgctxt "section-title"
msgid "Plant selection in the industrial age"
msgstr "La sélection des plantes à l’âge industriel"

msgid "Living creatures reproduce themselves for free, while industry’s raison d’être is profit."
msgstr "Les êtres vivants se reproduisent et se multiplient gratuitement."

msgid "The Law of Life is opposed to the Law of Profit."
msgstr "La loi de la Vie s’oppose à la loi du Profit."

msgid "Life exists thanks to the singularity of each organism, industry asserts itself through the uniformity of its goods."
msgstr "La Vie existe par la singularité de chaque organisme, l’industrie s’impose par l’uniformité des marchandises."

msgid "For industrial capitalism life is a two-fold sacrilege."
msgstr "Pour le capitalisme industriel, la Vie est doublement sacrilège."

msgid "Over the last two centuries, vanquishing this double sacrilege has been the historic task that industrial capitalism has entrusted plant to plant breeders and agronomic sciences. This task is now almost completed. You only have to see the oversize, over-green industrial fields scored by the scars left by the wheels of tractors, in which no plant is higher than its neighbours, to realize that uniformity has triumphed. As to the other sacrilege, the patenting of life crowns two centuries of efforts aimed at putting an end to the founding practice of agriculture, that of resowing one’s own harvested seed. This means separating production from reproduction, and making reproduction the privilege of a few. Today this privilege belongs to the cartel of “life sciences” – the producers of pesticides, herbicides, insecticides, larvicides, ovocides, gametocides, bactericides, molluscicides, rodenticides, acaricides, fungicides : the “Cartel of Cides”!"
msgstr ""
"Depuis deux siècles, en finir avec ce double sacrilège est la tâche "
"historique que le capitalisme industriel assigne aux sélectionneurs et "
"sélectionneuses et aux sciences agronomiques. Elle est maintenant presque "
"achevée. Le brevet du vivant couronne deux siècles d’efforts pour en finir "
"avec la pratique fondatrice de l’agriculture, semer le grain récolté. Il s’"
"agit de séparer la production de la reproduction, de faire de la "
"reproduction un privilège du cartel des « sciences de la vie » – les "
"fabricant·es de pesticides, herbicides, insecticides, larvicides, ovocides, "
"gamétocides, bactéricides, fongicides, mollusquicides, rodenticides, "
"acaricides, nématicides! Quant à l’uniformité, ces champs industriels trop "
"grands, trop verts, striés par les cicatrices des roues de tracteurs, où les "
"plantes « font la table » – pas une ne dépasse ses voisines – montrent que "
"le but est atteint."

msgid "Industrial selection is simple if you break through the genetic smokescreen. It consists of replacing a variety, the characteristic of which is being varied and therefore the opposite of uniformity, with copies of a selected plant within the variety – with clones. This signaled the death-bell for the sacrilege of diversity. There remained, however, the other sacrilege to be dealt with, which meant putting an end to reproduction free of charge. Already in its early days the new science of genetics achieved this miracle. The result was “hybrid” corn, the sacred cow of agronomic sciences and “paradigm” of plant breeding of the 20th century."
msgstr "L’histoire de la sélection industrielle est simple à condition de disperser le rideau de fumée génétique. Il s’agit de remplacer une variété – le caractère d’être varié, contraire de l’uniformité – par des copies d’une plante sélectionnée au sein de la variété – par un clone. Lorsque les plantes conservent leurs caractères individuels (comme les « autogames », blé, orge, soja, etc), le sélectionneur ou la sélectionneuse ne met fin qu’au sacrilège de la diversité. Lorsque les plantes ne les conservent pas, il ou elle met fin aux deux simultanément. C’est ce miracle qu’accomplit le maïs « hybride », la vache sacrée des sciences agronomiques au 20ème siècle."

msgid "In 1836, John Le Couteur, an English gentleman farmer - a capitalist who invested his capital in agricultural production - codified the technique of “isolation” practiced empirically since the turn of the century. Since we cultivate, he argues, varieties (the character of that which is varied, diversity) of plants, since each plant retains its individual characters, I will “isolate” in my fields the most promising plants to cultivate them. individually [1] and therefore reproduce and multiply them - to make copies, clones - to finally select the best clone and replace the variety. The technique of isolation is based on an unstoppable logical principle. There is always a gain in replacing a variety of “anything” with copies of a better “anything” isolated within the variety."
msgstr "En 1836, John Le Couteur, un gentilhomme agriculteur anglais – un capitaliste qui investit son capital dans la production agricole – codifie la technique de « l’isolement » pratiquée empiriquement depuis le début du siècle. Puisque nous cultivons, raisonne-t-il, des variétés (le caractère de ce qui est varié, diversité) de plantes, puisque chaque plante conserve ses caractères individuels, je vais « isoler » dans mes champs les plantes les plus prometteuses pour les cultiver individuellement [1] et donc les reproduire et les multiplier – pour en faire des copies, des clones – pour enfin sélectionner le meilleur clone et remplacer la variété. La technique de l’isolement repose sur un principe logique imparable. Il y a toujours un gain à remplacer une variété de « n’importe quoi » par des copies d’un meilleur « n’importe quoi » isolé au sein de la variété."

msgid "It remains to put an end to the sacrilege of gratuity. An autogamous clone reproduces identically. The harvested grain is also the seed of the following year. It was not until the 1920s for this problem to find the beginnings of an administrative / legal solution in France and the 21st century for the patent to put an end to the scandal of free reproduction."
msgstr "Reste à en finir avec le sacrilège de la gratuité. Un clone autogame se reproduit à l’identique. Le grain récolté est aussi la semence de l’année suivante. Il faudra attendre les années 1920 pour que ce problème trouve une amorce de solution administrative/légale en France et le 21ème siècle pour que le brevet vienne en finir avec le scandale de la gratuité de la reproduction."

msgid "At the end of the 1920s French technocratic agronomists imposed a new system according to which varieties put on to the market must be “homogenous” and “stable”. Homogenous means that the plants must be phenotypically (visually) identical, while to be considered stable the same plant must be put on the market every year. This dual requirement means that the plants must be genetically identical or almost so. Their homogeneity and stability are controlled by an official body. If a new variety satisfies these criteria, it is registered in a Catalogue and the approved breeder receives a certificate which gives him the exclusive right to put the variety on to the market. In 1960 this mechanism was adopted by the International Union for the Protection of New Varieties of Plants (UPOV) which has now been ratified by about sixty countries."
msgstr "Depuis la fin des années 1920, les variétés commercialisées doivent être « homogènes » et « stables ». Homogènes : les plantes doivent être phénotypiquement (visuellement) identiques. Stables : la même plante doit être offerte à la vente année après année. Cette double exigence implique que les plantes doivent être génétiquement identiques ou presque. Cette homogénéité et cette stabilité font l’objet d’un examen par un service officiel. Si une nouvelle variété satisfait ces critères, elle est inscrite à un Catalogue et son obtenteur ou obtentrice reçoit un certificat d’obtention qui lui donne le droit de la commercialiser. En 1960, ce dispositif est repris par le traité de l’Union Pour la Protection des Obtentions Végétales. La sélection-clonage de Le Couteur et La Gasca a maintenant en quelque sorte un cours international légal dans plus d’une soixantaine de pays."

msgid "The certificate protects the seed producer from “piracy” of his clones by competitors, but there were still too many farmers for the seed industry to dare to attack the “farmer’s privilege”. The certificate corresponded to the needs of traditional plant breeding companies run by agronomists passionate about their breeding work. Over the last thirty years, however, the “Cartel of Cides” has taken control over the world’s seeds. This cartel considers the farmer who sows the seeds he harvests to be a “pirate”. European directive 98/44 that authorizes the patenting of life is putting an end to this “piracy”."
msgstr "Le certificat d’obtention protège l’obtenteur ou l'obtentrice du « piratage » de ses clones par des concurrent·es. Il laisse libre l’agriculteur et l'agricultrice de semer le grain récolté. Il répondait aux besoins des maisons traditionnelles de sélection dirigées par des agronomes passionné·es par le travail de sélection. Mais depuis une trentaine d’années, le Cartel des Cides a pris le contrôle des semences dans le monde. Pour le Cartel, l’agriculteur ou l'agricultrice qui sème le grain récolté est désormais un·e « pirate » qui commet un sacrilège contre la Propriété. Le brevet du vivant est en train de mettre fin à ce sacrilège."

msgid "In 1900, the rediscovery of Mendel's laws extended the isolation method to corn. But in practice, it turns out to be so difficult, so improbable, that it was necessary to invent a biological phenomenon, always “unexplained and inexplicable [2]” - heterosis - to justify its implementation - then, as we we have seen, no justification is necessary! But “hybrid” corn puts an end to the double sacrilege and geneticists, breeders and breeders have been able to accredit and perpetuate the existence and their futile hunting of this genetic yeti."
msgstr "En 1900, la redécouverte des lois de Mendel permet d’étendre au maïs la méthode de l’isolement. Mais en pratique, elle s’avère si difficile, si invraisemblable, qu’il a fallu inventer un phénomène biologique, toujours « inexpliqué et inexplicable [2] » – l’hétérosis – pour justifier sa mise en œuvre – alors, que comme nous l’avons vu, aucune justification n’est nécessaire ! Mais le maïs « hybride » met fin au double sacrilège et généticien·nes, sélectionneurs ou sélectionneuses ont su accréditer et perpétuer l’existence et leur chasse vaine de ce yéti génétique."

msgid "In summary, the history of industrial selection is that of mining by selection-cloning which destroys the diversity created by the friendly cooperation between peasants and nature since the beginnings of the domestication of plants and animals. Because the peasants did not wait for genetics to “improve” the plants. This is evidenced by the profusion of cultivated varieties (and animal breeds)."
msgstr "En résumé, l’histoire de la sélection industrielle est celle de l’exploitation minière par la sélection-clonage destructrice de la diversité créée par la coopération amicale entre les paysan·nes et la nature depuis les débuts de la domestication des plantes et des animaux. Car les paysan·nes n’ont pas attendu la génétique pour « améliorer » les plantes. En témoigne le foisonnement de variétés cultivées (et des races animales)."

msgid "Genetics and breeding are separate activities. In the 1980s, I was fortunate enough to see a very great wheat breeder, Claude Benoît, at work in a disjunct wheat field. At first, for me, the plants were all the same. At the end of the day, I began to roughly distinguish them and understand Claude Benoît's selection criteria. “I was starting”, because Claude Benoît could not himself explain what made him retain this one rather than that one among the plants which seemed to me strictly similar. It is that selecting is based on an “uncoded” knowledge which cannot be made explicit or that it is with difficulty. The meticulous work of the breeder guided by experience, by a long familiarity with the plant, by empathy not to say the love that he and that she [3] has for him, by a sense acute observation, due to its agronomic knowledge, does not need the geneticist [4]. Rather, the esotericism of genetics serves to intimidate those who would like to make selection, to discourage them, to make them give up. As we have seen, the Geneticist, like other scientists, can make a mistake by deceiving us as long as he is not mistaken about the interests to be served."
msgstr "La génétique et la sélection sont des activités distinctes. Dans les années 1980, j’ai eu la chance de voir un très grand sélectionneur de blé, Claude Benoît, au travail dans un champ de blé en disjonction. Au début, pour moi, les plantes étaient toutes les mêmes. A la fin de la journée, je commençais à les distinguer grossièrement et à comprendre les critères de sélection de Claude Benoît. « Commençais », car Claude Benoît ne pouvait pas lui même expliquer ce qui lui faisait retenir parmi des plantes qui me paraissaient strictement semblables celle-ci plutôt que celle-là. C’est que sélectionner repose sur un savoir « non codifié » qui ne peut être explicité ou qu’il l’est avec difficulté. Le travail méticuleux du sélectionneur ou de la sélectionneuse guidé par l’expérience, par une longue familiarité avec la plante, par l’empathie pour ne pas dire l’amour qu’il et qu'elle [3] lui porte, par un sens aigu de l’observation, par sa connaissance agronomique, n’a pas besoin du généticien·ne [4]. L’ésotérisme de la génétique sert plutôt à intimider celles et ceux qui voudraient faire de la sélection, à les décourager, à faire qu’ils et elles renoncent. Comme on l’a vu, le Généticien ou la Généticienne, comme les autres scientifiques, peut se tromper en nous trompant pourvu de ne pas se tromper sur les intérêts à servir."

msgid "There is no point in shedding crocodile tears over the collapse of cultivated biodiversity when all the dynamics of industrial capitalism tend towards it and the regulatory and legislative system and repression framing the production and sale of seeds imposes a single two centuries old breeding method."
msgstr "Il ne sert à rien de verser des larmes de crocodile sur l’effondrement de la biodiversité cultivée alors que toute la dynamique du capitalisme industriel y tend et que le système réglementaire et législatif et de répression encadrant la production et la vente de semences impose une seule méthode de sélection vieille de deux siècles."

msgid "While waiting for the struggles against the infamy of the living patent to be successful, for a legal framework imposing a selection method for the destruction of diversity, enacted to industrialize agriculture and eliminate peasants, to expel the Cartel des Cides de la Vie, organizing collectively to cultivate diversity, share seeds, disseminate the corresponding know-how, as generations of peasants have done before us, are acts of survival all over the world as much as resistance and freedom. Kokopelli has shown the way. Longo Maï describes, step by step, how to reclaim our seeds and our future."
msgstr "En attendant que les luttes contre l’infamie du brevet du vivant aboutissent, que saute un encadrement juridique imposant une méthode de sélection de destruction de la diversité, édictée pour industrialiser l’agriculture et éliminer les paysan·nes, en attendant d’expulser le Cartel des Cides de la Vie, s’organiser collectivement pour cultiver la diversité, partager les semences, diffuser le savoir-faire correspondant, comme les générations de paysan·nes l’ont fait avant nous, sont dans le monde entier des actes de survie autant que de résistance et de liberté. Kokopelli a montré le chemin. Longo Maï décrit, pas à pas, comment nous réapproprier et nos semences et notre avenir."

msgid "Jean-Pierre Berlan"
msgstr "Jean-Pierre Berlan"

msgid "former director of research, French National Institute of Agronomic Research (INRA)"
msgstr "ancien directeur de recherche à l’INRA"

msgid "[1] This individualization of the plant accompanies the rise of bourgeois individualism."
msgstr "[1] Cette individualisation de la plante accompagne la montée de l’individualisme bourgeois."

msgid "[2] Terms used several times during the World Symposium (400 researchers!) Devoted to “Heterosis in crops” organized by the International Corn and Wheat Center in Mexico City."
msgstr "[2] Termes utilisés à plusieurs reprises lors du Symposium mondial (400 chercheur·es !) consacré à « Hétérosis dans les cultures » organisé par le Centre International du Maïs et du Blé à Mexico."

msgid "[3] An old INRA breeder told me this wonderful thing, a little embarrassed: “You know, when I'm alone with my plants, I talk to them”."
msgstr "[3] Un vieux sélectionneur de l’Inra m’a dit cette chose magnifique, un peu gêné : « Tu sais, quand je suis seul avec mes plantes, je leur parle »."

msgid "[4] For this reason, the Cides cartel bought out the seed companies. Their molecular biologists, manipulators and manipulators of genes, are quite incapable of carrying out selection work."
msgstr "[4] Pour cette raison, le cartel des Cides a racheté les entreprises de semences. Leurs biologistes moléculaires manipulateurs et manipulatrices de gènes sont bien incapables de faire un travail de sélection."

#. This title will be displayed by search engines like Google. 60 characters max.
msgctxt "seo-title"
msgid "The history of plant selection in the industrial age"
msgstr "L'histoire de la sélection des plantes à l’âge industriel"

#. This description will be displayed below the title on search engine results. 160 characters max.
msgctxt "seo-description"
msgid "This article summarizes the history of seed breeding or how the industry prevails through uniformity of commodities."
msgstr "Cet article résume l'histoire de la sélection de semences ou comment l'industrie s’impose par l’uniformité des marchandises."

#. The tags are used by search engines to reference the content. Use commas to separate tags.
msgctxt "seo-tags"
msgid "selection, seeds, history, industry, uniformity, patents, varieties, capitalism"
msgstr "séléction, semences, histoire, industrie, uniformité, brevets, variétés, capitalisme"
