1
00:00:11,480 --> 00:00:15,290
The radish is a member
of the Brassicaceae family

2
00:00:15,527 --> 00:00:17,992
and the Raphanus sativus species.

3
00:00:19,301 --> 00:00:23,694
There are small radishes
(Raphanus sativus sativus)

4
00:00:24,152 --> 00:00:28,138
and large radishes
(Raphanus sativus niger)

5
00:00:32,501 --> 00:00:36,116
with summer, autumn and winter varieties. 

6
00:00:37,190 --> 00:00:40,865
The small radishes are mainly white,
pink or red.

7
00:00:41,803 --> 00:00:45,003
There are also yellow, gray, purple

8
00:00:45,156 --> 00:00:49,069
or black varieties
with more or less elongated roots.

9
00:00:50,720 --> 00:00:56,298
The large radishes are white, pink,
purple or black with round

10
00:00:56,410 --> 00:00:59,425
or elongated roots of different lengths. 

11
00:01:09,229 --> 00:01:10,363
Pollination: 

12
00:01:18,894 --> 00:01:26,327
Radish flowers are hermaphrodite, which means
that they have both male and female organs.

13
00:01:30,276 --> 00:01:32,778
Most are self-sterile:

14
00:01:34,996 --> 00:01:39,803
the pollen from the flowers of one plant
can only fertilize another plant.

15
00:01:40,887 --> 00:01:45,570
In order to ensure good pollination
it is better to grow several plants. 

16
00:01:47,621 --> 00:01:50,145
The plants are therefore allogamous.

17
00:01:50,385 --> 00:01:53,221
Insects are the vectors of pollination.

18
00:01:57,905 --> 00:02:00,116
Radish flowers are white or purple.

19
00:02:00,400 --> 00:02:03,163
They produce nectar and attract many bees. 

20
00:02:09,614 --> 00:02:13,207
Cross pollination is possible between
all varieties of radish,

21
00:02:13,469 --> 00:02:17,818
even between the two sub-species,
sativus and niger. 

22
00:02:19,643 --> 00:02:22,072
To preserve the purity of the variety,

23
00:02:22,312 --> 00:02:26,727
two different varieties should be planted
at least 1 km apart. 

24
00:02:27,403 --> 00:02:32,080
This distance can be reduced
to 500 m if there is a natural barrier

25
00:02:32,283 --> 00:02:34,829
such as a hedge between the two varieties. 

26
00:02:38,734 --> 00:02:41,250
The varieties can also be isolated

27
00:02:41,432 --> 00:02:45,112
by alternately opening
and closing mosquito nets

28
00:02:51,163 --> 00:02:56,058
or by placing small hives with insects
inside a closed mosquito net

29
00:02:57,934 --> 00:02:58,974
for this technique,

30
00:02:59,214 --> 00:03:03,338
see the module on isolation techniques
in “The ABC of seed production”. 

31
00:03:12,654 --> 00:03:14,700
Life cycle of the radish 

32
00:03:19,003 --> 00:03:21,578
Small radishes are annual plants.

33
00:03:21,709 --> 00:03:24,858
If you sow early, in March or April

34
00:03:25,280 --> 00:03:28,938
you will be able to harvest
mature seeds at the end of summer. 

35
00:04:09,760 --> 00:04:12,741
To select 15 to 20 plants for seed,

36
00:04:13,170 --> 00:04:18,196
pull out 50 to 100 radishes
so that you can examine them closely.

37
00:04:36,200 --> 00:04:40,385
The selected radishes are replanted
entirely under the ground,

38
00:04:40,785 --> 00:04:46,058
25 cm apart with a distance of 30 cm between rows.

39
00:04:51,720 --> 00:04:53,243
They are then watered well. 

40
00:04:59,200 --> 00:05:02,138
Large radishes are biennial plants.

41
00:05:03,992 --> 00:05:07,032
To produce seed, they are sown
in the summer.

42
00:05:09,294 --> 00:05:14,567
They will have to pass the winter and then flower
and form seeds the following year. 

43
00:05:17,963 --> 00:05:22,050
At the end of autumn,
the large radishes are dug up and selected. 

44
00:05:26,080 --> 00:05:28,632
Seeds are produced by healthy radish plants

45
00:05:28,720 --> 00:05:33,461
that have been observed throughout the period
of growth for all characteristics:

46
00:05:33,934 --> 00:05:40,967
size, colour, vigour, rapid growth,
resistance to disease

47
00:05:41,258 --> 00:05:43,180
and to bolting too early,

48
00:05:46,421 --> 00:05:50,407
tender flesh without any holes, mild or strong. 

49
00:05:58,400 --> 00:06:01,350
The leaves are cut without damaging the collar

50
00:06:01,578 --> 00:06:06,334
and are then stored in damp sand
or in plastic conservation bags.

51
00:06:09,418 --> 00:06:14,218
They will overwinter in a cellar
or a cool place safe from frost.

52
00:06:20,007 --> 00:06:25,607
Throughout the winter you should regularly
check the roots and eliminate those that rot. 

53
00:06:53,076 --> 00:06:58,669
In spring, the radishes are replanted
in the garden 25 cm apart

54
00:06:59,352 --> 00:07:03,847
with a distance of 30 cm between rows
and then watered well.

55
00:07:41,770 --> 00:07:47,549
Since plants for seed can reach
a height of up to 2 m, stakes may be needed. 

56
00:07:55,207 --> 00:07:58,618
The seeds are mature
when the seed pods turn beige. 

57
00:08:02,305 --> 00:08:05,760
Generally, the stems do not mature
at the same time.

58
00:08:06,756 --> 00:08:08,550
To avoid wasting any seed,

59
00:08:08,858 --> 00:08:13,025
harvesting can take place gradually
as each stem matures.

60
00:08:16,378 --> 00:08:21,512
The entire plant can also be harvested
before all seed has completely matured. 

61
00:08:26,189 --> 00:08:30,349
The ripening process is completed
by placing them in a dry,

62
00:08:30,440 --> 00:08:32,130
well-ventilated place. 

63
00:08:46,523 --> 00:08:50,060
Extracting - sorting - storing

64
00:08:52,370 --> 00:08:54,800
The radish seeds are ready to be removed

65
00:08:55,090 --> 00:08:58,450
when the seed pods can be easily opened by hand. 

66
00:09:02,740 --> 00:09:07,629
To extract the seeds,
you can crush the seed pods with a rolling pin.

67
00:09:13,338 --> 00:09:18,901
You can also put the seed pods in a bag
and beat them against a soft surface.

68
00:09:20,060 --> 00:09:23,600
Larger quantities can be threshed
by walking on them

69
00:09:23,800 --> 00:09:26,334
or even driving over them with a vehicle. 

70
00:09:29,250 --> 00:09:34,560
Seed pods that are difficult
to open probably contain immature seeds

71
00:09:34,734 --> 00:09:36,429
that will not germinate well. 

72
00:09:40,705 --> 00:09:41,992
To sort the seeds,

73
00:09:42,189 --> 00:09:47,941
the chaff is removed by first passing the seeds
through a coarse sieve that retains the chaff,

74
00:09:55,272 --> 00:09:57,774
and then by passing them through another sieve

75
00:09:57,890 --> 00:10:02,247
that retains the seeds but allows
smaller particles to fall through. 

76
00:10:21,134 --> 00:10:24,101
Finally, they should be winnowed
by blowing on them

77
00:10:24,261 --> 00:10:28,596
or with the help of the wind
so that any remaining chaff is removed. 

78
00:10:39,170 --> 00:10:42,123
Always include a label
with the name of the variety,

79
00:10:42,349 --> 00:10:48,029
the species and the year in the bag
as writing on the outside can be rubbed off. 

80
00:10:55,940 --> 00:11:00,800
Storing the seeds in the freezer
for several days eliminates any parasites. 

81
00:11:02,840 --> 00:11:06,727
Radish seeds are able to germinate
for 5 to 10 years.

82
00:11:07,636 --> 00:11:10,945
This can be prolonged by storing
the seeds in a freezer. 
