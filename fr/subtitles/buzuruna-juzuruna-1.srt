﻿1
00:00:00,000 --> 00:00:05,760
Interview with Serge Harfouche from Buzuruna Juzuruna
May 2021, Beqaa Valley, Lebanon
www.DIYseeds.org
educational films on seed production

2
00:00:05,920 --> 00:00:08,760
-Qu’est-ce que ça veut dire « Buzuruna Juzuruna » ?

3
00:00:09,520 --> 00:00:12,100
Ça veut dire « nos graines, nos racines ».

4
00:00:12,140 --> 00:00:16,150
-Et en quoi ce nom représente votre association ?

5
00:00:17,870 --> 00:00:25,170
Nous sommes une ferme-école qui produit de la semence paysanne

6
00:00:26,090 --> 00:00:30,570
et nous donnons aussi des formations.

7
00:00:30,660 --> 00:00:35,520
Ce sont les deux plus grosses parties de notre activité.

8
00:00:36,220 --> 00:00:39,350
Nous produisons donc des graines

9
00:00:39,530 --> 00:00:42,460
qui elles-mêmes deviennent des racines.

10
00:00:42,460 --> 00:00:47,020
L’idée c’est de s’approcher d’une autonomie paysanne

11
00:00:47,130 --> 00:00:49,380
et d’une souveraineté alimentaire.

12
00:00:49,380 --> 00:00:53,120
Tout ça a l’air de bien sonner ensemble.

13
00:00:54,080 --> 00:01:04,730
-Pourquoi l’autonomie et la souveraineté alimentaire sont tellement importantes au Liban en ce moment ?

14
00:01:05,600 --> 00:01:07,840
C’est une urgence.

15
00:01:07,860 --> 00:01:13,600
Nous vivons une crise économique historique, sans précédent,

16
00:01:13,600 --> 00:01:15,600
qui est extrèmement violente.

17
00:01:15,710 --> 00:01:24,150
Notre monnaie a perdu 10 fois sa valeur.

18
00:01:24,260 --> 00:01:30,020
On est passé de 1500 livres libanais pour un dollar à 15 000 à un moment

19
00:01:30,040 --> 00:01:32,880
et là on est stabilisé plus ou moins à 12 000.

20
00:01:32,950 --> 00:01:36,600
C’est très aléatoire, on ne sait pas quand ça va péter.

21
00:01:36,680 --> 00:01:37,770
-C’est depuis quand ?

22
00:01:37,860 --> 00:01:43,400
La chute a commencé vers 2018

23
00:01:43,510 --> 00:01:54,640
mais on a vu l'inflation sur le marché à partir du milieu 2019, début 2020 je pense.

24
00:01:55,950 --> 00:02:01,260
-Quand ça arrive, ça a quelle influence sur la vie des gens ?

25
00:02:02,600 --> 00:02:07,020
C’est terrifiant, parce que les salaires n’ont pas augmentés en livres libanaises.

26
00:02:07,060 --> 00:02:12,200
Le SMIC valait autour de 400 € par mois.

27
00:02:12,200 --> 00:02:17,350
Maintenant il vaut 40 €,

28
00:02:17,880 --> 00:02:21,620
mais tout ce que tu consommes est plus cher

29
00:02:21,680 --> 00:02:24,750
parce que c’est aussi indexé au dollar, ça devient terrible.

30
00:02:24,840 --> 00:02:27,910
Imagine que tu achètes une brique de lait.

31
00:02:27,930 --> 00:02:31,530
Une brique de lait qui coûtait 3000 livres libanaises

32
00:02:31,530 --> 00:02:36,480
coûte maintenant 27 ou 25 000,

33
00:02:36,600 --> 00:02:40,200
alors que toi t’es toujours payé 100 000, par exemple.

34
00:02:40,200 --> 00:02:44,160
Donc au lieu de pouvoir en acheter plusieurs sur le mois

35
00:02:44,260 --> 00:02:47,380
tu peux à peine en acheter une seule.

36
00:02:47,440 --> 00:02:59,180
Comme notre consommation dépend de l’importation à 90-95 %,

37
00:02:59,260 --> 00:03:01,580
quand ça dépend d’une monnaie étrangère

38
00:03:01,620 --> 00:03:03,260
-le dollar dans notre cas-,

39
00:03:03,310 --> 00:03:05,520
tout devient beaucoup plus cher

40
00:03:05,570 --> 00:03:08,210
alors le pain devient plus cher,

41
00:03:08,360 --> 00:03:11,450
le carburant devient plus cher

42
00:03:11,560 --> 00:03:14,660
et les salaires sont toujours les mêmes.

43
00:03:14,690 --> 00:03:17,720
Ça veut dire que le niveau de vie,

44
00:03:17,750 --> 00:03:20,820
la qualité de vie elle en prend un grand coup

45
00:03:20,910 --> 00:03:23,090
et l’accès à l’alimentation

46
00:03:23,110 --> 00:03:26,530
et la sécurité alimentaire elle prend un très gros coup.

47
00:03:26,720 --> 00:03:30,370
De là vient l’urgence de produire ses propres semences

48
00:03:30,430 --> 00:03:33,200
au lieu de les importer de les acheter d’ailleurs

49
00:03:33,220 --> 00:03:35,490
surtout si elles sont hybrides et stériles

50
00:03:35,520 --> 00:03:38,230
et qu’il faut en racheter l’année d’après.

51
00:03:40,010 --> 00:03:43,980
Urgence aussi de produire ses propres intrants agricoles

52
00:03:44,020 --> 00:03:46,480
de façon organique et propre et bio,

53
00:03:46,530 --> 00:03:49,810
ses propres engrais, faire du compost...

54
00:03:49,890 --> 00:03:51,540
le package, comme on dit.

55
00:03:51,570 --> 00:03:55,290
C’est ce qu’on essaie de faire grace aux formations

56
00:03:55,330 --> 00:03:57,330
et grace à l’activité de la ferme.

57
00:03:58,730 --> 00:04:01,460
-Quand tu étais en France tu nous as montré

58
00:04:01,470 --> 00:04:04,110
des photos de comment,

59
00:04:04,160 --> 00:04:07,630
après ce que tu appelais la révolution,

60
00:04:07,640 --> 00:04:10,560
la vie a changée pour énormément de gens,

61
00:04:10,660 --> 00:04:12,610
des gens très diverses :

62
00:04:12,630 --> 00:04:18,410
étudiants, gens qui travaillent dans l’administration,

63
00:04:18,430 --> 00:04:20,730
des gens de tous bords qui viennent

64
00:04:20,750 --> 00:04:23,360
et qui s’intéressent tout d’un coup à ça.

65
00:04:23,630 --> 00:04:24,810
Exactement.

66
00:04:24,930 --> 00:04:27,530
Il me semble que ça se sentait,

67
00:04:28,180 --> 00:04:33,380
que quelque part on était déjà dans une crise économique sans précédent.

68
00:04:33,490 --> 00:04:38,210
C’est la raison pourquoi la révolution s’est déclenchée.

69
00:04:38,270 --> 00:04:43,040
Du coup les gens avaient une conscience diffuse

70
00:04:43,040 --> 00:04:47,650
que les années à venir vont être assez compliquées

71
00:04:47,680 --> 00:04:50,490
et qu’il va falloir se débrouiller.

72
00:04:50,540 --> 00:04:54,340
On est de plus en plus conscients

73
00:04:54,400 --> 00:05:00,000
qu’une économie qui n’est pas productive n’est pas viable,

74
00:05:00,080 --> 00:05:03,130
et la nôtre n’était pas une économie productive.

75
00:05:03,460 --> 00:05:05,920
Pendant très longtemps, 30, 40 ans,

76
00:05:05,970 --> 00:05:10,250
c’était vraiment une économie basée sur la dette.

77
00:05:11,090 --> 00:05:15,320
-Du coup beaucoup de gens se sont mis à produire?

78
00:05:15,460 --> 00:05:17,980
...

79
00:05:18,040 --> 00:05:19,290
Ça enregistre.

80
00:05:20,430 --> 00:05:23,260
Excuse-moi, je parlais au collègue.

81
00:05:23,280 --> 00:05:26,070
-Vous êtes nombreux sur la ferme ?

82
00:05:26,850 --> 00:05:31,620
On est 16 adultes, 27 enfants

83
00:05:31,650 --> 00:05:34,160
et en ce moment on a plein de potes qui sont venu-e-s,

84
00:05:34,210 --> 00:05:36,000
des volontaires, des stagiaires, ...

85
00:05:36,030 --> 00:05:38,930
En total on devrait être une cinquantaine de personnes,

86
00:05:39,000 --> 00:05:40,370
un peu plus peut-être.

87
00:05:40,410 --> 00:05:43,350
-Et c’est grand comment chez vous ?

88
00:05:44,940 --> 00:05:47,770
Sur la ferme avec l’association

89
00:05:47,920 --> 00:05:54,140
nous avons deux hectares de terrain:

90
00:05:54,580 --> 00:05:57,940
18 000 m² de plantés

91
00:05:57,950 --> 00:06:00,500
et 2000 m² entre les bâtiments,

92
00:06:00,670 --> 00:06:02,980
les pépinières,

93
00:06:03,070 --> 00:06:06,050
la bassecourt pour les poules,

94
00:06:06,060 --> 00:06:07,640
les chèvres et les brebis.

95
00:06:07,710 --> 00:06:09,820
Et cette année nous avons pu

96
00:06:09,830 --> 00:06:13,490
louer en plus 70 000 m²,

97
00:06:13,610 --> 00:06:16,600
7 hectares de plus il me semble,

98
00:06:16,700 --> 00:06:19,700
pour multiplier et agrandir notre collection,

99
00:06:19,720 --> 00:06:21,350
surtout de céréales.

100
00:06:22,280 --> 00:06:24,770
-Mais vous ne vivez pas sur la ferme ?

101
00:06:25,610 --> 00:06:27,510
Pas tout à fait.

102
00:06:27,520 --> 00:06:32,860
Un de nos collègues et cofondateurs vit sur la ferme.

103
00:06:34,400 --> 00:06:36,320
Nous avons un bâtiment polyvalent

104
00:06:36,340 --> 00:06:39,780
qui fait cuisine de transformation,

105
00:06:39,820 --> 00:06:43,090
cuisine pour nous, zone de stockage, école

106
00:06:43,140 --> 00:06:44,350
et appartement,

107
00:06:44,360 --> 00:06:47,040
un bâtiment assez long.

108
00:06:47,060 --> 00:06:50,120
C’est Walid qui habite là-bas avec sa famille.

109
00:06:50,260 --> 00:06:52,440
Après nos autres collègues et potes

110
00:06:52,460 --> 00:06:54,260
habitent juste en face de la ferme,

111
00:06:54,310 --> 00:06:56,530
et nous on est un peu plus haut dans la rue,

112
00:06:56,550 --> 00:06:58,450
à 4 minutes à pied.

113
00:06:59,590 --> 00:07:02,290
-Et vous produisez aussi un peu

114
00:07:02,300 --> 00:07:04,400
ce que vous consommez vous-mêmes ?

115
00:07:04,740 --> 00:07:06,950
Oui !

116
00:07:07,160 --> 00:07:10,070
On achète très peu.

117
00:07:10,110 --> 00:07:13,770
Tout ce qu’on peut produire, on ne l’achète pas, on l’a !

118
00:07:14,900 --> 00:07:17,980
Il y a des choses comme le riz,

119
00:07:18,010 --> 00:07:20,880
c'est un peu compliqué à faire pour nous,

120
00:07:20,910 --> 00:07:22,690
ce genre de choses.

121
00:07:22,920 --> 00:07:24,120
Mais pour le reste…

122
00:07:24,130 --> 00:07:27,230
L’année dernière on a même réussi à avoir un peu de miel,

123
00:07:27,250 --> 00:07:30,520
mais il y a un prédateur qui a défoncé nos abeilles,

124
00:07:30,560 --> 00:07:32,600
alors on est retombé à 3 ruches,

125
00:07:32,620 --> 00:07:34,570
tandis qu’on en avait 11.

126
00:07:36,830 --> 00:07:39,980
L’idée est aussi un peu de montrer que c’est faisable.

127
00:07:41,590 --> 00:07:46,920
-Au niveau des partenaires, des liens, du réseau

128
00:07:46,940 --> 00:07:50,600
que vous avez au Liban et aussi en Syrie,

129
00:07:50,610 --> 00:07:54,570
comment ça se développe, ça vient d’où ?

130
00:07:54,590 --> 00:07:56,450
Raconte-nous !

131
00:07:57,100 --> 00:08:00,160
En gros, l’idée était d’abord de produire de la semence

132
00:08:00,180 --> 00:08:03,170
et après d’apprendre à ceux que ça intéresse

133
00:08:03,190 --> 00:08:04,560
de les produire eux-mêmes,

134
00:08:04,590 --> 00:08:08,450
comme ça t’es pas obligé à chaque fois d’en refaire.

135
00:08:08,450 --> 00:08:11,030
C’est vraiment l’apprentissage de l’autonomie

136
00:08:11,040 --> 00:08:12,560
et de l’indépendence.

137
00:08:12,580 --> 00:08:16,380
Du coup, à partir de là des petits producteurs

138
00:08:16,400 --> 00:08:17,910
ont essaimé un peu partout.

139
00:08:17,920 --> 00:08:19,710
Il y en a qui sont rentrés en Syrie

140
00:08:19,710 --> 00:08:22,260
pour produire leurs semences eux-mêmes en Syrie.

141
00:08:22,280 --> 00:08:25,120
Il y a vraiment un peu de tout.

142
00:08:25,150 --> 00:08:29,610
Nos ami·e·s que nous suivons depuis le début

143
00:08:29,630 --> 00:08:32,020
sont maintenant complètement autonomes,

144
00:08:32,040 --> 00:08:34,970
illes se débrouillent très bien eux-mêmes.

145
00:08:34,980 --> 00:08:36,910
Bien sûr nous faisons un suivi,

146
00:08:36,920 --> 00:08:40,350
s’il y a besoin d’échange d’information, d’expérience:

147
00:08:40,360 --> 00:08:42,840
« nous on a testé ça », « eux ils ont testé ça ».

148
00:08:42,920 --> 00:08:46,690
L’idée c’est d’avoir autant de producteurs,

149
00:08:46,690 --> 00:08:49,360
de maisons de semences que possible

150
00:08:49,390 --> 00:08:51,640
sur tout le territoire libanais

151
00:08:51,690 --> 00:08:55,600
et peut-être même un jour syrien, on ne sait pas.

152
00:08:57,460 --> 00:09:02,240
-Ce n’est pas évident, la frontière est toujours fermée ?

153
00:09:03,970 --> 00:09:06,550
Oui, elle est fermée

154
00:09:06,570 --> 00:09:09,540
et c’est assez compliqué pour les gens

155
00:09:09,610 --> 00:09:12,040
de certaines nationalités de passer aussi.

156
00:09:12,050 --> 00:09:16,250
Si tu ne fais pas partie d'une mission diplomatique ou de l’armée

157
00:09:16,400 --> 00:09:17,990
c’est quand même dur –

158
00:09:18,660 --> 00:09:20,860
il vaut mieux pas le faire.

159
00:09:21,700 --> 00:09:23,550
Vaut mieux rester là où t’es.

160
00:09:23,580 --> 00:09:25,730
-Au niveau des formations...

161
00:09:25,920 --> 00:09:30,670
Toi tu as participé dans la traduction de DIYseeds –

162
00:09:34,330 --> 00:09:37,440
tu as traduit le site ?

163
00:09:38,750 --> 00:09:40,100
… le site en arabe,

164
00:09:40,130 --> 00:09:44,680
et j’ai revu les vidéos qui étaient faites par une autre équipe,

165
00:09:44,820 --> 00:09:47,750
un travail de titan je dois dire.

166
00:09:47,760 --> 00:09:50,090
C’est fou ce qu’ils ont fait, c’est trop beau.

167
00:09:50,170 --> 00:09:52,080
C’est hyperpratique.

168
00:09:52,120 --> 00:09:54,720
Quand tu donnes une formation

169
00:09:54,780 --> 00:09:57,840
où tu essaies d’expliquer comment extraire

170
00:09:57,870 --> 00:10:01,660
ou comment faire des distances de sécurité

171
00:10:01,670 --> 00:10:03,360
pour éviter les croisements,

172
00:10:03,380 --> 00:10:05,360
c’est mieux avec les illustrations,

173
00:10:05,360 --> 00:10:08,140
les dessins, la vidéo et les explications.

174
00:10:08,140 --> 00:10:11,550
Non seulement nous montrons ces vidéos pendant les formations,

175
00:10:11,560 --> 00:10:13,470
mais elles sont aussi accessibles

176
00:10:13,480 --> 00:10:17,000
aux gens directement sur leurs téléphones

177
00:10:17,020 --> 00:10:19,660
–quand ils ont internet, mais bon, c’est accessible-,

178
00:10:19,680 --> 00:10:23,800
ça permet aux gens de fonctionner très bien en autonomie.

179
00:10:23,910 --> 00:10:25,680
C’est nickel, c’est parfait.

180
00:10:27,600 --> 00:10:30,770
-Et c’est facile aussi pour les gens dans la région

181
00:10:31,240 --> 00:10:35,560
d’avoir ces infos, de les utiliser, de …

182
00:10:35,640 --> 00:10:36,480
Exactement!

183
00:10:36,540 --> 00:10:37,730
Ça leur facilite oui,

184
00:10:37,760 --> 00:10:40,500
parce que c’est un truc qui est assez léger, simple.

185
00:10:40,520 --> 00:10:44,910
Ce n’est pas un gros bouquin que tu dois trimbaler.

186
00:10:44,960 --> 00:10:47,940
Il y a beaucoup d’agriculteurs qui ne savent pas lire,

187
00:10:48,000 --> 00:10:50,220
mais ils savent entendre.

188
00:10:50,260 --> 00:10:52,970
Puis il y a les dessins et la vidéo en soi,

189
00:10:53,040 --> 00:10:55,570
du coup c’est beaucoup plus illustratif

190
00:10:55,580 --> 00:10:58,770
et beaucoup plus accessible et c’est plus simple,

191
00:10:58,800 --> 00:11:01,830
comme tutorial c’est plus simple d’appliquer ça

192
00:11:01,870 --> 00:11:05,270
que de sortir ton gros bouquin et déchiffrer.

193
00:11:06,000 --> 00:11:11,350
-Comme vous produisez des semences non-hybrides,

194
00:11:11,380 --> 00:11:14,580
des semences naturelles, reproductibles,

195
00:11:14,660 --> 00:11:18,030
ça permet aussi à tout le monde qui a ces semences-là

196
00:11:18,160 --> 00:11:21,380
et accès aux vidéos de tout de suite récupérer une autonomie.

197
00:11:21,400 --> 00:11:26,560
Tu peux nous raconter comment, à quelle vitesse ça peut fonctionner ?

198
00:11:26,580 --> 00:11:27,630
Quelle vitesse?

199
00:11:27,650 --> 00:11:30,530
Ça dépend de l’accès à la terre,

200
00:11:30,560 --> 00:11:32,160
ça dépend de l’accès à l’eau,

201
00:11:32,180 --> 00:11:34,310
ça dépend de la sécurité de la région.

202
00:11:34,320 --> 00:11:37,320
On a des amis qui ont dû bouger plusieurs fois

203
00:11:37,390 --> 00:11:39,260
de leur zone en Syrie.

204
00:11:39,300 --> 00:11:41,170
À chaque fois qu’ils s’installaient

205
00:11:41,180 --> 00:11:42,830
il y avaient des bombardements,

206
00:11:42,840 --> 00:11:44,810
alors ils devaient rebouger etc.

207
00:11:44,960 --> 00:11:47,240
Le contexte en soi est compliqué.

208
00:11:47,320 --> 00:11:51,670
Mais quand tu as les outils de base,

209
00:11:51,710 --> 00:11:54,660
où que tu arrives tu peux quand même te débrouiller.

210
00:11:54,690 --> 00:11:57,110
Tu as tes semences, tu sais comment les planter,

211
00:11:57,130 --> 00:12:00,260
tu sais comment les reproduire, les extraire,

212
00:12:00,290 --> 00:12:01,990
les garder, les conserver, etc.

213
00:12:02,020 --> 00:12:04,980
Tu es un homme ou une femme libre!

214
00:12:07,140 --> 00:12:08,050
C’est ça.

215
00:12:08,190 --> 00:12:12,120
-Alors tu vois des gens qui, en une saison,

216
00:12:12,170 --> 00:12:15,180
ont reappris à faire des semences

217
00:12:15,580 --> 00:12:19,470
et ils ont du coup aussi tout de suite leurs propres semences?

218
00:12:19,520 --> 00:12:20,510
Exactement.

219
00:12:20,530 --> 00:12:23,240
La plupart des gens avec lesquelles on travaille

220
00:12:23,290 --> 00:12:25,350
sont d’un milieu agricole,

221
00:12:25,370 --> 00:12:26,840
ou d’un background agricole

222
00:12:26,850 --> 00:12:29,000
ou qui ont travaillé dans l’agriculture,

223
00:12:29,020 --> 00:12:30,440
même si conventionnelle.

224
00:12:30,460 --> 00:12:34,350
C’est plus simple pour eux de faire cette transition.

225
00:12:36,550 --> 00:12:38,500
À la base ils étaient obligés

226
00:12:38,520 --> 00:12:39,970
d’acheter des semences

227
00:12:39,970 --> 00:12:42,450
et à la seconde où ils ont appris

228
00:12:42,470 --> 00:12:45,010
que leur sort était entre leurs mains,

229
00:12:45,260 --> 00:12:47,100
parce qu’il leur manquait juste

230
00:12:47,120 --> 00:12:50,730
des outils adaptés pour faire ça,

231
00:12:50,880 --> 00:12:53,890
voilà en une saison ou deux ils ont leur semence,

232
00:12:53,930 --> 00:12:55,040
on s’échange.

233
00:12:55,080 --> 00:12:56,800
On travaille ensemble sur le long terme,

234
00:12:56,820 --> 00:13:00,570
par exemple quelqu’un produit une variété,

235
00:13:00,580 --> 00:13:03,410
quelqu’un produit une autre, nous on fait la troisième…

236
00:13:03,423 --> 00:13:08,680
histoire d’avoir le plus de diversité possible et voilà,

237
00:13:08,720 --> 00:13:10,680
ça commence à fonctionner.

238
00:13:11,760 --> 00:13:16,280
-Et toi, comment tu t’es retrouvé à participer dans tout ça ?

239
00:13:18,426 --> 00:13:20,280
Vraiment par hasard.

240
00:13:20,330 --> 00:13:26,600
J’habitais à Beyrouth et mon coloc était à l'université avec Ferdi,

241
00:13:26,693 --> 00:13:30,613
qui est un des co-fondateurs de l’association et du projet.

242
00:13:32,320 --> 00:13:35,253
Alors quand Ferdi et Lara sont revenu-e-s au Liban

243
00:13:35,293 --> 00:13:38,253
pour voir comment démarrer le projet semence,

244
00:13:38,290 --> 00:13:41,270
comment produire, où faire tout ça,

245
00:13:41,308 --> 00:13:43,228
qu’est-ce qu’il faut mettre en place,

246
00:13:43,242 --> 00:13:44,927
ils sont resté-e-s chez nous

247
00:13:44,936 --> 00:13:46,592
pendant un mois à peu près

248
00:13:46,602 --> 00:13:48,545
et on a cliqué direct et voilà.

249
00:13:49,727 --> 00:13:51,303
Comme je disais:

250
00:13:51,336 --> 00:13:54,174
je me suis consciemment laissé embarquer !

251
00:13:54,988 --> 00:13:56,898
-Et tu t’es reconverti ?

252
00:13:57,068 --> 00:13:59,101
Tu faisais quoi avant ?

253
00:14:00,112 --> 00:14:03,303
Directement avant je faisais de la communication

254
00:14:03,336 --> 00:14:04,823
pour une université.

255
00:14:04,851 --> 00:14:06,960
Mais je suis libraire à la base,

256
00:14:09,520 --> 00:14:12,640
j’ai fait des études de psycho, de lettres,

257
00:14:12,672 --> 00:14:15,868
rien à voir avec les graines !

258
00:14:19,745 --> 00:14:21,520
Mais ça permet de traduire,

259
00:14:21,543 --> 00:14:22,922
du coup c’est parfait !

260
00:14:23,275 --> 00:14:24,992
-Et t’as envie de continuer ?

261
00:14:25,920 --> 00:14:27,637
Oui!

262
00:14:27,661 --> 00:14:29,863
Comme on disait tout à l’heure:

263
00:14:29,890 --> 00:14:33,002
on est en plein dans l’urgence,

264
00:14:34,164 --> 00:14:39,185
je pense que c’est vraiment un travail essentiel,

265
00:14:40,014 --> 00:14:41,477
c’est nécessaire.

266
00:14:41,515 --> 00:14:47,298
Et en plus ça enrichit à plusieurs niveaux:

267
00:14:47,310 --> 00:14:50,555
au niveau social, au niveau politique aussi.

268
00:14:50,597 --> 00:14:53,050
C’est une activité extrêmement importante

269
00:14:53,072 --> 00:14:54,823
et belle en même temps.

270
00:14:55,247 --> 00:14:58,211
Je ne pense pas que j’arrêterai bientôt !

271
00:14:58,418 --> 00:15:01,797
-Super ! Merci pour cet entretien.

272
00:15:01,960 --> 00:15:03,722
Je t’en prie, je t’en prie!

273
00:15:03,755 --> 00:15:06,290
C’était cool de te reparler aussi.

274
00:15:07,620 --> 00:15:08,936
-J’espère qu’on se reverra.

275
00:15:08,960 --> 00:15:11,134
On t’attend! On t’attend à la ferme!

276
00:15:11,181 --> 00:15:15,811
-J’ai bien envie:) Je ne peux rien promettre mais…

277
00:15:17,463 --> 00:15:20,560
C’est quand tu veux, tu nous dis que tu débarques

278
00:15:20,583 --> 00:15:22,672
et « ahla ou sahla » (bienvenu!)

279
00:15:25,251 --> 00:15:26,409
-Merci beaucoup!

280
00:15:26,432 --> 00:15:27,091
Parfait!

281
00:15:28,120 --> 00:15:35,720
Many thanks to Serge Harfouche and everyone at Buzuruna Juzuruna
more information: @buzurunajuzuruna (Instagram)
photos: Charlotte Joubert, Buzuruna Juzuruna, Rabih Yassine
interview & editing: Erik D'haese

282
00:15:35,720 --> 00:15:40,320
www.DIYseeds.org
educational films on seed production
