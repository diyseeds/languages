﻿1
00:00:00,000 --> 00:00:05,760
Interjú Serge Harfouche-sal a Buzuruna Juzurunától 
2021 május, Beqaa-völgy, Libanon
www.DIYseeds.org
oktatóvideó a magtermesztésről

2
00:00:05,920 --> 00:00:08,760
- Mit jelent az, hogy "Buzuruna Juzuruna"?

3
00:00:09,520 --> 00:00:12,100
Azt, hogy "a magjaink, a gyökereink".

4
00:00:12,140 --> 00:00:16,150
- Hogyan képviseli ez a név a szervezetet?

5
00:00:17,870 --> 00:00:25,170
Egy magtermesztő iskolafarm vagyunk,

6
00:00:26,090 --> 00:00:30,570
és képzéseket is tartunk.

7
00:00:30,660 --> 00:00:35,520
Ez a két fő működési területünk.

8
00:00:36,220 --> 00:00:39,350
Vetőmagokat állítunk elő,

9
00:00:39,530 --> 00:00:42,460
amelyekből aztán gyökerek nőnek.

10
00:00:42,460 --> 00:00:47,020
Célunk, hogy erősítsük a gazdák autonómiáját

11
00:00:47,130 --> 00:00:49,380
és az élelmiszer-önrendelkezést.

12
00:00:49,380 --> 00:00:53,120
Ezek a dolgok kéz a kézben járnak.

13
00:00:54,080 --> 00:01:04,730
- Miért ilyen fontos jelenleg az élelmiszer-önrendelkezés kérdése Libanonban?

14
00:01:05,600 --> 00:01:07,840
Vészhelyzet van.

15
00:01:07,860 --> 00:01:13,600
Soha nem látott gazdasági válságban vagyunk,

16
00:01:13,600 --> 00:01:15,600
ami rengeteg erőszakkal is társul.

17
00:01:15,710 --> 00:01:24,150
Az ország valutája 10-szer kevesebbet ér, mint régen.

18
00:01:24,260 --> 00:01:30,020
Régen 1500 Libanoni font volt 1 dollár. Egy ponton elérte 15,000 font értéket.

19
00:01:30,040 --> 00:01:32,880
Jelenleg kb. 12.000 fontnál állt meg.

20
00:01:32,950 --> 00:01:36,600
Teljesen kiszámíthatatlan, nem tudjuk, hogy mikor kezd el zuhanni.

21
00:01:36,680 --> 00:01:37,770
- Mikor történt mindez?

22
00:01:37,860 --> 00:01:43,400
2018-ban dőlt be.

23
00:01:43,510 --> 00:01:54,640
A piaci infláció 2019-ben, 2020 elején kezdődött.

24
00:01:55,950 --> 00:02:01,260
- Mi történik most, milyen hatással van mindez az emberek életére?

25
00:02:02,600 --> 00:02:07,020
Félelmetes a helyzet, mivel a fizetések nem igazodtak a libanoni font zuhanásához.

26
00:02:07,060 --> 00:02:12,200
A minimálbér régen kb. havi 400 € volt.

27
00:02:12,200 --> 00:02:17,350
Most kb. 40 €-nak felel meg,

28
00:02:17,880 --> 00:02:21,620
viszont minden áru drágább lett,

29
00:02:21,680 --> 00:02:24,750
mivel azokat a dollárhoz kötik. Rettenetes a helyzet.

30
00:02:24,840 --> 00:02:27,910
Képzeld el, hogy veszel egy doboz tejet.

31
00:02:27,930 --> 00:02:31,530
Ami 3000 libanoni fontba került,

32
00:02:31,530 --> 00:02:36,480
az most 27 vagy 25.000,

33
00:02:36,600 --> 00:02:40,200
miközben a fizetésed még mindig 100.000.

34
00:02:40,200 --> 00:02:44,160
Így ahelyett, hogy sok dobozt vehetnél egy hónapban,

35
00:02:44,260 --> 00:02:47,380
épp csak egyet tudsz venni.

36
00:02:47,440 --> 00:02:59,180
Mivel a fogyasztásunk 90-95%-a az importtól függ,

37
00:02:59,260 --> 00:03:01,580
külföldi valutához kötöttek az árak,

38
00:03:01,620 --> 00:03:03,260
jelen esetben a dollárhoz,

39
00:03:03,310 --> 00:03:05,520
és minden nagyon megdrágult:

40
00:03:05,570 --> 00:03:08,210
a kenyér is megdrágult,

41
00:03:08,360 --> 00:03:11,450
az üzemanyag is megdrágult,

42
00:03:11,560 --> 00:03:14,660
a fizetések viszont maradtak.

43
00:03:14,690 --> 00:03:17,720
Így az életszínvonal,

44
00:03:17,750 --> 00:03:20,820
az életminőség leromlott,

45
00:03:20,910 --> 00:03:23,090
és az ételhez való hozzáférés

46
00:03:23,110 --> 00:03:26,530
és az élelmiszer-biztonság szintén.

47
00:03:26,720 --> 00:03:30,370
Ezért fontos megtermeszteni a saját magjainkat

48
00:03:30,430 --> 00:03:33,200
ahelyett, hogy máshonnan importálnánk vagy vennénk,

49
00:03:33,220 --> 00:03:35,490
főleg ha azok hibridek és sterilek

50
00:03:35,520 --> 00:03:38,230
és utána megint meg kell venni őket.

51
00:03:40,010 --> 00:03:43,980
A talaj termékenységére is gondolnunk kell,

52
00:03:44,020 --> 00:03:46,480
nem használunk műtrágyát,

53
00:03:46,530 --> 00:03:49,810
komposztálunk...

54
00:03:49,890 --> 00:03:51,540
Mindent, ahogy mondják.

55
00:03:51,570 --> 00:03:55,290
Ezt szeretnénk elérni az oktatással,

56
00:03:55,330 --> 00:03:57,330
a farm összes tevékenységével.

57
00:03:58,730 --> 00:04:01,460
- Franciaországban mutattál

58
00:04:01,470 --> 00:04:04,110
képeket arról, hogy

59
00:04:04,160 --> 00:04:07,630
a "forradalom" után

60
00:04:07,640 --> 00:04:10,560
sok ember élete megváltozott,

61
00:04:10,660 --> 00:04:12,610
sokféle ember élete:

62
00:04:12,630 --> 00:04:18,410
diákoké, közigazgatásban dolgozóké,

63
00:04:18,430 --> 00:04:20,730
mindenféle embernek, aki ide jön,

64
00:04:20,750 --> 00:04:23,360
és hirtelen érdeklődni kezd.

65
00:04:23,630 --> 00:04:24,810
Pontosan.

66
00:04:24,930 --> 00:04:27,530
Tudtuk, hogy baj lesz,

67
00:04:28,180 --> 00:04:33,380
hogy már egy példa nélküli gazdasági válságban vagyunk.

68
00:04:33,490 --> 00:04:38,210
Ezért tört ki a forradalom.

69
00:04:38,270 --> 00:04:43,040
Az emberek sejtették,

70
00:04:43,040 --> 00:04:47,650
hogy az elkövetkező időszak nem lesz könnyű,

71
00:04:47,680 --> 00:04:50,490
és hogy túl kell élniük valahogy.

72
00:04:50,540 --> 00:04:54,340
Egyre inkább tudatában vagyunk,

73
00:04:54,400 --> 00:05:00,000
hogy improduktív gazdaság nem fenntartható,

74
00:05:00,080 --> 00:05:03,130
a miénk pedig nem volt hatékony gazdaság.

75
00:05:03,460 --> 00:05:05,920
Hosszú ideig, 30-40 évig,

76
00:05:05,970 --> 00:05:10,250
nagyon hitelalapú volt a gazdaság.

77
00:05:11,090 --> 00:05:15,320
- Akkor sokan kezdtek termelésbe?

78
00:05:15,460 --> 00:05:17,980
...

79
00:05:18,040 --> 00:05:19,290
Adásban vagyok.

80
00:05:20,430 --> 00:05:23,260
Bocsánat, a kollégám volt az.

81
00:05:23,280 --> 00:05:26,070
- Sokan vannak a farmon?

82
00:05:26,850 --> 00:05:31,620
16 felnőtt, 27 gyerek,

83
00:05:31,650 --> 00:05:34,160
és most épp sok barátunk van itt látogatóban.

84
00:05:34,210 --> 00:05:36,000
Önkéntesek, gyakornokok...

85
00:05:36,030 --> 00:05:38,930
Összesen kb. 50-en vagyunk,

86
00:05:39,000 --> 00:05:40,370
talán kicsit többen is.

87
00:05:40,410 --> 00:05:43,350
- És mekkora maga a hely?

88
00:05:44,940 --> 00:05:47,770
Az egyesületi gazdaságban

89
00:05:47,920 --> 00:05:54,140
5 hektár földünk van:

90
00:05:54,580 --> 00:05:57,940
4,5 hektár a terménynek,

91
00:05:57,950 --> 00:06:00,500
és fél hektár az épületeknek,

92
00:06:00,670 --> 00:06:02,980
a palántanevelésre,

93
00:06:03,070 --> 00:06:06,050
és az állatoknak, csirkéknek,

94
00:06:06,060 --> 00:06:07,640
kecskéknek, bárányoknak.

95
00:06:07,710 --> 00:06:09,820
Idén lehetőségünk van

96
00:06:09,830 --> 00:06:13,490
plusz 70.000 négyzetméter bérlésére,

97
00:06:13,610 --> 00:06:16,600
kb. 17 hektár,

98
00:06:16,700 --> 00:06:19,700
hogy szaporítsuk és bővítsük a gyűjteményt,

99
00:06:19,720 --> 00:06:21,350
különösen a gabonákat.

100
00:06:22,280 --> 00:06:24,770
- De nem a farmon laktok?

101
00:06:25,610 --> 00:06:27,510
Nem igazán.

102
00:06:27,520 --> 00:06:32,860
Az egyik alapító kollégánk a a farmon él.

103
00:06:34,400 --> 00:06:36,320
Van egy többfunkciós épületünk,

104
00:06:36,340 --> 00:06:39,780
egy feldolgozó helység,

105
00:06:39,820 --> 00:06:43,090
konyha, raktár, iskola

106
00:06:43,140 --> 00:06:44,350
és lakás.

107
00:06:44,360 --> 00:06:47,040
Jó hosszú épület.

108
00:06:47,060 --> 00:06:50,120
Walid és a családja lakik ott.

109
00:06:50,260 --> 00:06:52,440
A többi kollégánk és barátun

110
00:06:52,460 --> 00:06:54,260
az utca túloldalán lakik

111
00:06:54,310 --> 00:06:56,530
mi pedig kicsit feljebb az utcában,

112
00:06:56,550 --> 00:06:58,450
gyalog 4 percre.

113
00:06:59,590 --> 00:07:02,290
- Megtermeltek valamennyit a

114
00:07:02,300 --> 00:07:04,400
saját fogyasztásotokból is?

115
00:07:04,740 --> 00:07:06,950
Igen!

116
00:07:07,160 --> 00:07:10,070
Keveset vásárolunk.

117
00:07:10,110 --> 00:07:13,770
Amit csak lehet, megtermelünk, nem vesszük meg!

118
00:07:14,900 --> 00:07:17,980
Néhány dolgot, pl. rizst

119
00:07:18,010 --> 00:07:20,880
nehezen tudunk megtermelni,

120
00:07:20,910 --> 00:07:22,690
azt megvesszük.

121
00:07:22,920 --> 00:07:24,120
De ezeken kívül…

122
00:07:24,130 --> 00:07:27,230
Tavaly még egy kis mézet is sikerült termelnünk,

123
00:07:27,250 --> 00:07:30,520
de valami megtámadta a méheket,

124
00:07:30,560 --> 00:07:32,600
és most csak 3 kaptárunk van,

125
00:07:32,620 --> 00:07:34,570
a 11 helyett.

126
00:07:36,830 --> 00:07:39,980
Fontos, hogy megmutassuk: lehetséges.

127
00:07:41,590 --> 00:07:46,920
- Mi a helyzet a partnerekkel, a hálózattal,

128
00:07:46,940 --> 00:07:50,600
amit Libanonban és Szíriában építetek,

129
00:07:50,610 --> 00:07:54,570
hogy kezdődött, hogy alakul?

130
00:07:54,590 --> 00:07:56,450
Mesélj erről!

131
00:07:57,100 --> 00:08:00,160
Az alapötlet az volt, hogy termesszünk magokat,

132
00:08:00,180 --> 00:08:03,170
és megtanítsuk azoknak, akiket érdekel,

133
00:08:03,190 --> 00:08:04,560
hogy foghatnak saját magot,

134
00:08:04,590 --> 00:08:08,450
hogy ne kelljen mindent nekünk termeszteni.

135
00:08:08,450 --> 00:08:11,030
Ez az egész az önállóságról szól,

136
00:08:11,040 --> 00:08:12,560
és a függetlenségről.

137
00:08:12,580 --> 00:08:16,380
Ezután a magfogók

138
00:08:16,400 --> 00:08:17,910
szétszéledtek.

139
00:08:17,920 --> 00:08:19,710
Volt, aki visszatért Szíriába,

140
00:08:19,710 --> 00:08:22,260
hogy ott termessze a saját fajtáit.

141
00:08:22,280 --> 00:08:25,120
Annyiféle élethelyzet van!

142
00:08:25,150 --> 00:08:29,610
Azok a barátaink, akiket a kezdetektől fogva követünk,

143
00:08:29,630 --> 00:08:32,020
ma már teljesen önellátóak.

144
00:08:32,040 --> 00:08:34,970
Jól megy a soruk.

145
00:08:34,980 --> 00:08:36,910
Tartjuk a kapcsolatot,

146
00:08:36,920 --> 00:08:40,350
ha valamilyen tudás- vagy tapasztalatcserére lenne szükség:

147
00:08:40,360 --> 00:08:42,840
"ezt kipróbáltuk", "azt letesztelték".

148
00:08:42,920 --> 00:08:46,690
Szeretnénk, ha lenne annyi magfogó,

149
00:08:46,690 --> 00:08:49,360
és magbank, amennyi csak lehet,

150
00:08:49,390 --> 00:08:51,640
egész Libanonban,

151
00:08:51,690 --> 00:08:55,600
és egy nap talán Szíriában is, ki tudja.

152
00:08:57,460 --> 00:09:02,240
- A határ még mindig zárva van?

153
00:09:03,970 --> 00:09:06,550
Igen, zárva,

154
00:09:06,570 --> 00:09:09,540
és elég bonyolult átjutni

155
00:09:09,610 --> 00:09:12,040
bizonyos nemzetiségűek számára.

156
00:09:12,050 --> 00:09:16,250
Ha nem diplomata vagy a hadsereg tagja az ember,

157
00:09:16,400 --> 00:09:17,990
nehéz átmenni -

158
00:09:18,660 --> 00:09:20,860
nem érdemes próbálkozni.

159
00:09:21,700 --> 00:09:23,550
Jobb itthon maradni.

160
00:09:23,580 --> 00:09:25,730
- A képzésekről...

161
00:09:25,920 --> 00:09:30,670
Részt vettél a DIYseeds fordításában -

162
00:09:34,330 --> 00:09:37,440
te fordítottad a honlapot?

163
00:09:38,750 --> 00:09:40,100
... a honlapot arab nyelvre,

164
00:09:40,130 --> 00:09:44,680
és átnéztem a videókat, amiket egy másik csapat készített,

165
00:09:44,820 --> 00:09:47,750
- rengeteg munka volt.

166
00:09:47,760 --> 00:09:50,090
Elképesztő a végeredmény, gyönyörű lett.

167
00:09:50,170 --> 00:09:52,080
És nagyon gyakorlatias.

168
00:09:52,120 --> 00:09:54,720
Amikor tanfolyamot tartunk,

169
00:09:54,780 --> 00:09:57,840
és a magfogást próbáljuk elmagyarázni,

170
00:09:57,870 --> 00:10:01,660
vagy az izolációs távolságokat,

171
00:10:01,670 --> 00:10:03,360
hogy elkerüljük a keresztbeporzódást,

172
00:10:03,380 --> 00:10:05,360
egyszerűbb képekkel csinálni,

173
00:10:05,360 --> 00:10:08,140
a rajzokkal, a videóval és a magyarázatokkal.

174
00:10:08,140 --> 00:10:11,550
A videókat nem csak a megmutatjuk a résztvevőknek,

175
00:10:11,560 --> 00:10:13,470
hanem meg is lehet nézni őket

176
00:10:13,480 --> 00:10:17,000
bárkinek a telefonjáról

177
00:10:17,020 --> 00:10:19,660
- ha van internetük, de legalább elérhetőek -,

178
00:10:19,680 --> 00:10:23,800
így remekül tudnak egyedül is boldogulni.

179
00:10:23,910 --> 00:10:25,680
Ez nagyszerű, csodálatos.

180
00:10:27,600 --> 00:10:30,770
- És a régióban lakók számára

181
00:10:31,240 --> 00:10:35,560
hozzáférni az információhoz, használni.…

182
00:10:35,640 --> 00:10:36,480
Pontosan!

183
00:10:36,540 --> 00:10:37,730
Megkönnyíti a dolgukat,

184
00:10:37,760 --> 00:10:40,500
mert teljesen egyértelmű, egyszerű oldal.

185
00:10:40,520 --> 00:10:44,910
Nem egy vastag könyv, amit cipelni kell.

186
00:10:44,960 --> 00:10:47,940
Sok gazda van, aki nem tud olvasni,

187
00:10:48,000 --> 00:10:50,220
de megértik, amit hallanak.

188
00:10:50,260 --> 00:10:52,970
A képek és a videó

189
00:10:53,040 --> 00:10:55,570
sokkal jobban illusztrálják a szavakat,

190
00:10:55,580 --> 00:10:58,770
könnyebben elérhető és egyszerűbb,

191
00:10:58,800 --> 00:11:01,830
könnyen követhető útmutató,

192
00:11:01,870 --> 00:11:05,270
mint ha könyvből kéne kimazsolázni az egészet.

193
00:11:06,000 --> 00:11:11,350
- Mivel a fajtáitok nem hibridek,

194
00:11:11,380 --> 00:11:14,580
lehet magot fogni róluk

195
00:11:14,660 --> 00:11:18,030
mindenki, akinek van belőlük,

196
00:11:18,160 --> 00:11:21,380
és hozzáfér a videókhoz, gyorsan önállósodni tud.

197
00:11:21,400 --> 00:11:26,560
Elárulod, mennyire gyorsan?

198
00:11:26,580 --> 00:11:27,630
Milyen gyorsan?

199
00:11:27,650 --> 00:11:30,530
Ez a földhöz való hozzáféréstől függ,

200
00:11:30,560 --> 00:11:32,160
a vízhez való hozzáféréstől,

201
00:11:32,180 --> 00:11:34,310
az adott környék biztonsági helyzetétől.

202
00:11:34,320 --> 00:11:37,320
Vannak barátaink, akiknek többször is költözniük kellett,

203
00:11:37,390 --> 00:11:39,260
a lakóhelyükről Szíriában.

204
00:11:39,300 --> 00:11:41,170
Mindig amikor letelepedtek,

205
00:11:41,180 --> 00:11:42,830
bombázások következtek,

206
00:11:42,840 --> 00:11:44,810
és újra meg újra útra kellett kelniük.

207
00:11:44,960 --> 00:11:47,240
Bonyolultak a körülmények.

208
00:11:47,320 --> 00:11:51,670
De ha megvannak az alapvető eszközök,

209
00:11:51,710 --> 00:11:54,660
boldogul az ember, bárhol is köt ki.

210
00:11:54,690 --> 00:11:57,110
Megvannak a magok, tudjuk, hogy kell elültetni őket,

211
00:11:57,130 --> 00:12:00,260
hogy kell felszaporítani a fajtákat, magot fogni,

212
00:12:00,290 --> 00:12:01,990
tárolni, megőrizni a magokat.

213
00:12:02,020 --> 00:12:04,980
Szabadok vagyunk!

214
00:12:07,140 --> 00:12:08,050
Ennyi.

215
00:12:08,190 --> 00:12:12,120
- Vannak olyanok, akik egy szezon alatt

216
00:12:12,170 --> 00:12:15,180
megtanultak magot termeszteni...

217
00:12:15,580 --> 00:12:19,470
Így most saját magot fognak?

218
00:12:19,520 --> 00:12:20,510
Pontosan.

219
00:12:20,530 --> 00:12:23,240
A legtöbb ember, akikkel dolgozunk,

220
00:12:23,290 --> 00:12:25,350
mezőgazdálkodásban nőtt fel,

221
00:12:25,370 --> 00:12:26,840
gazdálkodói háttérrel jönnek,

222
00:12:26,850 --> 00:12:29,000
vagy mezőgazdaságban dolgoztak,

223
00:12:29,020 --> 00:12:30,440
gyakran konvencionális gazdaságokban.

224
00:12:30,460 --> 00:12:34,350
Nekik könnyebb az átállás.

225
00:12:36,550 --> 00:12:38,500
Korábban meg kellett

226
00:12:38,520 --> 00:12:39,970
venniük a magokat

227
00:12:39,970 --> 00:12:42,450
és ahogy megtudták,

228
00:12:42,470 --> 00:12:45,010
hogy a sorsuk a saját kezükben van,

229
00:12:45,260 --> 00:12:47,100
mert csak a megfelelő

230
00:12:47,120 --> 00:12:50,730
eszközökre volt szükségük,

231
00:12:50,880 --> 00:12:53,890
egy vagy két szezon után már saját magjaik vannak,

232
00:12:53,930 --> 00:12:55,040
és csereberélünk.

233
00:12:55,080 --> 00:12:56,800
Hosszútávon dolgozunk együtt,

234
00:12:56,820 --> 00:13:00,570
például valaki termeszt egy fajtát,

235
00:13:00,580 --> 00:13:03,410
valaki más egy másikat, mi pedig egy harmadikat…

236
00:13:03,423 --> 00:13:08,680
hogy a lehető legnagyobb legyen a változatosság, és ennyi,

237
00:13:08,720 --> 00:13:10,680
elkezd működni.

238
00:13:11,760 --> 00:13:16,280
- És te, te hogy kerültél ebbe az egészbe?

239
00:13:18,426 --> 00:13:20,280
Véletlenül.

240
00:13:20,330 --> 00:13:26,600
Bejrútban laktam, és a lakótársam Ferdivel járt egy egyetemre,

241
00:13:26,693 --> 00:13:30,613
aki az egyesület és a projekt egyik alapítója.

242
00:13:32,320 --> 00:13:35,253
Amikor Ferdi és Lara visszatértek Libanonba,

243
00:13:35,293 --> 00:13:38,253
és gondolkoztak, hogy kezdjék el a magos projektet

244
00:13:38,290 --> 00:13:41,270
hogy termesszék, hol csinálják az egészet,

245
00:13:41,308 --> 00:13:43,228
mit kell elintézni,

246
00:13:43,242 --> 00:13:44,927
nálunk laktak,

247
00:13:44,936 --> 00:13:46,592
nagyjából egy hónapig,

248
00:13:46,602 --> 00:13:48,545
nagyon jól kijöttünk egymással. Így kezdődött.

249
00:13:49,727 --> 00:13:51,303
Ahogy már mondtam:

250
00:13:51,336 --> 00:13:54,174
Tudatosan engedtem, hogy belerángassanak!

251
00:13:54,988 --> 00:13:56,898
- És megváltoztattad az életed?

252
00:13:57,068 --> 00:13:59,101
Előtte mit csináltál?

253
00:14:00,112 --> 00:14:03,303
Előtte a kommunikációs területen dolgoztam,

254
00:14:03,336 --> 00:14:04,823
egy egyetemen.

255
00:14:04,851 --> 00:14:06,960
Eredetileg könyvtáros vagyok,

256
00:14:09,520 --> 00:14:12,640
pszichológiát és irodalmat tanultam,

257
00:14:12,672 --> 00:14:15,868
semmi közöm nem volt magokhoz!

258
00:14:19,745 --> 00:14:21,520
De a fordításhoz jól jön,

259
00:14:21,543 --> 00:14:22,922
úgyhogy nem baj!

260
00:14:23,275 --> 00:14:24,992
- És szeretnéd folytatni?

261
00:14:25,920 --> 00:14:27,637
Igen!

262
00:14:27,661 --> 00:14:29,863
Ahogy korábban mondtuk:

263
00:14:29,890 --> 00:14:33,002
egy vészhelyzet közepén vagyunk,

264
00:14:34,164 --> 00:14:39,185
szerintem ez a munka létfontosságú,

265
00:14:40,014 --> 00:14:41,477
szükséges.

266
00:14:41,515 --> 00:14:47,298
És sok szinten kifizetődő:

267
00:14:47,310 --> 00:14:50,555
társadalmi és politikai szinten is.

268
00:14:50,597 --> 00:14:53,050
A munkánk kiemelkedően fontos

269
00:14:53,072 --> 00:14:54,823
ugyanakkor gyönyörű is.

270
00:14:55,247 --> 00:14:58,211
Nem hiszem, hogy valaha is megunom!

271
00:14:58,418 --> 00:15:01,797
- Remek! Köszönöm az interjút!

272
00:15:01,960 --> 00:15:03,722
Szívesen, nagyon szívesen!

273
00:15:03,755 --> 00:15:06,290
Jó volt ismét találkozni veled.

274
00:15:07,620 --> 00:15:08,936
- Remélem találkozunk még!

275
00:15:08,960 --> 00:15:11,134
Várunk szeretettel a gazdaságban!

276
00:15:11,181 --> 00:15:15,811
- Szívesen mennék :-) Nem ígérek semmit, de…

277
00:15:17,463 --> 00:15:20,560
Csak szólj, ha jönnél, amikor csak akarsz,

278
00:15:20,583 --> 00:15:22,672
és "ahla wa sahla" (Isten hozott)!

279
00:15:25,251 --> 00:15:26,409
- Nagyon köszönöm!

280
00:15:26,432 --> 00:15:27,091
Remek!

281
00:15:28,120 --> 00:15:35,720
Hálás köszönet Serge Harfouche-nak és mindenkinek a Buzuruna Juzurunában
további információ: @buzurunajuzuruna (Instagram)
képek: Charlotte Joubert, Buzuruna Juzuruna, Rabih Yassine
interjú és szerkesztés: Erik D'haese

282
00:15:35,720 --> 00:15:40,320
www.DIYseeds.org
oktatóvideók a magfogásról
