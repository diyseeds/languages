﻿1
00:00:11,220 --> 00:00:15,098
A galambbegysaláta a loncfélék (Caprifoliaceae) 
családjába tartozik.

2
00:00:17,200 --> 00:00:20,065
Téli, illetve kora tavaszi növény.

3
00:00:21,636 --> 00:00:23,890
Két faját termesztik:

4
00:00:25,120 --> 00:00:27,300
a Valerianella locustát,

5
00:00:32,916 --> 00:00:35,700
és a Valerianella eriocarpát,

6
00:00:36,340 --> 00:00:41,389
ami ízletesebb, és elsősorban 
Olaszországban és Dél-Franciaországban termesztik. 

7
00:00:46,596 --> 00:00:50,007
A galambbegysaláta Európában vadon is nő. 

8
00:01:02,700 --> 00:01:03,789
Megporzás 

9
00:01:14,487 --> 00:01:21,869
A galambbegysaláta öntermékeny növény.
Kétivarú önbeporzó virágai vannak, vagyis

10
00:01:22,436 --> 00:01:26,356
a hímivarú és a nőivarú szaporítószervek ugyanabban 
a virágban találhatóak,

11
00:01:26,574 --> 00:01:27,847
és kompatibilisek egymással.

12
00:01:29,578 --> 00:01:36,501
Ennek ellenére a különböző fajták között 
előfordulhat keresztbeporzás a rovarok miatt. 

13
00:01:43,607 --> 00:01:48,516
A V. locusta és a év a V. eriocarpa fajok nem

14
00:01:50,727 --> 00:01:57,110
kereszteződnek egymással, viszont a vad galambbegysaláta 
bármelyik termesztett fajtával kereszteződhet. 

15
00:02:02,225 --> 00:02:04,734
A genetikai tisztaság biztosítása érdekében

16
00:02:05,010 --> 00:02:11,760
érdemes 50 m izolációs távolságot hagyni 
a galambbegysaláta egy fajába tartozó fajtái között.

17
00:02:12,770 --> 00:02:19,738
Ezt 30 méterre csökkenthetjük, ha a fajták 
között van valamilyen természetes akadály, pl. sövény. 

18
00:02:22,960 --> 00:02:24,380
Életciklus

19
00:02:35,941 --> 00:02:41,214
A magfogásra vetett galambbegysalátákat ugyanúgy 
termesztjük, mint a fogyasztásra nevelteket.

20
00:02:42,014 --> 00:02:44,349
Kora ősszel kell elvetni a magjukat,

21
00:02:46,523 --> 00:02:51,527
télen áttelelnek a kertben, 
majd tavasszal virágzanak és magot hoznak. 

22
00:03:01,338 --> 00:03:05,832
A genetikai sokféleség biztosítása 
érdekében minimum 50 növényt neveljünk! 

23
00:03:08,989 --> 00:03:13,694
A magfogásra szánt egyedekről ne szedjünk levelet! 

24
00:03:17,585 --> 00:03:21,220
A kiválasztási kritériumok között 
szerepel a hidegtűrés,

25
00:03:22,356 --> 00:03:25,260
a levelek mérete, alakja és színe,

26
00:03:28,203 --> 00:03:33,590
a gombabetegségekkel szembeni ellenálló képesség, a késői virágzás. 

27
00:03:39,469 --> 00:03:44,276
Szabaduljunk meg a nem megfelelő formájú 
növényektől, amelyek nem fajtaazonosak! 

28
00:04:35,141 --> 00:04:41,670
A magok lassan érnek, és könnyen kiperegnek, 
ha elérték a megfelelő érettségi állapotot. 

29
00:04:52,740 --> 00:04:55,927
Ezért figyelemmel kell kísérni az 
érés folyamatát, és nem

30
00:04:56,480 --> 00:04:59,883
szabad addig várni, amíg a növények teljesen elszáradnak.

31
00:05:00,712 --> 00:05:06,720
Ideális esetben addig várunk a betakarítással, 
amíg a magok fele beérik. 

32
00:05:07,578 --> 00:05:11,912
A betakarítás során fellépő túlzott magveszteség 
elkerülése érdekében a

33
00:05:12,378 --> 00:05:17,040
betakarítás előtt terítsünk 
ponyvát a talajra a növények köré! 

34
00:05:17,985 --> 00:05:23,869
A növényeket száraz, jól 
szellőző helyen hagyjuk 2-3 hétig tovább száradni!

35
00:05:27,060 --> 00:05:29,069
A túlmelegedés elkerülése érdekében

36
00:05:29,323 --> 00:05:32,552
a növényeket nem szabad 
túl vastagon rétegezni,

37
00:05:37,876 --> 00:05:41,636
és folyamatosan kísérjük 
figyelemmel a folyamatot! 

38
00:05:51,447 --> 00:05:54,060
Magfogás – tisztítás - tárolás

39
00:05:58,196 --> 00:06:02,349
Egy napsütéses napon fogjunk magot 
a száraz növények összedörzsölésével!

40
00:06:04,174 --> 00:06:09,330
Ne próbáljuk meg a legutolsó apró magokat 
is kiszedni, azok valószínűleg éretlenek! 

41
00:06:11,760 --> 00:06:15,970
Különböző finomságú sziták segítségével
 távolítsuk el a durvább

42
00:06:16,378 --> 00:06:20,509
növényi törmelékeket, és 
az apróbb szennyeződéseket! 

43
00:06:43,178 --> 00:06:47,280
Ezt követően szeleljük ki a magokat, 
például a szél segítségével. 

44
00:07:05,880 --> 00:07:10,007
Ekkor a magok még nem a végleges 
színüket mutatják,

45
00:07:10,356 --> 00:07:12,443
a tárolás során még sötétedni fognak. 

46
00:07:14,487 --> 00:07:16,770
A magnyugalmi időszak két hónapig tart,

47
00:07:16,923 --> 00:07:21,549
a legjobb csírázás 1 vagy 
akár 2 év után tapasztalható.

48
00:07:22,720 --> 00:07:26,356
Ezért a korábbi években fogott magokat 
érdemes használni a vetés során. 

49
00:07:27,861 --> 00:07:32,101
Írjuk fel egy címkére a faj és a fajta 
nevét, valamint a magfogás évét,

50
00:07:32,334 --> 00:07:38,021
majd helyezzük azt a tasak belsejébe!
 A külső felirat könnyen letörlődhet. 

51
00:07:42,720 --> 00:07:47,265
Néhány fagyasztóban töltött nap 
alatt a kártevők lárvái elpusztulnak. 

52
00:07:50,720 --> 00:07:55,120
A galambbegysaláta magjai átlagosan 5 évig 
őrzik meg a csírázóképességüket.

53
00:08:00,240 --> 00:08:03,447
Ezt úgy lehet meghosszabbítani, 
ha a magokat fagyasztóban tároljuk. 
