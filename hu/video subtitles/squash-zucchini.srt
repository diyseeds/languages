1
00:00:09,440 --> 00:00:14,305
A tök és a cukkini is a tökfélék
(Cucurbitaceae) családjába tartozik.

2
00:00:16,960 --> 00:00:21,260
A legtöbbjük egyéves növény, 
és több fajba sorolhatóak:

3
00:00:26,260 --> 00:00:29,950
Az úritök (Cucurbita pepo) fajba 
tartoznak a cukkinik,

4
00:00:31,305 --> 00:00:32,600
a patisszonok (csillagtökök),

5
00:00:33,950 --> 00:00:35,105
az olajtökök

6
00:00:37,300 --> 00:00:39,500
és számos dísztök is.

7
00:00:43,660 --> 00:00:49,435
A legtöbb bokros növekedésű, szúrós levelekkel, 
amelyek gyakran mélyen szeldeltek.

8
00:00:50,525 --> 00:00:52,395
A száruk szintén szúrós.

9
00:00:55,480 --> 00:00:56,970
A magok domborúak,

10
00:00:57,255 --> 00:01:01,775
egyöntetűen szürkésfehér színűek, 
szélükön fehér szegély fut körbe. 

11
00:01:05,160 --> 00:01:12,340
 A sütőtök (Cucurbita maxima) fajba tartoznak 
Argentínában és Bolíviában megtalálható vad alakok is.

12
00:01:13,520 --> 00:01:19,985
Ennek a fajnak a szára hosszú,
de nem túl erős, szőrös, a talajon fut. 

13
00:01:23,010 --> 00:01:29,970
A levelek nagyok, szőrösek, sohasem
mélyen szeldeltek, lekerekített karéjúak.

14
00:01:32,095 --> 00:01:36,765
A terméskocsány is mindig lekerekített, 
és úgy néz ki, mint egy dugó.

15
00:01:38,800 --> 00:01:42,110
Sokszor megreped,
és jóval vastagabb, mint maga a szár.

16
00:01:46,120 --> 00:01:50,330
A magokat gyakran könnyen 
eltávolítható hártya borítja.

17
00:01:51,060 --> 00:01:53,785
A magok oválisak, általában duzzadtak. 

18
00:01:57,590 --> 00:02:03,160
A pézsmatök (Cucurbita moshata) fajba 
tartozó valamennyi növény kúszik a földön.

19
00:02:03,360 --> 00:02:06,980
Nagyon hosszú, szőrös, 
cikkcakk alakú száruk van.

20
00:02:11,755 --> 00:02:14,250
A leveleken gyakran vannak fehér pöttyök.

21
00:02:14,575 --> 00:02:18,790
A levélcsúcs hegyes, a levelek 
széle pedig enyhén fogazott.

22
00:02:20,265 --> 00:02:24,285
A kocsány kemény, szőrös, 
és kissé szögletes.

23
00:02:25,520 --> 00:02:29,955
A magok bézs színűek, téglalap alakúak, 
és sötétbézs szegély veszi körbe őket.

24
00:02:32,565 --> 00:02:35,850
Ennek a fajnak magasabb hőmérsékletre 
van szüksége, mint a többi töknek.

25
00:02:37,500 --> 00:02:40,615
Hidegebb éghajlatú területeken a 
termése nem mindig érik be teljesen. 

26
00:02:53,100 --> 00:02:54,085
Megporzás 

27
00:03:01,335 --> 00:03:03,990
A tökök egylaki növények,

28
00:03:04,100 --> 00:03:08,220
vagyis a hímivarú és a nőivarú virágok 
ugyanazon az egyeden találhatóak. 

29
00:03:09,170 --> 00:03:12,380
A nőivarú virágok esetében 
magház található a virág alatt.

30
00:03:12,985 --> 00:03:16,330
Valójában ez egy mini tök,
amely a megporzást követően kezd el fejlődni.

31
00:03:17,720 --> 00:03:21,220
A hímivarú virágok a hosszú szárak végén találhatóak.

32
00:03:23,940 --> 00:03:26,455
A virágok csak egy napra nyílnak ki. 

33
00:03:32,170 --> 00:03:34,825
A tökök saját magukat is megtermékenyíthetik,

34
00:03:35,285 --> 00:03:39,660
ha egy nőivarú virágot egy olyan 
hímivarú virág virágpora termékenyít meg,

35
00:03:39,855 --> 00:03:41,270
amely ugyanazon a növényen található.

36
00:03:46,760 --> 00:03:52,120
Viszont gyakrabban fordul elő keresztbeporzódás 
az adott fajta különböző egyedei között,

37
00:03:52,990 --> 00:03:54,855
illetve egy-egy fajon belül.

38
00:04:01,675 --> 00:04:05,765
Virágaikat rovarok, 
elsősorban méhek porozzák be. 

39
00:04:14,325 --> 00:04:20,175
Fajok közötti kereszteződés, 
úgynevezett interspecifikus hibridizáció,

40
00:04:20,310 --> 00:04:26,520
csak az ezüstmagvú tök (Cucurbita argyrosperma) 
és a pézsmatök (Cucurbita moschata) között lehetséges.

41
00:04:28,630 --> 00:04:32,405
A vad úritök (Cucurbita pepo) 
esetében csak ritkán fordul elő. 

42
00:04:35,915 --> 00:04:40,405
Ezért általában lehet két különböző 
tökfajt egymás mellett termeszteni

43
00:04:40,830 --> 00:04:43,380
a keresztbeporzódás veszélye nélkül. 

44
00:04:44,260 --> 00:04:45,675
Viszont fontos, hogy

45
00:04:45,855 --> 00:04:51,745
egy adott faj különböző fajtái között 
tartsunk 1 km-es izolációs távolságot. 

46
00:04:52,825 --> 00:04:58,030
Ezt 500 méterre csökkenthetjük, ha a fajták
között van valamilyen természetes akadály, például sövény. 

47
00:05:03,530 --> 00:05:07,500
Számos módszer létezik arra, hogy különböző
tökfajtákról gyűjthessünk magot

48
00:05:07,580 --> 00:05:08,900
ugyanabban a kertben. 

49
00:05:12,635 --> 00:05:15,765
Egy fajtát szúnyoghálóval 
takarhatunk le,

50
00:05:16,145 --> 00:05:18,875
és egy poszméhkaptárat helyezünk el alatta.

51
00:05:25,225 --> 00:05:27,915
Ez viszont nehézségekbe 
ütközhet a kúszó fajták esetében,

52
00:05:28,145 --> 00:05:30,570
mivel azok gyorsan kitöltik a rendelkezésükre álló teret,

53
00:05:31,325 --> 00:05:34,060
ezzel megakadályozva a 
rovarok szabad mozgását. 

54
00:05:38,095 --> 00:05:42,805
Letakarhatunk két fajtát
is két külön rovarhálóval,

55
00:05:43,840 --> 00:05:47,405
és naponta váltogatjuk, hogy
éppen melyik van nyitva,

56
00:05:48,050 --> 00:05:49,975
és melyik van zárva.

57
00:05:50,865 --> 00:05:52,955
Így hagyjuk, hogy a vadon élő rovarok elvégezzék a munkájukat. 

58
00:06:06,385 --> 00:06:09,400
A virágokat akár kézzel 
is beporozhatjuk.

59
00:06:11,780 --> 00:06:16,055
Ez viszonylag könnyű, mivel nagyok, 
és könnyen észrevehetőek.

60
00:06:19,870 --> 00:06:22,735
Erre három lehetőség áll rendelkezésre,
amelyekkel kapcsolatban

61
00:06:22,950 --> 00:06:25,875
bővebb információt a 
mechanikai izolációs technikákról

62
00:06:26,070 --> 00:06:30,290
és a kézi megporzásról szóló modulokban
találsz "A magtermesztés ábécéje" fejezetben.

63
00:06:40,580 --> 00:06:41,580
Életciklus 

64
00:07:06,860 --> 00:07:12,430
A magfogás céljából vetett tököket ugyanúgy 
termesztjük, mint a fogyasztási célból nevelteket. 

65
00:07:29,810 --> 00:07:33,125
A genetikai sokféleség biztosítása érdekében 
minimum 6 növényről kell magot fogni.

66
00:07:36,310 --> 00:07:38,145
Ideális esetben legalább egy tucatról. 

67
00:08:09,330 --> 00:08:12,255
A magfogásra szánt egyedeket 
a fajtára jellemző

68
00:08:12,325 --> 00:08:15,550
egyedi tulajdonságok alapján 
válasszuk ki,

69
00:08:17,700 --> 00:08:20,455
mint például, hogy futó vagy bokros-e. 

70
00:08:22,560 --> 00:08:28,785
Ellenőrizzük a termés alakját, méretét,
illetve a hús ízét és textúráját is,

71
00:08:29,560 --> 00:08:32,375
a jó tárolhatóság és a betegségekkel szembeni 

72
00:08:32,640 --> 00:08:35,460
ellenálló képesség megfigyelése mellett! 

73
00:08:36,110 --> 00:08:40,985
A magfogásra szánt tökök termése ugyanakkor 
válik éretté, mint az étkezési célból termesztett társaiké,

74
00:08:41,465 --> 00:08:44,255
amelyeket általában teljesen 
érett állapotban fogyasztanak.

75
00:08:45,050 --> 00:08:49,295
Ez alól kivételt képez az 
úritök (Cucurbita pepo) néhány fajtája

76
00:08:49,675 --> 00:08:54,510
például a patisszon és a cukkini,
ahol a terméseket éretlen állapotban fogyasztjuk. 

77
00:08:56,215 --> 00:09:00,980
A cukkinit, a tökhöz hasonlóan hagyjuk 
 addig érni, amíg megváltozik a színe,

78
00:09:01,150 --> 00:09:05,570
eléri a teljes méretét, vagyis 
a kocsánya száraz és kemény lesz,

79
00:09:06,170 --> 00:09:08,855
és a héja megkeményedik. 

80
00:09:13,875 --> 00:09:15,395
Ekkor takarítsuk be,

81
00:09:16,060 --> 00:09:21,445
majd meleg helyen hagyjuk
minimum egy hónapig továbbérni.

82
00:09:24,640 --> 00:09:28,270
Ezzel a technikával növelhető 
a magok termékenysége. 

83
00:09:33,780 --> 00:09:36,515
Magkinyerés – válogatás – tárolás

84
00:09:42,100 --> 00:09:44,580
A magok kinyeréséhez nyissuk fel a tököt,

85
00:09:47,530 --> 00:09:50,125
majd egy kanál segítségével távolítsuk el a magokat.

86
00:09:50,375 --> 00:09:52,435
Vigyázzunk, hogy ne szedjünk ki túl sok húst.

87
00:09:57,140 --> 00:09:58,660
A magokat öblítsük át folyóvíz alatt.

88
00:10:07,350 --> 00:10:11,765
Bizonyos fajtáknál nehéz 
eltávolítani a húst a magokról.

89
00:10:12,265 --> 00:10:16,430
Ilyenkor áztassuk a magokat és
a húst vízbe szobahőmérsékleten,

90
00:10:16,900 --> 00:10:19,130
és a magokat másnap távolítsuk el. 

91
00:10:30,405 --> 00:10:36,795
Végül a magokat szárítsuk meg
22-25°C között, szellős helyen. 

92
00:10:39,675 --> 00:10:42,455
Akkor száradtak meg kellőképpen,

93
00:10:42,660 --> 00:10:44,975
ha kettétörnek, amikor meghajlítjuk őket. 

94
00:11:02,500 --> 00:11:05,215
Mindig írjuk fel egy címkére
a faj és a fajta nevét,

95
00:11:05,415 --> 00:11:10,215
valamint a magfogás évét, majd helyezzük azt a tasak
belsejébe! A külső felirat könnyen letörlődhet. 

96
00:11:16,320 --> 00:11:21,910
Néhány fagyasztóban töltött nap alatt 
a kártevők lárvái elpusztulnak. 

97
00:11:27,240 --> 00:11:32,485
A tök és a cukkini magjai átlagosan 6 évig
őrzik meg a csírázóképességüket,

98
00:11:32,975 --> 00:11:34,930
de akár 10 évig is kicsírázhatnak.

99
00:11:38,510 --> 00:11:42,035
Ezt úgy tudjuk meghosszabbítani,
ha a magokat fagyasztóban tároljuk. 
