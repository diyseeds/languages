1
00:00:09,565 --> 00:00:14,960
A sárgarépa az ernyősök (Apiaceae) családjába 
tartozik, latin neve Daucus carota,

2
00:00:15,565 --> 00:00:17,510
és a gyökeréért termesztjük.

3
00:00:21,600 --> 00:00:23,545
Két alfaja létezik:

4
00:00:25,410 --> 00:00:30,860
a nyugati sárgarépa, a carota sativus, 
amely általában kétéves, illetve 

5
00:00:35,530 --> 00:00:42,060
a keleti sárgarépa, carota atrorubens, 
ami rendszerint egyéves. 

6
00:00:48,315 --> 00:00:56,250
Bizonyos répafajtákat korai fogyasztásra, 
míg másokat téli tárolásra termesztünk.

7
00:00:58,440 --> 00:01:06,025
A gyökér színe a fehértől a feketéig terjedhet, 
lehet sárga, narancssárga, vörös vagy lila.

8
00:01:06,455 --> 00:01:09,220
Az alakjuk is rendkívül változatos. 

9
00:01:15,015 --> 00:01:15,985
Megporzás 

10
00:01:26,685 --> 00:01:32,645
A répa virágzata kisebb, 
általában kétivarú virágokból

11
00:01:33,055 --> 00:01:35,270
álló ernyővirágzat.

12
00:01:36,875 --> 00:01:39,940
A porzó (hímivarú szaporítószerv) a

13
00:01:41,945 --> 00:01:43,850
termő (nőivarú szaporítószerv)

14
00:01:44,260 --> 00:01:45,960
előtt érik, így egy virágon

15
00:01:47,885 --> 00:01:51,700
belül nem történhet öntermékenyítés.

16
00:01:53,955 --> 00:01:57,210
Ennek ellenére mivel a virágok nem 
egyszerre nyílnak,

17
00:01:59,935 --> 00:02:03,585
az öntermékenyülés megvalósulhat 
egy ernyővirágzaton belül,

18
00:02:04,165 --> 00:02:07,225
vagy ugyanazon növény két különböző ernyője között. 

19
00:02:08,660 --> 00:02:13,290
Megtermékenyítés különböző növények 
ernyői között is előfordul. 

20
00:02:14,750 --> 00:02:20,510
Így a sárgarépa idegentermékenyülő növény, 
melynek megporzását rovarok végzik.

21
00:02:22,660 --> 00:02:26,760
A különböző fajták 
összekereszteződhetnek egymással. 

22
00:02:44,090 --> 00:02:47,225
Ráadásul a sárgarépa a gyakori vadmurokkal is

23
00:02:47,560 --> 00:02:52,905
kereszteződhet. Ilyenkor a vadmurok génjei 
lesznek a dominánsak, mint a vad fajokkal

24
00:02:53,305 --> 00:02:56,115
történő kereszteződés esetén általában. 

25
00:02:57,395 --> 00:03:00,600
A vadmurok könnyen felismerhető a

26
00:03:00,985 --> 00:03:05,620
virágzata közepén levő kis fekete 
virágnak köszönhetően. 

27
00:03:06,860 --> 00:03:08,770
A keresztbeporzódás elkerülése

28
00:03:09,150 --> 00:03:13,650
érdekében a répafajták között tartsunk 
1 kilométeres távolságot. 

29
00:03:16,420 --> 00:03:19,840
Ezt a távolságot 500 méterre csökkenthetjük,

30
00:03:20,330 --> 00:03:25,635
ha a fajták között van valamilyen természetes 
akadály, például sövény. 

31
00:03:28,225 --> 00:03:30,365
A fajtákat úgy is izolálhatjuk, ha zárt

32
00:03:30,585 --> 00:03:34,425
rovarháló alá kis kaptárakat 
teszünk rovarokkal együtt,

33
00:03:40,790 --> 00:03:46,065
vagy ha az egyes fajtákat takaró rovarhálókat 
felváltva nyitjuk és zárjuk.

34
00:03:47,330 --> 00:03:48,560
Bővebb információt erről

35
00:03:48,855 --> 00:03:54,480
az izolációs technikákról szóló modulban találsz 
A magtermesztés ábécéje fejezetben. 

36
00:04:01,930 --> 00:04:04,060
A répa életciklusa 

37
00:04:12,695 --> 00:04:17,455
A keleti sárgarépa különböző fajtái 
általában egyévesek,

38
00:04:17,535 --> 00:04:20,830
ha hosszúnappalos időszakban 
termesztik őket.

39
00:04:22,870 --> 00:04:25,485
Magot az első évben hoznak. 

40
00:04:31,215 --> 00:04:36,755
Ezzel szemben a nyugati sárgarépák esetében 
két évre van szükség a magfogáshoz.

41
00:04:37,090 --> 00:04:41,830
Az első évben fejlődik ki a gyökérzet és levelek. 

42
00:04:43,910 --> 00:04:47,840
Szükségük van a téli hideghatásra ahhoz,

43
00:04:48,400 --> 00:04:52,030
hogy virágot és 
magot hozzanak a második évben. 

44
00:04:54,405 --> 00:04:58,890
A rövid tenyészidejű fajtákat a lehető 
legkésőbb vetik a vegetációs idő során

45
00:04:59,365 --> 00:05:03,585
nehogy túlérettek legyenek a 
téli tároláshoz. Ha nem így cselekszünk,

46
00:05:04,055 --> 00:05:08,810
akkor a következő év tavaszán problémák 
adódhatnak a növények fejlődésével. 

47
00:05:39,670 --> 00:05:41,085
A különböző régiókban eltérő

48
00:05:41,255 --> 00:05:46,080
módszereket használnak a 
magfogásra szánt répák téli tárolására.

49
00:05:46,835 --> 00:05:50,435
Természetesen a legegyszerűbb az, ha 
a földben hagyjuk őket a kertben,

50
00:05:50,555 --> 00:05:51,975
ha ezt az éghajlat lehetővé teszi.

51
00:05:55,140 --> 00:06:00,120
Néha egy réteg szalma is elég ahhoz, hogy 
megvédjük őket az enyhébb fagyoktól. 

52
00:06:02,020 --> 00:06:06,820
A hidegebb éghajlatú területeken, ahol 
fokozott fagyveszély áll fenn,

53
00:06:07,540 --> 00:06:12,410
a növényeket még a téli hideg beállta előtt ki kell ásni, 
és fagymentes helyre kell betárolni. 

54
00:06:14,720 --> 00:06:18,040
Azokat az egyedeket, amik az első évben virágot hoznak,

55
00:06:18,485 --> 00:06:20,980
távolítsuk el, mert a magvaikból fejlődő

56
00:06:21,150 --> 00:06:24,645
növények minden betakarítás 
után hamarabb fognak virágozni!

57
00:06:29,305 --> 00:06:33,340
Azokat a répákat se tartsuk meg, 
melyeknek zöld a gyökérnyaka,

58
00:06:33,795 --> 00:06:36,355
megrepedtek, vagy elágaznak a gyökereik! 

59
00:06:39,810 --> 00:06:45,390
A gyökereket szárazon tisztítsuk meg, 
a leveleket a gyökérnyak felett vágjuk vissza,

60
00:06:46,855 --> 00:06:50,575
majd a répákat rövid ideig hagyjuk 
szabad levegőn száradni. 

61
00:07:20,175 --> 00:07:24,940
A répákat az adott fajta tulajdonságai 
alapján válogassuk ki, mint pl.

62
00:07:25,715 --> 00:07:31,155
a szín, a forma, a növekedési erély és a tárolhatóság.

63
00:07:38,910 --> 00:07:42,655
Továbbá íz alapján is 
fontos szelektálni,

64
00:07:43,080 --> 00:07:46,960
mivel egy adott fajtán belül is eltérő lehet 
az egyes répák íze. 

65
00:07:51,340 --> 00:07:54,845
A kóstoláshoz elegendő levágni a répa csúcsát.

66
00:07:55,865 --> 00:07:59,175
Ezt követően a gyökereket fahamuval fertőtlenítsük. 

67
00:08:05,180 --> 00:08:08,375
Végül a kiválasztott gyökereket
 tegyük fagyvédett helyen

68
00:08:08,505 --> 00:08:13,240
homokkal töltött ládába, 
vagy állítsuk őket függőlegesen faládákba.

69
00:08:19,295 --> 00:08:26,230
Az ideális tárolási hőmérséklet 1°C,
 90-95%-os páratartalom mellett. 

70
00:08:35,010 --> 00:08:38,450
Tél folyamán rendszeresen 
ellenőrizzük a gyökereket,

71
00:08:38,760 --> 00:08:40,985
és távolítsuk el azokat, amelyek elkezdenének rothadni! 

72
00:09:10,735 --> 00:09:14,115
A gyökereket az erősebb fagyok 
veszélyének elmúltával

73
00:09:14,500 --> 00:09:17,450
tavasz elején visszaültetjük a földbe.

74
00:09:17,800 --> 00:09:22,555
Gondoskodjunk arról, hogy a visszaültetett 
gyökerek ne száradjanak ki.

75
00:09:23,230 --> 00:09:28,355
Fokozatosan szoktassuk őket a fényhez,
 illetve védjük az erős napsütéstől is. 

76
00:09:31,055 --> 00:09:37,650
Bizonyos területeken a termesztett répák 
és a vadmurok egy időben virágzik.

77
00:09:37,920 --> 00:09:42,165
A keresztbeporzódás elkerülése érdekében 
el kell tolni egymástól a virágzási időket.

78
00:09:42,510 --> 00:09:43,635
Ennek fényében a

79
00:09:43,735 --> 00:09:48,735
magfogásra szánt répagyökereket tél végén ültessük 
edényekbe egy jól megvilágított,

80
00:09:49,115 --> 00:09:54,770
fagymentes helyen kialakított hidegágyba, 
majd pedig amint lehetséges

81
00:10:15,080 --> 00:10:18,295
ültessük ki őket a szabadföldbe! 

82
00:10:18,920 --> 00:10:24,660
A genetikai sokféleség biztosításához minimum 
30 növényről kell magot fogni.

83
00:10:25,785 --> 00:10:29,825
Ideális esetben 50-100 növényt válasszunk ki e célra!

84
00:10:51,830 --> 00:10:55,015
A növényeket karózzuk ki 
növekedésük során! 

85
00:10:59,080 --> 00:11:03,930
A sárgarépa több ernyőt is növeszt, 
amelyek azonban nem egyszerre nyílnak.

86
00:11:04,750 --> 00:11:09,905
Az először megjelenő elsődleges 
ernyő a központi szár csúcsán található. 

87
00:11:11,400 --> 00:11:15,575
A másodlagos ernyők a 
központi szárból nőnek ki. 

88
00:11:21,715 --> 00:11:25,270
A harmadlagos ernyők a másodlagos szárakon jönnek létre. 

89
00:11:30,535 --> 00:11:34,190
Mivel az ernyők érése 
elhúzódik, több menetben

90
00:11:34,475 --> 00:11:36,700
kell betakarítani őket. 

91
00:11:39,775 --> 00:11:45,370
Érdemes az elsődleges ernyőket betakarítani, 
mert ezekben vannak a legjobb minőségű magok.

92
00:11:45,665 --> 00:11:49,520
A másodlagos ernyőket csak 
szükség esetén gyűjtsük be. 

93
00:11:52,505 --> 00:11:56,030
Az ernyőket a szár felső részével együtt vágjuk le,

94
00:11:56,315 --> 00:11:59,055
amikor az első érett magok elkezdenek peregni!

95
00:11:59,465 --> 00:12:04,890
Mivel gyakran a talajra esnek, rossz időjárás 
esetén érdemes kicsit hamarabb levágni őket. 

96
00:12:20,280 --> 00:12:25,980
A hidegebb területeken a növényeket 
gyökerestől is kiáshatjuk szeptember folyamán. 

97
00:12:40,025 --> 00:12:45,205
A szárítást mindenképp száraz, 
szellős helyen kell folytatni. 

98
00:12:47,545 --> 00:12:51,990
A magok lassú érése folytatódik 
a száradás során is. 

99
00:12:58,655 --> 00:13:01,515
Magfogás – tisztítás - tárolás :

100
00:13:07,415 --> 00:13:10,395
A magokat kézzel távolítsuk el az ernyőkből.

101
00:13:10,775 --> 00:13:16,095
Közben érdemes kesztyűt viselni, 
mert a magokon tüskék találhatóak.

102
00:13:16,640 --> 00:13:20,600
Ezektől megszabadulhatunk, ha a 
magokat egy szitához dörzsöljük. 

103
00:13:24,780 --> 00:13:29,645
Először nagylyukú rostán engedjük át a 
magokat, amelyen a törmelékek fennmaradnak,

104
00:13:35,710 --> 00:13:41,260
majd egy finomabb szitán, amelyen
 a magok maradnak fenn. 

105
00:13:58,120 --> 00:14:04,290
Végül kiszeleljük a magokat, vagy 
kézzel távolítjuk el a maradék növényi részeket. 

106
00:14:15,700 --> 00:14:19,960
Mindig írjuk fel egy címkére a fajta és 
a faj nevét, valamint

107
00:14:20,260 --> 00:14:25,075
a magfogás évét, majd helyezzük a tasak belsejébe! 
A külső felirat könnyen letörlődhet. 

108
00:14:29,145 --> 00:14:34,445
Néhány fagyasztóban töltött nap 
alatt a kártevők lárvái elpusztulnak. 

109
00:14:40,065 --> 00:14:44,095
A répamagok akár 5 évig is képesek 
megőrizni a csírázóképességüket.

110
00:14:44,530 --> 00:14:48,475
Ez akár 10 évig is kitolható,

111
00:14:49,560 --> 00:14:53,680
ha a magokat fagyasztóban tároljuk.

112
00:14:55,555 --> 00:15:01,495
A répamagok időnként nyugalmi (alvó) állapotban 
maradnak a betakarítást követő 3 hónapban.

113
00:15:04,660 --> 00:15:11,810
Egy gramm nem koptatott magban 
körülbelül 700-800 szem található. 
