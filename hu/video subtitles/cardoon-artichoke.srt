1
00:00:08,475 --> 00:00:11,680
A kárdi a fészkesek (Asteraceae) családjának tagja,

2
00:00:12,015 --> 00:00:17,865
latin neve Cynara cardunculus, és a 
húsos hajtásáért termesztjük.

3
00:00:25,460 --> 00:00:31,585
Szintén a Cynara nemzetség (és a cardunculus faj) tagja 
a scolymus alfajba tartozó articsóka, amelynek a

4
00:00:32,190 --> 00:00:33,955
virágbimbóját fogyasztják.

5
00:00:38,095 --> 00:00:41,740
Magfogás céljából ugyanúgy 
kell termeszteni, mint a kárdit. 

6
00:00:53,500 --> 00:00:54,475
Megporzás 

7
00:01:10,100 --> 00:01:11,495
Az articsókának és a

8
00:01:11,870 --> 00:01:17,670
kárdinak is különálló csöves virágai vannak, 
amik kékeslila fészekvirágzatba tömörülnek.

9
00:01:19,585 --> 00:01:22,640
A virágok kétivarúak, de önmeddőek.

10
00:01:24,740 --> 00:01:28,340
Minden virágot egy másiknak kell beporoznia

11
00:01:28,630 --> 00:01:32,485
ugyanabból vagy egy 
másik fészekvirágzatból:

12
00:01:36,375 --> 00:01:39,705
a kárdi és az articsóka 
idegentermékenyülő növény.

13
00:01:40,575 --> 00:01:43,170
Beporzásukhoz szükség van a rovarokra. 

14
00:01:45,335 --> 00:01:49,160
A kárdi és az articsóka, illetve 
egy kerten belül termesztett különböző

15
00:01:49,470 --> 00:01:53,350
fajtáik összekereszteződhetnek egymással. 

16
00:01:57,685 --> 00:02:03,190
A keresztbeporzódás elkerüléséhez a fajták 
között tartsunk 1 kilométeres izolációs távolságot! 

17
00:02:05,735 --> 00:02:08,785
Ezt a távolságot 500 méterre csökkenthetjük,

18
00:02:09,020 --> 00:02:12,370
ha a fajták között van valamilyen 
természetes akadály, pl. sövény. 

19
00:02:15,790 --> 00:02:20,465
A fajtákat úgy is izolálhatjuk, ha zárt 
rovarháló alá kis kaptárakat teszünk

20
00:02:20,555 --> 00:02:22,740
rovarokkal együtt, vagy ha az egyes

21
00:02:23,375 --> 00:02:28,195
fajtákat takaró rovarhálókat 
felváltva nyitjuk és zárjuk.

22
00:02:29,485 --> 00:02:30,575
Bővebb információt ezzel

23
00:02:30,815 --> 00:02:35,950
kapcsolatban az izolációs technikákról szóló 
modulban találsz "A magtermesztés ábécéje" fejezetben. 

24
00:02:40,545 --> 00:02:41,625
Életciklus 

25
00:02:47,725 --> 00:02:49,445
Az életciklus első évében a

26
00:02:49,895 --> 00:02:55,085
magfogás céljából vetett egyedeket ugyanúgy 
termesztjük, mint a fogyasztási célból nevelteket.

27
00:02:59,160 --> 00:03:01,490
Magot a második évben hoznak. 

28
00:03:12,295 --> 00:03:18,690
Hidegebb éghajlatú területeken a kárdi 
magjait február közepén vetik cserépbe,

29
00:03:19,090 --> 00:03:25,025
míg a melegebb vidékeken 
április végén/május elején vetik szabadföldbe. 

30
00:03:28,405 --> 00:03:30,745
A genetikai sokféleség biztosításához

31
00:03:31,235 --> 00:03:34,615
legalább 12 növényt kell 
kiválasztani magfogáshoz az adott fajtával

32
00:03:36,165 --> 00:03:40,260
szemben támasztott kiválasztási kritériumok alapján. 

33
00:03:43,960 --> 00:03:49,120
Válasszuk azokat a szárakat, amik a legerőteljesebbek, 
ellenállnak a hidegnek és a rothadásnak,

34
00:03:51,610 --> 00:03:55,540
illetve amiknek szép leveleik 
és húsos, de nem szálkás bordái vannak,

35
00:03:56,845 --> 00:03:59,045
tüskékkel vagy anélkül! 

36
00:04:09,155 --> 00:04:13,480
Az articsóka esetében a virágbimbók száma, 
íze és szabályos

37
00:04:14,025 --> 00:04:16,520
növekedése alapján válogatunk. 

38
00:04:19,720 --> 00:04:23,475
Azokat a kárditöveket, amik az 
első évben virágot hoznak, távolítsuk el! 

39
00:04:30,375 --> 00:04:33,415
Ősszel fogyasztási célra 
betakaríthatjuk a leveleket. 

40
00:04:45,815 --> 00:04:48,270
Ha a tél rendkívül hideg a gyökereket

41
00:04:48,585 --> 00:04:53,345
még a fagyok előtt ássuk ki, és 
fagymentes helyen tároljuk. 

42
00:05:00,165 --> 00:05:02,235
Enyhébb éghajlatú vidékeken a kárdi,

43
00:05:02,670 --> 00:05:07,340
illetve az articsóka gyökerei 
egész télen a földben maradhatnak. 

44
00:05:12,840 --> 00:05:15,955
Tavasszal a magfogás céljából a pincében

45
00:05:16,075 --> 00:05:19,250
átteleltetett növényeket 
visszaültetjük a földbe. 

46
00:06:18,365 --> 00:06:22,125
A magfogásra szánt növények 
nyár végén hoznak virágot. 

47
00:06:38,810 --> 00:06:40,790
A magokat ősszel kell begyűjteni. 

48
00:06:47,435 --> 00:06:48,785
A magfogáshoz a

49
00:06:48,950 --> 00:06:54,870
fészekvirágzatot akkor vágjuk le, amikor 
kis fehér tollszerű pihék jelennek meg a csúcsain.

50
00:07:02,430 --> 00:07:07,440
A fészekvirágzat utóéréséhez biztosítsunk 
egy száraz, jól szellőző helyet! 

51
00:07:11,725 --> 00:07:15,040
Magfogás - tisztítás - tárolás

52
00:07:18,830 --> 00:07:22,795
Ha a fészekvirágzatok megszáradtak, 
a pihéket kézzel távolítsuk el.

53
00:07:24,020 --> 00:07:27,060
Legyen rajtunk kesztyű a tüskék miatt!

54
00:07:27,860 --> 00:07:31,010
Ezután a fészekvirágzatokat dörzsöljük össze 
a magok eltávolításához!

55
00:07:41,120 --> 00:07:47,455
Zsákba is tehetjük, majd nem túl kemény talajon bottal 
vagy műanyag kalapáccsal kicsépelhetjük őket.

56
00:07:55,100 --> 00:07:58,260
Először kézzel távolítsuk el a nagyobb törmeléket,

57
00:07:58,345 --> 00:08:02,370
majd óvatosan fújjunk a magokra, hogy a 
maradéktól is megszabaduljunk,

58
00:08:02,955 --> 00:08:05,240
és tiszta magokat kapjunk. 

59
00:08:15,790 --> 00:08:19,215
Végül öntsük a magokat egy műanyag zacskóba!

60
00:08:20,090 --> 00:08:24,300
Írjuk fel egy címkére a fajta és a faj 
nevét, a magfogás évét,

61
00:08:24,465 --> 00:08:25,910
majd helyezzük a tasak belsejébe! 

62
00:08:33,510 --> 00:08:38,090
Néhány fagyasztóban töltött nap alatt 
a kártevők lárvái elpusztulnak. 

63
00:08:41,260 --> 00:08:45,025
A kárdi magjai átlagosan 7 évig 
őrzik meg a csírázóképességüket.

64
00:08:47,370 --> 00:08:52,045
Ezt úgy tudjuk meghosszabbítani, ha 
a magokat fagyasztóban tároljuk. 

65
00:08:54,025 --> 00:08:58,920
Egy gramm körülbelül 25 darab magot tartalmaz. 
