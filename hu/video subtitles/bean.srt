﻿1
00:00:09,640 --> 00:00:12,705
A babok egyéves növények a pillangósvirágúak (Fabaceae) családjából.

2
00:00:19,320 --> 00:00:21,150
Számos babfaj létezik,

3
00:00:21,270 --> 00:00:27,130
a legelterjedtebb a Phaseolus vulgaris 
és a Phaseolus coccineus.

4
00:00:41,570 --> 00:00:45,180
A Phaseolus vulgaris fajhoz különböző típusok

5
00:00:45,690 --> 00:00:52,740
tartoznak: bokorbabok, 
ostoros babok, és karósbabok.

6
00:00:55,315 --> 00:01:00,180
Ide olyan babok tartoznak, 
amelyeket vagy a hüvelyükért (például zöldbab)

7
00:01:00,480 --> 00:01:06,100
vagy pedig a frissen vagy szárítva 
fogyasztott magjukért termesztünk. 

8
00:01:13,455 --> 00:01:18,100
Vannak egyéb fajok is, 
például a Phaseolus coccineus,

9
00:01:18,655 --> 00:01:22,180
más néven tűzbab, amelyek mind karósbabok.

10
00:01:23,430 --> 00:01:26,220
Nagyon szép piros vagy fehér virágaik vannak,

11
00:01:26,315 --> 00:01:31,500
a termésképzés idején pedig 
25°C alatti hőmérsékletet igényelnek. 

12
00:01:38,980 --> 00:01:44,025
A két fajt a hüvelyeik alapján 
lehet megkülönböztetni egymástól.

13
00:01:45,095 --> 00:01:48,315
A Phaseolus coccineus hüvelyei vastagabbak,

14
00:01:48,750 --> 00:01:52,995
a Phaseolus vulgaris hüvelyei 
pedig vékonyabbak, és puhábbak. 

15
00:02:04,125 --> 00:02:05,120
Megporzás 

16
00:02:21,325 --> 00:02:27,825
A Phaseolus vulgaris fajnak kétivarú önbeporzó 
virági vannak, vagyis a hímivarú és nőivarú

17
00:02:28,295 --> 00:02:32,790
szaporítószervek ugyanabban 
a virágban találhatóak.

18
00:02:33,125 --> 00:02:34,515
A virágok öntermékenyek. 

19
00:02:36,060 --> 00:02:38,750
Ennek ellenére előfordulhat 
rovarok általi keresztbeporzás

20
00:02:38,820 --> 00:02:40,530
a különböző fajták között.

21
00:02:41,475 --> 00:02:45,085
Ennek valószínűsége 
az adott fajtától függ. 

22
00:02:48,045 --> 00:02:49,390
A kockázat csökkentése

23
00:02:49,820 --> 00:02:53,575
érdekében a bokorbabfajták 
közt minimum 5-10 méter

24
00:02:53,875 --> 00:02:56,420
távolságot érdemes tartani.

25
00:02:59,075 --> 00:03:03,695
A karósbabfajták között tartsunk 
legalább 50 méter izolációs távolságot! 

26
00:03:05,315 --> 00:03:11,015
A bokorbabok és karósbabok között 
pedig minimum 10 métert hagyjunk ki! 

27
00:03:17,745 --> 00:03:22,305
Ha kicsi a kertünk, akkor a fajták 
genetikai tisztaságának

28
00:03:22,590 --> 00:03:27,285
megőrzéséhez minden Phaseolus vulgaris 
fajtát takarjunk le rovarhálóval!

29
00:03:33,635 --> 00:03:39,140
A rovarhálót már a virágzás kezdete előtt helyezzük el, 
hogy megelőzzük a keresztbeporzódást! 

30
00:03:44,900 --> 00:03:48,740
A Phaseolus coccineus fajnak 
kétivarú virágai vannak,

31
00:03:48,990 --> 00:03:52,680
vagyis a hímivarú és nőivarú szaporítószervek 
ugyanabban a virágban találhatóak.

32
00:03:59,865 --> 00:04:07,000
A piros virágú fajtáknál szükséges a rovarok, pl. 
méhek vagy poszméhek segítsége a megporzáshoz.

33
00:04:09,495 --> 00:04:14,225
A csontszínű virágú fajták 
képesek az önbeporzásra is. 

34
00:04:16,010 --> 00:04:20,995
A keresztbeporzás elkerüléséhez a 
Phaseolus coccineus két fajtája között,

35
00:04:21,720 --> 00:04:24,390
tartsunk 500 méteres izolációs távolságot.

36
00:04:25,640 --> 00:04:32,855
Ezt akár 150 méterre csökkenthetjük, ha valamilyen
 természetes akadály, pl. sövény van a fajták között. 

37
00:04:33,610 --> 00:04:40,415
Tartsunk 300 méteres távolságot a tűzbab és a 
karósbab típusba tartozó P. vulgaris fajták között! 

38
00:04:41,335 --> 00:04:47,165
A P. coccineus és a bokorbab típusú P. vulgaris 
között 50 méteres izolációs távolság tartandó. 

39
00:04:53,810 --> 00:04:54,840
Életciklus 

40
00:05:11,980 --> 00:05:16,255
A magfogás céljából vetett babokat ugyanúgy 
termesztjük, mint a fogyasztási célból nevelteket.

41
00:05:17,150 --> 00:05:19,440
A vetéshez meleg talajra van szükségük. 

42
00:06:01,920 --> 00:06:06,385
A zöldbabok esetében érdemes 
elválasztani egymástól

43
00:06:06,920 --> 00:06:09,635
a fogyasztásra és a magfogásra szánt növényeket.

44
00:06:15,030 --> 00:06:18,230
A fogyasztási célból történő 
betakarítás folyamatosan történik,

45
00:06:21,375 --> 00:06:26,235
míg magfogáshoz csak egyszer kell betakarítani, 
amikor már minden hüvely érett.

46
00:06:29,435 --> 00:06:33,210
Ha csak azokat a hüvelyeket tartjuk meg, 
amik a vegetációs idő végére érnek be,

47
00:06:33,910 --> 00:06:36,685
idővel egy későn érő fajtát hozunk létre. 

48
00:06:43,455 --> 00:06:46,890
A magok betakarításához 
várjuk meg, amíg a hüvelyek megszáradnak.

49
00:06:49,380 --> 00:06:53,060
Bizonyos fajtáknál, 
így a tűzbaboknál is,

50
00:06:53,640 --> 00:06:56,320
többszöri betakarításra lehet szükség. 

51
00:06:58,190 --> 00:07:03,235
Egyes bokorbabfajtáknál valamennyi
 hüvely egyszerre érik be,

52
00:07:04,340 --> 00:07:09,100
vagyis a vetőmagcélú betakarításhoz elegendő 
egy alkalom, amikor az összes növényt levágjuk. 

53
00:07:14,765 --> 00:07:19,800
Ha nyirkos az idő, és a magfogásra szánt 
növények nem teljesen szárazak,

54
00:07:20,390 --> 00:07:23,625
akkor tegyük őket egy jól szellőző pajtába száradni! 

55
00:07:29,245 --> 00:07:33,340
Fontos, hogy háló segítségével 
védjük őket a rovaroktól! 

56
00:07:36,140 --> 00:07:40,520
A betakarítás után hagyjuk a 
magokat 2-3 hétig száradni.

57
00:07:45,015 --> 00:07:48,075
Ezután harapjunk rá óvatosan egy magra!

58
00:07:49,120 --> 00:07:52,475
Ha nem látszik a harapás nyoma, akkor a mag elég száraz. 

59
00:08:00,070 --> 00:08:03,415
Magfogás - tisztítás - tárolás

60
00:08:14,930 --> 00:08:18,620
Kisebb mennyiségnél kézzel 
is kifejthetjük a magokat. 

61
00:08:21,480 --> 00:08:25,860
Nagyobb mennyiségnél a hüvelyeket 
bottal kicsépelhetjük vagy

62
00:08:26,915 --> 00:08:28,515
ki is taposhatjuk őket. 

63
00:08:43,585 --> 00:08:47,130
A cséplést követően 
át kell rostálni a babokat. 

64
00:08:47,485 --> 00:08:51,810
A rostán fennmaradnak a babok és a nagyobb 
törmelékek, amelyek könnyen eltávolíthatóak.

65
00:08:57,070 --> 00:09:04,550
A fennmaradt kisebb törmelékektől rostálással 
vagy szeleléssel szabadulhatunk meg. 

66
00:09:06,140 --> 00:09:08,895
A szelelést fújással, ventilátorral 
vagy egy kisméretű

67
00:09:09,305 --> 00:09:13,125
kompresszor segítségével végezhetjük el.

68
00:09:15,925 --> 00:09:21,520
Távolítsuk el a többitől különböző magokat! 
Ezek keresztbeporzódással jöttek létre.

69
00:09:24,410 --> 00:09:29,695
Szedjük ki a sérült, nem megfelelő formájú,
 és a zsizsikes magokat is! 

70
00:09:30,320 --> 00:09:36,280
A babzsizsik (Acanthocelides obtectus) egy olyan 
kisméretű rovar,

71
00:09:36,650 --> 00:09:40,795
amely a petéit a növényen található hüvely belsejébe rakja.

72
00:09:41,475 --> 00:09:46,330
Megszabadulhatunk tőlük, ha a magokat 
néhány napra a fagyasztóba tesszük. 

73
00:09:57,980 --> 00:10:01,960
Mindig írjuk fel egy címkére a faj 
és a fajta nevét,

74
00:10:02,140 --> 00:10:07,845
valamint a magfogás évét, majd helyezzük 
azt a tasak belsejébe!

75
00:10:08,720 --> 00:10:11,255
A külső felirat könnyen letörlődhet. 

76
00:10:13,325 --> 00:10:17,615
Néhány fagyasztóban töltött nap 
alatt valamennyi kártevő lárvája elpusztul. 

77
00:10:23,235 --> 00:10:26,955
A babok magjai 3 évig nagyon jól csíráznak.

78
00:10:27,225 --> 00:10:31,665
Ezt az időszakot meghosszabbíthatjuk, 
ha a magokat fagyasztóban tároljuk. 
