1
00:00:13,460 --> 00:00:16,836
A tarlórépa a káposztafélék 
(Brassicaceae) családjába,

2
00:00:17,396 --> 00:00:21,221
a Brassica rapa fajba, azon belül 
pedig a rapa alfajba tartozik.

3
00:00:24,167 --> 00:00:28,152
A Brassica rapa faj sok 
alfajt tartalmaz:

4
00:00:32,200 --> 00:00:37,780
pak choi, komatsuna vagy japán mustárspenót,

5
00:00:38,880 --> 00:00:41,150
rapini stb. 

6
00:00:47,112 --> 00:00:53,660
Valamennyi tarlórépafajta 
gyors növekedésű, alakjuk

7
00:00:54,520 --> 00:00:56,560
és színük pedig rendkívül változatos.

8
00:01:03,070 --> 00:01:04,781
A fajta függvényében

9
00:01:05,083 --> 00:01:10,000
a gyökerek vagy a talajfelszín felett nőnek, 
vagy kissé, esetleg félig a földbe süllyednek.

10
00:01:12,567 --> 00:01:17,280
Vannak korai és 
tárolásra szánt fajták is. 

11
00:01:22,094 --> 00:01:23,214
Megporzás 

12
00:01:30,509 --> 00:01:37,003
A Brassica rapa faj legtöbb fajtájának 
élénksárga kétivarú virágai vannak,

13
00:01:37,454 --> 00:01:39,680
melyek önmeddőek,

14
00:01:40,312 --> 00:01:43,236
vagyis egyes növények virágpora

15
00:01:43,469 --> 00:01:47,323
csak egy másik egyed virágait 
tudja megtermékenyíteni. 

16
00:01:53,585 --> 00:01:58,487
Ezért a növények idegentermékenyülők, 
a megporzásuk rovarok segítségével történik. 

17
00:02:05,229 --> 00:02:07,796
Ez azt jelenti, hogy minden tarlórépafajtát

18
00:02:07,900 --> 00:02:11,476
izolálni kell valamennyi 
kínai keltől,

19
00:02:11,960 --> 00:02:16,778
pak choi-tól, japán mustárspenóttól, 
rapinitől,

20
00:02:17,003 --> 00:02:20,072
és a Brassica rapa faj minden egyéb alfajától. 

21
00:02:23,520 --> 00:02:26,110
A fajta genetikai tisztaságának biztosítása érdekében

22
00:02:26,392 --> 00:02:31,069
a fajták között hagyjunk minimum 
1 kilométeres izolációs távolságot. 

23
00:02:33,840 --> 00:02:37,054
Ezt 500 méterre csökkenthetjük,

24
00:02:37,396 --> 00:02:41,614
ha két fajta között van valamilyen 
természetes akadály, például sövény. 

25
00:02:47,770 --> 00:02:50,181
A fajtákat úgy is izolálhatjuk egymástól,

26
00:02:50,414 --> 00:02:53,956
ha az egyes fajtákat takaró 
rovarhálókat felváltva nyitjuk és zárjuk,

27
00:02:55,752 --> 00:03:00,014
vagy ha zárt rovarháló alá kis 
kaptárakat teszünk rovarokkal együtt.

28
00:03:04,680 --> 00:03:05,701
Bővebb információt

29
00:03:05,883 --> 00:03:10,174
ezzel kapcsolatban az izolációs technikákról szóló 
modulban találsz "A magtermesztés ábécéje" fejezetben. 

30
00:03:17,607 --> 00:03:18,940
A tarlórépa életciklusa

31
00:03:35,316 --> 00:03:39,985
Van néhány nagyon korai tavaszi fajta, 
amelyet egyévesként termeszthetünk.

32
00:03:46,560 --> 00:03:49,861
De a legtöbb tarlórépafajta 
kétéves.

33
00:03:50,520 --> 00:03:51,803
A mérsékelt égövön

34
00:03:51,910 --> 00:03:55,127
közvetlenül a talajba vetjük július közepén,

35
00:03:55,250 --> 00:03:58,894
magot pedig majd a termesztés 
második évében hoz. 

36
00:04:10,283 --> 00:04:13,032
A magfogás céljából vetett tarlórépákat

37
00:04:13,163 --> 00:04:16,180
ugyanúgy termesztjük, 
mint a fogyasztási célból nevelteket. 

38
00:04:48,850 --> 00:04:53,540
Magokat azokról az egészséges egyedekről
fogjunk, amelyek növekedését

39
00:04:53,912 --> 00:04:56,749
végig figyelemmel tudtuk kísérni,
és megismertük minden tulajdonságukat,

40
00:04:56,860 --> 00:05:05,061
mint a növekedési erély és gyorsaság
vagy a betegségekkel szembeni ellenálló képesség. 

41
00:05:13,214 --> 00:05:15,540
Az őszi betakarítás alkalmával

42
00:05:15,876 --> 00:05:20,756
kb. 30 egészséges növényt kell kiválasztani, 
amelyek megfelelő alakúak, méretűek és színűek.

43
00:05:29,534 --> 00:05:31,980
Nem érdemes túl nagy gyökereket választani,

44
00:05:32,567 --> 00:05:35,716
mert azok könnyebben rothadásnak 
indulnak a tél folyamán. 

45
00:05:43,098 --> 00:05:47,083
Sem a gyökereket, sem a gyökérnyaknál 
található levélalapot nem szabad levágni. 

46
00:05:53,230 --> 00:05:55,463
A hideg téllel jellemezhető vidékeken

47
00:05:55,563 --> 00:06:01,789
a tarlórépát homokkal töltött ládákban tárolják,
lehetőleg földpadlós, száraz pincékben.

48
00:06:04,196 --> 00:06:08,830
A pince hőmérsékletének 0°C, illetve 
néhány Celsius-fok között kell lennie. 

49
00:06:17,825 --> 00:06:18,900
A tél folyamán

50
00:06:19,316 --> 00:06:23,700
rendszeresen ellenőrizzük a gyökereket, és 
távolítsuk azokat, amelyek rothadásnak indultak! 

51
00:06:32,280 --> 00:06:36,196
Egyes kemény húsú fajták 
ellenállnak a hidegnek,

52
00:06:36,400 --> 00:06:38,530
így a földben is áttelelnek.

53
00:06:41,745 --> 00:06:45,090
Az enyhébb éghajlatú, enyhe 
téllel jellemezhető vidékeken

54
00:06:45,505 --> 00:06:48,232
valamennyi tarlórépafajta a földben is áttelel.

55
00:06:49,265 --> 00:06:53,963
Az időszakos enyhe fagyok 
ellen fagyvédő takaróval

56
00:06:54,269 --> 00:06:56,523
vagy 10 cm vastag földtakarással védhetjük őket. 

57
00:06:59,636 --> 00:07:03,127
A gyökereket tavasszal
a betakarításkor válogatjuk ki.

58
00:07:11,956 --> 00:07:13,700
Vágjuk le a leveleket a gyökérnyak fölött,

59
00:07:22,940 --> 00:07:26,232
majd újra ültessük el úgy, hogy a gyökér 2/3-a a földbe kerüljön.

60
00:07:27,660 --> 00:07:29,570
Bőségesen öntözzük meg őket. 

61
00:07:37,367 --> 00:07:40,501
A tél során bent tárolt 
tarlórépák közül

62
00:07:40,920 --> 00:07:45,636
távolítsuk el a sérült gyökereket, és 
csak az egészségeseket ültessük vissza a földbe.

63
00:07:57,120 --> 00:07:59,229
Ezeket is öntözzük meg bőségesen. 

64
00:08:12,410 --> 00:08:19,430
Később minimum 1 méter magasra 
nőnek, ezért karózzuk ki a növényeket. 

65
00:08:46,981 --> 00:08:50,940
Magkinyerés – válogatás – tárolás

66
00:08:51,083 --> 00:08:55,054
Ezek ugyanúgy történnek, mint a 
Brassica oleraceae fajba tartozó káposzták esetén. 

67
00:09:01,876 --> 00:09:06,123
A magok akkor érettek, amikor a 
becőtermések bézs színűvé válnak.

68
00:09:07,810 --> 00:09:09,360
Érett állapotban

69
00:09:09,563 --> 00:09:14,494
becők könnyen felnyílnak, és 
szétszórják a bennük található magokat.

70
00:09:24,596 --> 00:09:28,305
Általában a szárak nem 
egyszerre érnek be.

71
00:09:29,880 --> 00:09:35,200
A magvesztés elkerülése érdekében a magszárakat
külön-külön kell betakarítani, amint azok beértek.

72
00:09:37,620 --> 00:09:42,810
A teljes növényt is felszedhetjük még 
azelőtt, hogy minden mag teljesen beérne. 

73
00:09:46,421 --> 00:09:48,000
Az érési folyamat befejezéséhez

74
00:09:48,240 --> 00:09:52,007
tegyük a növényeket 
egy száraz, jól szellőző,

75
00:09:52,087 --> 00:09:53,556
napfénytől védett helyre! 

76
00:10:05,985 --> 00:10:08,661
A tarlórépa magjai akkor állnak készen

77
00:10:08,916 --> 00:10:12,000
a magkinyerésre, amikor a becőtermések 
kézzel könnyen kinyithatóak. 

78
00:10:15,127 --> 00:10:16,458
A magok kinyeréséhez

79
00:10:16,567 --> 00:10:21,970
a becőterméseket műanyag fólián 
vagy vastag szöveten szétterítjük,

80
00:10:22,378 --> 00:10:25,112
majd megütögetjük, vagy a kezünk között összedörzsöljük őket.

81
00:10:28,240 --> 00:10:33,258
A terméseket akár zsákba is tehetjük, 
majd puha felülethez ütve kicsépelhetjük a magokat. 

82
00:10:35,105 --> 00:10:42,152
Nagyobb mennyiség esetén a magokat 
lábbal vagy valamilyen járművel taposhatjuk ki. 

83
00:10:52,360 --> 00:10:57,723
A nehezen felnyíló becőtermések 
valószínűleg éretlen magokat tartalmaznak,

84
00:10:57,898 --> 00:10:59,738
amelyek nem csíráznak jól. 

85
00:11:04,341 --> 00:11:10,334
A nemkívánatos növényi részek eltávolítása során
először egy olyan nagylyukú rostán engedjük át a magokat,

86
00:11:10,843 --> 00:11:12,320
amelyen az említett anyagok fennmaradnak,

87
00:11:17,243 --> 00:11:19,970
majd pedig egy olyan finomabb szitán,

88
00:11:20,276 --> 00:11:24,820
amin a magok maradnak fenn, 
a kisebb törmelékek viszont átesnek. 

89
00:11:28,520 --> 00:11:32,465
Végül fújva vagy pedig 
a szél segítségével

90
00:11:32,792 --> 00:11:36,938
ki kell szelelnünk a magokat, hogy 
a maradék növényi részeket is eltávolíthassuk. 

91
00:11:55,469 --> 00:11:59,469
Mindig írjuk fel egy címkére a faj 
és a fajta nevét, valamint a magfogás évét,

92
00:11:59,890 --> 00:12:05,498
majd helyezzük azt a tasak belsejébe! 
A külső felirat könnyen letörlődhet. 

93
00:12:09,800 --> 00:12:14,938
Néhány fagyasztóban töltött nap alatt 
valamennyi parazita lárvája elpusztul. 

94
00:12:18,770 --> 00:12:21,810
A tarlórépamagok min. 6 évig őrzik meg a csírázóképességüket.

95
00:12:28,196 --> 00:12:31,098
Ezt úgy tudjuk meghosszabbítani, 
ha a magokat fagyasztóban tároljuk. 
