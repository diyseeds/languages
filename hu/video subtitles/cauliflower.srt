﻿1
00:00:12,010 --> 00:00:15,660
A karfiol és a brokkoli a káposztafélék 
(Brassicaceae) családjába,

2
00:00:16,065 --> 00:00:18,985
azon belül a Brassica oleracea fajba tartoznak.

3
00:00:20,470 --> 00:00:25,500
A karfiol a botrytis var.botrytis alfaj,

4
00:00:25,745 --> 00:00:31,580
a brokkoli pedig a botrytis 
var. italica alfaj képviselője.

5
00:00:31,990 --> 00:00:35,950
A Brassica oleracea fajba tartozik 
a karalábé, a fejes káposzta,

6
00:00:36,080 --> 00:00:40,580
a kelbimbó (bimbós kel), a leveles kel 
és a kelkáposzta is. 

7
00:00:42,035 --> 00:00:47,780
A karfiol és a brokkoli egyéves növény, 
de a téli fajtáik kétévesek.

8
00:00:49,380 --> 00:00:55,340
A fejükért termesztik őket, ami nem más mint 
a jövőbeli virágaik kezdetleges, húsos csoportja.

9
00:00:57,225 --> 00:00:59,740
Léteznek korai és késői fajták is.

10
00:01:00,220 --> 00:01:02,960
A karfiol fajták lehetnek fehérek vagy lilák,

11
00:01:04,910 --> 00:01:07,380
míg a brokkoli fajták zöldek vagy lilák.

12
00:01:09,265 --> 00:01:12,515
A romanesco brokkoli sárgászöld. 

13
00:01:18,175 --> 00:01:22,820
Az összes. B. oleracea fajba tartozó 
káposzták megporzása 

14
00:01:38,840 --> 00:01:42,740
A Brassica oleracea faj virágai kétivarúak,

15
00:01:45,680 --> 00:01:49,815
vagyis vannak hímivarú és nőivarú 
szaporítószerveik is.

16
00:01:53,590 --> 00:01:56,100
Legtöbbjük önmeddő, vagyis az

17
00:01:56,730 --> 00:02:01,765
egyik növény virágai által termelt virágpor csak 
egy másik növényt képes megtermékenyíteni.

18
00:02:05,585 --> 00:02:07,800
Ezért a növények idegentermékenyülők.

19
00:02:08,720 --> 00:02:13,455
Az optimális megporzás érdekében 
ajánlott sok növényt ültetni. 

20
00:02:17,115 --> 00:02:20,130
A megporzást rovarok végzik.

21
00:02:23,555 --> 00:02:27,930
Az említett tulajdonságok biztosítják a 
nagy genetikai változatosságot.

22
00:02:34,945 --> 00:02:40,875
A Brassica oleracea faj valamennyi alfaja képes 
egymással kereszteződni, ezért nem szabad

23
00:02:42,865 --> 00:02:47,455
különböző típusú káposztákat egymáshoz közel 
termeszteni, ha magfogás a célunk! 

24
00:02:52,025 --> 00:02:53,460
A genetikai tisztaság

25
00:02:53,760 --> 00:02:57,700
biztosításához a 
Brassica oleracea faj fajtái között

26
00:02:58,065 --> 00:03:01,170
minimum 1 km-es izolációs távolságot hagyjunk! 

27
00:03:02,600 --> 00:03:05,380
Ezt 500 méterre csökkenthetjük, ha

28
00:03:05,650 --> 00:03:09,820
két fajta között van valamilyen 
természetes akadály, pl. sövény. 

29
00:03:13,190 --> 00:03:15,640
A fajtákat úgy is izolálhatjuk egymástól,

30
00:03:15,985 --> 00:03:20,740
ha zárt rovarháló alá kis kaptárakat teszünk
 rovarokkal együtt, vagy ha

31
00:03:25,985 --> 00:03:29,695
az egyes fajtákat takaró rovarhálókat 
felváltva nyitjuk és zárjuk.

32
00:03:34,115 --> 00:03:35,155
Bővebb információt erről

33
00:03:35,440 --> 00:03:39,600
az izolációs technikákról szóló modulban találsz 
"A magtermesztés ábécéje"fejezetben. 

34
00:03:48,350 --> 00:03:51,380
A karfiol és a brokkoli életciklusa 

35
00:04:09,935 --> 00:04:12,100
Az enyhébb éghajlatú vidékeken

36
00:04:12,235 --> 00:04:16,525
a brokkoli és a karfiol 
kétéves növényként termeszthető.

37
00:04:18,340 --> 00:04:22,260
Nyáron vetjük őket, a növények áttelelnek 
a talajban, majd a következő év

38
00:04:22,585 --> 00:04:26,410
tavaszán fejet növesztenek, 
illetve virágzanak.

39
00:04:27,065 --> 00:04:31,100
A második év nyarán lehet 
magot fogni a növényekről. 

40
00:04:34,795 --> 00:04:41,300
A Brassica oleracea fajon belül a karfiol és 
a brokkoli kivételnek számít abban a tekintetben,

41
00:04:41,940 --> 00:04:45,420
hogy egyetlen év leforgása 
alatt is képesek szaporodni.

42
00:04:45,995 --> 00:04:49,385
Az őszi magfogás esélyeinek növelés 
érdekében, a magokat

43
00:04:49,680 --> 00:04:53,710
a lehető leghamarabb, 
januárban vagy februárban

44
00:04:53,910 --> 00:04:55,780
vessük el védett, meleg helyen.

45
00:05:12,090 --> 00:05:14,020
Márciusban vagy április elején

46
00:05:14,360 --> 00:05:16,415
ültessük ki a növényeket szabadföldbe,

47
00:05:16,640 --> 00:05:20,495
és fagyvédő takaróval védjük őket 
a kései fagyoktól. 

48
00:05:46,675 --> 00:05:50,380
Magokat az egészséges és erőteljes 
növekedésű egyedekről

49
00:05:50,680 --> 00:05:53,915
fogjunk,amelyek fejlődését 
végig figyelemmel tudtuk kísérni!

50
00:05:54,275 --> 00:05:58,005
Így ellenőrizhetjük az adott fajta 
tulajdonságait, mint pl. a szabályos és

51
00:06:00,540 --> 00:06:02,640
erőteljes növekedést;

52
00:06:07,050 --> 00:06:10,300
a karfiol esetén a tömör fej növesztése,

53
00:06:10,675 --> 00:06:13,380
melyet sok levél fog körül,

54
00:06:17,670 --> 00:06:22,950
a brokkolinál pedig egyetlen fej vagy sok 
oldalhajtás kialakítása,

55
00:06:27,965 --> 00:06:34,020
a virágzást megelőzően hosszú ideig 
tartó bimbózás, és a betegségekkel szembeni ellenállóság. 

56
00:06:35,655 --> 00:06:41,135
A genetikai sokféleség biztosítása érdekében minimum 
15 növényt válasszunk ki magfogásra. 

57
00:06:43,395 --> 00:06:47,830
A fej kialakulását követően a karfiol érzékennyé 
válhat a magas páratartalomra.

58
00:06:47,975 --> 00:06:50,710
Akár egy kis tetővel is védhetjük a 
növényeket az esőtől.

59
00:06:52,630 --> 00:06:57,095
Ha a fej bizonyos részei elkezdenek 
rothadni, késsel ki kell vágni azokat.

60
00:07:06,120 --> 00:07:09,615
A karfiol oldalirányban nem 
növeszt virágszárakat,

61
00:07:11,840 --> 00:07:14,030
ezért soha sem szabad levágni a fejet! 

62
00:07:39,315 --> 00:07:43,300
A karfiolnak és a brokkolinak legkésőbb 
júliusban virágoznia kell ahhoz,

63
00:07:44,010 --> 00:07:50,175
hogy a magok hosszan tartó érési 
folyamatának befejezésére elég idő maradjon. 

64
00:08:03,200 --> 00:08:09,940
Magfogás, magtisztítás és magtárolás
 a B. oleracea fajba tartozó növények esetében

65
00:08:19,060 --> 00:08:22,750
A magok akkor érettek, amikor a 
becőtermések bézs színűvé válnak.

66
00:08:27,055 --> 00:08:29,500
Érett állapotban a becők könnyen

67
00:08:29,800 --> 00:08:34,780
felnyílnak, és szétszórják 
a bennük található magokat. 

68
00:08:43,395 --> 00:08:47,455
Általában a szárak nem egyszerre érnek be.

69
00:08:48,415 --> 00:08:54,425
A magvesztés elkerüléséhez a magszárakat 
külön kell betakarítani, amint azok beértek.

70
00:08:56,230 --> 00:09:01,875
A teljes növényt is felszedhetjük még 
azelőtt, hogy minden mag teljesen beérne. 

71
00:09:05,420 --> 00:09:12,240
Ilyenkor az érési folyamat a szárítás végére 
fejeződik be, ami száraz és jól szellőző helyen zajlik. 

72
00:09:24,845 --> 00:09:30,890
A káposztafélék akkor állnak készen a magfogásra, 
amikor a becőtermések kézzel könnyen kinyithatóak. 

73
00:09:32,995 --> 00:09:34,260
A magok kinyeréséhez a

74
00:09:34,438 --> 00:09:37,700
becőterméseket terítsük szét műanyag fólián

75
00:09:38,220 --> 00:09:43,010
vagy vastag szöveten, majd ütögessük 
meg, vagy a kezünk között dörzsöljük össze őket.

76
00:09:46,500 --> 00:09:51,455
A terméseket zsákba is tehetjük, és puha 
felülethez ütögetve kicsépelhetjük a magokat. 

77
00:09:52,960 --> 00:09:57,640
Nagy mennyiség esetén lábbal vagy 
valamilyen járművel ki is taposhatjuk őket. 

78
00:10:10,255 --> 00:10:15,550
A nehezen felnyíló becőtermések 
valószínűleg éretlen magokat

79
00:10:15,720 --> 00:10:17,440
tartalmaznak, amelyek nem csíráznak jól. 

80
00:10:22,185 --> 00:10:23,500
A válogatás során

81
00:10:23,710 --> 00:10:28,245
a törmelék eltávolításához először egy nagylyukú
rostán engedjük át a magokat,

82
00:10:28,680 --> 00:10:30,140
amelyen a törmelék

83
00:10:35,065 --> 00:10:37,820
fennmarad, majd pedig egy finomabb szitán, amin

84
00:10:38,100 --> 00:10:42,940
a magok maradnak fenn, 
a kisebb törmelékek viszont átesnek. 

85
00:10:46,405 --> 00:10:48,540
Végül fújva vagy pedig a szél segítségével

86
00:10:49,015 --> 00:10:52,140
ki kell szelelnünk a magokat, hogy a

87
00:10:52,290 --> 00:10:54,700
maradék növényi részeket is eltávolíthassuk. 

88
00:11:09,825 --> 00:11:14,900
Az összes Brassica oleracea fajba tartozó 
növény magja nagyon hasonló.

89
00:11:15,790 --> 00:11:19,595
Rendkívül nehéz 
megkülönböztetni őket,

90
00:11:20,065 --> 00:11:22,470
például a fejes káposzta és a karfiol magjait.

91
00:11:23,020 --> 00:11:26,020
Ezért fontos címkével ellátni a növényeket,

92
00:11:26,140 --> 00:11:29,940
majd a kinyert magokat, feltüntetve 
a faj és a fajta nevét,

93
00:11:30,220 --> 00:11:33,140
valamint a termesztés évét. 

94
00:11:34,600 --> 00:11:39,415
Néhány fagyasztóban töltött nap alatt 
valamennyi kártevő lárvája elpusztul. 

95
00:11:44,875 --> 00:11:47,940
A káposztafélék magjai 5 évig őrzik meg 
a csírázóképességüket,

96
00:11:49,090 --> 00:11:52,665
de ez akár 10 évig is kitolódhat.

97
00:11:54,550 --> 00:11:57,695
Úgy tudjuk meghosszabbítani, 
ha a magokat fagyasztóban tároljuk.

98
00:11:58,855 --> 00:12:05,205
Egy gramm fajtától függően 
250-300 darab magot tartalmaz. 
