1
00:00:08,756 --> 00:00:14,283
Az édeskömény az ernyősök (Apiaceae) családjába 
tartozik. A faj latin neve: Foeniculum vulgare.

2
00:00:16,501 --> 00:00:22,196
Lehet egyéves, kétéves, 
illetve bizonyos körülmények között évelő is.

3
00:00:24,230 --> 00:00:25,920
Két típusa létezik:

4
00:00:28,007 --> 00:00:31,820
a gumós édeskömény (vagy firenzei édeskömény),

5
00:00:34,340 --> 00:00:36,880
és a leveléért és a magjáért termesztett,

6
00:00:37,360 --> 00:00:43,032
gumót nem növesztő édeskömény, 
amelyet fűszerként és gyógyászati célból használnak. 

7
00:00:44,603 --> 00:00:47,410
Vannak rövid és hosszú tenyészidejű fajták.

8
00:00:50,807 --> 00:00:57,178
Az édeskömény hosszúnappalos növény, 
vagyis a virágzáshoz hosszú nappali megvilágítást igényel. 

9
00:01:07,818 --> 00:01:08,778
Megporzás 

10
00:01:28,538 --> 00:01:33,636
Az édeskömény virágzata ernyővirágzat, 
amely kis sárga, általában

11
00:01:33,934 --> 00:01:35,890
kétivarú virágokból áll.

12
00:01:38,349 --> 00:01:44,465
A porzó (hímivarú szaporítószerv) a 
termő (nőivarú szaporítószerv) előtt érik. 

13
00:01:50,138 --> 00:01:54,712
Ezért öntermékenyítés nem 
valósulhat meg egy virágon belül.

14
00:01:56,029 --> 00:01:59,636
Viszont mivel a virágok nem egyszerre nyílnak, 
ezért előfordulhat

15
00:02:00,174 --> 00:02:03,940
az önbeporzás egy ernyővirágzaton 
belül, illetve

16
00:02:04,247 --> 00:02:07,018
egy növény két ernyője között. 

17
00:02:11,032 --> 00:02:15,760
Természetesen a különböző egyedek 
ernyővirágzatai között is történik megporzás. 

18
00:02:19,440 --> 00:02:25,018
Így az édeskömény idegentermékenyülő növény,
 melynek beporzását főként a rovarok végzik.

19
00:02:32,320 --> 00:02:36,240
Különböző fajtái összeporzódhatnak egymással. 

20
00:02:39,156 --> 00:02:41,512
Az édeskömény a vad édesköménnyel is kereszteződhet,

21
00:02:41,883 --> 00:02:44,778
ami gyakori fajnak számít 
a világ bizonyos részein. 

22
00:02:50,269 --> 00:02:52,043
A keresztbeporzás elkerülése érdekében

23
00:02:52,261 --> 00:02:55,890
a különböző fajták között tartsunk 
1 kilométeres távolságot. 

24
00:03:00,581 --> 00:03:06,821
Ezt 500 méterre csökkenthetjük, ha a fajták között 
van valamilyen természetes akadály, pl. sövény. 

25
00:03:14,610 --> 00:03:20,420
A fajtákat úgy is izolálhatjuk egymástól, ha zárt 
rovarháló alá kis kaptárakat teszünk rovarokkal együtt,

26
00:03:26,152 --> 00:03:30,660
vagy ha az egyes fajtákat takaró 
rovarhálókat felváltva nyitjuk és zárjuk.

27
00:03:31,389 --> 00:03:32,523
Bővebb információt ezzel

28
00:03:32,680 --> 00:03:38,189
kapcsolatban az izolációs technikákról szóló modulban 
találsz A magtermesztés ábécéjében. 

29
00:03:44,276 --> 00:03:45,272
Életciklus

30
00:03:55,200 --> 00:03:57,629
A gyógyászati célú édeskömény nem fejleszt gumót.

31
00:03:59,054 --> 00:04:01,396
Általában évelőként termesztik.

32
00:04:08,727 --> 00:04:13,825
Tavasszal vetik, és az első évben 
virágzik, illetve magot is hoz.

33
00:04:14,210 --> 00:04:18,501
Jól bírja a fagyokat, így télen is 
a földben maradhat.

34
00:04:19,352 --> 00:04:23,687
A termesztés második évében több magot hoz. 

35
00:04:29,120 --> 00:04:32,341
A gumós édesköményt másként termesztik. 

36
00:04:34,727 --> 00:04:39,258
Enyhébb éghajlaton tavaszi változatokat 
termesztenek egyéves növényként.

37
00:04:39,563 --> 00:04:42,370
Kora tavasszal, márciusban vetik,

38
00:04:42,712 --> 00:04:46,458
így elég ideje van, gumót fejleszteni 
még a virágzás előtt.

39
00:04:53,047 --> 00:04:58,996
A magtermesztésre szánt gumós édesköményt 
legtöbbször kétéves növényként termesztik. 

40
00:04:59,556 --> 00:05:05,876
A nyári változatokat június 20-a után vetik, 
amikor a nappalok kezdenek rövidülni.

41
00:05:06,000 --> 00:05:08,683
Így nem kezdenek túl korán virágot hozni, illetve

42
00:05:09,461 --> 00:05:12,749
elegendő idejük marad gumót 
növeszteni még a tél beállta előtt.

43
00:05:14,072 --> 00:05:17,316
Virágot és magokat a második évben hoznak. 

44
00:05:43,578 --> 00:05:46,974
Csak az adott fajta jellegzetes 
tulajdonságaival rendelkező,

45
00:05:47,127 --> 00:05:50,138
gumót fejlesztő növényekről fogjunk magot!

46
00:05:55,258 --> 00:05:57,781
A túl korán virágzó egyedeket távolítsuk el! 

47
00:06:04,487 --> 00:06:06,850
A gumókat a magfogáshoz az adott fajta

48
00:06:06,952 --> 00:06:10,101
tulajdonságait alapul véve 
válogassuk ki, beleértve a színt,

49
00:06:11,127 --> 00:06:13,905
a formát és a növekedési erélyt! 

50
00:06:28,654 --> 00:06:32,472
Hideg területeken 
a gumók megfagyhatnak a talajban.

51
00:06:33,090 --> 00:06:36,380
Ennek elkerülésére a leveleket távolítsuk el 
az utolsó rügy felett,

52
00:06:36,698 --> 00:06:40,887
majd a gumókat gyökerestül, az őket 
körülvevő talajjal együtt tegyük üvegbe

53
00:06:41,170 --> 00:06:44,298
vagy edénybe, védve a fagytól és a túl sok nedvességtől. 

54
00:06:53,614 --> 00:06:56,312
A gumókat a tél folyamán 
rendszeresen ellenőrizzük,

55
00:06:56,981 --> 00:07:00,080
és távolítsuk el azokat, amelyek elkezdtek rothadni!

56
00:07:05,620 --> 00:07:11,236
A komolyabb fagyveszély elmúltával 
tavasszal visszaültethetjük a gumókat a talajba. 

57
00:07:30,900 --> 00:07:36,334
A genetikai sokféleség biztosítása érdekében 
15-20 növényre van szükség a magfogáshoz. 

58
00:07:40,094 --> 00:07:44,596
Az enyhébb éghajlatú 
területeken a gumóskömény a talajban is áttelelhet.

59
00:07:45,854 --> 00:07:49,694
A második év tavaszától tovább folytatja 
a fejlődését, majd virágot hoz. 

60
00:08:27,890 --> 00:08:32,225
Az édesköménynek rendkívül magas virágszárai vannak, 
amelyek gyakran eldőlnek,

61
00:08:33,054 --> 00:08:34,283
ezért ki kell karózni őket. 

62
00:08:35,440 --> 00:08:41,367
Amikor az érett magok elkezdenek peregni, 
az ernyővirágzatokat egy kis szárdarabbal együtt vágjuk le!

63
00:08:45,927 --> 00:08:47,750
Akár hamarabb is levághatjuk őket,

64
00:08:48,443 --> 00:08:53,120
hiszen a magok lassan utóérnek a száradás során.

65
00:08:56,218 --> 00:09:01,745
Száraz, jól szellőző helyen fejezzük be a szárítást! 

66
00:09:10,080 --> 00:09:12,756
Magfogás - tisztítás - tárolás

67
00:09:19,614 --> 00:09:22,967
A magok kinyeréséhez dörzsöljük össze 
az ernyővirágzatokat a tenyereink között! 

68
00:09:34,167 --> 00:09:39,323
Először nagylyukú szitát használjunk, 
amin fennmaradnak a növényi törmelékek!

69
00:09:45,578 --> 00:09:50,400
Ezután finomabb szitát alkalmazzunk, amelyen 
a magok nem tudnak átesni, a por viszont igen! 

70
00:10:06,600 --> 00:10:09,620
Végül fújással szeleljük ki a magokat.

71
00:10:09,890 --> 00:10:12,225
Ez eltávolítja a maradék növényi törmeléket is. 

72
00:10:15,505 --> 00:10:19,450
Mindig írjuk fel egy címkére a fajta és a faj 
nevét, valamint a magfogás évét,

73
00:10:19,556 --> 00:10:24,378
majd helyezzük azt a tasak belsejébe! 
A külső felirat könnyen letörlődhet.

74
00:10:27,687 --> 00:10:32,138
Néhány fagyasztóban töltött nap 
alatt a kártevők lárvái elpusztulnak. 

75
00:10:35,400 --> 00:10:39,345
Az édeskömény magjai 4 évig őrzik meg 
a csírázóképességüket,

76
00:10:39,520 --> 00:10:42,778
de ez időnként akár 7 évre is kitolódhat.

77
00:10:43,345 --> 00:10:46,916
Ezt úgy tudjuk meghosszabbítani, ha 
a magokat fagyasztóban tároljuk.

78
00:10:47,578 --> 00:10:50,560
Egy gramm körülbelül 300 magot tartalmaz. 
