﻿1
00:00:07,700 --> 00:00:12,140
A magtermesztésre szánt növények szelekciója és gondozása

2
00:00:18,565 --> 00:00:24,090
A szelekció lehetővé teszi, hogy a 
növényt fokozatosan az adott környezethez,

3
00:00:24,650 --> 00:00:26,430
szükségleteinkhez és kívánságainkhoz alakítsuk.

4
00:00:27,140 --> 00:00:32,625
Szenteljünk kiemelt figyelmet a magfogásra 
szánt növényeknek, hiszen ezek

5
00:00:33,795 --> 00:00:38,280
hozzák létre a zöldségek és a gyümölcsök 
jövőbeli generációit!

6
00:00:40,560 --> 00:00:43,780
Körültekintően válasszuk ki őket, és

7
00:00:44,095 --> 00:00:47,180
fejlődésük során végig 
kísérjük figyelemmel a tulajdonságaikat! 

8
00:00:52,725 --> 00:00:56,215
Ha csak a termés alapján 
szelektálunk, akkor

9
00:00:56,825 --> 00:01:01,800
nem vesszük figyelembe a növények 
fejlődésével kapcsolatos többi tulajdonságát. 

10
00:01:04,425 --> 00:01:07,795
Fogalmazzuk meg pontosan a kiválasztási kritériumokat,

11
00:01:08,380 --> 00:01:11,795
mert ezek fogják meghatározni, 
hogy mely növényekről fogunk magot! 

12
00:01:13,860 --> 00:01:18,640
Először is az adott fajtához kötődő 
kritériumokat kell figyelembe venni,

13
00:01:19,270 --> 00:01:24,270
pl. az ellenálló képességet, a terméshozamot, a rövid tenyészidőt,

14
00:01:24,695 --> 00:01:29,535
illetve a szubjektív tényezőket, 
mint az íz vagy a megjelenés.

15
00:01:43,890 --> 00:01:47,000
Ezeken kívül a növények 
környezethez és művelésmódhoz

16
00:01:47,195 --> 00:01:51,035
való alkalmazkodóképességét 
is mérlegelni kell. 

17
00:01:54,175 --> 00:01:59,515
A lényeg, hogy a kezdetektől fogva tudjuk, 
hogy mely szempontok fontosak számunkra,

18
00:02:00,160 --> 00:02:03,695
mert a fajta generációról-generációra 
fejlődni fog, és közben

19
00:02:03,895 --> 00:02:07,850
egyes tulajdonságok kevésbé lesznek 
észrevehetőek, vagy dominánsak. 

20
00:02:13,340 --> 00:02:17,465
Időnként nem érdemes szelektálni.

21
00:02:17,770 --> 00:02:22,450
Például ha csak néhány mag maradt 
egy kihalás szélén álló ritka fajtából -

22
00:02:22,740 --> 00:02:24,885
ez esetben nem tudunk

23
00:02:25,120 --> 00:02:28,235
elegendő növényt felnevelni 
az első évben ahhoz,

24
00:02:28,305 --> 00:02:30,400
hogy válogatni lehessen közöttük. 

25
00:02:35,765 --> 00:02:41,100
Úgy is dönthetünk, hogy egy fajtát
úgy őrzünk meg, ahogy van, és nem végzünk szelekciót.

26
00:02:41,470 --> 00:02:45,140
Ilyenkor a genetikai sokféleség 
növelése érdekében

27
00:02:45,660 --> 00:02:51,220
valamennyi változatos 
tulajdonságot megőrizzük.

28
00:02:52,195 --> 00:02:55,240
Ez kiemelkedő alkalmazkodóképességet eredményez,

29
00:02:55,670 --> 00:02:58,785
és lehetőséget biztosít azon tulajdonságok

30
00:02:58,955 --> 00:03:01,775
megőrzésére, amelyek egy jövőbeli 
szelekció alapjául szolgálhatnak. 

31
00:03:09,170 --> 00:03:12,870
Fontos a magfogásra szánt 
növényeket megjelölni,

32
00:03:14,590 --> 00:03:18,270
hogy később meg tudjuk különböztetni 
őket a fogyasztásra szánt növényektől. 

33
00:03:20,885 --> 00:03:25,595
Ha készítünk egy vázlatos rajzot a kertünkről 
az egyes fajták elhelyezkedésével,

34
00:03:25,940 --> 00:03:29,150
könnyebben megtalálhatjuk őket 
akkor is, ha a jelölések elvesznek. 

35
00:03:31,095 --> 00:03:35,920
A magtermesztés céljából nevelt növények 
magfogásig tartó teljes életciklusa

36
00:03:36,125 --> 00:03:38,505
sokszor hosszabb, mint fogyasztásra szánt társaiké.

37
00:03:39,215 --> 00:03:42,865
Például a fejes salátát 
2-3 hónap után megehetjük,

38
00:03:43,110 --> 00:03:48,060
de a magfogásig tartó teljes 
életciklusa 5-6 hónap.

39
00:04:05,450 --> 00:04:09,180
Létezik sok kétéves növény is,
 mint például a sárgarépa,

40
00:04:09,570 --> 00:04:13,910
amelyek csak az életciklusuk második évében 
virágzanak, illetve hoznak magot. 

41
00:05:00,780 --> 00:05:06,660
A legjobb megoldás az, ha a magtermesztést a kert 
valamelyik erre a célra elkülönített sarkában végezzük. 
