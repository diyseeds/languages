﻿1
00:00:12,995 --> 00:00:16,680
A kelbimbó vagy bimbós kel a káposztafélék 
(Brassicaceae) családjának tagja,

2
00:00:17,340 --> 00:00:22,210
Latin neve Brassica oleracea, ezen 
belül a gemmifera alfajhoz tartozik.

3
00:00:26,030 --> 00:00:30,215
Ebbe a fajba tartozik a karalábé, 
a brokkoli,

4
00:00:30,675 --> 00:00:37,130
a fejes káposzta, a leveles kel (fodros kel), 
a karfiol, és a kelkáposzta is. 

5
00:00:42,530 --> 00:00:45,832
A kelbimbót őszi és téli zöldségként 
termesztik a mérsékelt

6
00:00:45,950 --> 00:00:48,770
égöv hidegebb területein.

7
00:00:51,030 --> 00:00:56,825
Kisméretű bimbókat fejleszt a levelek tövében, és a 
rendkívül hideg hőmérsékletet is kibírja. 

8
00:01:05,760 --> 00:01:10,315
A B. oleracea fajba tartozó 
káposztafélék megporzása 

9
00:01:26,400 --> 00:01:30,285
A Brassica oleracea faj virágai kétivarúak,
 vagyis vannak hímivarú

10
00:01:33,250 --> 00:01:37,460
és nőivarú szaporítószerveik is.
A legtöbbjük önmeddő,

11
00:01:41,155 --> 00:01:43,650
az egyik növény virágai által

12
00:01:44,265 --> 00:01:49,250
termelt virágpor csak egy másik 
növényt képes megtermékenyíteni.

13
00:01:53,140 --> 00:01:55,315
Ezért a növények idegentermékenyülők.

14
00:01:56,285 --> 00:02:01,020
Az optimális megporzás érdekében 
ajánlott sok növényt ültetni. 

15
00:02:04,665 --> 00:02:07,615
A megporzást rovarok végzik.

16
00:02:11,095 --> 00:02:15,450
Ezek a tulajdonságok biztosítják 
a nagy genetikai változatosságot.

17
00:02:22,510 --> 00:02:28,405
A Brassica oleracea faj valamennyi alfaja 
képes egymással kereszteződni, ezért

18
00:02:30,330 --> 00:02:34,920
nem szabad különböző típusú káposztákat
 egymáshoz közel termeszteni, ha magfogás a célunk! 

19
00:02:39,565 --> 00:02:40,990
A genetikai tisztaság

20
00:02:41,330 --> 00:02:48,695
biztosításához a B. oleracea faj különböző fajtái 
között minimum 1 km-es izolációs távolságot hagyjunk! 

21
00:02:50,160 --> 00:02:53,025
Ezt 500 méterre csökkenthetjük, ha két fajta

22
00:02:53,215 --> 00:02:57,485
között van valamilyen 
természetes akadály, pl. sövény. 

23
00:03:00,745 --> 00:03:05,960
A fajtákat úgy is izolálhatjuk egymástól, 
ha zárt rovarháló alá kis kaptárakat teszünk

24
00:03:06,165 --> 00:03:08,340
rovarokkal együtt, vagy ha

25
00:03:13,530 --> 00:03:17,285
az egyes fajtákat takaró 
rovarhálókat felváltva nyitjuk és zárjuk.

26
00:03:21,680 --> 00:03:22,830
Bővebb információt

27
00:03:22,985 --> 00:03:27,270
erről az izolációs technikákról szóló modulban 
találsz "A magtermesztés ábécéje" fejezetben. 

28
00:03:38,710 --> 00:03:40,940
A kelbimbó életciklusa

29
00:03:56,615 --> 00:03:59,415
A kelbimbó kétéves növény.

30
00:04:00,210 --> 00:04:04,260
Az ehető bimbókat ősszel 
és télen hozza.

31
00:04:04,820 --> 00:04:08,100
A virágszárait a következő 
tavaszon növeszti.

32
00:04:14,360 --> 00:04:18,730
A magfogás céljából vetett növényeket ugyanúgy 
termesztjük, mint a fogyasztásra nevelteket.

33
00:04:19,840 --> 00:04:22,095
Májusban-júniusban kell vetni. 

34
00:04:45,350 --> 00:04:51,425
A genetikai sokféleség biztosítása érdekében minimum 
15 növényt válasszunk ki magfogásra. 

35
00:04:55,495 --> 00:04:58,065
A magfogásra szánt növényeket egészséges

36
00:04:58,190 --> 00:05:01,200
egyedek közül válasszuk ki, melyek 
növekedését végig figyelemmel tudtuk kísérni!

37
00:05:04,320 --> 00:05:08,595
Így az adott fajta valamennyi 
tulajdonságát ellenőrizhetjük,

38
00:05:09,075 --> 00:05:13,440
mint a szabályos bimbók fejlesztése 
a szár teljes hosszában,

39
00:05:15,035 --> 00:05:18,840
a bimbók tömörsége, színe, alakja

40
00:05:19,770 --> 00:05:22,390
és íze (ne legyen keserű),

41
00:05:23,010 --> 00:05:27,010
továbbá a hidegtűrés,
 a terméshozam és a növény mérete. 

42
00:05:28,495 --> 00:05:34,070
A növények az első évben elérhetik 
a 60-80 cm-es magasságot. 

43
00:05:36,135 --> 00:05:40,235
Ősszel a szár mentén kialakult bimbókat 
be lehet takarítani,

44
00:05:40,690 --> 00:05:43,915
de a szár tetején levőket nem szabad eltávolítani! 

45
00:05:46,690 --> 00:05:51,355
A kelbimbó sokkal jobban tűri a hideget, 
mint a nagyméretű káposzták.

46
00:05:53,410 --> 00:05:57,415
A téli fajták egész télen 
kint maradhatnak a földben.

47
00:05:59,980 --> 00:06:04,080
Ha szükséges, a növényeket fagyvédő takaróval 
óvhatjuk a nagyobb hidegektől. 

48
00:06:13,985 --> 00:06:19,020
A második évben a szárak akár 
az 1,5 m-es magasságot is elérhetik.

49
00:06:19,800 --> 00:06:21,135
Ilyenkor az eldőlés megelőzése

50
00:06:21,585 --> 00:06:25,670
érdekében szükséges lehet 
a virágszárak karózása. 

51
00:06:27,690 --> 00:06:31,785
A virágzás meggyorsítása érdekében 
a szár csúcsát akár le is vághatjuk. 

52
00:07:17,840 --> 00:07:24,640
Magfogás a B. oleracea fajról,
a magok válogatása és tárolása 

53
00:07:33,675 --> 00:07:37,525
A magok akkor érettek, amikor a 
becőtermések bézs színűvé válnak.

54
00:07:41,670 --> 00:07:43,980
Érett állapotban a becők könnyen

55
00:07:44,385 --> 00:07:49,310
felnyílnak, és szétszórják 
a bennük található magokat. 

56
00:07:57,965 --> 00:08:02,000
Általában a szárak nem egyszerre érnek be.

57
00:08:03,025 --> 00:08:09,000
A magvesztés elkerüléséhez a magszárakat 
külön kell betakarítani, amint azok beértek.

58
00:08:10,835 --> 00:08:16,460
A teljes növényt is felszedhetjük még 
azelőtt, hogy minden mag teljesen beérne. 

59
00:08:19,940 --> 00:08:25,045
Ilyenkor az érési folyamat a szárítás végére 
fejeződik be, ami száraz és

60
00:08:25,150 --> 00:08:26,970
jól szellőző helyen zajlik. 

61
00:08:39,465 --> 00:08:45,445
A káposztafélék akkor állnak készen a magfogásra, 
amikor a becőtermések kézzel könnyen kinyithatóak. 

62
00:08:47,570 --> 00:08:48,890
A magok kinyeréséhez a

63
00:08:49,040 --> 00:08:54,375
becőterméseket terítsük szét műanyag fólián 
vagy vastag szöveten, majd

64
00:08:54,785 --> 00:08:57,710
ütögessük meg, vagy a kezünk között dörzsöljük össze őket.

65
00:09:01,125 --> 00:09:05,840
A terméseket zsákba is tehetjük, és puha 
felülethez ütögetve kicsépelhetjük a magokat. 

66
00:09:07,600 --> 00:09:12,275
Nagy mennyiség esetén lábbal vagy 
valamilyen járművel ki is taposhatjuk őket. 

67
00:09:24,800 --> 00:09:30,090
A nehezen felnyíló becőtermések 
valószínűleg éretlen magokat

68
00:09:30,310 --> 00:09:32,060
tartalmaznak, amelyek nem csíráznak jól. 

69
00:09:36,765 --> 00:09:37,995
A válogatás során

70
00:09:38,310 --> 00:09:41,395
a törmelék eltávolításához először egy nagylyukú

71
00:09:41,580 --> 00:09:44,605
rostán engedjük át a magokat, amelyen törmelék

72
00:09:49,685 --> 00:09:52,265
fennmarad, majd pedig egy finomabb szitán, amin

73
00:09:52,715 --> 00:09:57,380
a magok maradnak fenn, 
a kisebb törmelékek viszont átesnek. 

74
00:10:01,020 --> 00:10:04,930
Végül fújva vagy pedig a szél segítségével 
ki kell szelelnünk

75
00:10:05,210 --> 00:10:09,245
a magokat, hogy a maradék 
növényi részeket is eltávolíthassuk. 

76
00:10:24,425 --> 00:10:29,380
Az összes Brassica oleracea fajba tartozó 
növény magja nagyon hasonló.

77
00:10:30,390 --> 00:10:37,015
Rendkívül nehéz megkülönböztetni őket, 
például a fejes káposzta és a karfiol magjait.

78
00:10:37,605 --> 00:10:40,500
Ezért fontos címkével ellátni a növényeket,

79
00:10:40,775 --> 00:10:44,550
majd a kinyert magokat, feltüntetve 
a faj és a fajta nevét,

80
00:10:44,835 --> 00:10:47,855
valamint a termesztés évét. 

81
00:10:49,275 --> 00:10:54,035
Néhány fagyasztóban töltött nap alatt 
valamennyi kártevő lárvája elpusztul. 

82
00:10:59,480 --> 00:11:02,505
A káposztafélék magjai 5 évig őrzik meg a csírázóképességüket,

83
00:11:03,695 --> 00:11:07,395
de ez akár 10 évig is kitolódhat.

84
00:11:09,145 --> 00:11:12,405
Úgy tudjuk meghosszabbítani, 
ha a magokat fagyasztóban tároljuk.

85
00:11:13,450 --> 00:11:19,625
Egy gramm fajtától függően 
250-300 darab magot tartalmaz. 
