1
00:00:15,994 --> 00:00:19,620
A mag a megporzás 
során megtermékenyített magkezdemény

2
00:00:19,780 --> 00:00:21,660
átalakulásának eredménye.

3
00:00:23,994 --> 00:00:25,820
Egy tartalék tápanyaggal

4
00:00:27,620 --> 00:00:35,500
körülvett embrióból, és az azt 
körülvevő maghéjból áll.

5
00:00:38,538 --> 00:00:42,260
A tartalék tápanyag-raktár 
mérete fajonként változó.

6
00:00:53,621 --> 00:01:00,180
A magok megjelenése rendkívül eltérő lehet, 
hiszen az alakjuk, méretük, színük

7
00:01:00,448 --> 00:01:03,660
és textúrájuk minden növényfaj esetében eltérő.

8
00:01:05,728 --> 00:01:09,780
A magokból egészséges 
és életerős növények bújnak ki,

9
00:01:10,060 --> 00:01:15,180
amelyek majd maguk is magokat hoznak. 
Így állandósul az élet körforgása.

10
00:01:21,904 --> 00:01:27,146
A magok kisebb vagy nagyobb 
távolságokra jutnak el az anyanövénytől.

11
00:01:30,474 --> 00:01:33,860
Az adott faj és a helyi
körülmények függvényében

12
00:01:34,060 --> 00:01:38,260
a magok egyszerűen leeshetnek 
az anyanövény mellé a földre,

13
00:01:38,580 --> 00:01:42,620
vagy sokkal távolabbra 
eljuthatnak a szél vagy

14
00:01:42,860 --> 00:01:46,460
az állatok segítségével, 
melyek szőrzetébe beleakadnak.

15
00:01:52,293 --> 00:01:55,060
A cél az, hogy minél messzebbre eljussanak.

16
00:01:56,260 --> 00:02:02,380
A terjedésüket a szél, a víz, a rovarok, a madarak

17
00:02:02,700 --> 00:02:06,021
és számos egyéb állat 
akaratlanul is elősegítheti.

18
00:02:07,861 --> 00:02:10,660
A magok rendkívül türelmesek.

19
00:02:10,940 --> 00:02:16,896
Néha sokat kell várniuk, hogy a külső körülmények 
megfelelőek legyenek a további fejlődésükhöz.

20
00:02:17,306 --> 00:02:20,380
Ezt a jelenséget magnyugalomnak nevezik.

21
00:02:24,597 --> 00:02:29,900
A magok ebből a nyugalmi állapotból különböző 
ingerek hatására kerülnek ki.

22
00:02:39,340 --> 00:02:43,520
Egyeseknek át kell jutniuk 
valamilyen állat bélcsatornáján,

23
00:02:44,026 --> 00:02:47,740
ahol aktiválják őket az 
állatok emésztőenzimjei,

24
00:02:48,133 --> 00:02:53,260
másokat pedig erjedési folyamat 
vagy fagyhatás stimulál.

25
00:02:56,053 --> 00:03:01,500
Ezért a kertészek időnként bizonyos 
természetes jelenségeket igyekeznek utánozni,

26
00:03:02,060 --> 00:03:04,500
hogy véget vessenek a magnyugalmi állapotnak.

27
00:03:05,082 --> 00:03:11,820
Vetéskor a csírázást elősegítő valamennyi feltételt 
biztosítanunk kell:

28
00:03:12,580 --> 00:03:15,820
a szükséges mennyiségű vizet, hőmérsékletet

29
00:03:16,260 --> 00:03:19,460
és fényt, az optimális vetési mélységet a talajban,

30
00:03:20,074 --> 00:03:23,380
illetve az adott fajta 
által igényelt évszakot.

31
00:03:24,160 --> 00:03:25,589
A magok élettartama,

32
00:03:26,186 --> 00:03:29,620
vagyis az az időtartam, 
amíg képesek kicsírázni,

33
00:03:30,180 --> 00:03:31,780
fajonként eltérő.

34
00:03:32,160 --> 00:03:37,540
Például a pasztinák magjai csupán 
egy évig őrzik meg a teljes csírázóképességüket.

35
00:03:38,060 --> 00:03:41,900
Ezzel szemben a cikóriamagok akár 
10 évig is csíraképesek maradhatnak.

36
00:03:42,380 --> 00:03:46,260
Ezt követően a magok 
csírázóképessége elkezd csökkenni.

37
00:03:48,298 --> 00:03:52,420
Ha szeretnénk ellenőrizni 
a magok termékenységét,

38
00:03:52,740 --> 00:03:55,420
végezzünk csírázási próbát!

39
00:03:55,700 --> 00:03:58,340
Ehhez először megszámoljuk az elvetett magokat,

40
00:03:59,340 --> 00:04:01,900
majd pedig a ténylegesen 
kikelt növényeket.

41
00:04:03,900 --> 00:04:08,060
Látni fogjuk, hogy az idősebb 
magok csírázóképessége alacsonyabb.

42
00:04:13,946 --> 00:04:19,140
A magok élettartamát a szárítási, illetve 
a tárolási körülmények is befolyásolják.

43
00:04:22,282 --> 00:04:26,220
Először is, tökéletesen 
szárítsuk ki a magokat!

44
00:04:27,088 --> 00:04:30,580
Ezt követően tároljuk őket 
hideg, száraz, sötét helyen,

45
00:04:31,300 --> 00:04:34,940
ahol közel állandó a hőmérséklet.

46
00:04:36,860 --> 00:04:41,260
A meleg és párás körülmények 
negatívan befolyásolják a magok minőségét.

47
00:04:43,248 --> 00:04:47,140
Továbbá oda kell figyelnünk 
a magokat fogyasztó apró rovarokra is.

48
00:04:48,320 --> 00:04:53,740
Ezektől könnyen megszabadulhatunk, 
ha a magokat néhány napra fagyasztóba rakjuk.

49
00:04:56,050 --> 00:04:58,042
A fagyasztás jó tárolási módszer,

50
00:04:58,469 --> 00:05:05,980
mert légmentesen lezárt zacskókban 
minden mag kibírja a fagyos (-18°C-os) hőmérsékletet.

51
00:05:08,300 --> 00:05:10,220
Ez meghosszabbítja az élettartalmukat is.

52
00:05:16,600 --> 00:05:18,741
Ennek ellenére a magok

53
00:05:19,180 --> 00:05:22,780
életerejének megőrzése érdekében 
rendszeresen ki kell vetni őket.

54
00:05:25,061 --> 00:05:28,900
Így biztosíthatjuk az alkalmazkodási 
lehetőséget a gyorsan változó

55
00:05:29,060 --> 00:05:30,740
környezeti és éghajlati körülményekhez.
