1
00:00:10,920 --> 00:00:16,378
A pasztinák az ernyősök (Apiaceae) családjába tartozik. 
A faj latin neve: Pastinaca sativa.

2
00:00:17,672 --> 00:00:20,356
Kétéves növény, amit a gyökeréért termesztünk.

3
00:00:25,534 --> 00:00:27,898
A pasztinák általában fehér színű,

4
00:00:28,174 --> 00:00:33,352
de léteznek különböző változatai, amelyek eltérő 
klimatikus, illetve talaj adottságokhoz alkalmazkodtak.

5
00:00:43,020 --> 00:00:44,020
Megporzás 

6
00:00:58,523 --> 00:01:03,534
A pasztinák virágzata kisebb, 
általában kétivarú virágokból

7
00:01:03,621 --> 00:01:05,380
álló ernyővirágzat.

8
00:01:07,280 --> 00:01:13,563
A porzó (hímivarú szaporítószerv),
hamarabb érik,

9
00:01:13,840 --> 00:01:15,294
mint a termő (nőivarú szaporítószerv), 

10
00:01:17,803 --> 00:01:22,443
ezért egy virágon belül 
nem történhet öntermékenyítés.

11
00:01:23,469 --> 00:01:26,901
Ennek ellenére mivel a virágok nem 
egyszerre nyílnak,

12
00:01:27,345 --> 00:01:30,756
az öntermékenyülés megvalósulhat 
egy ernyővirágzaton belül,

13
00:01:32,320 --> 00:01:35,003
vagy ugyanazon növény két különböző ernyője között. 

14
00:01:37,389 --> 00:01:41,454
Megtermékenyítés különböző növények 
ernyői között is előfordul. 

15
00:01:43,636 --> 00:01:49,221
A pasztinák idegentermékenyülő növény,
megporzását elsősorban rovarok végzik.

16
00:01:49,723 --> 00:01:55,498
A megporzók főként különböző
kétszárnyú- és lepkefajok,

17
00:01:56,080 --> 00:01:58,058
továbbá katicabogarak,

18
00:01:58,443 --> 00:02:02,509
amelyeket az ernyőket gyakran elborító
levéltetvek vonzanak a virágokhoz.

19
00:02:05,418 --> 00:02:08,385
A pasztinák keresztbeporzódhat a vadpasztinákokkal is. 

20
00:02:11,098 --> 00:02:13,230
A keresztbeporzódás elkerülése érdekében,

21
00:02:13,825 --> 00:02:18,130
tartsunk a különböző pasztinákfajták 
között 300 méteres izolációs távolságot. 

22
00:02:22,180 --> 00:02:26,740
Ezt 100 méterre csökkenthetjük, 
ha a fajták között van valamilyen

23
00:02:26,829 --> 00:02:29,425
természetes akadály, például sövény. 

24
00:02:31,960 --> 00:02:37,127
A fajtákat úgy izolálhatjuk egymástól,
hogy egy-egy rovarhálóval letakarjuk őket,

25
00:02:37,832 --> 00:02:40,552
majd azokat felváltva nyitjuk 
és zárjuk

26
00:02:40,872 --> 00:02:42,312
az egyes napokon,

27
00:02:44,036 --> 00:02:48,480
vagy egy fajtát
lefedünk szónyoghálóval,

28
00:02:48,930 --> 00:02:51,003
és poszméhkaptárat teszünk a háló alá.

29
00:02:52,843 --> 00:02:54,225
Bővebb információt erről

30
00:02:54,436 --> 00:02:58,436
az izolációs technikákról szóló modulban találsz 
"A magtermesztés ábécéje" fejezetben. 

31
00:03:04,020 --> 00:03:05,170
Életciklus 

32
00:03:16,007 --> 00:03:21,054
A magfogás céljából vetett pasztinákot ugyanúgy 
termesztjük, mint a fogyasztási célból nevelteket,

33
00:03:22,443 --> 00:03:26,603
viszont csak az életciklusuk 
második évében hoznak magot.

34
00:03:48,080 --> 00:03:55,083
A vegetációs idő kezdetén sokáig elhúzódhat
(körülbelül 3 hétig is) a magok csírázása. 

35
00:04:20,552 --> 00:04:21,883
Ha már kifejlődtek,

36
00:04:22,014 --> 00:04:28,014
különböző módszerekkel tárolhatjuk 
a magfogásra szánt pasztinákokat a tél során. 

37
00:04:31,730 --> 00:04:34,312
Kiáshatjuk a gyökereket ősszel.

38
00:04:40,880 --> 00:04:45,570
A gyökereket az adott fajta 
egyedi tulajdonságai alapján válasszuk ki:

39
00:04:45,949 --> 00:04:48,378
mint a szín, az alak és a növekedési erély.

40
00:04:49,112 --> 00:04:54,661
A gyökereket víz nélkül tisztítsuk meg,
a leveleket pedig a gyökérnyak felett vágjuk vissza.

41
00:04:55,149 --> 00:04:58,923
Ezután a pasztinákokat rövid ideig 
hagyjuk száradni szabad levegőn.

42
00:05:02,349 --> 00:05:04,429
Végül egy pincében tárolhatjuk őket.

43
00:05:18,276 --> 00:05:21,301
Télen rendszeresen 
ellenőrizzük a gyökereket,

44
00:05:21,541 --> 00:05:24,174
és távolítsuk el azokat, amelyek elkezdenének rothadni! 

45
00:05:27,883 --> 00:05:31,120
A legegyszerűbb az, ha hagyjuk 
a növényeket a földben áttelelni.

46
00:05:32,589 --> 00:05:35,083
A pasztinák az egyik legedzettebb növény,

47
00:05:35,316 --> 00:05:39,374
így akár a leghidegebb vidékeken 
is a földben maradhat télire.

48
00:05:40,152 --> 00:05:43,294
A fagytól ráadásul még az íze is jobb lesz. 

49
00:05:44,720 --> 00:05:48,330
A legnagyobb hidegek elmúltával 
ássuk ki a növényeket,

50
00:05:48,480 --> 00:05:51,060
és válasszuk ki azokat, amelyekről magot szeretnénk fogni.

51
00:05:55,280 --> 00:05:57,709
A növényeket tavasz elején ültessük át.

52
00:05:58,640 --> 00:06:01,850
A pasztinákokat egymástól 60-90 cm-es 
távolságban ültessük el úgy,

53
00:06:02,014 --> 00:06:07,687
hogy a gyökérnyakuk a talajszintre, 
esetleg egy kissé lejjebb kerüljön! 

54
00:06:13,381 --> 00:06:16,392
Ha közel ültetjük egymáshoz 
a növényeket,

55
00:06:16,560 --> 00:06:21,781
akkor kevesebb harmadlagos ernyőt fejlesztenek, 
amelyekben gyenge minőségű magok fejlődnek. 

56
00:06:26,043 --> 00:06:28,923
Minimum 15-20 növény szükséges 
a magfogáshoz,

57
00:06:29,025 --> 00:06:30,981
a genetikai sokféleség biztosítása érdekében. 

58
00:07:05,330 --> 00:07:09,098
A magfogás céljából termesztett pasztinákok pazar látványt nyújtanak,

59
00:07:09,410 --> 00:07:12,203
és kedvező években akár a 
2 méteres magasságot is elérhetik.

60
00:07:16,116 --> 00:07:18,780
Ezért előfordul, hogy ki kell karózni őket. 

61
00:07:23,876 --> 00:07:29,003
A pasztinák több ernyőt is növeszt, 
amelyek azonban nem egyszerre nyílnak.

62
00:07:31,927 --> 00:07:37,170
A legelőször megjelenő elsődleges ernyő 
a központi szár csúcsán található. 

63
00:07:39,520 --> 00:07:44,494
A másodlagos ernyők 
a központi szárból nőnek ki. 

64
00:07:46,690 --> 00:07:50,829
A harmadlagos ernyők a másodlagos szárakon jönnek létre.

65
00:07:53,127 --> 00:07:58,705
Érdemes az elsődleges ernyőket betakarítani, 
mert ezekben találhatóak a legjobb minőségű magok.

66
00:07:59,556 --> 00:08:03,900
A másodlagos ernyőket csak 
szükség esetén gyűjtsük be. 

67
00:08:30,218 --> 00:08:33,170
Az ernyőket a szár felső részével együtt vágjuk le,

68
00:08:33,352 --> 00:08:35,985
amikor az érett magok elkezdenek peregni.

69
00:08:37,360 --> 00:08:41,752
Mivel könnyen a talajra potyognak, 
így akár hamarabb is levághatjuk őket.

70
00:08:51,781 --> 00:08:56,821
Viszont mindenképp szükséges, hogy a száradás 
száraz, szellős helyen tovább folytatódhasson. 

71
00:09:05,272 --> 00:09:08,203
Magkinyerés – válogatás – tárolás

72
00:09:16,400 --> 00:09:19,287
A magokat kézzel távolítjuk el az ernyőkből.

73
00:09:20,101 --> 00:09:24,654
Viseljünk kesztyűt, mert a magok
illóolajokat bocsátanak ki,

74
00:09:24,760 --> 00:09:27,469
amik hólyagosodást okozhatnak, ha közvetlen napfény éri őket! 

75
00:09:31,854 --> 00:09:37,832
A magtisztítás során a nemkívánatos növényi részek
eltávolításához, először nagylyukú rostán engedjük át a magokat,

76
00:09:39,898 --> 00:09:44,930
majd pedig olyan finomabb szitán, 
amin a magok maradnak fenn, a por viszont átesik. 

77
00:09:57,000 --> 00:10:00,530
Mindig írjuk fel egy címkére a fajta és
a faj nevét, valamint a magfogás évét,

78
00:10:00,640 --> 00:10:05,345
majd helyezzük azt a tasak belsejébe!
A külső felirat könnyen letörlődhet. 

79
00:10:10,400 --> 00:10:14,683
Néhány fagyasztóban töltött nap
alatt a paraziták lárvái elpusztulnak. 

80
00:10:18,060 --> 00:10:21,796
A pasztinák magjai csupán egy évig 
őrzik meg a csírázóképességüket.

81
00:10:22,552 --> 00:10:25,512
Ezt úgy tudjuk meghosszabbítani, 
ha a magokat fagyasztóban tároljuk. 

82
00:10:26,698 --> 00:10:31,418
Egy gramm magban 
körülbelül 220 szem található. 
