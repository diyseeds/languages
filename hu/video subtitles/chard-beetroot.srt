﻿1
00:00:10,625 --> 00:00:14,247
A cékla és a mángold a libatopfélék 
családjába (Chenopodiaceae),

2
00:00:14,356 --> 00:00:17,229
a Beta vulgaris fajba tartozik.

3
00:00:22,021 --> 00:00:29,381
Kétéves növények, melyeket a 
gyökerükért és leveleikért termesztünk.

4
00:00:32,981 --> 00:00:35,134
Öt alfaj létezik:

5
00:00:41,003 --> 00:00:44,487
a cicla különböző mángold fajtákat foglal magába, 

6
00:00:46,276 --> 00:00:50,189
az esculenta fajtába céklák tartoznak, 

7
00:00:52,945 --> 00:00:56,421
a rapa fajtába takarmányrépák tartoznak, 

8
00:00:57,898 --> 00:01:01,723
az altissima fajtába cukorrépa fajták tartoznak, 

9
00:01:03,149 --> 00:01:05,694
a maritima pedig egy vad mángold fajta,

10
00:01:05,869 --> 00:01:10,501
ami Európa nyugati partjaitól 
egészen Indiáig megtalálható. 

11
00:01:12,080 --> 00:01:17,374
A cékla bizonyos fajtáit friss fogyasztásra, 
másokat pedig tartósításra termesztjük.

12
00:01:18,480 --> 00:01:23,112
A gyökérgumók színe változatos lehet: 
rózsaszín, vörös,

13
00:01:23,447 --> 00:01:27,025
néha sárga, világoslila és fehér változatok is ismertek.

14
00:01:28,109 --> 00:01:35,301
Alakjuk is változatos lehet: vannak 
gömbölyű, lapos, hosszú és kúp alakú változatok is. 

15
00:01:41,680 --> 00:01:45,890
A mángold főleg fehér levélnyeléről 
és zöld leveleiről ismert,

16
00:01:46,850 --> 00:01:50,530
de vannak sárga, rózsaszín és piros változatok is.

17
00:01:52,647 --> 00:01:58,800
Vannak kisebb méretű, teljesen zöld fajták is, 
melyek vad rokonaikra hasonlítanak. 

18
00:02:07,360 --> 00:02:08,500
Megporzás 

19
00:02:19,621 --> 00:02:23,178
A cékla és a mángold apró virágai kétivarúak,

20
00:02:24,763 --> 00:02:28,080
de a hím szaporítószerv, a porzó 
hamarabb kezd el

21
00:02:28,240 --> 00:02:34,298
virágport termelni, mint ahogy a 
női szaporítószerv, a bibe fogadóképes lenne.

22
00:02:36,567 --> 00:02:38,749
Így a virágok önmeddők. 

23
00:02:42,487 --> 00:02:44,472
A mángold és a cékla idegentermékenyülők,

24
00:02:44,840 --> 00:02:48,109
azaz szükségük van egy másik növényre 
a megtermékenyüléshez. 

25
00:02:53,723 --> 00:02:56,420
Általában szélmegporzásúak,

26
00:03:02,174 --> 00:03:03,789
de egyes rovarok,

27
00:03:04,036 --> 00:03:09,912
például legyek vagy poloskák 
meg tudják termékenyíteni őket. 

28
00:03:17,061 --> 00:03:20,436
Minden mángold keresztbeporzódhat, 
a vad változatok is,

29
00:03:20,850 --> 00:03:24,043
ahogy a céklák, a cukorrépák és a takarmányrépák is. 

30
00:03:29,170 --> 00:03:36,858
A keresztbeporzás elkerülése érdekében a 
Beta vulgaris fajtái között, tartsunk 1 km-es távolságot.

31
00:03:38,625 --> 00:03:45,861
Ezt akár 500 méterre is csökkenthetjük, ha 
valamilyen természetes akadály, pl. sövény van a fajták között. 

32
00:03:46,880 --> 00:03:51,600
Ha kereskedelmi célú magtermesztés a cél,

33
00:03:52,065 --> 00:03:55,432
biztosítsunk 7 km-es izolációs távolságot. 

34
00:03:57,156 --> 00:04:00,200
Két fajtát rovarháló segítségével

35
00:04:00,443 --> 00:04:03,690
is izolálhatunk, 
melyeket naponkénti váltásban

36
00:04:03,920 --> 00:04:06,414
teszünk szabaddá.

37
00:04:06,836 --> 00:04:08,967
Erről a módszerről

38
00:04:09,280 --> 00:04:15,534
"A magfogás ábécéje"
című videóban találsz bővebb információt. 

39
00:04:28,000 --> 00:04:29,301
Életciklus 

40
00:04:37,541 --> 00:04:41,483
A magfogás céljából vetett céklát és mángoldot

41
00:04:41,796 --> 00:04:45,534
ugyanúgy kell termeszteni, mint 
a fogyasztási célból nevelteket.

42
00:04:47,927 --> 00:04:51,243
Magot fogni a második évben lehet róluk.

43
00:05:29,960 --> 00:05:34,349
Bár egy növényen nagyon sok mag 
keletkezik, fontos legalább tucatnyi

44
00:05:34,698 --> 00:05:39,469
növényt nevelni magfogás céljából, 
hogy biztosítsuk a genetikai sokféleséget. 

45
00:05:43,280 --> 00:05:48,989
A céklát általában az első szezon végén, 
ősszel szedjük fel.

46
00:05:52,021 --> 00:05:55,992
Amikor felszedjük a gyökereket, 
a fajtajellemzőknek megfelelő példányokat

47
00:05:56,225 --> 00:05:59,316
jelöljük ki magfogásra,

48
00:05:59,660 --> 00:06:03,360
melyek színe, alakja, életképessége megfelelő. 

49
00:06:06,334 --> 00:06:12,167
Víz használata nélkül tisztítsuk meg a gyökérgumókat, 
a leveleket vágjuk le a gyökérnyak felett.

50
00:06:18,120 --> 00:06:23,476
A gyökérgumókat fagytól és fénytől védett helyen, 
egy homokkal töltött ládában tároljuk. 

51
00:06:24,763 --> 00:06:33,490
Ideális esetben 1 °C-on és 90-95%-os páratartalom 
mellett tároljuk a növényeket.

52
00:06:39,090 --> 00:06:43,287
Távolítsunk el minden rothadásnak 
indult gyökérgumót a tél folyamán! 

53
00:06:50,770 --> 00:06:56,312
Ami a mángoldot illeti, a kemény telekkel, 
illetve komoly fagyokkal jellemezhető

54
00:06:56,625 --> 00:07:01,018
vidékeken a növényeket ősszel gyökerestül 
fel kell szedni, és be kell tárolni.

55
00:07:02,043 --> 00:07:06,967
A középsőket kivéve valamennyi levelet 
le kell vágni a gyökérnyak felett. 

56
00:07:08,698 --> 00:07:12,167
Vigyázva, nehogy felsértsük őket,
 rakjuk a gyökereket pincébe,

57
00:07:12,581 --> 00:07:15,272
enyhén nyirkos homokkal töltött ládába! 

58
00:07:22,705 --> 00:07:24,472
Ha fejlett gyökérrendszerrel

59
00:07:24,698 --> 00:07:29,941
rendelkezik, akkor a mángoldot a legtöbb esetben 
a kertben is átteleltethetjük.

60
00:07:32,130 --> 00:07:37,454
A különösen hideg időszakokban 
viszont mulcsozzuk a növényeket szalmával! 

61
00:07:38,923 --> 00:07:42,901
Az enyhébb éghajlatú területeken akár a 
céklát is a földben hagyhatjuk télire,

62
00:07:43,287 --> 00:07:47,112
ha későn, augusztusban 
vagy szeptemberben vetettük.

63
00:07:53,723 --> 00:07:57,636
Tavasszal válasszuk ki a magfogásra szánt gyökereket!

64
00:07:58,800 --> 00:08:01,716
Kitakarhatjuk a gyökérnyakakat, hogy meg tudjuk

65
00:08:02,109 --> 00:08:07,592
vizsgálni őket, és megszabadulhatunk azoktól, 
amelyek nem mutatják a fajta kívánt tulajdonságait.

66
00:08:08,910 --> 00:08:13,818
De az a legjobb, ha 
gyökerestől kiszedjük, kiválogatjuk, 

67
00:08:17,803 --> 00:08:24,007
majd azonnal visszaültetjük a növényeket. 

68
00:08:34,385 --> 00:08:39,020
A betárolt és átteleltetett mángold és 
cékla újbóli kiültetésekor fontos, hogy

69
00:08:39,621 --> 00:08:43,112
a gyökérnyak egy vonalba kerüljön 
a talajszinttel, és alaposan

70
00:08:48,749 --> 00:08:50,705
locsoljuk be a növényeket! 

71
00:09:11,505 --> 00:09:16,014
Az életciklus következő lépései 
a mángold és a cékla esetében azonosak.

72
00:09:17,025 --> 00:09:20,872
A virágszárak 1,5 m magasra is megnőhetnek,

73
00:09:21,890 --> 00:09:24,072
gyakran ki kell karózni őket. 

74
00:09:26,007 --> 00:09:29,127
A magképződés problémákba ütközhet,

75
00:09:29,243 --> 00:09:33,505
vagy akár meg is hiúsulhat, 
ha a nappalok és az éjszakák hossza

76
00:09:33,600 --> 00:09:36,320
között kicsi a különbség, ugyanis

77
00:09:37,740 --> 00:09:40,625
hosszú nyári nappalokra van szükségük a magképzéshez. 

78
00:09:46,487 --> 00:09:51,310
Az első magok beérése után egy 
harmatos reggelen vágjuk le a virágszárakat!

79
00:09:55,621 --> 00:10:00,581
A magok valójában gomolyok, 
amelyek 2-6 különálló magot tartalmaznak. 

80
00:10:11,760 --> 00:10:18,043
Egy menetben is le lehet vágni az összes virágszárat, 
viszont ekkor az első érett magok egy részét elveszítjük. 

81
00:10:37,563 --> 00:10:41,818
Mindenesetre fontos, hogy 
a szárítást tovább folytassuk

82
00:10:42,138 --> 00:10:44,530
árnyékos, száraz, jól szellőző helyen! 

83
00:10:57,800 --> 00:11:01,229
Magfogás - tisztítás - tárolás

84
00:11:05,090 --> 00:11:09,280
A magok kinyeréséhez dörzsöljük 
össze a virágszárakat a kezeink közt.

85
00:11:09,710 --> 00:11:11,774
A folyamat során ajánlott kesztyűt viselni.

86
00:11:11,963 --> 00:11:16,807
A magszárakat akár ki is taposhatjuk, 
vagy egy bottal ki is csépelhetjük. 

87
00:11:19,990 --> 00:11:24,865
A tisztításhoz először használjunk nagylyukú
 rostát, amin fennmarad a hulladék,

88
00:11:27,070 --> 00:11:30,654
majd egy finomabb szitát, 
amelyen a magok nem jutnak át,

89
00:11:30,865 --> 00:11:32,610
viszont a kisebb törmelékek igen! 

90
00:11:35,490 --> 00:11:40,596
Végül szeleljük ki a magokat, hogy a 
maradék növényi anyagtól is megszabaduljunk!

91
00:11:41,450 --> 00:11:44,254
Ehhez segítségül hívhatjuk a szelet is. 

92
00:11:47,250 --> 00:11:50,661
Mindig írjuk fel egy címkére a fajta és a faj 
nevét, valamint a magfogás évét,

93
00:11:50,814 --> 00:11:53,716
majd helyezzük azt a tasak belsejébe!

94
00:11:54,270 --> 00:11:57,207
A külső felirat könnyen letörlődhet.

95
00:12:03,854 --> 00:12:09,236
Néhány fagyasztóban töltött nap alatt 
a legtöbb kártevő lárvái elpusztulnak. 

96
00:12:11,941 --> 00:12:16,872
A mángold és a cékla magjai 6 évig, vagy akár 
10 évig is képesek

97
00:12:20,138 --> 00:12:22,036
megőrizni a csírázóképességüket.

98
00:12:24,770 --> 00:12:28,560
Egy gramm magban körülbelül 50 gomoly található. 
