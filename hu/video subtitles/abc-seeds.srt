1
00:00:13,380 --> 00:00:16,340
Hogy válasszuk ki, mely magokat visszük tovább?

2
00:00:23,640 --> 00:00:26,580
Fontos, hogy tisztában legyünk 
azon fajták eredetével,

3
00:00:26,640 --> 00:00:29,050
melyeket a kertünkben szeretnénk termeszteni.

4
00:00:35,540 --> 00:00:39,410
Amikor kiválasztjuk azokat a fajtákat, 
melyekről magot szeretnénk fogni,

5
00:00:39,760 --> 00:00:44,070
kerüljük a nagy vetőmagcégek 
által nemesített fajtákat!

6
00:00:44,870 --> 00:00:50,550
Ne használjunk F1 vagy F2 hibrideket, GMO-kat,

7
00:00:50,840 --> 00:00:53,840
sem más, biotechnológiai úton létrehozott fajtákat. 

8
00:01:00,070 --> 00:01:07,540
Az F1 vagy F2 hibrid növények magjai 
nem alkalmasak az anyanövény reprodukálására.

9
00:01:08,430 --> 00:01:14,435
Magjaik sterilek vagy kiszámíthatatlan tulajdonságokkal 
rendelkező növények nőnek belőlük.

10
00:01:23,935 --> 00:01:26,295
Így a multinacionális vetőmagcégek

11
00:01:26,825 --> 00:01:31,550
megakadályozhatják, hogy a kertészek
 saját magot termesszenek

12
00:01:31,930 --> 00:01:34,975
és fenntarthatják egyeduralmukat a piacon. 

13
00:01:38,995 --> 00:01:43,545
Ezeket a technikai akadályokat  
a jogi szabályozás is megerősíti,

14
00:01:43,745 --> 00:01:46,630
mely szerint tilos ezeket a magokat szaporítani.

15
00:01:49,390 --> 00:01:54,245
Ezek a fajták mind 
szellemi tulajdonjogokkal, például

16
00:01:54,945 --> 00:01:58,230
fajtaoltalommal vagy szabadalommal védettek. 

17
00:02:03,300 --> 00:02:05,910
Ha saját magot szeretnénk termeszteni,

18
00:02:06,485 --> 00:02:10,425
szabad fajták magjaival kell kezdenünk,

19
00:02:10,780 --> 00:02:17,065
melyek lehetőleg ökológiai, biodinamikus vagy 
agroökológiai termesztésből származnak. 

20
00:02:38,575 --> 00:02:40,640
A természet bőkezű,

21
00:02:40,855 --> 00:02:46,165
és általában jóval több maggal lát el minket, 
mint amennyit saját célra fel tudunk használni.

22
00:02:57,520 --> 00:03:01,480
A felesleget elajándékozhatjuk vagy elcserélhetjük másokkal. 
