1
00:00:07,883 --> 00:00:14,676
A spenót a libatopfélék (Chenopodiaceae)
családjának tagja. A faj latin neve: Spinacia oleracea.

2
00:00:19,316 --> 00:00:24,123
Egy- és kétéves növények tartoznak ide, 
melyeket a leveleikért termesztünk.

3
00:00:27,750 --> 00:00:34,890
Vannak téli, tavaszi és nyári
termesztésű fajtái is.

4
00:00:39,396 --> 00:00:40,661
Megporzás 

5
00:00:47,636 --> 00:00:54,334
A spenót kétlaki faj, vagyis vannak
virágport termelő hímivarú egyedek,

6
00:00:56,749 --> 00:01:00,240
és termékeny magokat hozó nőivarú egyedek. 

7
00:01:02,560 --> 00:01:08,305
A nehezen észrevehető nőivarú virágok 
a szárelágazások hónaljában találhatóak. 

8
00:01:09,403 --> 00:01:15,847
A hímivarú virágok a növények tetején
helyezkednek el, és a nőivarú virágok előtt nyílnak. 

9
00:01:19,840 --> 00:01:21,694
A spenót idegentermékenyülő,

10
00:01:22,152 --> 00:01:26,305
vagyis a megporzás különböző 
egyedek között történik.

11
00:01:28,080 --> 00:01:29,832
Szélporozta növény. 

12
00:01:34,680 --> 00:01:36,836
Hosszúnappalos növény,

13
00:01:37,061 --> 00:01:43,963
vagyis akkor kezd virágokat hozni,
ha a nappalok hossza eléri a 10-14 órát.

14
00:01:50,000 --> 00:01:52,349
A virágzása 2-3 hétig tart. 

15
00:01:55,869 --> 00:02:02,581
A keresztbeporzás elkerülése érdekében
a különböző spenótfajták között tartsunk 1 km távolságot. 

16
00:02:05,680 --> 00:02:12,247
Ezt 500 méterre csökkenthetjük, ha a fajták között van
valamilyen természetes akadály, például sövény. 

17
00:02:14,989 --> 00:02:20,356
Különböző módszerek vannak, ha két fajtát 
szeretnénk egy kertben termeszteni.

18
00:02:21,360 --> 00:02:25,447
Például eltolhatjuk a különböző
fajták vetésidejét egymástól,

19
00:02:25,800 --> 00:02:29,550
így biztosítva azt, hogy ne 
egyszerre virágozzanak.

20
00:02:30,247 --> 00:02:35,025
Viszont figyeljünk rá, hogy a növényeknek maradjon elég idejük arra, 
hogy befejezhessék a teljes, magfogásig tartó életciklusukat. 

21
00:02:37,320 --> 00:02:40,443
Alkalmazhatunk mechanikai
izolációs módszert is,

22
00:02:40,814 --> 00:02:45,345
amikor mindegyik fajtát külön rovarhálóval takarjuk le, 
majd azokat naponta felváltva nyitjuk és zárjuk.

23
00:02:45,949 --> 00:02:47,934
Bővebb információt erről,

24
00:02:48,058 --> 00:02:53,040
az izolációs technikákról szóló modulban
találsz "A magtermesztés ábécéje" fejezetben. 

25
00:03:01,530 --> 00:03:02,894
Életciklus 

26
00:03:13,789 --> 00:03:15,498
A magfogás céljából vetett spenót

27
00:03:15,585 --> 00:03:19,243
esetében a termesztéstechnológia 
eltérő lehet az adott fajta függvényében 

28
00:03:19,752 --> 00:03:23,483
A tavaszi fajtákat a vegetációs 
időszak legelején vetjük.

29
00:03:24,014 --> 00:03:27,934
Ugyanebben az évben nyáron 
virágot és magot is hoznak.

30
00:03:28,138 --> 00:03:32,523
Télen nem termeszthetőek, 
mivel nem élnék túl a hideget. 

31
00:03:33,440 --> 00:03:36,370
A téli fajtákat ősszel vetjük.

32
00:03:36,778 --> 00:03:38,220
A tél során továbbfejlődnek,

33
00:03:38,472 --> 00:03:42,080
majd a következő év tavaszán 
virágoznak és hoznak magot. 

34
00:03:56,560 --> 00:04:02,741
A genetikai sokféleség biztosítása érdekében
magfogásra 25-30 növényt kell termeszteni.

35
00:04:21,150 --> 00:04:26,836
Olyan egészséges növényeket válasszunk ki, 
amelyek mutatják az adott fajta jellegzetes tulajdonságait.

36
00:04:34,189 --> 00:04:38,654
A téli spenótok estében jó 
kiválasztási kritérium a hidegtűrés

37
00:04:39,570 --> 00:04:45,563
és a gyökérfulladással szembeni ellenálló képesség.
Utóbbi esetben a levelek sárgára színeződnek.

38
00:04:45,760 --> 00:04:48,305
Sajnos télen gyakran előforduló jelenség.

39
00:04:52,247 --> 00:04:56,770
Érdemes csak kevés levelet leszedni 
a magfogásra szánt növényekről. 

40
00:05:19,949 --> 00:05:27,105
A spenót magszárai akár 80 cm magasra is nőhetnek, 
ennek ellenére nem igényelnek karózást.

41
00:05:31,076 --> 00:05:33,730
Először a hímivarú növények száradnak el.

42
00:05:33,960 --> 00:05:35,861
Érdemes őket gyökerestől kiszedni. 

43
00:05:46,880 --> 00:05:51,396
A nőivarú virágokat a világos
homokszínükről ismerhetjük fel. 

44
00:06:08,007 --> 00:06:12,501
Miután a magok beértek, a reggeli harmat 
után vágjuk le a virágszárakat. 

45
00:06:21,270 --> 00:06:26,940
Érdemes száraz, szellős helyen 
folytatni a szárítást. 

46
00:06:47,040 --> 00:06:49,660
Magkinyerés – válogatás – tárolás 

47
00:06:55,207 --> 00:06:58,901
A magok kinyeréséhez dörzsöljük 
a szárakat a kezeink között.

48
00:06:59,738 --> 00:07:01,340
Közben viseljünk kesztyűt. 

49
00:07:04,952 --> 00:07:08,349
Akár ki is taposhatjuk vagy bottal 
is kicsépelhetjük a szárakat. 

50
00:07:15,960 --> 00:07:20,160
Először egy nagyobb lyukú szitán engedjük át a magokat, 
amelyen a növényi törmelékek fennmaradnak,

51
00:07:31,541 --> 00:07:36,490
majd egy kisebb lyukún, amely 
fenntartja a magokat, viszont átengedi a port. 

52
00:07:54,232 --> 00:07:56,060
Végül fújással

53
00:07:56,625 --> 00:08:01,389
vagy a szél segítségével szeleljük ki a magokat,
így távolítva el a maradék törmeléket. 

54
00:08:19,236 --> 00:08:24,780
Mindig írjuk fel egy címkére a fajta és a faj nevét, 
valamint a magfogás évét, majd helyezzük azt a tasak belsejébe.

55
00:08:25,069 --> 00:08:27,221
A külső felirat könnyen letörlődhet. 

56
00:08:34,520 --> 00:08:38,916
Néhány fagyasztóban töltött nap alatt 
a kártevők lárvái elpusztulnak. 

57
00:08:42,090 --> 00:08:48,480
A spenótmagok 5, időnként 7 évig 
őrzik meg a csírázóképességüket.

58
00:08:49,650 --> 00:08:52,552
Ezt úgy tudjuk meghosszabbítani, 
ha a magokat fagyasztóban tároljuk.

59
00:08:54,167 --> 00:08:57,098
Egy gramm körülbelül 100 magot tartalmaz. 
