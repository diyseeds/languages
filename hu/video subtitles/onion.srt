1
00:00:09,820 --> 00:00:13,340
A vöröshagyma az amarilliszfélék 
(Amarylidaceae) családjának tagja,

2
00:00:13,775 --> 00:00:15,655
amit korábban hagymaféléknek (Alliaceae) neveztek.

3
00:00:15,975 --> 00:00:18,610
Az Allium cepa fajba tartozik.

4
00:00:22,395 --> 00:00:26,430
Kétéves növény, amely 
az első évben hagymát fejleszt.

5
00:00:27,745 --> 00:00:31,090
A második évben 
virágzik és magot hoz.

6
00:00:35,585 --> 00:00:38,460
Vannak korai és kései,

7
00:00:38,900 --> 00:00:42,550
hosszabb és rövidebb ideig 
tárolható fajtái.

8
00:00:48,330 --> 00:00:54,910
Az egyes fajták hagymái rendkívül 
különböző alakúak, színűek és ízűek lehetnek.

9
00:01:01,780 --> 00:01:02,600
Megporzás

10
00:01:20,310 --> 00:01:25,540
A vöröshagyma fejecskevirágzata 
kis kétivarú virágokból áll,

11
00:01:27,590 --> 00:01:30,030
amelyek önmeddők.

12
00:01:34,520 --> 00:01:37,320
A megporzáshoz szükségük van a rovarokra:

13
00:01:39,330 --> 00:01:41,995
a vöröshagyma idegentermékenyülő növény.

14
00:01:43,960 --> 00:01:50,020
Ezért fennáll a veszélye a 
keresztbeporzódásnak a különböző fajták közt.

15
00:01:57,180 --> 00:01:59,420
Ennek elkerülése érdekében két vöröshagymafajta

16
00:01:59,555 --> 00:02:04,160
között hagyjunk minimum 
1 km-es izolációs távolságot!

17
00:02:08,230 --> 00:02:14,270
Ezt 200 méterre csökkenthetjük, ha két fajta 
között van valamilyen természetes akadály, például sövény.

18
00:02:18,665 --> 00:02:23,635
Az egyes fajtákat rovarhálóval is 
izolálhatjuk egymástól.

19
00:02:25,185 --> 00:02:30,605
Például az egyiket egy fix rovarhálóval takarhatjuk be, 
és poszméhkaptárat tehetünk alá.

20
00:02:32,160 --> 00:02:35,770
Esetleg a két fajtát külön-külön 
is letakarhatjuk,

21
00:02:36,405 --> 00:02:41,200
majd azokat naponta felváltva 
nyithatjuk, és zárhatjuk.

22
00:02:42,945 --> 00:02:45,740
Bővebb információt ezzel kapcsolatban a

23
00:02:46,010 --> 00:02:48,900
mechanikai izolációs technikákról 
szóló modulban találsz

24
00:02:49,090 --> 00:02:51,000
"A magtermesztés ábécéje" fejezetben.

25
00:03:00,870 --> 00:03:02,020
Életciklus

26
00:03:13,220 --> 00:03:19,560
A magfogás céljából vetett vöröshagymákat 
ugyanúgy termesztjük, mint a téli tárolásra szántakat.

27
00:03:23,600 --> 00:03:27,150
Magot csak a
második évben hoznak.

28
00:03:35,115 --> 00:03:39,495
A magfogásra szánt növényeket 
érdemes inkább magról vetni,

29
00:03:40,370 --> 00:03:46,585
mint dughagymákat vagy sarjhagymákat használni, 
mivel ez utóbbiak hajlamosak túl korán virágozni.

30
00:04:03,565 --> 00:04:08,315
Minden vöröshagymát el kell távolítani, 
amelyik már az első évben virágot hoz,

31
00:04:09,155 --> 00:04:14,460
mert a magjából nevelt növények 
gyakran szintén túl hamar virágzanak.

32
00:04:20,015 --> 00:04:26,305
Első évben a vöröshagymának magas hőmérsékletre 
és hosszú nappalokra van szüksége a hagyma létrehozásához.

33
00:04:34,025 --> 00:04:37,535
A vöröshagymákat be kell takarítani 
mihelyt a hagymájuk teljesen kifejlődött.

34
00:04:46,740 --> 00:04:51,310
Hogy tavasszal 15-20 hagymát 
vissza tudjunk ültetni,

35
00:04:51,860 --> 00:04:57,970
20-30 növényt hagyjunk meg ősszel, 
mert a tél folyamán gyakoriak a veszteségek!

36
00:05:02,495 --> 00:05:06,720
A hagymákat az adott fajta 
egyedi tulajdonságai alapján válasszuk ki,

37
00:05:06,990 --> 00:05:09,660
mint az alak, szín és méret!

38
00:05:13,035 --> 00:05:17,030
Továbbá legyenek egészségesek, 
és legyen szép, sértetlen héjuk.

39
00:05:21,025 --> 00:05:25,800
Szabaduljunk meg a nem homogén hagymáktól,
illetve amelyek több részből állnak!

40
00:05:30,700 --> 00:05:36,575
A hagymákat 10-12 napig hagyjuk 
száradni meleg, szellős helyen.

41
00:05:37,600 --> 00:05:42,540
Ne sértsük meg a hagymák héját, 
mert ez a tél során rothadáshoz vezethet!

42
00:05:46,340 --> 00:05:52,195
A hagymák téli tárolásához hideg, 
jól szellőző helyre tegyük őket.

43
00:05:56,810 --> 00:06:02,175
A tél folyamán rendszeresen ellenőrizzük 
a hagymákat, és távolítsuk el a sérülteket!

44
00:06:06,840 --> 00:06:12,630
Minden fajtának eltérő hosszúságú a nyugalmi időszaka. 
Van olyan, amelyiknek hosszabb,

45
00:06:13,335 --> 00:06:17,880
de 12°C-os hőmérsékleten mindegyiké véget ér.

46
00:06:21,150 --> 00:06:23,380
A következő év tavaszán ültessük

47
00:06:23,895 --> 00:06:29,040
a hagymákat egymástól 20 cm-re, 
de ne temessük el őket túl mélyre!

48
00:07:42,680 --> 00:07:48,160
A növények 1-3 magszárat fejlesztenek, 
amik 1 méternél is magasabbra nőhetnek.

49
00:07:51,735 --> 00:07:55,675
Az eldőlés megelőzése érdekében 
karózzuk ki őket!

50
00:07:57,915 --> 00:07:59,900
Négy hét alatt az összes

51
00:08:00,165 --> 00:08:04,620
fejecskevirágzatban található 
különálló virág kinyílik.

52
00:08:06,075 --> 00:08:09,020
A virágzás kezdetétől a 
magok teljes éréséig

53
00:08:09,310 --> 00:08:11,390
tartó idő elég hosszú.

54
00:08:14,720 --> 00:08:19,420
A magok akkor érettek, amikor a tokok elszáradtak, 
és fekete magok láthatóvá válnak.

55
00:08:28,230 --> 00:08:32,470
A magok begyűjtéséhez 
vágjuk le a virágzatot a szár felső részével együtt,

56
00:08:33,335 --> 00:08:39,220
helyezzük egy szövetzsákba, szellős, 
meleg helyen hagyjuk tovább száradni!

57
00:08:43,780 --> 00:08:45,540
Hideg és párás vidékeken

58
00:08:45,800 --> 00:08:52,310
az eső és a szél miatt a magok kipereghetnek, 
ezért hamarabb be kell takarítani a növényeket.

59
00:08:53,650 --> 00:08:57,940
Ebben az esetben az egész növényt szedjük 
ki gyökerestül még a magok teljes érése előtt,

60
00:08:58,410 --> 00:09:00,615
majd száraz helyen hagyjuk utóérni.

61
00:09:06,925 --> 00:09:09,620
Magfogás - tisztítás - tárolás

62
00:09:22,460 --> 00:09:25,260
Először dörzsöljük össze a 
fejecskevirágzatokat a tenyereink között,

63
00:09:25,335 --> 00:09:27,650
majd zúzzuk össze őket sodrófával!

64
00:09:42,615 --> 00:09:47,310
Tisztításhoz használjunk apró lyukú szitát,
amin a magok fennmaradnak, a por pedig átesik!

65
00:10:02,790 --> 00:10:05,890
A könnyű növényi törmeléket 
szeleléssel távolítsuk el!

66
00:10:06,305 --> 00:10:11,450
Használhatjuk a szelet, egy kis ventillátort,
 vagy mi magunk is ráfújhatunk a magokra.

67
00:10:17,230 --> 00:10:20,620
Ezután öntsük a magokat hideg vízbe, és kavarjuk meg!

68
00:10:26,270 --> 00:10:29,855
A termékeny, nehezebb 
magok lesüllyednek.

69
00:10:33,625 --> 00:10:36,885
A lebegő léha magokat, és a törmeléket távolítsuk el,

70
00:10:44,540 --> 00:10:46,430
a jó magokat szárítsuk meg egy tányéron!

71
00:10:49,715 --> 00:10:52,625
Miután megszáradtak, úgy peregnek, mint a homok.

72
00:10:59,880 --> 00:11:03,015
Mindig írjuk fel egy címkére a fajta és 
a faj nevét, valamint

73
00:11:03,080 --> 00:11:09,530
a magfogás évét, majd helyezzük azt 
a tasak belsejébe! A külső felirat könnyen letörlődhet.

74
00:11:20,040 --> 00:11:25,340
Néhány fagyasztóban töltött nap alatt 
valamennyi kártevő lárvája elpusztul.

75
00:11:26,480 --> 00:11:29,940
A vöröshagymamagok 2 évig őrzik 
meg a csírázóképességüket.

76
00:11:30,290 --> 00:11:33,180
Ezt akár 4-5 évig is meghosszabbíthatjuk.

77
00:11:33,855 --> 00:11:37,340
Mivel ezek a magok 
rövid időn belül elvesztik a csírázóképességüket,

78
00:11:37,815 --> 00:11:41,230
ezen időszak meghosszabbításához 
tároljuk őket fagyasztóban!
