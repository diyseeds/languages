﻿1
00:00:08,650 --> 00:00:10,380
Izolációs technikák

2
00:00:17,140 --> 00:00:19,620
Ha fennáll a keresztbeporzódás veszélye

3
00:00:19,685 --> 00:00:22,620
egy adott faj 
különböző fajtái között,

4
00:00:23,420 --> 00:00:25,700
izolálni kell őket egymástól,

5
00:00:25,850 --> 00:00:30,100
hogy megőrizzük az egyes fajták 
egyedi tulajdonságait.

6
00:00:30,690 --> 00:00:33,750
A fajták izolálásához több 
módszer is rendelkezésre áll.

7
00:00:39,485 --> 00:00:41,180
Térbeli izoláció 

8
00:00:45,240 --> 00:00:51,820
A legegyszerűbb módszer az, ha minden 
fajnak csak egy fajtáját termesztjük egy parcellán.

9
00:00:52,820 --> 00:00:56,300
A többi fajtát pedig olyan 
távol termesztjük ettől,

10
00:00:56,490 --> 00:01:00,955
ami biztosítja, hogy ne fordulhasson elő 
keresztbeporzódás közöttük.

11
00:01:02,045 --> 00:01:05,230
Az izolációs távolságot minden növényfajnál feltüntetjük.

12
00:01:06,670 --> 00:01:11,100
Ez függ környezeti tényezőktől és a 
megporzás módjától is.

13
00:01:13,750 --> 00:01:18,580
Ha a megporzást rovarok végzik, 
néhány száz méter elég lehet. 

14
00:01:20,780 --> 00:01:24,020
Viszont a szélporozta növények 
esetében sokkal nagyobb

15
00:01:24,500 --> 00:01:29,020
izolációs távolságra 
van szükség a különböző fajták között. 

16
00:01:29,325 --> 00:01:33,420
Egy rendkívül sűrű sövény 
nagyban csökkentheti a szél

17
00:01:33,780 --> 00:01:36,660
vagy a rovarok általi keresztbeporzás kockázatát. 

18
00:01:37,820 --> 00:01:43,020
A magtermesztésre szánt növények közvetlen 
közelében található nagy mennyiségű virág

19
00:01:43,140 --> 00:01:48,540
magához csalogathatja a rovarokat, így 
megelőzhető, hogy keresztbeporozzák az adott fajtát. 

20
00:01:59,600 --> 00:02:01,140
Időbeli izoláció 

21
00:02:01,880 --> 00:02:07,940
Adott növényfaj két különböző fajtájának 
termesztési idejét eltolhatjuk egymástól.

22
00:02:08,770 --> 00:02:11,660
Mivel így a növények nem egyszerre virágoznak,

23
00:02:11,940 --> 00:02:14,220
a keresztbeporzás nem történhet meg.

24
00:02:20,090 --> 00:02:25,300
Például ha két fejes saláta fajtát 
ugyanazon a helyen termesztünk,

25
00:02:25,880 --> 00:02:31,725
az első, sokkal korábban vetett fajta virágzásának 
kezdetén a másik még csak fiatal növény.

26
00:02:32,940 --> 00:02:38,915
Ehhez természetesen ismernünk kell a növény 
életciklusát, valamint virágzási idejét. 

27
00:02:41,975 --> 00:02:43,820
Mechanikai izoláció 

28
00:02:45,550 --> 00:02:50,960
A rovarháló felbecsülhetetlen segítség 
a biztonságos magtermesztéshez.

29
00:02:52,560 --> 00:02:58,220
Nagyon finom szövésű hálót kell választani 
a rovarok és az aktuális cél függvényében,

30
00:02:59,045 --> 00:03:04,620
vagyis, hogy a rovarokat kívül 
vagy belül akarjuk-e tartani.

31
00:03:07,320 --> 00:03:09,975
A hálókat sokféleképpen alkalmazhatjuk. 

32
00:03:11,670 --> 00:03:14,035
Egyetlen virágot csak akkor takarjunk le

33
00:03:14,390 --> 00:03:18,340
ha kétivarú, önbeporzó 
virágról van szó

34
00:03:18,795 --> 00:03:21,230
és csak kis mennyiségű magot szeretnénk fogni.

35
00:03:22,295 --> 00:03:26,700
A még ki nem nyílt virágot 
kis hálóból készült zsákkal takarjuk le,

36
00:03:26,910 --> 00:03:30,340
és úgy kössük meg, hogy egy rovar se tudjon bemenni.

37
00:03:34,585 --> 00:03:37,700
Miután a virág elszáradt,
a zsákot távolítsuk el,

38
00:03:37,830 --> 00:03:40,590
hogy a termés zavartalanul fejlődhessen!

39
00:03:42,075 --> 00:03:46,050
Ne felejtsük el megjelölni 
a magfogásra szánt termést! 

40
00:03:55,890 --> 00:04:02,585
Ezt a módszert nagyobb zsákkal is alkalmazhatjuk, 
amivel egy ágat vagy egy egész növényt betakarhatunk.

41
00:04:04,800 --> 00:04:09,260
Először azonban távolítsuk el 
a már kinyílt virágokat,

42
00:04:09,730 --> 00:04:14,060
mert fennáll a veszélye annak, 
hogy a rovarok már beporozták őket! 

43
00:04:19,900 --> 00:04:22,780
Nagy mennyiségű mag előállításához

44
00:04:22,860 --> 00:04:26,380
hatékony módszer, ha hálós 
sátrakat vagy alagutakat használunk. 

45
00:05:29,540 --> 00:05:32,180
A kétivarú önbeporzó növényeket

46
00:05:32,320 --> 00:05:35,940
közös rovarháló-alagút 
alatt termeszthetjük,

47
00:05:36,060 --> 00:05:37,620
hogy megvédjük őket a rovaroktól. 

48
00:05:40,540 --> 00:05:45,700
Az idegentermékenyülő növények 
megporzását rovarok végzik,

49
00:05:46,340 --> 00:05:51,820
esetükben poszméhkaptárakat 
helyezhetünk az alagútba a beporzás érdekében.

50
00:05:54,610 --> 00:05:58,620
Ebben az esetben fajonként 
csak egy fajta termeszthető.

51
00:05:59,085 --> 00:06:02,020
A kaptár árának megtérüléséhez

52
00:06:02,230 --> 00:06:06,420
érdemes nagyméretű alagutat használni,
 amelyben sok magtermő növény elfér. 

53
00:06:08,050 --> 00:06:10,540
Hálós alagutak váltott használata

54
00:06:10,780 --> 00:06:14,740
a megoldás, ha egy 
idegentermékenyülő faj

55
00:06:14,900 --> 00:06:18,980
különböző fajtáiról szeretnénk 
magot fogni egy kertben,

56
00:06:19,940 --> 00:06:23,220
például 
két különböző cukkinifajta estén.

57
00:06:24,120 --> 00:06:25,900
Poszméhkaptárak nélkül ilyenkor

58
00:06:26,350 --> 00:06:30,980
a faj egyik fajtáját egy hálóalagút 
alatt termesztjük,

59
00:06:31,580 --> 00:06:36,520
a másik fajtát pedig 
egy másik hálóalagút alatt.

60
00:06:38,570 --> 00:06:40,060
A virágok kinyílásától kezdve

61
00:06:40,720 --> 00:06:43,780
felváltva nyissuk ki a hálókat.

62
00:06:44,880 --> 00:06:51,610
Az első nap kinyitjuk az első alagutat, 
miközben a második zárva marad.

63
00:06:52,810 --> 00:06:55,335
A második nap minden fordítva történik.

64
00:06:56,990 --> 00:06:59,800
Az alagutakat már kora reggel 
ki kell nyitni,

65
00:07:00,190 --> 00:07:02,860
mivel nyáron a rovarok már korán 
megkezdik a munkájukat,

66
00:07:03,225 --> 00:07:07,380
bezárni pedig akkor kell őket, amikor már 
nem hallunk rovarzúgást a növények között.

67
00:07:08,575 --> 00:07:09,740
Ezzel a módszerrel

68
00:07:10,030 --> 00:07:12,835
a megtermékenyített 
nőivarú virágok száma,

69
00:07:13,030 --> 00:07:15,805
ezáltal a termések száma is kisebb.

70
00:07:17,140 --> 00:07:19,900
Mindazonáltal a természet rendkívül bőkezű,

71
00:07:20,215 --> 00:07:23,980
és így is rengeteg magot gyűjthetünk 
az elkövetkezendő évekre. 
