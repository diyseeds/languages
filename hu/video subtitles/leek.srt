1
00:00:10,756 --> 00:00:15,780
A póréhagyma az amarilliszfélék (Amarylidaceae), a 
korábbi hagymafélék (Alliaceae) családjának tagja.

2
00:00:16,320 --> 00:00:19,614
Az Allium ampeloprasum fajba tartozik.

3
00:00:20,970 --> 00:00:27,250
Kétéves növény, melyet az első évben 
növesztett hosszú fehér száráért termesztünk.

4
00:00:31,160 --> 00:00:33,170
Számos fajtája létezik.

5
00:00:35,818 --> 00:00:39,454
A levelei lehetnek zöldek, kékek vagy akár lilák is.

6
00:00:44,210 --> 00:00:48,480
Egyes fajtákat a vegetációs idő elején 
termesztenek, másokat később.

7
00:00:53,250 --> 00:00:56,458
Egyesek a nagyon alacsony 
hőmérsékletet is kibírják,

8
00:00:57,389 --> 00:00:59,861
de vannak kevésbé télállóak is. 

9
00:01:12,440 --> 00:01:13,680
Megporzás 

10
00:01:24,669 --> 00:01:29,621
A póréhagyma fejecskevirágzata 
kis kétivarú virágokból áll,

11
00:01:31,752 --> 00:01:33,970
amelyek önmeddők.

12
00:01:39,047 --> 00:01:41,781
A megporzáshoz szükségük van a rovarokra.

13
00:01:47,330 --> 00:01:50,065
Vagyis a póréhagyma idegentermékenyülő növény. 

14
00:01:54,203 --> 00:01:59,105
A rovarok a póréhagyma különböző fajtáit, 
és a vad póréhagymát is

15
00:01:59,389 --> 00:02:00,792
keresztbeporozhatják egymással.

16
00:02:06,894 --> 00:02:13,854
Viszont nem kereszteződik a vöröshagymával, a 
metélőhagymával, és a fekete fokhagymával sem. 

17
00:02:18,843 --> 00:02:23,730
A keresztbeporzódás elkerülése érdekében 
két fajta között hagyjunk minimum

18
00:02:23,840 --> 00:02:26,087
1 km-es izolációs távolságot! 

19
00:02:29,374 --> 00:02:35,396
Ezt 400 méterre csökkenthetjük, ha van köztük 
valamilyen természetes akadály, pl. sövény. 

20
00:02:40,145 --> 00:02:44,669
Az egyes fajtákat rovarhálóval 
is izolálhatjuk egymástól.

21
00:02:44,909 --> 00:02:49,840
Például az egyiket egy fix rovarhálóval takarhatjuk be, 
és poszméhkaptárat tehetünk alá.

22
00:02:51,352 --> 00:02:54,283
Esetleg a két fajtát külön-külön is 
letakarhatjuk

23
00:02:54,620 --> 00:03:00,356
egy-egy rovarhálóval, majd azokat 
naponta felváltva nyithatjuk, és zárhatjuk. 

24
00:03:01,640 --> 00:03:04,130
Bővebb információt ezzel kapcsolatban a

25
00:03:04,280 --> 00:03:07,280
mechanikai izolációs technikákról 
szóló modulban találsz

26
00:03:07,643 --> 00:03:09,963
A magtermesztés ábécéjében. 

27
00:03:15,156 --> 00:03:16,247
Életciklus 

28
00:03:28,807 --> 00:03:33,934
A magfogásra vetett növényeket az első évben 
úgy kezeljük, mint a fogyasztásra nevelteket.

29
00:03:38,945 --> 00:03:41,621
Magot a második évben hoznak. 

30
00:04:33,578 --> 00:04:37,025
Szabaduljunk meg azoktól, amelyek 
az első évben virágot hoznak,

31
00:04:40,130 --> 00:04:44,341
mert utódaik szintén 
túl korán virágoznának! 

32
00:04:48,120 --> 00:04:51,585
Többféle módszer is létezik a magfogásra 
szánt póréhagymák tárolására. 

33
00:04:59,178 --> 00:05:04,298
Fagyos vidékeken szedjük fel 
gyökerestül a növényeket a fagyok előtt!

34
00:05:14,240 --> 00:05:17,381
Fontos, hogy azokat az 
egyedeket válasszuk ki

35
00:05:17,505 --> 00:05:20,887
magfogásra, amelyek hordozzák 
az adott fajta jellegzetes tulajdonságait.

36
00:05:21,310 --> 00:05:26,480
Ezek lehetnek: rövid tenyészidő, 
szár mérete és vastagsága,

37
00:05:27,090 --> 00:05:32,087
a levelek színe, fagytűrés, illetve a 
betegségekkel szembeni ellenálló képesség.

38
00:05:33,883 --> 00:05:38,407
Válasszunk ki 30 növényt, nem biztos, 
hogy mindegyiket sikerül átteleltetni!

39
00:05:44,632 --> 00:05:49,607
Hűvös, hidegtől védett helyen 
tegyük őket egy tárolóba!

40
00:05:51,040 --> 00:05:55,730
A genetikai sokféleség biztosításához 
minimum 20 növényre van szükség.

41
00:06:00,000 --> 00:06:05,505
Egyes „nyári póréhagymák” jobban áttelelnek, 
ha ezt a megoldást alkalmazzuk. 

42
00:06:08,967 --> 00:06:12,130
A legegyszerűbb az, ha 
elég meleg az időjárás,

43
00:06:12,414 --> 00:06:14,821
és a kertben hagyhatjuk áttelelni a póréhagymákat. 

44
00:06:20,727 --> 00:06:26,040
Tavasz elején ültessük ki 
a tél során eltárolt póréhagymákat!

45
00:06:54,080 --> 00:06:57,890
A levelek visszavágásával biztosíthatjuk 
a növények megfelelő növekedését. 

46
00:07:36,960 --> 00:07:40,980
A virágszárak 1,5 méteresre 
vagy akár magasabbra is nőhetnek.

47
00:07:56,894 --> 00:07:59,163
Ezért ki kell karózni őket. 

48
00:08:18,741 --> 00:08:22,298
Négy hét leforgása alatt az 
összes fejecskevirágzatban

49
00:08:22,421 --> 00:08:24,923
található különálló virág kinyílik.

50
00:08:27,970 --> 00:08:33,570
A virágzás kezdetétől a magok 
teljes éréséig tartó idő elég hosszú. 

51
00:08:37,960 --> 00:08:43,221
A magok akkor érettek, amikor a tokok 
elszáradtak, és fekete magok láthatóvá válnak. 

52
00:08:49,970 --> 00:08:54,967
A magok begyűjtéséhez vágjuk le 
a virágzatot a szár felső részével együtt,

53
00:08:55,512 --> 00:09:01,607
helyezzük egy szövetzsákba, majd szellős, 
meleg helyen hagyjuk tovább száradni! 

54
00:09:03,716 --> 00:09:09,170
A hideg és párás vidékeken, ahol a szél és az eső 
tönkreteheti a már beérett magokat,

55
00:09:10,036 --> 00:09:15,418
takarítsuk be a növényeket még a teljes érés előtt, 
majd száraz helyen hagyjuk őket utóérni! 

56
00:09:28,436 --> 00:09:31,660
Magfogás - tisztítás - tárolás 

57
00:09:36,727 --> 00:09:39,607
A póréhagymamagok kinyerése, 
tisztítása és tárolása

58
00:09:39,767 --> 00:09:41,941
ugyanúgy történik, mint a vöröshagyma esetében. 

59
00:09:55,534 --> 00:09:58,310
Először dörzsöljük össze a fejecskevirágzatokat 
a tenyereink között,

60
00:09:58,443 --> 00:10:00,589
majd zúzzuk össze őket sodrófával! 

61
00:10:05,781 --> 00:10:11,396
A beragadt magok kinyeréséhez 
a tokokat tegyük néhány órára a fagyasztóba!

62
00:10:11,789 --> 00:10:13,272
Ezután könnyebben kinyerhetőek a magok. 

63
00:10:20,920 --> 00:10:26,225
A tisztításhoz használjunk apró lyukú szitát, 
amin a magok fennmaradnak, a por pedig átesik. 

64
00:10:42,550 --> 00:10:46,298
A könnyű növényi törmeléket 
szeleléssel távolítsuk el!

65
00:10:47,556 --> 00:10:52,843
Használhatjuk a szelet, egy kis ventillátort,
 vagy mi magunk is ráfújhatunk a magokra. 

66
00:10:57,018 --> 00:11:00,320
Ezután öntsük a magokat hideg vízbe, és kavarjuk meg!

67
00:11:05,381 --> 00:11:08,880
A termékeny, nehezebb 
magok lesüllyednek.

68
00:11:11,905 --> 00:11:15,054
A lebegő léha magokat, és a törmeléket távolítsuk el,

69
00:11:22,814 --> 00:11:24,676
a jó magokat szárítsuk meg egy tányéron!

70
00:11:27,570 --> 00:11:30,574
Miután megszáradtak, úgy peregnek, mint a homok. 

71
00:11:38,530 --> 00:11:41,752
Mindig írjuk fel egy címkére a fajta 
és a faj nevét, valamint

72
00:11:41,912 --> 00:11:45,200
a magfogás évét, majd helyezzük azt a tasak belsejébe!

73
00:11:45,607 --> 00:11:47,800
A külső felirat könnyen letörlődhet. 

74
00:11:51,120 --> 00:11:55,527
Néhány fagyasztóban töltött nap 
alatt valamennyi kártevő lárvája elpusztul. 

75
00:12:02,523 --> 00:12:06,487
A póréhagymamagok 2 évig 
őrzik meg a csírázóképességüket.

76
00:12:07,250 --> 00:12:10,240
Ezt sikerülhet akár 6 évre is meghosszabbítani.

77
00:12:13,180 --> 00:12:16,647
Mivel ezek a magok gyorsan 
elvesztik a csírázóképességüket,

78
00:12:17,140 --> 00:12:20,465
ezen időszak meghosszabbításához 
tároljuk őket fagyasztóban! 
