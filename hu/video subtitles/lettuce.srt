﻿1
00:00:10,210 --> 00:00:13,300
A fejes saláta a fészkesek (Asteraceae) családjának tagja.

2
00:00:13,700 --> 00:00:15,980
A faj latin neve: Lactuca sativa.

3
00:00:16,460 --> 00:00:19,550
Négy fő típusa létezik:

4
00:00:21,505 --> 00:00:26,300
- a fejes saláta, amelybe 
beletartozik a kevéssé szeldelt,

5
00:00:26,420 --> 00:00:28,060
sima levelű vajsaláta és

6
00:00:30,905 --> 00:00:35,860
a ropogósabb, többé-kevésbé 
szeldelt levelű batávia saláta,

7
00:00:38,845 --> 00:00:43,060
- a kötözősaláta (római saláta), 
melynek téglalap alakú, hosszú levelei vannak, 

8
00:00:45,975 --> 00:00:49,100
- a tépősaláta, amely általában 
nem növeszt fejet,

9
00:00:49,540 --> 00:00:53,620
bizonyos fajtáinak nagyon 
fodros a levélszéle, 

10
00:00:58,260 --> 00:01:03,660
- a spárgasaláta vagy szársaláta, 
amelyet főként Ázsiában

11
00:01:04,180 --> 00:01:07,700
termesztenek a lédús száráért
 és a zsenge leveleiért. 

12
00:01:13,995 --> 00:01:16,580
A saláták fontos tulajdonsága, hogy képesek

13
00:01:16,805 --> 00:01:20,400
jól alkalmazkodni az éghajlathoz és 
az évszakokhoz.

14
00:01:28,140 --> 00:01:31,985
Vannak olyan fajták, amelyek különösen jól 
viselik a téli hideget vagy a

15
00:01:32,880 --> 00:01:35,820
hőhullámokat, vagy csak később mennek magszárba. 

16
00:01:39,440 --> 00:01:42,460
Mindenfelé találhatunk a helyi 
éghajlathoz alkalmazkodott fajtákat. 

17
00:01:50,510 --> 00:01:51,540
Megporzás 

18
00:02:00,395 --> 00:02:03,250
A saláta virágzata fészekvirágzat.

19
00:02:07,010 --> 00:02:10,060
Kétivarú önbeporzó virágokból áll,

20
00:02:11,450 --> 00:02:15,820
vagyis a hímivarú és a nőivarú szaporítószervek 
ugyanabban a virágban találhatóak,

21
00:02:16,890 --> 00:02:18,580
és kompatibilisek egymással.

22
00:02:20,540 --> 00:02:23,300
Ezért a virág öntermékenyülőnek tekinthető. 

23
00:02:26,800 --> 00:02:31,820
A különböző fajták között fennáll a rovarok 
általi keresztbeporzás veszélye;

24
00:02:35,095 --> 00:02:38,035
annál inkább, minél melegebb a terület éghajlata. 

25
00:02:44,640 --> 00:02:46,260
Egyes területeken élnek vad

26
00:02:46,615 --> 00:02:50,980
változatok is, melyek szintén 
összekereszteződhetnek a termesztett fajtákkal. 

27
00:02:58,400 --> 00:02:59,660
Ennek elkerülése érdekében

28
00:03:00,010 --> 00:03:05,140
mérsékelt éghajlaton a különböző fajtákat 
legalább néhány méter távolságra

29
00:03:05,365 --> 00:03:06,740
temesszük egymástól,

30
00:03:07,205 --> 00:03:10,155
a melegebb vidékeken hagyjunk nagyobb izolációs távolságot! 

31
00:03:12,500 --> 00:03:16,755
A fajtákat rovarháló segítségével 
is izolálhatjuk egymástól.

32
00:03:17,540 --> 00:03:18,590
Bővebb információt ezzel

33
00:03:18,635 --> 00:03:24,140
kapcsolatban az izolációs technikákról 
szóló modulban találsz A magtermesztés ábécéjében. 

34
00:03:32,845 --> 00:03:36,940
A különböző fajták ültetési idejének 
eltolásával is megelőzhetjük

35
00:03:37,105 --> 00:03:39,595
azt, hogy egyszerre virágozzanak.

36
00:03:41,680 --> 00:03:42,700
Fontos, hogy

37
00:03:42,945 --> 00:03:47,915
a magokat időben elvessük, és elegendő idő 
álljon rendelkezésre a növények számára a maghozáshoz. 

38
00:04:02,815 --> 00:04:03,940
Életciklus 

39
00:04:32,855 --> 00:04:37,630
A magfogásra vetett salátákat ugyanúgy termesztjük, 
mint a fogyasztási célból nevelteket. 

40
00:04:53,290 --> 00:04:59,100
A genetikai sokféleség biztosításához minimum 
12 egyedet válasszunk a magfogáshoz! 

41
00:05:02,990 --> 00:05:05,940
Olyan növényeket válasszunk ki, amelyek

42
00:05:06,080 --> 00:05:10,340
rendelkeznek a fajta jellegzetes 
tulajdonságaival, mint a forma,

43
00:05:10,915 --> 00:05:13,540
a szín, illetve a tenyészidőszak.

44
00:05:14,520 --> 00:05:19,265
Ha nem figyelünk oda, 
idővel elveszítjük ezeket a tulajdonságokat! 

45
00:05:21,005 --> 00:05:23,530
A fejes saláta tömör levelekből álló fejet növeszt.

46
00:05:24,345 --> 00:05:27,880
A batávia salátának ropogós szeldelt levelei vannak. 

47
00:05:28,160 --> 00:05:29,780
Ahogyan az a nevéből is látszik,

48
00:05:29,950 --> 00:05:33,660
a téli fejes salátát az év hideg 
időszakában termesztik,

49
00:05:34,050 --> 00:05:36,660
magot pedig a következő év tavaszán hoz.

50
00:05:37,205 --> 00:05:40,815
Így megőrzi azt a tulajdonságát, 
hogy kibírja az alacsony hőmérsékletet. 

51
00:05:43,955 --> 00:05:48,185
A fajta jellegzetes tulajdonságaival 
nem rendelkező egyedeket el kell távolítani.

52
00:05:56,960 --> 00:06:00,380
Továbbá azokat a salátákat is ki kell 
szedni, amelyek idő előtt

53
00:06:00,560 --> 00:06:03,465
magszárba mennek, vagy 
nem fejezték be a fejlődési ciklusukat.

54
00:06:03,850 --> 00:06:06,525
E növények magjaiból gyenge saláták fejlődnének. 

55
00:06:10,190 --> 00:06:15,220
Egyes fajták esetében a magszár nehezen tud 
keresztülnőni a fejen, különösen,

56
00:06:15,690 --> 00:06:18,405
ha az rendkívül tömör.

57
00:06:19,480 --> 00:06:24,540
Úgy segíthetünk a növényeknek, ha 
óvatosan bemetsszük a fej tetejét úgy,

58
00:06:24,820 --> 00:06:27,660
hogy közben az érzékeny hajtásrügyet nem sértjük meg.

59
00:06:40,120 --> 00:06:45,345
Vagy el is távolíthatjuk egyesével 
a fejet körülvevő leveleket.

60
00:06:55,740 --> 00:06:58,900
A levelek nedves időjárás esetén hajlamosak a rothadásra.

61
00:06:59,805 --> 00:07:02,620
Ha ez megtörténik, szintén el kell távolítani őket. 

62
00:07:52,320 --> 00:07:55,705
A virágzó saláta átlagosan
 1 méteres virágszárat növeszt,

63
00:07:56,000 --> 00:07:59,540
ezért azt egyesével vagy csoportosan ki kell karózni. 

64
00:08:05,160 --> 00:08:07,500
A környezeti feltételek függvényében

65
00:08:07,680 --> 00:08:13,260
a virágzást követően a magoknak 
12-24 napra van szükségük a kifejlődéshez.

66
00:08:16,740 --> 00:08:20,020
A fejes saláta virágzatai fokozatosan nyílnak,

67
00:08:20,675 --> 00:08:24,220
ezért a magok nem egyszerre érnek be.

68
00:08:28,975 --> 00:08:34,260
Egyszerre találhatunk rügyeket, virágokat 
és magokat is ugyanazon a növényen. 

69
00:08:41,050 --> 00:08:45,420
A magok érettségét úgy ellenőrizhetjük, 
ha egy elhervadt fészekvirágzatot leszedünk,

70
00:08:45,740 --> 00:08:49,005
majd azt elmorzsoljuk a 
hüvelyk- és a mutatóujjunk között.

71
00:08:49,490 --> 00:08:53,740
Ha a magok fészekvirágzatba 
ragadva maradnak, nem esnek ki,

72
00:08:53,910 --> 00:08:55,380
még tovább kell érniük. 

73
00:08:57,380 --> 00:09:02,500
Ha a magok könnyen kiesnek a fészekvirágzatból, 
akkor készen állnak a betakarításra. 

74
00:09:06,190 --> 00:09:09,640
A legjobb minőségű magok 
a központi virágszáron találhatóak. 

75
00:09:19,715 --> 00:09:23,815
Az időjárási körülmények nagyban 
befolyásolják a magok betakarítását.

76
00:09:24,040 --> 00:09:27,580
Az esős, párás idő negatívan hat a magok fejlődésére,

77
00:09:27,940 --> 00:09:33,070
és a túl nagy nedvességtartalom miatt 
a magok fogékonyabbak a gombabetegségekre. 

78
00:09:39,975 --> 00:09:44,605
Egyes területeken a magfogásra szánt 
egyedeket védett helyen kell termeszteni. 

79
00:09:48,615 --> 00:09:52,070
A magokat három különböző módon takaríthatjuk be. 

80
00:09:53,520 --> 00:09:59,955
Az első érett fészekvirágzatok betakarításával 
legalább egy kis mennyiségű mag begyűjtését biztosíthatjuk. 

81
00:10:00,445 --> 00:10:03,700
Rakjunk egy vödröt, zacskót vagy lepedőt

82
00:10:03,850 --> 00:10:08,300
a fészekvirágzat alá, majd 
rázzuk meg, hogy a magok kihulljanak belőle. 

83
00:10:13,340 --> 00:10:17,740
Akár azt is megvárhatjuk, amíg 
a fészekvirágzatok legalább a fele beérik.

84
00:10:18,040 --> 00:10:22,700
Ekkor levágjuk, majd egy nagy 
szövött zsákba helyezzük a virágszárakat.

85
00:10:28,640 --> 00:10:33,580
A zsákot akasszuk fel védett, 
jól szellőző, száraz helyen!

86
00:10:34,580 --> 00:10:37,620
Így a magok utóérhetnek a növényen. 

87
00:10:46,315 --> 00:10:49,740
Ha az érés során 
kedvezőtlen az időjárás,

88
00:10:50,155 --> 00:10:53,725
harmadik megoldásként a növényeket 
gyökerestül is felszedhetjük.

89
00:10:54,355 --> 00:10:59,500
Ha zacskót húzunk a gyökerekre, akkor megelőzhetjük, 
hogy a talaj és a kövek összekeveredjenek a magokkal.

90
00:11:00,020 --> 00:11:04,355
Végül a növényeket akasszuk fel fejjel lefelé 
száraz, szellős helyen!

91
00:11:05,970 --> 00:11:08,725
A magok jelentős része 
utóérik a növényen. 

92
00:11:14,865 --> 00:11:17,740
Magfogás – tisztítás - tárolás

93
00:11:23,665 --> 00:11:27,900
A magfogáskor a virágszáraknak teljesen 
száraznak kell lenniük.

94
00:11:29,235 --> 00:11:31,500
Ha a fészekvirágzatokat kézzel összedörzsöljük,

95
00:11:31,785 --> 00:11:34,535
a magok többsége könnyedén kipereg belőle.

96
00:11:38,525 --> 00:11:42,140
A be nem porzott virágokból léha magok fejlődnek.

97
00:11:48,630 --> 00:11:53,470
A virágszárakat ki is csépelhetjük 
egy ládában vagy nagyobb tárolóban. 

98
00:11:58,150 --> 00:12:02,780
A magokat különböző lyukméretű
 szitákkal válogatjuk ki. 

99
00:12:16,525 --> 00:12:20,535
Végül a magokat kiszeleljük, hogy a maradék 
törmeléket is eltávolítsuk.

100
00:12:21,500 --> 00:12:23,260
Ehhez a magokat egy tányérra vagy

101
00:12:23,390 --> 00:12:29,020
egy szeleléshez használt kosárra helyezzük, majd 
rájuk fújunk, hogy a könnyű növényi törmelék elrepüljön. 

102
00:12:30,940 --> 00:12:32,505
Kihasználhatjuk a légáramlatokat is.

103
00:12:33,230 --> 00:12:37,420
A szél elfújja a törmelékeket, ha a 
magokat egy földre helyezett

104
00:12:37,545 --> 00:12:39,300
tárolóba öntjük a szabadban.

105
00:12:40,745 --> 00:12:44,460
Változó szélsebesség esetén
 fektessünk egy lepedőt a tárolóedény alá! 

106
00:12:53,450 --> 00:12:56,260
Ezután a magokat egy műanyag zacskóba öntjük,

107
00:12:56,670 --> 00:13:01,580
amelybe egy címkét teszünk a 
faj és a fajta nevével,

108
00:13:01,900 --> 00:13:04,870
a magfogás évével és 
a termesztés helyszínével. 

109
00:13:08,535 --> 00:13:13,005
Néhány fagyasztóban töltött nap
 alatt a kártevők lárvái elpusztulnak. 

110
00:13:14,700 --> 00:13:17,980
A saláta magjai átlagosan 5 évig őrzik 
meg a csírázóképességüket, de akár

111
00:13:18,210 --> 00:13:21,700
9 évre is meghosszabbíthatjuk, ha fagyasztóban tároljuk őket.

112
00:13:26,430 --> 00:13:32,260
Ha a tárolási körülmények nem megfelelőek, 
a saláta magjai gyorsan elvesztik az életképességüket.

113
00:13:34,550 --> 00:13:39,750
Egy jól termő növényről akár 
10-15 gramm magot is begyűjthetünk. 
