﻿1
00:00:08,654 --> 00:00:11,840
A leveles kel a káposztafélék (Brassicaceae) családjába,

2
00:00:12,480 --> 00:00:20,072
a Brassica oleracea fajba, 
az acephala alfajba tartozik.

3
00:00:20,760 --> 00:00:26,894
Szintén a Brassica oleracea fajba tartozik 
a karalábé, a brokkoli,

4
00:00:27,665 --> 00:00:33,454
a fejes káposzta, a kelbimbó, 
a karfiol, és a kelkáposzta is. 

5
00:00:36,850 --> 00:00:40,220
A leveles kel egy olyan káposztaféle, amely nem képez fejet.

6
00:00:41,440 --> 00:00:45,796
Általában egy központi szárból áll, amely 
mentén levelek fejlődnek,

7
00:00:49,160 --> 00:00:51,774
de vannak olyan fajtái, amelyek több ágat növesztenek. 

8
00:00:58,240 --> 00:01:00,552
Őszi, illetve téli zöldség.

9
00:01:01,840 --> 00:01:04,690
A legtöbb fajta jól bírja a fagyot: a hidegben

10
00:01:05,156 --> 00:01:09,760
megváltozik a levelek íze, hiszen a bennük 
található keményítő cukorrá alakul át.

11
00:01:12,385 --> 00:01:16,567
De a hosszú ideig tartó kemény fagyok
 elpusztíthatják a növényeket. 

12
00:01:20,363 --> 00:01:24,940
Rengeteg változata ismert, 
amelyeket a magasságuk

13
00:01:25,287 --> 00:01:28,923
(40 cm - 1,5 m), a sima vagy fodros

14
00:01:29,505 --> 00:01:32,930
levélszerkezet, és a színük (világoszöldtől és

15
00:01:33,949 --> 00:01:40,530
sárgászöldtől világos- vagy sötétliláig) 
alapján különíthetünk el egymástól.

16
00:01:47,461 --> 00:01:50,340
A leveles kelt kisállatok takarmányaként

17
00:01:55,287 --> 00:01:57,410
vagy emberi fogyasztásra, továbbá kézműves

18
00:02:00,420 --> 00:02:05,861
termékek, például sétapálcák vagy 
akár szarufák előállítására is használják. 

19
00:02:12,080 --> 00:02:16,654
Az összes B. oleracea fajba tartozó
káposzta megporzása 

20
00:02:32,640 --> 00:02:36,581
A Brassica oleracea faj 
virágai kétivarúak, vagyis

21
00:02:39,549 --> 00:02:43,825
vannak hímivarú és nőivarú 
szaporítószerveik is.

22
00:02:47,505 --> 00:02:49,980
Legtöbbjük önmeddő, vagyis

23
00:02:50,625 --> 00:02:55,825
az egyik növény virágai által termelt virágpor csak 
egy másik növényt képes megtermékenyíteni.

24
00:02:59,469 --> 00:03:01,723
Ezért a növények idegentermékenyülők.

25
00:03:02,589 --> 00:03:07,425
Az optimális megporzás érdekében 
ajánlott sok növényt ültetni. 

26
00:03:11,010 --> 00:03:14,080
A megporzást rovarok végzik.

27
00:03:17,418 --> 00:03:21,956
Ezek a tulajdonságok biztosítják a 
nagy genetikai változatosságot.

28
00:03:28,829 --> 00:03:33,163
A Brassica oleracea faj valamennyi 
alfaja képes egymással

29
00:03:33,323 --> 00:03:34,858
kereszteződni, ezért nem szabad

30
00:03:36,680 --> 00:03:41,403
különböző típusú káposztákat egymáshoz 
közel termeszteni, ha magfogás a célunk! 

31
00:03:45,890 --> 00:03:47,338
A genetikai tisztaság

32
00:03:47,658 --> 00:03:55,090
biztosításához a Brassica oleracea faj fajtái között 
minimum 1 km-es izolációs távolságot hagyjunk! 

33
00:03:56,450 --> 00:04:00,850
Ezt 500 méterre csökkenthetjük, ha két fajta 
között van valamilyen

34
00:04:00,938 --> 00:04:03,876
természetes akadály, pl. sövény. 

35
00:04:07,069 --> 00:04:09,600
A fajtákat úgy is izolálhatjuk egymástól,

36
00:04:09,876 --> 00:04:14,654
ha zárt rovarháló alá kis kaptárakat teszünk 
rovarokkal együtt, vagy ha

37
00:04:19,890 --> 00:04:23,687
az egyes fajtákat takaró 
rovarhálókat felváltva nyitjuk és zárjuk.

38
00:04:27,920 --> 00:04:29,141
Bővebb információt erről

39
00:04:29,330 --> 00:04:33,425
az izolációs technikákról szóló modulban 
találsz A magtermesztés ábécéjében. 

40
00:04:45,580 --> 00:04:55,485
A leveles kel életciklusa 

41
00:05:03,134 --> 00:05:09,163
A leveles kel kétéves növény. A szárát és a 
leveleit a termesztés első évében növeszti,

42
00:05:09,890 --> 00:05:14,058
majd tavasszal virágzik, 
nyáron pedig magot hoz. 

43
00:05:28,989 --> 00:05:34,152
A magfogásral vetett növényeket 
ugyanúgy termesztjük mint a fogyasztásra nevelteket.

44
00:05:38,130 --> 00:05:43,381
A genetikai sokféleség biztosítása 
érdekében 10-15 növényről kell magot fogni. 

45
00:05:59,760 --> 00:06:02,356
Magokat egészséges egyedekről fogjunk, amelyek

46
00:06:02,440 --> 00:06:05,440
növekedését végig figyelemmel 
tudtuk kísérni, megismerve

47
00:06:05,658 --> 00:06:08,603
a tulajdonságaikat, mint a méret, szín,

48
00:06:09,410 --> 00:06:17,490
növekedési erély, gyors fejlődés, 
betegségekkel szembeni ellenálló képesség,

49
00:06:19,890 --> 00:06:25,236
a levelek jelenléte a szár teljes hosszában, és a hidegtűrés! 

50
00:06:28,516 --> 00:06:30,872
Télen a földben maradhatnak a növények.

51
00:06:53,614 --> 00:06:58,727
A második évben tavasszal virágoznak, 
nyáron pedig magot hoznak. 

52
00:07:16,065 --> 00:07:22,780
Magfogás, magtisztítás és magtárolás 
a B. oleracea faj esetében

53
00:07:31,905 --> 00:07:35,643
A magok akkor érettek, amikor 
a becőtermések bézs színűvé válnak.

54
00:07:39,912 --> 00:07:42,458
Érett állapotban a becők könnyen felnyílnak,

55
00:07:42,632 --> 00:07:47,665
és szétszórják a bennük található magokat. 

56
00:07:56,232 --> 00:08:00,240
Általában a szárak nem egyszerre érnek be.

57
00:08:01,294 --> 00:08:07,381
A magvesztés elkerüléséhez a
 magszárakat külön kell betakarítani, amint azok beértek.

58
00:08:09,083 --> 00:08:14,807
A teljes növényt is felszedhetjük 
még azelőtt, hogy minden mag teljesen beérne. 

59
00:08:18,080 --> 00:08:25,098
Ilyenkor az érési folyamat a szárítás végére 
fejeződik be, ami száraz és jól szellőző helyen zajlik. 

60
00:08:37,694 --> 00:08:43,890
Akkor állnak készen a magfogásra, amikor 
a becőtermések kézzel könnyen kinyithatóak. 

61
00:08:45,840 --> 00:08:47,090
A magok kinyeréséhez a

62
00:08:47,240 --> 00:08:52,618
becőterméseket terítsük szét műanyag 
fólián vagy vastag szöveten, majd

63
00:08:53,010 --> 00:08:55,832
ütögessük meg, vagy a kezünk között dörzsöljük össze őket.

64
00:08:59,345 --> 00:09:04,363
A terméseket zsákba is tehetjük, és
 puha felülethez ütögetve kicsépelhetjük a magokat. 

65
00:09:05,767 --> 00:09:10,618
Nagy mennyiség esetén lábbal vagy 
valamilyen járművel ki is taposhatjuk őket. 

66
00:09:23,040 --> 00:09:28,370
A nehezen felnyíló becőtermések 
valószínűleg éretlen magokat tartalmaznak,

67
00:09:28,560 --> 00:09:30,167
amelyek nem csíráznak jól. 

68
00:09:35,010 --> 00:09:36,240
A tisztítás során

69
00:09:36,574 --> 00:09:41,207
a törmelék eltávolításához először 
egy nagylyukú rostán engedjük át

70
00:09:41,512 --> 00:09:43,047
a magokat, amelyen

71
00:09:47,941 --> 00:09:50,654
a törmelék fennmarad, majd pedig egy finomabb szitán,

72
00:09:50,938 --> 00:09:55,500
amin a magok maradnak fenn, 
a kisebb törmelékek viszont átesnek. 

73
00:09:59,272 --> 00:10:03,025
Végül fújva vagy pedig a szél 
segítségével ki kell szelelnünk

74
00:10:03,454 --> 00:10:07,556
a magokat, hogy a maradék 
növényi részeket is eltávolítsuk. 

75
00:10:22,676 --> 00:10:27,610
Az összes Brassica oleracea fajba 
tartozó növény magja nagyon hasonló.

76
00:10:28,647 --> 00:10:35,360
Rendkívül nehéz megkülönböztetni őket, 
például a fejes káposzta és a karfiol magjait.

77
00:10:35,847 --> 00:10:40,836
Ezért fontos címkével ellátni a növényeket, 
majd a kinyert magokat, feltüntetve

78
00:10:41,040 --> 00:10:46,109
a faj és a fajta nevét, 
valamint a termesztés évét. 

79
00:10:47,440 --> 00:10:52,240
Néhány fagyasztóban töltött nap alatt 
valamennyi kártevő lárvája elpusztul. 

80
00:10:57,716 --> 00:11:00,960
A káposztafélék magjai 5 évig 
őrzik meg a csírázóképességüket,

81
00:11:01,934 --> 00:11:05,592
de ez akár 10 évig is kitolódhat.

82
00:11:07,310 --> 00:11:10,669
Ezt az időszakot meghosszabbíthatjuk, 
ha a magokat fagyasztóban tároljuk.

83
00:11:11,694 --> 00:11:17,905
Egy gramm fajtától függően 
250-300 darab magot tartalmaz. 
