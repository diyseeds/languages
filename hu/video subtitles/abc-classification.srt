﻿1
00:00:08,000 --> 00:00:09,675
Botanikai besorolás

2
00:00:22,555 --> 00:00:27,580
Aki magfogásra vállalkozik, jó, ha
tisztában van a botanikának (a növények tudományának)

3
00:00:27,940 --> 00:00:29,420
alapjaival.

4
00:00:37,860 --> 00:00:42,860
A növényeket jelenleg a 
virágaik, a szaporítószerveik, illetve

5
00:00:43,220 --> 00:00:46,140
a termésük anatómiája alapján osztályozzák.

6
00:00:47,460 --> 00:00:50,220
A besorolás latin nevük segítségével történik. 

7
00:00:57,325 --> 00:01:00,780
A latin nevek használata
pontos osztályozást tesz lehetővé,

8
00:01:01,180 --> 00:01:04,420
míg a hétköznapi nevek alkalmazása
félreértésre adhat okot.

9
00:01:09,285 --> 00:01:13,260
Például egyaránt „tök”-nek
nevezünk több fajt is:

10
00:01:13,460 --> 00:01:19,740
pl. a Cucurbita pepo, 
a Cucurbita moschata vagy a Cucurbita maxima fajokat. 

11
00:01:24,635 --> 00:01:28,700
A videóban csak bizonyos latin neveket használunk:

12
00:01:30,100 --> 00:01:32,780
a családokét, mint pl. a Fabaceae (pillangósvirágúak),

13
00:01:34,680 --> 00:01:35,940
Asteraceae (fészkesek),

14
00:01:38,200 --> 00:01:39,280
Solanaceae (burgonyafélék)

15
00:01:42,195 --> 00:01:43,540
vagy Cucurbitaceae (tökfélék);

16
00:01:45,765 --> 00:01:48,820
a nemzetségekét, mint pl. a Cucurbita;

17
00:01:49,180 --> 00:01:54,540
mint pl. a Cucurbita maxima,
végül pedig a fajtákét. 

18
00:01:59,665 --> 00:02:04,580
Például az uborka a tökfélék (Cucurbitaceae) 
családjának tagja,

19
00:02:05,060 --> 00:02:09,500
azon belül a Cucumis nemzetséghez 
és a Cucumis sativus fajhoz tartozik.

20
00:02:10,820 --> 00:02:13,300
Számos különböző fajtája ismert. 

21
00:02:20,735 --> 00:02:23,900
A sárgadinnye szintén a 
tökfélék (Cucurbitaceae) családjába,

22
00:02:24,340 --> 00:02:26,295
és a Cucumis nemzetségbe tartozik,

23
00:02:26,850 --> 00:02:30,300
de ez egy másik faj, a Cucumis melo,

24
00:02:30,740 --> 00:02:34,100
és a fajon belül szintén rengeteg fajtája van. 

25
00:02:39,240 --> 00:02:44,180
Az azonos fajtába tartozó növények 
nagyon hasonlítanak egymásra,

26
00:02:44,260 --> 00:02:47,620
pl. az alakjuk, színük, méretük tekintetében.

27
00:02:49,730 --> 00:02:54,220
Egy adott faj minden fajtája 
képes kereszteződni egymással. 

28
00:02:56,245 --> 00:03:00,820
A különböző fajok fajtái 
általában nem tudnak kereszteződni.

29
00:03:01,840 --> 00:03:05,975
Pl. nem áll fenn a veszélye, egy uborka 
és egy sárgadinnye kereszteződésének. 

30
00:03:21,440 --> 00:03:23,255
Viszont vannak kivételek.

31
00:03:24,475 --> 00:03:28,570
Az egymáshoz botanikailag közel álló fajok 
között előfordulhat kereszteződés,

32
00:03:29,255 --> 00:03:32,700
mint pl. a Cucurbita moschata (pézsmatök)

33
00:03:32,820 --> 00:03:36,180
és a Cucurbita argyrosperma (ezüstmagvú tök) között. 

34
00:03:39,220 --> 00:03:43,100
Különböző nemzetségek közt nem történik 
kereszteződés. 

35
00:03:52,435 --> 00:03:53,620
Ennek a tudásnak az elsajátítása

36
00:03:53,695 --> 00:03:58,260
segít majd kiválasztani a magfogáshoz szükséges 
megfelelő termesztési módszert

37
00:03:58,540 --> 00:04:00,980
és elkerülni a nemkívánatos keresztbeporzódást. 
