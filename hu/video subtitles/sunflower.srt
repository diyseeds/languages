1
00:00:10,990 --> 00:00:16,940
A napraforgó a fészkesek (Asteraceae) családjába 
tartozik. A faj latin neve: Helianthus annuus.

2
00:00:20,970 --> 00:00:26,500
Egyéves növény, amelyet az olajtartalmú 
magjáért vagy a virágzatáért termesztenek.

3
00:00:29,745 --> 00:00:32,020
Számos fajtája van.

4
00:00:33,925 --> 00:00:37,780
A családba tartozó fajok közül csak egyetlen
évelő fajt termesztenek az ehető gumójáért

5
00:00:38,020 --> 00:00:41,820
a csicsókát 
(Helianthus tuberosus). 

6
00:00:54,895 --> 00:00:55,980
Megporzás 

7
00:01:05,150 --> 00:01:08,580
A napraforgó virágzata 
fészekvirágzat.

8
00:01:09,660 --> 00:01:12,380
Rengeteg virágból áll,

9
00:01:13,155 --> 00:01:17,340
amelyek egymás után nyílnak 
a virágzat külső szélétől befelé haladva.

10
00:01:20,960 --> 00:01:23,100
Minden virág kétivarú.

11
00:01:24,240 --> 00:01:26,420
Először a hím szaporítószerv jelenik meg

12
00:01:31,435 --> 00:01:34,460
és egy napig bocsát ki virágport.

13
00:01:38,275 --> 00:01:44,060
Ezután a virág megváltozik, a nőivarú
szaporítószerv jelenik meg, és felveszi a virágport.

14
00:01:58,180 --> 00:02:01,300
Rovarok, különösen méhek és poszméhek

15
00:02:01,700 --> 00:02:06,540
porozzák be az egyes virágokat,
virágról-virágra szállítva a pollent. 

16
00:02:13,725 --> 00:02:19,180
A legtöbb fajta önmeddő, így
a napraforgók általában keresztbeporzódnak,

17
00:02:25,050 --> 00:02:31,060
vagyis az egyik növény virágait csak 
egy másik növény virágai termékenyíthetik meg.

18
00:02:32,080 --> 00:02:34,940
Ezért egyszerre sok
növényt kell termeszteni

19
00:02:35,190 --> 00:02:36,945
az optimális megporzás érdekében. 

20
00:02:42,720 --> 00:02:45,900
Bizonyos napraforgófajták 
öntermékenyek,

21
00:02:46,510 --> 00:02:51,100
vagyis a virágzatukban található
virágok megtermékenyíthetik egymást. 

22
00:02:57,290 --> 00:03:00,100
Minden napraforgófajta összekereszteződik egymással.

23
00:03:04,705 --> 00:03:09,620
Bizonyos térségekben vad napraforgók is élnek,
amelyek szintén kereszteződhetnek a termesztett fajtákkal.

24
00:03:13,505 --> 00:03:16,020
Ugyanez történhet a csicsóka esetében is,

25
00:03:16,267 --> 00:03:18,660
amely botanikai szempontból közeli rokon fajnak számít. 

26
00:03:24,815 --> 00:03:27,700
A napraforgófajták közötti keresztbeporzódás elkerülése

27
00:03:28,145 --> 00:03:31,570
érdekében tartsunk közöttük 1 km-es izolációs távolságot.

28
00:03:33,980 --> 00:03:37,140
Ezt 700 méterre csökkenthetjük,

29
00:03:37,420 --> 00:03:40,900
ha a fajták között van valamilyen
természetes akadály, például sövény. 

30
00:03:46,475 --> 00:03:49,740
Ha a kert egy nagy napraforgómező 
közelében található,

31
00:03:50,440 --> 00:03:54,560
vagy több fajtát szeretnénk 
egymás mellett termeszteni

32
00:03:54,985 --> 00:03:58,060
akkor kézzel kell beporozni a virágokat

33
00:03:58,700 --> 00:04:00,980
az egyes fajták genetikai tisztaságának megőrzése érdekében. 

34
00:04:11,905 --> 00:04:15,100
A virágok kézi megporzása
elég egyszerű.

35
00:04:25,065 --> 00:04:30,100
Minden fejet be kell csomagolni erős,
vízálló nátronpapírból készült zacskóval

36
00:04:30,435 --> 00:04:32,380
még a virágzás előtt.

37
00:05:05,845 --> 00:05:10,420
A virágzás idején vegyük le két
egymás mellett álló növény zacskóját.

38
00:05:17,190 --> 00:05:20,140
Közben figyeljünk a méhekre 
és a poszméhekre,

39
00:05:20,180 --> 00:05:23,860
amelyek folyamatosan megpróbálnak
rászállni a védtelenül maradt virágokra.

40
00:05:28,540 --> 00:05:31,035
Finoman dörzsöljük a virágzatokat egymáshoz.

41
00:05:42,975 --> 00:05:45,860
Amikor ezzel végeztünk,
tegyük vissza a zacskókat a fejekre.

42
00:05:49,235 --> 00:05:55,060
A virágzás 5-10 napig tart, ezért az előbb leírt
folyamatot minden nap el kell végeznünk ez idő alatt.

43
00:06:10,470 --> 00:06:13,860
A zacskókat a fejeken hagyhatjuk 
egészen a magok betakarításáig. 

44
00:06:28,470 --> 00:06:29,820
A napraforgó életciklusa

45
00:06:49,200 --> 00:06:51,500
A magfogási céllal vetett napraforgókat

46
00:06:51,790 --> 00:06:55,940
ugyanúgy termesztjük,
mint az olajért vagy a virágzatért nevelteket. 

47
00:07:19,590 --> 00:07:21,820
A genetikai sokféleség biztosítása érdekében

48
00:07:22,180 --> 00:07:25,620
minimum 10 egyedet kell 
kiválasztani a magfogáshoz.

49
00:07:35,440 --> 00:07:38,180
Olyan növényeket válasszunk ki, amelyek

50
00:07:38,395 --> 00:07:41,940
rendelkeznek az adott fajta 
jellegzetes tulajdonságaival

51
00:07:44,130 --> 00:07:45,540
mint a magasság,

52
00:07:47,635 --> 00:07:48,740
a virágzat mérete

53
00:07:49,500 --> 00:07:53,900
és színe, illetve
a magok minősége! 

54
00:07:54,635 --> 00:07:59,020
A magok fokozatosan alakulnak ki a
virágzat külső szélétől

55
00:07:59,260 --> 00:08:01,140
befelé haladva.

56
00:08:05,940 --> 00:08:08,780
Akkor takarítsuk be a napraforgót,
amikor a fej tele van magokkal,

57
00:08:09,145 --> 00:08:11,060
és a szirmok elkezdtek lehullani.

58
00:08:14,370 --> 00:08:16,535
A madarak nagyon szeretik a napraforgómagot,

59
00:08:16,810 --> 00:08:22,185
ezért a betakarítással ne várjunk addig, amíg 
az egész növény elszárad, különben az összes mag eltűnhet! 

60
00:08:48,145 --> 00:08:52,390
A száraz virágok eltávolításához dörzsöljük meg a fejeket,
és hagyjuk, hogy azok a földre hulljanak!

61
00:09:09,550 --> 00:09:13,940
Majd vágjuk le a külső szirmokat is,
hogy a virágzat könnyebben száradjon.

62
00:09:20,710 --> 00:09:23,020
Ezután tegyük őket száraz, levegős helyre

63
00:09:23,295 --> 00:09:27,340
úgy, hogy a magok felfelé nézzenek,
hogy elkerüljük a penészedést és a rothadást. 

64
00:09:42,410 --> 00:09:44,980
Magkinyerés – válogatás – tárolás

65
00:09:51,425 --> 00:09:54,420
Dörzsöljük meg a virágzatot, hogy a magok kihulljanak.

66
00:10:03,865 --> 00:10:08,660
Akár egy fém szitán is dörzsölhetjük 
őket, melyet egy vödörre teszünk. 

67
00:10:15,080 --> 00:10:19,420
Száraz, szellős helyen hagyjuk 
a magokat teljesen megszáradni. 

68
00:10:25,585 --> 00:10:29,590
A magok akkor száradtak meg
kellőképpen, ha kettétörnek,

69
00:10:30,155 --> 00:10:33,260
amikor meghajlítjuk őket. 

70
00:10:36,650 --> 00:10:40,405
Végül szeleljük ki a magokat a
növényi törmelék eltávolításához.

71
00:10:41,240 --> 00:10:46,420
Ehhez tegyük a magokat egy tányérra vagy
egy szeleléshez használt kosárra, majd fújjunk rájuk,

72
00:10:47,440 --> 00:10:49,305
hogy a könnyű törmelékektől megszabadulhassunk. 

73
00:11:03,545 --> 00:11:05,310
Rakjuk a magokat egy zacskóba,

74
00:11:05,420 --> 00:11:11,380
majd írjuk fel egy címkére a faj és a fajta nevét, 
valamint a magfogás évét, és helyezzük bele. 

75
00:11:15,440 --> 00:11:19,700
Néhány fagyasztóban töltött nap alatt
valamennyi kártevő lárvája elpusztul. 

76
00:11:25,280 --> 00:11:29,220
A napraforgómagok átlagosan 
7 évig őrzik meg a csírázóképességüket.

77
00:11:32,040 --> 00:11:35,340
Ezt úgy tudjuk meghosszabbítani,
ha a magokat alacsony hőmérsékleten tároljuk. 
