1
00:00:07,720 --> 00:00:11,940
A padlizsán vagy tojásgyümölcs a 
burgonyafélék (Solanaceae) családjába,

2
00:00:12,290 --> 00:00:15,060
a Solanum melongena fajba tartozik.

3
00:00:20,265 --> 00:00:25,540
A trópusokon évelőként, a mérsékelt égövben 
egyéves növényként termesztik.

4
00:00:37,350 --> 00:00:42,100
A padlizsánok méretüket, alakjukat 
és színüket tekintve rendkívül változatosak lehetnek.

5
00:00:55,840 --> 00:00:56,740
Megporzás 

6
00:01:15,800 --> 00:01:20,060
A padlizsánnak kétivarú önbeporzó 
virága van, vagyis

7
00:01:21,180 --> 00:01:25,700
a hímivarú és a nőivarú szaporítószervek 
ugyanabban a virágban

8
00:01:26,255 --> 00:01:27,855
találhatóak, és kompatibilisek egymással.

9
00:01:32,980 --> 00:01:34,930
A padlizsánt öntermékenyülő növénynek tekintjük.

10
00:01:39,505 --> 00:01:42,995
Ennek ellenére előfordulhat kereszteződés a fajták között.

11
00:01:44,065 --> 00:01:47,460
Gyakorisága a helyi környezeti 
adottságoktól és a

12
00:01:47,705 --> 00:01:50,380
beporzó rovarok számától függ. 

13
00:01:53,580 --> 00:01:57,220
A szélvédett helyeken termesztett 
növényeket érdemes rendszeresen

14
00:01:57,515 --> 00:02:02,740
megrázogatni a virágzás alatt, 
hogy elősegítsük a megporzást. 

15
00:02:06,990 --> 00:02:08,940
A keresztbeporzás elkerülése

16
00:02:09,350 --> 00:02:14,185
érdekében a fajták között 
tartsunk 100 méteres távolságot!

17
00:02:16,690 --> 00:02:22,580
Ezt a távolságot 50 méterre csökkenthetjük,
 ha van köztük valamilyen természetes akadály, pl. sövény.

18
00:02:24,840 --> 00:02:30,020
A trópusokon az 
izolációs távolság akár egy kilométer is lehet. 

19
00:02:32,935 --> 00:02:36,885
Rovarháló segítségével 
akár fizikai izolációt is alkalmazhatunk.

20
00:02:38,550 --> 00:02:42,790
Bővebb információt az izolációs technikákról 
szóló modulban találsz "A magtermesztés ábécéje" fejezetben. 

21
00:02:55,100 --> 00:02:56,540
Életciklus 

22
00:03:11,045 --> 00:03:16,620
A magfogásra szánt padlizsánokat ugyanúgy 
termesztjük, mint a fogyasztási célból nevelteket.

23
00:03:27,875 --> 00:03:30,060
A genetikai sokféleség biztosítása érdekében

24
00:03:30,235 --> 00:03:34,020
minden fajta esetében legalább 
6-12 növényt kell termeszteni. 

25
00:03:37,390 --> 00:03:42,005
A padlizsán fejlődéséhez
 elengedhetetlen a meleg.

26
00:03:44,810 --> 00:03:49,340
A vetésidőt igazítsuk ahhoz az időszakhoz, 
amikor már kiültethetőek a szabadba a növények! 

27
00:04:18,605 --> 00:04:23,815
A virágzástól számítva fajtától
 függően 60-100 nap szükséges ahhoz,

28
00:04:23,950 --> 00:04:28,420
hogy fogyasztásra 
alkalmas termést takaríthassunk be.

29
00:04:29,980 --> 00:04:33,895
De vigyázzunk, mert a 
magok ekkor még nem érettek! 

30
00:04:38,465 --> 00:04:44,380
A magfogásra szánt növényeket azon egészséges 
és életerős egyedek közül válasszuk ki,

31
00:04:44,720 --> 00:04:48,100
amelyek növekedését végig 
figyelemmel tudtuk kísérni. 

32
00:04:51,625 --> 00:04:56,460
A szabályos és erőteljes 
növekedésű egyedeket keressük, amelyeknek

33
00:04:57,025 --> 00:04:58,500
sok virága és szépen fejlett

34
00:04:58,960 --> 00:05:04,940
termése van, illetve amelyek jól alkalmazkodtak 
a hidegebb éghajlati körülményekhez! 

35
00:05:05,710 --> 00:05:09,555
Ami a termést illeti, válasszuk 
a legfinomabbakat amelyek a fajta jellegzetes

36
00:05:09,940 --> 00:05:13,260
alakját, méretét, hús- és héjszínét, a termés

37
00:05:14,010 --> 00:05:16,260
keseredésmentességét, illetve

38
00:05:16,830 --> 00:05:21,990
tipikus héjvastagságát mutatják. 

39
00:05:25,195 --> 00:05:29,380
A magfogáshoz nem szabad a már leszedett 
padlizsánokból válogatni, mert úgy

40
00:05:29,750 --> 00:05:34,780
nem tudjuk megfigyelni a növény 
valamennyi fejlődési jellegzetességét! 

41
00:05:40,025 --> 00:05:43,120
Beteg növényeket ne válasszunk ki magfogásra! 

42
00:05:48,120 --> 00:05:53,660
A teljes érettség elérésekor 
a termések megpuhulnak, és megváltozik a színük.

43
00:05:55,510 --> 00:06:00,055
A fehér padlizsánok sárgára, 
a lilák pedig barnára színeződnek.

44
00:06:02,125 --> 00:06:05,900
Ha a terméseknek nem volt elég idejük,
 hogy teljesen beérjenek a növényen,

45
00:06:06,220 --> 00:06:11,345
akkor hűvös, száraz helyen, 
fa dobozban utóérlelhetjük őket. 

46
00:06:16,285 --> 00:06:19,180
Magfogás – tisztítás - tárolás

47
00:06:24,170 --> 00:06:25,685
Magfogáshoz teljesen érett,

48
00:06:25,940 --> 00:06:29,145
de nem rothadt terméseket válasszunk! 

49
00:06:34,050 --> 00:06:36,560
A magok kinyerésére két módszer létezik:

50
00:06:40,660 --> 00:06:42,080
Kisebb mennyiség esetén

51
00:06:42,340 --> 00:06:44,250
vágjuk a padlizsánokat négyfelé,

52
00:06:46,875 --> 00:06:48,720
és késsel távolítsuk el a magokat.

53
00:06:56,080 --> 00:07:02,230
Nagyobb mennyiség esetén hámozzuk meg, kockázzuk fel, 
és tegyük vízzel teli befőttesüvegbe a padlizsánokat. 

54
00:07:08,905 --> 00:07:11,460
Néhány másodpercig turmixoljuk.

55
00:07:16,310 --> 00:07:19,530
A jó magok a tárolóedény 
aljára süllyednek.

56
00:07:27,185 --> 00:07:30,420
Szűrő segítségével távolítsuk el a terméshúst, 
a maradék héjat,

57
00:07:30,785 --> 00:07:33,620
és a fejletlen (léha) magokat tartalmazó réteget.

58
00:07:37,535 --> 00:07:41,695
Szedjük ki a jó magokat, majd tisztítsuk meg 
őket szűrőben, folyóvíz alatt.

59
00:07:43,725 --> 00:07:47,340
Ezután két napon
 belül ki kell szárítani a magokat.

60
00:07:50,215 --> 00:07:52,460
Helyezzük őket finom lyukú szitára

61
00:07:52,805 --> 00:07:58,140
vagy tányérra meleg 
(23-30°C közötti), száraz,

62
00:07:59,540 --> 00:08:02,380
jól szellőző helyen! 

63
00:08:04,580 --> 00:08:07,740
Kevés mag esetén használhatunk kávészűrő papírt,

64
00:08:08,110 --> 00:08:12,020
ami sok nedvességet felvesz, 
és nem tapadnak hozzá a magok.

65
00:08:15,525 --> 00:08:19,740
Maximum egy teáskanálnyi magot 
tegyünk egy filterbe.

66
00:08:20,210 --> 00:08:25,645
Száraz, szellős, árnyékos helyen csíptessük 
a tasakokat ruhaszárító kötélre. 

67
00:08:35,455 --> 00:08:37,465
Írjuk fel egy címkére a faj és

68
00:08:37,695 --> 00:08:45,790
a fajta nevét, valamint a magfogás évét,
 majd helyezzük azt a tasak belsejébe!

69
00:08:47,095 --> 00:08:49,930
A külső felirat könnyen letörlődhet. 

70
00:08:57,285 --> 00:09:01,100
Néhány fagyasztóban töltött nap 
alatt valamennyi kártevő lárvája elpusztul.

71
00:09:04,900 --> 00:09:09,710
A padlizsánmagok 3-6 évig őrzik meg 
a csírázóképességüket.

72
00:09:13,470 --> 00:09:16,275
Ezt meghosszabbíthatjuk, ha a magokat fagyasztóban tároljuk. 
