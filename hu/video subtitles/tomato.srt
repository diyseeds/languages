1
00:00:08,950 --> 00:00:16,700
A paradicsom a burgonyafélék (Solanaceae) 
családjába tartozik, latin neve: Solanum lycopersicum.

2
00:00:22,440 --> 00:00:28,220
Egyéves növényként termesztik a mérsékelt égövön,
a trópusokon pedig akár évelőként is.

3
00:00:33,340 --> 00:00:37,860
Rengeteg féle paradicsom létezik.
Több ezer fajtája van,

4
00:00:38,340 --> 00:00:44,685
amelyek színe, alakja,
mérete, íze, vegetációs ideje,

5
00:00:44,840 --> 00:00:49,715
koraisága, hideghez, meleghez és párás körülményekhez
való alkalmazkodóképessége különböző.

6
00:00:58,535 --> 00:01:01,620
Vannak folytonnövő 
(indeterminált) paradicsomok,

7
00:01:02,205 --> 00:01:05,430
amelyek folyamatosan újabb és 
újabb virágokat növesztenek,

8
00:01:05,580 --> 00:01:08,675
így a betakarítás 
hosszú ideig elhúzódik. 

9
00:01:20,620 --> 00:01:22,780
A determinált növekedésű paradicsomok

10
00:01:23,095 --> 00:01:27,000
esetében a virágzás 
rövid ideig tart,

11
00:01:27,875 --> 00:01:31,010
így a betakarítási időszak 
is rövidebb. 

12
00:01:39,160 --> 00:01:40,140
Megporzás

13
00:01:50,290 --> 00:01:52,580
A paradicsomnak kétivarú virágai vannak,

14
00:01:54,215 --> 00:01:58,345
vagyis a hímivarú és a nőivarú szaporítószervek
ugyanabban a virágban találhatóak.

15
00:01:58,680 --> 00:02:03,580
A porzó virágpora képes megtermékenyíteni
az azonos virágban található bibét,

16
00:02:03,995 --> 00:02:07,615
vagyis a paradicsom 
öntermékeny növény. 

17
00:02:08,950 --> 00:02:10,390
A mérsékelt égövön

18
00:02:10,630 --> 00:02:15,415
a szaporítószervek a legtöbbször
védve maradnak a virág belsejében.

19
00:02:17,035 --> 00:02:18,695
Keresztbeporzódás csak ritkán fordul elő. 

20
00:02:26,710 --> 00:02:30,920
A forró trópusi területeken viszont
sokkal többször fordul elő keresztbeporzás.

21
00:02:33,600 --> 00:02:38,170
A bibe hossza határozza meg, hogy
mennyire áll fenn a keresztbeporzás kockázata,

22
00:02:41,415 --> 00:02:46,030
ugyanis a rovaroknak könnyebb 
beporoznia egy porzóknál hosszabb bibét,

23
00:02:46,140 --> 00:02:50,770
mint egy rövidebbet, amely az 
összenőtt porzók alatt bújik meg. 

24
00:02:55,505 --> 00:02:59,380
A nagyméretű húsos paradicsomoknak 
gyakran van dupla virága.

25
00:02:59,675 --> 00:03:02,285
Ez megnöveli a keresztbeporzódás kockázatát,

26
00:03:06,140 --> 00:03:11,165
ezért érdemes megfigyelni 
minden fajta virágszerkezetét,

27
00:03:11,575 --> 00:03:14,785
és a rovarok aktivitását is 
a kertünkben! 

28
00:03:16,160 --> 00:03:20,950
A keresztbeporzódás veszélyét csökkentheti,
ha egyéb virágok is találhatóak a kertünkben,

29
00:03:21,255 --> 00:03:24,370
amelyek nektárját kedvelik 
a méhek és a poszméhek. 

30
00:03:32,900 --> 00:03:36,260
Ha a szélcsendes napokon többször megrázzuk a növényeket,

31
00:03:36,505 --> 00:03:40,225
az növeli az
öntermékenyítés esélyét. 

32
00:03:50,575 --> 00:03:52,100
A mérsékelt égövön

33
00:03:52,300 --> 00:03:56,445
úgy előzhetjük meg a keresztbeporzódást 
a virágban megbúvó rövid bibével

34
00:03:56,615 --> 00:04:02,020
rendelkező fajták esetében,
ha azok között 3 m izolációs távolságot hagyunk. 

35
00:04:06,495 --> 00:04:12,780
A kiálló, hosszú bibével rendelkező fajták
között tartsunk 9-12 m izolációs távolságot. 

36
00:04:17,060 --> 00:04:23,200
A forró trópusi területeken 1 km-es izolációs távolságot 
szükséges tartani az egyes fajták között. 

37
00:04:26,570 --> 00:04:33,385
Ezt 200 méterre csökkenthetjük, ha valamilyen
természetes akadály, például sövény van a fajták között. 

38
00:04:36,105 --> 00:04:39,960
A fajtákat rovarhálóval 
is izolálhatjuk.

39
00:04:41,455 --> 00:04:42,620
Bővebb információt

40
00:04:42,905 --> 00:04:46,895
ezzel kapcsolatban az izolációs technikákról 
szóló modulban találsz "A magtermesztés ábécéje" fejezetben. 

41
00:04:53,650 --> 00:04:54,820
Életciklus 

42
00:05:17,400 --> 00:05:20,540
A mérsékelt égövön a paradicsomot
egyéves növényként termesztik.

43
00:05:24,005 --> 00:05:27,380
A trópusokon viszont 
akár több évig is élhet.

44
00:05:36,630 --> 00:05:38,980
A magfogás céljából ültetett paradicsomokat

45
00:05:39,120 --> 00:05:42,540
ugyanúgy termesztjük, mint
a fogyasztásra szántakat. 

46
00:06:07,415 --> 00:06:08,980
A korai fajtáknál a virágzást

47
00:06:09,260 --> 00:06:13,180
követően minimum 
40 napnak kell eltelnie

48
00:06:13,495 --> 00:06:15,395
a termés teljes éréséig.

49
00:06:24,235 --> 00:06:30,580
Ez az időtartam akár 60-80 nap is lehet a 
középérésű, illetve a kései fajták esetében. 

50
00:06:37,300 --> 00:06:38,620
Azokat a növényeket válasszuk ki,

51
00:06:38,700 --> 00:06:42,010
amelyek fejlődését végig
nyomon tudtuk követni,

52
00:06:42,120 --> 00:06:44,780
és amelyek megfelelnek a szelekciós kritériumoknak. 

53
00:06:48,580 --> 00:06:52,250
Keressük a szabályos és
erős növekedésű,

54
00:06:53,010 --> 00:07:00,050
korán vagy későn termő,
sok virágot hozó, illetve jól kötődő egyedeket.

55
00:07:04,015 --> 00:07:06,140
A determinált növekedésű fajtáknál

56
00:07:06,550 --> 00:07:10,070
olyan kompakt növényeket válasszunk ki, 
amelyek betakarítási ideje rövid. 

57
00:07:14,080 --> 00:07:18,345
Kóstoljuk meg a termést is, hogy
kiderüljön, hogy édes vagy inkább savas-e. 

58
00:07:19,905 --> 00:07:24,405
Ami a termést illeti, keressük
a fajta jellegzetes tulajdonságait mutatókat

59
00:07:25,155 --> 00:07:31,945
a méret, hús színe,
héj szépsége,

60
00:07:32,785 --> 00:07:34,970
illetve a termés karéjainak száma alapján.

61
00:07:36,665 --> 00:07:39,545
Ha már leszedett paradicsomokból válogatunk

62
00:07:39,815 --> 00:07:43,630
a magfogáshoz, akkor nem tudjuk 
ellenőrizni az adott fajta

63
00:07:43,720 --> 00:07:45,950
növekedéssel kapcsolatos tulajdonságait. 

64
00:07:49,670 --> 00:07:53,740
A magfogásra szánt paradicsomokat 
egészséges növényekről gyűjtsük

65
00:07:53,925 --> 00:07:56,660
akkor, amikor a termések már 
érettek, és fogyaszthatóak.

66
00:07:57,415 --> 00:08:00,845
A legjobb az, ha az első vagy 
a második virágcsoportból választunk.

67
00:08:06,230 --> 00:08:09,595
Akár később is szedhetünk terméseket,
ha a növények ellenállónak

68
00:08:09,745 --> 00:08:12,310
bizonyultak a különböző betegségekkel szemben. 

69
00:08:16,880 --> 00:08:20,810
A genetikai sokféleség biztosítása 
érdekében minden fajta esetében

70
00:08:21,365 --> 00:08:25,150
min. 6-12 különböző növényről gyűjtsünk paradicsomokat,

71
00:08:25,695 --> 00:08:29,215
és ne szedjünk beteg egyedekről, 
illetve sérült terméseket. 

72
00:08:32,745 --> 00:08:36,220
Hidegebb éghajlatú, illetve 
hegyvidéki területeken előfordulhat,

73
00:08:37,070 --> 00:08:40,395
hogy a terméseknek nincs
idejük a növényen beérni.

74
00:08:41,340 --> 00:08:48,525
Ilyenkor hagyjuk a betakarított paradicsomokat
meleg helyen utóérni, például üvegházban vagy egy ablakpárkányon! 

75
00:08:56,525 --> 00:08:59,150
Magkinyerés – válogatás – tárolás 

76
00:09:04,430 --> 00:09:08,695
A magkinyeréshez válasszunk érett, 
de nem erjedt terméseket. 

77
00:09:13,590 --> 00:09:15,380
Ha kevés magot fogunk,

78
00:09:15,520 --> 00:09:20,220
vágjuk ketté a termést, majd kanál segítségével 
vagy a paradicsom kifacsarásával kaparjuk

79
00:09:20,620 --> 00:09:22,260
a magokat és a hús egy részét egy befőttesüvegbe. 

80
00:09:31,120 --> 00:09:36,140
Nagyobb mennyiségű magfogásnál, és kisméretű
cseresznyeparadicsomok vagy vad fajták esetében

81
00:09:36,690 --> 00:09:39,340
kockázzuk fel a terméseket, majd turmixoljuk le őket. 

82
00:09:47,045 --> 00:09:53,500
Minden magot kocsonyás burok 
vesz körül, amely meggátolja a csírázást. 

83
00:09:58,700 --> 00:10:03,980
Erjesztéssel elérhető, hogy ez 
a burok magától leváljon a magról.

84
00:10:06,940 --> 00:10:12,700
Bővebb információt ezzel kapcsolatban a 
„Magok kinyerése, nedves feldolgozása, szárítása és válogatása”

85
00:10:13,060 --> 00:10:15,340
című modulban találsz A magtermesztés ábécéje fejezetben 

86
00:10:33,355 --> 00:10:36,100
A magok vizes kinyerését követően nagyon fontos,

87
00:10:36,540 --> 00:10:40,140
hogy rögtön elkezdjük 
megszárítani őket

88
00:10:40,260 --> 00:10:43,180
száraz, árnyékos és szellős helyen. 

89
00:10:48,495 --> 00:10:52,940
Kisebb mennyiségeknél 
használhatunk kávészűrő papírt is,

90
00:10:53,775 --> 00:10:57,660
ami sok nedvességet tud felvenni, 
és a magok sem tapadnak hozzá.

91
00:10:59,565 --> 00:11:03,740
Maximum egy kis teáskanálnyi 
magot tegyünk egy-egy kávészűrő papírba.

92
00:11:05,715 --> 00:11:11,900
Száraz, szellős és árnyékos helyen 
csíptessük a tasakokat egy kötélre. 

93
00:11:21,650 --> 00:11:24,060
A paradicsommagokat hőtől,

94
00:11:24,380 --> 00:11:27,980
nedvességtől és fénytől védett helyen tároljuk
befőttesüvegekben vagy műanyag zacskókban!

95
00:11:30,605 --> 00:11:34,140
Ne felejtsük el felírni egy címkére a 
magfogás évét, illetve a faj és a fajta nevét

96
00:11:34,500 --> 00:11:36,260
majd pedig azt beletenni a tárolóedénybe! 

97
00:11:43,490 --> 00:11:48,985
A paradicsommagok 4-6 évig őrzik meg 
a csírázóképességüket.

98
00:11:49,880 --> 00:11:53,510
Ezt úgy tudjuk meghosszabbítani, 
ha a magokat fagyasztóban tároljuk. 
