1
00:00:09,295 --> 00:00:12,180
A sárgadinnye a tökfélék (Cucurbitaceae) családjába tartozik.

2
00:00:13,775 --> 00:00:16,220
A faj latin neve: Cucumis melo.

3
00:00:23,080 --> 00:00:24,460
Egyéves növény,

4
00:00:24,665 --> 00:00:30,340
melynek számos különböző alakú, 
színű és ízű típusa létezik.

5
00:00:34,830 --> 00:00:40,625
Például van sárgadinnye, kantalupdinnye, 
kandírozásra használt dinnye, és

6
00:00:41,425 --> 00:00:44,680
téli dinnye is, amelyet akár
 hónapokig is lehet tárolni. 

7
00:01:00,980 --> 00:01:01,980
Megporzás

8
00:01:20,015 --> 00:01:22,620
A sárgadinnye egylaki növény, vagyis

9
00:01:24,595 --> 00:01:29,080
a hímivarú és a nőivarú virágok is 
ugyanazon az egyeden találhatóak.

10
00:01:29,975 --> 00:01:33,200
A nőivarú virágok esetében magház található a virág alatt.

11
00:01:34,000 --> 00:01:37,420
Ez egy mini sárgadinnye, ami a megporzás után kezd el fejlődni. 

12
00:01:40,955 --> 00:01:44,285
A hímivarú virágok a hosszú szárak végén találhatóak.

13
00:01:49,275 --> 00:01:51,835
A virágok csak egy napra nyílnak ki. 

14
00:01:55,750 --> 00:01:57,660
A sárgadinnyék öntermékenyek,

15
00:01:58,165 --> 00:02:02,620
vagyis egy nőivarú virágot egy 
olyan hímivarú virág virágpora is megtermékenyíthet,

16
00:02:02,760 --> 00:02:03,960
ami ugyanazon a növényen található. 

17
00:02:07,765 --> 00:02:10,970
Viszont gyakrabban fordul elő keresztbeporzódás.

18
00:02:11,920 --> 00:02:16,250
A sárgadinnyék virágait rovarok, elsősorban méhek porozzák be.

19
00:02:23,730 --> 00:02:30,145
Cucumis melo faj összes fajtája keresztbeporzódik 
egymással, beleértve a vad sárgadinnyét is.

20
00:02:30,465 --> 00:02:37,155
Viszont a sárgadinnyék nem kereszteződnek az 
uborkákkal, a görögdinnyékkel, és a tökökkel. 

21
00:02:41,950 --> 00:02:47,725
A keresztbeporzás elkerülése érdekében a fajták 
között tartsunk 1 kilométeres izolációs távolságot! 

22
00:02:49,240 --> 00:02:54,665
Ezt 400 méterre csökkenthetjük, ha van köztük 
valamilyen természetes akadály, például sövény. 

23
00:02:59,560 --> 00:03:04,460
Számos módszer létezik arra, hogy különböző 
sárgadinnyefajtákról magot fogjunk

24
00:03:04,645 --> 00:03:06,080
ugyanabban a kertben. 

25
00:03:08,205 --> 00:03:14,010
Pl. egy fajta összes egyedét lefedhetjük rovarhálóval, és 
poszméhkaptárat helyezhetünk el a hálón belül. 

26
00:03:25,520 --> 00:03:29,820
Vagy két fajtát külön-külön 
letakarhatunk rovarhálóval,

27
00:03:30,800 --> 00:03:37,125
és az egyik nap csak az egyiket nyitjuk ki, 
a másik nap pedig a másikat.

28
00:03:39,210 --> 00:03:41,625
Így a vadon élő rovarok elvégezhetik a munkájukat.

29
00:03:44,270 --> 00:03:49,530
Viszont így kevesebb termésünk lesz, mert 
nem minden virág termékenyül meg. 

30
00:03:52,590 --> 00:03:55,985
A harmadik módszer a 
virágok kézi megporzása.

31
00:03:58,080 --> 00:04:00,940
A sárgadinnyénél ez aprólékosabb munka, mint a tökök vagy

32
00:04:01,195 --> 00:04:03,220
a cukkini esetében, mert a virágok kisebbek,

33
00:04:03,565 --> 00:04:06,895
és a virágzás pontos idejét is 
nehéz meghatározni.

34
00:04:08,035 --> 00:04:12,820
Ráadásul a nőivarú virágok 
80%-a spontán nem kötődik.

35
00:04:13,970 --> 00:04:17,540
A kézi megporzás kevésbé hatékony, 
mint a rovarok munkája:

36
00:04:22,430 --> 00:04:28,390
a kézzel megporzott virágok csupán 
10-15%-ából fejlődik termés. 

37
00:04:35,305 --> 00:04:38,740
Bővebb információt 
ezzel a három módszerrel kapcsolatban

38
00:04:38,880 --> 00:04:42,100
a mechanikai izolációs technikákról
és a kézi megporzásról

39
00:04:42,465 --> 00:04:47,055
szóló modulokban találsz 
"A magtermesztés ábécéje" fejezetben. 

40
00:04:51,100 --> 00:04:52,260
A sárgadinnye életciklusa 

41
00:05:09,315 --> 00:05:13,740
A magfogásra vetett sárgadinnyéket ugyanúgy 
termesztjük, mint a fogyasztási célból nevelteket.

42
00:05:43,035 --> 00:05:47,180
A fajta függvényében a sárgadinnyék 
eltérő hőmérsékletet igényelhetnek.

43
00:05:48,890 --> 00:05:52,130
A genetikai sokféleség biztosítása 
érdekében ideális esetben

44
00:05:52,220 --> 00:05:54,290
legalább egy tucat, de minimum

45
00:05:55,840 --> 00:05:57,755
6 növényről kell magot fogni. 

46
00:06:01,755 --> 00:06:04,860
A magfogásra szánt egyedeket 
a fajtára jellemző tulajdonságok

47
00:06:05,040 --> 00:06:09,685
alapján válasszuk ki, 
mint a rövid tenyészidő,

48
00:06:10,240 --> 00:06:11,100
a növekedési erély,

49
00:06:12,645 --> 00:06:13,845
a termések száma,

50
00:06:15,280 --> 00:06:19,415
a szabadföldi termesztés lehetősége 
mérsékelt égövön,

51
00:06:20,960 --> 00:06:23,500
illetve a hús íze és édessége.

52
00:06:26,330 --> 00:06:27,830
A beteg egyedeket ne tartsuk meg! 

53
00:06:39,085 --> 00:06:42,820
A sárgadinnye magjainak érettségét 
könnyű meghatározni:

54
00:06:43,410 --> 00:06:46,260
az érett, fogyasztásra kész termésekből 
foghatunk magot. 

55
00:06:51,975 --> 00:06:54,980
Magfogás - tisztítás – tárolás

56
00:07:00,835 --> 00:07:04,340
A magok kinyeréséhez vágjuk 
ketté a sárgadinnyét!

57
00:07:05,265 --> 00:07:07,820
Kanál segítségével távolítsuk el a magokat,

58
00:07:09,545 --> 00:07:11,380
és élvezettel fogyasszuk el a dinnye többi részét! 

59
00:07:17,615 --> 00:07:24,420
Öblítsük le a magokat folyóvíz alatt, majd 
árnyékos helyen hagyjuk őket megszáradni! 

60
00:07:30,330 --> 00:07:34,110
A magok akkor száradtak meg eléggé, 
ha kettétörnek, amikor meghajlítjuk őket. 

61
00:07:46,965 --> 00:07:50,300
Mindig írjuk fel egy címkére a fajta és a 
faj nevét, valamint a magfogás évét,

62
00:07:50,370 --> 00:07:54,980
majd helyezzük azt a tasak belsejébe! 
A külső felirat könnyen letörlődhet. 

63
00:08:02,380 --> 00:08:06,780
Néhány fagyasztóban töltött nap alatt 
a kártevők lárvái elpusztulnak. 

64
00:08:12,065 --> 00:08:15,730
A sárgadinnye magjai átlagosan 5 évig 
őrzik meg a csírázóképességüket,

65
00:08:15,950 --> 00:08:18,740
de akár 10 évig is kicsírázhatnak. 
