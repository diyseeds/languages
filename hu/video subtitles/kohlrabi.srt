1
00:00:12,320 --> 00:00:18,327
A karalábé a káposztafélék (Brassicaceae) családjának 
tagja, latin neve Brassica oleracea,

2
00:00:20,400 --> 00:00:24,261
ezen belül a caulorapa gongylodes alfajhoz tartozik.

3
00:00:25,949 --> 00:00:31,396
Szintén a Brassica oleracea fajba tartozik 
a brokkoli, a fejes káposzta,

4
00:00:32,334 --> 00:00:37,469
a kelbimbó (bimbós kel), a leveles kel, 
a karfiol, és a kelkáposzta is. 

5
00:00:42,749 --> 00:00:46,800
A karalábé kétéves növény, amelyet a 
szárgumójáért termesztünk.

6
00:00:52,550 --> 00:00:57,440
A növény nagyon kevés levelet fejleszt, 
a húsa pedig zsenge marad.

7
00:01:08,167 --> 00:01:12,900
Különböző fajtái léteznek, amelyeket a szár mérete 
és színe alapján különböztethetünk meg.

8
00:01:16,880 --> 00:01:20,938
Vannak rövid tenyészidejű, 
illetve tárolásra való fajták. 

9
00:01:28,494 --> 00:01:33,260
A B. oleracea fajba tartozó 
káposztafélék megporzása 

10
00:01:49,185 --> 00:01:53,127
A Brassica oleracea faj virágai kétivarúak,
 vagyis vannak hímivarú

11
00:01:56,021 --> 00:02:00,218
és nőivarú szaporítószerveik is.
A legtöbbjük önmeddő,

12
00:02:03,934 --> 00:02:06,458
az egyik növény virágai által

13
00:02:07,040 --> 00:02:12,196
termelt virágpor csak egy másik 
növényt képes megtermékenyíteni.

14
00:02:15,898 --> 00:02:18,167
Ezért a növények idegentermékenyülők.

15
00:02:19,047 --> 00:02:23,956
Az optimális megporzás érdekében 
ajánlott sok növényt ültetni. 

16
00:02:27,432 --> 00:02:30,574
A megporzást rovarok végzik.

17
00:02:33,876 --> 00:02:38,378
Ezek a tulajdonságok biztosítják 
a nagy genetikai változatosságot.

18
00:02:45,265 --> 00:02:49,621
A Brassica oleracea faj 
valamennyi alfaja

19
00:02:49,774 --> 00:02:51,330
képes egymással kereszteződni, ezért

20
00:02:53,200 --> 00:02:57,789
nem szabad különböző típusú káposztákat
 egymáshoz közel termeszteni, ha magfogás a célunk! 

21
00:03:02,334 --> 00:03:08,087
A genetikai tisztaság biztosításához 
a B. oleracea faj különböző fajtái

22
00:03:08,378 --> 00:03:11,607
között minimum 1 km-es izolációs távolságot hagyjunk! 

23
00:03:12,920 --> 00:03:17,258
Ezt 500 méterre csökkenthetjük, ha két fajta
között van valamilyen

24
00:03:17,330 --> 00:03:20,334
természetes akadály, pl. sövény. 

25
00:03:23,520 --> 00:03:25,992
A fajtákat úgy is izolálhatjuk egymástól,

26
00:03:26,312 --> 00:03:31,200
ha zárt rovarháló alá kis kaptárakat 
teszünk rovarokkal együtt, vagy ha

27
00:03:36,283 --> 00:03:39,949
az egyes fajtákat takaró 
rovarhálókat felváltva nyitjuk és zárjuk.

28
00:03:44,360 --> 00:03:45,541
Bővebb információt

29
00:03:45,716 --> 00:03:49,770
erről az izolációs technikákról szóló modulban 
találsz A magtermesztés ábécéjében. 

30
00:04:03,374 --> 00:04:05,374
A karalábé életciklusa 

31
00:04:19,083 --> 00:04:21,149
Az első évben a magfogás

32
00:04:21,483 --> 00:04:24,225
céljából vetett, tárolásra szánt fajták egyedeit

33
00:04:24,596 --> 00:04:28,087
ugyanúgy kell termeszteni, mint 
a fogyasztási célból nevelteket. 

34
00:04:29,381 --> 00:04:30,680
A karalábé az első évben

35
00:04:30,909 --> 00:04:37,061
szárgumót fejleszt, amelynek át kell 
telelnie, hogy a következő évben virágozhasson.

36
00:04:37,520 --> 00:04:43,876
A túlfejlett, illetve az ősz végén megrepedt 
gumók nehezen telelnek át.

37
00:04:45,163 --> 00:04:48,660
Ezért a legtöbb területen érdemes 
rövid tenyészidejű, illetve tárolásra

38
00:04:48,850 --> 00:04:52,116
alkalmas fajtákat is vetni júniusban vagy júliusban. 

39
00:05:50,276 --> 00:05:56,130
A genetikai sokféleség biztosításához 
az első év őszén 30 növényt válasszunk ki,

40
00:05:56,421 --> 00:06:00,160
hogy tavaszra biztosan 
maradjon belőlük 10-15 darab.

41
00:06:02,021 --> 00:06:05,869
Magokat olyan egészséges 
egyedekről fogjunk, amelyek

42
00:06:06,101 --> 00:06:09,069
növekedését végig figyelemmel 
tudtuk kísérni, ellenőrizve

43
00:06:09,330 --> 00:06:12,894
a fajta tulajdonságait, mint a szár minősége,

44
00:06:14,647 --> 00:06:18,945
a növekedési erély és a 
gyors növekedés, a betegségekkel

45
00:06:19,600 --> 00:06:24,414
szembeni ellenálló képesség, 
a jó tárolhatóság,

46
00:06:24,960 --> 00:06:27,760
a rövid tenyészidő, és a hidegtűrés. 

47
00:06:39,080 --> 00:06:43,483
A magfogásra szánt növényeket 
pincében tárolhatjuk.

48
00:06:44,276 --> 00:06:48,021
Ilyenkor az oldalsó leveleket vágjuk le, 
viszont a középsőket hagyjuk meg! 

49
00:06:54,509 --> 00:06:58,312
Ezt követően a karalábékat rakjuk 
homokba vagy tárolókba!

50
00:07:01,207 --> 00:07:06,029
A tél folyamán a karalábé a káposztáknál 
kevésbé hajlamos a rothadásra. 

51
00:07:12,167 --> 00:07:14,190
Az enyhébb éghajlatú területeken

52
00:07:14,545 --> 00:07:21,592
a karalábé a földben is áttelelhet, mivel 
akár a -7°C-os fagyokat is kibírja.

53
00:07:23,127 --> 00:07:27,454
A legjobb, ha száraz telünk van, mivel 
az alacsonyabb víztartalmú szárak

54
00:07:27,934 --> 00:07:30,690
jobban ellenállnak a zord téli időjárásnak. 

55
00:07:40,487 --> 00:07:44,436
Tavasszal a komolyabb fagyok veszélyének elmúltával

56
00:07:45,156 --> 00:07:48,180
szedjük ki a karalábékat 
a tárolóból, majd

57
00:07:48,400 --> 00:07:52,858
ültessük vissza őket a kertbe úgy,
 hogy a kétharmaduk a földbe kerüljön! 

58
00:08:02,560 --> 00:08:06,458
Ezt után a karalábé virágszárakat 
fog növeszteni, majd virágzik. 

59
00:08:10,110 --> 00:08:11,694
Az eldőlésük megelőzése

60
00:08:11,810 --> 00:08:15,483
érdekében időnként szükség 
lehet a virágszárak karózására is. 

61
00:08:47,301 --> 00:08:54,232
Magfogás a B. oleracea fajról,
a magok válogatása és tárolása 

62
00:09:03,127 --> 00:09:06,974
A magok akkor érettek, amikor a 
becőtermések bézs színűvé válnak.

63
00:09:11,170 --> 00:09:13,470
Érett állapotban a becők könnyen

64
00:09:13,869 --> 00:09:18,952
felnyílnak, és szétszórják 
a bennük található magokat. 

65
00:09:27,469 --> 00:09:31,476
Általában a szárak nem 
egyszerre érnek be.

66
00:09:32,440 --> 00:09:36,647
A magvesztés elkerüléséhez 
a magszárakat külön kell

67
00:09:36,741 --> 00:09:38,690
betakarítani, amint azok beértek.

68
00:09:40,334 --> 00:09:46,007
A teljes növényt is felszedhetjük még 
azelőtt, hogy minden mag teljesen beérne. 

69
00:09:49,360 --> 00:09:54,509
Ilyenkor az érési folyamat a szárítás végére 
fejeződik be, ami száraz és

70
00:09:54,640 --> 00:09:56,545
jól szellőző helyen zajlik. 

71
00:10:08,923 --> 00:10:15,040
A káposztafélék akkor állnak készen a magfogásra, 
amikor a becőtermések kézzel könnyen kinyithatóak. 

72
00:10:16,960 --> 00:10:18,341
A magok kinyeréséhez a

73
00:10:18,480 --> 00:10:23,847
becőterméseket terítsük szét műanyag fólián 
vagy vastag szöveten, majd

74
00:10:24,276 --> 00:10:27,098
ütögessük meg, vagy a kezünk között dörzsöljük össze őket.

75
00:10:30,596 --> 00:10:35,665
A terméseket zsákba is tehetjük, és puha 
felülethez ütögetve kicsépelhetjük a magokat. 

76
00:10:37,080 --> 00:10:41,774
Nagy mennyiség esetén lábbal vagy 
valamilyen járművel ki is taposhatjuk őket. 

77
00:10:54,280 --> 00:10:59,672
A nehezen felnyíló becőtermések 
valószínűleg éretlen magokat

78
00:10:59,818 --> 00:11:01,614
tartalmaznak, amelyek nem csíráznak jól. 

79
00:11:06,160 --> 00:11:08,820
A tisztítás során a törmelék eltávolításához

80
00:11:09,003 --> 00:11:12,392
először egy nagylyukú
rostán engedjük át a magokat,

81
00:11:12,749 --> 00:11:14,218
amelyen törmelék

82
00:11:19,185 --> 00:11:21,861
fennmarad, majd pedig egy finomabb szitán, amin

83
00:11:22,203 --> 00:11:26,720
a magok maradnak fenn, 
a kisebb törmelékek viszont átesnek. 

84
00:11:30,509 --> 00:11:34,261
Végül fújva vagy pedig a szél 
segítségével ki kell szelelnünk

85
00:11:34,698 --> 00:11:38,734
a magokat, hogy a maradék 
növényi részeket is eltávolítsuk. 

86
00:11:53,920 --> 00:11:58,850
Az összes Brassica oleracea fajba tartozó 
növény magja nagyon hasonló.

87
00:11:59,854 --> 00:12:03,760
Rendkívül nehéz megkülönböztetni őket,

88
00:12:04,160 --> 00:12:06,720
például a fejes káposzta és a karfiol magjait.

89
00:12:07,098 --> 00:12:10,000
Ezért fontos címkével ellátni a növényeket,

90
00:12:10,254 --> 00:12:14,094
majd a kinyert magokat, feltüntetve 
a faj és a fajta nevét,

91
00:12:14,280 --> 00:12:17,447
valamint a termesztés évét. 

92
00:12:18,756 --> 00:12:23,534
Néhány fagyasztóban töltött nap alatt 
valamennyi kártevő lárvája elpusztul. 

93
00:12:28,960 --> 00:12:32,167
A káposztafélék magjai 5 évig őrzik meg a csírázóképességüket,

94
00:12:33,156 --> 00:12:36,814
de ez akár 10 évig is kitolódhat.

95
00:12:38,640 --> 00:12:41,934
Úgy tudjuk meghosszabbítani, 
ha a magokat fagyasztóban tároljuk.

96
00:12:42,916 --> 00:12:49,294
Egy gramm, fajtától függően 
250-300 darab magot tartalmaz. 
