﻿1
00:00:09,980 --> 00:00:13,163
A cikória a fészkesek (Asteraceae) családjának tagja.

2
00:00:13,950 --> 00:00:20,261
Kétéves növény, és két faj tartozik bele: 
a Cichorium endivia

3
00:00:21,163 --> 00:00:23,330
és a Cichorium intybus.

4
00:00:28,021 --> 00:00:33,505
- A Cichorium endivia két jelentősebb alfajra osztható, 
melyeket a leveleikért termesztenek: 

5
00:00:37,636 --> 00:00:40,327
a göndör cikória (crispa típus), valamint

6
00:00:41,185 --> 00:00:44,960
a sima levelű endívia (latifolia típus), 
melynek nem szeldeltek a levelei. 

7
00:00:49,280 --> 00:00:53,454
A másik fajba (Cichorium intybus) az alábbiak tartoznak:

8
00:00:53,818 --> 00:01:01,374
- keserű cikóriák (foliosum típus) nagyméretű 
vörös, tarka vagy zöld levelekkel,

9
00:01:02,705 --> 00:01:07,090
- belga endívia néven ismert 
cikóriák (sativum típus), illetve 

10
00:01:08,443 --> 00:01:12,203
olyan fajták, amelyek gyökeréből 
cukrot vonnak ki,

11
00:01:12,465 --> 00:01:15,352
vagy amelyek gyökerét megpörkölik, 
és pótkávéként használják. 

12
00:01:18,807 --> 00:01:22,945
Az intybus fajba tartoznak az 
Európában élő vadcikóriák is. 

13
00:01:32,640 --> 00:01:33,730
Megporzás 

14
00:01:49,214 --> 00:01:54,618
A cikóriák virágzata kéktől az ibolyakékig 
terjedő színű csöves virágokból áll,

15
00:01:54,792 --> 00:02:00,450
amelyek fészkes virágzatot alkotnak.

16
00:02:01,403 --> 00:02:04,530
Mindkét faj virágai kétivarúak. 

17
00:02:06,647 --> 00:02:12,909
A Cicorium endivia virágai önbeporzóak, de hajlamosak 
az idegentermékenyülésre is,

18
00:02:13,410 --> 00:02:17,105
ezért nincs szükségük rovarokra 
a megtermékenyítéshez. 

19
00:02:17,432 --> 00:02:21,480
A Cicorium intybus virágai
 viszont önmeddőek,

20
00:02:21,941 --> 00:02:24,618
így szükségük van a beporzó rovarokra. 

21
00:02:26,203 --> 00:02:30,600
Az egyes fajokon belül található fajták
 keresztbeporzódnak egymással

22
00:02:30,858 --> 00:02:33,054
a rovarok jelenléte miatt.

23
00:02:35,221 --> 00:02:38,974
A fajok egyedi tulajdonságai miatt
 a Cicorium endivia

24
00:02:39,301 --> 00:02:43,280
beporozhatja a Cichorium intybust is,
 viszont az ellenkező irányban

25
00:02:43,738 --> 00:02:45,900
ez nem fordulhat elő. 

26
00:02:55,080 --> 00:02:56,810
A keresztbeporzódás elkerülése érdekében

27
00:02:56,996 --> 00:03:01,709
két cikóriafajta között 500 méteres 
izolációs távolságot kell tartani. 

28
00:03:03,447 --> 00:03:06,741
Ezt a távolságot 250 méterre csökkenthetjük,

29
00:03:07,010 --> 00:03:11,214
ha a fajták között van valamilyen 
természetes akadály, például sövény. 

30
00:03:13,207 --> 00:03:18,320
A Cichorium endivia fajtáit állandó rovarháló 
alatt tarthatjuk a keresztbeporzódás

31
00:03:18,640 --> 00:03:22,101
elkerülése érdekében, 
mivel a növények önbeporzóak. 

32
00:03:24,894 --> 00:03:26,560
A Cichorium intybus esetében

33
00:03:26,814 --> 00:03:30,749
az egyes fajtákat takaró rovarhálókat
 felváltva kell nyitni és zárni

34
00:03:31,105 --> 00:03:35,498
“A magtermesztés ábécéje” izolációs technikákról 
szóló moduljában ismertetett módon.

35
00:03:39,178 --> 00:03:42,960
Bizonyosodjunk meg róla, hogy a 
környéken nincsen sima levelű endívia,

36
00:03:43,054 --> 00:03:47,229
göndör endívia vagy vadcikória, 
mert azok keresztbeporzódhatnak

37
00:03:47,934 --> 00:03:50,560
a magfogásra szánt növényekkel! 

38
00:03:55,200 --> 00:03:56,540
Életciklus 

39
00:04:14,567 --> 00:04:16,901
A cikória kétéves növény.

40
00:04:19,180 --> 00:04:21,992
Magot csak a második évben hoz. 

41
00:04:37,520 --> 00:04:39,130
Az életciklus első évében

42
00:04:39,265 --> 00:04:44,356
a cikória magfogás céljából vetett egyedeit ugyanúgy 
termesztjük, mint a fogyasztási célból nevelteket. 

43
00:05:05,963 --> 00:05:11,380
Csak olyan növényeket válasszunk ki, amelyek 
az adott fajta tulajdonságait mutatják,

44
00:05:11,970 --> 00:05:16,260
beleértve a formát, a méretet, a levélszínt,

45
00:05:18,480 --> 00:05:19,927
illetve a szabályos felépítésű fejet! 

46
00:05:27,440 --> 00:05:28,705
A belga endíviák esetében

47
00:05:28,780 --> 00:05:33,192
jól fejlett gyökerű, illetve formás 
és tömör fejet növesztő egyedeket keressünk! 

48
00:05:39,047 --> 00:05:41,790
Az első évben magot hozó cikóriákat,

49
00:05:42,036 --> 00:05:46,036
amelyek nem a normális kétéves ciklus 
szerint fejlődnek, távolítsuk el!

50
00:05:47,600 --> 00:05:51,316
A következő években egyre hamarabb 
indulnának magszárba,

51
00:05:51,490 --> 00:05:54,952
ami a levélzöldségek esetében egyáltalán nem kívánatos. 

52
00:06:00,916 --> 00:06:06,283
A genetikai sokféleség biztosításához minimum 
10-15 növényről fogjunk magot! 

53
00:06:19,978 --> 00:06:24,814
Ősszel a leveleket betakaríthatjuk salátának, 
a gyökereket pedig tartsuk meg a magfogáshoz! 

54
00:06:33,323 --> 00:06:38,720
Az enyhe éghajlatú vidékeken a cikóriagyökerek 
a talajban is áttelelhetnek. 

55
00:06:43,105 --> 00:06:45,323
A zord klímájú területeken több megoldás

56
00:06:45,563 --> 00:06:48,850
közül választhatunk a magfogásra 
szánt növények átteleltetéséhez. 

57
00:06:53,760 --> 00:06:56,140
A tél kezdete előtt kiáshatjuk a gyökereket, néhány

58
00:06:57,883 --> 00:07:00,700
centiméterrel a gyökérnyak felett 
visszavágva, nyirkos homokban,

59
00:07:14,000 --> 00:07:18,363
vagy pincében elhelyezett edényekben 
tárolhatjuk őket. 

60
00:07:20,480 --> 00:07:28,254
80%-os páratartalom, és 0-4°C közötti 
hőmérséklet ideális az átteleltetéshez. 

61
00:07:30,094 --> 00:07:36,880
A cikóriát akár késő ősszel is vethetjük fűtetlen 
üvegházba, vagy enyhébb éghajlaton szabadföldbe.

62
00:07:38,290 --> 00:07:42,203
Ha tél előtt kis tőlevélrózsákat
 fejleszt, akkor

63
00:07:42,520 --> 00:07:45,149
jobban ellen tud állni a fagyoknak. 

64
00:07:46,887 --> 00:07:50,341
Ezután tavasszal fejet képez, majd nyáron virágzik. 

65
00:07:55,374 --> 00:08:00,210
A pincében átteleltetett gyökereket 
tavasszal újra elültetjük a kertben.

66
00:08:01,020 --> 00:08:03,323
A rothadásnak indult gyökereket távolítsuk el!

67
00:08:21,290 --> 00:08:24,101
Átültetés után alaposan öntözzük be őket! 

68
00:08:52,610 --> 00:08:58,640
A növények nem egyszerre virágoznak, és 
magas virágszárakat növesztenek, amiket ki kell karózni. 

69
00:09:25,480 --> 00:09:30,967
A cikória magjait az érésük ütemében 
kell betakarítani, ami hosszú ideig tarthat.

70
00:09:38,036 --> 00:09:41,854
Ne várjunk a magok betakarításával addig, 
amíg a növények teljesen elszáradnak,

71
00:09:42,020 --> 00:09:45,243
mert időközben a magok 
többsége elszóródik! 

72
00:09:54,840 --> 00:09:57,876
Akár a teljes fejet is betakaríthatjuk.

73
00:09:58,760 --> 00:10:02,400
Ebben az esetben a magok a betakarított 
növényen fejezik be az érésüket. 

74
00:10:05,825 --> 00:10:10,981
A magok szárítását mindenképp 
száraz, jól szellőző helyen fejezzük be. 

75
00:10:18,580 --> 00:10:22,290
Magfogás – tisztítás - tárolás

76
00:10:28,101 --> 00:10:31,680
Magfogáskor a növényeknek teljesen 
száraznak kell lenniük.

77
00:10:32,414 --> 00:10:36,080
A cikóriamagokat sokszor nem egyszerű 
elválasztani az őket körülvevő kis héjtól,

78
00:10:36,465 --> 00:10:39,287
de időnként könnyen a földre hullanak. 

79
00:10:44,298 --> 00:10:47,380
A száraz terméseket dörzsöljük 
erősen kézzel,

80
00:10:47,570 --> 00:10:48,792
vagy csépeljük ki egy bottal! 

81
00:10:52,458 --> 00:10:54,341
Ha a magok nem esnek ki a burokból,

82
00:10:54,509 --> 00:11:00,545
akkor egyéb mechanikai módszereket, például nyújtófát, 
traktorkereket vagy kalapácsot is használhatunk. 

83
00:11:04,843 --> 00:11:09,680
Egy másik módszerrel azokból a termésekből tudjuk 
kinyerni a magokat, amik nem nyílnak fel.

84
00:11:11,320 --> 00:11:15,220
Bámulatos eredményeket érhetünk el, 
ha a magokat néhány másodpercre

85
00:11:15,469 --> 00:11:17,060
kávé- vagy fűszerdarálóba tesszük.

86
00:11:19,700 --> 00:11:24,247
De vigyázzunk, nehogy túl sokáig 
daráljuk és porrá őröljük őket! 

87
00:11:30,430 --> 00:11:37,294
Különböző finomságú fémszitákkal tisztíthatjuk a magokat, 
és szabadulhatunk meg az ág- és szárdaraboktól, és a portól. 

88
00:11:43,949 --> 00:11:47,920
Végül a magokat ki kell szelelni, hogy 
a maradék törmeléket is eltávolítsuk.

89
00:11:49,243 --> 00:11:54,116
Fújjunk óvatosan rájuk egy tányér vagy 
egy szeleléshez használt kosár fölött! 

90
00:11:57,520 --> 00:12:00,414
Ezután öntsük a száraz magokat 
vízálló tasakba!

91
00:12:01,876 --> 00:12:05,105
Írjuk fel egy címkére a faj és a fajta 
nevét, valamint a magfogás évét,

92
00:12:05,294 --> 00:12:08,443
majd helyezzük azt 
a tasak belsejébe. 

93
00:12:13,112 --> 00:12:18,225
Néhány fagyasztóban töltött nap alatt 
a kártevők lárvái elpusztulnak. 

94
00:12:19,381 --> 00:12:23,490
A cikória magjai általában legalább 
5 évig megőrzik a csírázóképességüket.

95
00:12:23,980 --> 00:12:28,123
Ezt úgy tudjuk meghosszabbítani, ha 
a magokat fagyasztóban tároljuk. 
