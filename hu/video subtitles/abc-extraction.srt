﻿1
00:00:09,840 --> 00:00:14,960
A magok kinyerése, nedves feldolgozása, 
szárítása és válogatása

2
00:00:23,560 --> 00:00:28,520
Erjesztéssel történő nedves feldolgozás

3
00:00:34,680 --> 00:00:38,040
Az erjesztéssel történő nedves feldolgozást

4
00:00:38,200 --> 00:00:41,240
a paradicsom és az uborka esetében alkalmazzuk.

5
00:00:41,840 --> 00:00:46,880
Az erjesztés után eltávolítható a 
magokat körülvevő kocsonyás burok,

6
00:00:46,960 --> 00:00:49,760
ami a magnyugalmi állapot 
fenntartásáért felelős.

7
00:00:54,480 --> 00:00:58,000
A paradicsomot vagy az uborkát 
először félbevágjuk,

8
00:00:59,080 --> 00:01:05,680
majd a magokat és a nedves terméshúst 
kanállal egy befőttesüvegbe kaparjuk.

9
00:01:06,680 --> 00:01:09,760
Ha szükséges, adhatunk hozzá egy kis vizet is.

10
00:01:11,480 --> 00:01:15,480
Sérült vagy rothadó termésekből 
nem szabad magot fogni!

11
00:01:17,280 --> 00:01:21,640
Minden befőttesüvegre írjuk rá 
a faj és a fajta nevét!

12
00:01:23,080 --> 00:01:27,320
A befőttesüveg lehetővé teszi 
az erjesztés folyamatának nyomon követését.

13
00:01:28,000 --> 00:01:30,240
A befőttesüveget ne zárjuk le szorosan!

14
00:01:31,240 --> 00:01:36,000
Csak takarjuk le, 
szúnyoghálóval védjük a legyektől,

15
00:01:36,640 --> 00:01:43,680
és helyezzük meleg (23-30°C közötti) helyre, 
ahol nem éri közvetlen napfény.

16
00:01:49,720 --> 00:01:55,240
Az erjesztés időtartama 
a levegő hőmérsékletétől,

17
00:01:55,320 --> 00:01:58,320
valamint az erjesztendő 
folyadék cukortartalmától függ.

18
00:02:00,440 --> 00:02:05,160
Apránként fehér penészréteg 
jelenik meg a folyadék felszínén,

19
00:02:05,640 --> 00:02:07,880
amelyet többször is bele kell keverni a lébe

20
00:02:08,200 --> 00:02:10,840
az egyenletes erjesztés érdekében,

21
00:02:11,520 --> 00:02:15,160
illetve hogy elkerüljük a
 túl vastag penészréteg kialakulását.

22
00:02:17,400 --> 00:02:21,960
Egy csipet cukor hozzáadásával 
megelőzhetjük a káros penészfajok megjelenését,

23
00:02:22,560 --> 00:02:26,360
és aktiválhatjuk a folyamatot, 
ha nincs elegendő terméshús a lében.

24
00:02:31,800 --> 00:02:35,000
Kövessük nyomon a folyamatot!

25
00:02:35,720 --> 00:02:39,360
Meleg időben kevesebb mint 
48 óra is elegendő hozzá.

26
00:02:40,080 --> 00:02:45,040
Ha túl sokáig várunk, a kocsonyás 
burkuktól megfosztott magok

27
00:02:45,120 --> 00:02:48,560
akár ki is csírázhatnak,
és többé nem használhatóak fel vetőmagnak.

28
00:02:52,480 --> 00:02:55,960
A folyamat akkor van kész, 
ha a magok egymástól elválva

29
00:02:56,080 --> 00:02:59,080
a befőttesüveg aljára süllyedtek, 
a maradék terméshús és héj

30
00:02:59,640 --> 00:03:03,720
a lé tetejére úszott, a kocsonyás burok 
pedig teljesen lebomlott.

31
00:03:06,240 --> 00:03:07,880
Ekkor a magok készen állnak a tisztításra.

32
00:03:10,960 --> 00:03:14,800
Ehhez tegyük egy szűrőbe, majd 
folyó víz alatt mossuk át őket.

33
00:03:28,640 --> 00:03:32,360
Magkinyerés és nedves feldolgozás 
erjesztés nélkül

34
00:03:35,280 --> 00:03:38,560
Az erjesztés nélkül 
történő nedves feldolgozást

35
00:03:39,000 --> 00:03:41,920
a gyümölcstermő zöldségek esetében alkalmazzuk,

36
00:03:42,040 --> 00:03:44,600
mint például a padlizsán,

37
00:03:45,560 --> 00:03:48,280
a sütőtök, a főzőtök és a cukkini,

38
00:03:49,120 --> 00:03:50,840
a sárgadinnye vagy a görögdinnye.

39
00:03:54,640 --> 00:03:56,720
A magokat eltávolítjuk a termésből,

40
00:03:57,200 --> 00:04:00,480
majd egy szűrőedény 
segítségével folyó víz alatt átmossuk őket.

41
00:04:03,520 --> 00:04:06,840
Ha a magok nem válnak el 
könnyen a hústól,

42
00:04:07,360 --> 00:04:11,920
12-24 órára 
vízbe áztathatjuk őket,

43
00:04:12,560 --> 00:04:15,960
amíg a hús szétesik, és 
a magok szabaddá válnak.

44
00:04:17,080 --> 00:04:21,760
Az erjedés elkerülése érdekében 
nem szabad meleg helyre tenni őket.

45
00:04:22,520 --> 00:04:25,160
Az áztatás után a magokat 
azonnal meg kell szárítani.

46
00:04:34,240 --> 00:04:35,520
Szárítás

47
00:04:40,200 --> 00:04:43,720
A nedves feldolgozás után 
a magokat gyorsan,

48
00:04:45,720 --> 00:04:48,640
maximum 2 napon belül 
ki kell szárítani.

49
00:04:50,680 --> 00:04:56,320
Helyezzük apró lyukú szitára vagy 
egy tányérra őket egy szellős, száraz,

50
00:04:56,800 --> 00:05:00,520
23-30°C hőmérsékletű helyen.

51
00:05:06,720 --> 00:05:09,640
Szintén megoldás lehet kis magmennyiség esetén,

52
00:05:10,080 --> 00:05:13,680
ha a magokat olyan 
nedvszívó kávéfilterre helyezzük,

53
00:05:13,960 --> 00:05:15,840
amihez nem tapadnak oda.

54
00:05:16,600 --> 00:05:21,200
Egy-egy kávéfilterbe maximum 
egy teáskanál magot tegyünk.

55
00:05:23,640 --> 00:05:26,800
Alkoholos filccel írjuk fel a fajta és a faj

56
00:05:27,040 --> 00:05:30,360
nevét minden egyes filterre!

57
00:05:32,000 --> 00:05:34,520
A kávéfiltereket meleg, száraz, szellős helyen

58
00:05:34,600 --> 00:05:38,040
csíptessük fel szárítókötélre!

59
00:05:38,840 --> 00:05:41,560
A magokat 
nem szabad közvetlen napfénynek kitenni,

60
00:05:42,160 --> 00:05:44,280
vagy papíron szárítani,

61
00:05:44,600 --> 00:05:46,080
mert összetapadhatnak,

62
00:05:46,760 --> 00:05:48,840
és így nehéz lesz felszedni őket.

63
00:05:51,960 --> 00:05:54,920
Szedjük fel, majd dörzsöljük 
össze a kezünk között a magokat,

64
00:05:55,200 --> 00:05:57,120
hogy szétválasszuk őket egymástól!

65
00:06:18,240 --> 00:06:20,040
A magok válogatása

66
00:06:30,680 --> 00:06:33,560
Különböző módszerek léteznek 
a magok kinyerést követő válogatására.

67
00:06:34,720 --> 00:06:37,400
Vannak nedves és száraz módszerek is.

68
00:06:41,280 --> 00:06:46,080
Azokat, melyeket nem vesz körül terméshús 
(pl. a póréhagymák és a hagymák magjai)

69
00:06:46,520 --> 00:06:48,200
víz segítségével válogathatjuk ki.

70
00:06:49,800 --> 00:06:54,240
Ehhez nagy mennyiségű vizet 
öntünk egy átlátszó edénybe,

71
00:06:54,720 --> 00:06:56,480
majd beledobjuk a magokat.

72
00:06:58,880 --> 00:07:01,240
A vizet többször megkavarjuk,

73
00:07:01,760 --> 00:07:06,520
hogy a nehéz, termékeny magok 
az edény aljára süllyedjenek.

74
00:07:07,320 --> 00:07:10,720
A felúszó magokat és növényi törmeléket

75
00:07:11,360 --> 00:07:13,160
szűrővel lefölözzük.

76
00:07:18,040 --> 00:07:20,600
Ezután a vizet átöntjük egy szűrőn,

77
00:07:20,680 --> 00:07:23,440
hogy az edény aljára 
süllyedt magokat összegyűjtsük.

78
00:07:24,360 --> 00:07:26,280
Ezeket azonnal kiszárítjuk.

79
00:07:28,200 --> 00:07:32,760
Sok olyan nagyon könnyű mag van, 
amelyeket így nem lehet kiválogatni.

80
00:07:36,480 --> 00:07:39,880
A szárazon történő 
válogatás a legelterjedtebb módszer.

81
00:07:59,600 --> 00:08:02,240
A kézzel hántolt nagyméretű magoknál

82
00:08:02,320 --> 00:08:03,880
(mint például a babok esetében)

83
00:08:04,320 --> 00:08:07,600
csak el kell távolítanunk 
a hibás alakú, illetve sérült magokat.

84
00:08:12,840 --> 00:08:16,240
Az összes olyan növénynél, 
ahol a termést összetörjük vagy zúzzuk,

85
00:08:17,320 --> 00:08:19,080
a törmeléket el kell távolítani.

86
00:08:21,720 --> 00:08:24,800
A magokat először egy durva rostán 
kell átengedni, amin

87
00:08:25,280 --> 00:08:27,760
a legnagyobb törmelékdarabok fennmaradnak,

88
00:08:31,440 --> 00:08:34,560
a magok és a kisebb növényi 
részek átesnek egy vödörbe.

89
00:08:36,120 --> 00:08:39,320
A folyamatot megismételjük 
finom rostával,

90
00:08:39,760 --> 00:08:43,360
amelyen a magok fennakadnak, 
viszont a törmelékek átesnek.

91
00:08:50,640 --> 00:08:52,800
Fontos a megfelelő rostaméret megválasztása:

92
00:08:53,200 --> 00:08:57,160
a magoknak nem szabad átesniük, 
de a lehető legtöbb növényi törmeléknek át kell hullania.

93
00:09:06,240 --> 00:09:07,640
A tisztítás befejezéséhez

94
00:09:07,960 --> 00:09:10,360
a magokat egy lapos tárolóedénybe öntjük,

95
00:09:10,440 --> 00:09:13,600
majd finoman rájuk fújunk, 
hogy a könnyebb törmelékeket is eltávolítsuk.

96
00:09:17,760 --> 00:09:20,160
A szelet is felhasználhatjuk 
a magok válogatásához.

97
00:09:20,520 --> 00:09:22,320
Terítsünk egy nagy ponyvát a földre,

98
00:09:23,960 --> 00:09:28,160
majd erre öntsük a magokat, 
közben a szél elfújja a növényi törmelékeket.

99
00:09:30,000 --> 00:09:32,600
A szélnek egyenletesnek kell lennie,
mert az erősebb széllökések

100
00:09:32,680 --> 00:09:33,880
mindent elfújhatnak.

101
00:09:34,480 --> 00:09:36,400
Kisméretű ventilátort is használhatunk.

102
00:09:41,680 --> 00:09:46,080
A nehéz magok esetében 
kisebb kompresszor is alkalmazható,

103
00:09:46,560 --> 00:09:48,320
de a kisebb magokat ez már elfújja.

104
00:09:54,400 --> 00:09:56,520
Bármelyik módszert is használjuk,

105
00:09:56,760 --> 00:09:59,480
valamekkora veszteséggel mindig számolni kell.

106
00:09:59,800 --> 00:10:04,760
El kell döntenünk, hogy 
milyen mértékű válogatást szeretnénk megvalósítani.

107
00:10:32,320 --> 00:10:35,600
A természet rendkívül nagylelkű,
és ha elkezdünk magot termeszteni,

108
00:10:35,800 --> 00:10:39,320
hamarosan rájövünk, hogy hatalmas 
mennyiségű mag keletkezik,

109
00:10:39,880 --> 00:10:41,640
több mint amennyire szükségünk van.

110
00:10:43,280 --> 00:10:45,760
Ne próbáljunk minden magot megmenteni,

111
00:10:46,120 --> 00:10:47,320
hiszen mindig marad elegendő!
