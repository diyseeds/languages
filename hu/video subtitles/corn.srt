1
00:00:11,520 --> 00:00:16,996
A kukorica, a legtöbb gabonaféléhez hasonlóan 
a pázsitfűfélék (Poaceae) család tagja.

2
00:00:17,160 --> 00:00:19,774
A faj latin neve: Zea mays.

3
00:00:21,716 --> 00:00:23,643
Számos típusa ismert:

4
00:00:24,650 --> 00:00:27,912
- Csemegekukorica (Zea mays saccharata),

5
00:00:28,952 --> 00:00:34,630
amelyet a csövek éretlen állapotában nyersen, 
főzve vagy grillezve is fogyaszthatunk.

6
00:00:36,960 --> 00:00:39,520
A magjai összetöppednek a száradás során.

7
00:00:46,000 --> 00:00:48,618
Ezt a típust hűvösebb területeken is termesztik. 

8
00:00:53,280 --> 00:00:57,156
- Lófogú kukorica (Zea mays indentata),

9
00:00:57,640 --> 00:01:04,094
melynek magja keményítőt tartalmazó belső részből, 
és az azt körülvevő üvegszerű rétegből áll.

10
00:01:05,690 --> 00:01:09,323
A belső rész éréskor visszahúzódik, így

11
00:01:09,680 --> 00:01:13,505
alakul ki a metszőfoghoz hasonló 
alakja, amiről a nevét kapta.

12
00:01:15,050 --> 00:01:20,647
Ezt a típust lisztkészítésre és 
állati takarmányként használják. 

13
00:01:31,992 --> 00:01:35,476
- Simaszemű kukorica (Zea mays indurata),

14
00:01:37,934 --> 00:01:43,178
aminek magjában, a lófogú kukoricával ellentétben 
kicsi a keményítőtartalmú belső rész,

15
00:01:43,629 --> 00:01:50,000
viszont sokkal nagyobb az üvegszerű réteg. 
Főleg puliszka (polenta) készítésére használják. 

16
00:01:56,494 --> 00:02:05,614
- Pattogatni való kukorica (Zea mays microsperma vagy everta), 
melynek magjai szétrobbannak sütés közben. 

17
00:02:22,100 --> 00:02:23,100
Megporzás 

18
00:02:29,185 --> 00:02:30,836
A kukorica egylaki növény,

19
00:02:31,200 --> 00:02:35,192
vagyis a hímivarú és a nőivarú virágok 
ugyanazon a növényen találhatóak.

20
00:02:37,832 --> 00:02:42,603
A hímivarú virág (címer) a szár legtetején 
helyezkedik el,

21
00:02:44,450 --> 00:02:48,116
míg a nőivarú virág a szár közepén.
Ez utóbbi megduzzadt része tartalmazza

22
00:02:49,090 --> 00:02:56,014
a leendő kukoricacsövet, amiből 
kezdetben csak a kukoricabajusz nő ki. 

23
00:03:00,392 --> 00:03:03,301
Minden bajuszszál egy magkezdeményhez csatlakozik,

24
00:03:03,694 --> 00:03:08,152
amelyből a megporzást követően 
egy kukoricaszem jön létre. 

25
00:03:12,080 --> 00:03:17,083
A kukorica idegentermékenyülő növény, vagyis
különböző egyedek porozzák be egymást.

26
00:03:18,552 --> 00:03:23,585
Szélporozta növény, így 
a megporzása a szél segítségével történik.

27
00:03:23,949 --> 00:03:27,963
A virágport a szél akár 10 km-re is képes elszállítani. 

28
00:03:32,618 --> 00:03:36,283
A kukoricát csoportosan kell ültetni, 
legalább 3 sorba,

29
00:03:36,370 --> 00:03:39,890
hogy a szél megfelelően beporozza őket.

30
00:03:41,367 --> 00:03:43,130
Ha egy sorba ültetjük a növényeket,

31
00:03:43,345 --> 00:03:48,610
akkor a nem megfelelő beporzás következtében 
a kukoricacsövek nem fognak teljesen kifejlődni. 

32
00:03:51,149 --> 00:03:55,352
Bizonyos területeken a kukoricát a 
méhek is látogathatják

33
00:03:55,440 --> 00:03:58,843
az óriási mennyiségben termelt 
virágpora miatt.

34
00:04:00,349 --> 00:04:05,287
Egyetlen növény akár 18 millió 
pollenszemet is termelhet! 

35
00:04:10,749 --> 00:04:17,214
A keresztbeporzódás elkerüléséhez 
a kukorica fajtái közt legalább 3 km távolságot kell tartani. 

36
00:04:19,927 --> 00:04:26,378
Ezt 1 kilométerre csökkenthetjük, ha a 
fajták között van valami természetes akadály, pl. sövény. 

37
00:04:29,127 --> 00:04:32,058
Használhatjuk az időbeli izoláció módszerét is.

38
00:04:33,236 --> 00:04:39,643
Ha egy a kertben két kukoricafajtát szeretnénk termeszteni, 
a vetésidejüket néhány héttel toljuk el egymástól!

39
00:04:40,923 --> 00:04:45,287
Így elkerülhetjük, hogy az egyik fajta hímivarú 
virágainak pollenszórása

40
00:04:45,730 --> 00:04:50,385
egybeessen a másik fajta nőivarú 
virágainak megjelenésével.

41
00:04:51,701 --> 00:04:55,461
Ellenkező esetben a 
két fajta biztosan összeporzódik. 

42
00:04:56,858 --> 00:05:01,745
E technika alkalmazása során vegyük figyelembe 
a növények tenyészidejének hosszát,

43
00:05:02,276 --> 00:05:07,740
amely az adott fajta függvényében 
55-120 nap között változhat. 

44
00:05:09,781 --> 00:05:12,574
Szinte lehetetlen megvédeni a fajtánkat a környező

45
00:05:12,850 --> 00:05:17,607
területeken zajló nagyipari 
hibridkukorica-termeléstől. 

46
00:05:19,389 --> 00:05:24,778
Ilyenkor a kézi megporzás jelenti a megoldást.
Ebben az esetben érdemes nagyobb

47
00:05:26,378 --> 00:05:31,498
távolságot hagyni a sorok között, hogy 
a megporzást végző személy kényelmesen elférjen. 

48
00:05:34,720 --> 00:05:38,792
Ehhez erős, esőálló 
papírzacskókra van szükség. 

49
00:05:42,414 --> 00:05:46,450
A kukorica 10-14 napig virágzik.

50
00:05:48,814 --> 00:05:52,472
A kézi megporzás folyamata 3 napig tart. 

51
00:05:55,541 --> 00:05:59,825
Az első nap bezacskózzuk a nőivarú virágokat.

52
00:06:01,640 --> 00:06:06,167
Ezt közvetlenül a kukoricabajusz 
megjelenése előtt kell elvégeznünk,

53
00:06:07,338 --> 00:06:10,450
mert ha az már kibújt 
a kis csőkezdeményből,

54
00:06:10,610 --> 00:06:11,723
akkor elkéstünk. 

55
00:06:14,596 --> 00:06:18,850
Először a kis csőkezdeményt körülvevő 
csuhélevelek csúcsait levágjuk,

56
00:06:18,980 --> 00:06:22,356
hogy felfedjük az előbukkanó bajuszszálakat. 

57
00:06:31,498 --> 00:06:36,480
Majd a csövet betakarjuk, és a 
zacskót erősen rögzítjük a cső alapjánál. 

58
00:06:42,232 --> 00:06:45,450
A hímivarú virágokat a harmadik 
nap reggelén zacskózzuk be,

59
00:06:45,636 --> 00:06:48,203
amikor a portokok (a hím szaporítószervek)

60
00:06:48,400 --> 00:06:52,363
elkezdenek kiemelkedni a szár 
függőleges és oldalsó ágaiból. 

61
00:06:58,530 --> 00:07:02,625
Ha a portokok még zöldek, a 
bezacskózás megállíthatja a fejlődésüket. 

62
00:07:05,301 --> 00:07:09,830
Bezacskózás előtt rázzuk meg a növényeket, 
hogy az egyéb fajtákból a méhek vagy a szél

63
00:07:09,963 --> 00:07:13,229
segítségével esetlegesen rájuk került virágport eltávolítsuk! 

64
00:07:29,120 --> 00:07:34,349
A zacskót úgy kell rögzíteni, hogy 
a reggel termelt virágport összegyűjtse. 

65
00:07:49,476 --> 00:07:53,956
A legtöbb virágpor a harmat felszáradását
 követően, illetve délelőtt termelődik. 

66
00:07:57,280 --> 00:08:00,181
Ha párszor megütögetjük a virágokat, 
könnyebben lehullik. 

67
00:08:09,410 --> 00:08:14,196
A kézi megporzás dél körül történik 
ugyanazon a napon, mert a zacskóba került

68
00:08:14,298 --> 00:08:19,141
virágpor délutánra már túlságosan
 felmelegedhet, és elveszti az életképességét. 

69
00:08:20,523 --> 00:08:23,745
A délelőtt végén, még mielőtt elérkezne 
a nap legforróbb néhány órája,

70
00:08:24,247 --> 00:08:27,541
a virágporgyűjtő zacskókat kinyitjuk, 
a bennük található

71
00:08:37,970 --> 00:08:39,620
virágport összekeverjük, majd

72
00:08:41,512 --> 00:08:44,596
kinyitjuk az egyik csövet takaró zacskót.

73
00:08:44,741 --> 00:08:50,145
Az a jó, ha a kukoricabajusz körülbelül
 3-4 cm-t nőtt két nap alatt. 

74
00:08:50,480 --> 00:08:56,581
A virágport ecset segítségével felvisszük a 
szabadon álló bajuszszálakra azok teljes hosszában.

75
00:08:57,534 --> 00:09:00,930
Csövenként egy teáskanál virágporra van szükség. 

76
00:09:14,240 --> 00:09:19,360
A zacskót azonnal be kell zárni, elegendő 
helyet hagyva a cső körül a későbbi növekedéséhez. 

77
00:09:20,901 --> 00:09:23,912
A kukoricabajusz hetekig képes 
befogadni a virágport,

78
00:09:24,580 --> 00:09:28,305
így a zacskókat a betakarításig a csöveken kell hagyni. 

79
00:09:40,210 --> 00:09:41,460
Életciklus

80
00:10:00,900 --> 00:10:05,490
A kukorica egyéves növény, 
vagyis a csöveit egy év alatt hozza. 

81
00:10:07,389 --> 00:10:11,207
A magfogásra vetett kukoricát ugyanúgy
 termesztjük, mint a fogyasztásra nevelteket.

82
00:10:14,458 --> 00:10:20,101
Olyan kukoricafajtát érdemes választani, ami
 alkalmazkodott a helyi termesztési környezethez.

83
00:10:26,574 --> 00:10:32,167
A genetikai sokféleség biztosítása érdekében 
minimum 50 növényre van szükség,

84
00:10:33,090 --> 00:10:35,563
de az a legjobb, ha 200 növényünk van. 

85
00:10:48,720 --> 00:10:53,556
Olyan növényekről fogjunk magot, amik jól fejlődtek, 
és megfelelnek a kiválasztási kritériumoknak

86
00:10:53,890 --> 00:10:57,580
a méret, szín,

87
00:10:58,283 --> 00:11:01,020
növekedési erély, korai érés,

88
00:11:01,860 --> 00:11:05,540
a kukoricacső mérete 
és burkoltsága tekintetében! 

89
00:11:18,370 --> 00:11:20,618
A csöveket a növényen is száríthatjuk. 

90
00:11:23,658 --> 00:11:28,145
A kukorica akkor érett, ha a körmünket 
már nem tudjuk belenyomni a kukoricaszemekbe. 

91
00:11:32,581 --> 00:11:35,316
Ekkor a csöveket le lehet törni a szárról. 

92
00:11:41,900 --> 00:11:45,796
A csuhéleveleket húzzuk hátra, hogy a csövek jól

93
00:11:46,203 --> 00:11:49,730
látszódjanak, és száraz, szellős 
helyen tároljuk őket. 

94
00:12:10,036 --> 00:12:15,025
A teljes növényt is levághatjuk, hogy 
egyben szárítsuk tovább a pajtában. 

95
00:12:26,480 --> 00:12:29,956
Magfogás – tisztítás – tárolás

96
00:12:34,436 --> 00:12:38,880
A csöveket a kukoricaszemek alakja, 
a színük, a sorok száma,

97
00:12:39,018 --> 00:12:43,774
a szerkezetük, valamint 
a textúrájuk alapján válogassuk ki! 

98
00:12:51,230 --> 00:12:53,709
A genetikai sokféleség biztosítása érdekében

99
00:12:53,963 --> 00:12:59,003
a vetőmagnak szánt kukoricaszemeket 
sok csőről kell összeválogatni.

100
00:13:20,930 --> 00:13:24,596
A magokat érdemes a csövek
 középső részéről szedni. 

101
00:13:31,294 --> 00:13:36,269
A magokat dörzsöléssel távolítsuk el a csövekről! 
Közben ajánlott kesztyűt viselni. 

102
00:13:48,887 --> 00:13:51,781
Mindig írjuk fel egy címkére a fajta és a faj nevét,

103
00:13:51,898 --> 00:13:58,570
valamint a magfogás évét, majd helyezzük azt 
a tasak belsejébe! A külső felirat könnyen letörlődhet. 

104
00:14:02,460 --> 00:14:07,170
Néhány fagyasztóban töltött nap 
alatt a kártevők lárvái elpusztulnak. 

105
00:14:09,447 --> 00:14:16,589
A pattogatni való, a lófogú és a simaszemű 
kukorica magjai akár 5 évig is csíraképesek,

106
00:14:17,643 --> 00:14:21,883
de ez akár 10 évig is kitolható. 

107
00:14:22,560 --> 00:14:27,207
A csemegekukorica maximum
3 évig csíraképes. 

108
00:14:28,230 --> 00:14:31,687
Ezt úgy hosszabbíthatjuk meg,
ha a magokat a fagyasztóban tároljuk. 
