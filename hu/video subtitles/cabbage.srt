﻿1
00:00:11,040 --> 00:00:14,075
A fejes káposzta a káposztafélék (Brassicaceae) 
családjának tagja,

2
00:00:14,570 --> 00:00:17,565
latin neve Brassica oleracea,

3
00:00:19,715 --> 00:00:21,860
ezen belül a capitata alfajhoz tartozik.

4
00:00:22,845 --> 00:00:27,095
A Brassica oleracea fajba tartozik
 a karalábé, a brokkoli,

5
00:00:27,710 --> 00:00:33,725
a kelbimbó (bimbós kel), a leveles kel, 
a karfiol és a kelkáposzta is. 

6
00:00:38,300 --> 00:00:43,800
A fejes káposzták különböző színűek lehetnek, 
zöldek, fehérek vagy pirosak,

7
00:00:44,915 --> 00:00:48,920
és van fejük, amely vagy csúcsos, vagy kerek.

8
00:00:49,985 --> 00:00:55,490
Ennek az alfajnak sima levelei vannak, 
amelyek szorosan összetapadva fejet formálnak. 

9
00:01:06,560 --> 00:01:11,145
A B. oleracea fajba tartozó 
káposztafélék megporzása 

10
00:01:27,190 --> 00:01:31,130
A Brassica oleracea faj virágai kétivarúak,
 vagyis vannak hímivarú

11
00:01:34,035 --> 00:01:38,030
és nőivarú szaporítószerveik is.
A legtöbbjük önmeddő,

12
00:01:41,960 --> 00:01:44,460
az egyik növény virágai által

13
00:01:45,065 --> 00:01:50,075
termelt virágpor csak egy másik 
növényt képes megtermékenyíteni.

14
00:01:53,930 --> 00:01:56,075
Ezért a növények idegentermékenyülők.

15
00:01:57,085 --> 00:02:01,865
Az optimális megporzás érdekében 
ajánlott sok növényt ültetni. 

16
00:02:05,475 --> 00:02:08,490
A megporzást rovarok végzik.

17
00:02:11,895 --> 00:02:16,250
Ezek a tulajdonságok biztosítják 
a nagy genetikai változatosságot.

18
00:02:23,295 --> 00:02:27,615
A Brassica oleracea faj valamennyi alfaja

19
00:02:27,785 --> 00:02:29,340
képes egymással kereszteződni, ezért

20
00:02:31,235 --> 00:02:35,855
nem szabad különböző típusú káposztákat
 egymáshoz közel termeszteni, ha magfogás a célunk! 

21
00:02:40,370 --> 00:02:46,035
A genetikai tisztaság biztosításához 
a B. oleracea faj különböző fajtái

22
00:02:46,420 --> 00:02:49,645
között minimum 1 km-es izolációs távolságot hagyjunk! 

23
00:02:50,960 --> 00:02:53,780
Ezt 500 méterre csökkenthetjük, ha két fajta

24
00:02:54,020 --> 00:02:58,210
között van valamilyen 
természetes akadály, pl. sövény. 

25
00:03:01,560 --> 00:03:06,720
A fajtákat úgy is izolálhatjuk egymástól, 
ha zárt rovarháló alá kis kaptárakat teszünk

26
00:03:06,955 --> 00:03:09,155
rovarokkal együtt, vagy ha

27
00:03:14,310 --> 00:03:18,000
az egyes fajtákat takaró 
rovarhálókat felváltva nyitjuk és zárjuk.

28
00:03:22,485 --> 00:03:23,605
Bővebb információt

29
00:03:23,770 --> 00:03:28,090
erről az izolációs technikákról szóló modulban 
találsz "A magtermesztés ábécéje" fejezetben. 

30
00:03:41,380 --> 00:03:43,380
A káposzta életciklusa 

31
00:03:58,620 --> 00:04:02,220
A fejes káposzta kétéves növény.
A magfogás céljából vetett növényeket

32
00:04:02,475 --> 00:04:04,985
ugyanúgy termesztjük, mint a fogyasztásra nevelteket.

33
00:04:07,115 --> 00:04:08,270
Az első évben a

34
00:04:08,610 --> 00:04:12,195
növény áttelelő fejet fejleszt,

35
00:04:12,500 --> 00:04:14,415
virágot pedig a következő évben hoz. 

36
00:04:22,055 --> 00:04:25,735
A vetésidőt az éghajlati körülmények,
 az áttelelés módja,

37
00:04:25,935 --> 00:04:29,730
valamint a fajta koraisága határozza meg. 

38
00:05:24,900 --> 00:05:27,200
A káposztafejek átteleltetéséhez

39
00:05:27,390 --> 00:05:30,400
május közepén vagy június elején vessük el a magokat,

40
00:05:30,930 --> 00:05:33,530
a korai fajták esetében még később,

41
00:05:34,065 --> 00:05:38,996
hogy elkerüljük a fejek 
ősz végi túlfejlődését és megrepedését! 

42
00:05:42,923 --> 00:05:46,981
A kisebb méretű, de 
tömörebb fejek jobban áttelelnek. 

43
00:05:50,952 --> 00:05:57,294
Az első évben minimum 30 növényt kell teleltetni, 
hogy tél végére biztosan maradjon belőlük 10-15 darab!

44
00:06:06,196 --> 00:06:08,661
Magokat azokról az egészséges egyedekről

45
00:06:08,792 --> 00:06:12,036
fogjunk, amelyek növekedését végig
 figyelemmel tudtuk kísérni,

46
00:06:12,196 --> 00:06:15,450
így megismerve valamennyi tulajdonságukat! 

47
00:06:21,800 --> 00:06:23,992
A legerőteljesebb növekedésű fejeket

48
00:06:24,065 --> 00:06:27,352
a fajta tulajdonságai,
 illetve a saját kritériumaink alapján válasszuk ki,

49
00:06:27,498 --> 00:06:29,330
mint például:

50
00:06:30,160 --> 00:06:34,560
a szabályos és erőteljes növekedés, 
a tömör fejek képzése,

51
00:06:34,865 --> 00:06:40,734
a tárolhatóság, 
a rövid tenyészidő, illetve a hidegtűrés. 

52
00:06:43,563 --> 00:06:48,276
Az átteleltetés a magtermesztés 
leggyengébb láncszeme.

53
00:06:49,680 --> 00:06:54,996
A növények átteleltetésére számos módszer létezik 
az éghajlati körülmények, a vegetációs idő,

54
00:06:55,200 --> 00:06:59,018
valamint a rendelkezésre álló 
infrastruktúra függvényében. 

55
00:07:11,854 --> 00:07:14,200
Ahol a tél rendkívül kemény, ott

56
00:07:14,523 --> 00:07:20,072
a teljes növényt betakarítják 
ősz végén a gyökereivel együtt.

57
00:07:21,949 --> 00:07:25,949
A külső leveleket eltávolítják, hogy 
csak a szorosan

58
00:07:26,130 --> 00:07:28,414
tapadó kemény levelek maradjanak a fejen.

59
00:07:36,392 --> 00:07:39,578
A fejeknek száraznak és tisztának kell lenniük. 

60
00:07:44,080 --> 00:07:48,254
Az alacsony légnedvességű területeken 
a növényeket

61
00:07:48,850 --> 00:07:52,552
földpadlós pincében is tárolhatjuk. 

62
00:07:57,560 --> 00:08:04,196
Ahol a légnedvesség magas, a növényeket 
egy fagymentes szobában vagy a padláson lehet tárolni.

63
00:08:05,607 --> 00:08:10,007
A káposzták rövid ideig a -5° C-os 
hideget is kibírják,

64
00:08:10,145 --> 00:08:17,140
de hosszú távon nem szabad 
0° C alá süllyednie a hőmérsékletnek. 

65
00:08:19,280 --> 00:08:22,800
A tél folyamán ellenőrizni kell a káposztákat.

66
00:08:23,870 --> 00:08:29,440
A külső leveleket szürkepenész 
(Botrytis cinera) támadhatja meg.

67
00:08:30,320 --> 00:08:33,120
Ezeket el kell távolítani a 
rothadt részekkel együtt,

68
00:08:33,338 --> 00:08:37,149
majd pedig a sebeket 
fahamuval kell fertőtleníteni. 

69
00:08:40,370 --> 00:08:45,905
Ha enyhék a telek, vagy rendkívül 
ellenálló fajtát termesztünk, kint is hagyhatjuk

70
00:08:46,349 --> 00:08:48,930
a kertben, hogy 
a szabadban teleljenek át. 

71
00:08:52,843 --> 00:08:56,829
Az enyhébb éghajlatú vidékeken akár 
a földben is tárolhatóak a káposzták:

72
00:08:57,265 --> 00:09:00,814
ekkor a növényeket gyökerestül 
mély árkokba fektetjük,

73
00:09:01,000 --> 00:09:04,400
kissé felállítjuk, majd pedig talajjal takarjuk.

74
00:09:08,203 --> 00:09:10,552
A növények ne érjenek egymáshoz,

75
00:09:11,180 --> 00:09:15,200
és fagy esetén üvegtáblával, 
trágyával vagy

76
00:09:15,483 --> 00:09:17,890
avarral kell takarni őket. 

77
00:09:28,705 --> 00:09:33,789
Ezt a védőréteget tavasszal el kell távolítani, 
de a növényeket nem szabad átültetni.

78
00:09:34,276 --> 00:09:37,985
Az őket takaró talajon keresztülnőve 
fognak virágszárat hozni. 

79
00:09:47,040 --> 00:09:51,090
Egy másik megoldás, ha a gyökereket 
rakjuk el a fejek nélkül,

80
00:09:51,454 --> 00:09:52,647
amelyeket így meg lehet enni.

81
00:09:53,207 --> 00:09:56,203
Ekkor nyár végén, száraz időben

82
00:09:56,552 --> 00:10:00,014
a fejeket kissé sréhen levágjuk az alapjuknál,

83
00:10:00,980 --> 00:10:03,854
majd pedig csak a szárat és a gyökereket tartjuk meg. 

84
00:10:07,963 --> 00:10:12,363
Ezeket több napig szárítjuk, majd 
fertőtlenítjük fahamuval.

85
00:10:13,563 --> 00:10:18,145
A rothadás megelőzéséhez a száraz 
vágásfelületet oltóviaszba is márthatjuk. 

86
00:10:23,818 --> 00:10:28,123
Ez a szaporítási mód lehetővé teszi 
az átteleltetést, viszont

87
00:10:28,334 --> 00:10:33,127
az így megőrzött szárak kevesebb, 
és gyengébb minőségű magokat teremnek,

88
00:10:35,054 --> 00:10:38,058
hiszen nem tudnak a szár közepéből virágzani,

89
00:10:38,378 --> 00:10:42,072
ahonnan a legjobb minőségű magokat 
termő magszárak nőnének. 

90
00:10:47,360 --> 00:10:52,029
A magfogásra kiválasztott, pincében vagy
 padláson átteleltetett növényeket

91
00:10:52,530 --> 00:10:56,021
az életciklus második évének 
tavaszán, márciusban vagy

92
00:10:56,247 --> 00:10:57,927
áprilisban ültethetjük el újra. 

93
00:11:01,680 --> 00:11:08,327
Egymástól 60 cm távolságban ássuk el őket 
úgy, hogy a fejek teteje egy szintben legyen a talajjal.

94
00:11:09,360 --> 00:11:11,890
A növények új gyökereket növesztenek.

95
00:11:14,596 --> 00:11:18,269
Fontos a bőséges vízellátást biztosítása 
a növényeknek az ültetés során

96
00:11:18,480 --> 00:11:21,112
és a gyökérnövekedés időszaka alatt is. 

97
00:11:22,960 --> 00:11:26,283
A virágszárak kialakulásának elősegítése 
érdekében gyakran

98
00:11:26,560 --> 00:11:32,910
szükség van arra, hogy késsel kereszt alakú 
bemetszést ejtsünk

99
00:11:33,170 --> 00:11:34,327
a káposztafej tetején.

100
00:11:34,720 --> 00:11:39,980
A vágásnak 3-6 cm mélynek kell lennie 
a fej méretének függvényében.

101
00:11:41,541 --> 00:11:46,843
Ügyeljünk rá, nehogy megsértsük a káposzta alapját, 
ahonnan majd a magszárak fognak kinőni!

102
00:11:48,865 --> 00:11:55,098
Előfordulhat, hogy meg kell ismételni 
a bemetszést, ha nem jelennek meg virágszárak. 

103
00:12:11,440 --> 00:12:14,792
A központi virágszáron fejlődnek 
a legjobb minőségű magok.

104
00:12:17,032 --> 00:12:22,400
A gyengébb másodlagos szárakat eltávolíthatjuk, 
hogy a központi virágzat jobban

105
00:12:22,523 --> 00:12:27,905
fejlődhessen, illetve hogy a növény minden 
energiáját a magtermesztésre fordíthassa. 

106
00:13:03,250 --> 00:13:06,632
Mivel a magszárak akár 2 méter 
magasra is nőhetnek,

107
00:13:07,076 --> 00:13:09,629
minden növényt ki kell karózni,

108
00:13:09,956 --> 00:13:14,232
és a magszárakat rögzíteni kell, mert nagyon 
elnehezülhetnek a kifejlődött magok súlyától. 

109
00:13:35,992 --> 00:13:42,821
Magfogás a B. oleracea fajról,
a magok válogatása és tárolása 

110
00:13:51,832 --> 00:13:55,636
A magok akkor érettek, amikor a 
becőtermések bézs színűvé válnak.

111
00:13:59,810 --> 00:14:02,392
Érett állapotban a becők könnyen

112
00:14:02,560 --> 00:14:07,665
felnyílnak, és szétszórják 
a bennük található magokat. 

113
00:14:16,138 --> 00:14:20,218
Általában a szárak nem egyszerre érnek be.

114
00:14:21,207 --> 00:14:27,410
A magvesztés elkerüléséhez a magszárakat 
külön kell betakarítani, amint azok beértek.

115
00:14:28,920 --> 00:14:34,900
A teljes növényt is felszedhetjük még 
azelőtt, hogy minden mag teljesen beérne. 

116
00:14:37,992 --> 00:14:43,178
Ilyenkor az érési folyamat a szárítás végére 
fejeződik be, ami száraz és

117
00:14:43,320 --> 00:14:45,403
jól szellőző helyen zajlik. 

118
00:14:57,621 --> 00:15:03,781
A káposztafélék akkor állnak készen a magfogásra, 
amikor a becőtermések kézzel könnyen kinyithatóak. 

119
00:15:05,730 --> 00:15:07,130
A magok kinyeréséhez a

120
00:15:07,258 --> 00:15:12,574
becőterméseket terítsük szét műanyag fólián 
vagy vastag szöveten, majd

121
00:15:12,952 --> 00:15:15,789
ütögessük meg, vagy a kezünk között dörzsöljük össze őket.

122
00:15:19,200 --> 00:15:24,429
A terméseket zsákba is tehetjük, és puha 
felülethez ütögetve kicsépelhetjük a magokat. 

123
00:15:25,672 --> 00:15:30,589
Nagy mennyiség esetén lábbal vagy 
valamilyen járművel ki is taposhatjuk őket. 

124
00:15:42,960 --> 00:15:48,312
A nehezen felnyíló becőtermések 
valószínűleg éretlen magokat

125
00:15:48,509 --> 00:15:50,480
tartalmaznak, amelyek nem csíráznak jól. 

126
00:15:54,830 --> 00:15:56,232
A tisztítás során

127
00:15:56,480 --> 00:16:01,003
a törmelék eltávolításához először egy nagylyukú
rostán engedjük át a magokat,

128
00:16:01,345 --> 00:16:03,220
amelyen törmelék

129
00:16:07,832 --> 00:16:10,538
fennmarad, majd pedig egy finomabb szitán, amin

130
00:16:10,850 --> 00:16:15,716
a magok maradnak fenn, 
a kisebb törmelékek viszont átesnek. 

131
00:16:19,170 --> 00:16:24,865
Végül fújva vagy pedig a szél segítségével 
ki kell szelelnünk a magokat, hogy a

132
00:16:25,040 --> 00:16:27,636
maradék növényi részeket is eltávolítsuk. 

133
00:16:42,603 --> 00:16:47,745
Az összes Brassica oleracea fajba tartozó 
növény magja nagyon hasonló.

134
00:16:48,545 --> 00:16:55,258
Rendkívül nehéz megkülönböztetni őket, 
például a fejes káposzta és a karfiol magjait.

135
00:16:55,760 --> 00:16:58,821
Ezért fontos címkével ellátni a növényeket,

136
00:16:58,930 --> 00:17:02,734
majd a kinyert magokat, feltüntetve 
a faj és a fajta nevét,

137
00:17:03,010 --> 00:17:06,232
valamint a termesztés évét. 

138
00:17:07,447 --> 00:17:12,261
Néhány fagyasztóban töltött nap alatt 
valamennyi kártevő lárvája elpusztul. 

139
00:17:17,665 --> 00:17:20,850
A káposztafélék magjai 5 évig őrzik meg a csírázóképességüket,

140
00:17:21,854 --> 00:17:25,607
de ez akár 10 évig is kitolódhat.

141
00:17:27,338 --> 00:17:30,727
Úgy tudjuk meghosszabbítani, 
ha a magokat fagyasztóban tároljuk.

142
00:17:31,629 --> 00:17:38,196
Egy gramm fajtától függően 
250-300 darab magot tartalmaz. 
