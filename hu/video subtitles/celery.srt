﻿1
00:00:08,283 --> 00:00:11,140
A zeller az ernyősök (Apiaceae) családjába

2
00:00:11,576 --> 00:00:15,168
tartozik, latin neve Apium graveolens.

3
00:00:16,680 --> 00:00:22,100
Kétéves növény, amelyet a leveléért, 
gyökeréért, és száráért termesztenek.

4
00:00:22,980 --> 00:00:28,072
Három alfaja ismert:
szárzeller (dulce),

5
00:00:34,220 --> 00:00:38,036
a gumós zeller vagy gyökérzeller (rapaceum),

6
00:00:42,916 --> 00:00:45,643
valamint a levél-, vagy metélőzeller (secalinum).

7
00:00:51,178 --> 00:00:54,516
A zeller vad változata a vadzeller. 

8
00:01:01,340 --> 00:01:02,340
Megporzás 

9
00:01:15,170 --> 00:01:20,174
A zeller virágzata kisebb, 
általában kétivarú virágokból álló

10
00:01:20,349 --> 00:01:22,523
ernyővirágzat.

11
00:01:23,105 --> 00:01:27,949
A porzó (hímivarú szaporítószerv) 
a termő (nőivarú szaporítószerv) előtt

12
00:01:28,160 --> 00:01:29,992
érik, így egy virágon belül 

13
00:01:35,920 --> 00:01:41,163
nem történhet öntermékenyítés.

14
00:01:42,240 --> 00:01:45,905
Azonban mivel a virágok nem egyszerre nyílnak, 
az öntermékenyülés

15
00:01:46,138 --> 00:01:50,392
megvalósulhat egy ernyővirágzaton 
belül, illetve ugyanazon növény

16
00:01:51,018 --> 00:01:54,610
két különböző ernyője között. 

17
00:01:57,560 --> 00:02:02,254
Megtermékenyítés különböző növények 
ernyője között is előfordulhat. 

18
00:02:05,592 --> 00:02:09,287
A beporzást elsősorban rovarok végzik.

19
00:02:12,480 --> 00:02:16,100
A virágzó zeller rendkívül erős illatot áraszt,

20
00:02:16,232 --> 00:02:21,032
és nagy mennyiségű nektárt termel, 
ami sok rovart vonz. 

21
00:02:22,152 --> 00:02:25,280
A zeller valamennyi típusa kereszteződhet egymással,

22
00:02:25,600 --> 00:02:29,520
sőt a vadzellerrel is, ami 
vízparti területeken él.

23
00:02:29,723 --> 00:02:33,214
Nagyon ritkán a petrezselyemmel is kereszteződhet. 

24
00:02:35,585 --> 00:02:37,440
A keresztbeporzódás elkerüléséhez

25
00:02:37,847 --> 00:02:43,032
a különböző zellerfajták között 1 kilométeres 
izolációs távolságot kell tartani. 

26
00:02:45,840 --> 00:02:50,603
Ezt 500 méterre csökkenthetjük, ha a fajták 
között van valamilyen

27
00:02:50,756 --> 00:02:54,140
természetes akadály, pl. sövény. 

28
00:03:02,770 --> 00:03:09,221
A fajtákat úgy is izolálhatjuk egymástól, 
ha zárt rovarháló alá kis kaptárakat teszünk rovarokkal

29
00:03:10,021 --> 00:03:15,025
együtt, vagy ha az egyes fajtákat 
takaró rovarhálókat felváltva nyitjuk és zárjuk.

30
00:03:16,647 --> 00:03:17,730
Bővebb információt ezzel kapcsolatban

31
00:03:17,840 --> 00:03:22,305
az izolációs technikákról szóló modulban 
találsz A magtermesztés ábécéje fejezetben. 

32
00:03:27,818 --> 00:03:29,220
Életciklus

33
00:03:40,494 --> 00:03:42,909
Minden zellerfajta kétéves növény.

34
00:03:43,498 --> 00:03:45,330
A termesztés első évében a magfogás

35
00:03:45,530 --> 00:03:49,898
céljából vetett zellereket ugyanúgy kell termeszteni, 
mint a fogyasztási célból nevelteket.

36
00:03:52,290 --> 00:03:54,727
Magot a második évben hoznak. 

37
00:04:50,843 --> 00:04:55,890
Különböző módszerek léteznek a magfogás céljából 
termesztett zellerek téli tárolására.

38
00:05:00,203 --> 00:05:04,778
Enyhébb éghajlatú területen kint 
lehet hagyni őket a kertben, de

39
00:05:06,276 --> 00:05:11,083
fagyvédő takaróval vagy szalmával 
kell védeni a növényeket.

40
00:05:11,810 --> 00:05:14,203
A szalmát tavasszal el kell távolítani a tövekről. 

41
00:05:18,160 --> 00:05:21,470
Hidegebb éghajlatú területeken a 
zellert gyökerestül fel kell

42
00:05:21,600 --> 00:05:24,160
szedni a kemény fagyok érkezése előtt.

43
00:05:30,029 --> 00:05:33,258
A leveleket vágjuk vissza néhány 
centiméteresre a gyökérnyak felett!

44
00:05:34,430 --> 00:05:38,334
Minél kevesebb vizet tartalmaznak, 
annál tovább lehet őket tárolni. 

45
00:05:42,290 --> 00:05:44,540
Minden típusú zellert az adott fajta

46
00:05:44,640 --> 00:05:48,952
tulajdonságai alapján kell szelektálni:

47
00:05:58,420 --> 00:06:02,625
a gyökérzellernél a szín, az alak és az íz alapján;

48
00:06:15,592 --> 00:06:19,460
a szárzellernél a 
szárak mérete és színe alapján;

49
00:06:20,007 --> 00:06:24,552
a levélzellernél pedig a levelek 
mennyisége, illetve íze alapján. 

50
00:06:28,560 --> 00:06:32,276
Ezután a gyökereket fagymentes 
helyen található, homokkal töltött ládában

51
00:06:32,676 --> 00:06:35,236
helyezzük el úgy, hogy ne érjenek egymáshoz. 

52
00:06:45,640 --> 00:06:47,207
A tél folyamán rendszeresen ellenőrizzük

53
00:06:47,300 --> 00:06:51,912
a gyökereket, és távolítsuk el azokat, 
amelyek elkezdenének rothadni! 

54
00:07:12,720 --> 00:07:16,189
A gyökereket az erősebb fagyok 
veszélyének elmúltával

55
00:07:16,487 --> 00:07:19,483
tavasz elején visszaültetjük a földbe.

56
00:07:22,770 --> 00:07:27,061
Ha a magfogásra szánt zellereket közel ültetjük 
egymáshoz, akkor kevesebb

57
00:07:27,338 --> 00:07:33,207
harmadlagos ernyőt hoznak, így kevesebb 
gyenge minőségű magunk lesz. 

58
00:07:34,320 --> 00:07:38,872
Körülbelül 15 növény szükséges a 
genetikai sokféleség biztosításához.

59
00:07:40,632 --> 00:07:45,730
Arról is gondoskodni kell, hogy a 
visszaültetett gyökerek ne száradjanak ki. 

60
00:08:05,720 --> 00:08:10,436
A zeller több ernyőt is növeszt, 
amelyek azonban nem egyszerre nyílnak.

61
00:08:11,316 --> 00:08:15,534
Az először megjelenő elsődleges ernyő 
a központi szár csúcsán található. 

62
00:08:17,230 --> 00:08:21,389
A másodlagos ernyők a központi szárból nőnek ki. 

63
00:08:22,210 --> 00:08:28,167
A harmadlagos ernyők a 
másodlagos szárakon jönnek létre. 

64
00:08:45,120 --> 00:08:48,356
A legjobb minőségű magok az elsődleges ernyőkben vannak.

65
00:08:48,865 --> 00:08:53,040
A másodlagos ernyőket csak 
szükség esetén gyűjtsük be. 

66
00:08:55,600 --> 00:08:58,625
A zellermagok akkor érettek, ha a színük barnára változik.

67
00:08:59,520 --> 00:09:05,105
A magokat akkor kell begyűjteni, 
amikor a legtöbb elsődleges ernyő elkezd barnulni. 

68
00:09:08,807 --> 00:09:12,254
Az érett magok könnyen kiperegnek.

69
00:09:13,105 --> 00:09:18,232
Szeles vagy esős időjárás esetén 
az ernyőket a teljes érettség előtt takarítsuk be!

70
00:09:23,236 --> 00:09:29,432
Száraz, jól szellőző helyen fejezzük be a szárítást! 

71
00:09:40,232 --> 00:09:43,156
Magfogás - tisztítás - tárolás

72
00:09:56,778 --> 00:10:00,720
Viseljünk kesztyűt a magok ernyőből 
történő kinyerése során! 

73
00:10:11,054 --> 00:10:15,032
A tisztítás során olyan rostákat kell használni, 
amiken fennmaradnak a törmelékek. 

74
00:10:30,836 --> 00:10:36,930
Végül a magokat ki kell szelelni fújással, hogy a 
maradék törmeléktől is megszabaduljunk. 

75
00:10:50,421 --> 00:10:53,796
Mindig írjuk fel egy címkére a fajta és 
a faj nevét, valamint a magfogás évét,

76
00:10:53,910 --> 00:11:00,210
majd helyezzük azt a tasak belsejébe! 
A külső felirat könnyen letörlődhet. 

77
00:11:07,316 --> 00:11:12,589
Néhány fagyasztóban töltött nap 
alatt a kártevők lárvái elpusztulnak. 

78
00:11:13,800 --> 00:11:17,447
A zellermagok akár 8 évig is megőrzik a csírázóképességüket.

79
00:11:18,090 --> 00:11:22,669
Ez akár 10 évig is kitolható, 
ha a magokat

80
00:11:23,660 --> 00:11:27,432
fagyasztóban tároljuk. 

81
00:11:28,392 --> 00:11:31,709
Egy grammban körülbelül 2000 mag található. 

82
00:11:33,425 --> 00:11:36,218
A zellermagok csírázása szabálytalan lehet.

83
00:11:36,900 --> 00:11:40,465
Úgy tűnik, hogy egy bizonyos ideig nyugalmi 
(alvó) állapotban maradnak. 
