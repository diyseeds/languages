﻿1
00:00:09,030 --> 00:00:12,676
Az uborka a tökfélék (Cucurbitaceae) családjába tartozik.

2
00:00:12,820 --> 00:00:16,160
A faj latin neve: Cucumis sativus.

3
00:00:16,800 --> 00:00:19,080
Öt típusa létezik:  

4
00:00:20,400 --> 00:00:21,980
szabadföldön termesztett uborkák,

5
00:00:24,300 --> 00:00:26,700
melyek héján kis tüskék vannak;  

6
00:00:30,900 --> 00:00:34,420
üvegházban termesztett uborkák, amelyek héja rendkívül sima; 

7
00:00:38,760 --> 00:00:42,000
„Szikkim” típusú uborkák piros-narancssárga héjjal;

8
00:00:45,900 --> 00:00:50,720
nagyon apró termésű, feldolgozásra szánt fajták 
(pl. a savanyítani való uborkák);  

9
00:00:52,880 --> 00:00:54,630
gömbölyű uborkák.  

10
00:01:07,680 --> 00:01:08,440
Megporzás  

11
00:01:20,880 --> 00:01:23,598
Az uborka egylaki növény, vagyis

12
00:01:24,400 --> 00:01:28,880
a hímivarú és a nőivarú virágok 
is ugyanazon az egyeden találhatóak.

13
00:01:31,470 --> 00:01:34,770
A nőivarú virágok esetében magház található a virág alatt.

14
00:01:37,900 --> 00:01:42,400
Valójában ez egy mini uborka, 
ami a megporzást követően kezd el fejlődni.

15
00:01:46,150 --> 00:01:51,730
Először a hímivarú virágok jelennek meg.
Ezek a hosszú szárak végén találhatóak.

16
00:01:56,050 --> 00:01:58,900
A virágok csak egy napra nyílnak ki. 

17
00:02:00,280 --> 00:02:05,500
Az uborkák öntermékenyek, vagyis egy 
nőivarú virágot egy olyan hímivarú virág

18
00:02:05,700 --> 00:02:09,520
virágpora is megtermékenyíthet, ami 
ugyanazon a növényen található.

19
00:02:13,570 --> 00:02:15,550
De gyakoribb a keresztbeporzódás.

20
00:02:16,780 --> 00:02:21,250
Az uborkák virágait rovarok, 
elsősorban méhek porozzák be.

21
00:02:29,160 --> 00:02:34,980
A Cucumis sativus faj valamennyi 
fajtája keresztbeporzódik egymással.

22
00:02:36,960 --> 00:02:42,090
Viszont az uborkák nem kereszteződnek
 a sárgadinnyékkel, a görögdinnyékkel, és a tökökkel sem. 

23
00:02:44,440 --> 00:02:50,620
A keresztbeporzás elkerülése érdekében 
a fajták között tartsunk 1 kilométeres távolságot!

24
00:02:53,360 --> 00:02:58,490
Ezt 500 méterre csökkenthetjük, ha a fajták 
között van valamilyen természetes

25
00:02:58,690 --> 00:02:59,450
akadály, pl. sövény. 

26
00:03:02,450 --> 00:03:06,890
Számos módszer létezik arra, hogy különböző 
uborkafajtákról tudjunk magot fogni

27
00:03:07,220 --> 00:03:08,510
egy a kerten belül.

28
00:03:11,630 --> 00:03:15,547
Például az egyik fajta valamennyi egyedét 
lefedhetjük rovarhálóval,

29
00:03:16,140 --> 00:03:18,800
és kis poszméhkaptárat 
helyezhetünk el a hálón belül.

30
00:03:22,220 --> 00:03:26,660
Két fajtát külön-külön is letakarhatunk 
rovarhálóval, és

31
00:03:28,240 --> 00:03:33,500
egyik nap csak az egyiket nyitjuk ki, 
másik nap pedig a másikat.

32
00:03:34,340 --> 00:03:36,500
Így a vadon élő rovarok elvégezhetik a munkájukat.

33
00:03:37,460 --> 00:03:41,570
Így kevesebb termésünk lesz, mivel bizonyos 
virágok nem lesznek megtermékenyítve.

34
00:03:47,810 --> 00:03:50,180
A virágok kézi megtermékenyítése is egy lehetőség.

35
00:03:50,930 --> 00:03:55,430
Az uborkánál ez sokkal aprólékosabb munkát kíván, 
mint a tökök vagy a cukkini esetében,

36
00:03:55,940 --> 00:03:58,400
mivel a virágai jóval kisebbek.

37
00:04:01,070 --> 00:04:03,098
Bővebb információt ezekkel kapcsolatban a

38
00:04:03,180 --> 00:04:06,240
fizikai izolációs technikákról, 
valamint a kézi beporzásról

39
00:04:06,243 --> 00:04:10,040
szóló modulokban találsz 
“A magtermesztés ábécéje” című videóban. 

40
00:04:22,500 --> 00:04:23,140
Életciklus  

41
00:04:39,600 --> 00:04:44,220
A magfogás céljából vetett uborkákat ugyanúgy 
kell termeszteni, mint

42
00:04:44,340 --> 00:04:45,720
a fogyasztási célból nevelteket.

43
00:05:01,590 --> 00:05:06,300
A genetikai sokféleség biztosításához fajtánként 
legalább 6, ideálisan 12

44
00:05:07,440 --> 00:05:08,790
növényt kell termeszteni. 

45
00:05:33,790 --> 00:05:38,075
Figyeljünk rá, hogy a magfogásra szánt 
egyedeket a fajtára jellemző egyedi

46
00:05:38,220 --> 00:05:40,870
tulajdonságok alapján válasszuk ki!

47
00:05:42,100 --> 00:05:46,450
Az intenzív növekedésű, jól fejlett uborkákat 
termő növényekből válasszunk!

48
00:05:48,520 --> 00:05:50,110
A beteg egyedektől szabaduljunk meg!

49
00:06:04,260 --> 00:06:08,827
A magfogásra és a fogyasztási célra szánt 
uborkákat eltérő érettségi

50
00:06:08,960 --> 00:06:10,140
állapotban kell szedni.

51
00:06:10,430 --> 00:06:13,500
Fogyasztani még éretlen állapotban szoktuk az uborkát. 

52
00:06:14,250 --> 00:06:18,352
A magfogáshoz elengedhetetlen, hogy az 
uborkát hagyjuk addig fejlődni,

53
00:06:18,560 --> 00:06:20,040
amíg teljesen beérik:

54
00:06:22,870 --> 00:06:27,040
eléri a maximális méretét, 
és megváltozik a színe.

55
00:06:34,060 --> 00:06:37,540
A termést röviddel a teljes érettség elérése 
előtt is betakaríthatjuk.

56
00:06:38,140 --> 00:06:42,160
Ilyenkor az uborkát hagyjuk 
meleg helyen tovább érni.

57
00:06:42,970 --> 00:06:46,090
Ez a módszer növeli a magok termékenységét. 

58
00:06:55,790 --> 00:06:59,100
Magfogás - tisztítás - tárolás  

59
00:07:07,160 --> 00:07:09,920
A magok kinyeréséhez vágjuk fel az uborkát,


60
00:07:12,220 --> 00:07:13,960
távolítsuk el a belső pépes részt a magokkal,

61
00:07:25,020 --> 00:07:27,520
majd néhány napig hagyjuk erjedni!

62
00:07:32,250 --> 00:07:36,990
Így megszabadulhatunk a magokat 
körülvevő kocsonyás buroktól.

63
00:07:40,340 --> 00:07:44,990
Bővebb információt erről a nedves feldolgozási 
technikákról szóló modulban

64
00:07:45,190 --> 00:07:46,790
találsz “A magtermesztés ábécéjé"-ben.

65
00:07:53,520 --> 00:07:57,060
Ezt követően a magokat tegyük szűrőbe, 
és öblítsük át folyó vízzel!

66
00:08:00,850 --> 00:08:03,748
A léha magoktól úgy szabadulhatunk meg,

67
00:08:04,420 --> 00:08:07,300
hogy minden magot egy vízzel teli edénybe teszünk.

68
00:08:07,630 --> 00:08:12,460
Az életképes nehezebb magok lesüllyednek 
az edény aljára, az üresek fent maradnak. 

69
00:08:16,320 --> 00:08:21,240
Távolítsuk el az üres magokat, öblítsük le a többit, 
és jól szellőző helyiségben szárítsuk meg őket!

70
00:08:26,730 --> 00:08:28,980
Dörzsöljük össze őket, hogy elváljanak egymástól!

71
00:08:32,490 --> 00:08:37,920
Ellenőrzésképpen próbáljuk meghajlítani őket: 
a száraz magok ilyenkor eltörnek. 

72
00:08:43,820 --> 00:08:46,060
Írjuk fel egy címkére a faj és a fajta 
nevét, valamint a magfogás évét,

73
00:08:46,280 --> 00:08:51,180
majd helyezzük azt a tasak belsejébe!

74
00:08:51,440 --> 00:08:53,960
A külső felirat könnyen letörlődhet.

75
00:09:01,310 --> 00:09:05,630
Néhány fagyasztóban töltött nap alatt 
valamennyi kártevő lárvája elpusztul.

76
00:09:09,790 --> 00:09:15,460
Az uborkamagok 6 évig őrzik meg a csírázóképességüket, 
időnként akár tovább is.

77
00:09:18,340 --> 00:09:21,070
Úgy tudjuk meghosszabbítani, ha a magokat 
fagyasztóban tároljuk.

78
00:09:23,110 --> 00:09:27,250
Egy gramm 30-40 magot tartalmaz. 
