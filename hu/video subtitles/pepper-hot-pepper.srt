1
00:00:12,100 --> 00:00:16,780
Az édes paprika és a csípős paprika 
a burgonyafélék (Solanaceae) családjába tartozik.

2
00:00:18,065 --> 00:00:22,390
A Capsicum nemzetség 
5 termesztett fajt tartalmaz:

3
00:00:22,940 --> 00:00:34,205
a bogyós (Capsicum baccatum), a kínai (Capsicum chinense),
a cserjés (Capsicum frutescens), illetve szőrös paprika (Capsicum pubescens).

4
00:00:35,580 --> 00:00:38,860
De a legtöbb termesztett fajta

5
00:00:39,155 --> 00:00:42,060
a közönséges paprika (Capsicum annum) fajba tartozik,

6
00:00:46,185 --> 00:00:51,060
amelynek több ezer változata ismert,
beleértve az édes paprikákat,

7
00:00:53,300 --> 00:00:56,980
a közepesen erős chilipaprikákat és a rendkívül csípős chiliket.

8
00:00:58,915 --> 00:01:04,090
Rendkívül változatos méretűek, 
alakúak, illetve színűek lehetnek.

9
00:01:07,435 --> 00:01:08,340
Megporzás

10
00:01:21,440 --> 00:01:26,045
A paprikának kétivarú 
önbeporzó virágai vannak,

11
00:01:26,185 --> 00:01:31,980
vagyis a hímivarú és nőivarú szaporítószervek 
ugyanabban a virágban találhatóak, és kompatibilisek egymással.

12
00:01:35,380 --> 00:01:37,615
Ezért a paprika önbeporzónak tekintendő.

13
00:01:40,220 --> 00:01:43,380
Ennek ellenére a paprikát rovarok,

14
00:01:43,440 --> 00:01:47,490
például poszméhek vagy 
méhek is beporozhatják. 

15
00:01:51,565 --> 00:01:54,120
A paprikák érzékenyek a hőmérséklet változásaira.

16
00:01:55,350 --> 00:02:02,575
Ha este túl magas (29°C) vagy éppenséggel 
túl alacsony (5°C) a hőmérséklet,

17
00:02:02,740 --> 00:02:06,540
akkor a virágok nem kötnek, 
vagyis nem termékenyülnek meg.

18
00:02:07,500 --> 00:02:11,765
Ennek következtében vagy csak kevés, vagy 
egyetlen mag sem lesz a paprikákban,

19
00:02:12,235 --> 00:02:15,015
ami nagyban befolyásolja 
a termés méretét.

20
00:02:22,370 --> 00:02:29,020
A terméskötődés szempontjából a 
12-16°C közötti éjszakai hőmérséklet a legoptimálisabb. 

21
00:02:32,910 --> 00:02:35,100
Az önbeporzás elősegítése érdekében

22
00:02:35,420 --> 00:02:39,320
érdemes rendszeresen megrázogatni
a növényeket a virágzás idején. 

23
00:02:42,140 --> 00:02:46,825
A Capsicum nemzetség valamennyi faja 
keresztbeporzódhat egymással,

24
00:02:47,510 --> 00:02:50,840
kivéve a
szőrös paprikát (Capsiscum pubescens). 

25
00:02:58,215 --> 00:03:02,935
A keresztbeporzódás megelőzése érdekében 
a mérsékelt égövön hagyjunk

26
00:03:03,490 --> 00:03:06,050
100 m izolációs távolságot két fajta között. 

27
00:03:07,550 --> 00:03:13,910
Ezt 50 m-re csökkenthetjük, ha valamilyen 
természetes akadály, például sövény található a fajták között. 

28
00:03:18,080 --> 00:03:22,925
A trópusi területeken 1 km-es
izolációs távolságot kell tartani, 

29
00:03:23,600 --> 00:03:26,780
ami 500 méterre csökkenthető. 

30
00:03:31,485 --> 00:03:37,505
A rovarok általi keresztbeporzás 
elkerülése érdekében a növényeket

31
00:03:38,300 --> 00:03:42,120
alagútszerűen kialakított vagy fix rovarhálók
használatával is izolálhatjuk egymástól.

32
00:03:44,470 --> 00:03:47,840
De vigyázzunk, mert a paprikáknak rendkívül nagy a fényigénye.

33
00:03:48,780 --> 00:03:53,140
A túl szoros, illetve az alulméretezett
izolációs ketrecek akadályozzák a növények fejlődését,

34
00:03:53,680 --> 00:03:56,290
és csökkentik a termés, illetve a magok mennyiségét.

35
00:03:59,505 --> 00:04:04,815
Bővebb információt erről az izolációs technikákról
szóló modulban találsz "A magtermesztés ábécéje" fejezetben. 

36
00:04:14,300 --> 00:04:15,340
Életciklus 

37
00:04:31,505 --> 00:04:34,500
A paprika alapvetően egyéves növény,

38
00:04:36,880 --> 00:04:40,180
de bizonyos fajok évelőként is 
termeszthetőek a trópusi területeken.

39
00:04:45,395 --> 00:04:48,510
A paprikának rengeteg hőre 
van szüksége a megfelelő fejlődéshez. 

40
00:04:54,820 --> 00:04:59,340
A magfogás céljából ültetett paprikákat ugyanúgy 
kell termeszteni, mint a fogyasztásra szántakat. 

41
00:05:10,220 --> 00:05:15,865
A genetikai sokféleség biztosításához
fajtánként minimum 6-12 növényt termesszünk. 

42
00:05:31,315 --> 00:05:36,490
Fajtától függően a virágzástól számítva 
60-100 nap szükséges

43
00:05:36,780 --> 00:05:41,185
a fogyasztásra alkalmas
termés kifejlődéséhez. 

44
00:05:55,005 --> 00:05:57,390
A magfogásra kiválasztott növényeknek

45
00:05:57,520 --> 00:05:59,940
az egészséges, erőteljes növekedésű egyedek közül kell kikerülniük,

46
00:06:00,075 --> 00:06:03,215
amelyek fejlődését végig 
nyomon tudtuk követni, 

47
00:06:03,645 --> 00:06:06,760
és megfelelnek 
a kiválasztási kritériumoknak. 

48
00:06:09,035 --> 00:06:14,685
Olyanokat válasszunk, amik szabályos,
erőteljes növekedésűek, bőségesen hoznak virágokat,

49
00:06:15,155 --> 00:06:19,190
jól kötődnek, és erős ágakkal 
rendelkeznek, amelyek nem törnek le. 

50
00:06:19,590 --> 00:06:22,860
A termésnél fontos, hogy jó ízűek legyenek,

51
00:06:23,095 --> 00:06:29,820
és jól mutassák a fajtára jellemző
formát, méretet, színt, hús- és héjvastagságot! 

52
00:06:31,655 --> 00:06:35,430
Ne gyűjtsünk már betakarított 
paprikákból magot,

53
00:06:35,960 --> 00:06:39,580
mert így már nem tudjuk a fajta 
növekedésével kapcsolatos

54
00:06:39,650 --> 00:06:41,915
valamennyi tulajdonságot ellenőrizni. 

55
00:06:46,300 --> 00:06:49,775
A magok betakarításához
várjuk meg a teljes érettség állapotát.

56
00:06:52,575 --> 00:06:57,660
Ekkor a zöld termések pirosra, barnára, 
narancssárgára vagy sárgára színeződnek,

57
00:06:59,785 --> 00:07:04,900
a halványsárga színű termések pedig 
sötétsárgák, narancssárgák vagy pirosak lesznek. 

58
00:07:06,395 --> 00:07:10,025
Ekkor a magok sárgák és érettek. 

59
00:07:12,760 --> 00:07:15,275
Ne fogjunk magot éretlen termésekből,

60
00:07:15,405 --> 00:07:18,650
ezek csírázóképessége 
sokkal kisebb lesz. 

61
00:07:22,875 --> 00:07:27,380
A legjobb az, ha a növény legelső 
érett terméseiből fogunk magot.

62
00:07:29,880 --> 00:07:34,215
A később beérő termésekből gyűjtött 
magok csírázóképessége alacsonyabb lehet.

63
00:07:37,065 --> 00:07:39,955
Beteg paprikákból ne fogjunk magot. 

64
00:07:49,570 --> 00:07:52,460
Magkinyerés – válogatás – tárolás

65
00:08:02,205 --> 00:08:03,090
Figyelem!

66
00:08:03,590 --> 00:08:09,180
Fontos, hogy a csípős chilimagokat 
jól szellőző helyen nyerjük ki,

67
00:08:09,935 --> 00:08:14,105
ha lehetséges szabad levegőn, 
hogy elkerüljük a kapszaicin kiáramlását,

68
00:08:15,525 --> 00:08:19,840
mivel az szem-, torok- 
és orr irritációt okozhat.

69
00:08:24,750 --> 00:08:29,440
Továbbá fontos vastag gumikesztyűt 
és munkavédelmi szemüveget viselni. 

70
00:08:34,945 --> 00:08:38,830
Vágjuk ketté a paprikákat, és kés 
segítségével távolítsuk el a magokat.

71
00:08:41,440 --> 00:08:47,305
Tegyük őket egy vízzel teli tálba,
az üres (léha) magok fent maradnak.

72
00:08:53,360 --> 00:08:54,815
Ezeket szűrővel távolítsuk el. 

73
00:09:06,210 --> 00:09:10,320
A jó magokat folyóvíz alatt 
tisztítsuk meg egy szűrőben. 

74
00:09:14,095 --> 00:09:17,860
Ezután két napon belül 
meg kell szárítani a magokat.

75
00:09:26,195 --> 00:09:27,255
Helyezzük őket,

76
00:09:27,480 --> 00:09:31,725
meleg (23-30°C közötti),
száraz, jól szellőző helyen

77
00:09:32,010 --> 00:09:37,555
finom lyukú szitára vagy tányérra. 

78
00:09:43,190 --> 00:09:48,735
Kisebb mennyiségek esetén 
használhatunk kávészűrő papírt

79
00:09:49,310 --> 00:09:53,090
mivel ez sok nedvességet képes felvenni, 
és a magok sem tapadnak hozzá.

80
00:09:54,120 --> 00:09:58,575
Maximum egy kis teáskanálnyi magot 
tegyünk egy-egy kávészűrő papírba,

81
00:09:59,020 --> 00:10:00,840
majd írjuk rá a fajta

82
00:10:01,160 --> 00:10:04,935
és a faj nevét 
alkoholos filccel.

83
00:10:06,675 --> 00:10:13,805
Száraz, szellős, árnyékos, meleg helyen 
akasszuk a tasakokat ruhaszárító kötélre. 

84
00:10:15,600 --> 00:10:18,230
Ne tegyük ki a magokat közvetlen napsugárzásnak,

85
00:10:18,915 --> 00:10:22,130
és ne szárítsuk őket papíron, 
amihez könnyen hozzátapadnak. 

86
00:10:30,945 --> 00:10:33,525
Írjuk fel egy címkére a fajta és a faj nevét,

87
00:10:33,780 --> 00:10:38,420
valamint a magfogás évét, 
majd helyezzük azt a tasak belsejébe.

88
00:10:39,390 --> 00:10:42,010
A külső felirat könnyen letörlődhet. 

89
00:10:46,670 --> 00:10:50,740
Néhány fagyasztóban töltött nap alatt
valamennyi parazita lárvája elpusztul. 

90
00:10:53,855 --> 00:10:59,415
A paprikák magja
3-6 évig őrzi meg a csírázóképességét.

91
00:11:04,380 --> 00:11:07,785
Ezt meghosszabbíthatjuk, ha a magokat fagyasztóban tároljuk. 
