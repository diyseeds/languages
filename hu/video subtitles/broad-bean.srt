﻿1
00:00:09,300 --> 00:00:14,146
A lóbab a pillangósvirágúak (Fabaceae) családjába 
tartozik, latin neve: Vicia faba.

2
00:00:16,435 --> 00:00:21,824
Egyéves növény, melyet elsősorban különböző 
színű és méretű magjaiért termesztenek,

3
00:00:23,860 --> 00:00:26,232
de a fiatal hajtásai is ehetőek.

4
00:00:28,620 --> 00:00:30,848
Különböző típusai vannak, melyeket

5
00:00:31,232 --> 00:00:42,080
vagy emberi fogyasztásra vagy 
pedig állati takarmányként használnak. 

6
00:00:53,648 --> 00:00:54,712
Megporzás 

7
00:01:12,976 --> 00:01:17,880
A lóbabnak kétivarú önbeporzó virága van, 
vagyis a hímivarú és a nőivarú szaporítószervek

8
00:01:18,888 --> 00:01:24,616
ugyanabban a virágban vannak, 
és kompatibilisek egymással.

9
00:01:25,656 --> 00:01:27,744
A lóbabot öntermékenyülő növénynek tekintjük. 

10
00:01:31,120 --> 00:01:36,888
De a rovarok miatt előfordulhat kereszteződés: 
gyakorisága a fajtától, 

11
00:01:43,440 --> 00:01:50,064
a környezeti adottságoktól, és a 
természetes akadályok jelenlététől

12
00:01:50,376 --> 00:01:53,736
függően 5-60% között van.  
A keresztbeporzódás 

13
00:01:54,680 --> 00:02:00,496
elkerülése érdekében tartsunk 
1 km-es védőtávolságot! 

14
00:02:01,064 --> 00:02:04,312
Ezt néhány száz méterre csökkenthetjük, 
ha a fajták között van

15
00:02:04,624 --> 00:02:07,768
valamilyen természetes akadály, pl.sövény. 

16
00:02:12,616 --> 00:02:15,400
A fajta genetikai tisztaságának megőrzése

17
00:02:15,664 --> 00:02:18,856
érdekében a magfogásra 
szánt egyedeket takarjuk le hálóval!

18
00:02:24,910 --> 00:02:29,192
A hálót már a virágzás kezdete 
előtt helyezzük el! 

19
00:02:39,260 --> 00:02:41,128
Életciklus 

20
00:02:57,824 --> 00:03:02,512
A magfogás céljából vetett lóbabokat ugyanúgy 
termesztjük, mint a fogyasztási célból nevelteket. 

21
00:03:07,560 --> 00:03:10,144
A lóbab nem kedveli a magas hőmérsékletet,

22
00:03:10,456 --> 00:03:13,728
a melegben leáll a megporzás, 
és csökken a termésmennyiség.

23
00:03:17,952 --> 00:03:23,928
Az enyhébb éghajlatú területeken ősz végén, 
egyébként pedig tél végén vessük

24
00:03:24,040 --> 00:03:25,816
a lóbabot, mihelyst a talaj alkalmas rá! 

25
00:03:47,440 --> 00:03:49,775
A genetikai sokféleség biztosítása érdekében

26
00:03:50,192 --> 00:03:54,336
magfogás céljából 
legalább 10 növényt ültessünk! 

27
00:04:17,856 --> 00:04:22,416
Az adott fajta jellegzetességeit 
mutató növények közül válasszunk,

28
00:04:22,840 --> 00:04:26,520
például a növény mérete,
 a virág színe, a hüvelyek száma,

29
00:04:29,080 --> 00:04:34,410
a hüvelyenkénti magszám, illetve a magok mérete,

30
00:04:35,104 --> 00:04:38,232
színe és íze alapján. 

31
00:04:42,680 --> 00:04:46,416
A növények fejlődése során a 
legszebb, legegészségesebb,

32
00:04:46,736 --> 00:04:50,104
és legjobban termő egyedeket 
válasszuk ki a magfogáshoz! 

33
00:04:51,448 --> 00:04:55,552
A betakarítási időszak hossza 
szintén fontos kiválasztási kritérium. 

34
00:05:00,624 --> 00:05:04,240
Az állomány egy részét 
hagyjuk meg magfogásra, és csak akkor

35
00:05:04,570 --> 00:05:08,080
takarítsuk be a termést, ha a babok már teljesen érettek.

36
00:05:13,880 --> 00:05:16,624
Ne az első hüvelyeket szedjük le fogyasztásra, és az

37
00:05:17,050 --> 00:05:23,256
utoljára beérőkről fogjunk magot, mivel 
az először beérő hüvelyekben levő magok

38
00:05:23,576 --> 00:05:27,064
hordozzák a rövid tenyészidőt 
biztosító géneket! 

39
00:05:34,336 --> 00:05:35,630
Csapadékos időjárás esetén

40
00:05:36,072 --> 00:05:38,592
a magokat már a teljes érés előtt takarítsuk be,

41
00:05:39,072 --> 00:05:42,832
majd hagyjuk őket megszáradni 
száraz, jól szellőző helyen.

42
00:05:52,600 --> 00:05:53,776
Általában a növényeket

43
00:05:53,880 --> 00:05:58,208
hagyhatjuk lábon megszáradni egészen 
amíg a hüvelyek feketére színeződnek. 

44
00:06:06,520 --> 00:06:11,776
A legjobb minőségű magok az elsőként kifejlődött 
hüvelyekben találhatóak a növény alsó részén. 

45
00:06:13,570 --> 00:06:17,056
Óvatosan harapjunk rá egy magra!

46
00:06:19,680 --> 00:06:22,960
Ha nem látszik a harapás nyoma, 
a mag megfelelően száraz. 

47
00:06:33,080 --> 00:06:36,900
Magfogás – tisztítás - tárolás

48
00:06:46,400 --> 00:06:51,232
Kifejthetjünk hüvelyenként a magokat, 
vagy meg is taposhatjuk a hüvelyeket.

49
00:06:52,010 --> 00:06:58,920
Utóbbi esetben figyeljünk rá, hogy a cséplés talaja 
megfelelően puha legyen, nehogy károsodjanak a magok! 

50
00:07:02,520 --> 00:07:06,976
A válogatás során távolítsuk el a 
többitől különböző babokat.

51
00:07:07,456 --> 00:07:09,808
Ezek keresztbeporzódással jöttek létre.

52
00:07:12,256 --> 00:07:18,656
Szedjük ki a sérült, nem megfelelő formájú, 
illetve a zsizsikes magokat is! 

53
00:07:20,368 --> 00:07:26,000
A lóbab magjaiban gyakran 
zsizsikek (Bruchus rufinaus) laknak.

54
00:07:26,648 --> 00:07:31,088
Ezek olyan kisméretű rovarok, amelyek 
a petéiket a maghéj alá rakják. 

55
00:07:32,704 --> 00:07:37,808
Megszabadulhatunk tőlük, ha a magokat 
néhány napra a fagyasztóba tesszük. 

56
00:07:44,640 --> 00:07:48,160
Mindig írjuk fel egy címkére a 
faj és a fajta nevét, valamint

57
00:07:48,330 --> 00:07:53,720
a magfogás évét, majd helyezzük azt 
a tasak belsejébe! A külső felirat könnyen letörlődhet. 

58
00:08:00,224 --> 00:08:05,344
A lóbab magjai 5-10 évig őrzik meg 
a csírázóképességüket.

59
00:08:06,056 --> 00:08:09,808
Ezt úgy tudjuk meghosszabbítani, ha a 
magokat alacsony hőmérsékleten tároljuk. 
