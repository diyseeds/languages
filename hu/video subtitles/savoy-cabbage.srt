﻿1
00:00:09,480 --> 00:00:12,938
A kelkáposzta a káposztafélék 
(Brassicaceae) családjának tagja.

2
00:00:13,510 --> 00:00:18,930
A faj latin neve Brassica oleraceae,
ezen belül pedig a sabauda alfajhoz tartozik.

3
00:00:20,560 --> 00:00:25,025
Szintén a Brassica oleracea 
fajba tartozik a karalábé,

4
00:00:25,163 --> 00:00:31,396
a brokkoli, a fejes káposzta, a kelbimbó, 
a leveles kel (fodros kel), valamint a karfiol is. 

5
00:00:37,810 --> 00:00:41,949
A kelkáposztának 
fodros levelei vannak,

6
00:00:44,581 --> 00:00:48,196
feje kevésbé tömör,
mint a fejes káposztának.

7
00:00:50,220 --> 00:00:53,934
Vannak tavaszi és 
nyári fajták,

8
00:00:54,138 --> 00:00:56,792
amelyek lazábban álló leveleket és fejet képeznek,

9
00:00:58,698 --> 00:01:01,912
téli tárolásra szánt nagyméretű 
fejet növesztő kelkáposzták,

10
00:01:05,745 --> 00:01:10,240
és könnyű, zöld fejű 
téli kelkáposzták is. 

11
00:01:15,592 --> 00:01:20,340
A Brassica oleracea fajba 
tartozó összes káposztaféle megporzása: 

12
00:01:36,120 --> 00:01:40,240
A Brassica oleracea faj 
virágai kétivarúak,

13
00:01:43,090 --> 00:01:47,265
amely azt jelenti, hogy vannak hímivarú 
és nőivarú szaporítószerveik is.

14
00:01:50,996 --> 00:01:53,490
A legtöbbjük önmeddő,

15
00:01:54,101 --> 00:01:59,156
vagyis az egyik növény virágai által termelt virágpor
csak egy másik növényt képes megtermékenyíteni.

16
00:02:02,967 --> 00:02:05,178
Ezért a növények idegentermékenyülők.

17
00:02:06,123 --> 00:02:10,945
Az optimális megporzás érdekében
ajánlott sok növényt ültetni. 

18
00:02:14,501 --> 00:02:17,563
A megporzást rovarok végzik.

19
00:02:20,952 --> 00:02:25,403
Az említett tulajdonságok természetes módon 
biztosítják a nagy genetikai változatosságot.

20
00:02:32,356 --> 00:02:36,661
A Brassica oleracea faj
valamennyi alfaja képes

21
00:02:36,814 --> 00:02:38,690
egymással kereszteződni,

22
00:02:40,276 --> 00:02:44,923
ezért nem szabad különböző típusú káposztákat 
egymáshoz közel termeszteni, ha magfogás a célunk. 

23
00:02:49,447 --> 00:02:55,192
A genetikai tisztaság biztosítása érdekében
a Brassica oleracea faj különböző fajtái között

24
00:02:55,476 --> 00:02:58,690
minimum 1 km izolációs távolságot kell hagyni. 

25
00:03:00,000 --> 00:03:04,378
Ezt a távolságot 500 méterre csökkenthetjük, 
amennyiben van valamilyen természetes akadály

26
00:03:04,472 --> 00:03:07,287
két fajta között, például sövény. 

27
00:03:10,603 --> 00:03:14,680
A fajtákat úgy is izolálhatjuk egymástól, 
ha kis kaptárakat teszünk

28
00:03:14,763 --> 00:03:18,334
rovarokkal együtt, zárt szúnyogháló alá,

29
00:03:23,389 --> 00:03:27,090
vagy pedig ha az egyes fajtákat takaró 
szúnyoghálókat felváltva nyitjuk, illetve zárjuk.

30
00:03:31,440 --> 00:03:32,618
Bővebb információt erről,

31
00:03:32,843 --> 00:03:36,860
az izolációs technikákról szóló modulban
találsz "A magtermesztés ábécéje" fejezetben. 

32
00:03:50,180 --> 00:03:53,862
A kelkáposzta életciklusa

33
00:03:54,974 --> 00:03:57,680
A kelkáposzta kétéves növény.

34
00:03:58,705 --> 00:04:03,643
A magfogás céljából vetett növényeket 
ugyanúgy termesztjük, mint a fogyasztási célból nevelteket.

35
00:04:08,327 --> 00:04:10,596
Magot a második évben hoz. 

36
00:04:50,720 --> 00:04:56,436
A genetikai sokféleség biztosításához 
10-15 növényt válasszunk ki a magfogáshoz.

37
00:04:57,220 --> 00:05:00,843
Magokat egészséges 
egyedekről fogjunk,

38
00:05:01,112 --> 00:05:04,465
amelyek növekedését 
végig figyelem kísértük,

39
00:05:05,440 --> 00:05:09,980
így megismerve valamennyi fajtára
jellemző tulajdonságukat. 

40
00:05:10,356 --> 00:05:15,774
Azokat a legerőteljesebb növekedésű fejeket válasszuk ki,
amelyek megfelelnek a szelekciós kritériumoknak:

41
00:05:17,309 --> 00:05:22,865
szabályos és erőteljes növekedés, 
gyors fejképzés,

42
00:05:25,200 --> 00:05:28,660
jó tárolhatóság, rövid tenyészidő,

43
00:05:30,887 --> 00:05:33,310
hidegtűrés, és betegségekkel szembeni ellenálló képesség.

44
00:05:36,138 --> 00:05:41,520
A kiválasztást befolyásoló más tulajdonságok: 
fajtára jellemző forma,

45
00:05:42,080 --> 00:05:45,100
hegyes, lapos vagy kerek fej,

46
00:05:46,974 --> 00:05:52,472
rövid szár, jól fejlett gyökérrendszer, íz, és szín. 

47
00:05:55,934 --> 00:06:01,047
A kelkáposzta sokkal jobban bírja a hideget, 
mint bármelyik másik Brassica oleracea fajba tartozó növény,

48
00:06:01,483 --> 00:06:06,501
és még a -15°C-os 
fagyot is túléli.

49
00:06:06,890 --> 00:06:10,254
A legtöbb fajta a szabadban is áttelel. 

50
00:06:23,310 --> 00:06:26,618
Az átteleltetés egyéb módszerei, 
illetve az életciklus második évének történései

51
00:06:26,712 --> 00:06:30,340
ugyanazok, mint a fejes káposzta esetében.

52
00:07:07,636 --> 00:07:14,340
A Brassica oleracea fajba tartozó összes
káposztaféle betakarítása, magkinyerése, válogatása és tárolása.

53
00:07:23,505 --> 00:07:27,250
A magok akkor érettek, amikor az őket 
tartalmazó becőtermések bézs színűvé válnak.

54
00:07:31,483 --> 00:07:34,007
Érett állapotban a becők

55
00:07:34,196 --> 00:07:39,294
könnyen felnyílnak, és szétszórják 
a bennük található magokat. 

56
00:07:47,781 --> 00:07:51,963
A legtöbbször a szárak 
nem egyszerre érnek be.

57
00:07:52,836 --> 00:07:59,069
A magvesztés elkerülése érdekében a magszárakat 
 külön-külön kell betakarítani, amint azok beértek.

58
00:08:00,676 --> 00:08:06,298
A teljes növényt is felszedhetjük még 
azelőtt, hogy minden mag teljesen beérne. 

59
00:08:09,636 --> 00:08:14,800
Ilyenkor az érési folyamat a szárítás 
végére fejeződik be, amely egy száraz

60
00:08:14,950 --> 00:08:16,625
és jól szellőző helyen zajlik. 

61
00:08:29,236 --> 00:08:35,374
A káposztafélék magjai akkor állnak készen a magkinyerésre, 
amikor a becőtermések kézzel könnyen kinyithatóak. 

62
00:08:37,403 --> 00:08:38,690
A magok kinyeréséhez

63
00:08:38,800 --> 00:08:44,160
a becőterméseket szétterítjük egy 
műanyag fólián vagy egy vastag szöveten,

64
00:08:44,581 --> 00:08:47,461
majd megütögetjük, vagy a kezünk között összedörzsöljük őket.

65
00:08:50,930 --> 00:08:56,021
A terméseket akár egy zsákba is tehetjük és egy
puha felülethez ütve kicsépelhetjük a magokat. 

66
00:08:57,420 --> 00:09:02,072
Nagyobb mennyiség esetén a magokat
lábbal vagy valamilyen járművel taposhatjuk ki. 

67
00:09:14,600 --> 00:09:20,000
A nehezen felnyíló becőtermések 
valószínűleg éretlen magokat tartalmaznak,

68
00:09:20,130 --> 00:09:21,796
amelyek nem csíráznak jól. 

69
00:09:26,581 --> 00:09:27,898
A válogatás során

70
00:09:28,130 --> 00:09:32,690
a nemkívánatos növényi részek eltávolítása érdekében
először egy olyan nagylyukú rostán engedjük át a magokat,

71
00:09:33,025 --> 00:09:34,740
amelyen az említett anyagok fennmaradnak,

72
00:09:39,505 --> 00:09:42,043
majd pedig egy olyan finomabb szitán,

73
00:09:42,501 --> 00:09:47,060
amin a magok maradnak fenn, 
a kisebb törmelékek viszont átesnek. 

74
00:09:50,760 --> 00:09:54,610
Végül fújva vagy 
pedig a szél segítségével

75
00:09:55,040 --> 00:09:59,025
ki kell szelelnünk a magokat, hogy
a maradék növényi részeket is eltávolíthassuk. 

76
00:10:14,225 --> 00:10:19,180
Az összes Brassica oleracea fajba tartozó 
növény magja nagyon hasonló.

77
00:10:20,174 --> 00:10:24,101
Rendkívül nehéz 
megkülönböztetni őket, például

78
00:10:24,400 --> 00:10:26,865
a fejes káposzta és a karfiol magjait.

79
00:10:27,381 --> 00:10:30,320
Ezért fontos címkével ellátni a növényeket

80
00:10:30,589 --> 00:10:34,349
majd a kinyert magokat, 
feltüntetve a faj

81
00:10:34,600 --> 00:10:37,643
és a fajta nevét, valamint a termesztés évét. 

82
00:10:39,076 --> 00:10:43,767
Néhány fagyasztóban töltött nap alatt 
valamennyi parazita lárvája elpusztul. 

83
00:10:49,280 --> 00:10:52,429
A káposztafélék magjai 5 évig őrzik meg
a csírázóképességüket,

84
00:10:53,512 --> 00:10:57,200
de ez akár 10 évig is 
kitolódhat.

85
00:10:58,880 --> 00:11:02,290
Ezt úgy tudjuk meghosszabbítani,
ha a magokat fagyasztóban tároljuk.

86
00:11:03,243 --> 00:11:09,680
Egy gramm fajtától függően
250-300 darab magot tartalmaz. 
