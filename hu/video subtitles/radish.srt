1
00:00:11,480 --> 00:00:15,290
A retek a káposztafélék 
(Brassicaceae) családjának tagja.

2
00:00:15,527 --> 00:00:17,992
A faj latin neve: Raphanus sativus.

3
00:00:19,301 --> 00:00:23,694
Vannak kisebb méretű retkek, például 
a hónapos retek (Raphanus sativus sativus)

4
00:00:24,152 --> 00:00:28,138
és nagyobbak is, például
a fekete retek (Raphanus sativus niger)

5
00:00:32,501 --> 00:00:36,116
nyári, őszi és téli fajtákkal. 

6
00:00:37,190 --> 00:00:40,865
A kisebb méretű retkek általában fehérek, 
rózsaszínek vagy pirosak.

7
00:00:41,803 --> 00:00:45,003
Léteznek sárga, szürke, lila

8
00:00:45,156 --> 00:00:49,069
és fekete fajták is, amelyek 
gyökere többé-kevésbé megnyúlt.

9
00:00:50,720 --> 00:00:56,298
A nagyobb méretű retkek fehérek, rózsaszínűek, 
lilák vagy feketék, kerek

10
00:00:56,410 --> 00:00:59,425
vagy különböző hosszúságú megnyúlt gyökerekkel. 

11
00:01:09,229 --> 00:01:10,363
Megporzás 

12
00:01:18,894 --> 00:01:26,327
A retek virágai kétivarúak, vagyis vannak 
hímivarú és nőivarú szaporítószerveik is.

13
00:01:30,276 --> 00:01:32,778
A legtöbbjük önmeddő,

14
00:01:34,996 --> 00:01:39,803
vagyis az egyik növény virágai által termelt virágpor 
csak egy másik növényt képes megtermékenyíteni.

15
00:01:40,887 --> 00:01:45,570
Az optimális megporzás érdekében 
ajánlott sok növényt ültetni. 

16
00:01:47,621 --> 00:01:50,145
A retek tehát idegentermékenyülő,

17
00:01:50,385 --> 00:01:53,221
rovarmegporzású növény.

18
00:01:57,905 --> 00:02:00,116
Virágai fehérek vagy lilák.

19
00:02:00,400 --> 00:02:03,163
Nektárt termelnek, és sok méhet vonzanak. 

20
00:02:09,614 --> 00:02:13,207
A retek valamennyi fajtája 
keresztbeporzódhat egymással.

21
00:02:13,469 --> 00:02:17,818
Még a két alfaj
is összekereszteződhet. 

22
00:02:19,643 --> 00:02:22,072
A genetikai tisztaság biztosítása érdekében

23
00:02:22,312 --> 00:02:26,727
a fajták között minimum 1 kilométeres 
izolációs távolságot kell hagyni. 

24
00:02:27,403 --> 00:02:32,080
Ezt 500 méterre csökkenthetjük, 
ha két fajta között van valamilyen

25
00:02:32,283 --> 00:02:34,829
természetes akadály, például sövény. 

26
00:02:38,734 --> 00:02:41,250
A fajtákat úgy is izolálhatjuk egymástól,

27
00:02:41,432 --> 00:02:45,112
ha az egyes fajtákat takaró 
szúnyoghálókat felváltva nyitjuk és zárjuk,

28
00:02:51,163 --> 00:02:56,058
vagy, ha zárt rovarháló alá kis kaptárakat 
teszünk rovarokkal együtt.

29
00:02:57,934 --> 00:02:58,974
Bővebb információt erről,

30
00:02:59,214 --> 00:03:03,338
az izolációs technikákról szóló modulban
 találsz "A magtermesztés ábécéje" fejezetben. 

31
00:03:12,654 --> 00:03:14,700
A retek életciklusa 

32
00:03:19,003 --> 00:03:21,578
A kisméretű retkek egyéves növények.

33
00:03:21,709 --> 00:03:24,858
Ha korán elvetjük őket (márciusban vagy áprilisban),

34
00:03:25,280 --> 00:03:28,938
akkor már nyár végén érett 
magokat takaríthatunk be róluk. 

35
00:04:09,760 --> 00:04:12,741
15-20 magfogásra szánt növény kiválasztásához

36
00:04:13,170 --> 00:04:18,196
húzzunk ki 50-100 egyedet, hogy 
közelebbről is megvizsgálhassuk őket.

37
00:04:36,200 --> 00:04:40,385
Ezután a kiválasztott növényeket
teljesen visszaültetjük a föld alá,

38
00:04:40,785 --> 00:04:46,058
25 cm-es tőtávolságot és 30 cm-es sortávolságot hagyva,

39
00:04:51,720 --> 00:04:53,243
majd alaposan megöntözzük őket. 

40
00:04:59,200 --> 00:05:02,138
A nagyméretű retkek kétéves növények.

41
00:05:03,992 --> 00:05:07,032
Magfogás céljából 
nyáron kell vetni őket.

42
00:05:09,294 --> 00:05:14,567
Áttelelnek, majd a következő évben 
virágoznak és hoznak magot. 

43
00:05:17,963 --> 00:05:22,050
Ősz végén a nagyméretű 
retkeket kiássuk, és kiválogatjuk. 

44
00:05:26,080 --> 00:05:28,632
Magokat azokról az egészséges egyedekről fogjunk,

45
00:05:28,720 --> 00:05:33,461
amelyek növekedését végig figyelemmel tudtuk kísérni, 
így megismerve valamennyi tulajdonságukat,

46
00:05:33,934 --> 00:05:40,967
mint a méret, szín, növekedési erély és gyorsaság, 
a betegségekkel szembeni ellenálló képesség,

47
00:05:41,258 --> 00:05:43,180
a túl korai magszárba indulás hiánya,

48
00:05:46,421 --> 00:05:50,407
a pudvásodástól mentes zsenge hús, illetve az íz (enyhe vagy csípős). 

49
00:05:58,400 --> 00:06:01,350
A leveleket a gyökérnyak megsértése nélkül vágjuk le,

50
00:06:01,578 --> 00:06:06,334
majd a retkeket nedves homokban 
vagy műanyag zacskóban tároljuk.

51
00:06:09,418 --> 00:06:14,218
Pincében vagy fagymentes, 
hűvös helyen teleljenek át.

52
00:06:20,007 --> 00:06:25,607
A tél folyamán rendszeresen ellenőrizzük a gyökereket,
és távolítsuk el azokat, amelyek rothadásnak indultak! 

53
00:06:53,076 --> 00:06:58,669
Tavasszal újra elültetjük a retkeket 
a kertben 25 cm-es tőtávolsággal

54
00:06:59,352 --> 00:07:03,847
és 30 cm-es sortávolsággal, majd 
alaposan megöntözzük a növényeket.

55
00:07:41,770 --> 00:07:47,549
Mivel a magfogásra szánt növények akár a 
2 m-es magasságot is elérhetik, általában ki kell karózni őket. 

56
00:07:55,207 --> 00:07:58,618
A magok akkor érettek, 
amikor a termés bézs színűvé válik. 

57
00:08:02,305 --> 00:08:05,760
A szárak általában nem 
egyszerre érnek be.

58
00:08:06,756 --> 00:08:08,550
A magvesztés elkerülése érdekében

59
00:08:08,858 --> 00:08:13,025
a magszárakat külön-külön 
kell betakarítani, amint beértek.

60
00:08:16,378 --> 00:08:21,512
A teljes növényt is felszedhetjük még azelőtt, 
hogy minden mag teljesen beérne. 

61
00:08:26,189 --> 00:08:30,349
Az érési folyamat befejezéséhez 
tegyük a növényeket száraz,

62
00:08:30,440 --> 00:08:32,130
jól szellőző helyre. 

63
00:08:46,523 --> 00:08:50,060
Magkinyerés – válogatás – tárolás

64
00:08:52,370 --> 00:08:54,800
A retek magjai akkor állnak készen a magkinyerésre,

65
00:08:55,090 --> 00:08:58,450
amikor a becőtermések kézzel könnyen kinyithatóak. 

66
00:09:02,740 --> 00:09:07,629
A magok kinyeréséhez a becőterméseket 
egy sodrófával is összezúzhatjuk.

67
00:09:13,338 --> 00:09:18,901
A terméseket akár zsákba is tehetjük, 
aztán puha felülethez ütve kicsépelhetjük a magokat.

68
00:09:20,060 --> 00:09:23,600
Nagyobb mennyiség esetén 
a magokat lábbal

69
00:09:23,800 --> 00:09:26,334
vagy valamilyen járművel taposhatjuk ki. 

70
00:09:29,250 --> 00:09:34,560
A nehezen felnyíló becőtermések 
valószínűleg éretlen magokat tartalmaznak,

71
00:09:34,734 --> 00:09:36,429
amik nem csíráznak jól. 

72
00:09:40,705 --> 00:09:41,992
A válogatás során

73
00:09:42,189 --> 00:09:47,941
a nemkívánatos növényi részek eltávolításához először egy nagylyukú 
rostán engedjük át a magokat, amelyen ezek az anyagok fennmaradnak,

74
00:09:55,272 --> 00:09:57,774
majd egy finomabb szitán,

75
00:09:57,890 --> 00:10:02,247
amin a magok maradnak fenn, 
a kisebb törmelékek viszont átesnek. 

76
00:10:21,134 --> 00:10:24,101
Végül fújva vagy a szél segítségével 
kiszeleljük a magokat,

77
00:10:24,261 --> 00:10:28,596
hogy a maradék növényi részeket 
is eltávolítsuk. 

78
00:10:39,170 --> 00:10:42,123
Mindig írjuk fel egy címkére a fajta
és a faj nevét, valamint a magfogás évét,

79
00:10:42,349 --> 00:10:48,029
majd helyezzük azt a tasak belsejébe.
A külső felirat könnyen letörlődhet. 

80
00:10:55,940 --> 00:11:00,800
Néhány fagyasztóban töltött nap alatt 
valamennyi parazita lárvája elpusztul. 

81
00:11:02,840 --> 00:11:06,727
A retekmagok 5-10 évig őrzik meg 
a csírázóképességüket.

82
00:11:07,636 --> 00:11:10,945
Ezt úgy tudjuk meghosszabbítani, 
ha a magokat fagyasztóban tároljuk. 
