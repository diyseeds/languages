1
00:00:08,305 --> 00:00:13,752
A borsó a pillangósvirágúak (Fabaceae) családjába, 
ezen belül a Pisum sativum fajba tartozik.

2
00:00:16,450 --> 00:00:17,934
Egyéves növények.

3
00:00:18,189 --> 00:00:20,327
Számos változata ismert:

4
00:00:23,403 --> 00:00:27,461
Kerek, sima magvú
kifejtőborsók.

5
00:00:28,443 --> 00:00:32,981
Ezek szívós növények, korán érnek, 
és jól bírják a hideget. 

6
00:00:35,156 --> 00:00:39,018
Ráncos magvú borsók, 
melyeket szintén ki kell fejteni.

7
00:00:39,170 --> 00:00:43,970
Ezek kevésbé alkalmasak korai vetésre, 
viszont jobban bírják a meleget. 

8
00:00:48,545 --> 00:00:50,145
Ehető hüvelyű borsók,

9
00:00:50,981 --> 00:00:55,447
melyek teljes zsenge hüvelyét 
meg lehet enni a magok kifejlődése előtt. 

10
00:00:57,556 --> 00:00:59,460
A borsófajták között,

11
00:00:59,650 --> 00:01:03,963
vannak kúszó típusúak, amelyek akár 70 cm-re 
is megnőhetnek.

12
00:01:06,734 --> 00:01:08,290
Ezek támrendszert igényelnek. 

13
00:01:14,920 --> 00:01:20,087
Vannak törpe növésű borsók is, 
amelyek csak 45-70 cm magasra nőnek,

14
00:01:20,920 --> 00:01:22,880
ezért általában nem igényelnek támaszt. 

15
00:01:25,560 --> 00:01:30,043
A Pisum sativum fajon belül 
vannak takarmányborsók is. 

16
00:01:38,349 --> 00:01:39,425
Megporzás 

17
00:01:48,021 --> 00:01:52,580
A borsónak kétivarú 
önbeporzó virágai vannak,

18
00:01:52,974 --> 00:01:57,636
vagyis a hímivarú és nőivarú szaporítószervek 
ugyanabban a virágban találhatóak,

19
00:01:57,774 --> 00:01:59,650
és kompatibilisek egymással. 

20
00:02:00,720 --> 00:02:06,094
De előfordulhat rovarok általi 
keresztbeporzás a különböző fajták között.

21
00:02:11,403 --> 00:02:15,120
Ennek valószínűsége az adott fajtától 
és a környezeti feltételektől függ,

22
00:02:16,196 --> 00:02:18,807
például, hogy vannak-e természetes akadályok. 

23
00:02:22,930 --> 00:02:29,149
A keresztbeporzódás elkerülése érdekében
tartsunk 15 m izolációs távolságot a fajták között. 

24
00:02:31,854 --> 00:02:34,756
Néhány méter is elég lehet, 
ha valamilyen természetes akadály,

25
00:02:34,872 --> 00:02:36,538
például sövény található a fajták között. 

26
00:02:41,825 --> 00:02:45,178
Ha több fajtát szeretnénk termeszteni 
egy kertben,

27
00:02:45,350 --> 00:02:48,763
akkor egy fajtát rovarhálóval letakarva is izolálhatunk.

28
00:02:49,527 --> 00:02:53,134
Fontos, hogy a háló még a virágzás kezdete 
előtt a helyére kerüljön. 

29
00:03:02,734 --> 00:03:04,232
Életciklus 

30
00:03:14,290 --> 00:03:19,105
A magfogás céljából vetett borsókat ugyanúgy 
kell termeszteni, mint a fogyasztási célból nevelteket.

31
00:03:23,320 --> 00:03:28,836
Egyes fajtákat akár tél alá (azaz tél előtt) is vethetünk, 
de a borsók többsége általában kora tavasszal kerül a földbe.

32
00:03:33,120 --> 00:03:35,230
Ne várjunk túl sokáig a borsó vetésével,

33
00:03:35,483 --> 00:03:39,949
mivel 30°C fölött a virágai 
már nem termékenyülnek meg! 

34
00:03:55,723 --> 00:04:01,556
A genetikai sokféleség biztosítása és a hatékonyabb 
 szelekció érdekében minimum 50 növényt termesszünk! 

35
00:04:05,956 --> 00:04:11,061
A növekedésük során a legszebb, a legegészségesebb
és a legbőtermőbb növényeket válasszuk ki,

36
00:04:11,280 --> 00:04:12,880
a többit távolítsuk el. 

37
00:04:30,360 --> 00:04:33,880
Célszerű az állomány egy részét
magfogásra meghagyni.

38
00:04:34,000 --> 00:04:37,410
Ezekről az egyedekről egyetlen hüvelyt 
se szedjünk le fogyasztási célból.

39
00:04:42,043 --> 00:04:44,414
Meg kell várnunk, amíg az összes mag teljesen megérik.

40
00:04:46,960 --> 00:04:50,620
így megőrizhetjük 
a fajta koraiságát. 

41
00:04:55,330 --> 00:05:01,214
A magok betakarításához egyszerűen csak 
hagyni kell a növényeket elszáradni a kertben. 

42
00:05:17,410 --> 00:05:20,749
Ha szükséges a szárítást
egy pajtában, fészerben is befejezhetjük. 

43
00:05:31,592 --> 00:05:35,156
Úgy ellenőrizhetjük, hogy elég szárazak-e
a magok, ha egybe óvatosan beleharapunk.

44
00:05:36,029 --> 00:05:39,869
Ha nem látszik a harapás nyoma, akkor a mag teljesen száraz. 

45
00:05:50,080 --> 00:05:53,345
Magkinyerés – válogatás – tárolás

46
00:05:56,036 --> 00:05:59,730
A hüvelyeket vagy
egyesével fejtjük ki,

47
00:06:01,980 --> 00:06:04,269
vagy akár bottal is ütögethetjük,

48
00:06:08,734 --> 00:06:09,927
és ki is taposhatjuk őket. 

49
00:06:22,923 --> 00:06:26,472
Ezt követően a nagyobb méretű növényi
törmeléket távolítsuk el szita segítségével.

50
00:06:30,380 --> 00:06:32,901
A nagyobb darabokat kézzel is kiszedhetjük,

51
00:06:33,294 --> 00:06:36,000
míg a kisebb méretűek 
átesnek a szitán. 

52
00:06:39,650 --> 00:06:43,301
Végül szeleléssel távolíthatjuk
el a legkisebb törmelékeket.

53
00:06:43,380 --> 00:06:51,658
Ez történhet fújással, ventilátorral 
vagy kisméretű kompresszor segítségével is. 

54
00:07:04,421 --> 00:07:08,392
Fontos, hogy mindig írjuk fel
egy címkére a fajta és a faj nevét,

55
00:07:08,538 --> 00:07:11,250
valamint a magfogás évét, 
majd helyezzük azt a tasak belsejébe.

56
00:07:11,381 --> 00:07:14,020
A külső felirat könnyen letörlődhet. 

57
00:07:16,240 --> 00:07:21,803
A borsómagok gyakran borsózsizsikkel
(Bruchus pisorum) fertőzöttek.

58
00:07:22,087 --> 00:07:26,040
Ezek olyan kicsit rovarok, 
amelyek a petéiket a maghéj alá rakják

59
00:07:26,312 --> 00:07:28,596
miközben a növények a kertben fejlődnek. 

60
00:07:29,767 --> 00:07:34,600
Könnyen megszabadulhatunk tőlük, 
ha a magokat néhány napra a fagyasztóba tesszük. 

61
00:07:47,960 --> 00:07:51,861
A borsómagok 3-8 évig 
őrzik meg a csírázóképességüket.

62
00:07:57,280 --> 00:08:00,618
Ezt úgy tudjuk meghosszabbítani,
ha a magokat fagyasztóban tároljuk. 
