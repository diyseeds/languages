1
00:00:09,015 --> 00:00:13,610
A görögdinnye a tökfélék (Cucurbitaceae) 
családjába tartozó egyéves növény.

2
00:00:15,800 --> 00:00:18,680
A faj latin neve: Citrullus lanatus.

3
00:00:20,260 --> 00:00:22,590
Három fő típusa van:

4
00:00:25,690 --> 00:00:27,370
vannak édes húsúak,

5
00:00:29,995 --> 00:00:31,700
dzsemkészítéshez használtak,

6
00:00:34,680 --> 00:00:40,700
illetve Afrikában az olajban gazdag magjukért 
termesztett fajták, amelyek húsa keserű. 

7
00:00:46,420 --> 00:00:47,405
Megporzás 

8
00:01:08,380 --> 00:01:11,005
A görögdinnye egylaki növény,

9
00:01:11,105 --> 00:01:15,220
vagyis a hímivarú és a nőivarú virágok 
is ugyanazon az egyeden találhatóak.

10
00:01:15,915 --> 00:01:18,525
A virágok csak egy napra nyílnak ki. 

11
00:01:20,950 --> 00:01:24,400
A nőivarú virágok esetében 
magház található a virág alatt.

12
00:01:25,310 --> 00:01:29,590
Valójában ez egy mini görögdinnye, amely 
a megporzást követően elkezd fejlődni. 

13
00:01:31,730 --> 00:01:34,960
A hímivarú virágok a hosszú szárak végén találhatóak. 

14
00:01:43,815 --> 00:01:46,230
A görögdinnyék öntermékenyek,

15
00:01:46,360 --> 00:01:52,465
vagyis egy nőivarú virágot egy olyan hímivarú virág 
virágpora is megtermékenyíthet, amely ugyanazon a növényen található. 

16
00:01:58,280 --> 00:02:01,775
Viszont gyakrabban fordul elő keresztbeporzódás.

17
00:02:02,725 --> 00:02:07,395
A görögdinnyék virágait rovarok, 
elsősorban méhek porozzák be.

18
00:02:12,315 --> 00:02:15,435
Valamennyi görögdinnyefajta 
keresztbeporzódik egymással,

19
00:02:15,750 --> 00:02:17,810
beleértve a vad fajtákat is.

20
00:02:19,200 --> 00:02:24,545
Viszont a görögdinnyék nem kereszteződnek 
az uborkákkal, a sárgadinnyékkel, és a tökökkel sem. 

21
00:02:30,385 --> 00:02:36,095
A keresztbeporzás elkerülése érdekében 
a különböző fajták között tartsunk 1 km-es távolságot. 

22
00:02:38,970 --> 00:02:45,535
Ezt 400 méterre csökkenthetjük, ha a fajták között 
van valamilyen természetes akadály, például sövény. 

23
00:02:50,155 --> 00:02:52,320
Számos módszer létezik,

24
00:02:52,395 --> 00:02:56,295
ha különböző görögdinnyefajtákról 
akarunk magot fogni ugyanabban a kertben. 

25
00:02:58,560 --> 00:03:04,365
Például az egyik kiválasztott fajtát lefedhetjük rovarhálóval,
és egy kis poszméhkaptárat helyezhetünk el a hálón belül. 

26
00:03:13,915 --> 00:03:18,680
Vagy két fajtát külön-külön 
letakarhatunk rovarhálóval,

27
00:03:19,320 --> 00:03:25,460
és az egyik nap csak az egyiket nyitjuk ki, 
a másik nap pedig a másikat.

28
00:03:25,805 --> 00:03:27,930
Így a vadon élő rovarok elvégezhetik a munkájukat.

29
00:03:28,280 --> 00:03:33,970
Ennél a megoldásnál kevesebb termésünk lesz, 
mivel bizonyos virágok nem termékenyülnek meg. 

30
00:03:40,865 --> 00:03:44,375
A harmadik módszer a 
virágok kézi megporzása.

31
00:03:45,960 --> 00:03:48,755
Ez nem olyan egyszerű, mint a tökök vagy a cukkini esetében,

32
00:03:51,665 --> 00:03:58,530
mivel a görögdinnye virágok kisebbek és 
nehéz észrevenni őket virágzáskor.

33
00:04:02,215 --> 00:04:07,320
A kézzel megporzott virágok 
50-75%-ából fejlődik termés.

34
00:04:09,575 --> 00:04:13,180
Ha nem történik megporzás, 
akkor a virág elhervad. 

35
00:04:15,840 --> 00:04:19,310
Bővebb információt ezzel a 
három módszerrel kapcsolatban

36
00:04:19,400 --> 00:04:22,525
a mechanikai izolációs technikákról 
és a kézi megporzásról

37
00:04:22,800 --> 00:04:27,065
szóló modulokban találsz
" A magtermesztés ábécéje" fejezetben. 

38
00:04:37,475 --> 00:04:38,620
Életciklus 

39
00:04:57,090 --> 00:04:58,980
A magfogás céljából vetett görögdinnyéket

40
00:04:59,395 --> 00:05:02,820
ugyanúgy termesztjük, mint a 
fogyasztási célból nevelteket.

41
00:05:12,075 --> 00:05:17,270
Mivel Afrikából származnak, melegre van 
szükségük a csírázáshoz és a növekedéshez.

42
00:05:24,250 --> 00:05:29,500
A genetikai sokféleség biztosítása érdekében 
minimum 6 növényről kell magot fogni,

43
00:05:29,645 --> 00:05:31,650
ideális esetben pedig legalább 12-ről. 

44
00:05:56,805 --> 00:05:59,660
A magfogásra szánt egyedeket 
a fajtára jellemző

45
00:05:59,725 --> 00:06:02,930
egyedi tulajdonságok
alapján válasszuk ki,

46
00:06:03,350 --> 00:06:10,795
mint a rövid tenyészidő, a termések száma, 
az íz és a cukortartalom. 

47
00:06:14,020 --> 00:06:16,635
A betegségekkel szembeni ellenálló képességet is vegyük figyelembe. 

48
00:06:16,985 --> 00:06:20,970
A jól fejlett egyedeket tartsuk meg, 
a betegektől pedig szabaduljunk meg. 

49
00:06:24,680 --> 00:06:29,690
A görögdinnye magok érettségét
könnyű megállapítani:

50
00:06:30,415 --> 00:06:33,860
akkor jók, amikor a termés fogyasztásra kész. 

51
00:06:45,940 --> 00:06:49,660
Magkinyerés – válogatás – tárolás

52
00:06:56,820 --> 00:06:59,775
A magok kinyeréséhez vágjuk ketté

53
00:07:01,545 --> 00:07:05,230
majd pedig szeletekre a görögdinnyét, 
és kés segítségével távolítsuk el a magokat. 

54
00:07:07,345 --> 00:07:08,495
Ha vannak gyerekek is a környéken,

55
00:07:08,560 --> 00:07:11,655
ők pontosan tudni fogják, hogy 
hogyan kell a maradék magokat kiszedni. 

56
00:07:13,840 --> 00:07:16,240
Ezután öblítsük le a magokat folyóvíz alatt. 

57
00:07:20,320 --> 00:07:25,260
Az üres (terméketlen) magoktól úgy szabadulhatunk meg, 
ha vízzel teli edénybe öntjük a magokat.

58
00:07:27,560 --> 00:07:33,140
A termékenyek az edény aljára 
süllyednek, míg az üresek lebegnek a víz tetején. 

59
00:07:59,430 --> 00:08:01,090
Végül hagyjuk árnyékos helyen megszáradni őket. 

60
00:08:03,300 --> 00:08:07,750
A magok akkor száradtak meg eléggé, 
ha a körmünkkel nem tudunk nyomot hagyni rajtuk. 

61
00:08:11,755 --> 00:08:15,230
Mindig írjuk fel egy címkére a fajta és a 
faj nevét, valamint a magfogás évét,

62
00:08:15,345 --> 00:08:20,960
majd helyezzük azt a tasak belsejébe.
A külső felirat könnyen letörlődhet. 

63
00:08:25,655 --> 00:08:29,040
Néhány fagyasztóban töltött nap alatt valamennyi kártevő lárvája elpusztul. 

64
00:08:32,540 --> 00:08:36,940
A görögdinnye magjai átlagosan 5 évig 
őrzik meg a csírázóképességüket,

65
00:08:37,290 --> 00:08:39,115
de akár 10 évig is kicsírázhatnak.

66
00:08:43,655 --> 00:08:47,130
Ezt úgy tudjuk meghosszabbítani, 
ha a magokat fagyasztóban tároljuk. 
