# Copyright (C) 2021 Maxime Lecoq
# This file is distributed under the GNU General Public License v2 or later.
msgid ""
msgstr ""
"Project-Id-Version: Diyseeds 2.0\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"POT-Creation-Date: 2021-07-10T00:00:00+02:00\n"
"WPOT-Type: post-types/video\n"
"X-Domain: wpot\n"
"WPOT-Origin: carrot\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"Language: en\n"

msgid "The carrot belongs to the Apiaceae family and the Daucus carota species. It is grown for its root. There are two subspecies:"
msgstr ""

msgid "The western carrot, carota sativus, which is generally biennial,"
msgstr ""

msgid "and the oriental carrot, carota atrorubens, which is usually annual."
msgstr ""

msgid "Certain carrot varieties are early, while others are grown for storage. Their roots range from white to black and can be yellow, orange, red or purple. Their shapes are also very diverse."
msgstr ""

msgid "The inflorescence of the carrot is an umbel composed of small flowers that are usually hermaphrodite. The stamen, the male sexual organ, matures before the pistil, the female sexual organ. Self-fertilization does not occur within the same flower. Yet since the flowers do not bloom at the same time, self-fertilization is possible within the same umbel or between two umbels on the same plant. Fertilization also occurs between the umbels of different plants."
msgstr ""

msgid "The carrot is therefore an allogamous plant mainly pollinated by insects. There is a risk of cross-pollination between different varieties. The carrot can also cross with wild carrot, which is very common in many regions of the world and whose genes are dominant, as are those of all wild species. The wild carrot is easily recognizable thanks to the small black flower at the center of its inflorescence."
msgstr ""

msgid "To avoid cross-pollination, two varieties of carrot should be grown about one kilometer apart. This distance can be reduced to 500 meters if a natural barrier, such as a hedge, exists between the two varieties."
msgstr ""

msgid "The varieties can also be isolated by alternately opening and closing mosquito nets or by placing small hives of insects inside a closed mosquito net. For this technique, see the module on isolation techniques in the “ABC of seed production”."
msgstr ""

msgid "The varieties of oriental carrots generally function as annual plants when they are grown during the period when the days are long. They will produce seeds in the first year."
msgstr ""

msgid "With western carrots, on the other hand, you need two years to produce seeds. In the first year they will produce their root and a bouquet of leaves. They require a vernalisation period, a winter, before they flower and produce seed in the second year. Early carrot varieties are sown as late as possible in the season in order to avoid their being too mature for storage in winter. Otherwise, they will have difficulty in growing again the following spring."
msgstr ""

msgid "Depending on the region, there are different methods for storing carrots for seed production over the winter. The easiest is of course to leave them in the ground in the garden if the climate allows it. A layer of straw is sometimes enough to protect them from light frosts."
msgstr ""

msgid "In regions with a colder climate where there is a significant risk of frost, the plants should be dug up before the winter cold and stored away from the frost."
msgstr ""

msgid "Those that blossom in the first year are not kept because the plants from these seeds tend to blossom earlier and earlier with each harvest. You should also avoid keeping carrots if they have a green collar, are cracked, or have several roots. The roots are cleaned without water and the leaves are cut above the collar. They are then left to dry a short while in the open air."
msgstr ""

msgid "They should be selected according to the characteristics specific to the variety: colour, shape, vigour and storage capacity. It is also important to select on the basis of the taste, because each carrot of the same variety can have a different taste. To taste them, you can cut the tip of the carrot. The roots must then be disinfected with ash."
msgstr ""

msgid "You should then put the selected roots in a sandbox that is protected from frost or vertically in wooden boxes. Ideal storage conditions are 1°C and 90 to 95% humidity."
msgstr ""

msgid "Over winter, you should regularly check the roots and remove any that are beginning to rot. The roots are then replanted at the beginning of spring once the risk of a hard frost has passed. Care must be taken that the replanted roots do not dry out. They must gradually get used to the light and be protected from intense sunlight."
msgstr ""

msgid "In certain regions, cultivated carrots flower at the same time as wild carrots. To avoid crosses, you should stagger the flowering periods. With this in mind, at the end of winter you can replant the carrot roots intended for seed production in pots under a cold frame in a well-lit place safe from frost. You should then plant them out once it is possible."
msgstr ""

msgid "A minimum of 30 plants are necessary to maintain good genetic diversity, ideally you should keep between 50 and 100. The plants should be staked while they are growing."
msgstr ""

msgid "The carrot produces several umbels that do not bloom at the same time:"
msgstr ""

msgid "The first, the primary umbel, is found at the tip of the main stem."
msgstr ""

msgid "The secondary umbels are those that develop from the main stem."
msgstr ""

msgid "The tertiary umbels form on the secondary stems."
msgstr ""

msgid "Since the umbels mature over a long period of time, they should be harvested gradually."
msgstr ""

msgid "It is preferable to harvest the primary umbels because they produce the best seeds."
msgstr ""

msgid "The secondary umbels are only harvested if necessary."
msgstr ""

msgid "The umbels are cut along with the top of the stalk when the first mature seeds start to fall. As they tend to fall to the ground, they may be cut earlier if the weather is bad. In cold regions, the plants along with their roots can be dug up in September."
msgstr ""

msgid "In any case, drying should continue in a dry and well ventilated place."
msgstr ""

msgid "The seeds will continue to mature slowly during the drying process."
msgstr ""

msgid "The seeds are removed from the umbels by hand. Gloves are worn because the seeds are covered with spines or beards. If you rub them on a sieve this will debeard the seeds. To sort the seeds, you should first use a sieve that retains the chaff. Then the seeds are retained by a finer sieve that allows dust to pass through."
msgstr ""

msgid "Finally, they should be winnowed or you can remove any remaining chaff by hand."
msgstr ""

msgid "Always include a label with the name of the variety and species as well as the year in the bag as writing on the outside can be rubbed off."
msgstr ""

msgid "Storing the seeds in the freezer for several days kills certain parasite larvae."
msgstr ""

msgid "Carrot seeds are able to germinate for up to five years. In certain cases, this may be extended to ten years. This can be further prolonged by storing the seeds in a freezer. Carrot seeds sometimes remain dormant during the first three months after the harvest. One gram of seed with their beards contains around 700 to 800 seeds."
msgstr ""

#. This title will be displayed by search engines like Google. 60 characters max.
msgctxt "seo-title"
msgid "Produce your carrot seeds: video explanations"
msgstr ""

#. This description will be displayed below the title on search engine results. 160 characters max.
msgctxt "seo-description"
msgid "This video will explain, step by step, how to produce your own carrot seeds: pollination, life cycle, extraction, sorting and storing."
msgstr ""

#. The tags are used by search engines to reference the content. Use commas to separate tags.
msgctxt "seo-tags"
msgid "learn, how to, produce, seeds, video, carrot, step by step, explanations"
msgstr ""
