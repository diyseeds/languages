# Copyright (C) 2021 Maxime Lecoq
# This file is distributed under the GNU General Public License v2 or later.
msgid ""
msgstr ""
"Project-Id-Version: Diyseeds 2.0\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"POT-Creation-Date: 2021-07-10T00:00:00+02:00\n"
"WPOT-Type: post-types/video\n"
"X-Domain: wpot\n"
"WPOT-Origin: glossary\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"Language: en\n"

#. Title of a video chapter.
msgid "Botanical classification and diversity"
msgstr ""

#. Title of a video chapter.
msgid "Pollination"
msgstr ""

#. Title of a video chapter.
msgid "Life cycle"
msgstr ""

#. Title of a video chapter.
msgid "Extracting - sorting - storing"
msgstr ""

#. The first %s will be replaced by the old family name. The second %s will be replaced by the new family name.
msgid "Recent systems of plant taxonomy which are based on the evolution of hereditary material no longer classify the %s as a separate family but as part of the %s family."
msgstr ""

#. The %s will be replaced by the link to this website.
msgid "More information and videos on %s."
msgstr ""

#. For every vegetable. This title will be displayed on Online Video Platforms (OVP) like YouTube or Vimeo. The %s will be replaced by the name of the vegetable.
msgctxt "ovp-title"
msgid "How to produce your own %s seeds?"
msgstr ""

#. For every vegetable. This description will be displayed on Online Video Platforms (OVP) like YouTube or Vimeo. The %s will be replaced by the name of the vegetable.
msgctxt "ovp-description"
msgid "Learn how to produce your own %s seeds by watching this educational film. It explains step by step the botanical classification, pollination and life cycle of the plant, the extraction and sorting of the seeds. You will find all the tips and tricks to learn how to produce your own seeds."
msgstr ""
