# Copyright (C) 2021 Maxime Lecoq
# This file is distributed under the GNU General Public License v2 or later.
msgid ""
msgstr ""
"Project-Id-Version: Diyseeds 2.0\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"POT-Creation-Date: 2021-07-10T00:00:00+02:00\n"
"WPOT-Type: post-types/video\n"
"X-Domain: wpot\n"
"WPOT-Origin: abc-pollination\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"Language: en\n"

#. Title of the film
msgctxt "title"
msgid "What is pollination?"
msgstr ""

msgid "Pollination is the way in which a large number of plants reproduce. The word “reproduce” is in fact not really exact because by using this process the plant will not produce an identical copy of itself. The male and female genetic elements will mix and potentially create a different plant. Pollination involves the transfer of pollen from the plant's male organ to the female organ in order to fructify it."
msgstr ""

msgid "The stamen is the male part of the flower. It has anthers that contain pollen grains. The female organ is called the pistil. It includes the ovary which contains one or several ovules."
msgstr ""

msgid "When they are fertilised by pollen the ovules develop into seeds and the ovary becomes, in this case, a pepper. In the case of radishes, the ovaries turn into seed pods containing several seeds."
msgstr ""

msgid "The vegetable plants intended for the production of seeds have different kinds of flowers. Many of them are hermaphrodite, which means that they have both the male and female organs in the same flower."
msgstr ""

msgid "There are hermaphrodite flowers in which the pollen is transferred within the same flower. The male organs release their pollen once the female organ becomes receptive. The flower is said to be self-pollinating and the plant is autogamous. This is the case with beans, lettuces and tomatoes are example."
msgstr ""

msgid "There are also hermaphrodite flowers whose male organs can only pollinate the female organs of another flower. Sometimes the male organ can only pollinate the female organ of a flower on a different plant."
msgstr ""

msgid "They must therefore rely on an external pollinator that carries the pollen from one flower to another, either on the same plant or on another plant. We call this cross-pollination and the plants are said to be allogamous."
msgstr ""

msgid "There are also plants with unisexual flowers, which means that they only have one organ, either male or female. The transfer of pollen must therefore happen between different flowers, from a male flower to a female one. The pollination is also crossed, and the plants allogamous. The male and female flowers can be present on the same plant. This is the case with cucumbers and corn, for example."
msgstr ""

msgid "But sometimes the male and female flowers can be on different plants. An example is spinach whose male flowers are on one plant and the female ones on another plant. Spinach is anemophilous, which means that it is wind-pollinated."
msgstr ""

msgid "There are therefore two ways in which pollination takes place:"
msgstr ""

msgid "with autogamous plants it can occur within the same flower,"
msgstr ""

msgid "and in the case of allogamous plants it requires external vectors."
msgstr ""

msgid "Some species can, however, combine the two forms of pollination. For example, in certain climatic conditions the tomato, which is generally considered to be autogamous, can become allogamous. In very hot weather, the female organs come out of the calyx and receive pollen from other flowers."
msgstr ""

msgid "Depending on the quantity and diversity of the insects present, the lettuce that is usually autogamous becomes allogamous when insects come and visit their flowers and transport the pollen further afield."
msgstr ""

msgid "Environmental conditions and the presence of other flowers in or near the garden will have an influence on the activity of insects and therefore on the pollination of the flowers of the plants you are growing for seed production."
msgstr ""

#. This title will be displayed by search engines like Google. 60 characters max.
msgctxt "seo-title"
msgid "How does pollination work?"
msgstr ""

#. This description will be displayed below the title on search engine results. 160 characters max.
msgctxt "seo-description"
msgid "Pollination is the preferred means used by a large part of plants to reproduce. Video explanations."
msgstr ""

#. The tags are used by search engines to reference the content. Use commas to separate tags.
msgctxt "seo-tags"
msgid "pollination, functioning, reproduction, plants, video, seeds, explanations, learn, ABC"
msgstr ""

#. This title will be displayed on Online Video Platforms (OVP) like YouTube or Vimeo.
msgctxt "ovp-title"
msgid "ABC What is pollination?"
msgstr ""

#. This description will be displayed on Online Video Platforms (OVP) like YouTube or Vimeo.
msgctxt "ovp-description"
msgid "Pollination is the preferred way for many plants to reproduce. This video explains how pollination works and its consequences for seed production."
msgstr ""
