# Copyright (C) 2021 Maxime Lecoq
# This file is distributed under the GNU General Public License v2 or later.
msgid ""
msgstr ""
"Project-Id-Version: Diyseeds 2.0\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"POT-Creation-Date: 2021-07-10T00:00:00+02:00\n"
"WPOT-Type: post-types/video\n"
"X-Domain: wpot\n"
"WPOT-Origin: cabbage\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"Language: en\n"

msgid "The cabbage is a member of the Brassicaceae family, the Brassica oleracea species and the capitata subspecies."
msgstr ""

msgid "The brassica oleracea species also includes kohlrabi, broccoli, Brussels sprouts, kale, cauliflower and the Savoy Cabbage."
msgstr ""

msgid "Cabbages can have different colours, green, white or red and have heads that are pointed or round. This subspecies is characterized by smooth leaves that form a tight head."
msgstr ""

msgid "Cabbage is a biennial plant that is grown for seed in the same way as cabbages for consumption. In the first year, the plant forms a head that remains over the winter and will flower the following year."
msgstr ""

msgid "The time to sow is determined by climate conditions, the overwintering method and the earliness of the variety."
msgstr ""

msgid "To overwinter heads of cabbage, sow in mid-May or at the beginning of June, even later for early varieties, to avoid over-developed and split heads at the end of autumn. Heads that are smaller but tight will winter better."
msgstr ""

msgid "At least 30 plants should be saved the first year so that 10 to 15 remain at the end of winter."
msgstr ""

msgid "Seeds are saved from healthy plants that have been observed over the entire period of growth so that all of their characteristics are known."
msgstr ""

msgid "The most vigorous heads are chosen in accordance with the variety’s specific characteristics and your selection criteria: regular and vigorous growth, formation of tight heads, storage capacity, precocity and resistance to cold."
msgstr ""

msgid "Overwintering is the most sensitive period of seed production. There are several methods for overwintering plants depending on climate conditions, vegetation period and available infrastructure."
msgstr ""

msgid "In regions with a harsh winter, the entire plant along with its roots is harvested at the end of autumn."
msgstr ""

msgid "The outer leaves are removed so that only the tight, firm leaves of the head remain. The heads should be dry and free of soil."
msgstr ""

msgid "In regions in which the level of humidity of the air is low, plants can be stored in a cellar with an earthen floor."
msgstr ""

msgid "In regions with high air humidity, plants can be stored in a frost-free room or attic. The temperature in the room should not fall below 0° C over a long period even though the cabbages can resist short periods of frost at -5° C."
msgstr ""

msgid "Throughout winter it is necessary to keep an eye on the cabbages. The outer leaves can be attacked by gray mold (Botrytis cinera). They should be removed along with the rotten parts and then the wounds should be disinfected with wood ash."
msgstr ""

msgid "Certain very resistant varieties or those grown in regions with mild winters can be left in the ground over winter."
msgstr ""

msgid "In mild climates they can also be stored in the ground: the plants along with their roots are laid in deep channels, slightly tilted up and covered with soil. The plants should not touch one another and in cases of frost must be covered with a pane of glass, manure or dead leaves."
msgstr ""

msgid "This protection is removed in spring but the plants should not be replanted. They will push up through the soil covering them and then flower."
msgstr ""

msgid "Another technique involves saving the roots without the heads, which can be eaten. At the end of summer during a dry period, the heads are cut off at the base at a slight angle. Only the stem and the roots are kept."
msgstr ""

msgid "These are left to dry for several days and disinfected with wood ash. To prevent rot, the dry cut can be coated with grafting wax. This method of propagation makes overwintering possible, but the stems preserved in this way produce fewer seeds of lower quality. They cannot flower from the centre of the stem from which the most beautiful seed stalks produce the best seeds."
msgstr ""

msgid "The seed plants that have been stored over winter in a cellar or attic are replanted in the spring of the second year of the cycle, in March or April."
msgstr ""

msgid "The cabbages are buried 60 cm apart so that the top of the heads are at ground level. The plants will grow new roots. It is important to water the plants well when they are planted and during the period of root growth."
msgstr ""

msgid "To encourage the emergence of flower stalks from the heads it is often necessary to make an incision in the form of a cross at the top of the head using a knife. This should be 3 to 6 cm deep depending on the size of the head. Take care not to damage the base of the cabbage from which the seed stalks will grow. Sometimes it is necessary to repeat the incision if a flower stalk does not appear."
msgstr ""

msgid "The central flower stalk produces the best seeds. Weaker secondary stalks can be removed to allow the central inflorescence to develop better and to channel all the strength of the plant into producing seed."
msgstr ""

msgid "Since the seed stalks can reach a height of 2 meters, it is necessary to stake each plant and secure the stalks which can become very heavy when the seeds are formed."
msgstr ""

#. This title will be displayed by search engines like Google. 60 characters max.
msgctxt "seo-title"
msgid "Produce your cabbage seeds: video explanations"
msgstr ""

#. This description will be displayed below the title on search engine results. 160 characters max.
msgctxt "seo-description"
msgid "This video will explain, step by step, how to produce your own cabbage seeds: pollination, life cycle, extraction, sorting and storing."
msgstr ""

#. The tags are used by search engines to reference the content. Use commas to separate tags.
msgctxt "seo-tags"
msgid "learn, how to, produce, seeds, video, cabbage, step by step, explanations"
msgstr ""
