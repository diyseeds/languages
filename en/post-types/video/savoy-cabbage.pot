# Copyright (C) 2021 Maxime Lecoq
# This file is distributed under the GNU General Public License v2 or later.
msgid ""
msgstr ""
"Project-Id-Version: Diyseeds 2.0\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"POT-Creation-Date: 2021-07-10T00:00:00+02:00\n"
"WPOT-Type: post-types/video\n"
"X-Domain: wpot\n"
"WPOT-Origin: savoy-cabbage\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"Language: en\n"

msgid "The Savoy cabbage is a member of the Brassicaceae family, the Brassica oleraceae species and the Sabauda subspecies."
msgstr ""

msgid "The brassica oleracea species also includes kohlrabi, broccoli, cabbage, Brussels sprouts, kale and cauliflower."
msgstr ""

msgid "Savoy cabbages are characterized by curly leaves that form a head slightly less compact than cabbages. There are Savoy cabbages grown in spring and summer with leaves and a head that are less tight, Savoy cabbages with a very large head for storage and winter Savoy cabbages with a head that is green and light."
msgstr ""

msgid "Savoy cabbage is a biennial plant. In the first year of the cycle it is grown for seed in the same way as for consumption."
msgstr ""

msgid "It will produce seeds in the second year."
msgstr ""

msgid "You should select 10 to 15 plants to ensure good genetic diversity."
msgstr ""

msgid "Savoy cabbage seeds are saved from healthy plants that have been observed over the entire period of growth. This ensures that all the characteristics of the variety are known."
msgstr ""

msgid "You should choose the most vigorous heads that meet the desired selection criteria : regular and vigorous growth, rapid formation of heads, good storage capacity, precocity, resistance to cold and to disease. Other characteristics influencing selection are the typical shape of the variety, a pointed, flat or round head, a short stem, a good root system, taste, colour."
msgstr ""

msgid "It is much more resistant to cold than other species of Brassica oleracea and can withstand temperatures as low as -15°. Most varieties can spend the winter outdoors."
msgstr ""

msgid "The other methods of overwintering and the second year of the life cycle are the same as for the cabbage."
msgstr ""

#. This title will be displayed by search engines like Google. 60 characters max.
msgctxt "seo-title"
msgid "Produce your savoy cabbage seeds: video explanations"
msgstr ""

#. This description will be displayed below the title on search engine results. 160 characters max.
msgctxt "seo-description"
msgid "This video will explain, step by step, how to produce your own savoy cabbage seeds: pollination, life cycle, extraction, sorting and storing."
msgstr ""

#. The tags are used by search engines to reference the content. Use commas to separate tags.
msgctxt "seo-tags"
msgid "learn, how to, produce, seeds, video, savoy cabbage, step by step, explanations"
msgstr ""
