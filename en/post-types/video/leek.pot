# Copyright (C) 2021 Maxime Lecoq
# This file is distributed under the GNU General Public License v2 or later.
msgid ""
msgstr ""
"Project-Id-Version: Diyseeds 2.0\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"POT-Creation-Date: 2021-07-10T00:00:00+02:00\n"
"WPOT-Type: post-types/video\n"
"X-Domain: wpot\n"
"WPOT-Origin: leek\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"Language: en\n"

msgid "Leeks belong to the Amarylidaceae family, which used to be called Alliaceae. They are part of the Allium ampeloprasum species. It is a biennial plant grown for the long, white stalks it produces in its first year."
msgstr ""

msgid "There are several varieties of leeks. Their leaves may be green, blue or even purple."
msgstr ""

msgid "Some varieties may be grown early in the season, others late. Certain varieties will resist very cold temperatures. Others will struggle to survive the winter."
msgstr ""

msgid "The flower heads of leeks are made up of small hermaphrodite flowers that are individually sterile. They need insects to fertilise them. This means that leeks are allogamous."
msgstr ""

msgid "Insects may cause cross pollination between different varieties of leek, or with wild leeks. On the other hand, they do not cross with onions, chives or black garlic."
msgstr ""

msgid "To avoid crossing between varieties don’t plant two varieties of leek less than 1 kilometer apart. This distance can be reduced to 400 meters if there's a natural barrier, such as a hedge." 
msgstr ""

msgid "It is also possible to use mosquito nets to isolate each variety. You can cover one variety with a permanent net and put a bumble bee hive inside. Or you can cover two varieties with different nets: open one while the other is closed on one day, and alternate the next day. These methods are described in greater detail in the module on ‘mechanical isolation techniques’ in the “ABC of seed production”."
msgstr ""

msgid "Grow leeks for seeds in the same way as those for food in the first year. They will produce seeds in the second year."
msgstr ""

msgid "Get rid of leeks that produce a flower in their first year. Plants that result from those seeds would also flower too early."
msgstr ""

msgid "There are several methods for storing leeks grown for seeds."
msgstr ""

msgid "In regions where the winter is cold, uproot them before the frosts start. It is important to choose the seed-bearing plants according to the specific characteristics of the variety. These are precocity, the size and thickness of the stalk, the colour of the leaves and their resistance to frost and to illnesses. Select 30 plants as some of them may be lost during the winter. Then place them in a container in a cool area that is protected from the cold."
msgstr ""

msgid "You need 20 plants for seeds to guarantee good genetic diversity. Certain so-called summer leeks will survive winter better if you do this."
msgstr ""

msgid "The simplest method, if the weather is warm enough, is to leave them in the ground in the garden."
msgstr ""

msgid "At the beginning of spring replant the leeks that you have stored over winter. Cutting the leaves will help ensure that the plants develop well."
msgstr ""

msgid "The flowering stalks will grow to up to 1,50 meter, if not more."
msgstr ""

msgid "They need to be supported with stakes."
msgstr ""

msgid "Over a period of four weeks each individual flower within the leek flower head will blossom. The period lasting from the beginning of blossoming to full seed maturity is long."
msgstr ""

msgid "The seeds are ready when the seed pods have dried and reveal black seeds."
msgstr ""

msgid "To collect the seeds cut the flower head together with the top of the stem, place them in a cloth bag and leave them to dry in a well-ventilated and warm place."
msgstr ""

msgid "In cold and wet regions where wind or rain can cause the loss of mature seeds, harvest the plants before they are fully mature and leave them to mature in a dry place."
msgstr ""

msgid "The extraction, sorting and storing of leek seeds are practically the same as for onions."
msgstr ""

msgid "First of all, rub the flower heads between your hands and then crush them using a rolling pin. To remove the seeds that remain stuck, leave the seed pods in a freezer for a few hours. They should come out easier. To sort the seeds, use a fine sieve to retain the seeds but not the dust."
msgstr ""

msgid "Finish by winnowing the seeds to remove the light debris. You can place them in the wind, blow on top of them or use a small ventilator."
msgstr ""

msgid "Then pour the seeds in cold water and stir. The fertilised seeds are heavier, so they will sink. Remove the empty ones and the debris that float. Dry the good seeds on a plate. Once they are dry, they will run like sand."
msgstr ""

msgid "Always put a label with the name of the variety and species as well as the year of harvesting in the package, as writing on the outside may rub off."
msgstr ""

msgid "Leave the seeds in the freezer for a few days to kill any parasite larvae."
msgstr ""

msgid "Leek seeds have a germination capacity of two years. This can sometimes stretch to six years. As these seeds quickly lose their germination capacity, to lengthen this period keep the seeds in the freezer."
msgstr ""

#. This title will be displayed by search engines like Google. 60 characters max.
msgctxt "seo-title"
msgid "Produce your leek seeds: video explanations"
msgstr ""

#. This description will be displayed below the title on search engine results. 160 characters max.
msgctxt "seo-description"
msgid "This video will explain, step by step, how to produce your own leek seeds: pollination, life cycle, extraction, sorting and storing."
msgstr ""

#. The tags are used by search engines to reference the content. Use commas to separate tags.
msgctxt "seo-tags"
msgid "learn, how to, produce, seeds, video, leek, step by step, explanations"
msgstr ""
