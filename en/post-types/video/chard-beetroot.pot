# Copyright (C) 2021 Maxime Lecoq
# This file is distributed under the GNU General Public License v2 or later.
msgid ""
msgstr ""
"Project-Id-Version: Diyseeds 2.0\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"POT-Creation-Date: 2021-07-10T00:00:00+02:00\n"
"WPOT-Type: post-types/video\n"
"X-Domain: wpot\n"
"WPOT-Origin: chard-beetroot\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"Language: en\n"

msgid "Chard and beetroot belong to the Chenopodiaceae family and the Beta vulgaris species. They are biennial plants grown for their roots, stalks and leaves. There are five sub-species:"
msgstr ""

msgid "cicla includes different varieties of chard,"
msgstr ""

msgid "esculenta is made up of vegetable beetroots,"
msgstr ""

msgid "rapa consists of fodder beet varieties,"
msgstr ""

msgid "altissima consists of sugar beet varieties,"
msgstr ""

msgid "and the wild chard, called maritima, can be found on the Atlantic coast in Europe and as far away as India."
msgstr ""

msgid "Certain varieties of beetroot are early, while others are grown for conservation. The colour of the roots may vary: they can be pink, red and sometimes yellow, light purple or white. Their shape can also be very different: some are round, others flat, long or cone-shaped."
msgstr ""

msgid "Chard is mainly known for its white stalks and green leaves, but they can also be yellow, pink or red. Some varieties are entirely green and smaller, resembling the wild species."
msgstr ""

msgid "The small flowers of chard and beetroot are hermaphrodite. But the stamen, the male sexual organ of the flower, releases its pollen before the pistil, the female sexual organ, is ready to receive it. They are therefore self-sterile."
msgstr ""

msgid "Chard and beet are autogamous, because they need other plants to ensure fertilisation. They are generally wind-pollinated, but certain insects, such as dyptera or hemiptera can also pollinate chard and beetroot. All chards, including the wild ones cross-pollinate, as do beetroot, sugar and fodder beets."
msgstr ""

msgid "To avoid cross-fertilisation, leave 1 kilometer between two varieties of Beta vulgaris. This distance can be reduced to 500 meters if there's a natural barrier, such as a hedge. In regions where beet is cultivated commercially for its seeds, ensure a distance of 7 kilometers."
msgstr ""

msgid "It is also possible to isolate two varieties by protecting them under different mosquito nets that you open on alternate days. For more information on this technique, refer to the module on Isolation techniques in the “ABC of seed production”."
msgstr ""

msgid "Chard and beet plants grown for seed production are cultivated in the same way as those for consumption. They will produce seeds only in the second year."
msgstr ""

msgid "In spite of the very large amount of seeds produced by each plant, it is important to keep a dozen plants to ensure genetic diversity."
msgstr ""

msgid "Beetroot are generally harvested in the autumn at the end of the first year’s cycle."
msgstr ""

msgid "When uprooting the crop you should select those for seed production according to the specific criteria of the variety, such as colour, shape, strength of the plant..."
msgstr ""

msgid "Clean the roots by brushing them, without water, and cut the leaves above the collar. Store the roots in a sandbox to protect them from the frost and light."
msgstr ""

msgid "Ideal storing conditions are at a temperature of 1°C and between 90 and 95% humidity. During winter, make sure that you remove any roots that rot."
msgstr ""

msgid "As for chards, in regions with severe winters and strong frosts, you will also have to uproot and store the roots inside in autumn. You must cut all the leaves at the collar except those at the centre."
msgstr ""

msgid "Without wounding them, store the roots in a cellar, in a box with slightly dampened sand."
msgstr ""

msgid "Most of the time, however, you can leave chard in the garden over winter if they have a well-developed root system. Mulch them with straw during particularly cold spells to protect them."
msgstr ""

msgid "In milder climates you can also leave beetroots in the ground, provided that you sow them late in August or September."
msgstr ""

msgid "In spring select the roots for seed production. You can uncover the collar to examine them and get rid of those which do not correspond to the variety’s required characteristics. This is not, however, the best method to select seed producing plants. It is better to uproot, select and immediately replant them."
msgstr ""

msgid "When replanting chard and beetroots that have been stored over the winter you should ensure that the collar is at ground level. Water copiously."
msgstr ""

msgid "The next steps of the cycle are the same for chard and beetroots."
msgstr ""

msgid "The flower stalks will develop to up to 1,50 meter. They often need supporting with a stake. The seed-forming process may be difficult or even impossible in regions where there is little difference between the lengths of day and night. They need long summer days to fructify."
msgstr ""

msgid "Cut the flower stalks, after the dew, when the first seeds are mature. The seeds are in fact clusters containing 2 to 6 individual seeds. It is possible to cut the stalks all in one go, but you may lose part of the first mature seeds. Whatever the case, it is better to continue drying in a shaded, dry and well-ventilated area."
msgstr ""

msgid "Extract the seeds by rubbing the stems between your hands. It is better to wear gloves. It is also possible to walk on the stems, or to beat them using a stick."
msgstr ""

msgid "To sort them, first use a coarse sieve that will retain the waste. Then use a thinner sieve that will retain the seeds, but not the fine chaff."
msgstr ""

msgid "Finally, winnow them by blowing on them to get rid of the remaining chaff. Either do this yourself or use the wind."
msgstr ""

msgid "Always put a label with the name of the variety and species, as well as the year inside the sachet. Writing on the outside sometimes rubs off."
msgstr ""

msgid "Placing the seeds in the freezer for a few days will kill certain parasites."
msgstr ""

msgid "Chard and beet seeds have a germination capacity of 6 years, sometimes even 10 years. One gram of seeds represents about 50 clusters."
msgstr ""

#. This title will be displayed by search engines like Google. 60 characters max.
msgctxt "seo-title"
msgid "Produce your chard and beetroot seeds: video explanations"
msgstr ""

#. This description will be displayed below the title on search engine results. 160 characters max.
msgctxt "seo-description"
msgid "This video will explain, step by step, how to produce your own chard and beetroot seeds: pollination, life cycle, extraction, sorting and storing."
msgstr ""

#. The tags are used by search engines to reference the content. Use commas to separate tags.
msgctxt "seo-tags"
msgid "learn, how to, produce, seeds, video, chard, beetroot, step by step, explanations"
msgstr ""
