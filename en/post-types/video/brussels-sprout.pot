# Copyright (C) 2021 Maxime Lecoq
# This file is distributed under the GNU General Public License v2 or later.
msgid ""
msgstr ""
"Project-Id-Version: Diyseeds 2.0\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"POT-Creation-Date: 2021-07-10T00:00:00+02:00\n"
"WPOT-Type: post-types/video\n"
"X-Domain: wpot\n"
"WPOT-Origin: brussels-sprout\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"Language: en\n"

msgid "The Brussels sprout is a member of the Brassicaceae family, the Brassica oleracea species and the gemmifera subspecies."
msgstr ""

msgid "The brassica oleracea species also includes kohlrabi, broccoli, cabbage, kale, cauliflower and the Savoy Cabbage."
msgstr ""

msgid "Brussels sprouts are an autumn and winter vegetable in regions with a cold, temperate climate. They form small sprouts at the base of the leaves and can endure very low temperatures."
msgstr ""

msgid "The Brussels sprout is a biennial plant. It will produce its edible sprouts in autumn and winter. It will form its flower stalks in the following spring."
msgstr ""

msgid "Plants for seed are grown in the same way as those for consumption. They are sown in May or June."
msgstr ""

msgid "You should select 15 plants for seed production to ensure good genetic diversity."
msgstr ""

msgid "Seeds are saved from healthy plants that have been observed throughout the period of growth. This enables you to check all of the characteristics of the variety, such as the formation of regular sprouts along the entire stem, the compactness, colour and shape of the sprouts, their taste (no bitterness), resistance to the cold, yield, and the size of the plant."
msgstr ""

msgid "Plants can reach a height of 60 to 80 cm in the first year."
msgstr ""

msgid "In autumn, the sprouts along the stalk can be harvested, but the sprouts at the top must never be removed."
msgstr ""

msgid "The Brussels sprout is more resistant to cold than large cabbages, and winter varieties can remain in the ground throughout the winter. If necessary, they can be protected with a frost blanket."
msgstr ""

msgid "In the second year, the stalks can reach a height of one and a half meters. To avoid them falling, it is sometimes necessary to support the flower stalks with stakes."
msgstr ""

msgid "The top of the stem can be cut to accelerate the flowering process."
msgstr ""

#. This title will be displayed by search engines like Google. 60 characters max.
msgctxt "seo-title"
msgid "Produce your Brussels sprout seeds: video explanations"
msgstr ""

#. This description will be displayed below the title on search engine results. 160 characters max.
msgctxt "seo-description"
msgid "This video will explain, step by step, how to produce your own Brussels sprout seeds: pollination, life cycle, extraction, sorting and storing."
msgstr ""

#. The tags are used by search engines to reference the content. Use commas to separate tags.
msgctxt "seo-tags"
msgid "learn, how to, produce, seeds, video, Brussels sprout, step by step, explanations"
msgstr ""
