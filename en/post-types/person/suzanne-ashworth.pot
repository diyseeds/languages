# Copyright (C) 2021 Maxime Lecoq
# This file is distributed under the GNU General Public License v2 or later.
msgid ""
msgstr ""
"Project-Id-Version: Diyseeds 2.0\n"
"POT-Creation-Date: 2021-07-10T00:00:00+02:00\n"
"PO-Revision-Date: 2021-08-13 09:28+0000\n"
"Language: en\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 4.5.1\n"
"WPOT-Type: post-types/person\n"
"WPOT-Origin: suzanne-ashworth\n"
"X-Domain: wpot\n"

#. This title will be displayed into the browser's window title bar.
msgctxt "browser-title"
msgid "Suzanne Ashworth"
msgstr ""

#. The slug is the last part of the URL (www.example.org/slug-is-here). Only alphanumerical characters are allowed, use dash to replace space. The slug should be short and nice and it must reflect the content it refers to. It is usually made from the title. If you don't provide it, a slug will be auto-generated.
msgctxt "slug"
msgid "suzanne-ashworth"
msgstr ""

msgctxt "name"
msgid "Suzanne Ashworth"
msgstr ""

msgctxt "given-name"
msgid "Suzanne"
msgstr ""

msgctxt "family-name"
msgid "Ashworth"
msgstr ""

msgctxt "description"
msgid "Suzanne Ashworth is the author of the book “Seed to seed”."
msgstr ""
