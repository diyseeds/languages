# Copyright (C) 2021 Maxime Lecoq
# This file is distributed under the GNU General Public License v2 or later.
msgid ""
msgstr ""
"Project-Id-Version: Diyseeds 2.0\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"POT-Creation-Date: 2021-07-10T00:00:00+02:00\n"
"Language: en\n"
"WPOT-Type: post\n"
"WPOT-Origin: why-produce-your-own-seeds\n"
"X-Domain: wpot\n"

#. This title will be displayed into the browser's window title bar.
msgctxt "browser-title"
msgid "Why produce your own seeds?"
msgstr ""

#. The slug is the last part of the URL (www.example.org/slug-is-here). Only alphanumerical characters are allowed, use dash to replace space. The slug should be short and nice and it must reflect the content it refers to. It is usually made from the title. If you don't provide it, a slug will be auto-generated.
msgctxt "slug"
msgid "why-produce-your-own-seeds"
msgstr ""

#. Title of a page's section. Main title of this article.
msgctxt "section-title"
msgid "Why produce your own seeds?"
msgstr ""

msgid "Producing your own seeds will give you greater autonomy; you will thereby contribute towards maintaining a common heritage that has been developed over countless generations and that is fast disappearing; and it is an act of civil disobedience against increasingly restrictive laws that enable the big seed companies to gain total control over this source of life."
msgstr ""

msgid "The monopoly of agro-industry in a standardized and globalised market has led to the destruction of thousand-year old agricultural systems that enabled generations to feed themselves."
msgstr ""

#. Title of a page's section. The industry stoled the knowledges of farmers and use them in a dangerous way.
msgctxt "section-title"
msgid "By spoliating peasant know-how, industry makes us vulnerable"
msgstr ""

msgid "There used to exist hundreds of thousands of vegetable species across the planet, but today they face an accelerated process of extinction and the world’s food supply relies on an increasingly limited number of plant species. Each region, each valley, used to have its own varieties adapted to local conditions. Exchange between farmers was part of life. Varieties travelled."
msgstr ""

msgid "Industrial agriculture, on the other hand, needs “homogenous” and “stable” varieties that will produce uniform vegetables with a long shelf life. This is the very opposite of the selection criteria used by farmers who developed “populations” rich in diversity and with the faculty to adapt, to evolve and to resist to changing local constraints."
msgstr ""

msgid "Most plant diseases are exacerbated by industrial agriculture. The monocultures at the heart of oversimplified and mechanised agrarian systems bring about irreversible genetic erosion that will only lead to famine in the future. They are an insult to the boundless ingenuity of generations of farmers."
msgstr ""

#. Title of a page's section. Producing peasant seeds could be illegal in Europe.
msgctxt "section-title"
msgid "An ancestral heritage that has become out of the law"
msgstr ""

msgid "In Europe increasingly restrictive legislation is linked to the obligation for seed producers to register their varieties in an official catalogue which imposes strict DHS criteria (distinctness, homogeneity and stability). These criteria are modeled on the needs of intensive production and in no way suit heirloom seeds or small producers. Those who do not comply with these laws can find themselves taken to court, as Kokopelli has experienced on several occasions in France. Such laws need to be fought. One of the best ways to resist against them is to increase the number of people producing seeds, be it legal or not."
msgstr ""

#. Title of a page's section. Peasant seeds are central in the sense they are at heart of societal resilience, it's a key topic.
msgctxt "section-title"
msgid "Peasant seeds are central to societal resilience"
msgstr ""

msgid "We live in times of crisis and conflict. Each war, each economic crisis, forces those affected, both individuals and societies as a whole, to readdress their most basic needs: a roof over one’s head, clothes to wear, and food to eat. The situation is not very promising. Most city dwellers no longer know how to plant a vegetable, while farmers depend almost entirely on a few multinationals for their seeds."
msgstr ""

msgid "In Greece, Syria and elsewhere, populations destabilized by crisis and war seek access to seeds. In Syria, in Irak, the cradle of all cereal crops, in Afghanistan where many vegetables have their origins, seed banks have been systematically destroyed, often by western bombs. These were an invaluable heritage of traditional heirloom seeds of varieties domesticated by generations of farmers."
msgstr ""

msgid "It is dangerous to entrust this heritage to a few gene banks to which farmers have little access. We should also keep in mind the bloody, mostly urban, food riots at the beginning of the 21st century, the consequence of speculation on cereal crops and of climate change."
msgstr ""

#. Title of a page's section. The genetic manipulation is a blind alley in the sense of a deadlock or an impasse.
msgctxt "section-title"
msgid "Idealized, the genetic manipulation is a dangerous blind alley"
msgstr ""

msgid "On a global scale genetically modified plants now cover a surface equivalent to that of Western Europe. The collections of ancient traditional corn varieties in Mexico, the cradle of this culture, have been contaminated by GMO corn imported from the United States."
msgstr ""

msgid "The GMOs that are being imposed on us will solve neither famine nor malnutrition, nor will they prevent plant or human diseases. They are in fact a menace to the environment and public health."
msgstr ""

msgid "In France, for example, selection work on oats has been abandoned by research institutes because it costs too much and because of the disappearance of the working horses for whom oats were the staple diet. But what if one day we need to return to animal power?"
msgstr ""

msgid "Proponents claim that industrial agriculture is the only way to ensure that a constantly growing population can be fed. On the contrary, they are endangering the future of the planet."
msgstr ""

#. Title of a page's section. Our societies need to produce their seeds in a resilient way to ensure their future.
msgctxt "section-title"
msgid "Peasant seeds are our future, let's get them back!"
msgstr ""

msgid "The disappearance of small farmers results in the disappearance of varieties and of the know-how needed to grow and multiply them. Small farmers and gardeners are seed guardians and we must therefore ensure that there are more and more of them."
msgstr ""

msgid "For all of these reasons we must preserve the great diversity of heirloom varieties and ensure free access to them, as they are the seeds of the future. The only way to guarantee their survival is to cultivate them in our gardens, instead of just keeping them in deep freezers or gene banks."
msgstr ""

msgid "The aim of this film is to provide a tool that will enable you to produce your own seeds. It is not very difficult, costs nothing and is a source of real pleasure. This know-how must not be monopolized by specialists working for companies that privatize access to seeds by patenting them or by creating sterile hybrid varieties."
msgstr ""

msgid "By producing your own seeds you can demystify this knowledge and achieve greater autonomy. Seeds are a common heritage that we must reappropriate and protect for the future."
msgstr ""

msgid "Jacques Berguerand, Longo maï, july 2015"
msgstr ""

#. This title will be displayed by search engines like Google. 60 characters max.
msgctxt "seo-title"
msgid "Why is it important to produce your own seeds?"
msgstr ""

#. This description will be displayed below the title on search engine results. 160 characters max.
msgctxt "seo-description"
msgid "Peasant seeds are very important for our future in order to preserve a common heritage that guarantees biodiversity. It is an essential freedom."
msgstr ""

#. The tags are used by search engines to reference the content. Use commas to separate tags.
msgctxt "seo-tags"
msgid "peasant seeds, heritage, protect, biodiversity, future, freedom"
msgstr ""
