# Copyright (C) 2021 Maxime Lecoq
# This file is distributed under the GNU General Public License v2 or later.
msgid ""
msgstr ""
"Project-Id-Version: Diyseeds 2.0\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"POT-Creation-Date: 2021-07-10T00:00:00+02:00\n"
"Language: en\n"
"WPOT-Type: post\n"
"WPOT-Origin: plant-selection-in-the-industrial-age\n"
"X-Domain: wpot\n"

#. This title will be displayed into the browser's window title bar.
msgctxt "browser-title"
msgid "Plant selection in the industrial age"
msgstr ""

#. The slug is the last part of the URL (www.example.org/slug-is-here). Only alphanumerical characters are allowed, use dash to replace space. The slug should be short and nice and it must reflect the content it refers to. It is usually made from the title. If you don't provide it, a slug will be auto-generated.
msgctxt "slug"
msgid "plant-selection-in-the-industrial-age"
msgstr ""

#. Title of a page's section. Main title of this article.
msgctxt "section-title"
msgid "Plant selection in the industrial age"
msgstr ""

msgid "Life exists thanks to the singularity of each organism, industry asserts itself through the uniformity of its goods."
msgstr ""

msgid "Living creatures reproduce themselves for free, while industry’s raison d’être is profit."
msgstr ""

msgid "For industrial capitalism life is this two-fold sacrilege."
msgstr ""

#. Title of a page's section. Industrial capitalism took control in the sense that it has granted itself control, using force with technics and laws.
msgctxt "section-title"
msgid "Industrial capitalism took control of selection by filing patents"
msgstr ""

msgid "Over the last two centuries, annihilating this double sacrilege has been the task that industrial capitalism has entrusted to plant breeders and the agronomic sciences. This task is now almost complete. You only have to see the oversized, over-green industrial fields scarred by the wheels of tractors, in which no plant is higher than its neighbours, to realize that uniformity has triumphed."
msgstr ""

msgid "As to the other sacrilege, the patenting of life crowns two centuries of efforts aimed at putting an end to the founding practice of agriculture, that of resowing one’s own harvested seed. This means separating production from reproduction, and making reproduction the privilege of a few. Today this privilege belongs to the cartel of “life sciences” – the producers of pesticides, herbicides, insecticides, larvicides, ovocides, gametocides, bactericides, molluscicides, rodenticides, acaricides, fungicides : the “Cartel of Cides”!"
msgstr ""

#. Title of a page's section. Industrial selection close off reproduction and multiplication in the sense that you cannot do it even if you would like to (it's technically locked).
msgctxt "section-title"
msgid "Industrial selection is based on the cloning technique to close off reproduction and multiplication"
msgstr ""

msgid "Industrial selection is simple if you break through the genetic smokescreen. It consists of replacing a variety (i.e. the opposite of uniformity) with copies of a selected plant within the variety – with clones. This signaled the death-bell for the sacrilege of diversity. There remained, however, the other sacrilege to be dealt with, which meant putting an end to free reproduction. This miracle was achieved by the new science of genetics. The result was “hybrid” corn, the sacred cow of agronomic sciences and “paradigm” of plant breeding of the 20th century."
msgstr ""

msgid "The cloning technique of breeding had been used empirically with autogamous species since the beginning of the 19th century, at the same time as the expansion of the industrial revolution. In 1836 it was codified as the “isolation method”. As an autogamous clone duplicates itself exactly, the harvested cereal is also the seed for the following year. The “farmer’s privilege”, that of sowing one’s own harvested grain, remained intact."
msgstr ""

#. Title of a page's section. This title should be understood like the law validates the fact that industry controls selection.
msgctxt "section-title"
msgid "How the law establishes the power of industry over the selection control"
msgstr ""

msgid "At the end of the 1920s French technocratic agronomists imposed a new system according to which varieties put on to the market must be “homogenous” and “stable”. Homogenous means that the plants must be phenotypically (visually) identical; to be considered stable an identical plant must be put on the market every year. This dual requirement means that the plants must be genetically identical or almost so. Their homogeneity and stability are controlled by an official body. If a new variety satisfies these criteria, it is registered in a Catalogue and the approved breeder receives a certificate which gives him the exclusive right to put the variety on the market. In 1960 this mechanism was adopted by the International Union for the Protection of New Varieties of Plants (UPOV) which has now been ratified by about sixty countries."
msgstr ""

msgid "The certificate protects the seed producer from “piracy” of his clones by competitors, but there were still too many farmers for the seed industry to dare to attack the “farmer’s privilege”. The certificate met the needs of traditional plant breeding companies run by agronomists passionate about their work. Over the last thirty years, however, the “Cartel of Cides” has taken over the world’s seeds. This cartel considers the farmer who sows the seeds he harvests to be a “pirate”. European directive 98/44 authorizing the patenting of life is putting an end to this “piracy”."
msgstr ""

#. Title of a page's section. The industry cannot do a real work of selection because it requires years of experiment producing vegetables, it is a farmer work.
msgctxt "section-title"
msgid "True plant selection cannot be industrial"
msgstr ""

msgid "In short, the history of industrial plant breeding is that of destructive selection-cloning and the relentless exploitation of the diversity created by the cooperation between farmers and nature since the beginning of domestication of plants and animals. Farmers did not wait for genetics to “improve” plants. The proof lies in the great profusion of cultivated varieties and animal breeds."
msgstr ""

msgid "In the 1980s I had the fortune to see a great wheat breeder, Claude Benoît, working in a wheat field. As far as I could see, all the plants were the same. By the end of the day, I began to crudely distinguish them. Claude Benoît could not himself explain what drew his attention to this or that plant in a crop that seemed identical to me."
msgstr ""

msgid "Selection depends on a “non-codified” know-how that cannot be fully explained. The meticulous work of the plant breeder is guided by experience, by years of familiarity with the plant, by empathy or even love [1], by an acute sense of observation, by his agronomic knowledge. He does not need the geneticist [2]. The esoteric nature of genetics intimidates and discourages those who would like to become involved in selection. The aim is of course to ensure that they give up."
msgstr ""

#. Title of a page's section. Our societies need to produce their seeds in a resilient way to ensure their future.
msgctxt "section-title"
msgid "It is vital that our societies take back control of the selection of plants to ensure their future"
msgstr ""

msgid "There is no point in shedding crocodile tears about the collapse of cultivated biodiversity if one ignores the dynamics of industrial capitalism that encourage this phenomenon. Moreover, the legislative and regulatory system that constitutes the framework for the production and sale of seeds imposes one single selection method based on cloning that started two centuries ago."
msgstr ""

msgid "It is therefore vital to struggle against the disgrace of patenting life, to demand the abolition of a legal framework that enforces selection methods that destroy diversity. We must ensure that the “Cartel of Cides” no longer poisons life. But at the same time, we must organize collectively throughout the world to cultivate diversity, share seeds, spread the required know-how, as generations of farmers have done before us. This is an act of survival, of resistance and of freedom. Kokopelli has shown the way. In this film Longo maï describes, step by step, how we can regain possession of both our seeds and our future."
msgstr ""

msgid "Jean-Pierre Berlan, former director of research, french National Instutute of Agronomic Research (INRA)"
msgstr ""

msgid "[1] An old plant breeder from INRA said this magnificent thing to me, a little embarrassed: “You know, when I’m on my own with my plants I talk to them”."
msgstr ""

msgid "[2] For this reason the “Cartel of Cides” has bought out seed companies. Their molecular biologists who manipulate genes are quite incapable of carrying out plant selection."
msgstr ""

#. This title will be displayed by search engines like Google. 60 characters max.
msgctxt "seo-title"
msgid "The history of plant selection in the industrial age"
msgstr ""

#. This description will be displayed below the title on search engine results. 160 characters max.
msgctxt "seo-description"
msgid "This article summarizes the history of seed breeding or how the industry prevails through uniformity of commodities."
msgstr ""

#. The tags are used by search engines to reference the content. Use commas to separate tags.
msgctxt "seo-tags"
msgid "selection, seeds, history, industry, uniformity, patents, varieties, capitalism"
msgstr ""
