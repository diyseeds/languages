# Copyright (C) 2021 Maxime Lecoq
# This file is distributed under the GNU General Public License v2 or later.
msgid ""
msgstr ""
"Project-Id-Version: Diyseeds 2.0\n"
"POT-Creation-Date: 2021-07-10T00:00:00+02:00\n"
"PO-Revision-Date: 2021-08-13 13:21+0000\n"
"Language: en\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 4.5.1\n"
"WPOT-Type: post-types/organization\n"
"WPOT-Origin: fifteenth-garden\n"
"X-Domain: wpot\n"

#. This title will be displayed into the browser's window title bar.
msgctxt "browser-title"
msgid "Fifteenth Garden"
msgstr ""

#. The slug is the last part of the URL (www.example.org/slug-is-here). Only alphanumerical characters are allowed, use dash to replace space. The slug should be short and nice and it must reflect the content it refers to. It is usually made from the title. If you don't provide it, a slug will be auto-generated.
msgctxt "slug"
msgid "fifteenth-garden"
msgstr ""

msgctxt "name"
msgid "Fifteenth Garden"
msgstr ""

msgctxt "description"
msgid "15th Garden is a bottom-up network of urban gardens and seed savers in bombed, besieged and blockaded Syrian cities. This growing food sovereignty network also includes family gardens, rural farms and agricultural initiatives in refugee camps in neighbouring states."
msgstr ""

#. This title will be displayed by search engines like Google. 60 characters max.
msgctxt "seo-title"
msgid "15th Garden, a bottom-up network of urban gardens"
msgstr ""

#. This description will be displayed below the title on search engine results. 160 characters max.
msgctxt "seo-description"
msgid "A bottom-up network of urban gardens and seed savers in bombed, besieged and blockaded Syrian cities."
msgstr ""

#. The tags are used by search engines to reference the content. Use commas to separate tags.
msgctxt "seo-tags"
msgid "network, gardens, Syria, bombings, food sovereignty, refugees"
msgstr ""
