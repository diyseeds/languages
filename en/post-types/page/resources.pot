# Copyright (C) 2021 Maxime Lecoq
# This file is distributed under the GNU General Public License v2 or later.
msgid ""
msgstr ""
"Project-Id-Version: Diyseeds 2.0\n"
"POT-Creation-Date: 2021-07-10T00:00:00+02:00\n"
"PO-Revision-Date: 2021-08-13 09:28+0000\n"
"Language: en\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 4.5.1\n"
"WPOT-Type: post-types/page\n"
"WPOT-Origin: resources\n"
"X-Domain: wpot\n"

#. This title will be displayed into the browser's window title bar.
msgctxt "browser-title"
msgid "Supplementary Diyseeds resources"
msgstr ""

#. The slug is the last part of the URL (www.example.org/slug-is-here). Only alphanumerical characters are allowed, use dash to replace space. The slug should be short and nice and it must reflect the content it refers to. It is usually made from the title. If you don't provide it, a slug will be auto-generated.
msgctxt "slug"
msgid "resources"
msgstr ""

#. Title of a page's section.
msgctxt "section-title"
msgid "Resources"
msgstr ""

#. Title of a page's section.
msgctxt "section-title"
msgid "The botanical classification trees"
msgstr ""

msgid "We commissioned the artist Myleine Guiard Schmid to draw the botanical family trees. She would like to make these watercolours available for non profit-making training activities under the creative commons licence. For any requests please contact Myleine Guiard Schmid."
msgstr ""

#. Title of a page's section.
msgctxt "section-title"
msgid "Books on seed production"
msgstr ""

msgid "To help us make these films we used the following very precious books:"
msgstr ""

#. Text of the button to download all the images of the botanical trees.
msgctxt "button"
msgid "All"
msgstr ""

#. This title will be displayed by search engines like Google. 60 characters max.
msgctxt "seo-title"
msgid "To go further in the production of seeds"
msgstr ""

#. This description will be displayed below the title on search engine results. 160 characters max.
msgctxt "seo-description"
msgid "Books, drawings, other educational content and communication material. Find a set of resources on seed production recommended by Diyseeds."
msgstr ""

#. The tags are used by search engines to reference the content. Use commas to separate tags.
msgctxt "seo-tags"
msgid "seeds, books, drawings, resources, botanical trees, diyseeds"
msgstr ""
