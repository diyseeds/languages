﻿1
00:00:07,700 --> 00:00:12,140
The selection and growing of plants intended for seed production

2
00:00:18,565 --> 00:00:24,090
The selection process enables you
to gradually adapt the plant to the environment,

3
00:00:24,650 --> 00:00:26,430
to your needs and wishes.

4
00:00:27,140 --> 00:00:32,625
It is important to pay particular attention
to plants grown for their seeds,

5
00:00:33,795 --> 00:00:38,280
as they will produce the future generations
of vegetables and fruits.

6
00:00:40,560 --> 00:00:43,780
It is therefore necessary to choose them well

7
00:00:44,095 --> 00:00:47,180
and to observe them
throughout their development. 

8
00:00:52,725 --> 00:00:56,215
Selection carried out only
on the basis of the fruit

9
00:00:56,825 --> 00:01:01,800
will not reflect all of the characteristics
linked to the development of plants. 

10
00:01:04,425 --> 00:01:07,795
Selection criteria must be precisely defined,

11
00:01:08,380 --> 00:01:11,795
as they will determine the choice
of plants for seed production. 

12
00:01:13,860 --> 00:01:18,640
You should first of all consider
the criteria specific to the variety,

13
00:01:19,270 --> 00:01:24,270
such as resistance to diseases, yield, precocity,

14
00:01:24,695 --> 00:01:29,535
as well as more subjective criteria
such as taste and appearance.

15
00:01:43,890 --> 00:01:47,000
The capacity of plants
to adapt to their environment

16
00:01:47,195 --> 00:01:51,035
and to cultivation methods should
also be taken into account. 

17
00:01:54,175 --> 00:01:59,515
The key thing is to know from the start
what aspects you want to give priority to,

18
00:02:00,160 --> 00:02:03,695
because over generations
the variety will evolve

19
00:02:03,895 --> 00:02:07,850
and certain characteristics will
become less present or dominant. 

20
00:02:13,340 --> 00:02:17,465
Sometimes it is not wise
to carry out a selection process.

21
00:02:17,770 --> 00:02:22,450
This is the case when there are
very few seeds left of a rare variety

22
00:02:22,740 --> 00:02:24,885
facing the risk of extinction,

23
00:02:25,120 --> 00:02:28,235
as there will not be enough plants
produced the first year

24
00:02:28,305 --> 00:02:30,400
to enable you to carry out selection. 

25
00:02:35,765 --> 00:02:41,100
You can also decide to preserve
the variety as it is, without a selection process.

26
00:02:41,470 --> 00:02:45,140
In this case you take all
of the elements of the variety,

27
00:02:45,660 --> 00:02:51,220
even if there is a strong disparity,
in order to encourage genetic diversity.

28
00:02:52,195 --> 00:02:55,240
This creates a strong capacity for adaptation

29
00:02:55,670 --> 00:02:58,785
and provides a reservoir of potentialities

30
00:02:58,955 --> 00:03:01,775
which could provide
the basis of future selection. 

31
00:03:09,170 --> 00:03:12,870
It is important to clearly mark
the plants grown for seed production

32
00:03:14,590 --> 00:03:18,270
so as to distinguish them from
those grown for their vegetables. 

33
00:03:20,885 --> 00:03:25,595
Drawing up a plan of the garden
indicating the location of each variety

34
00:03:25,940 --> 00:03:29,150
will help you find them
if the labels have disappeared. 

35
00:03:31,095 --> 00:03:35,920
The complete cycle of plants through
to their seeds is often longer

36
00:03:36,125 --> 00:03:38,505
than that of those grown for their vegetables.

37
00:03:39,215 --> 00:03:42,865
For example, you can eat lettuce
after two or three months,

38
00:03:43,110 --> 00:03:48,060
but its full life cycle through
to seed harvest is five to six months.

39
00:04:05,450 --> 00:04:09,180
There exist many biennial plants,
such as the carrot,

40
00:04:09,570 --> 00:04:13,910
which flower and produce their seeds
in the second year of growth. 

41
00:05:00,780 --> 00:05:06,660
The best of all is to reserve a small corner
of the garden specifically for seed production. 

