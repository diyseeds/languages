1
00:00:12,100 --> 00:00:16,780
Sweet peppers and chilli peppers
belong to the Solanaceae family.

2
00:00:18,065 --> 00:00:22,390
The genus Capsicum includes
5 cultivated species :

3
00:00:22,940 --> 00:00:34,205
capsicum baccatum, capsicum chinense,
capsicum frutescens and capsicum pubescens.

4
00:00:35,580 --> 00:00:38,860
The vast majority of cultivated varieties

5
00:00:39,155 --> 00:00:42,060
belong to the Capsicum annum species,

6
00:00:46,185 --> 00:00:51,060
of which there are thousands of varieties,
including sweet peppers,

7
00:00:53,300 --> 00:00:56,980
mild chilli peppers and very spicy chillis.

8
00:00:58,915 --> 00:01:04,090
There is a great diversity in size,
shape and colour of peppers.

9
00:01:07,435 --> 00:01:08,340
Pollination

10
00:01:21,440 --> 00:01:26,045
The flowers of pepper plants
are hermaphrodite and self-fertilising,

11
00:01:26,185 --> 00:01:31,980
which means that the male and female organs
are in the same flower and are compatible.

12
00:01:35,380 --> 00:01:37,615
They are therefore autogamous.

13
00:01:40,220 --> 00:01:43,380
The flowers can however also be fertilised

14
00:01:43,440 --> 00:01:47,490
by pollinating insects
such as bumblebees and bees. 

15
00:01:51,565 --> 00:01:54,120
They are sensitive to changes in temperature :

16
00:01:55,350 --> 00:02:02,575
if at night, the temperature is too high (29°)
or too low (5°),

17
00:02:02,740 --> 00:02:06,540
the flowers will fail,
meaning they will not be pollinated.

18
00:02:07,500 --> 00:02:11,765
As a consequence,
the peppers will have few or no seeds,

19
00:02:12,235 --> 00:02:15,015
which will greatly influence
the size of the fruit.

20
00:02:22,370 --> 00:02:29,020
The best fructification is when
night temperatures vary between 12 and 16°C. 

21
00:02:32,910 --> 00:02:35,100
To encourage self-fertilisation,

22
00:02:35,420 --> 00:02:39,320
you can regularly shake the plants
during the flowering season. 

23
00:02:42,140 --> 00:02:46,825
All species of the Capsicum genus
can cross-pollinate,

24
00:02:47,510 --> 00:02:50,840
with the exception
of the Capsiscum pubescens species. 

25
00:02:58,215 --> 00:03:02,935
To avoid cross-pollination between
two varieties in temperate climates,

26
00:03:03,490 --> 00:03:06,050
leave 100m between them. 

27
00:03:07,550 --> 00:03:13,910
This distance can be reduced to 50m
if there is a natural barrier such as a hedge. 

28
00:03:18,080 --> 00:03:22,925
In tropical climates,
keep 1km between two varieties. 

29
00:03:23,600 --> 00:03:26,780
And 500m if there is a hedge 

30
00:03:31,485 --> 00:03:37,505
To avoid cross-pollination by insects,
you can also isolate plants using nets,

31
00:03:38,300 --> 00:03:42,120
either in a tunnel
or under a permanent mosquito net.

32
00:03:44,470 --> 00:03:47,840
But watch out, peppers need a lot of light.

33
00:03:48,780 --> 00:03:53,140
Too tight and restrictive cages
will hinder plant development

34
00:03:53,680 --> 00:03:56,290
and reduce fruit and seed production.

35
00:03:59,505 --> 00:04:04,815
See the module on mechanical isolation
techniques in the ABC of seed production. 

36
00:04:14,300 --> 00:04:15,340
Life cycle 

37
00:04:31,505 --> 00:04:34,500
Peppers are annual plants in most cases,

38
00:04:36,880 --> 00:04:40,180
but some species are perennial
in tropical climates.

39
00:04:45,395 --> 00:04:48,510
It is a plant that needs
a lot of heat to develop well. 

40
00:04:54,820 --> 00:04:59,340
Peppers grown for their seed are cultivated
in the same way as those for food. 

41
00:05:10,220 --> 00:05:15,865
Grow 6 to 12 plants of each variety
to ensure good genetic variety. 

42
00:05:31,315 --> 00:05:36,490
Once the flower is in bloom,
it will take between 60 and 100 days,

43
00:05:36,780 --> 00:05:41,185
depending on the variety,
for the fruit to be ready for consumption. 

44
00:05:55,005 --> 00:05:57,390
The plants you select for seed production

45
00:05:57,520 --> 00:05:59,940
should be healthy and vigorous ones

46
00:06:00,075 --> 00:06:03,215
that you have been able
to observe throughout their growth 

47
00:06:03,645 --> 00:06:06,760
and that correspond
to the desired selection criteria: 

48
00:06:09,035 --> 00:06:14,685
For the plants, look for regular
and strong growth, numerous flowers,

49
00:06:15,155 --> 00:06:19,190
a good fructification
and branches that do not break. 

50
00:06:19,590 --> 00:06:22,860
For the fruits, look for the best tasting ones,

51
00:06:23,095 --> 00:06:29,820
the variety's typical shape, size,
colour and thickness of the flesh and the skin. 

52
00:06:31,655 --> 00:06:35,430
You should avoid extracting seeds
from already harvested peppers,

53
00:06:35,960 --> 00:06:39,580
as this does not enable you to check
all of the characteristics

54
00:06:39,650 --> 00:06:41,915
linked to the growth of the variety. 

55
00:06:46,300 --> 00:06:49,775
To harvest the seeds,
wait until full maturity:

56
00:06:52,575 --> 00:06:57,660
the green fruits will have become red,
brown, orange or yellow.

57
00:06:59,785 --> 00:07:04,900
The pale yellow fruits will have
turned dark yellow, orange or red. 

58
00:07:06,395 --> 00:07:10,025
At this stage the seeds are yellow and are mature. 

59
00:07:12,760 --> 00:07:15,275
Don't pick the seeds of immature fruits

60
00:07:15,405 --> 00:07:18,650
as their germination capacity
will be much lower. 

61
00:07:22,875 --> 00:07:27,380
It is best to harvest the seeds
from the first ripe fruits of a plant.

62
00:07:29,880 --> 00:07:34,215
Those taken from later fruit
tend to have a lower germination rate.

63
00:07:37,065 --> 00:07:39,955
Never harvest seeds from sick peppers. 

64
00:07:49,570 --> 00:07:52,460
Extracting - sorting - storing

65
00:08:02,205 --> 00:08:03,090
Watch out!

66
00:08:03,590 --> 00:08:09,180
It is important to extract spicy chilli seeds
in a well ventilated space

67
00:08:09,935 --> 00:08:14,105
and if possible outside to avoid
emanations of capsaicin.

68
00:08:15,525 --> 00:08:19,840
These can induce eye,
throat and nose irritations.

69
00:08:24,750 --> 00:08:29,440
It is also important to use thick rubber gloves
and even safety goggles. 

70
00:08:34,945 --> 00:08:38,830
Cut the peppers in two and remove
the seeds using a knife.

71
00:08:41,440 --> 00:08:47,305
Place them in a bowl full of water,
the empty seeds will float.

72
00:08:53,360 --> 00:08:54,815
Remove them with a sieve. 

73
00:09:06,210 --> 00:09:10,320
Finish cleaning the good seeds
in the sieve under running water. 

74
00:09:14,095 --> 00:09:17,860
It is then important
to dry the seeds within two days.

75
00:09:26,195 --> 00:09:27,255
To do that,

76
00:09:27,480 --> 00:09:31,725
place them on a fine-meshed sieve
or a plate in a dry,

77
00:09:32,010 --> 00:09:37,555
airy and warm place (between 23° and 30°C). 

78
00:09:43,190 --> 00:09:48,735
Another method for small quantities
is dry them on coffee filters,

79
00:09:49,310 --> 00:09:53,090
as they are very absorbing and the seeds
do not stick to them.

80
00:09:54,120 --> 00:09:58,575
Place at most a small teaspoon
of seeds on each filter

81
00:09:59,020 --> 00:10:00,840
and write the name of the variety

82
00:10:01,160 --> 00:10:04,935
and the species
on the filter using a marker pen.

83
00:10:06,675 --> 00:10:13,805
Hang the sachets on a clothes' rack in a dry,
airy, shaded and warm place. 

84
00:10:15,600 --> 00:10:18,230
Avoid exposing the seeds to the sun,

85
00:10:18,915 --> 00:10:22,130
and don’t dry them on paper
to which they could stick. 

86
00:10:30,945 --> 00:10:33,525
Write the name of the variety and species,

87
00:10:33,780 --> 00:10:38,420
as well as the year, on a label
and place it inside the sachet.

88
00:10:39,390 --> 00:10:42,010
Writing on the outside often rubs off. 

89
00:10:46,670 --> 00:10:50,740
A few days in the freezer
will kill any parasite larvae. 

90
00:10:53,855 --> 00:10:59,415
The germination capacity of chilli
and pepper seeds is 3 to 6 years.

91
00:11:04,380 --> 00:11:07,785
To lengthen it, keep the seeds in the freezer. 

