1
00:00:13,380 --> 00:00:16,340
How to choose the seeds for seed production ?

2
00:00:23,640 --> 00:00:26,580
It is important to know
the origin of the varieties

3
00:00:26,640 --> 00:00:29,050
that you are going to cultivate in your garden.

4
00:00:35,540 --> 00:00:39,410
When choosing the varieties from
which you plan to produce seeds,

5
00:00:39,760 --> 00:00:44,070
you should avoid using ones that
have been developed by the big seed industry.

6
00:00:44,870 --> 00:00:50,550
So do not use F1 or F2 hybrid seeds, GMOs

7
00:00:50,840 --> 00:00:53,840
or all seeds developed through biotechnology. 

8
00:01:00,070 --> 00:01:07,540
Plants grown from F1 and F2 hybrid seeds
will not reproduce themselves identically.

9
00:01:08,430 --> 00:01:14,435
Their seeds can also be sterile or produce
plants with unpredictable characteristics.

10
00:01:23,935 --> 00:01:26,295
By technically blocking their varieties,

11
00:01:26,825 --> 00:01:31,550
the seed multinationals can prevent gardeners
from reproducing their own seeds

12
00:01:31,930 --> 00:01:34,975
and thereby keep their monopoly on the market. 

13
00:01:38,995 --> 00:01:43,545
These technical mechanisms
reinforce the legal regulations

14
00:01:43,745 --> 00:01:46,630
that prohibit the free reproduction of seeds.

15
00:01:49,390 --> 00:01:54,245
All of these varieties are covered
by intellectual property rights,

16
00:01:54,945 --> 00:01:58,230
such as plant property rights and patents. 

17
00:02:03,300 --> 00:02:05,910
To begin producing your own seeds,

18
00:02:06,485 --> 00:02:10,425
the seeds should be from varieties
that are open-pollinated

19
00:02:10,780 --> 00:02:17,065
and if possible from organic, biodynamic
or agro-ecological agriculture. 

20
00:02:38,575 --> 00:02:40,640
Nature is very generous

21
00:02:40,855 --> 00:02:46,165
and will in general provide you with far more
seeds than you will need for your own use.

22
00:02:57,520 --> 00:03:01,480
The surplus can be given to others or exchanged. 

