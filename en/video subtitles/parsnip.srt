1
00:00:10,920 --> 00:00:16,378
Parsnips belong to the Apiaceae family
and the Pastinaca sativa species.

2
00:00:17,672 --> 00:00:20,356
It is a biennial plant grown for its root.

3
00:00:25,534 --> 00:00:27,898
Parsnips are mainly white in colour

4
00:00:28,174 --> 00:00:33,352
but there are different varieties adapted
to various soil and climate conditions.

5
00:00:43,020 --> 00:00:44,020
Pollination 

6
00:00:58,523 --> 00:01:03,534
The flower head of the parsnip
is an umbel composed of small flowers

7
00:01:03,621 --> 00:01:05,380
that are usually hermaphrodite.

8
00:01:07,280 --> 00:01:13,563
The stamen, the male sexual organ,
matures before the pistil,

9
00:01:13,840 --> 00:01:15,294
the female sexual organ. 

10
00:01:17,803 --> 00:01:22,443
Self-fertilization does not therefore
occur within the same flower.

11
00:01:23,469 --> 00:01:26,901
Yet since the flowers do not bloom
at the same time,

12
00:01:27,345 --> 00:01:30,756
self-fertilization is possible
within the same umbel

13
00:01:32,320 --> 00:01:35,003
or between two umbels on the same plant. 

14
00:01:37,389 --> 00:01:41,454
Fertilization also occurs between
the umbels of different plants. 

15
00:01:43,636 --> 00:01:49,221
The parsnip is an allogamous plant
primarily pollinated by insects,

16
00:01:49,723 --> 00:01:55,498
in particular insects from the Diptera
and Lepidoptera species

17
00:01:56,080 --> 00:01:58,058
as well as by ladybugs,

18
00:01:58,443 --> 00:02:02,509
which are attracted to umbels
that are often covered with aphids.

19
00:02:05,418 --> 00:02:08,385
Parsnips can cross with wild parsnips. 

20
00:02:11,098 --> 00:02:13,230
To avoid cross-pollination,

21
00:02:13,825 --> 00:02:18,130
two varieties of parsnip
should be grown 300 meters apart. 

22
00:02:22,180 --> 00:02:26,740
This distance can be reduced to 100 meters
if a natural barrier

23
00:02:26,829 --> 00:02:29,425
such as a hedge exists between them. 

24
00:02:31,960 --> 00:02:37,127
To isolate varieties you can either
cover two varieties with different nets,

25
00:02:37,832 --> 00:02:40,552
open one while the other
is closed on one day

26
00:02:40,872 --> 00:02:42,312
and alternate the next,

27
00:02:44,036 --> 00:02:48,480
or you can permanently cover
one variety with an insect net

28
00:02:48,930 --> 00:02:51,003
and put a bumble bee hive inside

29
00:02:52,843 --> 00:02:54,225
for more information,

30
00:02:54,436 --> 00:02:58,436
see the module on isolation techniques
in "The ABC of seed production.” 

31
00:03:04,020 --> 00:03:05,170
Life cycle 

32
00:03:16,007 --> 00:03:21,054
Parsnips grown for seed production are grown
in the same way as those for consumption,

33
00:03:22,443 --> 00:03:26,603
but they will only produce their seeds
in the second year of the cycle.

34
00:03:48,080 --> 00:03:55,083
It may take a long time for seeds to germinate
at the start of the season – around three weeks. 

35
00:04:20,552 --> 00:04:21,883
Once they have developed,

36
00:04:22,014 --> 00:04:28,014
different methods exist for the winter storing
of parsnips intended for seed production. 

37
00:04:31,730 --> 00:04:34,312
It is possible to dig up the roots in autumn.

38
00:04:40,880 --> 00:04:45,570
They should be selected for characteristics
specific to the variety:

39
00:04:45,949 --> 00:04:48,378
colour, shape, vigour.

40
00:04:49,112 --> 00:04:54,661
The roots are cleaned without water
and the leaves are cut above the collar.

41
00:04:55,149 --> 00:04:58,923
They are then left to dry a short
while in the open air.

42
00:05:02,349 --> 00:05:04,429
You should then store them in the cellar.

43
00:05:18,276 --> 00:05:21,301
During the winter
you should regularly check them

44
00:05:21,541 --> 00:05:24,174
and remove those that have begun to rot. 

45
00:05:27,883 --> 00:05:31,120
The easiest is to leave them
in the ground in the garden.

46
00:05:32,589 --> 00:05:35,083
The parsnip is one of the hardiest plants

47
00:05:35,316 --> 00:05:39,374
and can stay in the ground
in winter even in very cold regions.

48
00:05:40,152 --> 00:05:43,294
Its flavour is even improved by frost. 

49
00:05:44,720 --> 00:05:48,330
After the worst of the cold has passed,
you should uproot them

50
00:05:48,480 --> 00:05:51,060
and select those intended for seed production.

51
00:05:55,280 --> 00:05:57,709
Replant the roots at the beginning of spring.

52
00:05:58,640 --> 00:06:01,850
Bury them so that the collar
is either at ground level

53
00:06:02,014 --> 00:06:07,687
or slightly below with a distance
of 60 to 90 cm between each plant. 

54
00:06:13,381 --> 00:06:16,392
It would seem that planting parsnips
close to each other

55
00:06:16,560 --> 00:06:21,781
reduces the number of tertiary umbels,
whose seeds are of poor quality. 

56
00:06:26,043 --> 00:06:28,923
A minimum of fifteen to twenty plants
is necessary

57
00:06:29,025 --> 00:06:30,981
to maintain good genetic diversity. 

58
00:07:05,330 --> 00:07:09,098
Parsnips grown for seed are magnificent plants

59
00:07:09,410 --> 00:07:12,203
that can reach a height
of two meters in good years.

60
00:07:16,116 --> 00:07:18,780
It is sometimes necessary to stake them. 

61
00:07:23,876 --> 00:07:29,003
The parsnip produces several umbels
that bloom at different times.

62
00:07:31,927 --> 00:07:37,170
The first, the primary umbel,
is found at the tip of the main stem. 

63
00:07:39,520 --> 00:07:44,494
The secondary umbels are those
that develop from the main stem. 

64
00:07:46,690 --> 00:07:50,829
The tertiary umbels form on the secondary stems.

65
00:07:53,127 --> 00:07:58,705
It is preferable to harvest the primary umbels
because they produce the best seeds.

66
00:07:59,556 --> 00:08:03,900
The secondary umbels should be
harvested only if necessary. 

67
00:08:30,218 --> 00:08:33,170
The umbels are cut along with the top of the stalk

68
00:08:33,352 --> 00:08:35,985
when the first mature seeds begin to fall.

69
00:08:37,360 --> 00:08:41,752
You can also cut them earlier as they
tend to fall very easily to the ground.

70
00:08:51,781 --> 00:08:56,821
In any case, they should continue
to dry in a well ventilated and dry place. 

71
00:09:05,272 --> 00:09:08,203
Extracting - sorting - storing

72
00:09:16,400 --> 00:09:19,287
The seeds are removed from the umbels by hand.

73
00:09:20,101 --> 00:09:24,654
You should wear gloves because
the seeds release essential oils

74
00:09:24,760 --> 00:09:27,469
that may cause blisters in direct sunlight. 

75
00:09:31,854 --> 00:09:37,832
To clean the seeds, you should first
use coarse mesh sieves that retain the chaff.

76
00:09:39,898 --> 00:09:44,930
You then use a finer sieve that will retain
the seeds but let the dust pass through. 

77
00:09:57,000 --> 00:10:00,530
Always put a label with the name
of the variety and species,

78
00:10:00,640 --> 00:10:05,345
as well as the year, inside the package,
as writing on the outside may rub off. 

79
00:10:10,400 --> 00:10:14,683
Storing the seeds in the freezer
for several days kills parasite larvae. 

80
00:10:18,060 --> 00:10:21,796
Parsnip seeds only have a germination
capacity of one year,

81
00:10:22,552 --> 00:10:25,512
but this can be prolonged
by storing them in the freezer. 

82
00:10:26,698 --> 00:10:31,418
One gram of seed contains
around 220 individual seeds. 

