1
00:00:08,756 --> 00:00:14,283
Fennel belongs to the Apiaceae family
and the Foeniculum vulgare species.

2
00:00:16,501 --> 00:00:22,196
It can be annual, biennial
and under certain conditions perennial.

3
00:00:24,230 --> 00:00:25,920
There are two different types :

4
00:00:28,007 --> 00:00:31,820
fennel with a bulb called Florence fennel

5
00:00:34,340 --> 00:00:36,880
and herb fennel without a bulb,

6
00:00:37,360 --> 00:00:43,032
cultivated for its leaves and seeds,
used for medicinal or aromatic reasons. 

7
00:00:44,603 --> 00:00:47,410
There are both early and late varieties.

8
00:00:50,807 --> 00:00:57,178
Fennel is a photoperiodical plant,
which means that long days encourage flowering. 

9
00:01:07,818 --> 00:01:08,778
Pollination 

10
00:01:28,538 --> 00:01:33,636
The inflorescence of fennel is an umbel
composed of small yellow flowers

11
00:01:33,934 --> 00:01:35,890
that are usually hermaphrodite.

12
00:01:38,349 --> 00:01:44,465
The stamen, the male sexual organ,
matures before the pistil, the female organ. 

13
00:01:50,138 --> 00:01:54,712
Self-fertilization does not therefore
occur within the same flower.

14
00:01:56,029 --> 00:01:59,636
Yet since the flowers do not bloom
at the same time,

15
00:02:00,174 --> 00:02:03,940
self-fertilization is possible
within the same umbel

16
00:02:04,247 --> 00:02:07,018
or between two umbels on the same plant. 

17
00:02:11,032 --> 00:02:15,760
Fertilization also occurs between
the umbels of different plants. 

18
00:02:19,440 --> 00:02:25,018
Fennel is therefore an allogamous plant
that is mainly pollinated by insects.

19
00:02:32,320 --> 00:02:36,240
There is a risk of cross-pollination
between different varieties. 

20
00:02:39,156 --> 00:02:41,512
Fennel can also cross with wild fennel,

21
00:02:41,883 --> 00:02:44,778
which is very common
in certain regions of the world. 

22
00:02:50,269 --> 00:02:52,043
To avoid cross-pollination,

23
00:02:52,261 --> 00:02:55,890
two varieties of fennel should be grown
one kilometer apart. 

24
00:03:00,581 --> 00:03:06,821
This distance can be reduced to 500 meters
if there is a natural barrier such as a hedge. 

25
00:03:14,610 --> 00:03:20,420
The varieties can also be isolated by
alternately opening and closing mosquito nets

26
00:03:26,152 --> 00:03:30,660
or by placing small hives
with insects inside a closed mosquito net

27
00:03:31,389 --> 00:03:32,523
for this technique,

28
00:03:32,680 --> 00:03:38,189
see the module on isolation techniques
in “The ABC of seed production”. 

29
00:03:44,276 --> 00:03:45,272
Life cycle

30
00:03:55,200 --> 00:03:57,629
Herb fennel does not form a bulb.

31
00:03:59,054 --> 00:04:01,396
It is perennial in most regions.

32
00:04:08,727 --> 00:04:13,825
It is sown in spring, it flowers
and produces seeds in the first year.

33
00:04:14,210 --> 00:04:18,501
It resists well to frosts and can stay
in the ground over winter.

34
00:04:19,352 --> 00:04:23,687
The amount of seeds produced will be greater
in the second year of cultivation. 

35
00:04:29,120 --> 00:04:32,341
Bulb fennel can be grown in different ways. 

36
00:04:34,727 --> 00:04:39,258
In a mild climate spring varieties
are grown as annual plants.

37
00:04:39,563 --> 00:04:42,370
They are sown early in spring, in March,

38
00:04:42,712 --> 00:04:46,458
which gives them time to form
a bulb before they flower.

39
00:04:53,047 --> 00:04:58,996
In most cases, bulb fennel grown
for seed is cultivated as a biennial. 

40
00:04:59,556 --> 00:05:05,876
Summer varieties are sown after 20 June
when the days start getting shorter.

41
00:05:06,000 --> 00:05:08,683
This will prevent them from flowering too early.

42
00:05:09,461 --> 00:05:12,749
They will have the time to form
their bulbs before winter.

43
00:05:14,072 --> 00:05:17,316
They will flower and produce seeds
in the second year. 

44
00:05:43,578 --> 00:05:46,974
Only plants with the typical
characteristics of the variety

45
00:05:47,127 --> 00:05:50,138
that have formed bulbs are chosen for seed.

46
00:05:55,258 --> 00:05:57,781
Plants that flower too early are removed. 

47
00:06:04,487 --> 00:06:06,850
Select the bulbs for seed production

48
00:06:06,952 --> 00:06:10,101
according to the specific
characteristics of the variety:

49
00:06:11,127 --> 00:06:13,905
colour, shape, vigour. 

50
00:06:28,654 --> 00:06:32,472
In a cold climate,
the bulbs may freeze in the ground.

51
00:06:33,090 --> 00:06:36,380
This is why you should remove the leaves
above the last bud

52
00:06:36,698 --> 00:06:40,887
and put the bulbs in a jar or pot
with their roots and soil,

53
00:06:41,170 --> 00:06:44,298
protected from the frost and too much humidity. 

54
00:06:53,614 --> 00:06:56,312
You should check the bulbs
throughout the winter

55
00:06:56,981 --> 00:07:00,080
and eliminate any that have started to rot.

56
00:07:05,620 --> 00:07:11,236
They will be replanted in spring
when the risk of a hard frost has passed. 

57
00:07:30,900 --> 00:07:36,334
Fifteen to twenty plants are required
to ensure good genetic diversity. 

58
00:07:40,094 --> 00:07:44,596
In mild regions,
bulb fennel can be left in the ground over winter.

59
00:07:45,854 --> 00:07:49,694
It will continue to grow in the spring
of its second year and then flower. 

60
00:08:27,890 --> 00:08:32,225
Fennel plants have very high floral stalks
that tend to fall over.

61
00:08:33,054 --> 00:08:34,283
They should be staked. 

62
00:08:35,440 --> 00:08:41,367
The umbels are cut with a piece of the stalk
when the first mature seeds start to fall.

63
00:08:45,927 --> 00:08:47,750
They can be cut earlier;

64
00:08:48,443 --> 00:08:53,120
the seeds will continue to mature slowly
during the drying process.

65
00:08:56,218 --> 00:09:01,745
In any case, they should continue to be dried
in a dry and well ventilated place. 

66
00:09:10,080 --> 00:09:12,756
Extracting - sorting - storing

67
00:09:19,614 --> 00:09:22,967
Rub the umbels between your hands
to remove the seeds. 

68
00:09:34,167 --> 00:09:39,323
To sort the seeds use first of all coarse
mesh sieves that retain the chaff.

69
00:09:45,578 --> 00:09:50,400
Then the seeds are retained by a finer sieve
that allows dust to pass through. 

70
00:10:06,600 --> 00:10:09,620
Finally winnow the seeds by blowing on them.

71
00:10:09,890 --> 00:10:12,225
This will get rid of any remaining chaff. 

72
00:10:15,505 --> 00:10:19,450
Always include a label with the name
of the variety and species

73
00:10:19,556 --> 00:10:24,378
as well as the year in the bag
as writing on the bag can be rubbed off.

74
00:10:27,687 --> 00:10:32,138
Storing the seeds in the freezer
for several days kills certain parasite larvae. 

75
00:10:35,400 --> 00:10:39,345
Fennel seeds have a germination
capacity of four years.

76
00:10:39,520 --> 00:10:42,778
Sometimes this can be extended to seven years.

77
00:10:43,345 --> 00:10:46,916
This will be further prolonged
by storing the seeds in the freezer.

78
00:10:47,578 --> 00:10:50,560
One gram contains around 300 seeds. 

