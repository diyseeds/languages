﻿1
00:00:08,000 --> 00:00:09,675
Botanical classification

2
00:00:22,555 --> 00:00:27,580
Anyone who wants to produce seeds
should have a general understanding of botany,

3
00:00:27,940 --> 00:00:29,420
the science of plants.

4
00:00:37,860 --> 00:00:42,860
Plants are at present classified
according to the anatomy of their flowers,

5
00:00:43,220 --> 00:00:46,140
their reproductive organs and their fruit.

6
00:00:47,460 --> 00:00:50,220
They are classified by their Latin names. 

7
00:00:57,325 --> 00:01:00,780
Using Latin names
ensures precise classification,

8
00:01:01,180 --> 00:01:04,420
whereas using everyday language
can lead to confusion.

9
00:01:09,285 --> 00:01:13,260
For example, the term “squash”
covers very different species,

10
00:01:13,460 --> 00:01:19,740
such as cucurbita pepo,
cucurbita moschata and cucurbita maxima. 

11
00:01:24,635 --> 00:01:28,700
In this film we will only use certain latin names :

12
00:01:30,100 --> 00:01:32,780
families, such as Fabaceae,

13
00:01:34,680 --> 00:01:35,940
Asteraceae,

14
00:01:38,200 --> 00:01:39,280
Solanaceae

15
00:01:42,195 --> 00:01:43,540
or Cucurbitaceae;

16
00:01:45,765 --> 00:01:48,820
then the genuses such as Cucurbita,

17
00:01:49,180 --> 00:01:54,540
species such as Cucurbita Maxima
and finally the varieties. 

18
00:01:59,665 --> 00:02:04,580
For example, the cucumber is a member
of the Cucurbitaceae family,

19
00:02:05,060 --> 00:02:09,500
the Cucumis genus,
and the Cucumis Sativus species.

20
00:02:10,820 --> 00:02:13,300
It is divided into many varieties. 

21
00:02:20,735 --> 00:02:23,900
The melon also belongs to
the Cucurbitaceae family

22
00:02:24,340 --> 00:02:26,295
and the same Cucumis genus

23
00:02:26,850 --> 00:02:30,300
but it is from a different species, Cucumis Melo,

24
00:02:30,740 --> 00:02:34,100
which is also subdivided into many varieties. 

25
00:02:39,240 --> 00:02:44,180
Plants of the same variety will be
very similar in their characteristics

26
00:02:44,260 --> 00:02:47,620
such as shape, colour and size.

27
00:02:49,730 --> 00:02:54,220
All varieties within the same species
can cross with each other. 

28
00:02:56,245 --> 00:03:00,820
Most of the time,
varieties from different species cannot cross.

29
00:03:01,840 --> 00:03:05,975
For example, there is no risk
of a cucumber crossing with a melon. 

30
00:03:21,440 --> 00:03:23,255
But there are exceptions.

31
00:03:24,475 --> 00:03:28,570
Crosses are possible between species
that are botanically close,

32
00:03:29,255 --> 00:03:32,700
such as between the Cucurbita moschata species

33
00:03:32,820 --> 00:03:36,180
and the Cucurbita argyrosperma species. 

34
00:03:39,220 --> 00:03:43,100
Crosses between plants from different genuses
are impossible. 

35
00:03:52,435 --> 00:03:53,620
Acquiring this knowledge

36
00:03:53,695 --> 00:03:58,260
will help you to use the cultivation methods
best adapted for seed production

37
00:03:58,540 --> 00:04:00,980
and avoid unwanted cross-pollination. 

