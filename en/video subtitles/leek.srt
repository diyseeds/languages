1
00:00:10,756 --> 00:00:15,780
Leeks belong to the Amarylidaceae family,
which used to be called Alliaceae.

2
00:00:16,320 --> 00:00:19,614
They are part
of the Allium ampeloprasum species.

3
00:00:20,970 --> 00:00:27,250
It is a biennial plant grown for the long,
white stalks it produces in its first year.

4
00:00:31,160 --> 00:00:33,170
There are several varieties of leeks.

5
00:00:35,818 --> 00:00:39,454
Their leaves may be green, blue or even purple.

6
00:00:44,210 --> 00:00:48,480
Some varieties may be grown early
in the season, others late.

7
00:00:53,250 --> 00:00:56,458
Certain varieties will resist
very cold temperatures.

8
00:00:57,389 --> 00:00:59,861
Others will struggle to survive the winter. 

9
00:01:12,440 --> 00:01:13,680
Pollination 

10
00:01:24,669 --> 00:01:29,621
The flower heads of leeks are made up
of small hermaphrodite flowers

11
00:01:31,752 --> 00:01:33,970
that are individually sterile.

12
00:01:39,047 --> 00:01:41,781
They need insects to fertilise them.

13
00:01:47,330 --> 00:01:50,065
This means that leeks are allogamous. 

14
00:01:54,203 --> 00:01:59,105
Insects may cause cross pollination
between different varieties of leek,

15
00:01:59,389 --> 00:02:00,792
or with wild leeks.

16
00:02:06,894 --> 00:02:13,854
On the other hand, they do not cross
with onions, chives or black garlic. 

17
00:02:18,843 --> 00:02:23,730
To avoid crossing between varieties
don’t plant two varieties of leek

18
00:02:23,840 --> 00:02:26,087
less than 1km apart. 

19
00:02:29,374 --> 00:02:35,396
You can reduce this distance to 400m
if there is a natural barrier, such as a hedge. 

20
00:02:40,145 --> 00:02:44,669
It is also possible to use mosquito
nets to isolate each variety.

21
00:02:44,909 --> 00:02:49,840
You can cover one variety with a permanent net
and put a bumble bee hive inside.

22
00:02:51,352 --> 00:02:54,283
Or you can cover two varieties
with different nets:

23
00:02:54,620 --> 00:03:00,356
open one while the other is closed
on one day, and alternate the next day. 

24
00:03:01,640 --> 00:03:04,130
These methods are described in greater detail

25
00:03:04,280 --> 00:03:07,280
in the module
on ‘mechanical isolation techniques’

26
00:03:07,643 --> 00:03:09,963
in the ABC of Seed production. 

27
00:03:15,156 --> 00:03:16,247
Life cycle 

28
00:03:28,807 --> 00:03:33,934
Grow leeks for seeds in the same way
as those for food in the first year.

29
00:03:38,945 --> 00:03:41,621
They will produce seeds in the second year. 

30
00:04:33,578 --> 00:04:37,025
Get rid of leeks that produce
a flower in their first year.

31
00:04:40,130 --> 00:04:44,341
Plants that result from those seeds
would also flower too early. 

32
00:04:48,120 --> 00:04:51,585
There are several methods for storing
leeks grown for seeds. 

33
00:04:59,178 --> 00:05:04,298
In regions where the winter is cold,
uproot them before the frosts start.

34
00:05:14,240 --> 00:05:17,381
It is important to choose
the seed-bearing plants

35
00:05:17,505 --> 00:05:20,887
according to the specific
characteristics of the variety.

36
00:05:21,310 --> 00:05:26,480
These are precocity,
the size and thickness of the stalk,

37
00:05:27,090 --> 00:05:32,087
the colour of the leaves and their resistance
to frost and to illnesses.

38
00:05:33,883 --> 00:05:38,407
Select 30 plants as some of them
may be lost during the winter.

39
00:05:44,632 --> 00:05:49,607
Then place them in a container
in a cool area that is protected from the cold.

40
00:05:51,040 --> 00:05:55,730
You need 20 plants for seeds to guarantee
good genetic diversity.

41
00:06:00,000 --> 00:06:05,505
Certain so-called summer leeks
will survive winter better if you do this. 

42
00:06:08,967 --> 00:06:12,130
The simplest method,
if the weather is warm enough,

43
00:06:12,414 --> 00:06:14,821
is to leave them in the ground in the garden. 

44
00:06:20,727 --> 00:06:26,040
At the beginning of spring replant
the leeks that you have stored over winter.

45
00:06:54,080 --> 00:06:57,890
Cutting the leaves will help ensure
that the plants develop well. 

46
00:07:36,960 --> 00:07:40,980
The flowering stalks will grow
to up to 1m50, if not more.

47
00:07:56,894 --> 00:07:59,163
They need to be supported with stakes. 

48
00:08:18,741 --> 00:08:22,298
Over a period of four weeks
each individual flower

49
00:08:22,421 --> 00:08:24,923
within the leek flower head will blossom.

50
00:08:27,970 --> 00:08:33,570
The period lasting from the beginning
of blossoming to full seed maturity is long. 

51
00:08:37,960 --> 00:08:43,221
The seeds are ready when the seed pods
have dried and reveal black seeds. 

52
00:08:49,970 --> 00:08:54,967
To collect the seeds cut the flower
head together with the top of the stem,

53
00:08:55,512 --> 00:09:01,607
place them in a cloth bag and leave them
to dry in a well-ventilated and warm place. 

54
00:09:03,716 --> 00:09:09,170
In cold and wet regions where wind or rain
can cause the loss of mature seeds,

55
00:09:10,036 --> 00:09:15,418
harvest the plants before they are fully mature
and leave them to mature in a dry place. 

56
00:09:28,436 --> 00:09:31,660
Extracting - sorting - storing 

57
00:09:36,727 --> 00:09:39,607
The extraction, sorting
and storing of leek seeds

58
00:09:39,767 --> 00:09:41,941
are practically the same as for onions. 

59
00:09:55,534 --> 00:09:58,310
First of all, rub the flower heads
between your hands

60
00:09:58,443 --> 00:10:00,589
and then crush them using a rolling pin. 

61
00:10:05,781 --> 00:10:11,396
To remove the seeds that remain stuck,
leave the seed pods in a freezer for a few hours.

62
00:10:11,789 --> 00:10:13,272
They should come out easier. 

63
00:10:20,920 --> 00:10:26,225
To sort the seeds, use a fine sieve
to retain the seeds but not the dust. 

64
00:10:42,550 --> 00:10:46,298
Finish by winnowing the seeds
to remove the light debris.

65
00:10:47,556 --> 00:10:52,843
You can place them in the wind,
blow on top of them or use a small ventilator. 

66
00:10:57,018 --> 00:11:00,320
Then pour the seeds in cold water and stir.

67
00:11:05,381 --> 00:11:08,880
The fertilised seeds are heavier,
so they will sink.

68
00:11:11,905 --> 00:11:15,054
Remove the empty ones and the debris that float.

69
00:11:22,814 --> 00:11:24,676
Dry the good seeds on a plate.

70
00:11:27,570 --> 00:11:30,574
Once they are dry, they will run like sand. 

71
00:11:38,530 --> 00:11:41,752
Always put a label with the name
of the variety and species

72
00:11:41,912 --> 00:11:45,200
as well as the year of harvesting in the package,

73
00:11:45,607 --> 00:11:47,800
as writing on the outside may rub off. 

74
00:11:51,120 --> 00:11:55,527
Leave the seeds in the freezer
for a few days to kill any parasite larvae. 

75
00:12:02,523 --> 00:12:06,487
Leek seeds have a germination
capacity of two years.

76
00:12:07,250 --> 00:12:10,240
This can sometimes stretch to six years.

77
00:12:13,180 --> 00:12:16,647
As these seeds quickly
lose their germination capacity,

78
00:12:17,140 --> 00:12:20,465
to lengthen this period keep
the seeds in the freezer. 

