1
00:00:13,460 --> 00:00:16,836
The turnip is a member
of the Brassicaceae family,

2
00:00:17,396 --> 00:00:21,221
the Brassica rapa species
and the rapa sub-species.

3
00:00:24,167 --> 00:00:28,152
Brassica rapa includes
a great variety of subspecies:

4
00:00:32,200 --> 00:00:37,780
bok choy, komatsuna or Japanese mustard spinach,

5
00:00:38,880 --> 00:00:41,150
broccoli raab, and many more. 

6
00:00:47,112 --> 00:00:53,660
All turnip varieties are characterized
by rapid growth and a wide variety of shapes.

7
00:00:54,520 --> 00:00:56,560
They also have different colours.

8
00:01:03,070 --> 00:01:04,781
Depending on the variety,

9
00:01:05,083 --> 00:01:10,000
the roots grow at the surface of the soil
or are slightly or even half buried.

10
00:01:12,567 --> 00:01:17,280
There are early varieties
and storage varieties. 

11
00:01:22,094 --> 00:01:23,214
Pollination 

12
00:01:30,509 --> 00:01:37,003
Most varieties of the Brassica rapa species
have bright yellow hermaphrodite flowers

13
00:01:37,454 --> 00:01:39,680
that are self-incompatible:

14
00:01:40,312 --> 00:01:43,236
the pollen of each plant is viable

15
00:01:43,469 --> 00:01:47,323
but can only fertilize
the flowers of another plant. 

16
00:01:53,585 --> 00:01:58,487
It is therefore an allogamous plant
pollinated by insects. 

17
00:02:05,229 --> 00:02:07,796
This also means that each turnip variety

18
00:02:07,900 --> 00:02:11,476
should be isolated
from all varieties of Chinese cabbage,

19
00:02:11,960 --> 00:02:16,778
bok choy, Japanese mustard spinach,
broccoli raab

20
00:02:17,003 --> 00:02:20,072
and all other subspecies of Brassica rapa. 

21
00:02:23,520 --> 00:02:26,110
To ensure the purity of the variety,

22
00:02:26,392 --> 00:02:31,069
two different varieties
should be planted at least 1 km apart. 

23
00:02:33,840 --> 00:02:37,054
This distance can be reduced to 500 meters

24
00:02:37,396 --> 00:02:41,614
if there is a natural barrier
such as a hedge between the two varieties. 

25
00:02:47,770 --> 00:02:50,181
The varieties can also be isolated

26
00:02:50,414 --> 00:02:53,956
by alternately opening
and closing mosquito nets

27
00:02:55,752 --> 00:03:00,014
or by placing small hives
with insects inside a closed mosquito net

28
00:03:04,680 --> 00:03:05,701
for this technique,

29
00:03:05,883 --> 00:03:10,174
see the module on isolation techniques
in “The ABC of seed propagation”. 

30
00:03:17,607 --> 00:03:18,940
Life cycle

31
00:03:35,316 --> 00:03:39,985
There are some very early spring varieties
that can be treated like annuals.

32
00:03:46,560 --> 00:03:49,861
Most turnip varieties are,
however, biennial.

33
00:03:50,520 --> 00:03:51,803
In a temperate climate,

34
00:03:51,910 --> 00:03:55,127
they are sown directly in the ground in mid-July

35
00:03:55,250 --> 00:03:58,894
and they will produce their seeds
in the second year of cultivation. 

36
00:04:10,283 --> 00:04:13,032
Turnip plants intended for seed production

37
00:04:13,163 --> 00:04:16,180
are grown in the same way
as turnips for consumption. 

38
00:04:48,850 --> 00:04:53,540
For the production of turnip seeds
you should select healthy plants

39
00:04:53,912 --> 00:04:56,749
that you have observed throughout
the growth period

40
00:04:56,860 --> 00:05:05,061
to identify the characteristics of the variety:
vigour, rapid growth, resistance to disease. 

41
00:05:13,214 --> 00:05:15,540
In autumn, when harvesting the crop,

42
00:05:15,876 --> 00:05:20,756
you should select about thirty healthy plants
with good shape, size and colour.

43
00:05:29,534 --> 00:05:31,980
You should not select very large roots

44
00:05:32,567 --> 00:05:35,716
because they tend to rot
more easily during winter. 

45
00:05:43,098 --> 00:05:47,083
Neither the roots nor the base
of the leaves at the collar should be cut. 

46
00:05:53,230 --> 00:05:55,463
In regions with severe winters,

47
00:05:55,563 --> 00:06:01,789
turnips are stored in sand boxes in a dry cellar,
preferably one with an earthen floor.

48
00:06:04,196 --> 00:06:08,830
The temperature in the cellar should
be between 0 and several degrees Celsius. 

49
00:06:17,825 --> 00:06:18,900
During the winter

50
00:06:19,316 --> 00:06:23,700
you should regularly check the roots
and remove any that are beginning to rot. 

51
00:06:32,280 --> 00:06:36,196
Certain firm-fleshed varieties
are resistant to the cold

52
00:06:36,400 --> 00:06:38,530
and can stay the winter in the ground.

53
00:06:41,745 --> 00:06:45,090
In regions with milder climates
and less severe winters,

54
00:06:45,505 --> 00:06:48,232
all turnip varieties can remain in the ground.

55
00:06:49,265 --> 00:06:53,963
They can be protected
from sporadic light frosts

56
00:06:54,269 --> 00:06:56,523
with a frost blanket or a 10 cm layer of earth. 

57
00:06:59,636 --> 00:07:03,127
The roots are selected in spring
at the time of the harvest.

58
00:07:11,956 --> 00:07:13,700
Cut the leaves above the collar

59
00:07:22,940 --> 00:07:26,232
and then replant them, burying 2/3 of the root.

60
00:07:27,660 --> 00:07:29,570
They should be copiously watered. 

61
00:07:37,367 --> 00:07:40,501
With turnips that have spent
the winter stored inside

62
00:07:40,920 --> 00:07:45,636
you should remove damaged roots
and replant only the healthy ones.

63
00:07:57,120 --> 00:07:59,229
Again they should be watered well. 

64
00:08:12,410 --> 00:08:19,430
They will later grow to be at least one meter tall
and should be supported with stakes. 

65
00:08:46,981 --> 00:08:50,940
Extraction, sorting and storage of turnip seeds

66
00:08:51,083 --> 00:08:55,054
are the same as for the cabbages
of the Brassica oleraceae species. 

67
00:09:01,876 --> 00:09:06,123
The seeds are mature once
the seed pods take on a beige colour.

68
00:09:07,810 --> 00:09:09,360
They are very dehiscent,

69
00:09:09,563 --> 00:09:14,494
that is to say that when mature
they open easily and disperse their seeds.

70
00:09:24,596 --> 00:09:28,305
Usually all of the stems are not mature
at the same time.

71
00:09:29,880 --> 00:09:35,200
To avoid losing seeds,
you can harvest them one by one as they ripen.

72
00:09:37,620 --> 00:09:42,810
You can also harvest the whole plant
before full maturity of all the seeds. 

73
00:09:46,421 --> 00:09:48,000
To complete the process,

74
00:09:48,240 --> 00:09:52,007
you should ensure that they dry fully
in a dry and well-ventilated place

75
00:09:52,087 --> 00:09:53,556
protected from sunlight. 

76
00:10:05,985 --> 00:10:08,661
Turnip seed pods are ready for extraction

77
00:10:08,916 --> 00:10:12,000
when you can easily break them
open between your fingers. 

78
00:10:15,127 --> 00:10:16,458
To extract the seeds,

79
00:10:16,567 --> 00:10:21,970
the seed pods are spread across
a plastic sheet or thick piece of fabric

80
00:10:22,378 --> 00:10:25,112
and then beaten or rubbed together by hand.

81
00:10:28,240 --> 00:10:33,258
You can also put them in a bag
and beat them against a soft surface. 

82
00:10:35,105 --> 00:10:42,152
Larger quantities can be threshed
by walking or driving on them. 

83
00:10:52,360 --> 00:10:57,723
Seed pods that do not open easily
probably contain immature seeds

84
00:10:57,898 --> 00:10:59,738
that will not germinate well. 

85
00:11:04,341 --> 00:11:10,334
During sorting, the chaff is removed by first
passing the seeds through a coarse sieve

86
00:11:10,843 --> 00:11:12,320
that retains the chaff

87
00:11:17,243 --> 00:11:19,970
and then by passing them through another sieve

88
00:11:20,276 --> 00:11:24,820
that retains the seeds
but allows smaller particles to fall through. 

89
00:11:28,520 --> 00:11:32,465
Finally, you should winnow them
by blowing on them

90
00:11:32,792 --> 00:11:36,938
or with the help of the wind
so that any remaining chaff is removed. 

91
00:11:55,469 --> 00:11:59,469
It is important to put a label in the packet
with the name of the species,

92
00:11:59,890 --> 00:12:05,498
the variety and the year of cultivation,
as writing on the outside may rub off. 

93
00:12:09,800 --> 00:12:14,938
Storing the seeds in the freezer
for several days eliminates any parasites. 

94
00:12:18,770 --> 00:12:21,810
Turnip seed can be kept for at least six years.

95
00:12:28,196 --> 00:12:31,098
This can be prolonged by storing
the seeds in a freezer. 

