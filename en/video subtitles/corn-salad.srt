﻿1
00:00:11,220 --> 00:00:15,098
Corn salad belongs
to the Caprifoliaceae family.

2
00:00:17,200 --> 00:00:20,065
It is a winter and early spring plant.

3
00:00:21,636 --> 00:00:23,890
There are two cultivated species :

4
00:00:25,120 --> 00:00:27,300
Valerianella locusta

5
00:00:32,916 --> 00:00:35,700
and Valerianella eriocarpa

6
00:00:36,340 --> 00:00:41,389
which is more delicate and is mostly grown
in Italy and the south of France. 

7
00:00:46,596 --> 00:00:50,007
Corn salad also grows wild in Europe. 

8
00:01:02,700 --> 00:01:03,789
Pollination 

9
00:01:14,487 --> 00:01:21,869
Corn salad is an autogamous plant : the flowers
are hermaphrodite and self-fertilising,

10
00:01:22,436 --> 00:01:26,356
meaning they have the male and female organs
within the same flower

11
00:01:26,574 --> 00:01:27,847
and they are compatible.

12
00:01:29,578 --> 00:01:36,501
There is, however, a risk of cross-pollination
between different varieties by insects. 

13
00:01:43,607 --> 00:01:48,516
The Locusta and Eriocarpa species cannot cross,

14
00:01:50,727 --> 00:01:57,110
but you should be careful with wild corn salad
which can cross with the cultivated varieties. 

15
00:02:02,225 --> 00:02:04,734
To ensure the purity of the variety,

16
00:02:05,010 --> 00:02:11,760
it is better to leave 50m between different
varieties of the same species of corn salad.

17
00:02:12,770 --> 00:02:19,738
This distance can be reduced to 30 m
if there is a natural barrier such as a hedge. 

18
00:02:22,960 --> 00:02:24,380
Life cycle

19
00:02:35,941 --> 00:02:41,214
Grow corn salad for its seeds in the same way
as you grow it for consumption.

20
00:02:42,014 --> 00:02:44,349
You should sow out early in the autumn.

21
00:02:46,523 --> 00:02:51,527
It will then overwinter in the garden,
flowering and producing seeds in the spring. 

22
00:03:01,338 --> 00:03:05,832
You need to grow 50 plants
to ensure good genetic diversity. 

23
00:03:08,989 --> 00:03:13,694
You should not pick any leaves from the plants
you want to keep for their seeds. 

24
00:03:17,585 --> 00:03:21,220
Selection criteria include
resistance to the cold,

25
00:03:22,356 --> 00:03:25,260
the size, shape and colour of the leaves,

26
00:03:28,203 --> 00:03:33,590
resistance to fungal diseases, late flowering. 

27
00:03:39,469 --> 00:03:44,276
Make sure you get rid of badly formed plants
that are not true to type. 

28
00:04:35,141 --> 00:04:41,670
The seeds mature slowly over a long period
and fall easily once they are ready. 

29
00:04:52,740 --> 00:04:55,927
You should therefore closely
observe their maturity

30
00:04:56,480 --> 00:04:59,883
and not wait until the plants are completely dry.

31
00:05:00,712 --> 00:05:06,720
Ideally, wait until half of the seeds are mature
before starting to harvest them. 

32
00:05:07,578 --> 00:05:11,912
In order to avoid losing too many seeds
when you harvest them,

33
00:05:12,378 --> 00:05:17,040
you should first of all spread
a sheet on the ground around the plant. 

34
00:05:17,985 --> 00:05:23,869
Let them dry in a dry
and well-ventilated place for 2 to 3 weeks.

35
00:05:27,060 --> 00:05:29,069
To avoid the plants heating up,

36
00:05:29,323 --> 00:05:32,552
you should not dry them in piles
that are too thick.

37
00:05:37,876 --> 00:05:41,636
You should keep a close eye on them
during the drying process. 

38
00:05:51,447 --> 00:05:54,060
Extracting – sorting - storing

39
00:05:58,196 --> 00:06:02,349
On a sunny day extract the seeds
by rubbing the dry plants.

40
00:06:04,174 --> 00:06:09,330
Don't try to take the very last small seeds,
as they are probably not mature. 

41
00:06:11,760 --> 00:06:15,970
Sort them by sifting them through
different sized sieves

42
00:06:16,378 --> 00:06:20,509
to get rid of the coarser chaff
and then the finer waste. 

43
00:06:43,178 --> 00:06:47,280
You should then winnow them,
for example by using the wind. 

44
00:07:05,880 --> 00:07:10,007
At this stage the seeds do not have
their final colour.

45
00:07:10,356 --> 00:07:12,443
They will darken during storing. 

46
00:07:14,487 --> 00:07:16,770
They have a dormancy period of 2 months,

47
00:07:16,923 --> 00:07:21,549
but the germination rate is best
after one or even two years.

48
00:07:22,720 --> 00:07:26,356
You should therefore use seeds
harvested in previous years. 

49
00:07:27,861 --> 00:07:32,101
Always put a label with the name
and variety of the seeds

50
00:07:32,334 --> 00:07:38,021
as well as the year inside the package,
as writing on the outside may rub off. 

51
00:07:42,720 --> 00:07:47,265
Leave the seeds in the freezer
for a few days to kill off any parasites. 

52
00:07:50,720 --> 00:07:55,120
The seeds of corn salad have an average
germination capacity of 5 years.

53
00:08:00,240 --> 00:08:03,447
This will be even longer
if you store them in the freezer. 

