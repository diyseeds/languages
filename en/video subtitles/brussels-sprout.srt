﻿1
00:00:12,995 --> 00:00:16,680
The Brussels sprout is a member
of the Brassicaceae family,

2
00:00:17,340 --> 00:00:22,210
the Brassica oleracea species
and the gemmifera subspecies.

3
00:00:26,030 --> 00:00:30,215
The brassica oleracea species
also includes kohlrabi,

4
00:00:30,675 --> 00:00:37,130
broccoli, cabbage, kale, cauliflower
and the Savoy Cabbage. 

5
00:00:42,530 --> 00:00:45,832
Brussels sprouts are an autumn
and winter vegetable

6
00:00:45,950 --> 00:00:48,770
in regions with a cold, temperate climate.

7
00:00:51,030 --> 00:00:56,825
They form small sprouts at the base of the leaves
and can endure very low temperatures. 

8
00:01:05,760 --> 00:01:10,315
Pollination of all of the cabbages
of the Oleracea species: 

9
00:01:26,400 --> 00:01:30,285
The flowers of the Brassica oleracea
species are hermaphrodite

10
00:01:33,250 --> 00:01:37,460
which means that they have both male
and female organs.

11
00:01:41,155 --> 00:01:43,650
Most of them are self-sterile:

12
00:01:44,265 --> 00:01:49,250
the pollen from the flowers of one plant
can only fertilize another plant.

13
00:01:53,140 --> 00:01:55,315
The plants are therefore allogamous.

14
00:01:56,285 --> 00:02:01,020
In order to ensure good pollination
it is better to grow several plants. 

15
00:02:04,665 --> 00:02:07,615
Insects are the vectors of pollination.

16
00:02:11,095 --> 00:02:15,450
These characteristics ensure
great natural genetic diversity.

17
00:02:22,510 --> 00:02:28,405
All of the cabbage sub-species of the Brassica
oleracea species can cross with each other.

18
00:02:30,330 --> 00:02:34,920
You should therefore not grow different kinds
of cabbage for seeds close to each other. 

19
00:02:39,565 --> 00:02:40,990
To ensure purity,

20
00:02:41,330 --> 00:02:48,695
different varieties of the Brassica oleracea
species should be planted at least 1 km apart. 

21
00:02:50,160 --> 00:02:53,025
This distance can be reduced to 500 meters

22
00:02:53,215 --> 00:02:57,485
if there is a natural barrier
such as a hedge between the two varieties. 

23
00:03:00,745 --> 00:03:05,960
The varieties can also be isolated
by placing small hives with insects

24
00:03:06,165 --> 00:03:08,340
inside a closed mosquito net

25
00:03:13,530 --> 00:03:17,285
or by alternately
opening and closing mosquito nets.

26
00:03:21,680 --> 00:03:22,830
For this technique,

27
00:03:22,985 --> 00:03:27,270
see the module on isolation techniques
in “The ABC of seed production”. 

28
00:03:38,710 --> 00:03:40,940
Life cycle of the Brussels sprout

29
00:03:56,615 --> 00:03:59,415
The Brussels sprout is a biennial plant.

30
00:04:00,210 --> 00:04:04,260
It will produce its edible sprouts
in autumn and winter.

31
00:04:04,820 --> 00:04:08,100
It will form its flower
stalks in the following spring.

32
00:04:14,360 --> 00:04:18,730
Plants for seed are grown in the same way
as those for consumption.

33
00:04:19,840 --> 00:04:22,095
They are sown in May or June. 

34
00:04:45,350 --> 00:04:51,425
You should select 15 plants for seed production
to ensure good genetic diversity. 

35
00:04:55,495 --> 00:04:58,065
Seeds are saved from healthy plants

36
00:04:58,190 --> 00:05:01,200
that have been observed
throughout the period of growth.

37
00:05:04,320 --> 00:05:08,595
This enables you to check
all of the characteristics of the variety,

38
00:05:09,075 --> 00:05:13,440
such as the formation of regular sprouts
along the entire stem,

39
00:05:15,035 --> 00:05:18,840
the compactness, colour and shape of the sprouts,

40
00:05:19,770 --> 00:05:22,390
their taste (no bitterness),

41
00:05:23,010 --> 00:05:27,010
resistance to the cold, yield,
and the size of the plant. 

42
00:05:28,495 --> 00:05:34,070
Plants can reach a height of 60 to 80 cm
in the first year. 

43
00:05:36,135 --> 00:05:40,235
In autumn, the sprouts along the stalk
can be harvested,

44
00:05:40,690 --> 00:05:43,915
but the sprouts at the top must never be removed. 

45
00:05:46,690 --> 00:05:51,355
The Brussels sprout is more resistant
to cold than large cabbages,

46
00:05:53,410 --> 00:05:57,415
and winter varieties can remain
in the ground throughout the winter.

47
00:05:59,980 --> 00:06:04,080
If necessary, they can be protected
with a frost blanket. 

48
00:06:13,985 --> 00:06:19,020
In the second year, the stalks can reach
a height of one and a half meters.

49
00:06:19,800 --> 00:06:21,135
To avoid them falling,

50
00:06:21,585 --> 00:06:25,670
it is sometimes necessary to support
the flower stalks with stakes. 

51
00:06:27,690 --> 00:06:31,785
The top of the stem can be cut
to accelerate the flowering process. 

52
00:07:17,840 --> 00:07:24,640
Harvesting, extracting,
sorting and storing of all Brassica oleracea 

53
00:07:33,675 --> 00:07:37,525
The seeds are mature
when the seed pods turn beige.

54
00:07:41,670 --> 00:07:43,980
The seed pods are very dehiscent,

55
00:07:44,385 --> 00:07:49,310
which means that they open very easily
when mature and disperse their seed. 

56
00:07:57,965 --> 00:08:02,000
Most of the time, the stalks do not
all mature at the same time.

57
00:08:03,025 --> 00:08:09,000
To avoid wasting any seed,
harvesting can take place as each stalk matures.

58
00:08:10,835 --> 00:08:16,460
The entire plant can also be harvested
before all of the seeds have completely matured. 

59
00:08:19,940 --> 00:08:25,045
The ripening process is then completed
by drying them in a dry,

60
00:08:25,150 --> 00:08:26,970
well-ventilated place. 

61
00:08:39,465 --> 00:08:45,445
Cabbage seeds are ready to be removed
when the seed pods can be easily opened by hand. 

62
00:08:47,570 --> 00:08:48,890
To extract the seeds,

63
00:08:49,040 --> 00:08:54,375
the seed pods are spread across
a plastic sheet or thick piece of fabric

64
00:08:54,785 --> 00:08:57,710
and then beaten or rubbed together by hand.

65
00:09:01,125 --> 00:09:05,840
You can also put them in a bag
and beat them against a soft surface. 

66
00:09:07,600 --> 00:09:12,275
Larger quantities can be threshed
by walking or driving on them. 

67
00:09:24,800 --> 00:09:30,090
Seed pods that do not open easily
probably contain immature seeds

68
00:09:30,310 --> 00:09:32,060
that will not germinate well. 

69
00:09:36,765 --> 00:09:37,995
During sorting,

70
00:09:38,310 --> 00:09:41,395
the chaff is removed by first passing the seeds

71
00:09:41,580 --> 00:09:44,605
through a coarse sieve that retains the chaff

72
00:09:49,685 --> 00:09:52,265
and then by passing them through another sieve

73
00:09:52,715 --> 00:09:57,380
that retains the seeds
but allows smaller particles to fall through. 

74
00:10:01,020 --> 00:10:04,930
Finally, you should winnow them
by blowing on them

75
00:10:05,210 --> 00:10:09,245
or with the help of the wind
so that any remaining chaff is removed. 

76
00:10:24,425 --> 00:10:29,380
All seeds from the Brassica oleracea
species resemble one another.

77
00:10:30,390 --> 00:10:37,015
It is very difficult to distinguish between,
for example, cabbage and cauliflower seeds.

78
00:10:37,605 --> 00:10:40,500
This is why it is important to label the plants

79
00:10:40,775 --> 00:10:44,550
and then the extracted seeds
with the name of the species,

80
00:10:44,835 --> 00:10:47,855
the variety and the year of cultivation. 

81
00:10:49,275 --> 00:10:54,035
Storing the seeds in the freezer
for several days eliminates any parasites. 

82
00:10:59,480 --> 00:11:02,505
Cabbage seeds are able to germinate up to 5 years.

83
00:11:03,695 --> 00:11:07,395
However, they may retain
this capacity up to 10 years.

84
00:11:09,145 --> 00:11:12,405
This can be prolonged by storing
them in the freezer.

85
00:11:13,450 --> 00:11:19,625
One gram contains 250 to 300 seeds
depending on the variety. 

