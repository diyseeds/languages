﻿1
00:00:11,510 --> 00:00:12,780
Pollination

2
00:00:28,890 --> 00:00:34,090
Pollination is the way in which
a large number of plants reproduce.

3
00:00:35,815 --> 00:00:39,420
The word “reproduce” is in fact not really exact

4
00:00:39,890 --> 00:00:41,860
because by using this process

5
00:00:42,205 --> 00:00:45,895
the plant will not produce
an identical copy of itself.

6
00:00:48,465 --> 00:00:52,340
The male and female genetic elements will mix

7
00:00:52,590 --> 00:00:55,330
and potentially create a different plant.

8
00:00:58,095 --> 00:01:00,900
Pollination involves the transfer of pollen

9
00:01:01,355 --> 00:01:06,900
from the plant's male organ
to the female organ in order to fructify it. 

10
00:01:38,570 --> 00:01:42,240
The stamen is the male part of the flower.

11
00:01:43,335 --> 00:01:47,190
It has anthers that contain pollen grains. 

12
00:01:49,035 --> 00:01:51,375
The female organ is called the pistil.

13
00:01:53,295 --> 00:01:58,180
It includes the ovary
which contains one or several ovules. 

14
00:02:01,285 --> 00:02:06,220
When they are fertilised by pollen
the ovules develop into seeds

15
00:02:06,605 --> 00:02:09,990
and the ovary becomes, in this case, a pepper.

16
00:02:16,690 --> 00:02:18,420
In the case of radishes,

17
00:02:18,815 --> 00:02:22,870
the ovaries turn into seed pods
containing several seeds. 

18
00:02:30,200 --> 00:02:33,860
The vegetable plants intended
for the production of seeds

19
00:02:34,615 --> 00:02:36,510
have different kinds of flowers.

20
00:02:37,155 --> 00:02:39,300
Many of them are hermaphrodite,

21
00:02:39,960 --> 00:02:45,045
which means that they have both the male
and female organs in the same flower. 

22
00:02:50,385 --> 00:02:52,300
There are hermaphrodite flowers

23
00:02:52,620 --> 00:02:56,045
in which the pollen is transferred
within the same flower.

24
00:02:56,580 --> 00:03:02,040
The male organs release their pollen
once the female organ becomes receptive.

25
00:03:03,760 --> 00:03:09,715
The flower is said to be self-pollinating
and the plant is autogamous. 

26
00:03:12,400 --> 00:03:14,380
This is the case with beans,

27
00:03:15,740 --> 00:03:16,820
lettuces

28
00:03:18,170 --> 00:03:19,695
and tomatoes are example. 

29
00:03:24,090 --> 00:03:26,260
There are also hermaphrodite flowers

30
00:03:26,460 --> 00:03:31,370
whose male organs can only pollinate
the female organs of another flower.

31
00:03:37,340 --> 00:03:42,020
Sometimes the male organ can only
pollinate the female organ

32
00:03:42,250 --> 00:03:44,670
of a flower on a different plant. 

33
00:03:46,155 --> 00:03:50,060
They must therefore rely
on an external pollinator

34
00:03:50,225 --> 00:03:53,300
that carries the pollen
from one flower to another,

35
00:03:53,930 --> 00:03:57,310
either on the same plant or on another plant.

36
00:03:59,340 --> 00:04:04,390
We call this cross-pollination
and the plants are said to be allogamous. 

37
00:04:09,010 --> 00:04:11,995
There are also plants with unisexual flowers,

38
00:04:12,200 --> 00:04:16,520
which means that they only have one organ,
either male or female. 

39
00:04:20,200 --> 00:04:24,700
The transfer of pollen must therefore
happen between different flowers,

40
00:04:25,150 --> 00:04:27,585
from a male flower to a female one.

41
00:04:29,640 --> 00:04:33,850
The pollination is also crossed,
and the plants allogamous. 

42
00:04:54,515 --> 00:04:58,800
The male and female flowers
can be present on the same plant.

43
00:04:59,520 --> 00:05:02,955
This is the case with cucumbers
and corn, for example. 

44
00:05:11,440 --> 00:05:16,255
But sometimes the male and female
flowers can be on different plants.

45
00:05:17,675 --> 00:05:19,260
An example is spinach

46
00:05:19,700 --> 00:05:25,225
whose male flowers are on one plant
and the female ones on another plant.

47
00:05:29,720 --> 00:05:34,935
Spinach is anemophilous,
which means that it is wind-pollinated. 

48
00:05:38,280 --> 00:05:42,010
There are therefore two ways
in which pollination takes place.

49
00:05:42,810 --> 00:05:47,020
With autogamous plants it can occur
within the same flower

50
00:05:49,195 --> 00:05:53,685
and in the case of allogamous plants
it requires external vectors.

51
00:05:58,470 --> 00:06:03,105
Some species can, however,
combine the two forms of pollination. 

52
00:06:07,185 --> 00:06:10,380
For example,
in certain climatic conditions

53
00:06:10,465 --> 00:06:14,420
the tomato, which is generally
considered to be autogamous,

54
00:06:14,840 --> 00:06:16,425
can become allogamous.

55
00:06:18,945 --> 00:06:20,340
In very hot weather,

56
00:06:20,630 --> 00:06:25,675
the female organs come out of the calyx
and receive pollen from other flowers. 

57
00:06:46,960 --> 00:06:50,940
Depending on the quantity
and diversity of the insects present,

58
00:06:51,530 --> 00:06:56,580
the lettuce that is usually autogamous
becomes allogamous

59
00:06:56,970 --> 00:06:59,460
when insects come and visit their flowers

60
00:06:59,730 --> 00:07:02,110
and transport the pollen further afield. 

61
00:07:13,545 --> 00:07:19,180
Environmental conditions and the presence
of other flowers in or near the garden

62
00:07:19,635 --> 00:07:22,900
will have an influence
on the activity of insects

63
00:07:23,020 --> 00:07:26,940
and therefore on the pollination of the flowers
of the plants you are growing

64
00:07:27,180 --> 00:07:28,500
for seed production. 

