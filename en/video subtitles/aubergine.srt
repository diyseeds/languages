1
00:00:07,720 --> 00:00:11,940
The aubergine or eggplant
belongs to the Solanaceae family

2
00:00:12,290 --> 00:00:15,060
and to the Solanum melongena species.

3
00:00:20,265 --> 00:00:25,540
It is a perennial plant in tropical countries
but annual in temperate regions.

4
00:00:37,350 --> 00:00:42,100
There is a great diversity in the size,
shape and colour of aubergines.

5
00:00:55,840 --> 00:00:56,740
Pollination 

6
00:01:15,800 --> 00:01:20,060
The aubergine flower is hermaphrodite
and self-fertilising,

7
00:01:21,180 --> 00:01:25,700
which means that male and female organs
are on the same flower,

8
00:01:26,255 --> 00:01:27,855
and that they are compatible.

9
00:01:32,980 --> 00:01:34,930
They are said to be autogamous.

10
00:01:39,505 --> 00:01:42,995
Crossing between varieties can, however, occur.

11
00:01:44,065 --> 00:01:47,460
Its frequency varies depending
on the environment

12
00:01:47,705 --> 00:01:50,380
and the number of pollinating insects. 

13
00:01:53,580 --> 00:01:57,220
For plants grown in places
well sheltered from the wind,

14
00:01:57,515 --> 00:02:02,740
you can regularly shake them during
flowering to encourage pollination. 

15
00:02:06,990 --> 00:02:08,940
To avoid cross-pollination,

16
00:02:09,350 --> 00:02:14,185
grow different varieties
of aubergines 100 meters apart.

17
00:02:16,690 --> 00:02:22,580
This distance can be reduced to 50m
if there's a natural barrier, such as a hedge.

18
00:02:24,840 --> 00:02:30,020
In tropical climates,
distances of up to one kilometer are necessary. 

19
00:02:32,935 --> 00:02:36,885
Mechanical isolation
can also be used with a mosquito net.

20
00:02:38,550 --> 00:02:42,790
See the module on isolation techniques
in the ABC of seed production. 

21
00:02:55,100 --> 00:02:56,540
Life cycle 

22
00:03:11,045 --> 00:03:16,620
Aubergines grown for seeds are cultivated
in the same way as those for food.

23
00:03:27,875 --> 00:03:30,060
To ensure good genetic diversity,

24
00:03:30,235 --> 00:03:34,020
you should grow between
6 and 12 plants of each variety. 

25
00:03:37,390 --> 00:03:42,005
Aubergines are plants that require
a lot of heat to develop well.

26
00:03:44,810 --> 00:03:49,340
You should therefore adapt sowing time
to the date you can plant outside. 

27
00:04:18,605 --> 00:04:23,815
Once the flower blooms,
it will take between 60 and 100 days,

28
00:04:23,950 --> 00:04:28,420
depending on the variety,
for the fruit to be ready for consumption.

29
00:04:29,980 --> 00:04:33,895
But watch out, the seeds
are not ready at this stage. 

30
00:04:38,465 --> 00:04:44,380
Plants used for seed production are chosen
amongst those that are healthy and vigorous

31
00:04:44,720 --> 00:04:48,100
and that you have been able to observe
throughout their growth. 

32
00:04:51,625 --> 00:04:56,460
With regard to the plants,
you should look for regular and vigorous growth,

33
00:04:57,025 --> 00:04:58,500
numerous flowers,

34
00:04:58,960 --> 00:05:04,940
well-developed fruit or good capacity
of adaptation to cold climates. 

35
00:05:05,710 --> 00:05:09,555
As for the fruit, you should
choose those with the best flavour,

36
00:05:09,940 --> 00:05:13,260
the typical shape of the variety, the size,

37
00:05:14,010 --> 00:05:16,260
the colour of the flesh and the skin,

38
00:05:16,830 --> 00:05:21,990
the absence of bitterness in the fruit,
and the thickness of the skin. 

39
00:05:25,195 --> 00:05:29,380
You should not select fruits for seeds
from already picked aubergines

40
00:05:29,750 --> 00:05:34,780
as this does not enable you to observe
all of the growth characteristics of the plant. 

41
00:05:40,025 --> 00:05:43,120
Avoid choosing sick plants for seed production. 

42
00:05:48,120 --> 00:05:53,660
At complete maturity,
the fruits become soft and they change colour.

43
00:05:55,510 --> 00:06:00,055
White aubergines become yellow,
purple ones become brown.

44
00:06:02,125 --> 00:06:05,900
If the fruit have not had the time
to fully mature on the plant

45
00:06:06,220 --> 00:06:11,345
they can be left to mature
in wooden boxes in a warm and dry place. 

46
00:06:16,285 --> 00:06:19,180
Extracting – sorting - storing

47
00:06:24,170 --> 00:06:25,685
To extract the seeds,

48
00:06:25,940 --> 00:06:29,145
choose fully ripe but not fermented fruits. 

49
00:06:34,050 --> 00:06:36,560
There are two methods for extracting seeds :

50
00:06:40,660 --> 00:06:42,080
For a small harvest,

51
00:06:42,340 --> 00:06:44,250
cut the aubergines in four

52
00:06:46,875 --> 00:06:48,720
and remove the seeds with a knife.

53
00:06:56,080 --> 00:07:02,230
For larger crops, peel and chop the aubergines
in cubes and place them in a jar of water. 

54
00:07:08,905 --> 00:07:11,460
Mix in a blender for a few seconds.

55
00:07:16,310 --> 00:07:19,530
The good seeds will sink
to the bottom of the container.

56
00:07:27,185 --> 00:07:30,420
Take out the layer with the flesh,
the left-over skin

57
00:07:30,785 --> 00:07:33,620
and the undeveloped seeds using a sieve.

58
00:07:37,535 --> 00:07:41,695
Take the good seeds and clean them
in the sieve under running water.

59
00:07:43,725 --> 00:07:47,340
It is then important to dry the seeds
within two days at most.

60
00:07:50,215 --> 00:07:52,460
Place them on a fine-meshed sieve

61
00:07:52,805 --> 00:07:58,140
or a plate in a warm, dry,
airy or well-ventilated space

62
00:07:59,540 --> 00:08:02,380
between 23° and 30°C. 

63
00:08:04,580 --> 00:08:07,740
For smaller quantities, use coffee filters,

64
00:08:08,110 --> 00:08:12,020
as they are very absorbent
and the seeds do not stick to them.

65
00:08:15,525 --> 00:08:19,740
Place at most a small teaspoon
of seeds in each filter.

66
00:08:20,210 --> 00:08:25,645
Hang the sachets on a clothes rack in a dry,
airy and shaded place. 

67
00:08:35,455 --> 00:08:37,465
Write the name of the variety,

68
00:08:37,695 --> 00:08:45,790
the species and the year of production
on a label and place it inside the packet.

69
00:08:47,095 --> 00:08:49,930
Writing on the outside may rub off. 

70
00:08:57,285 --> 00:09:01,100
A few days in the freezer
will kill any parasite larvae.

71
00:09:04,900 --> 00:09:09,710
The germination capacity of aubergine
seeds is 3 to 6 years.

72
00:09:13,470 --> 00:09:16,275
To lengthen it, keep the seeds in a freezer. 

