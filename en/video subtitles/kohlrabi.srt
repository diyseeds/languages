1
00:00:12,320 --> 00:00:18,327
Kohlrabi is a member of the Brassicaceae family,
the Brassica oleracea species

2
00:00:20,400 --> 00:00:24,261
and the caulorapa gongylodes subspecies.

3
00:00:25,949 --> 00:00:31,396
The brassica oleracea species also
includes broccoli, cabbage,

4
00:00:32,334 --> 00:00:37,469
Brussels sprouts, kale,
Cauliflower and the Savoy Cabbage. 

5
00:00:42,749 --> 00:00:46,800
Kohlrabi is a biennial plant grown
for its swollen stem.

6
00:00:52,550 --> 00:00:57,440
The plants produce very few leaves
and its flesh remains tender.

7
00:01:08,167 --> 00:01:12,900
There are different varieties distinguished
by the size and colour of the stem.

8
00:01:16,880 --> 00:01:20,938
There are early varieties
and varieties for storage. 

9
00:01:28,494 --> 00:01:33,260
Pollination of all of the cabbages
of the Oleracea species: 

10
00:01:49,185 --> 00:01:53,127
The flowers of the Brassica oleracea
species are hermaphrodite

11
00:01:56,021 --> 00:02:00,218
which means that they have both male
and female organs.

12
00:02:03,934 --> 00:02:06,458
Most of them are self-sterile:

13
00:02:07,040 --> 00:02:12,196
the pollen from the flowers of one plant
can only fertilize another plant.

14
00:02:15,898 --> 00:02:18,167
The plants are therefore allogamous.

15
00:02:19,047 --> 00:02:23,956
In order to ensure good pollination
it is better to grow several plants. 

16
00:02:27,432 --> 00:02:30,574
Insects are the vectors of pollination.

17
00:02:33,876 --> 00:02:38,378
These characteristics ensure great
natural genetic diversity.

18
00:02:45,265 --> 00:02:49,621
All of the cabbage sub-species
of the Brassica oleracea species

19
00:02:49,774 --> 00:02:51,330
can cross with each other.

20
00:02:53,200 --> 00:02:57,789
You should therefore not grow different kinds
of cabbage for seeds close to each other. 

21
00:03:02,334 --> 00:03:08,087
To ensure purity, different varieties
of the Brassica oleracea species

22
00:03:08,378 --> 00:03:11,607
should be planted at least 1 km apart. 

23
00:03:12,920 --> 00:03:17,258
This distance can be reduced to 500 meters
if there is a natural barrier

24
00:03:17,330 --> 00:03:20,334
such as a hedge between
the two varieties. 

25
00:03:23,520 --> 00:03:25,992
The varieties can also be isolated

26
00:03:26,312 --> 00:03:31,200
by placing small hives
with insects inside a closed mosquito net

27
00:03:36,283 --> 00:03:39,949
or by alternately opening and closing
mosquito nets.

28
00:03:44,360 --> 00:03:45,541
For this technique,

29
00:03:45,716 --> 00:03:49,770
see the module on isolation techniques
in “The ABC of seed production”. 

30
00:04:03,374 --> 00:04:05,374
Life cycle of the kohlrabi 

31
00:04:19,083 --> 00:04:21,149
In the first year of cultivation,

32
00:04:21,483 --> 00:04:24,225
plants of storage varieties grown for seed

33
00:04:24,596 --> 00:04:28,087
are cultivated in the same way
as those for consumption. 

34
00:04:29,381 --> 00:04:30,680
In the first year,

35
00:04:30,909 --> 00:04:37,061
they form swollen stems that have to overwinter
and then flower the following year.

36
00:04:37,520 --> 00:04:43,876
Stems that are over-developed or split at the end
of autumn will be difficult to overwinter.

37
00:04:45,163 --> 00:04:48,660
In most regions, you should
therefore sow both early

38
00:04:48,850 --> 00:04:52,116
and storage varieties in June or July. 

39
00:05:50,276 --> 00:05:56,130
To ensure good genetic diversity,
you should select 30 kohlrabi plants in autumn

40
00:05:56,421 --> 00:06:00,160
in order to obtain 10
to 15 plants in spring.

41
00:06:02,021 --> 00:06:05,869
Producing kohlrabi seed
requires healthy plants

42
00:06:06,101 --> 00:06:09,069
that have been observed
throughout the growth period

43
00:06:09,330 --> 00:06:12,894
to identify the characteristics of the variety:

44
00:06:14,647 --> 00:06:18,945
the quality of the stem,
vigour and rapid growth,

45
00:06:19,600 --> 00:06:24,414
resistance to diseases,
good storage capacity,

46
00:06:24,960 --> 00:06:27,760
precocity, resistance to cold. 

47
00:06:39,080 --> 00:06:43,483
The plants intended for seed production
can be stored in a cellar;

48
00:06:44,276 --> 00:06:48,021
you should cut off the side leaves
but leave the central ones. 

49
00:06:54,509 --> 00:06:58,312
The plants are then placed in sand
or in containers.

50
00:07:01,207 --> 00:07:06,029
During winter kohlrabi is less
likely to rot than cabbages. 

51
00:07:12,167 --> 00:07:14,190
In regions with a mild climate,

52
00:07:14,545 --> 00:07:21,592
kohlrabi can remain in the ground since
it can tolerate frosts as low as -7° C.

53
00:07:23,127 --> 00:07:27,454
A dry winter is the best and stems
with less water content

54
00:07:27,934 --> 00:07:30,690
are more resistant to the rigours of winter. 

55
00:07:40,487 --> 00:07:44,436
In spring when the danger of a heavy frost is past,

56
00:07:45,156 --> 00:07:48,180
take out the kohlrabi from
their place of storage

57
00:07:48,400 --> 00:07:52,858
and replant them in the ground,
burying 2/3 of their volume. 

58
00:08:02,560 --> 00:08:06,458
The kohlrabi will grow flower stalks
and then flower. 

59
00:08:10,110 --> 00:08:11,694
To avoid them falling,

60
00:08:11,810 --> 00:08:15,483
it is sometimes necessary to support
the flower stalks with stakes. 

61
00:08:47,301 --> 00:08:54,232
Harvesting, extracting, sorting
and storing of all Brassica oleracea 

62
00:09:03,127 --> 00:09:06,974
The seeds are mature
when the seed pods turn beige.

63
00:09:11,170 --> 00:09:13,470
The seed pods are very dehiscent,

64
00:09:13,869 --> 00:09:18,952
which means that they open very easily
when mature and disperse their seed. 

65
00:09:27,469 --> 00:09:31,476
Most of the time, the stalks
do not all mature at the same time.

66
00:09:32,440 --> 00:09:36,647
To avoid wasting any seed,
harvesting can take place

67
00:09:36,741 --> 00:09:38,690
as each stalk matures.

68
00:09:40,334 --> 00:09:46,007
The entire plant can also be harvested
before all of the seeds have completely matured. 

69
00:09:49,360 --> 00:09:54,509
The ripening process is then completed
by drying them in a dry,

70
00:09:54,640 --> 00:09:56,545
well-ventilated place. 

71
00:10:08,923 --> 00:10:15,040
Cabbage seeds are ready to be removed
when the seed pods can be easily opened by hand. 

72
00:10:16,960 --> 00:10:18,341
To extract the seeds,

73
00:10:18,480 --> 00:10:23,847
the seed pods are spread across
a plastic sheet or thick piece of fabric

74
00:10:24,276 --> 00:10:27,098
and then beaten or rubbed together by hand.

75
00:10:30,596 --> 00:10:35,665
You can also put them in a bag
and beat them against a soft surface. 

76
00:10:37,080 --> 00:10:41,774
Larger quantities can be threshed
by walking or driving on them. 

77
00:10:54,280 --> 00:10:59,672
Seed pods that do not open easily
probably contain immature seeds

78
00:10:59,818 --> 00:11:01,614
that will not germinate well. 

79
00:11:06,160 --> 00:11:08,820
During sorting, the chaff is removed

80
00:11:09,003 --> 00:11:12,392
by first passing the seeds
through a coarse sieve

81
00:11:12,749 --> 00:11:14,218
that retains the chaff

82
00:11:19,185 --> 00:11:21,861
and then by passing them through another sieve

83
00:11:22,203 --> 00:11:26,720
that retains the seeds but allows
smaller particles to fall through. 

84
00:11:30,509 --> 00:11:34,261
Finally, you should winnow them
by blowing on them

85
00:11:34,698 --> 00:11:38,734
or with the help of the wind
so that any remaining chaff is removed. 

86
00:11:53,920 --> 00:11:58,850
All seeds from the Brassica oleracea
species resemble one another.

87
00:11:59,854 --> 00:12:03,760
It is very difficult to distinguish
between, for example,

88
00:12:04,160 --> 00:12:06,720
cabbage and cauliflower seeds.

89
00:12:07,098 --> 00:12:10,000
This is why it is important to label the plants

90
00:12:10,254 --> 00:12:14,094
and then the extracted seeds
with the name of the species,

91
00:12:14,280 --> 00:12:17,447
the variety and the year of cultivation. 

92
00:12:18,756 --> 00:12:23,534
Storing the seeds in the freezer
for several days eliminates any parasites. 

93
00:12:28,960 --> 00:12:32,167
Cabbage seeds are able to germinate up to 5 years.

94
00:12:33,156 --> 00:12:36,814
However, they may retain
this capacity up to 10 years.

95
00:12:38,640 --> 00:12:41,934
This can be prolonged by storing
them in the freezer.

96
00:12:42,916 --> 00:12:49,294
One gram contains 250 to 300 seeds
depending on the variety. 

