1
00:00:08,950 --> 00:00:16,700
The tomato belongs to the Solanaceae family
and the Solanum lycopersicum species.

2
00:00:22,440 --> 00:00:28,220
It is an annual plant in temperate countries
and sometimes perennial in tropical climates.

3
00:00:33,340 --> 00:00:37,860
There is a great diversity of tomatoes
with thousands of varieties

4
00:00:38,340 --> 00:00:44,685
which differ in colour, shape,
size, taste, length of growth,

5
00:00:44,840 --> 00:00:49,715
precocity, adaptation to cold,
hot or humid conditions.

6
00:00:58,535 --> 00:01:01,620
There are also tomatoes
with an indeterminate growth,

7
00:01:02,205 --> 00:01:05,430
meaning that the plant will continue
to produce new flowers

8
00:01:05,580 --> 00:01:08,675
and the harvest will spread out
over a long period. 

9
00:01:20,620 --> 00:01:22,780
For tomatoes with a determinate growth,

10
00:01:23,095 --> 00:01:27,000
the plant’s flowering will be concentrated
over a short period.

11
00:01:27,875 --> 00:01:31,010
The harvesting period will therefore
be quite short. 

12
00:01:39,160 --> 00:01:40,140
Pollination

13
00:01:50,290 --> 00:01:52,580
Tomato flowers are hermaphrodite,

14
00:01:54,215 --> 00:01:58,345
which means that the male and female organs
are on the same flower

15
00:01:58,680 --> 00:02:03,580
and the pollen on the stamen can fertilise
the pistil of the same flower.

16
00:02:03,995 --> 00:02:07,615
The flower is therefore said
to be self-fertilising. 

17
00:02:08,950 --> 00:02:10,390
In temperate regions,

18
00:02:10,630 --> 00:02:15,415
the reproductive organs are for most of the time
well protected inside the flower.

19
00:02:17,035 --> 00:02:18,695
Cross-pollination is rare. 

20
00:02:26,710 --> 00:02:30,920
There is a lot more cross-pollination
in hot and tropical climates.

21
00:02:33,600 --> 00:02:38,170
It is the length of the pistil that indicates
the risk of cross-pollination :

22
00:02:41,415 --> 00:02:46,030
it is easier for insects to pollinate a pistil
that is longer than the stamen,

23
00:02:46,140 --> 00:02:50,770
than a short pistil hidden amongst
stamen that have grown together. 

24
00:02:55,505 --> 00:02:59,380
Large fleshy tomatoes often have
double blossoms.

25
00:02:59,675 --> 00:03:02,285
This increases the risk of cross-pollination.

26
00:03:06,140 --> 00:03:11,165
It is therefore indispensable to observe
the flower structure of each variety,

27
00:03:11,575 --> 00:03:14,785
as well as the level of insect activity
in your garden. 

28
00:03:16,160 --> 00:03:20,950
The risk of cross-pollination may be reduced
by the presence of other flowers in the garden

29
00:03:21,255 --> 00:03:24,370
whose nectar is preferred
by bees and bumblebees. 

30
00:03:32,900 --> 00:03:36,260
Shaking the tomato plants several times a day

31
00:03:36,505 --> 00:03:40,225
when there is no wind
will increase self-fertilisation. 

32
00:03:50,575 --> 00:03:52,100
In temperate regions

33
00:03:52,300 --> 00:03:56,445
avoid cross-pollination
between varieties with short pistils

34
00:03:56,615 --> 00:04:02,020
that remain inside the flower
by ensuring a distance of 3m between varieties. 

35
00:04:06,495 --> 00:04:12,780
For varieties with long pistils that stick out,
keep a distance of 9 to 12m. 

36
00:04:17,060 --> 00:04:23,200
In hot and tropical regions,
keep a distance of 1km between varieties. 

37
00:04:26,570 --> 00:04:33,385
This distance can be reduced to 200m if there is
a natural barrier such as a hedge between them. 

38
00:04:36,105 --> 00:04:39,960
It is also possible to isolate
varieties under a mosquito net.

39
00:04:41,455 --> 00:04:42,620
For this technique,

40
00:04:42,905 --> 00:04:46,895
consult the module àn isolation techniques
in the ABC of seed production. 

41
00:04:53,650 --> 00:04:54,820
Life cycle 

42
00:05:17,400 --> 00:05:20,540
The tomato is an annual plant
in temperate climates.

43
00:05:24,005 --> 00:05:27,380
In tropical regions,
it can live for several years.

44
00:05:36,630 --> 00:05:38,980
Tomato plants grown for their seeds

45
00:05:39,120 --> 00:05:42,540
will be cultivated in the same way
as those for consumption. 

46
00:06:07,415 --> 00:06:08,980
With early varieties

47
00:06:09,260 --> 00:06:13,180
you will have to wait at least 40 days
after the flower is in bloom

48
00:06:13,495 --> 00:06:15,395
before the fruit is fully ripe.

49
00:06:24,235 --> 00:06:30,580
This period can last as long as 60 or 80 days
for mid-season or late varieties. 

50
00:06:37,300 --> 00:06:38,620
Selection should be done

51
00:06:38,700 --> 00:06:42,010
with plants that you have observed
throughout their development

52
00:06:42,120 --> 00:06:44,780
and that correspond to the desired criteria: 

53
00:06:48,580 --> 00:06:52,250
With regard to the plant,
look for regular and strong growth,

54
00:06:53,010 --> 00:07:00,050
early or late fructification,
numerous flowers with good fructification.

55
00:07:04,015 --> 00:07:06,140
For varieties of determinate growth,

56
00:07:06,550 --> 00:07:10,070
choose compact plants
with a short harvesting period. 

57
00:07:14,080 --> 00:07:18,345
You should also taste the fruit
and see if it is sweet or acid. 

58
00:07:19,905 --> 00:07:24,405
With regard to the fruit,
look for the variety's typical characteristics:

59
00:07:25,155 --> 00:07:31,945
the size, the colour of the flesh,
the beauty of the skin,

60
00:07:32,785 --> 00:07:34,970
and the number of lobes in the fruit.

61
00:07:36,665 --> 00:07:39,545
Selecting seeds from already picked tomatoes

62
00:07:39,815 --> 00:07:43,630
will not enable you to check
all of the characteristics

63
00:07:43,720 --> 00:07:45,950
linked to the different varieties’ growth. 

64
00:07:49,670 --> 00:07:53,740
Harvesting tomatoes for seeds
should be done from healthy plants

65
00:07:53,925 --> 00:07:56,660
when the fruit are ripe
and ready for consumption.

66
00:07:57,415 --> 00:08:00,845
It is best to choose the first
or second group of flowers.

67
00:08:06,230 --> 00:08:09,595
It is also possible to pick tomatoes
later in the season

68
00:08:09,745 --> 00:08:12,310
if the plants have resisted well to diseases. 

69
00:08:16,880 --> 00:08:20,810
To ensure good genetic
diversity within a variety,

70
00:08:21,365 --> 00:08:25,150
harvest tomatoes from 6 to 12 different plants

71
00:08:25,695 --> 00:08:29,215
and do not harvest from sick plants
or damaged tomatoes. 

72
00:08:32,745 --> 00:08:36,220
If the fruits have not had time
to mature on the plant,

73
00:08:37,070 --> 00:08:40,395
this may happen in cold climates
or mountain regions,

74
00:08:41,340 --> 00:08:48,525
let the harvested fruit ripen in a warm place,
such as a greenhouse or on a window sill. 

75
00:08:56,525 --> 00:08:59,150
Extracting - sorting - storing 

76
00:09:04,430 --> 00:09:08,695
To extract the seeds,
choose ripe but not fermented fruit. 

77
00:09:13,590 --> 00:09:15,380
For small quantities of seeds,

78
00:09:15,520 --> 00:09:20,220
cut the fruit and put the seeds
and part of the flesh in a glass jar using a spoon

79
00:09:20,620 --> 00:09:22,260
or by squeezing the tomato. 

80
00:09:31,120 --> 00:09:36,140
For larger quantities of seeds,
or for small cherry or wild tomato varieties,

81
00:09:36,690 --> 00:09:39,340
dice the fruit and mix everything in a blender. 

82
00:09:47,045 --> 00:09:53,500
Each tomato seed has a gelatin-like envelope
which stops the seed from germinating. 

83
00:09:58,700 --> 00:10:03,980
You should use a fermentation method to get
this envelope to detach itself from the seed.

84
00:10:06,940 --> 00:10:12,700
Refer to the 'Extraction by fermentation,
seed processing and selecting' module

85
00:10:13,060 --> 00:10:15,340
in the ABC of Seed production. 

86
00:10:33,355 --> 00:10:36,100
After having processed the seeds using water,

87
00:10:36,540 --> 00:10:40,140
it is crucial to put them
to dry immediately in a dry,

88
00:10:40,260 --> 00:10:43,180
shaded and well-ventilated area. 

89
00:10:48,495 --> 00:10:52,940
Another method for small quantities
is dry them on coffee filters,

90
00:10:53,775 --> 00:10:57,660
as they are very absorbing
and the seeds do not stick to them.

91
00:10:59,565 --> 00:11:03,740
Place at most a small teaspoon
of seeds on each filter.

92
00:11:05,715 --> 00:11:11,900
Hang the sachets on a clothes' rack in a dry,
airy, shaded and warm place. 

93
00:11:21,650 --> 00:11:24,060
Store the tomato seeds away from heat,

94
00:11:24,380 --> 00:11:27,980
moisture and light in a glass jar
or in a plastic sachet.

95
00:11:30,605 --> 00:11:34,140
Don't forget to insert a label
indicating the year of production,

96
00:11:34,500 --> 00:11:36,260
the species and the variety. 

97
00:11:43,490 --> 00:11:48,985
The germination capacity of tomato
seeds varies from 4 to 6 years.

98
00:11:49,880 --> 00:11:53,510
To lengthen this period,
keep the seeds in a freezer. 

