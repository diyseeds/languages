﻿1
00:00:09,980 --> 00:00:13,163
Chicory is a member of the Asteraceae family.

2
00:00:13,950 --> 00:00:20,261
It is a biennial plant consisting of two species:
Cichorium endivia

3
00:00:21,163 --> 00:00:23,330
and Cichorium intybus.

4
00:00:28,021 --> 00:00:33,505
- Cichorium endivia can be divided into two major
subspecies grown for their leaves: 

5
00:00:37,636 --> 00:00:40,327
- curly endive, of the crispa type

6
00:00:41,185 --> 00:00:44,960
- and escarole, of the latifolia type
with whole leaves. 

7
00:00:49,280 --> 00:00:53,454
The second species Cichorium intybus includes:

8
00:00:53,818 --> 00:01:01,374
- bitter chicories of the foliosum type,
with large red, variegated or green leaves

9
00:01:02,705 --> 00:01:07,090
- chicory of the sativum type
that produce Belgian endives, 

10
00:01:08,443 --> 00:01:12,203
- or varieties from whose roots
sugar is extracted

11
00:01:12,465 --> 00:01:15,352
or whose roots are roasted
as a coffee substitute. 

12
00:01:18,807 --> 00:01:22,945
The species intybus also includes
the wild chicories of Europe. 

13
00:01:32,640 --> 00:01:33,730
Pollination 

14
00:01:49,214 --> 00:01:54,618
The inflorescence of chicories is composed
of florets ranging in colour

15
00:01:54,792 --> 00:02:00,450
from blue to blue-violet, it is called a capitulum.

16
00:02:01,403 --> 00:02:04,530
The florets of both species are hermaphrodite. 

17
00:02:06,647 --> 00:02:12,909
Cicorium endivia florets are self-pollinating
but tend to be allogamous.

18
00:02:13,410 --> 00:02:17,105
They therefore do not need insects
to be fertilised. 

19
00:02:17,432 --> 00:02:21,480
Cicorium intybus florets,
however, are self-sterile,

20
00:02:21,941 --> 00:02:24,618
so they do need pollinating insects. 

21
00:02:26,203 --> 00:02:30,600
The varieties within each species
tend to cross-pollinate

22
00:02:30,858 --> 00:02:33,054
thanks to the presence of insects.

23
00:02:35,221 --> 00:02:38,974
Because of the characteristics
specific to each species,

24
00:02:39,301 --> 00:02:43,280
Cicorium endivia can also pollinate
Cichorium intybus,

25
00:02:43,738 --> 00:02:45,900
but the contrary is impossible. 

26
00:02:55,080 --> 00:02:56,810
To avoid cross-pollination,

27
00:02:56,996 --> 00:03:01,709
two varieties of chicory should be grown
at least 500 meters apart. 

28
00:03:03,447 --> 00:03:06,741
This distance can be reduced to 250 meters

29
00:03:07,010 --> 00:03:11,214
if a natural barrier such
as a hedge exists between them. 

30
00:03:13,207 --> 00:03:18,320
Cichorium endivia varieties can be grown
under permanent mosquito net cages

31
00:03:18,640 --> 00:03:22,101
to avoid crossing because
they are self-pollinating. 

32
00:03:24,894 --> 00:03:26,560
With cichorium intybus,

33
00:03:26,814 --> 00:03:30,749
use the technique of alternately opening
and closing net cages

34
00:03:31,105 --> 00:03:35,498
presented in the isolation techniques module
in “The ABC of seed production.”

35
00:03:39,178 --> 00:03:42,960
Nevertheless, it is important
to ensure that no escarole,

36
00:03:43,054 --> 00:03:47,229
curly endive or wild chicory plants
are growing nearby

37
00:03:47,934 --> 00:03:50,560
because they could cross
with the plants for seed. 

38
00:03:55,200 --> 00:03:56,540
Life cycle 

39
00:04:14,567 --> 00:04:16,901
Chicory is a biennial plant.

40
00:04:19,180 --> 00:04:21,992
It only produces seed in the second year. 

41
00:04:37,520 --> 00:04:39,130
In the first year of the cycle,

42
00:04:39,265 --> 00:04:44,356
chicory plants for seed production are grown
in the same way as those for consumption. 

43
00:05:05,963 --> 00:05:11,380
Take great care to select plants corresponding
to specific characteristics of the variety

44
00:05:11,970 --> 00:05:16,260
such as shape, size, leaf colour

45
00:05:18,480 --> 00:05:19,927
and a well-formed head. 

46
00:05:27,440 --> 00:05:28,705
For Belgian endives,

47
00:05:28,780 --> 00:05:33,192
select well-developed roots
as well as well-shaped and tight heads. 

48
00:05:39,047 --> 00:05:41,790
Chicory plants that go to seed in the first year

49
00:05:42,036 --> 00:05:46,036
and do not complete a normal cycle
of development must be removed.

50
00:05:47,600 --> 00:05:51,316
In the next few years, they would tend
to go to seed earlier and earlier,

51
00:05:51,490 --> 00:05:54,952
which is not what you want from leafy vegetables. 

52
00:06:00,916 --> 00:06:06,283
10 to 15 plants for seed are necessary
to maintain good genetic diversity. 

53
00:06:19,978 --> 00:06:24,814
In autumn, the leaves are harvested
for salad and the roots are kept for seed. 

54
00:06:33,323 --> 00:06:38,720
In regions with a mild climate, the chicory roots
can remain in the ground during winter. 

55
00:06:43,105 --> 00:06:45,323
In regions where the climate is harsh,

56
00:06:45,563 --> 00:06:48,850
there are several methods
for overwintering plants for seed. 

57
00:06:53,760 --> 00:06:56,140
First, the roots can be dug up before winter,

58
00:06:57,883 --> 00:07:00,700
trimmed several centimeters
above the collar

59
00:07:14,000 --> 00:07:18,363
and stored either in moist sand
or in pots in a cellar. 

60
00:07:20,480 --> 00:07:28,254
The ideal humidity rate is 80%
and the temperature should be between 0 and 4°C. 

61
00:07:30,094 --> 00:07:36,880
Chicory can also be sown late in autumn
in a cold greenhouse or outside in a mild climate.

62
00:07:38,290 --> 00:07:42,203
If it has formed small rosettes
by the onset of winter,

63
00:07:42,520 --> 00:07:45,149
it will be more resistant to frost. 

64
00:07:46,887 --> 00:07:50,341
They form a head in spring and flower in summer. 

65
00:07:55,374 --> 00:08:00,210
In spring, the roots that were stored
in the cellar are replanted in the garden.

66
00:08:01,020 --> 00:08:03,323
Roots that have rotted are removed.

67
00:08:21,290 --> 00:08:24,101
After replanting water copiously. 

68
00:08:52,610 --> 00:08:58,640
They will flower gradually and produce
very high flower stalks that should be staked. 

69
00:09:25,480 --> 00:09:30,967
Chicory seed should be harvested
as they mature, which can take a long time.

70
00:09:38,036 --> 00:09:41,854
Don't wait to harvest the seeds
until the plant is completely dry

71
00:09:42,020 --> 00:09:45,243
or else the majority
of the seeds will have fallen. 

72
00:09:54,840 --> 00:09:57,876
The entire head can also be harvested.

73
00:09:58,760 --> 00:10:02,400
The seeds will finish maturing
on the harvested plant. 

74
00:10:05,825 --> 00:10:10,981
In any case, the seeds should finish
drying in a dry, well-ventilated place. 

75
00:10:18,580 --> 00:10:22,290
Extracting – sorting - storing

76
00:10:28,101 --> 00:10:31,680
The plants should be completely dry
when the seed is extracted.

77
00:10:32,414 --> 00:10:36,080
Chicory seeds are often difficult
to remove from their small hull

78
00:10:36,465 --> 00:10:39,287
but sometimes they fall easily to the ground. 

79
00:10:44,298 --> 00:10:47,380
The dry seed pods are vigorously
rubbed by hand

80
00:10:47,570 --> 00:10:48,792
or beaten with a stick. 

81
00:10:52,458 --> 00:10:54,341
If the seeds remain in their hulls,

82
00:10:54,509 --> 00:11:00,545
other mechanical methods such as rolling pins,
tractor wheels or a hammer can be used. 

83
00:11:04,843 --> 00:11:09,680
Another technique can be used to extract seeds
from pods that do not want to open.

84
00:11:11,320 --> 00:11:15,220
Seeds can be run through a coffee
or herb grinder for several seconds,

85
00:11:15,469 --> 00:11:17,060
with astonishing results,

86
00:11:19,700 --> 00:11:24,247
but watch out, not too long
or else the seeds will be reduced to powder. 

87
00:11:30,430 --> 00:11:37,294
Mesh sieves of different sizes are used to sort
the seed and get rid of twigs, stems, and dust. 

88
00:11:43,949 --> 00:11:47,920
Finally, the seeds should be winnowed
to remove any remaining debris.

89
00:11:49,243 --> 00:11:54,116
Blow gently on the seeds over a plate
or a winnowing basket. 

90
00:11:57,520 --> 00:12:00,414
The dry seeds are then poured
into a waterproof bag.

91
00:12:01,876 --> 00:12:05,105
Put a label with the name
of the species, the variety,

92
00:12:05,294 --> 00:12:08,443
as well as the year of production
inside the packet. 

93
00:12:13,112 --> 00:12:18,225
Storing the seeds in the freezer
for several days kills parasite larvae. 

94
00:12:19,381 --> 00:12:23,490
Chicory seeds are able to germinate
for at least 5 years on average.

95
00:12:23,980 --> 00:12:28,123
This period of time can be lengthened
if the seeds are stored in a freezer. 

