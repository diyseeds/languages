﻿1
00:00:09,300 --> 00:00:14,146
Broad beans are from the Fabaceae
family and the Vicia faba species.

2
00:00:16,435 --> 00:00:21,824
They are annual plants grown for their seeds
that can be of different colours and sizes.

3
00:00:23,860 --> 00:00:26,232
They are also grown for their young
shoots.

4
00:00:28,620 --> 00:00:30,848
There are different types of broad beans,

5
00:00:31,232 --> 00:00:42,080
the ones for human consumption
and beans for animal forage. 

6
00:00:53,648 --> 00:00:54,712
Pollination 

7
00:01:12,976 --> 00:01:17,880
The flowers of broad beans
are hermaphrodite and self-fertilising,

8
00:01:18,888 --> 00:01:24,616
meaning that the male and female organs are
on the same flower and are compatible.

9
00:01:25,656 --> 00:01:27,744
They are therefore autogamous. 

10
00:01:31,120 --> 00:01:36,888
There is however a risk of cross-pollination
between different varieties by insects. 

11
00:01:43,440 --> 00:01:50,064
The frequency of crosses varies
from 5 to 60 % depending on varieties,

12
00:01:50,376 --> 00:01:53,736
the environment and whether
there are natural barriers. 

13
00:01:54,680 --> 00:02:00,496
To avoid cross-pollination,
grow different varieties 1km apart. 

14
00:02:01,064 --> 00:02:04,312
This distance can be reduced
to a few hundred meters

15
00:02:04,624 --> 00:02:07,768
if there's a natural barrier, such as a hedge. 

16
00:02:12,616 --> 00:02:15,400
To ensure the purity of a variety,

17
00:02:15,664 --> 00:02:18,856
you can cover the plants
for seed production with a net.

18
00:02:24,910 --> 00:02:29,192
It is important to put it in place
before flowering begins. 

19
00:02:39,260 --> 00:02:41,128
Life cycle 

20
00:02:57,824 --> 00:03:02,512
Broad beans grown for seeds are cultivated
in the same way as those for food. 

21
00:03:07,560 --> 00:03:10,144
This plant does not like high temperatures

22
00:03:10,456 --> 00:03:13,728
as this stops pollination
and reduces production.

23
00:03:17,952 --> 00:03:23,928
Sow broad beans either at the end of autumn
in fair climates or at the end of winter,

24
00:03:24,040 --> 00:03:25,816
when the earth is ready. 

25
00:03:47,440 --> 00:03:49,775
To ensure good genetic diversity,

26
00:03:50,192 --> 00:03:54,336
it is necessary to grow at least
10 broad bean plants for seeds. 

27
00:04:17,856 --> 00:04:22,416
Choose the plants according
to the criteria specific to the variety,

28
00:04:22,840 --> 00:04:26,520
such as the size of the plant,
the colour of the flower,

29
00:04:29,080 --> 00:04:34,410
the number of pods, the number of seeds per pod,

30
00:04:35,104 --> 00:04:38,232
their size, colour and taste. 

31
00:04:42,680 --> 00:04:46,416
While the plants are developing,
choose the most beautiful,

32
00:04:46,736 --> 00:04:50,104
healthy and productive plants
for seed production. 

33
00:04:51,448 --> 00:04:55,552
The length of the harvest period
is also a selection criteria. 

34
00:05:00,624 --> 00:05:04,240
You should reserve a part
of the crop for seed production

35
00:05:04,570 --> 00:05:08,080
and not harvest any beans before full maturity.

36
00:05:13,880 --> 00:05:16,624
Avoid picking the first pods for consumption

37
00:05:17,050 --> 00:05:23,256
and keeping the last ones for seeds
as seeds from the first pods

38
00:05:23,576 --> 00:05:27,064
will keep the early characteristics
of the variety. 

39
00:05:34,336 --> 00:05:35,630
If the weather is wet,

40
00:05:36,072 --> 00:05:38,592
harvest the seeds before they are fully mature

41
00:05:39,072 --> 00:05:42,832
and leave them to dry in a dry
and well-ventilated area.

42
00:05:52,600 --> 00:05:53,776
Most of the time,

43
00:05:53,880 --> 00:05:58,208
the plants can be left standing to dry
until the pods become black. 

44
00:06:06,520 --> 00:06:11,776
The best seeds are in the first pods to be formed,
at the base of the plant. 

45
00:06:13,570 --> 00:06:17,056
To make sure that the seeds are dry,
bite one gently.

46
00:06:19,680 --> 00:06:22,960
If this leaves no mark,
then they are fully dry. 

47
00:06:33,080 --> 00:06:36,900
Extracting – sorting - storing

48
00:06:46,400 --> 00:06:51,232
You can either extract pod by pod
or drive over them with a vehicle.

49
00:06:52,010 --> 00:06:58,920
In this case, make sure to place the harvest
on soft ground so as to not damage the seeds. 

50
00:07:02,520 --> 00:07:06,976
To sort the beans, remove those
that are of a different type.

51
00:07:07,456 --> 00:07:09,808
They are a sign of cross-pollination.

52
00:07:12,256 --> 00:07:18,656
Also remove damaged or badly formed beans
and those invested with weavils. 

53
00:07:20,368 --> 00:07:26,000
Broad bean seeds are often inhabited
by weavils (bruchus rufinamus),

54
00:07:26,648 --> 00:07:31,088
small insects that lay their eggs
under the skin of seeds. 

55
00:07:32,704 --> 00:07:37,808
An easy way to get rid of them is to leave
the seeds in the freezer for a few days. 

56
00:07:44,640 --> 00:07:48,160
It is important to put a label
with the name of the variety and species,

57
00:07:48,330 --> 00:07:53,720
as well as the year, inside the package,
as writing on the outside often rubs off. 

58
00:08:00,224 --> 00:08:05,344
Broad bean seeds have a germination
capacity of 5 to 10 years.

59
00:08:06,056 --> 00:08:09,808
This can be extended by storing
them at a low temperature. 

