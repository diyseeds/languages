1
00:00:09,440 --> 00:00:14,305
Both squash and zucchini, or courgette,
belong to the Cucurbitacea family.

2
00:00:16,960 --> 00:00:21,260
Most are annual plants that are
divided into several species :

3
00:00:26,260 --> 00:00:29,950
The Cucurbita pepo species
includes zucchinis,

4
00:00:31,305 --> 00:00:32,600
pattypan squashes,

5
00:00:33,950 --> 00:00:35,105
oil squashes

6
00:00:37,300 --> 00:00:39,500
and several ornamental squashes.

7
00:00:43,660 --> 00:00:49,435
Most are bushy, the leaves are
prickly and are often deeply cut.

8
00:00:50,525 --> 00:00:52,395
The stalks are also prickly.

9
00:00:55,480 --> 00:00:56,970
The seeds are curved,

10
00:00:57,255 --> 00:01:01,775
of a uniform greyish white colour
and they have a white margin. 

11
00:01:05,160 --> 00:01:12,340
 Cucurbita maxima includes wild specimens
which can be found in Argentina and Bolivia.

12
00:01:13,520 --> 00:01:19,985
It is a species with long but not very firm
hairy vines that run along the ground. 

13
00:01:23,010 --> 00:01:29,970
The leaves are big and hairy,
they are never deeply cut and have round lobes.

14
00:01:32,095 --> 00:01:36,765
The fruit peduncle is also always
round and looks similar to cork.

15
00:01:38,800 --> 00:01:42,110
It often splits
and it is much thicker than the stem.

16
00:01:46,120 --> 00:01:50,330
The seeds are often covered by
a membrane that is easy to remove.

17
00:01:51,060 --> 00:01:53,785
They are oval and often swollen. 

18
00:01:57,590 --> 00:02:03,160
Cucurbita moshata is a species
in which all plants run along the ground.

19
00:02:03,360 --> 00:02:06,980
They have very long
and hairy zig-zag shaped vines.

20
00:02:11,755 --> 00:02:14,250
Their leaves are often spotted with white,

21
00:02:14,575 --> 00:02:18,790
have pointed tips and slight
indentations along the sides.

22
00:02:20,265 --> 00:02:24,285
The peduncle is hard,
hairy and slightly angular.

23
00:02:25,520 --> 00:02:29,955
The seeds are beige, oblong,
and have a dark beige margin.

24
00:02:32,565 --> 00:02:35,850
This species needs higher temperatures
than other squashes.

25
00:02:37,500 --> 00:02:40,615
The fruit do not always mature
fully in cold climates. 

26
00:02:53,100 --> 00:02:54,085
Pollination 

27
00:03:01,335 --> 00:03:03,990
Squashes are monoecious plants,

28
00:03:04,100 --> 00:03:08,220
meaning that they have both male
and female flowers on the same plant. 

29
00:03:09,170 --> 00:03:12,380
The female flowers
have an ovary below the petals.

30
00:03:12,985 --> 00:03:16,330
It looks like a mini squash
and will later develop.

31
00:03:17,720 --> 00:03:21,220
The male flowers are at the end of long stems.

32
00:03:23,940 --> 00:03:26,455
The flowers open for one day only. 

33
00:03:32,170 --> 00:03:34,825
The squash can be self-fertilised,

34
00:03:35,285 --> 00:03:39,660
meaning that the female flowers
can be fertilised by pollen from a male flower

35
00:03:39,855 --> 00:03:41,270
on the same plant.

36
00:03:46,760 --> 00:03:52,120
However, cross-pollination is more common
between different plants of the same variety

37
00:03:52,990 --> 00:03:54,855
and within the same species.

38
00:04:01,675 --> 00:04:05,765
Insects and above all bees
pollinate squash flowers. 

39
00:04:14,325 --> 00:04:20,175
The only inter-species cross fertilisation,
also called inter-specifics,

40
00:04:20,310 --> 00:04:26,520
happens between cucurbita argyrosperma
and Cucurbita moschata.

41
00:04:28,630 --> 00:04:32,405
It happens only rarely
with a wild Cucurbita pepo. 

42
00:04:35,915 --> 00:04:40,405
It is therefore generally possible to grow
two species of squash next to each other

43
00:04:40,830 --> 00:04:43,380
without there being a risk of cross-pollination. 

44
00:04:44,260 --> 00:04:45,675
It is, on the other hand,

45
00:04:45,855 --> 00:04:51,745
important to separate two varieties
from the same species by a distance of 1km. 

46
00:04:52,825 --> 00:04:58,030
This can be reduced to 500m if there is
a natural barrier such as a hedge. 

47
00:05:03,530 --> 00:05:07,500
There are several methods to produce seeds
from different varieties of squash

48
00:05:07,580 --> 00:05:08,900
grown in the same garden. 

49
00:05:12,635 --> 00:05:15,765
The first is to cover
one variety with a mosquito net

50
00:05:16,145 --> 00:05:18,875
and to place a bumblebee hive inside.

51
00:05:25,225 --> 00:05:27,915
But this is difficult
with the running varieties,

52
00:05:28,145 --> 00:05:30,570
as they rapidly fill up all of the space

53
00:05:31,325 --> 00:05:34,060
and prevent the insects
from moving around easily. 

54
00:05:38,095 --> 00:05:42,805
Another method is to cover
two varieties with two different net cages :

55
00:05:43,840 --> 00:05:47,405
open one while the other
is closed on one day,

56
00:05:48,050 --> 00:05:49,975
and the other way round the next day.

57
00:05:50,865 --> 00:05:52,955
Let the wild insects do their work. 

58
00:06:06,385 --> 00:06:09,400
It is also possible to pollinate
the flowers manually.

59
00:06:11,780 --> 00:06:16,055
It is relatively simple to do this,
as the flowers are big and visible.

60
00:06:19,870 --> 00:06:22,735
These three methods are explained
in greater detail

61
00:06:22,950 --> 00:06:25,875
in the modules on mechanical
isolation techniques

62
00:06:26,070 --> 00:06:30,290
and on manual pollination
in the ABC of seed production

63
00:06:40,580 --> 00:06:41,580
Life cycle 

64
00:07:06,860 --> 00:07:12,430
Squashes grown for seed are cultivated
in the same way as those for consumption. 

65
00:07:29,810 --> 00:07:33,125
Keep at least 6 plants to ensure
good genetic diversity.

66
00:07:36,310 --> 00:07:38,145
Ideally, grow a dozen. 

67
00:08:09,330 --> 00:08:12,255
Take great care to select the plants
you keep for seeds

68
00:08:12,325 --> 00:08:15,550
according to the specific characteristics
of the variety,

69
00:08:17,700 --> 00:08:20,455
for example whether it is running or not. 

70
00:08:22,560 --> 00:08:28,785
Check the shape and size of the fruit
the taste and texture of its flesh.

71
00:08:29,560 --> 00:08:32,375
Whether it has a good storing capacity. 

72
00:08:32,640 --> 00:08:35,460
Also check its resistance to diseases. 

73
00:08:36,110 --> 00:08:40,985
A squash grown for seed is mature
at the same time as squashes for consumption

74
00:08:41,465 --> 00:08:44,255
which are generally eaten
when they are fully ripe.

75
00:08:45,050 --> 00:08:49,295
Exceptions are certain varieties
of the Cucurbita pepo species,

76
00:08:49,675 --> 00:08:54,510
such as pattypan squashes and zucchinis
which are eaten before they are mature. 

77
00:08:56,215 --> 00:09:00,980
Let the zucchini, like the squash,
ripen until its colour has changed,

78
00:09:01,150 --> 00:09:05,570
that it has attained its full size,
that the peduncle is dry and hard,

79
00:09:06,170 --> 00:09:08,855
and in the case of zucchinis that its skin is hard. 

80
00:09:13,875 --> 00:09:15,395
You can then harvest them,

81
00:09:16,060 --> 00:09:21,445
place them in a warm place
and let them ripen at least one month.

82
00:09:24,640 --> 00:09:28,270
With this technique
the seeds' fertility will be increased. 

83
00:09:33,780 --> 00:09:36,515
Extracting - sorting - storing

84
00:09:42,100 --> 00:09:44,580
To extract the seeds, open the squash,

85
00:09:47,530 --> 00:09:50,125
and remove the seeds using a spoon.

86
00:09:50,375 --> 00:09:52,435
Avoid taking too much flesh,

87
00:09:57,140 --> 00:09:58,660
rinse with water.

88
00:10:07,350 --> 00:10:11,765
With certain varieties it is difficult
to separate the seeds from the flesh.

89
00:10:12,265 --> 00:10:16,430
In this case soak the seeds and flesh
in water at room temperature

90
00:10:16,900 --> 00:10:19,130
and remove the seeds the following day. 

91
00:10:30,405 --> 00:10:36,795
Dry the seeds at a temperature
between 22 and 25°C in a well ventilated area. 

92
00:10:39,675 --> 00:10:42,455
To make sure the seed is dry, try to fold it.

93
00:10:42,660 --> 00:10:44,975
If it breaks it is dry enough. 

94
00:11:02,500 --> 00:11:05,215
Put a label with the name
of the species and variety,

95
00:11:05,415 --> 00:11:10,215
as well as the year, inside the package,
as writing on the outside may rub off. 

96
00:11:16,320 --> 00:11:21,910
It is best to put the seeds in the freezer
for a few days to kill unwanted parasites. 

97
00:11:27,240 --> 00:11:32,485
Squash and zucchini seeds have an average
germination capacity of 6 years,

98
00:11:32,975 --> 00:11:34,930
but this can also last for 10 years.

99
00:11:38,510 --> 00:11:42,035
You can increase this by storing
the seeds in a freezer. 

