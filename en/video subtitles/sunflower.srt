 1
00:00:10,990 --> 00:00:16,940
The sunflower belongs to the Asteraceae family
and to the Helianthus annuus species.

2
00:00:20,970 --> 00:00:26,500
It is an annual plant grown for its seeds,
from which we make oil, or for its flower heads.

3
00:00:29,745 --> 00:00:32,020
There are several varieties of sunflower.

4
00:00:33,925 --> 00:00:37,780
Only one perennial species is used
for its edible tuber:

5
00:00:38,020 --> 00:00:41,820
the Jerusalem artichoke,
Helianthus tuberosus. 

6
00:00:54,895 --> 00:00:55,980
Pollination 

7
00:01:05,150 --> 00:01:08,580
The flower of the sunflower
is called a capitulum.

8
00:01:09,660 --> 00:01:12,380
It is composed of very many florets

9
00:01:13,155 --> 00:01:17,340
that bloom one after another
starting from the periphery.

10
00:01:20,960 --> 00:01:23,100
Each flower is hermaphrodite.

11
00:01:24,240 --> 00:01:26,420
The male organ comes out first,

12
00:01:31,435 --> 00:01:34,460
and releases pollen for one day.

13
00:01:38,275 --> 00:01:44,060
The flower then changes and the female organ
appears to receive the pollen.

14
00:01:58,180 --> 00:02:01,300
Insects, in particular bees and bumblebees

15
00:02:01,700 --> 00:02:06,540
pollinate each flower by carrying
the pollen from one floret to another. 

16
00:02:13,725 --> 00:02:19,180
Sunflowers mainly cross pollinate
since most varieties are auto-incompatible.

17
00:02:25,050 --> 00:02:31,060
This means that the florets of one plant
can only be fertilised by those of another plant.

18
00:02:32,080 --> 00:02:34,940
You should therefore grow
several sunflower plants together

19
00:02:35,190 --> 00:02:36,945
to ensure good pollination. 

20
00:02:42,720 --> 00:02:45,900
Some varieties of sunflower
are auto compatible,

21
00:02:46,510 --> 00:02:51,100
meaning their florets can be fertilised
by other florets from the same flower head. 

22
00:02:57,290 --> 00:03:00,100
All varieties of sunflower cross fertilise.

23
00:03:04,705 --> 00:03:09,620
There are wild sunflowers in some regions
which can fertilise cultivated ones.

24
00:03:13,505 --> 00:03:16,020
The same occurs with the Jerusalem artichoke,

25
00:03:16,267 --> 00:03:18,660
a close species, botanically speaking. 

26
00:03:24,815 --> 00:03:27,700
To avoid cross-pollination between varieties,

27
00:03:28,145 --> 00:03:31,570
grow two varieties of sunflower 1km apart.

28
00:03:33,980 --> 00:03:37,140
You can reduce this distance to 700 metres

29
00:03:37,420 --> 00:03:40,900
if there is a natural barrier
such as a hedge between them. 

30
00:03:46,475 --> 00:03:49,740
If your garden is close to
a large sunflower field

31
00:03:50,440 --> 00:03:54,560
or if you want to grow several varieties close
to each other in the same garden,

32
00:03:54,985 --> 00:03:58,060
you will need to pollinate the flowers manually

33
00:03:58,700 --> 00:04:00,980
to protect the purity of each variety. 

34
00:04:11,905 --> 00:04:15,100
It is quite simple to pollinate
flowers manually.

35
00:04:25,065 --> 00:04:30,100
You need to wrap each flower head
in a solid and waterproof paper kraft bag

36
00:04:30,435 --> 00:04:32,380
before the flowers start blossoming.

37
00:05:05,845 --> 00:05:10,420
When they are in bloom, take the bags off
two plants that are next to each other.

38
00:05:17,190 --> 00:05:20,140
While you do this,
watch out for bees and bumblebees

39
00:05:20,180 --> 00:05:23,860
that will constantly try to come
and visit unprotected flowers.

40
00:05:28,540 --> 00:05:31,035
Gently rub the flower heads against each other.

41
00:05:42,975 --> 00:05:45,860
When you have finished,
put the bags back on.

42
00:05:49,235 --> 00:05:55,060
Blossoming lasts 5 to 10 days,
so you must do this every day during this period.

43
00:06:10,470 --> 00:06:13,860
You can leave the sachet in place
until you harvest the seeds. 

44
00:06:28,470 --> 00:06:29,820
Life cycle

45
00:06:49,200 --> 00:06:51,500
Sunflowers grown for seed production

46
00:06:51,790 --> 00:06:55,940
are cultivated in the same way
as those for their seeds or flower heads. 

47
00:07:19,590 --> 00:07:21,820
To ensure good genetic diversity,

48
00:07:22,180 --> 00:07:25,620
it is better to grow at least
10 plants for their seeds.

49
00:07:35,440 --> 00:07:38,180
Be careful to select the seed-bearing plants

50
00:07:38,395 --> 00:07:41,940
according to the criteria
specific to the variety,

51
00:07:44,130 --> 00:07:45,540
such as height,

52
00:07:47,635 --> 00:07:48,740
size

53
00:07:49,500 --> 00:07:53,900
and colour of the flower heads
as well as the quality of the seeds. 

54
00:07:54,635 --> 00:07:59,020
The seeds are formed progressively,
starting from the periphery of the flower head

55
00:07:59,260 --> 00:08:01,140
and going towards the centre.

56
00:08:05,940 --> 00:08:08,780
Harvest the sunflower
when the head is full of seeds

57
00:08:09,145 --> 00:08:11,060
and the petals have started to fall.

58
00:08:14,370 --> 00:08:16,535
Birds are very fond of sunflower seeds,

59
00:08:16,810 --> 00:08:22,185
so don’t wait until the whole plant has dried
before harvesting as all the seeds may have gone! 

60
00:08:48,145 --> 00:08:52,390
Rub the heads to remove the dried flowers
and let them fall to the ground.

61
00:09:09,550 --> 00:09:13,940
Also, cut the outer petals
so that the flower heads can dry better.

62
00:09:20,710 --> 00:09:23,020
Then put them in a dry and airy place,

63
00:09:23,295 --> 00:09:27,340
with the seeds facing upwards
to avoid mould or fermentation. 

64
00:09:42,410 --> 00:09:44,980
Extracting – sorting – storing

65
00:09:51,425 --> 00:09:54,420
Rub the flower head and the seeds will fall.

66
00:10:03,865 --> 00:10:08,660
You can also put a metal mesh on a bucket
and rub the flower heads on it. 

67
00:10:15,080 --> 00:10:19,420
Leave the seeds to dry fully
in a dry and well-ventilated area. 

68
00:10:25,585 --> 00:10:29,590
To make sure drying is complete,
try to fold a seed.

69
00:10:30,155 --> 00:10:33,260
If it breaks, it is ready to be stored. 

70
00:10:36,650 --> 00:10:40,405
To finish, winnow the seeds
to remove unwanted debris.

71
00:10:41,240 --> 00:10:46,420
To do this, place the seeds on a plate
or a winnowing basket and blow on top of it.

72
00:10:47,440 --> 00:10:49,305
This will remove the lighter waste. 

73
00:11:03,545 --> 00:11:05,310
Place the seeds in a sachet

74
00:11:05,420 --> 00:11:11,380
and add a label inside indicating the species
and variety as well as the year of harvest. 

75
00:11:15,440 --> 00:11:19,700
Leaving the seeds in the freezer
for a few days will kill any parasite larvae. 

76
00:11:25,280 --> 00:11:29,220
Sunflower seeds have an average
germination capacity of 7 years.

77
00:11:32,040 --> 00:11:35,340
Storing them at a low temperature
can lengthen this period. 

