1
00:00:09,295 --> 00:00:12,180
The melon belongs to the Cucurbitaceae family

2
00:00:13,775 --> 00:00:16,220
and to the Cucumis melo species.

3
00:00:23,080 --> 00:00:24,460
It is an annual plant

4
00:00:24,665 --> 00:00:30,340
that is divided into several types
with different shapes, colours and tastes.

5
00:00:34,830 --> 00:00:40,625
For example, there are the musk melon,
cantaloupe melon, melons for candying,

6
00:00:41,425 --> 00:00:44,680
and the winter melon
that can be stored for several months. 

7
00:01:00,980 --> 00:01:01,980
Pollination

8
00:01:20,015 --> 00:01:22,620
The melon is a monoecious plant,

9
00:01:24,595 --> 00:01:29,080
meaning that it bears both male
and female flowers on the same plant.

10
00:01:29,975 --> 00:01:33,200
Female flowers have an ovary under the flower.

11
00:01:34,000 --> 00:01:37,420
It is in fact a miniature melon that will develop. 

12
00:01:40,955 --> 00:01:44,285
The male flowers are at the end of long stems.

13
00:01:49,275 --> 00:01:51,835
The flowers only open during one day. 

14
00:01:55,750 --> 00:01:57,660
It can be self-fertilised,

15
00:01:58,165 --> 00:02:02,620
meaning that the female flowers
can be fertilised by pollen from a male flower

16
00:02:02,760 --> 00:02:03,960
on the same plant. 

17
00:02:07,765 --> 00:02:10,970
Cross-fertilisation is, however, the most common.

18
00:02:11,920 --> 00:02:16,250
Insects, above all bees, pollinate melon flowers.

19
00:02:23,730 --> 00:02:30,145
All varieties of Cucumis melo cross-fertilise
between each other, including with wild melons.

20
00:02:30,465 --> 00:02:37,155
But cross-pollination is impossible between
melons and cucumbers, watermelons, or squashes. 

21
00:02:41,950 --> 00:02:47,725
To avoid cross-pollination, keep a distance
of 1km between two varieties of melon. 

22
00:02:49,240 --> 00:02:54,665
This can be reduced to 400m if there is
a natural barrier such as a hedge. 

23
00:02:59,560 --> 00:03:04,460
There are several methods to produce seeds
from different varieties of melon

24
00:03:04,645 --> 00:03:06,080
grown in the same garden. 

25
00:03:08,205 --> 00:03:14,010
The first is to cover one variety
with a net and to place a bumblebee hive inside. 

26
00:03:25,520 --> 00:03:29,820
Another one is to cover two varieties
with two different nets :

27
00:03:30,800 --> 00:03:37,125
you open one while the other is closed
on one day, and alternate the next day.

28
00:03:39,210 --> 00:03:41,625
Leave the wild insects to do their work.

29
00:03:44,270 --> 00:03:49,530
The production in this case will be lower
as some flowers will not be pollinated. 

30
00:03:52,590 --> 00:03:55,985
The third method is to pollinate
the flowers manually.

31
00:03:58,080 --> 00:04:00,940
This is not as simple as for squashes or zucchini,

32
00:04:01,195 --> 00:04:03,220
as melon flowers are very small

33
00:04:03,565 --> 00:04:06,895
and it can be difficult to identify
the moment of blossoming.

34
00:04:08,035 --> 00:04:12,820
Above all, melons will abort
about 80% of the female blossoms.

35
00:04:13,970 --> 00:04:17,540
Hand pollination is even less effective
than insect pollination,

36
00:04:22,430 --> 00:04:28,390
so only about 10-15% of the hand-pollinated
blossoms will develop into fruits. 

37
00:04:35,305 --> 00:04:38,740
These three methods
are described in greater detail

38
00:04:38,880 --> 00:04:42,100
in the modules
on mechanical isolation techniques

39
00:04:42,465 --> 00:04:47,055
and on manual pollination
in the ABC on seed production. 

40
00:04:51,100 --> 00:04:52,260
The melon’s life cycle 

41
00:05:09,315 --> 00:05:13,740
Melons grown for seed are cultivated
in the same way as those for consumption.

42
00:05:43,035 --> 00:05:47,180
Depending on the variety,
melons need different temperatures.

43
00:05:48,890 --> 00:05:52,130
It is better to grow at least
6 plants for seed production

44
00:05:52,220 --> 00:05:54,290
to ensure good genetic diversity.

45
00:05:55,840 --> 00:05:57,755
Ideally, grow a dozen. 

46
00:06:01,755 --> 00:06:04,860
Take great care to select the plants
you keep for seeds

47
00:06:05,040 --> 00:06:09,685
according to the specific characteristics
of the variety, such as precocity,

48
00:06:10,240 --> 00:06:11,100
plant vigour,

49
00:06:12,645 --> 00:06:13,845
the number of fruit,

50
00:06:15,280 --> 00:06:19,415
its capacity to be grown
in open fields in temperate climates,

51
00:06:20,960 --> 00:06:23,500
the taste and sweetness of the flesh.

52
00:06:26,330 --> 00:06:27,830
Get rid of sick plants. 

53
00:06:39,085 --> 00:06:42,820
It is easy to identify the level
of maturity of the melon seed:

54
00:06:43,410 --> 00:06:46,260
the fruit must be ripe and ready
for consumption. 

55
00:06:51,975 --> 00:06:54,980
Extraction - sorting – storing

56
00:07:00,835 --> 00:07:04,340
To extract the seeds,
open the melon by cutting it in two.

57
00:07:05,265 --> 00:07:07,820
Remove the seeds using a spoon.

58
00:07:09,545 --> 00:07:11,380
Enjoy eating the rest of the melon. 

59
00:07:17,615 --> 00:07:24,420
Simply rinse the seeds under water
and let them dry in a shaded area. 

60
00:07:30,330 --> 00:07:34,110
To be sure that the seeds are dry,
they should break when you bend them. 

61
00:07:46,965 --> 00:07:50,300
Always place a label with the name
of the variety and species

62
00:07:50,370 --> 00:07:54,980
as well as the year in the package,
as writing on the outside may rub off. 

63
00:08:02,380 --> 00:08:06,780
Ideally, put them in the freezer
for a few days to destroy any parasites. 

64
00:08:12,065 --> 00:08:15,730
Melon seeds have a germination capacity
of 5 years on average,

65
00:08:15,950 --> 00:08:18,740
but they can germinate for up to 10 years. 

