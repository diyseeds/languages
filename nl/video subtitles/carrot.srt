1
00:00:09,565 --> 00:00:14,960
The carrot belongs to the Apiaceae family
and the Daucus carota species.

2
00:00:15,565 --> 00:00:17,510
It is grown for its root.

3
00:00:21,600 --> 00:00:23,545
There are two subspecies :

4
00:00:25,410 --> 00:00:30,860
- The western carrot, carota sativus,
which is generally biennial 

5
00:00:35,530 --> 00:00:42,060
- and the oriental carrot, carota atrorubens,
which is usually annual. 

6
00:00:48,315 --> 00:00:56,250
Certain carrot varieties are early,
while others are grown for storage.

7
00:00:58,440 --> 00:01:06,025
Their roots range from white to black
and can be yellow, orange, red or purple.

8
00:01:06,455 --> 00:01:09,220
Their shapes are also very diverse. 

9
00:01:15,015 --> 00:01:15,985
Pollination 

10
00:01:26,685 --> 00:01:32,645
The inflorescence of the carrot
is an umbel composed of small flowers

11
00:01:33,055 --> 00:01:35,270
that are usually hermaphrodite.

12
00:01:36,875 --> 00:01:39,940
The stamen, the male sexual organ,

13
00:01:41,945 --> 00:01:43,850
matures before the pistil,

14
00:01:44,260 --> 00:01:45,960
the female sexual organ.

15
00:01:47,885 --> 00:01:51,700
Self-fertilization does not occur
within the same flower.

16
00:01:53,955 --> 00:01:57,210
Yet since the flowers do not bloom
at the same time,

17
00:01:59,935 --> 00:02:03,585
self-fertilization is possible
within the same umbel

18
00:02:04,165 --> 00:02:07,225
or between two umbels on the same plant. 

19
00:02:08,660 --> 00:02:13,290
Fertilization also occurs between
the umbels of different plants. 

20
00:02:14,750 --> 00:02:20,510
The carrot is therefore an allogamous plant
mainly pollinated by insects.

21
00:02:22,660 --> 00:02:26,760
There is a risk of cross-pollination
between different varieties. 

22
00:02:44,090 --> 00:02:47,225
The carrot can also cross with wild carrot,

23
00:02:47,560 --> 00:02:52,905
which is very common in many regions
of the world and whose genes are dominant,

24
00:02:53,305 --> 00:02:56,115
as are those of all wild species. 

25
00:02:57,395 --> 00:03:00,600
The wild carrot is easily recognizable

26
00:03:00,985 --> 00:03:05,620
thanks to the small black flower
at the center of its inflorescence. 

27
00:03:06,860 --> 00:03:08,770
To avoid cross-pollination,

28
00:03:09,150 --> 00:03:13,650
two varieties of carrot should be grown
about one kilometer apart. 

29
00:03:16,420 --> 00:03:19,840
This distance can be reduced to 500 meters

30
00:03:20,330 --> 00:03:25,635
if a natural barrier such as a hedge exists
between the two varieties. 

31
00:03:28,225 --> 00:03:30,365
The varieties can also be isolated

32
00:03:30,585 --> 00:03:34,425
by alternately opening
and closing mosquito nets

33
00:03:40,790 --> 00:03:46,065
or by placing small hives of insects
inside a closed mosquito net.

34
00:03:47,330 --> 00:03:48,560
For this technique,

35
00:03:48,855 --> 00:03:54,480
see the module on isolation techniques
in " The ABC of seed production ". 

36
00:04:01,930 --> 00:04:04,060
Life cycle of the carrot 

37
00:04:12,695 --> 00:04:17,455
The varieties of oriental carrots
generally function as annual plants

38
00:04:17,535 --> 00:04:20,830
when they are grown during the period
when the days are long.

39
00:04:22,870 --> 00:04:25,485
They will produce seeds in the first year. 

40
00:04:31,215 --> 00:04:36,755
With western carrots, on the other hand,
you need two years to produce seeds.

41
00:04:37,090 --> 00:04:41,830
In the first year they will produce
their root and a bouquet of leaves. 

42
00:04:43,910 --> 00:04:47,840
They require a vernalisation period, a winter,

43
00:04:48,400 --> 00:04:52,030
before they flower
and produce seed in the second year. 

44
00:04:54,405 --> 00:04:58,890
Early carrot varieties are sown
as late as possible in the season

45
00:04:59,365 --> 00:05:03,585
in order to avoid their being
too mature for storage in winter.

46
00:05:04,055 --> 00:05:08,810
Otherwise, they will have difficulty
in growing again the following spring. 

47
00:05:39,670 --> 00:05:41,085
Depending on the region,

48
00:05:41,255 --> 00:05:46,080
there are different methods for storing
carrots for seed production over the winter.

49
00:05:46,835 --> 00:05:50,435
The easiest is of course to leave them
in the ground in the garden

50
00:05:50,555 --> 00:05:51,975
if the climate allows it.

51
00:05:55,140 --> 00:06:00,120
A layer of straw is sometimes enough
to protect them from light frosts. 

52
00:06:02,020 --> 00:06:06,820
In regions with a colder climate
where there is a significant risk of frost,

53
00:06:07,540 --> 00:06:12,410
the plants should be dug up before the winter
cold and stored away from the frost. 

54
00:06:14,720 --> 00:06:18,040
Those that blossom in the first year are not kept

55
00:06:18,485 --> 00:06:20,980
because the plants from these seeds

56
00:06:21,150 --> 00:06:24,645
tend to blossom earlier
and earlier with each harvest.

57
00:06:29,305 --> 00:06:33,340
You should also avoid keeping carrots
if they have a green collar,

58
00:06:33,795 --> 00:06:36,355
are cracked, or have several roots. 

59
00:06:39,810 --> 00:06:45,390
The roots are cleaned without water
and the leaves are cut above the collar.

60
00:06:46,855 --> 00:06:50,575
They are then left to dry a short
while in the open air. 

61
00:07:20,175 --> 00:07:24,940
They should be selected according
to the characteristics specific to the variety:

62
00:07:25,715 --> 00:07:31,155
colour, shape, vigour and storage capacity.

63
00:07:38,910 --> 00:07:42,655
It is also important to select
on the basis of the taste,

64
00:07:43,080 --> 00:07:46,960
because each carrot of the same variety
can have a different taste. 

65
00:07:51,340 --> 00:07:54,845
To taste them, you can cut the tip of the carrot.

66
00:07:55,865 --> 00:07:59,175
The roots must then be disinfected with ash. 

67
00:08:05,180 --> 00:08:08,375
You should then put the selected roots
in a sandbox

68
00:08:08,505 --> 00:08:13,240
that is protected from frost
or vertically in wooden boxes.

69
00:08:19,295 --> 00:08:26,230
Ideal storage conditions are 1°C
and 90 to 95% humidity. 

70
00:08:35,010 --> 00:08:38,450
Over winter, you should
regularly check the roots

71
00:08:38,760 --> 00:08:40,985
and remove any that are beginning to rot. 

72
00:09:10,735 --> 00:09:14,115
The roots are then replanted
at the beginning of spring

73
00:09:14,500 --> 00:09:17,450
once the risk of a hard frost has passed.

74
00:09:17,800 --> 00:09:22,555
Care must be taken that the replanted
roots do not dry out.

75
00:09:23,230 --> 00:09:28,355
They must gradually get used to the light
and be protected from intense sunlight. 

76
00:09:31,055 --> 00:09:37,650
In certain regions, cultivated carrots flower
at the same time as wild carrots.

77
00:09:37,920 --> 00:09:42,165
To avoid crosses, you should stagger
the flowering periods.

78
00:09:42,510 --> 00:09:43,635
With this in mind,

79
00:09:43,735 --> 00:09:48,735
at the end of winter you can replant the carrot
roots intended for seed production

80
00:09:49,115 --> 00:09:54,770
in pots under a cold frame
in a well-lit place safe from frost.

81
00:10:15,080 --> 00:10:18,295
You should then plant them out
once it is possible. 

82
00:10:18,920 --> 00:10:24,660
A minimum of 30 plants are necessary
to maintain good genetic diversity,

83
00:10:25,785 --> 00:10:29,825
ideally you should keep between 50 and 100.

84
00:10:51,830 --> 00:10:55,015
The plants should be staked
while they are growing. 

85
00:10:59,080 --> 00:11:03,930
The carrot produces several umbels
that do not bloom at the same time.

86
00:11:04,750 --> 00:11:09,905
The first, the primary umbel,
is found at the tip of the main stem. 

87
00:11:11,400 --> 00:11:15,575
The secondary umbels are those
that develop from the main stem. 

88
00:11:21,715 --> 00:11:25,270
The tertiary umbels form on the secondary stems. 

89
00:11:30,535 --> 00:11:34,190
Since the umbels mature over
a long period of time,

90
00:11:34,475 --> 00:11:36,700
they should be harvested gradually. 

91
00:11:39,775 --> 00:11:45,370
It is preferable to harvest the primary umbels
because they produce the best seeds.

92
00:11:45,665 --> 00:11:49,520
The secondary umbels are
only harvested if necessary. 

93
00:11:52,505 --> 00:11:56,030
The umbels are cut along with the top of the stalk

94
00:11:56,315 --> 00:11:59,055
when the first mature seeds start to fall.

95
00:11:59,465 --> 00:12:04,890
As they tend to fall to the ground,
they may be cut earlier if the weather is bad. 

96
00:12:20,280 --> 00:12:25,980
In cold regions, the plants along
with their roots can be dug up in September. 

97
00:12:40,025 --> 00:12:45,205
In any case, drying should continue
in a dry and well ventilated place. 

98
00:12:47,545 --> 00:12:51,990
The seeds will continue to mature
slowly during the drying process. 

99
00:12:58,655 --> 00:13:01,515
Extracting – sorting - storing :

100
00:13:07,415 --> 00:13:10,395
The seeds are removed from the umbels by hand.

101
00:13:10,775 --> 00:13:16,095
Gloves are worn because the seeds
are covered with spines or beards.

102
00:13:16,640 --> 00:13:20,600
If you rub them on a sieve
this will debeard the seeds. 

103
00:13:24,780 --> 00:13:29,645
To sort the seeds, you should first
use a sieve that retains the chaff.

104
00:13:35,710 --> 00:13:41,260
Then the seeds are retained by a finer sieve
that allows dust to pass through. 

105
00:13:58,120 --> 00:14:04,290
Finally, they should be winnowed
or you can remove any remaining chaff by hand. 

106
00:14:15,700 --> 00:14:19,960
Always include a label with the name
of the variety and species

107
00:14:20,260 --> 00:14:25,075
as well as the year in the bag as writing
on the outside can be rubbed off. 

108
00:14:29,145 --> 00:14:34,445
Storing the seeds in the freezer
for several days kills certain parasite larvae. 

109
00:14:40,065 --> 00:14:44,095
Carrot seeds are able to germinate
for up to five years.

110
00:14:44,530 --> 00:14:48,475
In certain cases,
this may be extended to ten years.

111
00:14:49,560 --> 00:14:53,680
This can be further prolonged
by storing the seeds in a freezer.

112
00:14:55,555 --> 00:15:01,495
Carrot seeds sometimes remain dormant
during the first three months after the harvest.

113
00:15:04,660 --> 00:15:11,810
One gram of seed with their beards
contains around 700 to 800 seeds. 
