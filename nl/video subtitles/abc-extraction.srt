﻿1
00:00:09,840 --> 00:00:14,960
Extraction, wet processing,
drying and sorting of seeds

2
00:00:23,560 --> 00:00:28,520
The extraction and wet-processing method
with fermentation

3
00:00:34,680 --> 00:00:38,040
Wet-processing with a fermentation process

4
00:00:38,200 --> 00:00:41,240
is used for tomatoes and cucumbers.

5
00:00:41,840 --> 00:00:46,880
The process of fermentation
allows removal of the gelatinous coating

6
00:00:46,960 --> 00:00:49,760
surrounding each seed
that keeps it dormant.

7
00:00:54,480 --> 00:00:58,000
The tomatoes or cucumbers are cut in half.

8
00:00:59,080 --> 00:01:05,680
Their seeds and juice are extracted
with a spoon and placed in a glass jar.

9
00:01:06,680 --> 00:01:09,760
A little water can be added if necessary.

10
00:01:11,480 --> 00:01:15,480
Seeds should not be saved
from damaged or fermented fruit.

11
00:01:17,280 --> 00:01:21,640
Each jar is labeled with the name
of the species and the variety.

12
00:01:23,080 --> 00:01:27,320
The glass jar makes it possible
to observe the fermentation process.

13
00:01:28,000 --> 00:01:30,240
Do not close the jar tightly.

14
00:01:31,240 --> 00:01:36,000
Simply cover it and protect it
from flies with insect netting

15
00:01:36,640 --> 00:01:43,680
and place it in a warm place between
23° and 30° out of direct sunlight.

16
00:01:49,720 --> 00:01:55,240
The time required for fermentation
varies depending on air temperature

17
00:01:55,320 --> 00:01:58,320
and the amount of sugar
in the fermentation liquid.

18
00:02:00,440 --> 00:02:05,160
Little by little, a white layer
of mold forms on the surface

19
00:02:05,640 --> 00:02:07,880
that you should mix in several times

20
00:02:08,200 --> 00:02:10,840
to ensure more consistent fermentation

21
00:02:11,520 --> 00:02:15,160
and to avoid the formation
of a too thick layer of mold.

22
00:02:17,400 --> 00:02:21,960
Adding a pinch of sugar
prevents the growth of harmful molds

23
00:02:22,560 --> 00:02:26,360
and activates this process
whenever there is not enough flesh.

24
00:02:31,800 --> 00:02:35,000
The fermentation process
should be closely observed.

25
00:02:35,720 --> 00:02:39,360
It may take less than 48 hours
on very hot days.

26
00:02:40,080 --> 00:02:45,040
If left too long, the seeds now
lacking their gelatinous coating

27
00:02:45,120 --> 00:02:48,560
will start to germinate
and can no longer be used for seed.

28
00:02:52,480 --> 00:02:55,960
When the seeds break away
and fall to the bottom of the jar

29
00:02:56,080 --> 00:02:59,080
and the rest of the flesh and skin
float to the top,

30
00:02:59,640 --> 00:03:03,720
the gelatinous coating has been destroyed
and the process is complete.

31
00:03:06,240 --> 00:03:07,880
The seeds can now be cleaned.

32
00:03:10,960 --> 00:03:14,800
The seeds retained in the sieve
are cleaned under a jet of water.

33
00:03:28,640 --> 00:03:32,360
The extraction and wet-processing
method without fermentation

34
00:03:35,280 --> 00:03:38,560
Wet-processing
without a fermentation process

35
00:03:39,000 --> 00:03:41,920
is used for vegetables with fruits,

36
00:03:42,040 --> 00:03:44,600
such as eggplants or aubergines,

37
00:03:45,560 --> 00:03:48,280
pumpkins and courgettes or zucchini,

38
00:03:49,120 --> 00:03:50,840
melons and watermelons.

39
00:03:54,640 --> 00:03:56,720
The seeds are removed from the fruit

40
00:03:57,200 --> 00:04:00,480
and washed
under running water in a colander.

41
00:04:03,520 --> 00:04:06,840
If the seeds do not separate easily
from the flesh,

42
00:04:07,360 --> 00:04:11,920
they can be soaked in water
for 12 to 24 hours

43
00:04:12,560 --> 00:04:15,960
until the flesh disintegrates
and the seeds are released.

44
00:04:17,080 --> 00:04:21,760
To avoid fermentation,
they should not be put in a warm place.

45
00:04:22,520 --> 00:04:25,160
The seeds should then
be dried without delay.

46
00:04:34,240 --> 00:04:35,520
Drying

47
00:04:40,200 --> 00:04:43,720
After wet-processing,
the seeds must be dried quickly.

48
00:04:45,720 --> 00:04:48,640
They should be dry
after a maximum of two days.

49
00:04:50,680 --> 00:04:56,320
They are placed on a fine sieve
or a plate in a well-ventilated dry place

50
00:04:56,800 --> 00:05:00,520
with a temperature of between 23° to 30°.

51
00:05:06,720 --> 00:05:09,640
Another method for small amounts of seed

52
00:05:10,080 --> 00:05:13,680
is to put them
on a very absorbent coffee filter

53
00:05:13,960 --> 00:05:15,840
on which they do not stick.

54
00:05:16,600 --> 00:05:21,200
A maximum of one teaspoon of seeds
is placed on each filter.

55
00:05:23,640 --> 00:05:26,800
The name of the variety and species

56
00:05:27,040 --> 00:05:30,360
is written in permanent ink
on each filter.

57
00:05:32,000 --> 00:05:34,520
The filters are hung on a clothes rack

58
00:05:34,600 --> 00:05:38,040
in a warm, dry, well ventilated place.

59
00:05:38,840 --> 00:05:41,560
The seeds
should not be exposed to sunlight,

60
00:05:42,160 --> 00:05:44,280
nor should they be dried on paper

61
00:05:44,600 --> 00:05:46,080
because they will stick together

62
00:05:46,760 --> 00:05:48,840
and it will be difficult to remove them.

63
00:05:51,960 --> 00:05:54,920
Remove the seeds
and rub them between your hands

64
00:05:55,200 --> 00:05:57,120
to separate them from each other.

65
00:06:18,240 --> 00:06:20,040
Sorting seeds

66
00:06:30,680 --> 00:06:33,560
There are different ways
to sort seeds after extraction.

67
00:06:34,720 --> 00:06:37,400
Either wet or dry methods can be used.

68
00:06:41,280 --> 00:06:46,080
Seeds that are not surrounded
by flesh such as leeks and onions

69
00:06:46,520 --> 00:06:48,200
can be sorted using water.

70
00:06:49,800 --> 00:06:54,240
A large amount of water is poured
into a transparent container

71
00:06:54,720 --> 00:06:56,480
and the seeds are dropped inside.

72
00:06:58,880 --> 00:07:01,240
The water is stirred several times

73
00:07:01,760 --> 00:07:06,520
so that the heavy fertile seeds
fall to the bottom of the container.

74
00:07:07,320 --> 00:07:10,720
The seeds that remain on the surface
along with the chaff

75
00:07:11,360 --> 00:07:13,160
are skimmed off with a colander.

76
00:07:18,040 --> 00:07:20,600
The water is then poured through a sieve

77
00:07:20,680 --> 00:07:23,440
to recover the seeds
that have fallen to the bottom.

78
00:07:24,360 --> 00:07:26,280
They must be dried immediately.

79
00:07:28,200 --> 00:07:32,760
Many seeds that are very light
cannot be sorted in this manner.

80
00:07:36,480 --> 00:07:39,880
Dry sorting
is the most commonly used method.

81
00:07:59,600 --> 00:08:02,240
With large seeds that are shelled by hand

82
00:08:02,320 --> 00:08:03,880
such as beans, for example,

83
00:08:04,320 --> 00:08:07,600
you just have to remove
the badly shaped and damaged seeds.

84
00:08:12,840 --> 00:08:16,240
For all other seeds
whose seed heads are beaten or crushed,

85
00:08:17,320 --> 00:08:19,080
the chaff must be removed.

86
00:08:21,720 --> 00:08:24,800
The seeds are first passed
through a very coarse sieve

87
00:08:25,280 --> 00:08:27,760
that retains the largest pieces of chaff;

88
00:08:31,440 --> 00:08:34,560
the seeds and smaller chaff
fall into a bucket.

89
00:08:36,120 --> 00:08:39,320
The process is then repeated
using a fine sieve

90
00:08:39,760 --> 00:08:43,360
that retains the seeds
and lets the chaff pass through.

91
00:08:50,640 --> 00:08:52,800
The choice of the sieve is critical;

92
00:08:53,200 --> 00:08:57,160
it should retain the seeds
and let through as much chaff as possible.

93
00:09:06,240 --> 00:09:07,640
To finish the cleaning,

94
00:09:07,960 --> 00:09:10,360
the seeds are poured into a flat container

95
00:09:10,440 --> 00:09:13,600
and blown on gently
to remove any light chaff.

96
00:09:17,760 --> 00:09:20,160
The wind can also be used
to sort the seeds.

97
00:09:20,520 --> 00:09:22,320
A large sheet is spread on the ground.

98
00:09:23,960 --> 00:09:28,160
The seeds are poured on top of it
and the wind blows away the chaff.

99
00:09:30,000 --> 00:09:32,600
The wind must be regular,
because strong gusts

100
00:09:32,680 --> 00:09:33,880
will blow everything away.

101
00:09:34,480 --> 00:09:36,400
A small fan can also be used.

102
00:09:41,680 --> 00:09:46,080
A small compressor
is also effective with heavy seeds;

103
00:09:46,560 --> 00:09:48,320
all others might be blown away.

104
00:09:54,400 --> 00:09:56,520
No matter what method is used,

105
00:09:56,760 --> 00:09:59,480
a small number of seeds are always lost.

106
00:09:59,800 --> 00:10:04,760
What's important is to know
to what extent you want to sort the seeds.

107
00:10:32,320 --> 00:10:35,600
Nature is very generous,
and when you start to propagate seeds,

108
00:10:35,800 --> 00:10:39,320
you will soon realise that an enormous
amount of seed is produced,

109
00:10:39,880 --> 00:10:41,640
more than you need for your own garden.

110
00:10:43,280 --> 00:10:45,760
Don't absolutely try and save every seed.

111
00:10:46,120 --> 00:10:47,320
There will always be enough.
