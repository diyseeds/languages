1
00:00:08,305 --> 00:00:13,752
Peas belong to the Fabaceae family
and to the Pisum sativum species.

2
00:00:16,450 --> 00:00:17,934
They are annual plants.

3
00:00:18,189 --> 00:00:20,327
There are many varieties of peas :

4
00:00:23,403 --> 00:00:27,461
peas which must be shelled
and whose seeds are round and smooth.

5
00:00:28,443 --> 00:00:32,981
These are hardy varieties that mature
early and resist to cold. 

6
00:00:35,156 --> 00:00:39,018
Peas which must also be shelled
but whose seeds are wrinkled.

7
00:00:39,170 --> 00:00:43,970
They are less suitable for early sowing,
but they are more resistant to hot temperatures. 

8
00:00:48,545 --> 00:00:50,145
Edible podded peas.

9
00:00:50,981 --> 00:00:55,447
You can eat the whole pods when they are young,
before the seeds have developed. 

10
00:00:57,556 --> 00:00:59,460
Among these pea varieties,

11
00:00:59,650 --> 00:01:03,963
there are certain climbing varieties
that grow to over 70 cm.

12
00:01:06,734 --> 00:01:08,290
It is important to stake them. 

13
00:01:14,920 --> 00:01:20,087
There are also dwarf peas
that grow to 45 to 70 cm high.

14
00:01:20,920 --> 00:01:22,880
They don't generally need staking. 

15
00:01:25,560 --> 00:01:30,043
In the Pisum sativum species
there are also fodder peas. 

16
00:01:38,349 --> 00:01:39,425
Pollination 

17
00:01:48,021 --> 00:01:52,580
Pea flowers are hermaphrodite
and self-fertilising,

18
00:01:52,974 --> 00:01:57,636
meaning they have the male and female organs
within the same flower

19
00:01:57,774 --> 00:01:59,650
and that they are compatible. 

20
00:02:00,720 --> 00:02:06,094
Cross pollination by insects may however
occur between different varieties.

21
00:02:11,403 --> 00:02:15,120
The risk depends on the variety
and the environment,

22
00:02:16,196 --> 00:02:18,807
whether there are natural barriers or not. 

23
00:02:22,930 --> 00:02:29,149
To avoid cross-pollination,
grow different varieties of pea 15m apart. 

24
00:02:31,854 --> 00:02:34,756
A few meters are enough
if there is a natural barrier

25
00:02:34,872 --> 00:02:36,538
such as a hedge between them. 

26
00:02:41,825 --> 00:02:45,178
When you wish to grow several varieties
in the same garden

27
00:02:45,350 --> 00:02:48,763
you can isolate one variety under a net cage.

28
00:02:49,527 --> 00:02:53,134
Make sure you put the cage in place
before flowering begins. 

29
00:03:02,734 --> 00:03:04,232
Life cycle 

30
00:03:14,290 --> 00:03:19,105
Peas grown for seeds are grown
in the same way as those for consumption.

31
00:03:23,320 --> 00:03:28,836
Some peas can be sown before the winter,
but usually peas are sown early in the spring.

32
00:03:33,120 --> 00:03:35,230
You should not sow peas too late

33
00:03:35,483 --> 00:03:39,949
as pea flowers cannot be fertilized
at temperatures above 30°c. 

34
00:03:55,723 --> 00:04:01,556
Grow at least 50 plants to ensure
good genetic diversity and a better selection. 

35
00:04:05,956 --> 00:04:11,061
During growth, select the most beautiful,
healthy and productive plants,

36
00:04:11,280 --> 00:04:12,880
and remove the others. 

37
00:04:30,360 --> 00:04:33,880
It is better to reserve part
of the crop for seed production

38
00:04:34,000 --> 00:04:37,410
from which you should not harvest
any pods for consumption.

39
00:04:42,043 --> 00:04:44,414
You should let all seeds mature fully.

40
00:04:46,960 --> 00:04:50,620
In this way you will preserve
the precocity of the variety. 

41
00:04:55,330 --> 00:05:01,214
To harvest the seeds, all you have to do
is let the plants dry in the garden. 

42
00:05:17,410 --> 00:05:20,749
If necessary, you can complete
drying in a shed. 

43
00:05:31,592 --> 00:05:35,156
To make sure that the seeds are dry,
bite one gently.

44
00:05:36,029 --> 00:05:39,869
If this leaves no mark, then they are fully dry. 

45
00:05:50,080 --> 00:05:53,345
Extracting - sorting - storing

46
00:05:56,036 --> 00:05:59,730
The shelling of the pods
can either be done pod by pod,

47
00:06:01,980 --> 00:06:04,269
or you can beat them with a stick

48
00:06:08,734 --> 00:06:09,927
or tread on them. 

49
00:06:22,923 --> 00:06:26,472
After this, remove the large debris
by using a sieve.

50
00:06:30,380 --> 00:06:32,901
The larger waste should be removed by hand

51
00:06:33,294 --> 00:06:36,000
and the smaller chaff will pass
through the sieve. 

52
00:06:39,650 --> 00:06:43,301
You must then winnow the seeds
to remove the very last chaff

53
00:06:43,380 --> 00:06:51,658
by blowing on them yourself or by using
a ventilator or a small air-compressor. 

54
00:07:04,421 --> 00:07:08,392
It is important to put a label
with the name of the variety and species,

55
00:07:08,538 --> 00:07:11,250
as well as the year,
inside the package,

56
00:07:11,381 --> 00:07:14,020
as writing on the outside often rubs off. 

57
00:07:16,240 --> 00:07:21,803
Pea seeds are often infested
by pea weavils (bruchus pisurum),

58
00:07:22,087 --> 00:07:26,040
small insects that lay their eggs
under the skin of seeds

59
00:07:26,312 --> 00:07:28,596
while the plants are growing in the garden. 

60
00:07:29,767 --> 00:07:34,600
An easy way to get rid of them is to leave
the seeds in the freezer for a few days. 

61
00:07:47,960 --> 00:07:51,861
Pea seeds have a germination
capacity of 3 to 8 years.

62
00:07:57,280 --> 00:08:00,618
This can be extended by storing
the seeds in a freezer. 
