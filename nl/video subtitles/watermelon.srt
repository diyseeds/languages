1
00:00:09,015 --> 00:00:13,610
The watermelon is an annual plant
of the Cucurbitaceae family

2
00:00:15,800 --> 00:00:18,680
that belongs to the Citrullus lanatus species.

3
00:00:20,260 --> 00:00:22,590
There are three main types of watermelon,

4
00:00:25,690 --> 00:00:27,370
those with sweet flesh,

5
00:00:29,995 --> 00:00:31,700
those used for jam

6
00:00:34,680 --> 00:00:40,700
and those cultivated in Africa for their seeds
which are rich in oïl and whose flesh is bitter. 

7
00:00:46,420 --> 00:00:47,405
Pollination 

8
00:01:08,380 --> 00:01:11,005
Watermelon is a monoecious plant,

9
00:01:11,105 --> 00:01:15,220
meaning it bears both male
and female flowers on the same plant.

10
00:01:15,915 --> 00:01:18,525
Its flowers bloom only for one day. 

11
00:01:20,950 --> 00:01:24,400
The female flowers have
an ovary below the flower.

12
00:01:25,310 --> 00:01:29,590
It is in fact a mini watermelon
that will develop after pollination. 

13
00:01:31,730 --> 00:01:34,960
The male flowers are at the end of long stems. 

14
00:01:43,815 --> 00:01:46,230
Watermelon can be self-fertilised,

15
00:01:46,360 --> 00:01:52,465
meaning the female flowers can be fertilised
by pollen from a male flower on the same plant. 

16
00:01:58,280 --> 00:02:01,775
Cross-fertilisation is however the most common.

17
00:02:02,725 --> 00:02:07,395
Insects, above all bees,
pollinate watermelon flowers.

18
00:02:12,315 --> 00:02:15,435
All varieties of watermelon
will cross with each other,

19
00:02:15,750 --> 00:02:17,810
including with wild watermelon.

20
00:02:19,200 --> 00:02:24,545
Watermelons cannot however cross
with cucumbers, melons or squashes. 

21
00:02:30,385 --> 00:02:36,095
To avoid crossing,
separate two varieties of watermelon by 1km. 

22
00:02:38,970 --> 00:02:45,535
This distance can be reduced to 400m
if there is a natural barrier such as a hedge. 

23
00:02:50,155 --> 00:02:52,320
There are several methods to produce seeds

24
00:02:52,395 --> 00:02:56,295
from different varieties of watermelon
grown close to each other. 

25
00:02:58,560 --> 00:03:04,365
The first is to cover one variety with a net cage
and to place a bumblebee hive inside. 

26
00:03:13,915 --> 00:03:18,680
The second is to cover two varieties
with different net cages :

27
00:03:19,320 --> 00:03:25,460
open one while the other is closed
on one day, and alternate the next day.

28
00:03:25,805 --> 00:03:27,930
Let the wild insects do their work.

29
00:03:28,280 --> 00:03:33,970
The seed production in this case will be lower
as some flowers will not be pollinated. 

30
00:03:40,865 --> 00:03:44,375
The third method is to pollinate
the flowers manually.

31
00:03:45,960 --> 00:03:48,755
This is not as simple as for squashes or zucchini.

32
00:03:51,665 --> 00:03:58,530
Watermelon flowers are smaller, and it is
also difficult to notice when they blossom.

33
00:04:02,215 --> 00:04:07,320
The success rate with manual
cross-pollination is 50% to 75%.

34
00:04:09,575 --> 00:04:13,180
Whenever pollination does not happen,
the flower withers. 

35
00:04:15,840 --> 00:04:19,310
These three methods
are described in greater detail

36
00:04:19,400 --> 00:04:22,525
in the modules
on mechanical isolation techniques

37
00:04:22,800 --> 00:04:27,065
and on manual pollination
in the ABC of seed production. 

38
00:04:37,475 --> 00:04:38,620
Life cycle 

39
00:04:57,090 --> 00:04:58,980
Watermelons grown for their seeds

40
00:04:59,395 --> 00:05:02,820
are cultivated in the same way
as those for consumption.

41
00:05:12,075 --> 00:05:17,270
Since they come from Africa,
watermelons need heat to germinate and to grow.

42
00:05:24,250 --> 00:05:29,500
It is better to grow at least 6 plants for seeds
to ensure good genetic diversity.

43
00:05:29,645 --> 00:05:31,650
Ideally, grow a dozen. 

44
00:05:56,805 --> 00:05:59,660
Take great care to select the plants
you keep for seeds

45
00:05:59,725 --> 00:06:02,930
according to the specific
characteristics of the variety,

46
00:06:03,350 --> 00:06:10,795
for example its precocity, the number of fruit,
the taste and sugar content. 

47
00:06:14,020 --> 00:06:16,635
Also check its resistance to diseases. 

48
00:06:16,985 --> 00:06:20,970
Keep the plants that are well developed
and get rid of those that are ill. 

49
00:06:24,680 --> 00:06:29,690
The level of maturity
for the watermelon seed is easy to identify :

50
00:06:30,415 --> 00:06:33,860
the fruit must be ripe, ready for consumption. 

51
00:06:45,940 --> 00:06:49,660
Extracting - sorting - storing

52
00:06:56,820 --> 00:06:59,775
To harvest the seeds, open the watermelon,

53
00:07:01,545 --> 00:07:05,230
cut it in slices and remove
the seeds using a knife. 

54
00:07:07,345 --> 00:07:08,495
If there are children around,

55
00:07:08,560 --> 00:07:11,655
they will know exactly
how to remove the remaining seeds. 

56
00:07:13,840 --> 00:07:16,240
Simply rinse the seeds under water. 

57
00:07:20,320 --> 00:07:25,260
To get rid of empty sterile seeds,
put the seeds in a container full of water.

58
00:07:27,560 --> 00:07:33,140
The fertile seeds will fall to the bottom,
while the empty ones will float on the surface. 

59
00:07:59,430 --> 00:08:01,090
Leave them to dry in the shade. 

60
00:08:03,300 --> 00:08:07,750
To check if the seeds are dry,
your finger nail should not leave any trace. 

61
00:08:11,755 --> 00:08:15,230
Always put a label with the name
of the variety and species

62
00:08:15,345 --> 00:08:20,960
as well as the year inside the sachet,
as writing on the outside often rubs off. 

63
00:08:25,655 --> 00:08:29,040
A few days in the freezer will kill parasites. 

64
00:08:32,540 --> 00:08:36,940
Watermelon seeds have an average
germination capacity of 5 years,

65
00:08:37,290 --> 00:08:39,115
but it can last for 10 years.

66
00:08:43,655 --> 00:08:47,130
You can increase this by storing
the seeds in a freezer. 
