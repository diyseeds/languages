# Copyright (C) 2021 Maxime Lecoq
# This file is distributed under the GNU General Public License v2 or later.
msgid ""
msgstr ""
"Project-Id-Version: Diyseeds 2.0\n"
"POT-Creation-Date: 2021-07-10T00:00:00+02:00\n"
"PO-Revision-Date: 2022-06-05 17:15+0000\n"
"Last-Translator: Erik D'haese <fretjex@hotmail.com>\n"
"Language-Team: Dutch <http://translate.diyseeds.org/projects/"
"diyseeds-org-organizations/seed-savers-exchange/nl/>\n"
"Language: nl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 4.8\n"
"WPOT-Type: post-types/organization\n"
"X-Domain: wpot\n"
"WPOT-Origin: seed-savers-exchange\n"

#. This title will be displayed into the browser's window title bar.
msgctxt "browser-title"
msgid "Seed Savers Exchange"
msgstr "Seed savers exchange"

#. The slug is the last part of the URL (www.example.org/slug-is-here). Only alphanumerical characters are allowed, use dash to replace space. The slug should be short and nice and it must reflect the content it refers to. It is usually made from the title. If you don't provide it, a slug will be auto-generated.
msgctxt "slug"
msgid "seed-savers-exchange"
msgstr "seed-savers-exchange"

msgctxt "name"
msgid "Seed Savers Exchange"
msgstr "Seed Savers Exchange"

msgctxt "description"
msgid "Seed Savers Exchange is a nonprofit organization dedicated to the preservation of heirloom seeds."
msgstr ""
"Seed Savers Exchange is een non-profit organisatie die zich inzet voor de "
"instandhouding van landrassen in de Verenigde Staten."

#. This title will be displayed by search engines like Google. 60 characters max.
msgctxt "seo-title"
msgid "the Seed Savers Exchange organization"
msgstr "De organisatie Seed Savers Exchange"

#. This description will be displayed below the title on search engine results. 160 characters max.
msgctxt "seo-description"
msgid "Nonprofit organization dedicated to the preservation of heirloom seeds."
msgstr ""
"Non-profit organisatie die zich inzet voor de instandhouding van landrassen."

#. The tags are used by search engines to reference the content. Use commas to separate tags.
msgctxt "seo-tags"
msgid "organization, non-profit, seeds, preservation"
msgstr ""
"organisatie, USA, Verenigde Staten, landras, landrassen, heirloom, "
"traditionele, rassen, soorten, zaadteelt, zadenteelt, zaaigoedproductie, "
"zaaigoed, zaden, zaad, Seed Saver, Seed Savers, uitwisseling, exchange, "
"instandhouding, bewaring, oude rassen, zonder winstbejag, non-profit, non "
"profit, netwerk"
