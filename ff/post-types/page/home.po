# Copyright (C) 2021 Maxime Lecoq
# This file is distributed under the GNU General Public License v2 or later.
msgid ""
msgstr ""
"Project-Id-Version: Diyseeds 2.0\n"
"POT-Creation-Date: 2021-07-10T00:00:00+02:00\n"
"PO-Revision-Date: 2021-08-10 07:56+0000\n"
"Language: ff\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"
"X-Generator: Weblate 4.5.1\n"
"WPOT-Type: post-types/page\n"
"X-Domain: wpot\n"
"WPOT-Origin: home\n"

#. This title will be displayed into the browser's window title bar.
msgctxt "browser-title"
msgid "Home - Diyseeds"
msgstr ""

#. The slug is the last part of the URL (www.example.org/slug-is-here). Only alphanumerical characters are allowed, use dash to replace space. The slug should be short and nice and it must reflect the content it refers to. It is usually made from the title. If you don't provide it, a slug will be auto-generated.
msgctxt "slug"
msgid "home"
msgstr ""

#. Title of a page's section.
msgctxt "section-title"
msgid "From seed to seed: produce your own seeds"
msgstr ""

msgid "This site presents the 40 short educational films initially published in the set of 4 DVDs, “From seed to seed”. With the help of magnificent images and animated drawings these videos show you, step by step, how to produce the seeds of 40 different vegetables. In addition, eight theoretical or practical ABCs explain the general basics of seed production. These films were produced by Longo maï and the European Civic Forum, two associations which have been involved for many years in the defence and promotion of open-pollinated seeds."
msgstr ""

#. Title of a page's section.
msgctxt "section-title"
msgid "Why produce your own seeds?"
msgstr ""

msgid "By multiplying our own seeds, we participate in the dynamics of the biodiversity to which we belong as living beings. Each reproducible seed contributes to ensuring the continuity of life on earth. This is all the more essential as today farmers and gardeners are still far too dependent on large seed companies."
msgstr ""

#. Title of a page's section.
msgctxt "section-title"
msgid "How to save seeds?"
msgstr ""

msgid "We've created this website to answer all your questions about seed production. Reclaiming this know-how is vital. How to multiply tomato, salad, bean, zucchini, parsnip, cabbage or cardoon seeds? What is pollination? What are the life cycle and botanical classification of the plants we grow in the garden?"
msgstr ""

msgid "To help you discover this forgotten knowledge, take the time to view these practical videos that explain how to produce the seeds of over thirty vegetables. These films are real educational tools and can also be ordered as a set of four DVDs in several languages. Various resources can be downloaded, such as the texts of the films, the botanical trees or the videos themselves."
msgstr ""

#. Title of a page's section.
msgctxt "section-title"
msgid "Where can I find open-pollinated seeds?"
msgstr ""

msgid "To sow your own vegetable seeds each year, it is preferable to carefully select your seeds: organic and open-pollinated. This should normally be the definition of a seed, yet most of the seeds sold by major seed companies are patented F1 hybrids and therefore not reproducible."
msgstr ""

msgid "You can find seeds of old, hardy and open-pollinated varieties from specialist suppliers. There are probably some in your country, otherwise an internet order is still possible. Consult our page dedicated to this subject."
msgstr ""

msgid "It is also possible to exchange seeds in seed libraries or seed exchanges. You can also swap seeds with your neighbours. If no seed exchange exists near you, why not start one?"
msgstr ""

msgctxt "button"
msgid "How can I find seed suppliers?"
msgstr ""

msgctxt "button"
msgid "With which seeds should I start?"
msgstr ""

#. Title of a page's section.
msgctxt "section-title"
msgid "Is it legal to reproduce your own seeds?"
msgstr ""

msgid "In Europe extremely restrictive legislation obliges seed producers to register their varieties in an official catalogue which imposes very strict selection criteria. In France, for example, seeds authorized for sale must be listed in the official SEMAE (formerly GNIS) catalogue. This system only reinforces the domination and near-monopoly of large seed companies like Bayer/Monsanto. Failure to comply with this rule has, for example, resulted in years of lawsuits against the Kokopelli association which markets seeds produced by a network of seed producers, most of which are not listed in the official catalogue."
msgstr ""

msgctxt "button"
msgid "Learn more about the legislation"
msgstr ""

#. This title will be displayed by search engines like Google. 60 characters max.
msgctxt "seo-title"
msgid "Diyseeds or how to learn to produce your own seeds"
msgstr ""

#. This description will be displayed below the title on search engine results. 160 characters max.
msgctxt "seo-description"
msgid "Take a look at our free video tutorials, articles and other information on seeds. A DVD set is available, don't forget to subscribe to our newsletter!"
msgstr ""

#. The tags are used by search engines to reference the content. Use commas to separate tags.
msgctxt "seo-tags"
msgid "heritage seeds, learn, tutorial, video, DVD, From seed to seed"
msgstr ""
