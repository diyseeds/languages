# Copyright (C) 2021 Maxime Lecoq
# This file is distributed under the GNU General Public License v2 or later.
msgid ""
msgstr ""
"Project-Id-Version: Diyseeds 2.0\n"
"POT-Creation-Date: 2021-07-10T00:00:00+02:00\n"
"PO-Revision-Date: 2021-08-13 09:28+0000\n"
"Language: ff\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"
"X-Generator: Weblate 4.5.1\n"
"WPOT-Type: post-types/organization\n"
"WPOT-Origin: kokopelli\n"
"X-Domain: wpot\n"

#. This title will be displayed into the browser's window title bar.
msgctxt "browser-title"
msgid "Kokopelli"
msgstr ""

#. The slug is the last part of the URL (www.example.org/slug-is-here). Only alphanumerical characters are allowed, use dash to replace space. The slug should be short and nice and it must reflect the content it refers to. It is usually made from the title. If you don't provide it, a slug will be auto-generated.
msgctxt "slug"
msgid "kokopelli"
msgstr ""

msgctxt "name"
msgid "Kokopelli"
msgstr ""

msgctxt "description"
msgid "Association dedicated to the defence of biodiversity and humus. They sell seeds of over 2000 varieties of vegetables, aromatic plants, cereals, flowers and medicinal plants, all of which can be multiplied and are free of intellectual property rights."
msgstr ""

#. This title will be displayed by search engines like Google. 60 characters max.
msgctxt "seo-title"
msgid "The Kokopelli association"
msgstr ""

#. This description will be displayed below the title on search engine results. 160 characters max.
msgctxt "seo-description"
msgid "Association dedicated to the defence of biodiversity and humus. It sells seeds of more than 2,000 varieties of vegetables and other plants."
msgstr ""

#. The tags are used by search engines to reference the content. Use commas to separate tags.
msgctxt "seo-tags"
msgid "association, biodiversity, seeds, production, vegetables, aromatic plants, flowers, medicinal plants, open-pollinated"
msgstr ""
