1
00:00:11,520 --> 00:00:16,996
Corn is a member of the Poaceae family,
like the majority of cereal crops.

2
00:00:17,160 --> 00:00:19,774
It is part of the Zea mays species.

3
00:00:21,716 --> 00:00:23,643
There are several types of corn:

4
00:00:24,650 --> 00:00:27,912
- Sweet corn, Zea mays zaccharata

5
00:00:28,952 --> 00:00:34,630
which can be eaten raw when the cobs are immature,
boiled in water or grilled.

6
00:00:36,960 --> 00:00:39,520
Its seeds shrink as they dry.

7
00:00:46,000 --> 00:00:48,618
This type can be grown in cool regions. 

8
00:00:53,280 --> 00:00:57,156
- Dent corn, Zea mays indentata

9
00:00:57,640 --> 00:01:04,094
whose seed is composed of a starchy core
surrounded by a harder vitreous layer.

10
00:01:05,690 --> 00:01:09,323
Its starchy centre retracts at maturity,

11
00:01:09,680 --> 00:01:13,505
producing an incisive shape
that gives it its name.

12
00:01:15,050 --> 00:01:20,647
It is a variety used to make flour,
also used as animal feed. 

13
00:01:31,992 --> 00:01:35,476
- Flint corn, Zea mays indurata

14
00:01:37,934 --> 00:01:43,178
which in contrast to dent corn
has a seed with a very small starchy core

15
00:01:43,629 --> 00:01:50,000
and a more substantial vitreous layer,
which is why it is mainly used for polenta. 

16
00:01:56,494 --> 00:02:05,614
- Popcorn,  Zea mays microsperma or everta
whose seeds burst open when cooked. 

17
00:02:22,100 --> 00:02:23,100
Pollination 

18
00:02:29,185 --> 00:02:30,836
Corn is monoecious,

19
00:02:31,200 --> 00:02:35,192
which means there are male
and female flowers on the same plant.

20
00:02:37,832 --> 00:02:42,603
The male flower or tassel is located
at the top of the stem

21
00:02:44,450 --> 00:02:48,116
and the female flower
is found in the middle of the stem,

22
00:02:49,090 --> 00:02:56,014
the swollen part containing the future cob
from which only the cornsilk emerges. 

23
00:03:00,392 --> 00:03:03,301
Each silk is connected to an ovule,

24
00:03:03,694 --> 00:03:08,152
which after pollination will produce
one kernel in the cob. 

25
00:03:12,080 --> 00:03:17,083
Corn is allogamous, which means
that one plant fertilizes another.

26
00:03:18,552 --> 00:03:23,585
It is also anemophilous,
meaning that it is pollinated by the wind,

27
00:03:23,949 --> 00:03:27,963
which transports pollen distances of over 10 km. 

28
00:03:32,618 --> 00:03:36,283
Corn should be planted in groups
with at least 3 rows

29
00:03:36,370 --> 00:03:39,890
so that the wind can properly
transport the pollen.

30
00:03:41,367 --> 00:03:43,130
If there is only one row,

31
00:03:43,345 --> 00:03:48,610
pollination will be poor and the cobs
will not be fully developed when harvested. 

32
00:03:51,149 --> 00:03:55,352
In certain regions, corn can also
be visited by bees

33
00:03:55,440 --> 00:03:58,843
attracted by the impressive amount of pollen
that is produced.

34
00:04:00,349 --> 00:04:05,287
One plant can produce up
to 18 million pollen grains! 

35
00:04:10,749 --> 00:04:17,214
To avoid cross-pollination,
plant two varieties of corn at least 3 km apart. 

36
00:04:19,927 --> 00:04:26,378
This distance can be reduced to 1 km
if there is a natural barrier such as a hedge. 

37
00:04:29,127 --> 00:04:32,058
You can also use the time isolation method.

38
00:04:33,236 --> 00:04:39,643
Two varieties of corn are sown in the same garden
with an interval of several weeks between them.

39
00:04:40,923 --> 00:04:45,287
The goal is to avoid that the male flowers
from the first variety

40
00:04:45,730 --> 00:04:50,385
release their pollen when the female flowers
of the second variety appear.

41
00:04:51,701 --> 00:04:55,461
Otherwise, the varieties
would inevitably cross-pollinate. 

42
00:04:56,858 --> 00:05:01,745
For this technique, take into account
the length of the growing cycle,

43
00:05:02,276 --> 00:05:07,740
which ranges from 55 to 120 days,
depending on the variety. 

44
00:05:09,781 --> 00:05:12,574
It is nearly impossible to protect your variety

45
00:05:12,850 --> 00:05:17,607
from large industrial crops
of hybrid corn grown nearby. 

46
00:05:19,389 --> 00:05:24,778
In this specific case, hand pollination
is a solution for seed production.

47
00:05:26,378 --> 00:05:31,498
With this method, the rows should be further
apart so there is room to walk between them. 

48
00:05:34,720 --> 00:05:38,792
Sturdy paper bags resistant
to the rain are required. 

49
00:05:42,414 --> 00:05:46,450
Corn plants blossom for 10 to 14 days.

50
00:05:48,814 --> 00:05:52,472
The hand pollination process takes three days. 

51
00:05:55,541 --> 00:05:59,825
The first day is devoted to bagging
the female flowers.

52
00:06:01,640 --> 00:06:06,167
You should do this just before
the silk emerges from the small cobs;

53
00:06:07,338 --> 00:06:10,450
if it has already emerged before
the flower is bagged,

54
00:06:10,610 --> 00:06:11,723
it's too late! 

55
00:06:14,596 --> 00:06:18,850
First of all, the tips of the husk leaves
around the small cob

56
00:06:18,980 --> 00:06:22,356
are cut off to expose the emerging silks. 

57
00:06:31,498 --> 00:06:36,480
The cob can then be covered
and the bag firmly attached at its base. 

58
00:06:42,232 --> 00:06:45,450
The male flowers are bagged
the morning of the third day

59
00:06:45,636 --> 00:06:48,203
when the anthers, the male organs,

60
00:06:48,400 --> 00:06:52,363
begin to emerge from the vertical
and lateral stems of the stalk. 

61
00:06:58,530 --> 00:07:02,625
If the anthers are still green,
bagging may stop their development. 

62
00:07:05,301 --> 00:07:09,830
Before bagging, shake the plants
to remove pollen from other varieties

63
00:07:09,963 --> 00:07:13,229
that may have been deposited by bees or the wind. 

64
00:07:29,120 --> 00:07:34,349
The bag should be attached so that it collects
the pollen released during the morning. 

65
00:07:49,476 --> 00:07:53,956
Most pollen is released after the dew
has dried and before noon. 

66
00:07:57,280 --> 00:08:00,181
Tapping the flowers a few times
helps make it fall. 

67
00:08:09,410 --> 00:08:14,196
Hand pollination is done around noon
on the same day because in the afternoon,

68
00:08:14,298 --> 00:08:19,141
the pollen in the bag may heat up
too much and no longer be viable. 

69
00:08:20,523 --> 00:08:23,745
At the end of the morning before
the hottest part of the day,

70
00:08:24,247 --> 00:08:27,541
the different bags for collecting
pollen are opened,

71
00:08:37,970 --> 00:08:39,620
the pollen is mixed together

72
00:08:41,512 --> 00:08:44,596
and then the bag covering a cob is opened.

73
00:08:44,741 --> 00:08:50,145
The silk should have grown about
3 to 4 cm within two days. 

74
00:08:50,480 --> 00:08:56,581
The pollen is applied with a brush to all
of the exposed silk along its entire length.

75
00:08:57,534 --> 00:09:00,930
A teaspoon of pollen per cob is needed. 

76
00:09:14,240 --> 00:09:19,360
The bag is immediately closed around
the cob leaving enough space for it to develop. 

77
00:09:20,901 --> 00:09:23,912
The silk is receptive to pollen
for several weeks.

78
00:09:24,580 --> 00:09:28,305
Thus the cobs are left in the bags until harvest. 

79
00:09:40,210 --> 00:09:41,460
Life cycle

80
00:10:00,900 --> 00:10:05,490
Corn is an annual plant
that produces cobs within one year. 

81
00:10:07,389 --> 00:10:11,207
It is grown for seed
in the same way as for consumption.

82
00:10:14,458 --> 00:10:20,101
In general, you should make sure that you choose
a corn variety adapted to your environment.

83
00:10:26,574 --> 00:10:32,167
A minimum of 50 plants are necessary
to maintain good genetic diversity;

84
00:10:33,090 --> 00:10:35,563
it is best to grow 200 plants. 

85
00:10:48,720 --> 00:10:53,556
Choose plants that have developed correctly
and meet the selection criteria:

86
00:10:53,890 --> 00:10:57,580
size, colour,

87
00:10:58,283 --> 00:11:01,020
vigour, precocity,

88
00:11:01,860 --> 00:11:05,540
cob size
and how well it is enveloped. 

89
00:11:18,370 --> 00:11:20,618
The cobs can dry on the plant. 

90
00:11:23,658 --> 00:11:28,145
Corn is ripe when a fingernail
can no longer be pressed into the kernel. 

91
00:11:32,581 --> 00:11:35,316
The cobs can then be picked from the stalk. 

92
00:11:41,900 --> 00:11:45,796
The husks are pulled back up to uncover the cobs

93
00:11:46,203 --> 00:11:49,730
and they are stored in a dry,
well-ventilated place. 

94
00:12:10,036 --> 00:12:15,025
The entire plant can also be cut
and left to dry in a shed. 

95
00:12:26,480 --> 00:12:29,956
Extracting – sorting – storing

96
00:12:34,436 --> 00:12:38,880
Cobs are selected for the shape
of their kernels, their colour,

97
00:12:39,018 --> 00:12:43,774
the number of rows of kernels,
their structure and texture. 

98
00:12:51,230 --> 00:12:53,709
Kernels that will be used for seed

99
00:12:53,963 --> 00:12:59,003
should be chosen from a large number
of cobs to maintain genetic diversity.

100
00:13:20,930 --> 00:13:24,596
It is also recommended to take
the kernels from the middle of the cob. 

101
00:13:31,294 --> 00:13:36,269
Remove the seeds by rubbing the cobs;
gloves should be worn. 

102
00:13:48,887 --> 00:13:51,781
Always put a label with the name of the variety,

103
00:13:51,898 --> 00:13:58,570
the species and the year inside the bag,
as writing on the outside is often rubbed off. 

104
00:14:02,460 --> 00:14:07,170
Storing the seeds in the freezer
for several days kills parasite larvae. 

105
00:14:09,447 --> 00:14:16,589
Popcorn, dent corn and flint corn seeds
are able to germinate for up to five years.

106
00:14:17,643 --> 00:14:21,883
In certain cases,
this may be extended to ten years. 

107
00:14:22,560 --> 00:14:27,207
Sweet corn seeds are able to germinate
for up to three years. 

108
00:14:28,230 --> 00:14:31,687
This may be extended
when they are stored in the freezer. 
