1
00:00:09,500 --> 00:00:12,205
Manual Pollination of Cucurbitaceae

2
00:00:13,815 --> 00:00:18,140
The flowers of Cucurbitaceae
can be pollinated by hand

3
00:00:18,460 --> 00:00:21,140
to prevent unwanted cross-pollination.

4
00:00:21,970 --> 00:00:24,825
The advantages of this method
are its simplicity

5
00:00:25,380 --> 00:00:28,740
and the fact that it enables you to grow
several different varieties

6
00:00:28,860 --> 00:00:31,220
of the same species in the same garden

7
00:00:31,620 --> 00:00:33,380
without having to isolate them.

8
00:00:37,780 --> 00:00:42,940
Let us first look at manual pollination
of flowers of larger Cucurbitaceae. 

9
00:00:45,200 --> 00:00:48,300
The evening before you carry out
manual pollination,

10
00:00:48,900 --> 00:00:51,860
you should identify female and male flowers

11
00:00:52,140 --> 00:00:55,410
of the same variety that are about to open.

12
00:01:02,140 --> 00:01:06,700
At this time, they are a characteristic
creamy yellow colour

13
00:01:07,020 --> 00:01:08,540
and have not yet opened.

14
00:01:10,740 --> 00:01:13,180
The flowers only bloom for one day. 

15
00:01:16,140 --> 00:01:19,140
Female flowers have
a small baby fruit on their stem

16
00:01:19,500 --> 00:01:21,340
that has not started to develop. 

17
00:01:23,180 --> 00:01:26,820
Male flowers have a slender stem
that does not bulge out. 

18
00:01:28,835 --> 00:01:32,620
The female flowers that you have chosen
on several plants

19
00:01:32,940 --> 00:01:40,425
are closed using a clothespin, adhesive paper
tape or a small piece of string

20
00:01:40,900 --> 00:01:44,420
loosely tied to avoid damaging the flowers.

21
00:01:46,660 --> 00:01:49,260
To ensure good genetic diversity,

22
00:01:50,200 --> 00:01:55,460
for every female flower chosen
three male flowers on different plants

23
00:01:55,560 --> 00:01:57,180
are closed in the same way.

24
00:01:59,220 --> 00:02:04,020
This prevents insects from entering the flowers
early in the morning.

25
00:02:07,565 --> 00:02:11,940
To make it easier to find
the selected flowers on the next morning,

26
00:02:12,460 --> 00:02:14,660
they can be marked with stakes. 

27
00:02:18,850 --> 00:02:22,460
The next morning, the male flowers are removed

28
00:02:23,030 --> 00:02:25,420
and applied to the female flowers

29
00:02:25,740 --> 00:02:27,820
that were chosen the previous evening.

30
00:02:29,515 --> 00:02:33,045
It is necessary to start well
before the heat of the day.

31
00:02:33,940 --> 00:02:38,265
Otherwise, the pollen will ferment,
it will no longer be fertile,

32
00:02:38,380 --> 00:02:40,135
and the flowers will close up again. 

33
00:02:44,925 --> 00:02:48,980
The petals of the three male flowers
are carefully torn off.

34
00:02:50,170 --> 00:02:52,220
The female flower is unclipped

35
00:02:52,500 --> 00:02:54,425
and opened gently

36
00:03:00,025 --> 00:03:05,940
and the pollen of the three male flowers
is applied to the stigma of the female flower. 

37
00:03:10,535 --> 00:03:14,060
If a bee has the bad idea of coming
and collecting the nectar

38
00:03:14,285 --> 00:03:17,540
from the female flower
that is currently being pollinated,

39
00:03:17,860 --> 00:03:19,460
the flower should be removed.

40
00:03:21,270 --> 00:03:24,480
Bees carry a mixture of pollen from other plants

41
00:03:24,710 --> 00:03:27,340
and cross-pollination would be inevitable. 

42
00:03:38,385 --> 00:03:40,980
Once manual pollination is complete,

43
00:03:41,360 --> 00:03:44,100
the female flowers are gently closed

44
00:03:44,635 --> 00:03:49,020
and they are tied shut again
as they were the previous evening.

45
00:03:52,350 --> 00:03:55,620
To find the manually pollinated
fruit at harvest time,

46
00:03:56,020 --> 00:04:00,620
the peduncle is marked with a label
or a coloured piece of string. 

47
00:04:05,380 --> 00:04:08,060
Manual pollination of Cucurbitaceae

48
00:04:08,460 --> 00:04:14,380
whose flowers are far smaller requires
much more meticulous care and patience.

49
00:04:17,935 --> 00:04:23,660
With melons, where 80% of the female
flowers abort spontaneously,

50
00:04:24,300 --> 00:04:27,940
manual pollination
is less efficient than insects.

51
00:04:29,270 --> 00:04:33,540
A success rate of no more
than 10-15% can be expected.

52
00:04:35,060 --> 00:04:43,740
In contrast, the success rate of manual
pollination of watermelons is 50-75%.

53
00:04:45,460 --> 00:04:48,300
The rate for cucumbers is 85%. 

54
00:04:51,130 --> 00:04:57,680
Other kinds of plants such as corn and sunflower
require a slightly different method.

55
00:04:58,805 --> 00:05:01,900
Refer to the modules on each specific vegetable. 
