﻿1
00:00:08,650 --> 00:00:10,380
Isolation techniques

2
00:00:17,140 --> 00:00:19,620
Whenever there is a risk of cross-pollination

3
00:00:19,685 --> 00:00:22,620
between different varieties
of the same species,

4
00:00:23,420 --> 00:00:25,700
it is necessary to isolate them

5
00:00:25,850 --> 00:00:30,100
in order to preserve the specific
characteristics of each variety.

6
00:00:30,690 --> 00:00:33,750
There are several techniques
for isolating varieties.

7
00:00:39,485 --> 00:00:41,180
Spatial isolation 

8
00:00:45,240 --> 00:00:51,820
The simplest technique is to cultivate
only one variety of each species in one plot.

9
00:00:52,820 --> 00:00:56,300
Another variety is grown
in a plot far enough away

10
00:00:56,490 --> 00:01:00,955
to ensure that cross-pollination
does not occur between the varieties.

11
00:01:02,045 --> 00:01:05,230
The distances are indicated for each vegetable.

12
00:01:06,670 --> 00:01:11,100
They also depend on environmental factors
and the type of pollination.

13
00:01:13,750 --> 00:01:18,580
If insects are the pollinators,
a few hundred meters should be sufficient. 

14
00:01:20,780 --> 00:01:24,020
On the other hand,
in cases of pollination by wind

15
00:01:24,500 --> 00:01:29,020
it is necessary to ensure
a much larger distance between varieties. 

16
00:01:29,325 --> 00:01:33,420
A very thick hedge
can greatly reduce the risk of pollen

17
00:01:33,780 --> 00:01:36,660
being transported by the wind and insects. 

18
00:01:37,820 --> 00:01:43,020
The presence of many flowers in the immediate
surroundings of the plants grown for seed

19
00:01:43,140 --> 00:01:48,540
will attract insects and divert them
from the flowers of other plants grown for seed. 

20
00:01:59,600 --> 00:02:01,140
Time isolation 

21
00:02:01,880 --> 00:02:07,940
The periods of culture of two varieties
of the same vegetable can be staggered over time.

22
00:02:08,770 --> 00:02:11,660
As the plants will not blossom at the same time,

23
00:02:11,940 --> 00:02:14,220
cross-pollination becomes impossible.

24
00:02:20,090 --> 00:02:25,300
For example, two varieties of lettuce
can be grown in the same plot;

25
00:02:25,880 --> 00:02:31,725
one sown much earlier will start to flower
while the other is still only a young plant.

26
00:02:32,940 --> 00:02:38,915
It is of course important to know the cycle
of the plant well as well as its blossoming time. 

27
00:02:41,975 --> 00:02:43,820
Mechanical isolation 

28
00:02:45,550 --> 00:02:50,960
Insect netting is an invaluable aid
for producing seeds in total security.

29
00:02:52,560 --> 00:02:58,220
Very fine netting should be chosen according
to the insects and to your objective:

30
00:02:59,045 --> 00:03:04,620
either preventing insects
from getting in or keeping them inside.

31
00:03:07,320 --> 00:03:09,975
These nets can be used in several ways. 

32
00:03:11,670 --> 00:03:14,035
The technique of covering one flower

33
00:03:14,390 --> 00:03:18,340
is reserved for hermaphrodite
and self-fertilizing flowers

34
00:03:18,795 --> 00:03:21,230
to produce very small quantities of seed.

35
00:03:22,295 --> 00:03:26,700
The unopened flower
is surrounded by a small net sleeve

36
00:03:26,910 --> 00:03:30,340
that is tied on so that no insect can enter.

37
00:03:34,585 --> 00:03:37,700
The sleeve should be removed
when the flower has withered

38
00:03:37,830 --> 00:03:40,590
so that the vegetable can develop undisturbed.

39
00:03:42,075 --> 00:03:46,050
Do not forget to label the fruit
that is being reserved for seed. 

40
00:03:55,890 --> 00:04:02,585
This method can also be used with large sleeves
that surround a branch or even the entire plant.

41
00:04:04,800 --> 00:04:09,260
Beforehand, flowers that are
already open must be removed

42
00:04:09,730 --> 00:04:14,060
because there is a risk that they have
already been pollinated by insects. 

43
00:04:19,900 --> 00:04:22,780
A system of net cages or net tunnels

44
00:04:22,860 --> 00:04:26,380
is very effective for producing
a larger amount of seeds. 

45
00:05:29,540 --> 00:05:32,180
Hermaphrodite and self-fertilizing plants

46
00:05:32,320 --> 00:05:35,940
can be grouped together
under the same mosquito net tunnel

47
00:05:36,060 --> 00:05:37,620
to protect them from insects. 

48
00:05:40,540 --> 00:05:45,700
For allogamous plants that require
the presence of insects to be pollinated,

49
00:05:46,340 --> 00:05:51,820
hives of bumblebees are inserted
into the tunnel to pollinate the plants.

50
00:05:54,610 --> 00:05:58,620
In this case, only one variety
per species can be grown.

51
00:05:59,085 --> 00:06:02,020
To make the cost of the hive pay off,

52
00:06:02,230 --> 00:06:06,420
it is preferable to have a large tunnel
with many seed-bearing plants. 

53
00:06:08,050 --> 00:06:10,540
An alternating system of net cages

54
00:06:10,780 --> 00:06:14,740
can also be used when
two varieties of allogamous plants

55
00:06:14,900 --> 00:06:18,980
from the same species are grown
for seed in the same garden,

56
00:06:19,940 --> 00:06:23,220
for example
two different varieties of zucchini.

57
00:06:24,120 --> 00:06:25,900
Without a bumblebee hive,

58
00:06:26,350 --> 00:06:30,980
one variety of a species
can be grown under one net tunnel

59
00:06:31,580 --> 00:06:36,520
and another variety of the same species
can be grown under another net tunnel.

60
00:06:38,570 --> 00:06:40,060
When the flowers open up,

61
00:06:40,720 --> 00:06:43,780
each tunnel can be opened every other day.

62
00:06:44,880 --> 00:06:51,610
On the first day, tunnel number one
is opened and tunnel number two is closed.

63
00:06:52,810 --> 00:06:55,335
On the next day, the opposite is done.

64
00:06:56,990 --> 00:06:59,800
The tunnels should be opened
early in the morning

65
00:07:00,190 --> 00:07:02,860
because insects start their work
early in summer

66
00:07:03,225 --> 00:07:07,380
and closed late when no more noise
can be heard among the plants.

67
00:07:08,575 --> 00:07:09,740
With this method,

68
00:07:10,030 --> 00:07:12,835
the number of female flowers
that are pollinated

69
00:07:13,030 --> 00:07:15,805
and thus the number of fruits decrease.

70
00:07:17,140 --> 00:07:19,900
Nevertheless, nature is very generous

71
00:07:20,215 --> 00:07:23,980
and the gardener will collect
plenty of seeds for the years to come. 
