# Copyright (C) 2021 Maxime Lecoq
# This file is distributed under the GNU General Public License v2 or later.
msgid ""
msgstr ""
"Project-Id-Version: Diyseeds 2.0\n"
"POT-Creation-Date: 2021-07-10T00:00:00+02:00\n"
"PO-Revision-Date: 2022-02-09 09:14+0000\n"
"Last-Translator: Olya Zubyk <olya.zubyk@gmail.com>\n"
"Language-Team: Russian <http://translate.diyseeds.org/projects/"
"diyseeds-org-videos/pea/ru/>\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n%10==1 && n%100!=11 ? 0 : n%10>=2 && n"
"%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Weblate 4.8\n"
"WPOT-Type: post-types/video\n"
"X-Domain: wpot\n"
"WPOT-Origin: pea\n"

msgid "Peas belong to the Fabaceae family and to the Pisum sativum species. They are annual plants. There are many varieties of peas:"
msgstr ""
"Горох относится к семейству Fabaceae и виду Pisum sativum. Это однолетние "
"растения. Существует множество сортов гороха:"

msgid "Peas which must be shelled and whose seeds are round and smooth. These are hardy varieties that mature early and resist to cold."
msgstr ""
"Лущильный горох, который должен быть очищен от стручка и горошины которого "
"круглые и гладкие. Это выносливые сорта, которые рано созревают и устойчивые "
"к холодам."

msgid "Peas which must also be shelled but whose seeds are wrinkled. They are less suitable for early sowing, but they are more resistant to hot temperatures."
msgstr ""
"Мозговой горох, который также должен быть очищен, но горошины которого "
"сморщенные. Он менее пригоден для раннего посева, но более устойчив к жаре."

msgid "Edible podded peas. You can eat the whole pods when they are young, before the seeds have developed."
msgstr ""
"Стручковый (или сахарный) горошек, целые молодые стручки которого "
"потребляются до развития зерен."

msgid "Among these pea varieties, there are certain climbing varieties that grow to over 70 centimeters. It is important to stake them."
msgstr ""
"Среди лущильного и стручкового горошка есть сорта со стеблем высотой более "
"70 см. Они обязательно должны иметь опору."

msgid "There are also dwarf peas that grow to 45 to 70 centimeters high. They don't generally need staking."
msgstr ""
"И карликовые сорта высотой от 45 до 70 см, которые обычно не требуют опоры."

msgid "In the Pisum sativum species there are also fodder peas."
msgstr "К виду Pisum sativum также принадлежит кормовой горох."

msgid "Pea flowers are hermaphrodite and self-fertilising, meaning they have the male and female organs within the same flower and that they are compatible."
msgstr ""
"Цветы гороха являются гермафродитами и самоплодными, что означает наличие "
"мужских и женских органов в одном цветке и их совместимость."

msgid "Cross pollination by insects may however occur between different varieties. The risk depends on the variety and the environment, whether there are natural barriers or not."
msgstr ""
"Однако перекрестное опыление насекомыми может происходить между различными "
"сортами. Риск зависит от сорта и окружающей среды, наличия или отсутствия "
"естественных барьеров."

msgid "To avoid cross-pollination, grow different varieties of pea 15 meters apart. A few meters are enough if there is a natural barrier such as a hedge between them."
msgstr ""
"Чтобы избежать перекрестного опыления, выращивайте разные сорта гороха на "
"расстоянии 15 метров друг от друга. Достаточно нескольких метров, если между "
"ними есть естественный барьер, например, живая изгородь."

msgid "When you wish to grow several varieties in the same garden you can isolate one variety under a net cage. Make sure you put the cage in place before flowering begins."
msgstr ""
"Если вы хотите выращивать несколько сортов в одном саду, вы можете "
"изолировать один сорт под сетчатой клеткой. Убедитесь, что вы установили "
"клетку до начала цветения."

msgid "Peas grown for seeds are grown in the same way as those for consumption."
msgstr ""
"Горох, выращиваемый на семена, выращивается так же, как и горох для "
"потребления."

msgid "Some peas can be sown before the winter, but usually peas are sown early in the spring. You should not sow peas too late as pea flowers cannot be fertilized at temperatures above 30°c."
msgstr ""
"Некоторые виды гороха можно высевать до наступления зимы, но обычно горох "
"высевают рано весной. Не следует сеять горох слишком поздно, так как при "
"температуре выше 30°c цветки гороха не могут быть оплодотворены."

msgid "Grow at least 50 plants to ensure good genetic diversity and a better selection."
msgstr ""
"Выращивайте не менее 50 растений, чтобы обеспечить хорошее генетическое "
"разнообразие и лучший выбор."

msgid "During growth, select the most beautiful, healthy and productive plants, and remove the others."
msgstr ""
"Во время роста выберите самые красивые, здоровые и продуктивные растения, а "
"остальные удалите."

msgid "It is better to reserve part of the crop for seed production from which you should not harvest any pods for consumption. You should let all seeds mature fully. In this way you will preserve the precocity of the variety."
msgstr ""
"Лучше зарезервировать часть урожая для производства семян, с которых не "
"следует собирать стручки для потребления. Вы должны дать всем семенам "
"полностью созреть. Таким образом вы сохраните скороспелость сорта."

msgid "To harvest the seeds, all you have to do is let the plants dry in the garden."
msgstr "Чтобы собрать семена, достаточно дать растениям высохнуть в саду."

msgid "If necessary, you can complete drying in a shed."
msgstr "При необходимости сушку можно завершить в сарае."

msgid "To make sure that the seeds are dry, bite one gently. If this leaves no mark, then they are fully dry."
msgstr ""
"Чтобы убедиться, что семена сухие, слегка надкусите одно из них. Если при "
"этом не останется следа, значит, они полностью высохли."

msgid "The shelling of the pods can either be done pod by pod, or you can beat them with a stick or tread on them."
msgstr ""
"Снятие шелухи производится вручную, стручок за стручком или ударяя палкой по "
"ним или шагая по стручках."

msgid "After this, remove the large debris by using a sieve. The larger waste should be removed by hand and the smaller chaff will pass through the sieve."
msgstr ""
"После этого удалите крупный мусор с помощью сита. Крупные отходы следует "
"удалять вручную, а мелкая мякина пройдет через сито."

msgid "You must then winnow the seeds to remove the very last chaff by blowing on them yourself or by using a ventilator or a small air-compressor."
msgstr ""
"Затем необходимо провеять семена, чтобы удалить самую последнюю мякину, дуя "
"на них самостоятельно или используя вентилятор или небольшой воздушный "
"компрессор."

msgid "It is important to put a label with the name of the variety and species, as well as the year, inside the package, as writing on the outside often rubs off."
msgstr ""
"Важно поместить этикетку с названием сорта и вида, а также годом внутри "
"упаковки, так как надпись на внешней стороне часто стирается."

msgid "Pea seeds are often infested by pea weavils (bruchus pisurum), small insects that lay their eggs under the skin of seeds while the plants are growing in the garden."
msgstr ""
"Семена гороха очень часто заражены зерновкой (bruchus pisurum), мелким "
"насекомым, которое откладывает яйца под оболочкой семян во время роста "
"растения в огороде."

msgid "An easy way to get rid of them is to leave the seeds in the freezer for a few days."
msgstr ""
"Простой способ избавиться от них - оставить семена в морозильной камере на "
"несколько дней."

msgid "Pea seeds have a germination capacity of 3 to 8 years."
msgstr "Семена гороха имеют всхожесть от 3 до 8 лет."

msgid "This can be extended by storing the seeds in a freezer."
msgstr "Этот срок можно продлить, если хранить семена в морозильной камере."

#. This title will be displayed by search engines like Google. 60 characters max.
msgctxt "seo-title"
msgid "Produce your pea seeds: video explanations"
msgstr "Выращивание собственных семян гороха: видео объяснения"

#. This description will be displayed below the title on search engine results. 160 characters max.
msgctxt "seo-description"
msgid "This video will explain, step by step, how to produce your own pea seeds: pollination, life cycle, extraction, sorting and storing."
msgstr ""
"В этом видеоролике шаг за шагом объясняется, как вырастить собственные "
"семена гороха: опыление, жизненный цикл, извлечение, сортировка и хранение."

#. The tags are used by search engines to reference the content. Use commas to separate tags.
msgctxt "seo-tags"
msgid "learn, how to, produce, seeds, video, pea, step by step, explanations"
msgstr ""
"научиться, как, производить, семена, видео, горох, шаг за шагом, объяснения"
