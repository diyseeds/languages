﻿1
00:00:12,010 --> 00:00:15,660
Cauliflowers and broccoli are members
of the Brassicaceae family

2
00:00:16,065 --> 00:00:18,985
and the Brassica oleracea species.

3
00:00:20,470 --> 00:00:25,500
Cauliflowers belong to the botrytis
var.botrytis subspecies,

4
00:00:25,745 --> 00:00:31,580
whereas broccoli is part of
the botrytis var.italica subspecies.

5
00:00:31,990 --> 00:00:35,950
The brassica oleracea species
also includes kohlrabi,

6
00:00:36,080 --> 00:00:40,580
cabbage, Brussels sprouts, kale
and the Savoy Cabbage. 

7
00:00:42,035 --> 00:00:47,780
Cauliflowers and broccoli are annual plants
or biennial ones for winter varieties.

8
00:00:49,380 --> 00:00:55,340
They are grown for their heads which are
embryonic fleshy clusters of future flowers.

9
00:00:57,225 --> 00:00:59,740
There are early and late varieties;

10
00:01:00,220 --> 00:01:02,960
cauliflowers can be white or purple,

11
00:01:04,910 --> 00:01:07,380
broccoli green or purple.

12
00:01:09,265 --> 00:01:12,515
The Romanesco broccoli is yellowish green. 

13
00:01:18,175 --> 00:01:22,820
Pollination of all of the cabbages
of the Oleracea species: 

14
00:01:38,840 --> 00:01:42,740
The flowers of the Brassica oleracea species
are hermaphrodite

15
00:01:45,680 --> 00:01:49,815
which means that they have both male
and female organs.

16
00:01:53,590 --> 00:01:56,100
Most of them are self-sterile:

17
00:01:56,730 --> 00:02:01,765
the pollen from the flowers of one plant
can only fertilize another plant.

18
00:02:05,585 --> 00:02:07,800
The plants are therefore allogamous.

19
00:02:08,720 --> 00:02:13,455
In order to ensure good pollination
it is better to grow several plants. 

20
00:02:17,115 --> 00:02:20,130
Insects are the vectors of pollination.

21
00:02:23,555 --> 00:02:27,930
These characteristics ensure great
natural genetic diversity.

22
00:02:34,945 --> 00:02:40,875
All of the cabbage sub-species of the Brassica
oleracea species can cross with each other.

23
00:02:42,865 --> 00:02:47,455
You should therefore not grow different
kinds of cabbage for seeds close to each other. 

24
00:02:52,025 --> 00:02:53,460
To ensure purity,

25
00:02:53,760 --> 00:02:57,700
different varieties
of the Brassica oleracea species

26
00:02:58,065 --> 00:03:01,170
should be planted at least 1 km apart. 

27
00:03:02,600 --> 00:03:05,380
This distance can be reduced to 500 meters

28
00:03:05,650 --> 00:03:09,820
if there is a natural barrier such
as a hedge between the two varieties. 

29
00:03:13,190 --> 00:03:15,640
The varieties can also be isolated

30
00:03:15,985 --> 00:03:20,740
by placing small hives with insects
inside a closed mosquito net

31
00:03:25,985 --> 00:03:29,695
or by alternately opening and closing
mosquito nets.

32
00:03:34,115 --> 00:03:35,155
For this technique,

33
00:03:35,440 --> 00:03:39,600
see the module on isolation techniques
in “The ABC of seed production”. 

34
00:03:48,350 --> 00:03:51,380
Life cycle of the cauliflower and broccoli 

35
00:04:09,935 --> 00:04:12,100
In regions with a mild climate

36
00:04:12,235 --> 00:04:16,525
you can cultivate broccoli
and cauliflower as biennial plants.

37
00:04:18,340 --> 00:04:22,260
You should sow them in the summer;
the plants will overwinter in the ground,

38
00:04:22,585 --> 00:04:26,410
and will then form their heads
and flower in the following spring.

39
00:04:27,065 --> 00:04:31,100
You will be able to harvest the seeds
in the summer of the second year. 

40
00:04:34,795 --> 00:04:41,300
Cauliflower and broccoli are, however,
exceptions in the brassica oleracea species

41
00:04:41,940 --> 00:04:45,420
in that they can multiply
in a single year of growth.

42
00:04:45,995 --> 00:04:49,385
To increase the chance of obtaining
seeds in the autumn,

43
00:04:49,680 --> 00:04:53,710
they are sown in a warm sheltered
place as early as possible,

44
00:04:53,910 --> 00:04:55,780
in January or February.

45
00:05:12,090 --> 00:05:14,020
In March or early April,

46
00:05:14,360 --> 00:05:16,415
they are replanted out in the ground

47
00:05:16,640 --> 00:05:20,495
and protected from late frosts
by a frost blanket. 

48
00:05:46,675 --> 00:05:50,380
Seeds are saved from healthy
and vigorous plants

49
00:05:50,680 --> 00:05:53,915
that you have observed
throughout the period of growth.

50
00:05:54,275 --> 00:05:58,005
In this way you can check all
of the characteristics of the variety :

51
00:06:00,540 --> 00:06:02,640
regular and vigorous growth,

52
00:06:07,050 --> 00:06:10,300
for cauliflowers the formation of tight heads

53
00:06:10,675 --> 00:06:13,380
that are well-protected by abundant leaves,

54
00:06:17,670 --> 00:06:22,950
for broccoli the formation of a single head
or multiple side shoots,

55
00:06:27,965 --> 00:06:34,020
as well as a long period of budding
before flowering, resistance to disease. 

56
00:06:35,655 --> 00:06:41,135
You should select 15 plants for seed production
to ensure good genetic diversity. 

57
00:06:43,395 --> 00:06:47,830
Once cauliflowers have formed their heads,
they can suffer from humidity.

58
00:06:47,975 --> 00:06:50,710
You can protect them from the rain
with a small roof.

59
00:06:52,630 --> 00:06:57,095
If parts of the head start to rot
you should remove them with a knife.

60
00:07:06,120 --> 00:07:09,615
Cauliflowers do not develop
lateral flower stalks.

61
00:07:11,840 --> 00:07:14,030
You should therefore never cut the head. 

62
00:07:39,315 --> 00:07:43,300
Cauliflower and broccoli should
flower in July at the latest,

63
00:07:44,010 --> 00:07:50,175
to ensure that they can complete their ripening
process which spreads out over a long period. 

64
00:08:03,200 --> 00:08:09,940
Harvesting, extracting, sorting
and storing of all Brassica oleracea

65
00:08:19,060 --> 00:08:22,750
The seeds are mature
when the seed pods turn beige.

66
00:08:27,055 --> 00:08:29,500
The seed pods are very dehiscent,

67
00:08:29,800 --> 00:08:34,780
which means that they open very easily
when mature and disperse their seed. 

68
00:08:43,395 --> 00:08:47,455
Most of the time, the stalks do not
all mature at the same time.

69
00:08:48,415 --> 00:08:54,425
To avoid wasting any seed, harvesting
can take place as each stalk matures.

70
00:08:56,230 --> 00:09:01,875
The entire plant can also be harvested
before all of the seeds have completely matured. 

71
00:09:05,420 --> 00:09:12,240
The ripening process is then completed
by drying them in a dry, well-ventilated place. 

72
00:09:24,845 --> 00:09:30,890
Cabbage seeds are ready to be removed
when the seed pods can be easily opened by hand. 

73
00:09:32,995 --> 00:09:34,260
To extract the seeds,

74
00:09:34,438 --> 00:09:37,700
the seed pods are spread across a plastic sheet

75
00:09:38,220 --> 00:09:43,010
or thick piece of fabric
and then beaten or rubbed together by hand.

76
00:09:46,500 --> 00:09:51,455
You can also put them in a bag
and beat them against a soft surface. 

77
00:09:52,960 --> 00:09:57,640
Larger quantities can be threshed
by walking or driving on them. 

78
00:10:10,255 --> 00:10:15,550
Seed pods that do not open easily
probably contain immature seeds

79
00:10:15,720 --> 00:10:17,440
that will not germinate well. 

80
00:10:22,185 --> 00:10:23,500
During sorting,

81
00:10:23,710 --> 00:10:28,245
the chaff is removed by first passing
the seeds through a coarse sieve

82
00:10:28,680 --> 00:10:30,140
that retains the chaff

83
00:10:35,065 --> 00:10:37,820
and then by passing them through another sieve

84
00:10:38,100 --> 00:10:42,940
that retains  the seeds but allows
smaller particles to fall through. 

85
00:10:46,405 --> 00:10:48,540
Finally, you should winnow them

86
00:10:49,015 --> 00:10:52,140
by blowing on them or with the help of the wind

87
00:10:52,290 --> 00:10:54,700
so that any remaining chaff is removed. 

88
00:11:09,825 --> 00:11:14,900
All seeds from the Brassica oleracea
species resemble one another.

89
00:11:15,790 --> 00:11:19,595
It is very difficult to distinguish
between, for example,

90
00:11:20,065 --> 00:11:22,470
cabbage and cauliflower seeds.

91
00:11:23,020 --> 00:11:26,020
This is why it is important to label the plants

92
00:11:26,140 --> 00:11:29,940
and then the extracted seeds
with the name of the species,

93
00:11:30,220 --> 00:11:33,140
the variety and the year of cultivation. 

94
00:11:34,600 --> 00:11:39,415
Storing the seeds in the freezer
for several days eliminates any parasites. 

95
00:11:44,875 --> 00:11:47,940
Cabbage seeds are able to germinate
up to 5 years.

96
00:11:49,090 --> 00:11:52,665
However, they may retain
this capacity up to 10 years.

97
00:11:54,550 --> 00:11:57,695
This can be prolonged by storing
them in the freezer.

98
00:11:58,855 --> 00:12:05,205
One gram contains 250 to 300 seeds
depending on the variety. 
