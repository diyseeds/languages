﻿1
00:00:11,040 --> 00:00:14,075
The cabbage is a member
of the Brassicaceae family,

2
00:00:14,570 --> 00:00:17,565
the Brassica oleracea species

3
00:00:19,715 --> 00:00:21,860
and the capitata subspecies.

4
00:00:22,845 --> 00:00:27,095
The brassica oleracea species
also includes kohlrabi,

5
00:00:27,710 --> 00:00:33,725
broccoli, Brussels sprouts, kale,
cauliflower and the Savoy Cabbage. 

6
00:00:38,300 --> 00:00:43,800
Cabbages can have different colours,
green, white or red

7
00:00:44,915 --> 00:00:48,920
and have heads that are pointed or round.

8
00:00:49,985 --> 00:00:55,490
This subspecies is characterized
by smooth leaves that form a tight head. 

9
00:01:06,560 --> 00:01:11,145
Pollination of all of the cabbages
of the Oleracea species: 

10
00:01:27,190 --> 00:01:31,130
The flowers of the Brassica oleracea
species are hermaphrodite

11
00:01:34,035 --> 00:01:38,030
which means that they have both male
and female organs.

12
00:01:41,960 --> 00:01:44,460
Most of them are self-sterile:

13
00:01:45,065 --> 00:01:50,075
the pollen from the flowers of one plant
can only fertilize another plant.

14
00:01:53,930 --> 00:01:56,075
The plants are therefore allogamous.

15
00:01:57,085 --> 00:02:01,865
In order to ensure good pollination
it is better to grow several plants. 

16
00:02:05,475 --> 00:02:08,490
Insects are the vectors of pollination.

17
00:02:11,895 --> 00:02:16,250
These characteristics ensure great
natural genetic diversity.

18
00:02:23,295 --> 00:02:27,615
All of the cabbage sub-species
of the Brassica oleracea species

19
00:02:27,785 --> 00:02:29,340
can cross with each other.

20
00:02:31,235 --> 00:02:35,855
You should therefore not grow different
kinds of cabbage for seeds close to each other. 

21
00:02:40,370 --> 00:02:46,035
To ensure purity, different varieties
of the Brassica oleracea species

22
00:02:46,420 --> 00:02:49,645
should be planted at least 1 km apart. 

23
00:02:50,960 --> 00:02:53,780
This distance can be reduced to 500 meters

24
00:02:54,020 --> 00:02:58,210
if there is a natural barrier
such as a hedge between the two varieties. 

25
00:03:01,560 --> 00:03:06,720
The varieties can also be isolated
by placing small hives with insects

26
00:03:06,955 --> 00:03:09,155
inside a closed mosquito net

27
00:03:14,310 --> 00:03:18,000
or by alternately opening and closing
mosquito nets.

28
00:03:22,485 --> 00:03:23,605
For this technique,

29
00:03:23,770 --> 00:03:28,090
see the module on isolation techniques
in “The ABC of seed production”. 

30
00:03:41,380 --> 00:03:43,380
Life cycle of the cabbage 

31
00:03:58,620 --> 00:04:02,220
Cabbage is a biennial plant
that is grown for seed

32
00:04:02,475 --> 00:04:04,985
in the same way as cabbages for consumption.

33
00:04:07,115 --> 00:04:08,270
In the first year,

34
00:04:08,610 --> 00:04:12,195
the plant forms a head
that remains over the winter

35
00:04:12,500 --> 00:04:14,415
and will flower the following year. 

36
00:04:22,055 --> 00:04:25,735
The time to sow is determined
by climate conditions,

37
00:04:25,935 --> 00:04:29,730
the overwintering method
and the earliness of the variety. 

38
00:05:24,900 --> 00:05:27,200
To overwinter heads of cabbage,

39
00:05:27,390 --> 00:05:30,400
sow in mid-May or at the beginning of June,

40
00:05:30,930 --> 00:05:33,530
even later for early varieties,

41
00:05:34,065 --> 00:05:38,996
to avoid over-developed
and split heads at the end of autumn. 

42
00:05:42,923 --> 00:05:46,981
Heads that are smaller
but tight will winter better. 

43
00:05:50,952 --> 00:05:57,294
At least 30 plants should be saved the first year
so that 10 to 15 remain at the end of winter.

44
00:06:06,196 --> 00:06:08,661
Seeds are saved from healthy plants

45
00:06:08,792 --> 00:06:12,036
that have been observed over
the entire period of growth

46
00:06:12,196 --> 00:06:15,450
so that all of their characteristics are known. 

47
00:06:21,800 --> 00:06:23,992
The most vigorous heads are chosen

48
00:06:24,065 --> 00:06:27,352
in accordance
with the variety’s specific characteristics

49
00:06:27,498 --> 00:06:29,330
and your selection criteria:

50
00:06:30,160 --> 00:06:34,560
regular and vigorous growth,
formation of tight heads,

51
00:06:34,865 --> 00:06:40,734
storage capacity,
precocity and resistance to cold. 

52
00:06:43,563 --> 00:06:48,276
Overwintering is the most sensitive
period of seed production.

53
00:06:49,680 --> 00:06:54,996
There are several methods for overwintering
plants depending on climate conditions,

54
00:06:55,200 --> 00:06:59,018
vegetation period
and available infrastructure. 

55
00:07:11,854 --> 00:07:14,200
In regions with a harsh winter,

56
00:07:14,523 --> 00:07:20,072
the entire plant along with its roots
is harvested at the end of autumn.

57
00:07:21,949 --> 00:07:25,949
The outer leaves are removed
so that only the tight,

58
00:07:26,130 --> 00:07:28,414
firm leaves of the head remain.

59
00:07:36,392 --> 00:07:39,578
The heads should be dry and free of soil. 

60
00:07:44,080 --> 00:07:48,254
In regions in which the level of humidity
of the air is low,

61
00:07:48,850 --> 00:07:52,552
plants can be stored in a cellar
with an earthen floor. 

62
00:07:57,560 --> 00:08:04,196
In regions with high air humidity,
plants can be stored in a frost-free room or attic.

63
00:08:05,607 --> 00:08:10,007
The temperature in the room
should not fall below 0° C

64
00:08:10,145 --> 00:08:17,140
over a long period even though the cabbages
can resist short periods of frost at -5° C. 

65
00:08:19,280 --> 00:08:22,800
Throughout winter it is necessary
to keep an eye on the cabbages.

66
00:08:23,870 --> 00:08:29,440
The outer leaves can be attacked
by gray mold (Botrytis cinera).

67
00:08:30,320 --> 00:08:33,120
They should be removed along
with the rotten parts

68
00:08:33,338 --> 00:08:37,149
and then the wounds should be
disinfected with wood ash. 

69
00:08:40,370 --> 00:08:45,905
Certain very resistant varieties
or those grown in regions with mild winters

70
00:08:46,349 --> 00:08:48,930
can be left
in the ground over winter. 

71
00:08:52,843 --> 00:08:56,829
In mild climates they can also
be stored in the ground:

72
00:08:57,265 --> 00:09:00,814
the plants along with their roots
are laid in deep channels,

73
00:09:01,000 --> 00:09:04,400
slightly tilted up and covered with soil.

74
00:09:08,203 --> 00:09:10,552
The plants should not touch one another

75
00:09:11,180 --> 00:09:15,200
and in cases of frost must be covered
with a pane of glass,

76
00:09:15,483 --> 00:09:17,890
manure or dead leaves. 

77
00:09:28,705 --> 00:09:33,789
This protection is removed in spring
but the plants should not be replanted.

78
00:09:34,276 --> 00:09:37,985
They will push up through the soil
covering them and then flower. 

79
00:09:47,040 --> 00:09:51,090
Another technique involves saving
the roots without the heads,

80
00:09:51,454 --> 00:09:52,647
which can be eaten.

81
00:09:53,207 --> 00:09:56,203
At the end of summer during a dry period,

82
00:09:56,552 --> 00:10:00,014
the heads are cut off at the base at a slight angle.

83
00:10:00,980 --> 00:10:03,854
Only the stem and the roots are kept. 

84
00:10:07,963 --> 00:10:12,363
These are left to dry for several days
and disinfected with wood ash.

85
00:10:13,563 --> 00:10:18,145
To prevent rot, the dry cut can be
coated with grafting wax. 

86
00:10:23,818 --> 00:10:28,123
This method of propagation makes
overwintering possible,

87
00:10:28,334 --> 00:10:33,127
but the stems preserved in this way
produce fewer seeds of lower quality.

88
00:10:35,054 --> 00:10:38,058
They cannot flower from the centre of the stem

89
00:10:38,378 --> 00:10:42,072
from which the most beautiful seed
stalks produce the best seeds. 

90
00:10:47,360 --> 00:10:52,029
The seed plants that have been
stored over winter in a cellar or attic

91
00:10:52,530 --> 00:10:56,021
are replanted in the spring
of the second year of the cycle,

92
00:10:56,247 --> 00:10:57,927
in March or April. 

93
00:11:01,680 --> 00:11:08,327
The cabbages are buried 60 cm apart
so that the top of the heads are at ground level.

94
00:11:09,360 --> 00:11:11,890
The plants will grow new roots.

95
00:11:14,596 --> 00:11:18,269
It is important to water the plants
well when they are planted

96
00:11:18,480 --> 00:11:21,112
and during the period of root growth. 

97
00:11:22,960 --> 00:11:26,283
To encourage the emergence of flower
stalks from the heads

98
00:11:26,560 --> 00:11:32,910
it is often necessary to make an incision
in the form of a cross at the top of the head

99
00:11:33,170 --> 00:11:34,327
using a knife.

100
00:11:34,720 --> 00:11:39,980
This should be 3 to 6 cm deep depending
on the size of the head.

101
00:11:41,541 --> 00:11:46,843
Take care not to damage the base of the cabbage
from which the seed stalks will grow.

102
00:11:48,865 --> 00:11:55,098
Sometimes it is necessary to repeat
the incision if a flower stalk does not appear. 

103
00:12:11,440 --> 00:12:14,792
The central flower stalk
produces the best seeds.

104
00:12:17,032 --> 00:12:22,400
Weaker secondary stalks can be removed
to allow the central inflorescence

105
00:12:22,523 --> 00:12:27,905
to develop better and to channel all the strength
of the plant into producing seed. 

106
00:13:03,250 --> 00:13:06,632
Since the seed stalks can reach
a height of 2 meters,

107
00:13:07,076 --> 00:13:09,629
it is necessary to stake each plant

108
00:13:09,956 --> 00:13:14,232
and secure the stalks which can become
very heavy when the seeds are formed. 

109
00:13:35,992 --> 00:13:42,821
Harvesting, extracting, sorting
and storing of all Brassica oleracea 

110
00:13:51,832 --> 00:13:55,636
The seeds are mature
when the seed pods turn beige.

111
00:13:59,810 --> 00:14:02,392
The seed pods are very dehiscent,

112
00:14:02,560 --> 00:14:07,665
which means that they open very easily
when mature and disperse their seed. 

113
00:14:16,138 --> 00:14:20,218
Most of the time, the stalks
do not all mature at the same time.

114
00:14:21,207 --> 00:14:27,410
To avoid wasting any seed,
harvesting can take place as each stalk matures.

115
00:14:28,920 --> 00:14:34,900
The entire plant can also be harvested
before all of the seeds have completely matured. 

116
00:14:37,992 --> 00:14:43,178
The ripening process is then completed
by drying them in a dry,

117
00:14:43,320 --> 00:14:45,403
well-ventilated place. 

118
00:14:57,621 --> 00:15:03,781
Cabbage seeds are ready to be removed
when the seed pods can be easily opened by hand. 

119
00:15:05,730 --> 00:15:07,130
To extract the seeds,

120
00:15:07,258 --> 00:15:12,574
the seed pods are spread
across a plastic sheet or thick piece of fabric

121
00:15:12,952 --> 00:15:15,789
and then beaten or rubbed together by hand.

122
00:15:19,200 --> 00:15:24,429
You can also put them in a bag
and beat them against a soft surface. 

123
00:15:25,672 --> 00:15:30,589
Larger quantities can be threshed
by walking or driving on them. 

124
00:15:42,960 --> 00:15:48,312
Seed pods that do not open easily
probably contain immature seeds

125
00:15:48,509 --> 00:15:50,480
that will not germinate well. 

126
00:15:54,830 --> 00:15:56,232
During sorting,

127
00:15:56,480 --> 00:16:01,003
the chaff is removed by first passing
the seeds through a coarse sieve

128
00:16:01,345 --> 00:16:03,220
that retains the chaff

129
00:16:07,832 --> 00:16:10,538
and then by passing them through another sieve

130
00:16:10,850 --> 00:16:15,716
that retains the seeds
but allows smaller particles to fall through. 

131
00:16:19,170 --> 00:16:24,865
Finally, you should winnow them
by blowing on them or with the help of the wind

132
00:16:25,040 --> 00:16:27,636
so that any remaining chaff is removed. 

133
00:16:42,603 --> 00:16:47,745
All seeds from the Brassica oleracea
species resemble one another.

134
00:16:48,545 --> 00:16:55,258
It is very difficult to distinguish between,
for example, cabbage and cauliflower seeds.

135
00:16:55,760 --> 00:16:58,821
This is why it is important to label the plants

136
00:16:58,930 --> 00:17:02,734
and then the extracted seeds
with the name of the species,

137
00:17:03,010 --> 00:17:06,232
the variety and the year of cultivation. 

138
00:17:07,447 --> 00:17:12,261
Storing the seeds in the freezer
for several days eliminates any parasites. 

139
00:17:17,665 --> 00:17:20,850
Cabbage seeds are able to germinate up to 5 years.

140
00:17:21,854 --> 00:17:25,607
However, they may retain
this capacity up to 10 years.

141
00:17:27,338 --> 00:17:30,727
This can be prolonged
by storing them in the freezer.

142
00:17:31,629 --> 00:17:38,196
One gram contains 250 to 300 seeds
depending on the variety. 
