﻿1
00:00:00,000 --> 00:00:05,760
Buzuruna Juzuruna'dan Serge Harfouche ile röportaj
Mayıs 2021, Bekaa Vadisi, Lübnan
www.DIYseeds.org
tohum üretimi üzerine eğitsel filmler

2
00:00:05,920 --> 00:00:08,760
-"Buzuruna Juzuruna" ne anlama geliyor?

3
00:00:09,520 --> 00:00:12,100
Anlamı "tohumlarımız köklerimiz"dir.

4
00:00:12,140 --> 00:00:16,150
-Peki bu isim derneğiniz açısından nasıl bir önem taşımaktadır?

5
00:00:17,870 --> 00:00:25,170
Köylü tohumları üreten bir okul çiftliğiyiz

6
00:00:26,090 --> 00:00:30,570
ayrıca eğitimler düzenliyoruz.

7
00:00:30,660 --> 00:00:35,520
Bunlar iki temel faaliyetimiz.

8
00:00:36,220 --> 00:00:39,350
Yani tohumlar üretiyoruz

9
00:00:39,530 --> 00:00:42,460
ve onlar da köklere dönüşüyor.

10
00:00:42,460 --> 00:00:47,020
Temel fikir çiftçi özerkliği

11
00:00:47,130 --> 00:00:49,380
ve gıda bağımsızlığı için çalışmak.

12
00:00:49,380 --> 00:00:53,120
Bu ikisi uyum içindedir.

13
00:00:54,080 --> 00:01:04,730
-Neden günümüzde Lübnan’da gıda bağımsızlığı bu kadar önemli bir mevzu?

14
00:01:05,600 --> 00:01:07,840
Bir acil durum sözkonusu.

15
00:01:07,860 --> 00:01:13,600
Eşi benzeri görülmemiş bir ekonomik krizden geçmekteyiz,

16
00:01:13,600 --> 00:01:15,600
çok şiddetli bir ekonomik kriz.

17
00:01:15,710 --> 00:01:24,150
Para birimimiz on kat değer kaybetti.

18
00:01:24,260 --> 00:01:30,020
1 doların 1500 Lübnan lirasından 15 binlere çıktığı zamanlar oldu

19
00:01:30,040 --> 00:01:32,880
şimdilerde 12 bin seviyesinde aşağı yukarı sabitlendi.

20
00:01:32,950 --> 00:01:36,600
Çok belirsiz bir durumdayız, ne zaman patlayacağını bilemiyoruz.

21
00:01:36,680 --> 00:01:37,770
-Bu ne kadar zaman önceydi?

22
00:01:37,860 --> 00:01:43,400
Çöküş 2018 civarında başladı

23
00:01:43,510 --> 00:01:54,640
Fakat asıl enflasyon 2019 ortalarında, 2020 başlarında ortaya çıktı sanırım.

24
00:01:55,950 --> 00:02:01,260
-Bu olduğunda insanlar üzerindeki etkisi ne oldu?

25
00:02:02,600 --> 00:02:07,020
Korkunç bir etkisi oldu, çünkü Lübnan lirası üzerinden maaşlarda artış olmadı.

26
00:02:07,060 --> 00:02:12,200
Aylık asgari ücret yaklaşık 400 avroydu.

27
00:02:12,200 --> 00:02:17,350
Şimdi ise sadece 40 avro,

28
00:02:17,880 --> 00:02:21,620
bütün tüketim ürünleri de çok daha pahalı

29
00:02:21,680 --> 00:02:24,750
çünkü her şey dolara endeksli, giderek felakete doğru gidiyor.

30
00:02:24,840 --> 00:02:27,910
Bir şişe süt aldığınızı düşünün.

31
00:02:27,930 --> 00:02:31,530
3000 Lübnan lirası olan bir şişe süt

32
00:02:31,530 --> 00:02:36,480
şimdi 25-27 bin civarında,

33
00:02:36,600 --> 00:02:40,200
ama hâlâ 100,000 kazanıyorsunuz.

34
00:02:40,200 --> 00:02:44,160
Her ay bir kaç tane alabiliyorken

35
00:02:44,260 --> 00:02:47,380
artık sadece bir şişe alabiliyorsunuz.

36
00:02:47,440 --> 00:02:59,180
Tüketimimiz % 90-95 oranında ithalata dayandığı için,

37
00:02:59,260 --> 00:03:01,580
her işlem bir yabancı para birimine bağlı olduğunda

38
00:03:01,620 --> 00:03:03,260
-ki bizde bu para birimi dolardır-,

39
00:03:03,310 --> 00:03:05,520
her şey daha da pahalı hale gelir:

40
00:03:05,570 --> 00:03:08,210
ekmek daha pahalanır,

41
00:03:08,360 --> 00:03:11,450
benzin fiyatı uçar

42
00:03:11,560 --> 00:03:14,660
ancak maaşlar yerinde sayar.

43
00:03:14,690 --> 00:03:17,720
Bu koşullarda, yaşam standardı,

44
00:03:17,750 --> 00:03:20,820
ve yaşam kalitesinin yanısıra

45
00:03:20,910 --> 00:03:23,090
gıdaya erişim ve gıda güvenliği de

46
00:03:23,110 --> 00:03:26,530
büyük bir darbe almış oluyor.

47
00:03:26,720 --> 00:03:30,370
Bu nedenle, eğer hibrit ve steril tohumlar alıyorsanız

48
00:03:30,430 --> 00:03:33,200
ve her yıl daha da fazla satın almanız gerekiyorsa,

49
00:03:33,220 --> 00:03:35,490
dışardan ithal etmek veya piyasadan satın almak yerine

50
00:03:35,520 --> 00:03:38,230
kendi tohumlarımızı üretmek çok acil bir ihtiyaç.

51
00:03:40,010 --> 00:03:43,980
Aynı şekilde, kendi gübrelerimizi de

52
00:03:44,020 --> 00:03:46,480
organik ve temiz yöntemlerle üretip

53
00:03:46,530 --> 00:03:49,810
kompost yapmak zorundayız...

54
00:03:49,890 --> 00:03:51,540
Tüm paket yani.

55
00:03:51,570 --> 00:03:55,290
Eğitimlerimizde ve çiftlikteki tüm diğer aktivitelerimizde

56
00:03:55,330 --> 00:03:57,330
yapmaya çalıştığımız budur.

57
00:03:58,730 --> 00:04:01,460
-Fransa'dayken bize

58
00:04:01,470 --> 00:04:04,110
devrim dediğin şeyden sonra

59
00:04:04,160 --> 00:04:07,630
hayatın birçok insan için,

60
00:04:07,640 --> 00:04:10,560
çok çeşitli insanlar için,

61
00:04:10,660 --> 00:04:12,610
nasıl değiştiğinin resimlerini gösterdin:

62
00:04:12,630 --> 00:04:18,410
öğrenciler, memurlar,

63
00:04:18,430 --> 00:04:20,730
her türden insan

64
00:04:20,750 --> 00:04:23,360
aniden bu konuya ilgi duymaya başladı.

65
00:04:23,630 --> 00:04:24,810
Kesinlikle.

66
00:04:24,930 --> 00:04:27,530
Neyin gelmekte olduğunu o şekilde fark ettik,

67
00:04:28,180 --> 00:04:33,380
daha önce benzeri görülmemiş bir ekonomik krizin yaklaştığını böyle anladık.

68
00:04:33,490 --> 00:04:38,210
Devrim bu şekilde başladı.

69
00:04:38,270 --> 00:04:43,040
İnsanlar gelecek yılların kolay olmayacağının

70
00:04:43,040 --> 00:04:47,650
ve bir şekilde idare etmeleri gerektiğinin

71
00:04:47,680 --> 00:04:50,490
belli belirsiz farkındaydılar.

72
00:04:50,540 --> 00:04:54,340
Giderek daha çok farkına varıyoruz ki

73
00:04:54,400 --> 00:05:00,000
üretken olmayan bir ekonomi sürdürülebilir değildir,

74
00:05:00,080 --> 00:05:03,130
ve bizim ekonomimizin üretken olduğunu söylemek mümkün değil.

75
00:05:03,460 --> 00:05:05,920
Uzun zamandır, yaklaşık 30-40 yıldır,

76
00:05:05,970 --> 00:05:10,250
borca dayalı bir ekonomimiz vardı.

77
00:05:11,090 --> 00:05:15,320
-Bunun üzerine mi pek çok insan üretim yapmaya başladı?

78
00:05:15,460 --> 00:05:17,980
...

79
00:05:18,040 --> 00:05:19,290
Kayıttayız.

80
00:05:20,430 --> 00:05:23,260
Özür dilerim, iş arkadaşımla konuşuyordum.

81
00:05:23,280 --> 00:05:26,070
-Çiftliğin nüfusu kaç?

82
00:05:26,850 --> 00:05:31,620
Burada 16 yetişkin, 27 çocuk var

83
00:05:31,650 --> 00:05:34,160
Şu sıralar çok sayıda arkadaşımız bizi ziyarete geldi,

84
00:05:34,210 --> 00:05:36,000
ayrıca gönüllüler ve staj yapanlar da var...

85
00:05:36,030 --> 00:05:38,930
Toplam 50 kişiyiz,

86
00:05:39,000 --> 00:05:40,370
belki biraz daha fazla.

87
00:05:40,410 --> 00:05:43,350
-Peki buranın büyüklüğü nedir?

88
00:05:44,940 --> 00:05:47,770
Derneğin olduğu çiftlik

89
00:05:47,920 --> 00:05:54,140
20 dönümlük bir alan üzerine kurulu:

90
00:05:54,580 --> 00:05:57,940
18 dönümü mahsullere

91
00:05:57,950 --> 00:06:00,500
2 dönümü binalara,

92
00:06:00,670 --> 00:06:02,980
fideliklere,

93
00:06:03,070 --> 00:06:06,050
kümeslere,

94
00:06:06,060 --> 00:06:07,640
keçi ve koyun ağıllarına ayrılmıştır.

95
00:06:07,710 --> 00:06:09,820
Ayrıca bu sene

96
00:06:09,830 --> 00:06:13,490
üretimimizi arttırmak ve çeşitlendirmek için,

97
00:06:13,610 --> 00:06:16,600
özellikle de tahıl üretimimizi arttırmak için,

98
00:06:16,700 --> 00:06:19,700
ilaveten yaklaşık 70 dönümlük

99
00:06:19,720 --> 00:06:21,350
bir arsa daha kiraladık.

100
00:06:22,280 --> 00:06:24,770
-Çiftlikte yaşamıyor musunuz?

101
00:06:25,610 --> 00:06:27,510
Tam olarak değil.

102
00:06:27,520 --> 00:06:32,860
Kurucularımızdan biri çiftlikte yaşıyor.

103
00:06:34,400 --> 00:06:36,320
Bir binamız var

104
00:06:36,340 --> 00:06:39,780
içinde bir mutfak

105
00:06:39,820 --> 00:06:43,090
hem kendi kullanımımız için, hem de işlenmiş gıdaları depolamak için.

106
00:06:43,140 --> 00:06:44,350
Oldukça uzun bir yapı,

107
00:06:44,360 --> 00:06:47,040
içinde odalar da var.

108
00:06:47,060 --> 00:06:50,120
Walid ailesiyle orada ikamet ediyor.

109
00:06:50,260 --> 00:06:52,440
Diğer arkadaşlarımız ve meslektaşlarımız

110
00:06:52,460 --> 00:06:54,260
yolun karşı tarafında yaşıyor,

111
00:06:54,310 --> 00:06:56,530
biz biraz daha yukarıdayız,

112
00:06:56,550 --> 00:06:58,450
yürüyerek 4 dakikalık bir mesafede.

113
00:06:59,590 --> 00:07:02,290
-Yetiştirdiklerinizden bazılarını da

114
00:07:02,300 --> 00:07:04,400
kendi tüketiminiz için ayırıyor musunuz?

115
00:07:04,740 --> 00:07:06,950
Evet!

116
00:07:07,160 --> 00:07:10,070
Çok az şey satın alıyoruz.

117
00:07:10,110 --> 00:07:13,770
Üretebileceklerimizi üretiyoruz, satın almıyoruz!

118
00:07:14,900 --> 00:07:17,980
Bizim için üretmesi zor olan bir kaç ürün var,

119
00:07:18,010 --> 00:07:20,880
örneğin pirinç,

120
00:07:20,910 --> 00:07:22,690
o tür şeyleri satın alıyoruz.

121
00:07:22,920 --> 00:07:24,120
Fakat bunlardan başka yok…

122
00:07:24,130 --> 00:07:27,230
Hatta geçen yıl bal üretmeyi bile başardık,

123
00:07:27,250 --> 00:07:30,520
fakat yırtıcı bir hayvan arılarımıza saldırdı,

124
00:07:30,560 --> 00:07:32,600
bu nedenle kovan sayımız,

125
00:07:32,620 --> 00:07:34,570
11'den 3'e düştü.

126
00:07:36,830 --> 00:07:39,980
Tabii asıl önemli olan bunun mümkün olduğunu göstermek.

127
00:07:41,590 --> 00:07:46,920
-Peki sizin Lübnan ve Suriye'de sahip olduğunuz

128
00:07:46,940 --> 00:07:50,600
partnerler, ağlar ve işbirlikleri,

129
00:07:50,610 --> 00:07:54,570
onlar nasıl gidiyor, nasıl gelişmeler oldu?

130
00:07:54,590 --> 00:07:56,450
Lütfen anlatın!

131
00:07:57,100 --> 00:08:00,160
Temel fikir ilk olarak tohum üretmek

132
00:08:00,180 --> 00:08:03,170
ardından bu konuyla ilgilenen herkese

133
00:08:03,190 --> 00:08:04,560
kendi tohumlarını nasıl üreteceklerini öğretmekti

134
00:08:04,590 --> 00:08:08,450
Böylece her yıl yeniden üretmek zorunda kalmayacaktık.

135
00:08:08,450 --> 00:08:11,030
Bu, bağımsızlık ve

136
00:08:11,040 --> 00:08:12,560
özerklik ile ilgili bir durumdur.

137
00:08:12,580 --> 00:08:16,380
Daha sonra bu tohum kurtarıcıları

138
00:08:16,400 --> 00:08:17,910
bir çok yere dağıldılar.

139
00:08:17,920 --> 00:08:19,710
Bir kısmı Suriye'ye döndü

140
00:08:19,710 --> 00:08:22,260
ve orada kendi tohumlarını üretmeye başladılar.

141
00:08:22,280 --> 00:08:25,120
Bir çok farklı durum ortaya çıktı.

142
00:08:25,150 --> 00:08:29,610
En baştan beri birlikte olduğumuz bazı arkadaşlarımız

143
00:08:29,630 --> 00:08:32,020
artık tamamen özerk hale geldiler.

144
00:08:32,040 --> 00:08:34,970
Çok başarılı bir şekilde üretim yapıyorlar.

145
00:08:34,980 --> 00:08:36,910
Elbette onları yakından takip ediyoruz,

146
00:08:36,920 --> 00:08:40,350
bir bilgi veya deneyim gerekli olduğu her durumda:

147
00:08:40,360 --> 00:08:42,840
"şunu denemiştik", "bunu yapmıştık" diyoruz.

148
00:08:42,920 --> 00:08:46,690
Hedefimiz Lübnan'da

149
00:08:46,690 --> 00:08:49,360
hatta belki bir gün Suriye'de

150
00:08:49,390 --> 00:08:51,640
mümkün olduğu kadar çok tohum koruyucusu

151
00:08:51,690 --> 00:08:55,600
ve tohum bankasına sahip olmaktır.

152
00:08:57,460 --> 00:09:02,240
-Sınır hâlâ kapalı mı?

153
00:09:03,970 --> 00:09:06,550
Evet, kapalı

154
00:09:06,570 --> 00:09:09,540
ve pek çok ülkenin vatandaşları için

155
00:09:09,610 --> 00:09:12,040
bu sınırı geçebilmek çok zor.

156
00:09:12,050 --> 00:09:16,250
Eğer diplomatik bir misyonda veya orduda görevli değilseniz

157
00:09:16,400 --> 00:09:17,990
sınırı geçmek hâlâ çok zor -

158
00:09:18,660 --> 00:09:20,860
denememekte fayda var.

159
00:09:21,700 --> 00:09:23,550
Olduğunuz yerde kalmanız daha iyi olabilir.

160
00:09:23,580 --> 00:09:25,730
-Eğitimlere gelirsek...

161
00:09:25,920 --> 00:09:30,670
DIYseeds çevirisinde yer aldınız-

162
00:09:34,330 --> 00:09:37,440
websitesini mi çevirdiniz?

163
00:09:38,750 --> 00:09:40,100
Websitesini Arapçaya evet,

164
00:09:40,130 --> 00:09:44,680
ve yapılan videoları gözden geçirdim

165
00:09:44,820 --> 00:09:47,750
yaptıkları iş inanılmaz.

166
00:09:47,760 --> 00:09:50,090
Çılgın bir iş çıkarmışlar, olağanüstü.

167
00:09:50,170 --> 00:09:52,080
Aynı zamanda da çok işe yarayacak bir eser.

168
00:09:52,120 --> 00:09:54,720
Bir eğitim verdiğinizde,

169
00:09:54,780 --> 00:09:57,840
tohumların nasıl çıkarılması gerektiğini

170
00:09:57,870 --> 00:10:01,660
veya çapraz tozlaşmayı önlemek için bitkiler arasında ne kadar mesafe bırakılması gerektiğini

171
00:10:01,670 --> 00:10:03,360
anlatmanız gerektiğinde,

172
00:10:03,380 --> 00:10:05,360
bunu fotoğraflarla, çizimlerle,

173
00:10:05,360 --> 00:10:08,140
ve videolarla açıklamak çok daha kolay oluyor.

174
00:10:08,140 --> 00:10:11,550
Bu videoları sadece eğitimler sırasında kullanılmıyor,

175
00:10:11,560 --> 00:10:13,470
aynı zamanda, internet bağlantıları olduğu sürece,

176
00:10:13,480 --> 00:10:17,000
insanlar doğrudan telefonlarından da erişebilmektedir

177
00:10:17,020 --> 00:10:19,660
Bu onlara süreci kendi kendilerine de

178
00:10:19,680 --> 00:10:23,800
gayet güzel bir şekilde yönetme imkanı veriyor.

179
00:10:23,910 --> 00:10:25,680
Bu mükemmel bir imkan.

180
00:10:27,600 --> 00:10:30,770
-Bölgedeki insanlar için bilgiye bu şekilde ulaşmak,

181
00:10:31,240 --> 00:10:35,560
onu kullanmak çok kolaylaşıyor…

182
00:10:35,640 --> 00:10:36,480
Kesinlikle!

183
00:10:36,540 --> 00:10:37,730
İşler onlar için çok basitleşiyor böylece,

184
00:10:37,760 --> 00:10:40,500
çünkü çok kullanışlı, çok basit bir şey.

185
00:10:40,520 --> 00:10:44,910
Kocaman ağır bir kitap değil ki.

186
00:10:44,960 --> 00:10:47,940
Çiftçilerin çoğunun okuması yazması yok,

187
00:10:48,000 --> 00:10:50,220
ama duydukları bir şeyi rahatlıkla anlayabilirler.

188
00:10:50,260 --> 00:10:52,970
Ayrıca, anlatılanları çok daha iyi ifade eden,

189
00:10:53,040 --> 00:10:55,570
çizimler ve videolar var,

190
00:10:55,580 --> 00:10:58,770
içeriği çok daha anlaşılır ve basit bir hale getiren

191
00:10:58,800 --> 00:11:01,830
takip etmesi kolay dersler niteliğinde

192
00:11:01,870 --> 00:11:05,270
Kalın bir kitaba göre kavraması çok daha kolay.

193
00:11:06,000 --> 00:11:11,350
-Tabii ürettiğiniz tohumlar hibrid olmayan,

194
00:11:11,380 --> 00:11:14,580
doğal ve yenilenebilir tohumlar olduğu için,

195
00:11:14,660 --> 00:11:18,030
bu tohumlara sahip olan ve videoları da izleyebilen herkes

196
00:11:18,160 --> 00:11:21,380
hızla özerklik elde eder.

197
00:11:21,400 --> 00:11:26,560
Bunun ne kadar hızla gerçekleştiğini söyleyebilir misiniz?

198
00:11:26,580 --> 00:11:27,630
Ne kadar hızlı bir şekilde?

199
00:11:27,650 --> 00:11:30,530
Bunun bağlı olduğu bazı faktörler var,

200
00:11:30,560 --> 00:11:32,160
toprağa erişim, suya erişim,

201
00:11:32,180 --> 00:11:34,310
bölgenin ne kadar güvenli bir yer olduğu...

202
00:11:34,320 --> 00:11:37,320
Suriye'de yaşadıklarını yeri

203
00:11:37,390 --> 00:11:39,260
defalarca değiştirmek zorunda kalmış arkadaşlarımız var.

204
00:11:39,300 --> 00:11:41,170
Yerleştikleri her yer

205
00:11:41,180 --> 00:11:42,830
bombalandığı için,

206
00:11:42,840 --> 00:11:44,810
yeniden ve yeniden taşınmak zorunda kalmışlar.

207
00:11:44,960 --> 00:11:47,240
İçinde bulundukları durum çok karmaşık.

208
00:11:47,320 --> 00:11:51,670
Ama elinizde temel araçlar olduğunda,

209
00:11:51,710 --> 00:11:54,660
nerede olursanız olun bunu başarmanız mümkün.

210
00:11:54,690 --> 00:11:57,110
Tohumlarınız var, onları nasıl ekeceğinizi biliyorsunuz,

211
00:11:57,130 --> 00:12:00,260
nasıl büyüteceğinizi, nasıl çoğaltacağınızı,

212
00:12:00,290 --> 00:12:01,990
nasıl hasat edeceğinizi, tohumları nasıl muhafaza edeceğinizi vs.

213
00:12:02,020 --> 00:12:04,980
Özgür bir insansınız!

214
00:12:07,140 --> 00:12:08,050
Bu kadar basit.

215
00:12:08,190 --> 00:12:12,120
-Yani bir mevsimde tohum üretmeyi öğrenmiş,

216
00:12:12,170 --> 00:12:15,180
insanlardan bahsediyoruz-

217
00:12:15,580 --> 00:12:19,470
ve şimdi kendi tohumlarına mı sahipler?

218
00:12:19,520 --> 00:12:20,510
Aynen öyle.

219
00:12:20,530 --> 00:12:23,240
Birlikte çalıştığımız insanlardan çoğu

220
00:12:23,290 --> 00:12:25,350
tarımla ilişkili bir geçmişe sahip,

221
00:12:25,370 --> 00:12:26,840
tarımla içiçe büyümüşler,

222
00:12:26,850 --> 00:12:29,000
tarımsal işlerde çalışmışlar,

223
00:12:29,020 --> 00:12:30,440
konvansiyonel tarımdan bahsediyoruz tabii.

224
00:12:30,460 --> 00:12:34,350
Onlar için bu dönüşümü gerçekleştirmek çok zor değil.

225
00:12:36,550 --> 00:12:38,500
Geçmişte

226
00:12:38,520 --> 00:12:39,970
tohum satın almak zorunda kalmışlar,

227
00:12:39,970 --> 00:12:42,450
fakat kaderlerinin kendi ellerinde olduğunu,

228
00:12:42,470 --> 00:12:45,010
yalnızca doğru araçlara ihtiyaç duyduklarını anlar anlamaz,

229
00:12:45,260 --> 00:12:47,100
bir veya iki sezon içinde

230
00:12:47,120 --> 00:12:50,730
kendi tohumlarını üretmeye başladılar,

231
00:12:50,880 --> 00:12:53,890
ve artık onlarla

232
00:12:53,930 --> 00:12:55,040
tohum takası yapıyoruz.

233
00:12:55,080 --> 00:12:56,800
Uzun vadeli bir ortak çalışma ilkemiz var,

234
00:12:56,820 --> 00:13:00,570
örneğin biri bir çeşidin üretimini yapıyor,

235
00:13:00,580 --> 00:13:03,410
bir diğeri başka bir çeşit yetiştiriyor, biz üçüncü bir çeşit yetiştiriyoruz…

236
00:13:03,423 --> 00:13:08,680
bunun amacı olabildiğince fazla çeşitlilik elde etmek,

237
00:13:08,720 --> 00:13:10,680
ve bu yöntemin işe yaradığını gördük.

238
00:13:11,760 --> 00:13:16,280
-Peki ya siz nasıl dahil oldunuz bu çalışmaya?

239
00:13:18,426 --> 00:13:20,280
Bu gerçekten tesadüfen oldu.

240
00:13:20,330 --> 00:13:26,600
Beyrut'ta yaşıyordum ve ev arkadaşım Ferdi ile birlikte üniversitedeydi.

241
00:13:26,693 --> 00:13:30,613
Derneğin kurucularından ve projenin mimarlarından biri.

242
00:13:32,320 --> 00:13:35,253
Sonra Ferdi ve Lara Lübnan'a döndüler

243
00:13:35,293 --> 00:13:38,253
tohum projesinin nasıl başlatılacağını,

244
00:13:38,290 --> 00:13:41,270
nasıl üretim yapılacağını, bütün bunların nerede olacağını,

245
00:13:41,308 --> 00:13:43,228
ne yapılması gerektiğini görmek için,

246
00:13:43,242 --> 00:13:44,927
bir ay kadar

247
00:13:44,936 --> 00:13:46,592
bizimle kaldılar,

248
00:13:46,602 --> 00:13:48,545
hemen kaynaştık ve sonra da hep öyle gitti.

249
00:13:49,727 --> 00:13:51,303
Daha önce de dediğim gibi:

250
00:13:51,336 --> 00:13:54,174
Bu işe dahil olmak için can atıyordum!

251
00:13:54,988 --> 00:13:56,898
-Ve hayatınızı değiştirdiniz?

252
00:13:57,068 --> 00:13:59,101
Daha öncesinde ne yapıyordunuz?

253
00:14:00,112 --> 00:14:03,303
Hemen öncesinde bir üniversitede

254
00:14:03,336 --> 00:14:04,823
iletişim işinde çalışıyordum.

255
00:14:04,851 --> 00:14:06,960
Fakat aslen kütüphaneciyim,

256
00:14:09,520 --> 00:14:12,640
psikoloji okudum, edebiyat okudum,

257
00:14:12,672 --> 00:14:15,868
tohumlarla hiç alakası yok!

258
00:14:19,745 --> 00:14:21,520
Fakat çeviri yaparken yardımı oluyor,

259
00:14:21,543 --> 00:14:22,922
o yüzden de harika!

260
00:14:23,275 --> 00:14:24,992
-Devam etmek istiyor musunuz?

261
00:14:25,920 --> 00:14:27,637
Evet!

262
00:14:27,661 --> 00:14:29,863
Başta da demiştik:

263
00:14:29,890 --> 00:14:33,002
acil bir durumun ortasındayız,

264
00:14:34,164 --> 00:14:39,185
Bu çalışmanın gerçekten hayati olduğunu düşünüyorum,

265
00:14:40,014 --> 00:14:41,477
çok gerekli.

266
00:14:41,515 --> 00:14:47,298
Ve bir çok açıdan ödüllendirici olduğunu söyleyebilirim:

267
00:14:47,310 --> 00:14:50,555
toplumsal açıdan ve siyasi açıdan da.

268
00:14:50,597 --> 00:14:53,050
Yaptığımız şey inanılmaz derecede önemli

269
00:14:53,072 --> 00:14:54,823
ve aynı zamanda da güzel.

270
00:14:55,247 --> 00:14:58,211
Yakın bir tarihte bırakacağımı hiç sanmıyorum!

271
00:14:58,418 --> 00:15:01,797
-Müthiş! Bu söyleşi için çok teşekkürler.

272
00:15:01,960 --> 00:15:03,722
Rica ederim, rica ederim!

273
00:15:03,755 --> 00:15:06,290
Sizinle sohbet etmek çok keyifliydi.

274
00:15:07,620 --> 00:15:08,936
-Yeniden görüşebilmeyi umuyorum.

275
00:15:08,960 --> 00:15:11,134
Sizi çiftliğe bekliyoruz!

276
00:15:11,181 --> 00:15:15,811
-Bunu çok isterim :-) Fakat söz vermem mümkün değil…

277
00:15:17,463 --> 00:15:20,560
Ne zaman gelmek isterseniz, geleceğinizi haber vermeniz yeterli,

278
00:15:20,583 --> 00:15:22,672
şimdiden "ehlen ve sehlen" (hoşgeldiniz!)

279
00:15:25,251 --> 00:15:26,409
-Çok sağolun!

280
00:15:26,432 --> 00:15:27,091
Harika!

281
00:15:28,120 --> 00:15:35,720
Serge Harfouche ve Buzuruna Juzuruna'daki herkese çok teşekkürler
Daha fazla bilgi için: @buzurunajuzuruna (Instagram)
Fotoğraflar: Charlotte Joubert, Buzuruna Juzuruna, Rabih Yassine
Söyleşi & editing: Erik D'haese

282
00:15:35,720 --> 00:15:40,320
www.DIYseeds.org
tohum üretimi üzerine eğitsel filmler
