# Copyright (C) 2021 Maxime Lecoq
# This file is distributed under the GNU General Public License v2 or later.
msgid ""
msgstr ""
"Project-Id-Version: Diyseeds 2.0\n"
"POT-Creation-Date: 2021-07-10T00:00:00+02:00\n"
"PO-Revision-Date: 2023-03-30 10:12+0000\n"
"Last-Translator: Bediz Yilmaz <bedizyilmaz@yahoo.com>\n"
"Language-Team: Turkish <http://translate.diyseeds.org/projects/"
"diyseeds-org-pages/team/tr/>\n"
"Language: tr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 4.8\n"
"WPOT-Type: post-types/page\n"
"WPOT-Origin: team\n"
"X-Domain: wpot\n"

#. This title will be displayed into the browser's window title bar.
msgctxt "browser-title"
msgid "The Diyseeds Team"
msgstr "Diyseeds Ekibi"

#. The slug is the last part of the URL (www.example.org/slug-is-here). Only alphanumerical characters are allowed, use dash to replace space. The slug should be short and nice and it must reflect the content it refers to. It is usually made from the title. If you don't provide it, a slug will be auto-generated.
msgctxt "slug"
msgid "team"
msgstr "ekip"

#. Title of a page's section.
msgctxt "section-title"
msgid "Team"
msgstr "Ekip"

#. Title of a page's section.
msgctxt "section-title"
msgid "The directors : Martina Widmer and Sylvie Seguin"
msgstr "Yönetmenler: Martina Widmer ve Sylvie Seguin"

msgid "Martina Widmer and Sylvie Seguin from the Longo Maï Cooperative and the European Civic Forum have experience in market gardening and seed production since the early 2000s thanks to collaboration with Kokopelli. They have organized Seed Swaps and have participated in the network of seed producers for Kokopelli's “Seeds without borders” network. Both are involved in the European Civic Forum's public awareness campaigns against restrictive seed legislation and in favour of the access to open-pollinated heritage seeds."
msgstr ""
"Avrupa Sivil Forumu'ndan Martina Widmer ve Longo Maï Kooperatifinden Sylvie "
"Seguin, Kokopelli ile işbirliği sayesinde 2000'li yılların başından bu yana "
"bahçecilik ve tohum üretiminde deneyim sahibidir. Her ikisi de Tohum "
"Takasları düzenledi ve Kokopelli'nin \"Sınır Tanımayan Tohumlar\" ağı "
"kapsamında tohum üreticileri ağına katıldı. Ayrıca, Avrupa Sivil Forumu'nun "
"kısıtlayıcı tohum mevzuatına karşı ve açık tozlaşan ata tohumlarına erişim "
"lehine halkı bilinçlendirme kampanyalarında yer alıyorlar."

#. Title of a page's section.
msgctxt "section-title"
msgid "The co-director : Olga Widmer"
msgstr "Yardımcı yönetmen: Olga Widmer"

msgid "Director, head camerawoman, editor, Olga Widmer has worked in documentary cinema since the early 2000s. She has produced several documentaries, including one for the Longo Maï association in Switzerland on agricultural policy issues."
msgstr ""
"Yönetmen, baş kameraman ve kurgucu Olga Widmer 15 yılı aşkın bir süredir "
"belgesel sinema alanında faaliyet gösteriyor. İsviçre'deki Longo Maï derneği "
"için tarım politikası konularında bir belgesel de dahil olmak üzere birçok "
"belgesel üretti."

#. Title of a page's section.
msgctxt "section-title"
msgid "Drawings and animated drawings: Myleine Guiard Schmid"
msgstr "Çizim ve animasyonlar: Myleine Guiard Schmid"

msgid "Myleine Guiard Schmid is a creator of animated films and works in visual arts. Cinema, sculpture, painting... Art is her passion, her profession. Learning the techniques, knowing how to listen and interact with others allowed her to give meaning to her work. Art has an undeniable strength, that of being able to show, tell, denounce with beauty and style."
msgstr ""
"Myleine Guiard-Schmid, sinema için çizimler ve animasyonlar yaratır. Sinema, "
"heykel, resim, tüm görsel sanatlar onun tutkusu, mesleğidir. Teknikler "
"öğrenerek, dinlemeyi bilerek, başkalarıyla değiş tokuşlar yaparak işine "
"anlam katmayı sever. Sanat, güzellik ve güçlü bir tarzla göstermek, anlatmak "
"ve ifşa etmek için yadsınamaz bir güce sahiptir."

#. Title of a page's section.
msgctxt "section-title"
msgid "The film producers"
msgstr "Film yapımcıları"

msgid "Longo maï was founded in 1973. It is made up of ten self-managed cooperative farms in Europe. They produce and process a wide variety of local cereal and vegetable crops. All of the cooperatives produce part of their own seeds and organise regular seed swaps, thereby helping to increase interest for such varieties and encouraging more people to produce their own seeds."
msgstr ""
"1973 yılında kurulan Longo maï hareketi, altı Avrupa ülkesine yayılmış, "
"kendi kendini yöneten on tarım ve zanaat kooperatifinden oluşan bir ağdır. "
"Kooperatiflerde tahıl, yem ve sebze tohumları üretilir. Tüm kooperatifler "
"kendi tohumlarının bir kısmını üretir, düzenli tohum takasları düzenler. "
"Böylece bu çeşitlere olan ilginin artmasını ve daha fazla insanın kendi "
"tohumlarını üretmesini teşvik eder."

msgid "The European Civic Forum is an international solidarity network active in many fields, including the defence of open-pollinated seeds and small-scale farming that respects social rights."
msgstr ""
"Avrupa Sivil Forumu, bir çok alanda aktif olan uluslararası bir dayanışma "
"ağıdır, bunlar arasında açık tozlanan tohumları ve sosyal haklara saygılı "
"küçük ölçekli çiftçiliği savunma da ön sırada yer alır."

msgid "Sylvie Seguin"
msgstr "Sylvie Seguin"

msgid "Martina Widmer"
msgstr "Martina Widmer"

msgid "Olga Widmer, François and Mayra Aymonier"
msgstr "Olga Widmer, François ve Mayra Aymonier"

msgid "Myleine Guiard Schmid"
msgstr "Myleine Guiard Schmid"

#. This title will be displayed by search engines like Google. 60 characters max.
msgctxt "seo-title"
msgid "Discover the team behind Diyseeds"
msgstr "Diyseeds'in arkasındaki ekibi keşfedin"

#. This description will be displayed below the title on search engine results. 160 characters max.
msgctxt "seo-description"
msgid "Released in 2015, the Diyseeds films were directed by Sylvie Seguin, Martina and Olga Widmer. They are produced by Longo Maï and the European Civic Forum."
msgstr ""
"2015 yılında vizyona giren Diyseeds filmlerinin yönetmenliğini Sylvie "
"Seguin, Martina ve Olga Widmer üstlendi. Filmlerin yapımcıları ise Longo Maï "
"ve Avrupa Sivil Forumu."

#. The tags are used by search engines to reference the content. Use commas to separate tags.
msgctxt "seo-tags"
msgid "diyseeds, team, direction, production, film, images, illustrations, drawings"
msgstr ""
"Diyseeds, ekip, yönetici, yapımcı, film, imajlar, çizimler, illüstrasyon"
