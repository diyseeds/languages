﻿1
00:00:11,040 --> 00:00:14,075
Lahana, 
Turpgiller (Brassicaceae) familyasının,

2
00:00:14,570 --> 00:00:17,565
Brassica oleracea türünün

3
00:00:19,715 --> 00:00:21,860
ve capitata alt türünün üyesidir.

4
00:00:22,845 --> 00:00:27,095
Brassica oleracea türü
alabaşı da içerir,

5
00:00:27,710 --> 00:00:33,725
brokoli, Brüksel lahanası, kale, 
karnabahar ve Milano lahanasını da içerir. 

6
00:00:38,300 --> 00:00:43,800
Lahanalar yeşil, beyaz veya kırmızı olmak üzere 
farklı renklerde olabilir,

7
00:00:44,915 --> 00:00:48,920
başları da sivri veya yuvarlak olabilir.

8
00:00:49,985 --> 00:00:55,490
Bu alt türün tipik özellikleri, 
sıkı bir baş oluşturan pürüzsüz yapraklarıdır. 

9
00:01:06,560 --> 00:01:11,145
Oleracea türünün 
tüm lahanalarının tozlaşması: 

10
00:01:27,190 --> 00:01:31,130
Brassica oleracea türlerinin 
çiçekleri hermafrodittir,

11
00:01:34,035 --> 00:01:38,030
yani hem erkek 
hem de dişi organlara sahiptirler.

12
00:01:41,960 --> 00:01:44,460
Çoğu kendi kendini dölleyemez:

13
00:01:45,065 --> 00:01:50,075
Bir bitkinin çiçeklerinden gelen polen 
sadece başka bir bitkiyi dölleyebilir.

14
00:01:53,930 --> 00:01:56,075
Yani bu bitkiler allogamdır.

15
00:01:57,085 --> 00:02:01,865
İyi bir tozlaşma sağlamak için 
birkaç bitki yetiştirmek daha iyidir. 

16
00:02:05,475 --> 00:02:08,490
Böcekler tozlaşmanın aracılarıdır.

17
00:02:11,895 --> 00:02:16,250
Bu özellikleri büyük bir 
doğal genetik çeşitliliğin oluşmasını sağlar.

18
00:02:23,295 --> 00:02:27,615
Brassica oleracea türünün 
tüm lahana alt türleri

19
00:02:27,785 --> 00:02:29,340
birbirleriyle çaprazlanabilir.

20
00:02:31,235 --> 00:02:35,855
Bu nedenle de farklı lahana türlerini 
tohumluk olarak birbirlerine yakın yetiştirmemelisiniz. 

21
00:02:40,370 --> 00:02:46,035
Saflığı sağlamak için, 
Brassica oleracea türünün farklı çeşitleri

22
00:02:46,420 --> 00:02:49,645
en az 1 km arayla ekilmelidir. 

23
00:02:50,960 --> 00:02:53,780
İki çeşit arasında çit gibi doğal bir bariyer varsa

24
00:02:54,020 --> 00:02:58,210
bu mesafe 500 metreye 
kadar düşürülebilir. 

25
00:03:01,560 --> 00:03:06,720
Çeşitler, sinek telleri kullanıp 
bunları dönüşümlü olarak açıp kapatarak

26
00:03:06,955 --> 00:03:09,155
veya kapalı bir sinek telinin içine

27
00:03:14,310 --> 00:03:18,000
böcekli küçük kovanlar 
yerleştirerek de izole edilebilir.

28
00:03:22,485 --> 00:03:23,605
Bu teknik için,

29
00:03:23,770 --> 00:03:28,090
"Tohum üretiminin ABC'si" bölümündeki 
izolasyon teknikleri modülüne bakın. 

30
00:03:41,380 --> 00:03:43,380
Lahananın yaşam döngüsü 

31
00:03:58,620 --> 00:04:02,220
Lahana, tüketilen ve 
tohumluk ayrılan bitkileri

32
00:04:02,475 --> 00:04:04,985
aynı şekilde yetiştirilen iki yıllık bir bitkidir.

33
00:04:07,115 --> 00:04:08,270
İlk yılında,

34
00:04:08,610 --> 00:04:12,195
bitki kış boyu kalacak 
ve ertesi yıl çiçek açacak

35
00:04:12,500 --> 00:04:14,415
bir baş oluşturur. 

36
00:04:22,055 --> 00:04:25,735
Ekim zamanı iklim koşullarına, 
kışlama yöntemine

37
00:04:25,935 --> 00:04:29,730
ve çeşidin erkenciliğine 
göre belirlenir. 

38
00:05:24,900 --> 00:05:27,200
Lahana başlarını kışlatmak için,

39
00:05:27,390 --> 00:05:30,400
Mayıs ortasında veya Haziran başında,

40
00:05:30,930 --> 00:05:33,530
hatta erkenci çeşitler için daha da geç ekim yapın,

41
00:05:34,065 --> 00:05:38,996
bu sayede sonbaharın sonunda başların aşırı gelişmesi 
ve ikiye bölünmesi sorunundan kaçınabilirsiniz. 

42
00:05:42,923 --> 00:05:46,981
Daha küçük ama sıkı olan başlar 
daha iyi kışlayacaktır. 

43
00:05:50,952 --> 00:05:57,294
Kış sonunda 10 ila 15 bitki kalması amacıyla 
ilk yıl en az 30 bitki saklanmalıdır.

44
00:06:06,196 --> 00:06:08,661
Tohumlar tüm büyüme dönemi boyunca gözlemlenen

45
00:06:08,792 --> 00:06:12,036
sağlıklı bitkilerden seçilir, 
böylece tohumu alınacak bitkilerin

46
00:06:12,196 --> 00:06:15,450
tüm özelliklerini bilmek mümkün olur. 

47
00:06:21,800 --> 00:06:23,992
En kuvvetli başlar, çeşidin kendine has özelliklerine

48
00:06:24,065 --> 00:06:27,352
düzenli ve kuvvetli büyüme, ve sizin 
seçim kriterlerinize

49
00:06:27,498 --> 00:06:29,330
uygun olarak belirlenir:

50
00:06:30,160 --> 00:06:34,560
sıkı başların oluşumu, 
saklanma kapasitesi,

51
00:06:34,865 --> 00:06:40,734
erkencilik ve 
soğuğa dayanıklılık gibi. 

52
00:06:43,563 --> 00:06:48,276
Kışlama, tohum üretiminin 
en hassas dönemidir.

53
00:06:49,680 --> 00:06:54,996
İklim koşullarına, 
vejetasyon dönemine ve mevcut altyapıya

54
00:06:55,200 --> 00:06:59,018
bağlı olarak bitkileri kışlatmak için 
çeşitli yöntemler vardır. 

55
00:07:11,854 --> 00:07:14,200
Kışı sert geçen bölgelerde

56
00:07:14,523 --> 00:07:20,072
bitkinin tamamı kökleriyle birlikte 
sonbaharın sonunda hasat edilir.

57
00:07:21,949 --> 00:07:25,949
Dış yapraklar, 
sadece başın sıkı ve sert yaprakları

58
00:07:26,130 --> 00:07:28,414
kalacak şekilde çıkarılır.

59
00:07:36,392 --> 00:07:39,578
Başlar kuru ve topraksız olmalıdır. 

60
00:07:44,080 --> 00:07:48,254
Havadaki nem oranının 
düşük olduğu bölgelerde,

61
00:07:48,850 --> 00:07:52,552
bitkiler toprak zeminli 
bir mahzende saklanabilir. 

62
00:07:57,560 --> 00:08:04,196
Havadaki nemin yüksek olduğu bölgelerde 
bitkiler donmayan bir odada veya tavan arasında saklanabilir.

63
00:08:05,607 --> 00:08:10,007
Lahanalar -5°C'ye kadar 
kısa süreli donlara dayanabilir,

64
00:08:10,145 --> 00:08:17,140
yine de oda sıcaklığı uzun süre 
0°C'nin altına düşmemelidir. 

65
00:08:19,280 --> 00:08:22,800
Kış boyunca lahanalara 
göz kulak olmak gerekir.

66
00:08:23,870 --> 00:08:29,440
Dış yapraklar, gri küf (Botrytis cinera) 
saldırısına uğrayabilir.

67
00:08:30,320 --> 00:08:33,120
Bunlar çürümüş kısımlarla 
birlikte çıkarılmalı

68
00:08:33,338 --> 00:08:37,149
ve ardından yaralar 
odun külü ile dezenfekte edilmelidir. 

69
00:08:40,370 --> 00:08:45,905
Bazı çok dayanıklı çeşitler 
ve kışı ılıman geçen bölgelerde yetişenler

70
00:08:46,349 --> 00:08:48,930
kış boyunca 
bırakılabilir. 

71
00:08:52,843 --> 00:08:56,829
Ilıman iklimlerde 
toprakta da saklanabilirler:

72
00:08:57,265 --> 00:09:00,814
bitkiler kökleriyle birlikte 
derin kanallara yerleştirilir,

73
00:09:01,000 --> 00:09:04,400
hafifçe yukarı eğilir ve üzeri toprakla örtülür.

74
00:09:08,203 --> 00:09:10,552
Bitkiler birbirine değmemeli,

75
00:09:11,180 --> 00:09:15,200
don olduğunda ise 
cam bir bölme,

76
00:09:15,483 --> 00:09:17,890
gübre veya kuru yapraklarla örtülmelidir. 

77
00:09:28,705 --> 00:09:33,789
Bu koruyucu örtü ilkbaharda kaldırılmalı 
ancak bitkiler yeniden dikilmemelidir.

78
00:09:34,276 --> 00:09:37,985
Bitkiler üzerlerini örten toprağın içinden 
yukarı doğru çıkacak ve çiçek açacaklardır. 

79
00:09:47,040 --> 00:09:51,090
Bir başka teknik de köklerin 
başları kesilerek saklanmasıdır,

80
00:09:51,454 --> 00:09:52,647
bu başlar yenebilir.

81
00:09:53,207 --> 00:09:56,203
Yaz sonunda, kurak bir dönemde,

82
00:09:56,552 --> 00:10:00,014
başları tabandan hafif bir açıyla kesilir.

83
00:10:00,980 --> 00:10:03,854
Sadece gövde ve kökler saklanır. 

84
00:10:07,963 --> 00:10:12,363
Bunlar birkaç gün kurumaya bırakılır 
ve odun külü ile dezenfekte edilirler.

85
00:10:13,563 --> 00:10:18,145
Çürümeyi önlemek için 
kesim yeri aşı macunu ile kaplanabilir. 

86
00:10:23,818 --> 00:10:28,123
Bu çoğaltma yöntemi 
kışı geçirmeyi mümkün kılar,

87
00:10:28,334 --> 00:10:33,127
ama bu şekilde korunan gövdeler 
daha düşük kalitede olur ve daha az tohum üretir.

88
00:10:35,054 --> 00:10:38,058
Sapın merkezinden, yani en güzel tohum saplarının

89
00:10:38,378 --> 00:10:42,072
en iyi tohumları ürettiği yerden 
çiçek açamazlar. 

90
00:10:47,360 --> 00:10:52,029
Kışın bir mahzende 
ya da tavan arasında saklanan tohumluk bitkiler,

91
00:10:52,530 --> 00:10:56,021
döngünün ikinci yılının ilkbaharında, 
Mart ya da Nisan ayında,

92
00:10:56,247 --> 00:10:57,927
yeniden ekilir. 

93
00:11:01,680 --> 00:11:08,327
Lahanalar, başlarının üst kısmı toprak seviyesinde 
olacak şekilde 60 cm aralıklarla gömülür.

94
00:11:09,360 --> 00:11:11,890
Bitkiler yeni kökler geliştirecektir.

95
00:11:14,596 --> 00:11:18,269
Bitkilerin ekim ve 
kök büyüme dönemlerinde

96
00:11:18,480 --> 00:11:21,112
iyi sulanması önemlidir. 

97
00:11:22,960 --> 00:11:26,283
Çiçek saplarının başlardan çıkmasını 
teşvik etmek için

98
00:11:26,560 --> 00:11:32,910
genellikle başın üst kısmında 
bıçak yardımıyla

99
00:11:33,170 --> 00:11:34,327
haç şeklinde bir kesik atmak gerekir.

100
00:11:34,720 --> 00:11:39,980
Bu, başın büyüklüğüne bağlı olarak 
3 ila 6 cm derinlikte olmalıdır.

101
00:11:41,541 --> 00:11:46,843
Tohum saplarının büyüyeceği yer olan 
lahananın tabanına zarar vermemeye dikkat edin.

102
00:11:48,865 --> 00:11:55,098
Bir çiçek sapı çıkmazsa 
bazen kesiyi tekrarlamak gerekebilir. 

103
00:12:11,440 --> 00:12:14,792
Merkezi çiçek sapı 
en iyi tohumları üretir.

104
00:12:17,032 --> 00:12:22,400
Bu sapın daha iyi gelişmesini sağlamak 
ve bitkinin tüm gücünü tohum üretimine

105
00:12:22,523 --> 00:12:27,905
yönlendirmek için 
zayıf ikincil saplar bitkiden çıkarılabilir. 

106
00:13:03,250 --> 00:13:06,632
Tohum sapları 2 metre 
yüksekliğe ulaşabildiğinden,

107
00:13:07,076 --> 00:13:09,629
her bitkiyi kazığa bağlamak

108
00:13:09,956 --> 00:13:14,232
ve tohumlar oluştuğunda 
çok ağırlaşabilen sapları sabitlemek gerekir. 

109
00:13:35,992 --> 00:13:42,821
Tüm Brassica oleracea'nın hasat edilmesi, 
ayıklanması, ayrıştırılması ve depolanması 

110
00:13:51,832 --> 00:13:55,636
Tohumlar, tohum kabukları 
bej rengini aldığında olgunlaşmış demektir.

111
00:13:59,810 --> 00:14:02,392
Kabukları çok gevşektir,

112
00:14:02,560 --> 00:14:07,665
yani olgunlaştıklarında kolayca açılır 
ve tohumlarını saçarlar. 

113
00:14:16,138 --> 00:14:20,218
Çoğu zaman sapların hepsi 
aynı anda olgunlaşmaz.

114
00:14:21,207 --> 00:14:27,410
Tohum israfını önlemek için, 
hasat her sap olgunlaştıkça yapılabilir.

115
00:14:28,920 --> 00:14:34,900
Tüm bitki, tohumların hepsi 
tamamen olgunlaşmadan da hasat edilebilir. 

116
00:14:37,992 --> 00:14:43,178
Olgunlaşma süreci daha sonra nemsiz, 
iyi havalandırılan bir yerde

117
00:14:43,320 --> 00:14:45,403
kurutularak tamamlanır. 

118
00:14:57,621 --> 00:15:03,781
Tohum kabuklarının elle kolayca açılabilmesi 
lahana tohumlarının çıkarılmaya hazır olduğunu gösterir. 

119
00:15:05,730 --> 00:15:07,130
Tohumları çıkarmak için,

120
00:15:07,258 --> 00:15:12,574
tohum kabukları 
plastik bir yaygı veya kalın bir kumaş parçası

121
00:15:12,952 --> 00:15:15,789
üzerine yayılır ve ardından elle dövülür veya ovulur.

122
00:15:19,200 --> 00:15:24,429
Ayrıca bir torbaya koyup 
yumuşak bir yüzeyde üstlerine vurabilirsiniz. 

123
00:15:25,672 --> 00:15:30,589
Daha büyük miktarlar, 
üzerlerinde yürünerek veya araba ile geçerek ayrıştırılabilir. 

124
00:15:42,960 --> 00:15:48,312
Kolayca açılmayan tohum kabukları 
muhtemelen iyi çimlenmeyecek,

125
00:15:48,509 --> 00:15:50,480
olgunlaşmamış tohumlar içerir. 

126
00:15:54,830 --> 00:15:56,232
Ayıklama sırasında,

127
00:15:56,480 --> 00:16:01,003
tohumlar önce samanları tutan 
geniş delikli bir elekten geçirilerek

128
00:16:01,345 --> 00:16:03,220
ve ardından tohumları tutan

129
00:16:07,832 --> 00:16:10,538
ancak daha küçük parçacıkların düşmesine izin veren

130
00:16:10,850 --> 00:16:15,716
başka bir elekten geçirilerek 
saptan samandan arındırılır. 

131
00:16:19,170 --> 00:16:24,865
Son olarak, tohumların üzerine üfleyerek 
ya da rüzgâr yardımıyla

132
00:16:25,040 --> 00:16:27,636
çer çöpü temizlemelisiniz. 

133
00:16:42,603 --> 00:16:47,745
Tüm Brassica oleracea türlerinin 
tohumları birbirine benzer.

134
00:16:48,545 --> 00:16:55,258
Örneğin lahana ve karnabahar tohumlarını 
birbirinden ayırdetmek çok zordur.

135
00:16:55,760 --> 00:16:58,821
Bu nedenle bitkileri de, çıkarılan tohumları da

136
00:16:58,930 --> 00:17:02,734
türün adı, çeşidi ve 
ekim yılı ile

137
00:17:03,010 --> 00:17:06,232
etiketlemek önemlidir. 

138
00:17:07,447 --> 00:17:12,261
Tohumları birkaç gün dondurucuda saklamak 
parazitleri ortadan kaldırır. 

139
00:17:17,665 --> 00:17:20,850
Lahana tohumları 5 yıla kadar çimlenebilmektedir.

140
00:17:21,854 --> 00:17:25,607
Ancak bu kapasitelerini 
10 yıla kadar korumaları mümkündür.

141
00:17:27,338 --> 00:17:30,727
Dondurucuda saklamak 
bu süreyi uzatır.

142
00:17:31,629 --> 00:17:38,196
Bir gramında, 
çeşidine göre 250 ila 300 tohum bulunur. 
