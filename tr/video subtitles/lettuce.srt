﻿1
00:00:10,210 --> 00:00:13,300
Lettuce is a member of the Asteraceae family

2
00:00:13,700 --> 00:00:15,980
and the Lactuca sativa species,

3
00:00:16,460 --> 00:00:19,550
which is divided into four main types:

4
00:00:21,505 --> 00:00:26,300
- Head lettuce, which includes
butter head lettuce with smooth,

5
00:00:26,420 --> 00:00:28,060
barely indented leaves

6
00:00:30,905 --> 00:00:35,860
and Batavia lettuce with crispy,
more or less indented leaves

7
00:00:38,845 --> 00:00:43,060
- Romaine lettuces
with an oblong shape and long leaves 

8
00:00:45,975 --> 00:00:49,100
- Leaf lettuce that generally
does not form a heart,

9
00:00:49,540 --> 00:00:53,620
certain varieties of which have leaves
with very frilly edges 

10
00:00:58,260 --> 00:01:03,660
- Asparagus lettuce or celtuce,
cultivated especially in Asia,

11
00:01:04,180 --> 00:01:07,700
which is grown for its succulent stem
and tender leaves. 

12
00:01:13,995 --> 00:01:16,580
An important characteristic of lettuces

13
00:01:16,805 --> 00:01:20,400
is their ability to adapt to the climate
and the seasons.

14
00:01:28,140 --> 00:01:31,985
There are varieties that are particularly
resistant to the winter cold

15
00:01:32,880 --> 00:01:35,820
or to heat waves and that are slow to bolt. 

16
00:01:39,440 --> 00:01:42,460
There are lettuces adapted
to all climate conditions. 

17
00:01:50,510 --> 00:01:51,540
Pollination 

18
00:02:00,395 --> 00:02:03,250
The lettuce flower is called a capitulum.

19
00:02:07,010 --> 00:02:10,060
It is hermaphrodite and self-pollinating,

20
00:02:11,450 --> 00:02:15,820
which means that the male and female organs
are in the same flower

21
00:02:16,890 --> 00:02:18,580
and compatible with each other.

22
00:02:20,540 --> 00:02:23,300
The flower is therefore autogamous. 

23
00:02:26,800 --> 00:02:31,820
There is a risk of cross-pollination
between different varieties by insects,

24
00:02:35,095 --> 00:02:38,035
the hotter the climate the greater the risk. 

25
00:02:44,640 --> 00:02:46,260
In some regions of the world,

26
00:02:46,615 --> 00:02:50,980
there are wild varieties of lettuce
which can cross with cultivars. 

27
00:02:58,400 --> 00:02:59,660
To avoid crossing,

28
00:03:00,010 --> 00:03:05,140
two varieties of lettuce are grown at least
several meters apart from each other

29
00:03:05,365 --> 00:03:06,740
in temperate regions

30
00:03:07,205 --> 00:03:10,155
and with an even greater distance in hot regions. 

31
00:03:12,500 --> 00:03:16,755
The varieties can also be isolated
using mosquito net cages.

32
00:03:17,540 --> 00:03:18,590
For this technique,

33
00:03:18,635 --> 00:03:24,140
see the module on isolation technics
in “The ABC of seed production”. 

34
00:03:32,845 --> 00:03:36,940
The planting of different varieties
can also be spaced out

35
00:03:37,105 --> 00:03:39,595
to avoid them flowering at the same time.

36
00:03:41,680 --> 00:03:42,700
Nevertheless,

37
00:03:42,945 --> 00:03:47,915
seed should be sown early enough so that
the plant has enough time to produce seed. 

38
00:04:02,815 --> 00:04:03,940
Life cycle 

39
00:04:32,855 --> 00:04:37,630
Lettuce for seed is grown in the same way
as lettuce grown for food. 

40
00:04:53,290 --> 00:04:59,100
At least a dozen plants for seed are necessary
to maintain good genetic diversity. 

41
00:05:02,990 --> 00:05:05,940
Much care is taken to select plants

42
00:05:06,080 --> 00:05:10,340
for the specific characteristics
of the variety such as shape,

43
00:05:10,915 --> 00:05:13,540
colour or growing season.

44
00:05:14,520 --> 00:05:19,265
If you do not do this,
you will lose these characteristics over time. 

45
00:05:21,005 --> 00:05:23,530
Head lettuce has a heart of compact leaves.

46
00:05:24,345 --> 00:05:27,880
Batavia lettuce has crispy, incised leaves. 

47
00:05:28,160 --> 00:05:29,780
As its name indicates,

48
00:05:29,950 --> 00:05:33,660
winter lettuce is cultivated
during the cold time of the year

49
00:05:34,050 --> 00:05:36,660
and produces seed the following spring.

50
00:05:37,205 --> 00:05:40,815
It thus maintains its ability
to endure low temperatures. 

51
00:05:43,955 --> 00:05:48,185
You should remove plants
that do not have the specific characteristics.

52
00:05:56,960 --> 00:06:00,380
You should also eliminate lettuces
that have bolted

53
00:06:00,560 --> 00:06:03,465
and that have not completed
their development cycle.

54
00:06:03,850 --> 00:06:06,525
They would only reproduce stunted lettuces. 

55
00:06:10,190 --> 00:06:15,220
Certain varieties of lettuce have difficulty
pushing up the seed stalk through the head,

56
00:06:15,690 --> 00:06:18,405
above all when the head is very compact.

57
00:06:19,480 --> 00:06:24,540
Sometimes you can help them by carefully
cutting a slit in the top of the head

58
00:06:24,820 --> 00:06:27,660
so that the fragile growth bud is not injured.

59
00:06:40,120 --> 00:06:45,345
Alternatively, you can remove the leaves
surrounding the heart one at a time.

60
00:06:55,740 --> 00:06:58,900
The leaves tend to rot when the weather is humid.

61
00:06:59,805 --> 00:07:02,620
When this happens, they must also be removed. 

62
00:07:52,320 --> 00:07:55,705
Flowering lettuce can reach
an average height of one meter

63
00:07:56,000 --> 00:07:59,540
and needs to be staked individually or in groups. 

64
00:08:05,160 --> 00:08:07,500
Depending on environmental conditions,

65
00:08:07,680 --> 00:08:13,260
it takes between 12 and 24 days
for seed to form after blossoming.

66
00:08:16,740 --> 00:08:20,020
The flowerheads of lettuces blossom gradually

67
00:08:20,675 --> 00:08:24,220
and therefore the seeds
do not all mature at the same time.

68
00:08:28,975 --> 00:08:34,260
Buds, flowers and seeds are found
on the same plant at the same time. 

69
00:08:41,050 --> 00:08:45,420
Seed maturity is determined
by harvesting a withered capitulum

70
00:08:45,740 --> 00:08:49,005
and crushing it between
the thumb and index finger.

71
00:08:49,490 --> 00:08:53,740
If the seeds do not separate
and remain stuck within the capitulum,

72
00:08:53,910 --> 00:08:55,380
they must further mature. 

73
00:08:57,380 --> 00:09:02,500
When the seeds easily fall out of the capitulum,
they are ready to be harvested. 

74
00:09:06,190 --> 00:09:09,640
The best seeds are found
on the main stem of the lettuce. 

75
00:09:19,715 --> 00:09:23,815
Weather conditions have a great influence
on the seed harvest.

76
00:09:24,040 --> 00:09:27,580
Rainy, humid weather damages seed formation

77
00:09:27,940 --> 00:09:33,070
and too much humidity
makes the seeds susceptible to fungal attacks. 

78
00:09:39,975 --> 00:09:44,605
In certain regions, plants for seed
should be grown under a shelter. 

79
00:09:48,615 --> 00:09:52,070
Seeds can be harvested in three different ways. 

80
00:09:53,520 --> 00:09:59,955
The first mature capitula can be gathered,
thereby ensuring at least a small harvest. 

81
00:10:00,445 --> 00:10:03,700
Place a bucket, a bag or a sheet

82
00:10:03,850 --> 00:10:08,300
underneath the seed head
and shake it so the seeds fall down. 

83
00:10:13,340 --> 00:10:17,740
It is also possible to wait until
at least half of the capitula are mature

84
00:10:18,040 --> 00:10:22,700
and then cut the flower stems
and place them in a large woven bag.

85
00:10:28,640 --> 00:10:33,580
Hang up the bag in a sheltered,
well-ventilated and dry place.

86
00:10:34,580 --> 00:10:37,620
The seed can thus finish maturing on the plant. 

87
00:10:46,315 --> 00:10:49,740
In case the weather is bad
during the maturing period,

88
00:10:50,155 --> 00:10:53,725
the third possibility involves
uprooting the plants,

89
00:10:54,355 --> 00:10:59,500
putting a bag around the root to prevent the soil
and stones from mixing with the seed

90
00:11:00,020 --> 00:11:04,355
and hanging the plants upside down
in a dry, well-ventilated place.

91
00:11:05,970 --> 00:11:08,725
A good amount of the seeds
will continue to mature. 

92
00:11:14,865 --> 00:11:17,740
Extracting – sorting - storing

93
00:11:23,665 --> 00:11:27,900
The flower stems should be completely dry
when the seed are removed.

94
00:11:29,235 --> 00:11:31,500
The capitula are rubbed by hand

95
00:11:31,785 --> 00:11:34,535
and the majority of seeds come away easily.

96
00:11:38,525 --> 00:11:42,140
Unpollinated flowers will produce empty seeds.

97
00:11:48,630 --> 00:11:53,470
The flower stems can also be beaten
in a bin or other large container. 

98
00:11:58,150 --> 00:12:02,780
The seeds are sorted using sieves
of different mesh sizes. 

99
00:12:16,525 --> 00:12:20,535
Finally, the seeds should be winnowed
to remove any remaining debris.

100
00:12:21,500 --> 00:12:23,260
The seeds are placed on a plate

101
00:12:23,390 --> 00:12:29,020
or a winnowing basket then breathed upon
so that any light chaff is blown away. 

102
00:12:30,940 --> 00:12:32,505
Take advantage of a breeze.

103
00:12:33,230 --> 00:12:37,420
Pour the seeds into a container
placed on the ground outside

104
00:12:37,545 --> 00:12:39,300
and the chaff will be blown away.

105
00:12:40,745 --> 00:12:44,460
Place the container on a sheet
in case there are gusts of wind. 

106
00:12:53,450 --> 00:12:56,260
The seeds are then poured into a plastic bag

107
00:12:56,670 --> 00:13:01,580
in which you should put a label
with the name of the species, the variety,

108
00:13:01,900 --> 00:13:04,870
the year of production
and the place it was grown. 

109
00:13:08,535 --> 00:13:13,005
Storing the seeds in the freezer
for several days kills parasite larvae. 

110
00:13:14,700 --> 00:13:17,980
Lettuce seeds are able to germinate
for 5 years on average

111
00:13:18,210 --> 00:13:21,700
and up to 9 years or longer if kept in the freezer.

112
00:13:26,430 --> 00:13:32,260
When storage conditions are inadequate,
lettuce seed loses its viability very quickly.

113
00:13:34,550 --> 00:13:39,750
A good seed plant can easily produce
10 to 15 grams of seed. 
