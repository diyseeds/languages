1
00:00:15,994 --> 00:00:19,620
Tohum, tozlaşma yoluyla döllenmiş olan 
tohum taslağının

2
00:00:19,780 --> 00:00:21,660
tohuma dönüşmesinin sonucudur.

3
00:00:23,994 --> 00:00:25,820
Besin rezervi ile çevrili bir embriyonun

4
00:00:27,620 --> 00:00:35,500
etrafındaki koruyucu 
tohum zarı tabakasından oluşur.

5
00:00:38,538 --> 00:00:42,260
Bu besin deposunun büyüklüğü 
türden türe değişir.

6
00:00:53,621 --> 00:01:00,180
Tohumların görünümü, 
hangi bitkinin tohumu olduğuna bağlı olarak

7
00:01:00,448 --> 00:01:03,660
şekil, boyut, renk ve dokuları yönünden büyük ölçüde farklılık gösterebilir.

8
00:01:05,728 --> 00:01:09,780
Bu tohumlardan sağlıklı 
ve güçlü bitkiler ortaya çıkacak,

9
00:01:10,060 --> 00:01:15,180
zamanı geldiğinde bunlar da yeni tohumlar üretecek, 
böylece yaşam döngüsü sürüp gidecektir.

10
00:01:21,904 --> 00:01:27,146
Tohumlar, ana bitkiye yakın 
veya daha uzak mesafelere yayılır.

11
00:01:30,474 --> 00:01:33,860
Türlere ve 
özel koşullara bağlı olarak,

12
00:01:34,060 --> 00:01:38,260
tohumlar ana bitkinin 
hemen dibine de düşebilir,

13
00:01:38,580 --> 00:01:42,620
rüzgarla yahut 
tohumların kürküne

14
00:01:42,860 --> 00:01:46,460
yapıştığı hayvanlar sayesinde
çok uzaklara da gidebilir.

15
00:01:52,293 --> 00:01:55,060
Amaç tohumların mümkün olduğu kadar yayılmasıdır.

16
00:01:56,260 --> 00:02:02,380
Rüzgar, su, böcekler, kuşlar ve diğer birçok hayvan

17
00:02:02,700 --> 00:02:06,021
farkında olmadan 
bu yayılmanın taşıyıcısı olabilirler.

18
00:02:07,861 --> 00:02:10,660
Tohumlar, dış koşullar gelişimleri için

19
00:02:10,940 --> 00:02:16,896
uygun olana kadar gerekirse çok uzun süre 
beklemek gibi olağanüstü bir yeteneğe sahiptir.

20
00:02:17,306 --> 00:02:20,380
Bu olaya dormansi adı verilir.

21
00:02:24,597 --> 00:02:29,900
Tohumların dormansi döneminden çıkması 
farklı uyaranlar sayesinde olur.

22
00:02:39,340 --> 00:02:43,520
Bazı tohumlar için 
bu uyanışın koşulu

23
00:02:44,026 --> 00:02:47,740
sindirim enzimleri tarafından aktive edildikleri 
hayvanların bağırsaklarından geçmektir.

24
00:02:48,133 --> 00:02:53,260
Diğerleri bir fermantasyon süreci 
veya soğuklama ile uyarılır.

25
00:02:56,053 --> 00:03:01,500
Bu nedenle tohumların 
uyku halini sona erdirmek için üreticiler

26
00:03:02,060 --> 00:03:04,500
bazı doğal olayları taklit etmeye çalışır.

27
00:03:05,082 --> 00:03:11,820
Ekim yaparken, çimlenmeyi kolaylaştıran 
tüm koşulların karşılandığından emin olmalısınız:

28
00:03:12,580 --> 00:03:15,820
sulama miktarı, sıcaklık, ışık seviyesi,

29
00:03:16,260 --> 00:03:19,460
toprak derinliği,

30
00:03:20,074 --> 00:03:23,380
çeşide uygun 
mevsimin seçimi.

31
00:03:24,160 --> 00:03:25,589
Tohumların ömrü,

32
00:03:26,186 --> 00:03:29,620
yani 
çimlenebilecekleri süre,

33
00:03:30,180 --> 00:03:31,780
türe bağlıdır.

34
00:03:32,160 --> 00:03:37,540
Örneğin yaban havucu tohumları 
tam çimlenme kapasitelerini yalnızca bir yıl muhafaza eder.

35
00:03:38,060 --> 00:03:41,900
Hindiba tohumları için ise 
bu süre on yıldır.

36
00:03:42,380 --> 00:03:46,260
Bu süre geçtikten sonra 
çimlenme oranları düşer.

37
00:03:48,298 --> 00:03:52,420
Verimliliğini ölçmek istediğiniz 
tohumlar varsa,

38
00:03:52,740 --> 00:03:55,420
çimlenme testi yapabilirsiniz.

39
00:03:55,700 --> 00:03:58,340
Bunun için ektiğiniz tohum sayısı ile

40
00:03:59,340 --> 00:04:01,900
çimlenen bitki sayısını 
karşılaştırmalısınız.

41
00:04:03,900 --> 00:04:08,060
Tohumların yaşlandıkça 
daha az çimlendiğini göreceksiniz.

42
00:04:13,946 --> 00:04:19,140
Tohumların ömrü, kurutma ve 
saklama koşullarından da etkilenir.

43
00:04:22,282 --> 00:04:26,220
Her şeyden önce tohumların 
tamamen kuru olduğundan emin olmalısınız.

44
00:04:27,088 --> 00:04:30,580
Daha sonrasında ise, 
sıcak ve nemli bir ortam

45
00:04:31,300 --> 00:04:34,940
tohum kalitesini olumsuz etkileyebileceğinden,
az ışık alan

46
00:04:36,860 --> 00:04:41,260
ve minimum sıcaklık değişimi yaşayan 
serin ve kuru bir ortamda saklanmaları gerekir.

47
00:04:43,248 --> 00:04:47,140
Ayrıca tohumları yiyen 
küçük böceklere de dikkat etmelisiniz.

48
00:04:48,320 --> 00:04:53,740
Tohumları birkaç gün dondurucuda bırakarak 
bunlardan kolayca kurtulabilirsiniz.

49
00:04:56,050 --> 00:04:58,042
Dondurucuya koymak iyi bir saklama yöntemidir

50
00:04:58,469 --> 00:05:05,980
çünkü tüm tohumlar hava geçirmez torbalarda 
-18 dereceye kadar düşük sıcaklıklara dayanabilir.

51
00:05:08,300 --> 00:05:10,220
Hatta bu onların ömrünü uzatır.

52
00:05:16,600 --> 00:05:18,741
Ancak tohumların canlılığını korumak için

53
00:05:19,180 --> 00:05:22,780
en iyi yöntem tohumların 
düzenli olarak ekilip yetiştirilmesidir.

54
00:05:25,061 --> 00:05:28,900
Bu, hızla gelişen çevre 
ve iklim koşullarına

55
00:05:29,060 --> 00:05:30,740
uyum sağlamalarını da mümkün kılar.
