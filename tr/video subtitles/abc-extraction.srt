﻿1
00:00:09,840 --> 00:00:14,960
ABC Tohum çıkarma, sulu yöntem, 
kurutma, tohumların ayrıştırılması

2
00:00:23,560 --> 00:00:28,520
Fermantasyon ve suyla temizleme yoluyla 
tohum çıkarma yöntemi

3
00:00:34,680 --> 00:00:38,040
Fermantasyon ve suyla temizleme yoluyla tohum çıkarma yöntemi

4
00:00:38,200 --> 00:00:41,240
domates ve salatalıklar için kullanılır.

5
00:00:41,840 --> 00:00:46,880
Fermantasyon, her bir tohumu çevreleyen 
ve onu uykuda tutan

6
00:00:46,960 --> 00:00:49,760
jelatinimsi kaplamanın 
ortadan kalkmasını sağlar.

7
00:00:54,480 --> 00:00:58,000
Domates veya salatalık ortadan ikiye kesilir.

8
00:00:59,080 --> 00:01:05,680
Tohumlar, sulu kısımlarla birlikte 
bir kaşık yardımıyla çıkarılarak cam kavanoza konur.

9
00:01:06,680 --> 00:01:09,760
Gerek duyulursa biraz su eklenebilir.

10
00:01:11,480 --> 00:01:15,480
Zarar görmüş veya fermante olmuş sebzelerin 
tohumları alınmamalıdır.

11
00:01:17,280 --> 00:01:21,640
Her kavanoz türün 
ve çeşidin adı ile etiketlenir.

12
00:01:23,080 --> 00:01:27,320
Cam kavanoz, fermantasyon sürecini 
gözlemlemeye olanak sağlar.

13
00:01:28,000 --> 00:01:30,240
Kavanozun kapağını çok sıkı kapatmayın.

14
00:01:31,240 --> 00:01:36,000
Sadece üzerini örtün 
ve sineklerden koruyun.

15
00:01:36,640 --> 00:01:43,680
ardından doğrudan güneş ışığı almayan 
23° ile 30° arasında sıcak bir yere yerleştirin.

16
00:01:49,720 --> 00:01:55,240
Fermantasyon için gereken süre 
hava sıcaklığına

17
00:01:55,320 --> 00:01:58,320
ve fermantasyon sıvısındaki 
şeker miktarına bağlı olarak değişir.

18
00:02:00,440 --> 00:02:05,160
Yavaş yavaş yüzeyde 
beyaz bir küf tabakası

19
00:02:05,640 --> 00:02:07,880
oluşmaya başlayacaktır

20
00:02:08,200 --> 00:02:10,840
daha düzenli bir fermantasyon sağlamak

21
00:02:11,520 --> 00:02:15,160
ve çok kalın bir küf tabakası oluşumunu önlemek için 
arada sırada karıştırmanız gerekir.

22
00:02:17,400 --> 00:02:21,960
Sıvı içerisinde sebzenin 
etli kısmından yeterince mevcut değilse

23
00:02:22,560 --> 00:02:26,360
bir miktar şeker eklemek süreci hızlandıracak 
ve zararlı küflerin oluşmasını engelleyecektir.

24
00:02:31,800 --> 00:02:35,000
Fermantasyon süreci 
yakından izlenmelidir.

25
00:02:35,720 --> 00:02:39,360
Çok sıcak günlerde bu süreç 
48 saatten kısa sürebilir.

26
00:02:40,080 --> 00:02:45,040
Çok uzun süre bekletilirse, 
artık jelatinimsi yüzeyi bulunmayan tohumlar

27
00:02:45,120 --> 00:02:48,560
çimlenmeye başlar 
ve tohum olarak saklanamaz hale gelir.

28
00:02:52,480 --> 00:02:55,960
Tohumlar birer birer 
kavanozun dibine çöküp de

29
00:02:56,080 --> 00:02:59,080
sebzenin etli kısmı ile kabuğu 
su yüzeyine çıktığında,

30
00:02:59,640 --> 00:03:03,720
jelatinimsi kaplamanın 
yok olma işlemi de tamamlanmış olur.

31
00:03:06,240 --> 00:03:07,880
Artık tohumların temizlenme aşamasına geçilebilir.

32
00:03:10,960 --> 00:03:14,800
Bu aşamada bir süzgeç yardımıyla süzülen tohumlar 
akan suyun altında temizlenir.

33
00:03:28,640 --> 00:03:32,360
Fermantasyon olmaksızın 
suyla temizleme yöntemi

34
00:03:35,280 --> 00:03:38,560
Fermantasyon yapmaksızın 
suyla temizleme işlemi

35
00:03:39,000 --> 00:03:41,920
patlıcan, balkabağı, kabak, kavun,

36
00:03:42,040 --> 00:03:44,600
ve karpuz gibi

37
00:03:45,560 --> 00:03:48,280
meyveli sebzeler

38
00:03:49,120 --> 00:03:50,840
için kullanılır.

39
00:03:54,640 --> 00:03:56,720
Tohumlar meyveden çıkarılır

40
00:03:57,200 --> 00:04:00,480
ve bir kevgir içinde 
akan su altında yıkanır.

41
00:04:03,520 --> 00:04:06,840
Tohumlar etli kısımdan 
kolayca ayrılmıyorsa,

42
00:04:07,360 --> 00:04:11,920
et parçalanıp 
tohumlar serbest kalana kadar

43
00:04:12,560 --> 00:04:15,960
12 ila 24 saat 
suda bekletilebilir.

44
00:04:17,080 --> 00:04:21,760
Fermantasyonu önlemek için 
sıcak bir yere koymaktan kaçınmak gerekir.

45
00:04:22,520 --> 00:04:25,160
İşlemin ardından tohumlar 
derhal kurutulmalıdır.

46
00:04:34,240 --> 00:04:35,520
Kurutma

47
00:04:40,200 --> 00:04:43,720
Yıkama işleminden sonra tohumların 
hızlı bir şekilde kurutulması ihmal edilmemelidir.

48
00:04:45,720 --> 00:04:48,640
En fazla iki gün içinde 
kurumuş olmaları gerekir.

49
00:04:50,680 --> 00:04:56,320
Bunun için 23° ila 30° arasında bir sıcaklığa sahip 
iyi havalandırılan kuru bir yerde

50
00:04:56,800 --> 00:05:00,520
ince bir elek veya bir satıh üzerine yerleştirmelisiniz.

51
00:05:06,720 --> 00:05:09,640
Küçük miktarlardaki tohumlar için başka bir yöntem de

52
00:05:10,080 --> 00:05:13,680
yapışmayan 
fakat oldukça emici

53
00:05:13,960 --> 00:05:15,840
kahve filtrelerinden yararlanmaktır.

54
00:05:16,600 --> 00:05:21,200
Her filtreye en fazla 
bir tatlı kaşığı tohum koymalısınız.

55
00:05:23,640 --> 00:05:26,800
Çeşit ve türün adını her filtreye

56
00:05:27,040 --> 00:05:30,360
kalıcı mürekkeple yazmayı 
ihmal etmeyin.

57
00:05:32,000 --> 00:05:34,520
Filtreler, sıcak, kuru ve

58
00:05:34,600 --> 00:05:38,040
iyi havalandırılan bir yere asılır.

59
00:05:38,840 --> 00:05:41,560
Tohumlar 
güneş ışığına maruz bırakılmamalı

60
00:05:42,160 --> 00:05:44,280
ve kesinlikle kağıt ya da peçete üzerinde kurutulmamalıdır,

61
00:05:44,600 --> 00:05:46,080
çünkü yapışırlar ve

62
00:05:46,760 --> 00:05:48,840
çıkarılmaları çok zor olur.

63
00:05:51,960 --> 00:05:54,920
Kuruma sonrası 
çekirdekleri birbirinden ayırmak için

64
00:05:55,200 --> 00:05:57,120
ellerinizin arasında ovalayın.

65
00:06:18,240 --> 00:06:20,040
Tohumların ayrıştırılması

66
00:06:30,680 --> 00:06:33,560
Tohumlar alındıktan sonra 
onları tasnif etmenin farklı yolları vardır.

67
00:06:34,720 --> 00:06:37,400
Sulu veya kuru yöntemlerden yararlanılabilir.

68
00:06:41,280 --> 00:06:46,080
Pırasa, soğan gibi 
etle çevrili olmayan tohumlar

69
00:06:46,520 --> 00:06:48,200
su kullanılarak ayrılabilir.

70
00:06:49,800 --> 00:06:54,240
Şeffaf bir kaba 
bol miktarda su dökülür

71
00:06:54,720 --> 00:06:56,480
ve tohumlar içine bırakılır.

72
00:06:58,880 --> 00:07:01,240
Verimli tohumlar ağır çeker

73
00:07:01,760 --> 00:07:06,520
onların kabın dibine çökmesi için 
su birkaç kez karıştırılır.

74
00:07:07,320 --> 00:07:10,720
Samanla birlikte 
yüzeyde kalan tohumlar

75
00:07:11,360 --> 00:07:13,160
bir kevgir ile alınır.

76
00:07:18,040 --> 00:07:20,600
Ardından dibe düşen tohumları toplamak için

77
00:07:20,680 --> 00:07:23,440
bir elekten yararlanılır.

78
00:07:24,360 --> 00:07:26,280
Bunların derhal kurutulmaları gerekir.

79
00:07:28,200 --> 00:07:32,760
Çok hafif olan tohumların 
bu şekilde ayrılması mümkün değildir.

80
00:07:36,480 --> 00:07:39,880
Kuru ayırma 
en yaygın kullanılan yöntemdir.

81
00:07:59,600 --> 00:08:02,240
Elde ayıklanan büyük tohumlu sebzelerde

82
00:08:02,320 --> 00:08:03,880
örneğin fasulyelerde,

83
00:08:04,320 --> 00:08:07,600
bozuk şekilli veya zarar görmüş 
tohumları ayırmanız yeterlidir.

84
00:08:12,840 --> 00:08:16,240
Tohumlukların dövüldüğü 
veya ezildiği durumlarda

85
00:08:17,320 --> 00:08:19,080
çer çöpün ayıklanması gerekir.

86
00:08:21,720 --> 00:08:24,800
Önce en büyük 
sap parçalarını tutan

87
00:08:25,280 --> 00:08:27,760
çok kaba bir elek kullanılır;

88
00:08:31,440 --> 00:08:34,560
tohumlar ve daha küçük parçalar 
bir kovaya düşer.

89
00:08:36,120 --> 00:08:39,320
İşlem daha sonra tohumları tutan 
ve kalan sap parçalarının

90
00:08:39,760 --> 00:08:43,360
geçmesine izin veren 
ince bir elek kullanılarak tekrarlanır.

91
00:08:50,640 --> 00:08:52,800
Elek seçimi çok önemlidir;

92
00:08:53,200 --> 00:08:57,160
tohumları tutmalı ve mümkün olduğunca 
fazla çer çöpün geçmesine olanak vermelidir.

93
00:09:06,240 --> 00:09:07,640
Temizlemeyi bitirmek için,

94
00:09:07,960 --> 00:09:10,360
tohumlar düz bir kaba dökülür

95
00:09:10,440 --> 00:09:13,600
ve hafif sapları ayırmak için 
üzerlerine hafifçe üflenir.

96
00:09:17,760 --> 00:09:20,160
Tohumları ayırmak için 
rüzgardan da faydalanılabilir.

97
00:09:20,520 --> 00:09:22,320
Yere büyük bir çarşaf serilir.

98
00:09:23,960 --> 00:09:28,160
Üzerine tohumlar dökülürken 
rüzgar samanı savurur.

99
00:09:30,000 --> 00:09:32,600
Bu işlem çok şiddetli rüzgarın 
olmadığı bir günde yapılmalıdır,

100
00:09:32,680 --> 00:09:33,880
çünkü kuvvetli rüzgarlar her şeyi darmadağın eder.

101
00:09:34,480 --> 00:09:36,400
Küçük bir vantilatör veya kompresörden de yararlanılabilir.

102
00:09:41,680 --> 00:09:46,080
Bu yöntem 
ağır tohumlarda etkilidir;

103
00:09:46,560 --> 00:09:48,320
ancak çok hafif tohumlar uçup gidebilir.

104
00:09:54,400 --> 00:09:56,520
Hangi yöntem kullanılırsa kullanılsın,

105
00:09:56,760 --> 00:09:59,480
her zaman bir miktar tohum kaybı yaşanır.

106
00:09:59,800 --> 00:10:04,760
Önemli olan 
ne kadar tohum ayırmak istediğinizi bilmektir.

107
00:10:32,320 --> 00:10:35,600
Doğa o kadar cömerttir ki 
tohumları çoğaltmaya başladığınızda,

108
00:10:35,800 --> 00:10:39,320
kendi bahçeniz için 
ihtiyacınız olandan çok daha fazla miktarda

109
00:10:39,880 --> 00:10:41,640
tohum elde ettiğinizi kısa sürede anlayacaksınız.

110
00:10:43,280 --> 00:10:45,760
Kesinlikle bütün tohumları saklamaya çalışmayın.

111
00:10:46,120 --> 00:10:47,320
Elinizdeki bol bol yeterli olacaktır.
