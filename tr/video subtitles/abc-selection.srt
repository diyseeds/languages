﻿1
00:00:07,700 --> 00:00:12,140
Tohumluk bitkilerin seçimi ve yetiştirilmesi

2
00:00:18,565 --> 00:00:24,090
Seçim sayesinde, bitkinin çevre koşullarına 
ve sizin ihtiyaçlarınız ile isteklerinize

3
00:00:24,650 --> 00:00:26,430
uyum göstermesi sağlanır.

4
00:00:27,140 --> 00:00:32,625
Tohumları için yetiştirilen bitkilere, 
gelecek nesillerin sebze ve meyvelerini üretecek

5
00:00:33,795 --> 00:00:38,280
olan bitkiler oldukları için 
özel bir dikkat göstermek gerekir.

6
00:00:40,560 --> 00:00:43,780
Onları doğru belirlemek ve

7
00:00:44,095 --> 00:00:47,180
gelişimleri boyunca gözlemlemek 
büyük önem taşır. 

8
00:00:52,725 --> 00:00:56,215
Yalnızca meyve bazında 
yapılan seçim

9
00:00:56,825 --> 00:01:01,800
bitkilerin gelişimiyle bağlantılı
tüm özellikleri yansıtmayacaktır. 

10
00:01:04,425 --> 00:01:07,795
Tohum üretimi için bitki seçiminde rol oynayan

11
00:01:08,380 --> 00:01:11,795
tüm kriterler 
titizlikle belirlenmelidir. 

12
00:01:13,860 --> 00:01:18,640
Öncelikle hastalığa dayanıklılık, 
verim, erken gelişme gibi

13
00:01:19,270 --> 00:01:24,270
çeşide özgü kriterlerin yanı sıra tat ve görünüm gibi

14
00:01:24,695 --> 00:01:29,535
daha sübjektif kriterleri de 
göz önünde bulundurmalısınız.

15
00:01:43,890 --> 00:01:47,000
Bitkilerin çevrelerine ve 
yetiştirme yöntemlerine

16
00:01:47,195 --> 00:01:51,035
uyum sağlama becerileri de 
dikkate alınmalıdır. 

17
00:01:54,175 --> 00:01:59,515
Kilit nokta, hangi yönlere 
öncelik vermek istediğinizi en baştan belirlemektir,

18
00:02:00,160 --> 00:02:03,695
çünkü elinizdeki çeşit 
nesiller boyunca değişim gösterecek

19
00:02:03,895 --> 00:02:07,850
ve belirli özellikler az ya da çok 
baskın hale gelecektir. 

20
00:02:13,340 --> 00:02:17,465
Bazen bir seçim süreci yürütmek 
akıllıca olmayabilir.

21
00:02:17,770 --> 00:02:22,450
Böyle bir durumla, 
nesli tükenme riskiyle karşı karşıya olan nadir bir çeşitten

22
00:02:22,740 --> 00:02:24,885
çok az sayıda tohum kaldığında karşılaşılır,

23
00:02:25,120 --> 00:02:28,235
zira ilk yıl seçim yapmaya 
yetecek sayıda

24
00:02:28,305 --> 00:02:30,400
bitki üretilemeyecektir. 

25
00:02:35,765 --> 00:02:41,100
Ayrıca, bir seçim yapmaksızın 
çeşidi olduğu gibi korumaya karar vermek de mümkündür.

26
00:02:41,470 --> 00:02:45,140
Bu durumda, aralarında 
büyük uyuşmazlıklar olsa bile

27
00:02:45,660 --> 00:02:51,220
genetik çeşitliliği teşvik etmek için 
çeşidin tüm unsurları korunmaya çalışılır.

28
00:02:52,195 --> 00:02:55,240
Bu çeşitlilik bitkinin uyum kapasitesini büyük ölçüde geliştirir

29
00:02:55,670 --> 00:02:58,785
ve gelecekteki seçimin temelini oluşturabilecek

30
00:02:58,955 --> 00:03:01,775
bir potansiyeller deposu 
oluşmasını sağlar. 

31
00:03:09,170 --> 00:03:12,870
Tohum üretimi için yetiştirilen bitkileri, 
ürünü için yetiştirilenlerden

32
00:03:14,590 --> 00:03:18,270
ayırt etmek için 
net bir şekilde işaretlemek önemlidir. 

33
00:03:20,885 --> 00:03:25,595
Her çeşidin yerini gösteren 
bir bahçe planı hazırlamak

34
00:03:25,940 --> 00:03:29,150
bitkilerin yanına koyduğunuz etiketlerin 
kaybolması durumunda onları bulmanıza yardımcı olacaktır. 

35
00:03:31,095 --> 00:03:35,920
Tohumluk bitkilerin yaşam döngüsü 
genellikle tüketim amacıyla yetiştirilen

36
00:03:36,125 --> 00:03:38,505
sebzelerden daha uzun sürer.

37
00:03:39,215 --> 00:03:42,865
Örneğin, marul 
2-3 ay sonra tüketilebilir,

38
00:03:43,110 --> 00:03:48,060
ancak tohumlarını üretmesi
5-6 ay kadar sürer.

39
00:04:05,450 --> 00:04:09,180
Ayrıca ekiminin ikinci yılında 
çiçek açan ve tohum üreten

40
00:04:09,570 --> 00:04:13,910
havuç gibi birçok 
iki yıllık bitki de vardır. 

41
00:05:00,780 --> 00:05:06,660
İmkan olduğu ölçüde bahçenin küçük bir köşesini 
tohumluk bitkilerin yetiştirilmesi için ayırmak iyi olur. 
