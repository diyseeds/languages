1
00:00:07,720 --> 00:00:11,940
Patlıcan, Solanaceae 
(Patlıcangiller) familyasına

2
00:00:12,290 --> 00:00:15,060
ve Solanum melongena türüne mensuptur.

3
00:00:20,265 --> 00:00:25,540
Tropikal ülkelerde çok yıllık, 
ılıman bölgelerde ise tek yıllık bir bitkidir.

4
00:00:37,350 --> 00:00:42,100
Büyüklük, şekil ve renk yönünden 
patlıcan bitkisi büyük bir çeşitlilik arz eder.

5
00:00:55,840 --> 00:00:56,740
Tozlaşma 

6
00:01:15,800 --> 00:01:20,060
Patlıcan çiçeği erselik 
ve kendine döllektir,

7
00:01:21,180 --> 00:01:25,700
yani erkek ve dişi organlar 
aynı çiçek üzerinde

8
00:01:26,255 --> 00:01:27,855
ve uyumludur.

9
00:01:32,980 --> 00:01:34,930
Bir başka deyişle patlıcanlar otogam bitkilerdir.

10
00:01:39,505 --> 00:01:42,995
Bununla birlikte, çeşitler arasında çapraz döllenme meydana gelebilir.

11
00:01:44,065 --> 00:01:47,460
Bunun sıklığı çevreye 
ve tozlaşmayı sağlayan böceklerin

12
00:01:47,705 --> 00:01:50,380
sayısına bağlı olarak değişir. 

13
00:01:53,580 --> 00:01:57,220
Rüzgar almayan yerlerde, 
çiçeklenme sırasında

14
00:01:57,515 --> 00:02:02,740
bitkileri düzenli olarak sallamak 
tozlaşmayı teşvik edecektir. 

15
00:02:06,990 --> 00:02:08,940
Çapraz tozlaşmayı önlemek için,

16
00:02:09,350 --> 00:02:14,185
farklı patlıcan çeşitleri 
100 metre mesafeyle yetiştirilmelidir.

17
00:02:16,690 --> 00:02:22,580
Bir çit bitkisi gibi doğal bir engel varsa 
bu mesafe 50 metreye düşürülebilir.

18
00:02:24,840 --> 00:02:30,020
Tropik iklimlerde en az bir kilometre 
mesafe bırakmaya özen gösterin. 

19
00:02:32,935 --> 00:02:36,885
Mekanik izolasyon için 
cibinlikten yararlanılabilir.

20
00:02:38,550 --> 00:02:42,790
Tohum üretiminin ABC'sindeki 
izolasyon teknikleri modülüne bakabilirsiniz. 

21
00:02:55,100 --> 00:02:56,540
Yaşam döngüsü 

22
00:03:11,045 --> 00:03:16,620
Tohumluk amacıyla üretilen patlıcanlar, 
yemeklik olanlarla aynı şekilde yetiştirilir.

23
00:03:27,875 --> 00:03:30,060
İyi bir genetik çeşitlilik sağlamak için

24
00:03:30,235 --> 00:03:34,020
her çeşitten 6 ila 12 
bitki yetiştirmelisiniz. 

25
00:03:37,390 --> 00:03:42,005
Patlıcanlar, iyi bir gelişim göstermek için 
yüksek sıcaklığa ihtiyaç duyan bitkilerdendir.

26
00:03:44,810 --> 00:03:49,340
Bu nedenle ekim zamanı dışarıda 
ekim yapılabilecek tarihe göre ayarlanmalıdır. 

27
00:04:18,605 --> 00:04:23,815
Çiçek açtıktan sonra 
meyvenin tüketime hazır hale gelmesi

28
00:04:23,950 --> 00:04:28,420
çeşide bağlı olarak 
60 ila 100 gün arasında sürer.

29
00:04:29,980 --> 00:04:33,895
Fakat dikkat edin, tohumlar bu aşamada 
henüz hazır değildir. 

30
00:04:38,465 --> 00:04:44,380
Tohum üretimi için belirlenecek bitkiler, 
gelişimleri boyunca gözlemleyebildiğiniz,

31
00:04:44,720 --> 00:04:48,100
sağlıklı ve güçlü olan bitkiler 
arasından seçilmelidir. 

32
00:04:51,625 --> 00:04:56,460
Bitkilerde, düzenli ve güçlü büyüme, 
çok sayıda çiçeğin varlığı,

33
00:04:57,025 --> 00:04:58,500
iyi gelişmiş meyveler

34
00:04:58,960 --> 00:05:04,940
veya soğuk iklimlere iyi uyum 
kapasitesine bakılır. 

35
00:05:05,710 --> 00:05:09,555
Meyveye gelince, en iyi tada, 
çeşidin tipik şekline, büyüklüğüne,

36
00:05:09,940 --> 00:05:13,260
et ve kabuk rengine sahip olma,

37
00:05:14,010 --> 00:05:16,260
meyvede acılık olmaması

38
00:05:16,830 --> 00:05:21,990
veya kabuk kalınlığı
tohumluk seçim kriterleridir. 

39
00:05:25,195 --> 00:05:29,380
Önceden toplanmış patlıcanlardan 
tohum alınmamalıdır,

40
00:05:29,750 --> 00:05:34,780
çünkü bu durumda bitkinin tüm büyüme özelliklerini 
gözlemlemeniz mümkün olmaz. 

41
00:05:40,025 --> 00:05:43,120
Hasta bitkiler de tohum üretimi için seçilmemelidir. 

42
00:05:48,120 --> 00:05:53,660
Tam olgunlukta meyveler yumuşar 
ve renk değiştirir.

43
00:05:55,510 --> 00:06:00,055
Beyaz patlıcanlar sararır, 
mor olanlar ise kahverengiye döner.

44
00:06:02,125 --> 00:06:05,900
Meyveler bitki üzerinde 
tam olarak olgunlaşmamışsa,

45
00:06:06,220 --> 00:06:11,345
serin ve kuru bir yerde ahşap kutularda 
olgunlaşmaya bırakılabilir. 

46
00:06:16,285 --> 00:06:19,180
Tohum alma – Tasnif – Saklama

47
00:06:24,170 --> 00:06:25,685
Tohumları çıkarmak için

48
00:06:25,940 --> 00:06:29,145
tamamen olgunlaşmış ancak fermante olmamış sebzeleri tercih edin. 

49
00:06:34,050 --> 00:06:36,560
Tohum çıkarmak için iki yöntem vardır:

50
00:06:40,660 --> 00:06:42,080
Elinizdeki ürün miktarı düşükse

51
00:06:42,340 --> 00:06:44,250
patlıcanları dörde bölün

52
00:06:46,875 --> 00:06:48,720
ve çekirdeklerini bıçakla çıkarın.

53
00:06:56,080 --> 00:07:02,230
Daha büyük miktarda hasat sözkonusu ise 
patlıcanları soyarak küpler halinde doğrayın ve suyla dolu bir kavanoza koyun. 

54
00:07:08,905 --> 00:07:11,460
Bir kaç saniye blender yardımıyla karıştırın.

55
00:07:16,310 --> 00:07:19,530
İyi tohumlar 
kavanozun dibine çökecektir.

56
00:07:27,185 --> 00:07:30,420
Etli tabakayı, 
arta kalan kabuğu

57
00:07:30,785 --> 00:07:33,620
ve iyi gelişmemiş tohumları süzgeç yardımıyla çıkarın.

58
00:07:37,535 --> 00:07:41,695
İyi tohumları ayırın ve 
akan suyun altında süzgeç içinde temizleyin.

59
00:07:43,725 --> 00:07:47,340
Tohumları çıkardıktan sonra 
en fazla iki gün içinde kurutmak önemlidir.

60
00:07:50,215 --> 00:07:52,460
Sıcak, kuru ve havadar hatta

61
00:07:52,805 --> 00:07:58,140
havalandırması bulunan 
bir alanda 23° ile 30°C arasında

62
00:07:59,540 --> 00:08:02,380
ince gözenekli bir eleğe veya bir tabağa koyun. 

63
00:08:04,580 --> 00:08:07,740
Daha küçük miktarlar için, çok emici oldukları

64
00:08:08,110 --> 00:08:12,020
ve çekirdeklerin yapışmasını önledikleri için 
kahve filtreleri kullanılabilir.

65
00:08:15,525 --> 00:08:19,740
Her filtreye en fazla 
bir tatlı kaşığı tohum koyun.

66
00:08:20,210 --> 00:08:25,645
Poşetleri kuru, havadar 
ve gölgeli bir yerde askıya asın. 

67
00:08:35,455 --> 00:08:37,465
Çeşit, tür ve üretim yılını

68
00:08:37,695 --> 00:08:45,790
bir etikete yazın ve 
paketin içine yerleştirin.

69
00:08:47,095 --> 00:08:49,930
Dışına yazarsanız zamanla silinebilir. 

70
00:08:57,285 --> 00:09:01,100
Dondurucuda birkaç gün 
herhangi bir parazit larvasını öldürmeye yetecektir.

71
00:09:04,900 --> 00:09:09,710
Patlıcan tohumlarının 
çimlenme kapasitesi 3 ila 6 yıldır.

72
00:09:13,470 --> 00:09:16,275
Bu süreyi uzatmak amacıyla tohumları dondurucuda saklayabilirsiniz. 
