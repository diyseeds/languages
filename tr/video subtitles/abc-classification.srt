﻿1
00:00:08,000 --> 00:00:09,675
Botanik sınıflandırma

2
00:00:22,555 --> 00:00:27,580
Tohum üretmek isteyen herkesin 
bitkiler bilimi olan botanik hakkında

3
00:00:27,940 --> 00:00:29,420
genel bir bilgiye sahip olması gerekir.

4
00:00:37,860 --> 00:00:42,860
Bitkiler çiçeklerinin anatomisine, 
üreme organlarına,

5
00:00:43,220 --> 00:00:46,140
ve meyvelerine göre sınıflandırılmıştır.

6
00:00:47,460 --> 00:00:50,220
Her bitki Latince ismine göre tasnif edilir. 

7
00:00:57,325 --> 00:01:00,780
Latince sınıflandırma sayesinde
gündelik dilin

8
00:01:01,180 --> 00:01:04,420
yaratabileceği kafa karışıklığı 
önlenmiş olur.

9
00:01:09,285 --> 00:01:13,260
Örneğin "kabak" kelimesi 
yemeklik kabaktan

10
00:01:13,460 --> 00:01:19,740
balkabağına hatta su kabağına 
pek çok farklı türe verilen ortak bir addır. 

11
00:01:24,635 --> 00:01:28,700
Bu filmde sadece Latince botanik adlandırmalar kullanılacaktır:

12
00:01:30,100 --> 00:01:32,780
Fabaceae,

13
00:01:34,680 --> 00:01:35,940
Asteraceae,

14
00:01:38,200 --> 00:01:39,280
Solanaceae

15
00:01:42,195 --> 00:01:43,540
veya Cucurbitaceae gibi familyalar;

16
00:01:45,765 --> 00:01:48,820
Cucurbita gibi cinsler,

17
00:01:49,180 --> 00:01:54,540
Cucurbita Maxima gibi türler 
ve son olarak çeşitler. 

18
00:01:59,665 --> 00:02:04,580
Örneğin salatalık, Cucurbitaceae familyasının 
Cucumis cinsine ait olan

19
00:02:05,060 --> 00:02:09,500
Cucumis Sativus türünün 
bir parçasıdır.

20
00:02:10,820 --> 00:02:13,300
Birçok çeşidi bulunur. 

21
00:02:20,735 --> 00:02:23,900
Kavun da aynı Cucurbitaceae 
yani Kabakgiller familyasının

22
00:02:24,340 --> 00:02:26,295
aynı Cucumis cinsinin

23
00:02:26,850 --> 00:02:30,300
bir parçasıdır, ancak Cucumis melo adını taşıyan

24
00:02:30,740 --> 00:02:34,100
farklı bir türdür ve birden fazla çeşidi mevcuttur. 

25
00:02:39,240 --> 00:02:44,180
Aynı çeşidin bitkileri şekil, 
renk ve boyut

26
00:02:44,260 --> 00:02:47,620
gibi özellikler bakımından birbirine çok benzer.

27
00:02:49,730 --> 00:02:54,220
Aynı tür içindeki tüm çeşitler 
kendi aralarında çapraz döllenebilir. 

28
00:02:56,245 --> 00:03:00,820
Çoğu zaman, 
farklı türlerin çeşitleri kendi aralarında çapraz döllenemez.

29
00:03:01,840 --> 00:03:05,975
Örneğin hıyar ile 
kavun arasında melezleme riski yoktur. 

30
00:03:21,440 --> 00:03:23,255
Ancak bu durumun istisnaları vardır.

31
00:03:24,475 --> 00:03:28,570
Botanik açıdan yakın olan
türler arasında,

32
00:03:29,255 --> 00:03:32,700
örneğin her ikisi de kışlık kabak türleri olan

33
00:03:32,820 --> 00:03:36,180
Cucurbita moschata ile Cucurbita argyrosperma arasında çapraz döllenmeler mümkündür. 

34
00:03:39,220 --> 00:03:43,100
Farklı cinslere mensup bitkiler arasında melezlenme ise 
imkansızdır. 

35
00:03:52,435 --> 00:03:53,620
Bu bilgiyi edinmek,

36
00:03:53,695 --> 00:03:58,260
tohum üretimi için en uygun 
yetiştirme yöntemlerini kullanmanıza

37
00:03:58,540 --> 00:04:00,980
ve istenmeyen çapraz döllenmeleri önlemenize yardımcı olacaktır. 
