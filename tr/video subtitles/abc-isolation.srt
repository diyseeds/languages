﻿1
00:00:08,650 --> 00:00:10,380
İzolasyon Teknikleri

2
00:00:17,140 --> 00:00:19,620
Bahçede aynı türün farklı çeşitleri arasında

3
00:00:19,685 --> 00:00:22,620
çapraz tozlaşma 
riski olduğunda,

4
00:00:23,420 --> 00:00:25,700
çeşitlerin kendilerine özgü

5
00:00:25,850 --> 00:00:30,100
özelliklerini korumak için
onları izole etmek gerekir.

6
00:00:30,690 --> 00:00:33,750
Bir çok izolasyon tekniği 
bulunmaktadır.

7
00:00:39,485 --> 00:00:41,180
Mekansal İzolasyon 

8
00:00:45,240 --> 00:00:51,820
En basit yöntem, bir parselde 
her türden yalnızca bir çeşit yetiştirmektir.

9
00:00:52,820 --> 00:00:56,300
Başka bir çeşit, 
çapraz tozlaşma oluşmasını önlemek için

10
00:00:56,490 --> 00:01:00,955
yeterince uzaktaki 
bir parselde yetiştirilmelidir.

11
00:01:02,045 --> 00:01:05,230
Her sebze için bu uzaklık belirtilmiştir.

12
00:01:06,670 --> 00:01:11,100
Ayrıca çevresel faktörlere ve 
tozlaşma tipine de bağlıdır.

13
00:01:13,750 --> 00:01:18,580
Eğer böcekler yardımıyla döllenen bir bitki sözkonusu ise, 
birkaç yüz metrelik mesafe yeterli olacaktır. 

14
00:01:20,780 --> 00:01:24,020
Rüzgarla tozlaşma durumunda ise 
çeşitler arasında

15
00:01:24,500 --> 00:01:29,020
çok daha büyük bir 
mesafenin bırakılması gereklidir. 

16
00:01:29,325 --> 00:01:33,420
Sık bir çalı, 
rüzgar ve böcekler tarafından taşınan

17
00:01:33,780 --> 00:01:36,660
polen riskini büyük ölçüde azaltabilir. 

18
00:01:37,820 --> 00:01:43,020
Tohum için yetiştirilen bitkilerin 
yakın çevresinde çok sayıda çiçeğin bulunması,

19
00:01:43,140 --> 00:01:48,540
böcekleri çekecek ve onları 
diğer tohumluk bitkilerin çiçeklerinden uzaklaştıracaktır. 

20
00:01:59,600 --> 00:02:01,140
Zamansal İzolasyon 

21
00:02:01,880 --> 00:02:07,940
Aynı sebzenin iki çeşidinin yetiştirilme periyotları
 zaman içinde kademelendirilebilir.

22
00:02:08,770 --> 00:02:11,660
Bu sayede, bitkiler aynı anda çiçek açmayacağından dolayı

23
00:02:11,940 --> 00:02:14,220
çapraz tozlaşma imkansız hale gelir.

24
00:02:20,090 --> 00:02:25,300
Örneğin aynı parselde 
iki çeşit marul yetiştirilebilir;

25
00:02:25,880 --> 00:02:31,725
çok daha erken ekilen biri çiçek açmaya başlarken 
diğeri henüz fide halindedir.

26
00:02:32,940 --> 00:02:38,915
Elbette, bitkinin döngüsünü, özellikle de 
çiçeklenme döneminin uzunluğunu bilmek önemlidir. 

27
00:02:41,975 --> 00:02:43,820
Mekanik İzolasyon 

28
00:02:45,550 --> 00:02:50,960
Sinek tülü, tam anlamıyla güvenlikli tohum üretmek için 
paha biçilmez bir yardımcıdır.

29
00:02:52,560 --> 00:02:58,220
Böceklere ve amacınıza 
uygun bir tül seçilmelidir:

30
00:02:59,045 --> 00:03:04,620
böceklerin içeri girmesini engellemek mi istiyorsunuz
yoksa onları içeride tutmak mı?

31
00:03:07,320 --> 00:03:09,975
Bu tüller çeşitli şekillerde kullanılabilir. 

32
00:03:11,670 --> 00:03:14,035
Tek bir çiçeği örtme tekniği,

33
00:03:14,390 --> 00:03:18,340
hermafrodit ve kendi kendine 
döllenen çiçeklerde

34
00:03:18,795 --> 00:03:21,230
az miktarda tohum elde etmek için kullanılır.

35
00:03:22,295 --> 00:03:26,700
Henüz açılmamış 
çiçeğin etrafına

36
00:03:26,910 --> 00:03:30,340
içine hiçbir böceğin giremeyeceği şekilde küçük bir tül kese bağlanır.

37
00:03:34,585 --> 00:03:37,700
Çiçek solduğunda, 
sebzenin bozulmadan gelişebilmesi için

38
00:03:37,830 --> 00:03:40,590
kese çıkarılmalıdır.

39
00:03:42,075 --> 00:03:46,050
Tohumluk olarak ayrılan bitkiyi 
işaretlemeyi unutmayın. 

40
00:03:55,890 --> 00:04:02,585
Bu yöntem, bir dalı veya hatta tüm bitkiyi 
kapatma biçiminde de kullanılabilir.

41
00:04:04,800 --> 00:04:09,260
Ancak bunun öncesinde, 
zaten açık olan çiçekler kaldırılmalıdır

42
00:04:09,730 --> 00:04:14,060
çünkü onların böcekler tarafından 
tozlaştırılmış olma riski vardır. 

43
00:04:19,900 --> 00:04:22,780
File örtüler veya tüneller

44
00:04:22,860 --> 00:04:26,380
daha büyük miktarda 
tohum üretmek için çok etkilidir. 

45
00:05:29,540 --> 00:05:32,180
Hermafrodit ve kendi kendine döllenen bitkiler

46
00:05:32,320 --> 00:05:35,940
böceklerden 
korunmak için

47
00:05:36,060 --> 00:05:37,620
aynı tünel içerisinde yetiştirilebilir. 

48
00:05:40,540 --> 00:05:45,700
Tozlaşmak için böceklere 
gereksinim duyan bitkileri tozlaştırmak için

49
00:05:46,340 --> 00:05:51,820
tünele bombus arı 
kovanları yerleştirilir.

50
00:05:54,610 --> 00:05:58,620
Bu durumda her türden 
sadece bir çeşit yetiştirilebilir.

51
00:05:59,085 --> 00:06:02,020
Kovanın maliyetini karşılamak için,

52
00:06:02,230 --> 00:06:06,420
birçok tohumluk bitkinin bulunduğu 
geniş bir seraya sahip olmak tercih edilir. 

53
00:06:08,050 --> 00:06:10,540
Aynı bahçede aynı türden

54
00:06:10,780 --> 00:06:14,740
iki çeşit bitki 
yetiştirmek istiyorsanız,

55
00:06:14,900 --> 00:06:18,980
örneğin iki farklı 
kabak çeşidi gibi,

56
00:06:19,940 --> 00:06:23,220
sırayla örtü altına alma 
yöntemi de uygulanabilir.

57
00:06:24,120 --> 00:06:25,900
Bir kovan kullanılmaksızın,

58
00:06:26,350 --> 00:06:30,980
türün bir çeşidi bir tünelde, 
aynı türden başka

59
00:06:31,580 --> 00:06:36,520
bir çeşit de 
başka bir tünelde yetiştirilir.

60
00:06:38,570 --> 00:06:40,060
Çiçeklerin açtığı süre boyunca

61
00:06:40,720 --> 00:06:43,780
tünel örtüleri dönüşümlü olarak açılabilir.

62
00:06:44,880 --> 00:06:51,610
İlk gün 1 numaralı tünel açılır, 
2 numaralı tünel kapatılır.

63
00:06:52,810 --> 00:06:55,335
Ertesi gün ise tersi yapılır.

64
00:06:56,990 --> 00:06:59,800
Tüneller sabah 
erkenden açılmalıdır

65
00:07:00,190 --> 00:07:02,860
çünkü böcekler yaz aylarında
erken saatte faaliyete geçer

66
00:07:03,225 --> 00:07:07,380
akşam ise artık herhangi bir ses 
duyulmaz oluncaya kadar kapanmaz.

67
00:07:08,575 --> 00:07:09,740
Bu yöntemle

68
00:07:10,030 --> 00:07:12,835
tozlaşan dişi 
çiçek sayısı ve dolayısıyla

69
00:07:13,030 --> 00:07:15,805
meyve sayısında azalma görülür.

70
00:07:17,140 --> 00:07:19,900
Ancak doğa o kadar bereketlidir ki,

71
00:07:20,215 --> 00:07:23,980
sonraki yıllarda bahçeden 
bol miktarda tohum alınacaktır. 
