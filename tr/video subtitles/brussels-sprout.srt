﻿1
00:00:12,995 --> 00:00:16,680
Brüksel lahanası 
Turpgiller (Brassicaceae) familyasının,

2
00:00:17,340 --> 00:00:22,210
lahana (Brassica oleracea) türünün, 
gemmifera alt türünün bir üyesidir.

3
00:00:26,030 --> 00:00:30,215
Brassica oleracea
yani Lahana türü ayrıca alabaş,

4
00:00:30,675 --> 00:00:37,130
brokoli, baş lahana, kale, karnabahar 
ve Milano lahanasını da içerir. 

5
00:00:42,530 --> 00:00:45,832
Brüksel lahanası 
soğuk ve ılıman iklime sahip bölgelerde yetişen

6
00:00:45,950 --> 00:00:48,770
bir sonbahar ve kış sebzesidir.

7
00:00:51,030 --> 00:00:56,825
Yapraklarının tabanında küçük filizler oluşturan ve 
çok düşük sıcaklıklara dayanabilen bir bitkidir. 

8
00:01:05,760 --> 00:01:10,315
Oleracea türü lahanaların 
tamamında tozlaşma: 

9
00:01:26,400 --> 00:01:30,285
Lahana türünün 
çiçekleri hermafrodittir

10
00:01:33,250 --> 00:01:37,460
yani hem dişi 
hem de erkek organ bulundururlar.

11
00:01:41,155 --> 00:01:43,650
Çoğu kendine verimsizdir:

12
00:01:44,265 --> 00:01:49,250
bitkinin çiçeklerinden gelen polen 
sadece başka bir bitkiyi dölleyebilir.

13
00:01:53,140 --> 00:01:55,315
Bir başka ifadeyle bu bitkiler allogamdır.

14
00:01:56,285 --> 00:02:01,020
İyi bir tozlaşma sağlamak için 
birkaç bitki yetiştirmek iyi olacaktır. 

15
00:02:04,665 --> 00:02:07,615
Böcekler tozlaşmanın aracılarıdır.

16
00:02:11,095 --> 00:02:15,450
Bunlar, doğal bir 
genetik çeşitlilik sağlar.

17
00:02:22,510 --> 00:02:28,405
Lahana türünün tüm alt türleri 
birbirleriyle çaprazlanabilir.

18
00:02:30,330 --> 00:02:34,920
Bu nedenle tohum elde etmek için 
farklı tür lahanaları birbirine yakın büyütmemelisiniz. 

19
00:02:39,565 --> 00:02:40,990
Saflık sağlamak adına,

20
00:02:41,330 --> 00:02:48,695
farklı lahana çeşitleri 
en az 1 kilometre aralıkla ekilmelidir. 

21
00:02:50,160 --> 00:02:53,025
İki çeşit arasında çit gibi

22
00:02:53,215 --> 00:02:57,485
doğal bir bariyer varsa 
bu mesafe 500 metreye düşürülebilir. 

23
00:03:00,745 --> 00:03:05,960
Çeşitler, kapalı bir sinekliğin içine 
böceklerin bulunduğu küçük kovanlar

24
00:03:06,165 --> 00:03:08,340
yerleştirilerek veya sinekliklerin

25
00:03:13,530 --> 00:03:17,285
dönüşümlü olarak 
açılıp kapatılmasıyla izole edilebilir.

26
00:03:21,680 --> 00:03:22,830
Bu teknik için,

27
00:03:22,985 --> 00:03:27,270
"Tohum üretiminin ABC'si" bölümündeki 
izolasyon teknikleri kısmına bakınız. 

28
00:03:38,710 --> 00:03:40,940
Brüksel Lahanasının hayat döngüsü

29
00:03:56,615 --> 00:03:59,415
Brüksel Lahanası iki yıllık bir bitkidir.

30
00:04:00,210 --> 00:04:04,260
Yenilebilir filizlerini 
sonbahar ve kış aylarında üretir.

31
00:04:04,820 --> 00:04:08,100
Çiçek saplarını ise 
takip eden baharda oluşturur.

32
00:04:14,360 --> 00:04:18,730
Tohumluk olan bitkiler de 
tüketim için üretilenler ile aynı şekilde yetiştirilir.

33
00:04:19,840 --> 00:04:22,095
Mayıs ya da Haziran ayında ekilirler. 

34
00:04:45,350 --> 00:04:51,425
İyi bir genetik çeşitlilik yaratmak için 
tohumluk bırakmak üzere 15 bitki belirlenmelidir. 

35
00:04:55,495 --> 00:04:58,065
Tohumlar, tüm büyüme dönemi boyunca

36
00:04:58,190 --> 00:05:01,200
gözlemlenen 
sağlıklı bitkilerden alınmalıdır.

37
00:05:04,320 --> 00:05:08,595
Bu, tüm gövde boyunca düzenli filiz oluşumu, 
filizlerin sıklığı,

38
00:05:09,075 --> 00:05:13,440
rengi ve şekli, 
tadında acılık olmaması,

39
00:05:15,035 --> 00:05:18,840
soğuğa dayanıklılığı, verimi

40
00:05:19,770 --> 00:05:22,390
ve bitkinin boyu gibi

41
00:05:23,010 --> 00:05:27,010
çeşidin tüm özelliklerini 
kontrol edebilmeye imkan sağlar. 

42
00:05:28,495 --> 00:05:34,070
Bitkiler ilk yılda 60-80 cm arası 
bir uzunluğa ulaşabilir. 

43
00:05:36,135 --> 00:05:40,235
Sap boyunca uzanan filizler 
sonbaharda hasat edilebilir

44
00:05:40,690 --> 00:05:43,915
ama tepedeki filizler asla toplanmamalıdır. 

45
00:05:46,690 --> 00:05:51,355
Brüksel lahanası, 
büyük lahanalara göre soğuğa daha dayanıklıdır

46
00:05:53,410 --> 00:05:57,415
ve kışlık çeşitler 
kış boyunca toprakta kalabilir.

47
00:05:59,980 --> 00:06:04,080
Gerek görülürse bir don örtüsü altında 
muhafaza edilebilirler. 

48
00:06:13,985 --> 00:06:19,020
İkinci yılda sapları 
bir buçuk metre uzunluğa ulaşabilir.

49
00:06:19,800 --> 00:06:21,135
Eğilmelerini engellemek için

50
00:06:21,585 --> 00:06:25,670
çiçek saplarını kazıklar yardımıyla 
desteklemek gerekebilir. 

51
00:06:27,690 --> 00:06:31,785
Çiçeklenme sürecini hızlandırmak için 
sapın baş kısmı kesilebilir. 

52
00:07:17,840 --> 00:07:24,640
Tüm lahanalarda hasat, tohum alma, 
tasnif ve saklama 

53
00:07:33,675 --> 00:07:37,525
Tohum kabukları bej rengini aldığında 
tohumlar olgunlaşmış demektir.

54
00:07:41,670 --> 00:07:43,980
Tohum kabukları iyice kuruyunca,

55
00:07:44,385 --> 00:07:49,310
yani olgunlaştıklarında 
çok kolay açılırlar ve tohumlarını saçarlar. 

56
00:07:57,965 --> 00:08:02,000
Çoğu zaman sapların tümü 
aynı anda olgunlaşmaz.

57
00:08:03,025 --> 00:08:09,000
Tohumları boşa harcamamak adına hasat 
her sap olgunlaştıkça bir bir yapılabilir.

58
00:08:10,835 --> 00:08:16,460
Bitki, tüm tohumlar
tamamen olgunlaşmadan da hasat edilebilir. 

59
00:08:19,940 --> 00:08:25,045
Olgunlaşma süreci, bitkinin kuru 
ve iyi havalandırılan

60
00:08:25,150 --> 00:08:26,970
bir yerde kurutulması ile sonlandırılır. 

61
00:08:39,465 --> 00:08:45,445
Lahana tohumları, tohum kabukları 
elle kolayca açılabilir hale geldiğinde toplanmaya hazırdır. 

62
00:08:47,570 --> 00:08:48,890
Tohumları çıkarmak için,

63
00:08:49,040 --> 00:08:54,375
tohum kabukları plastik bir tabaka 
veya kalın bir kumaş parçası üzerine yayılır

64
00:08:54,785 --> 00:08:57,710
ardından elle dövülür veya ovulur.

65
00:09:01,125 --> 00:09:05,840
Bunları bir torbaya koyup
yumuşak bir yüzey üzerindeyken dövebilirsiniz. 

66
00:09:07,600 --> 00:09:12,275
Daha büyük miktarda tohum kabukları, üzerlerinde yürünerek 
veya üzerlerinden araba ile geçilerek parçalanabilir. 

67
00:09:24,800 --> 00:09:30,090
Kolayca açılamayan tohum kabukları 
muhtemelen güzel filizlenemeyecek

68
00:09:30,310 --> 00:09:32,060
olgunlaşmamış tohumlar içerir. 

69
00:09:36,765 --> 00:09:37,995
Tohum alınırken,

70
00:09:38,310 --> 00:09:41,395
tohumlar öncelikle kabukları ve sapları tutan

71
00:09:41,580 --> 00:09:44,605
büyük bir elekten geçirilir

72
00:09:49,685 --> 00:09:52,265
ardından tohumları tutan ancak

73
00:09:52,715 --> 00:09:57,380
daha küçük çer çöpün düşmesine izin veren 
başka bir elekten geçirilir. 

74
00:10:01,020 --> 00:10:04,930
Son olarak, üfleyerek 
ya da rüzgar yardımıyla

75
00:10:05,210 --> 00:10:09,245
kalan ufak parçalar 
ayıklanır. 

76
00:10:24,425 --> 00:10:29,380
Brassica oleracea yani Lahana türünün 
bütün tohumları birbirine benzer.

77
00:10:30,390 --> 00:10:37,015
Bunları birbirlerinden ayırt etmek çok zordur, 
örneğin lahana ve karnabahar tohumunu ayırmak pek kolay değildir.

78
00:10:37,605 --> 00:10:40,500
Bu sebepten dolayı, bitkilerin ve çıkarılan tohumların,

79
00:10:40,775 --> 00:10:44,550
türün adını, çeşidini 
ve ekim yılını

80
00:10:44,835 --> 00:10:47,855
yazmak suretiyle etiketlendirilmesi önemlidir. 

81
00:10:49,275 --> 00:10:54,035
Tohumları birkaç gün dondurucuda saklamak 
parazitleri ortadan kaldırır. 

82
00:10:59,480 --> 00:11:02,505
Lahana tohumları 5 yıla kadar çimlenebilir.

83
00:11:03,695 --> 00:11:07,395
Bu kapasitelerini 10 yıla kadar 
koruyabilmeleri mümkündür.

84
00:11:09,145 --> 00:11:12,405
Bu süre dondurucuda 
saklama yoluyla uzatılabilir.

85
00:11:13,450 --> 00:11:19,625
Çeşide göre değişmekle beraber bir gramda, 
250 ila 300 tohum bulunur. 
