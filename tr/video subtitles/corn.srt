1
00:00:11,520 --> 00:00:16,996
Mısır, tahıl ürünlerinin çoğu gibi,
Poaceae ailesinin bir üyesidir.

2
00:00:17,160 --> 00:00:19,774
Tür adı ise Zea mays’tır.

3
00:00:21,716 --> 00:00:23,643
Birkaç çeşit mısır vardır:

4
00:00:24,650 --> 00:00:27,912
- Zea mays zaccharata,

5
00:00:28,952 --> 00:00:34,630
koçanı tam olgunlaşmadan önce çiğ olarak veya 
suda haşlama ya da ızgara yapılarak tüketilen şeker mısırıdır.

6
00:00:36,960 --> 00:00:39,520
Tohumları kurudukça küçülür.

7
00:00:46,000 --> 00:00:48,618
Bu tür serin bölgelerde yetiştirilebilir. 

8
00:00:53,280 --> 00:00:57,156
- Zea mays indentata,

9
00:00:57,640 --> 00:01:04,094
tohumu daha sert bir camsı tabaka ile çevrili 
nişastalı bir çekirdekten oluşan atdişi mısır.

10
00:01:05,690 --> 00:01:09,323
Nişastalı kısım olgunluklaştıkça küçülür

11
00:01:09,680 --> 00:01:13,505
ve ona adını veren 
keskin bir şekil oluşturur.

12
00:01:15,050 --> 00:01:20,647
Un yapımında kullanılan, 
hayvan yemi olarak da kullanılan bir çeşittir. 

13
00:01:31,992 --> 00:01:35,476
- Zea mays indurata, yani sert mısır,

14
00:01:37,934 --> 00:01:43,178
atdişi mısırdan farklı olarak 
çok küçük nişastalı bir çekirdeğe

15
00:01:43,629 --> 00:01:50,000
ve daha büyük bir camsı tabakaya sahiptir,
bu nedenle de esas olarak polenta yapımında kullanılır. 

16
00:01:56,494 --> 00:02:05,614
- Zea mays microsperma veya everta, cin mısır olarak bilinen
mısır çeşidi ısıtıldığında patlar ve patlamış mısır olarak tüketilir. 

17
00:02:22,100 --> 00:02:23,100
Tozlaşma 

18
00:02:29,185 --> 00:02:30,836
Mısır tek evciklidir,

19
00:02:31,200 --> 00:02:35,192
yani aynı bitki üzerinde 
erkek ve dişi çiçekler bulunur.

20
00:02:37,832 --> 00:02:42,603
Erkek çiçek veya püskül, 
gövdenin en tepesinde bulunur

21
00:02:44,450 --> 00:02:48,116
dişi çiçek ise 
gövdenin orta kısmında yer alır,

22
00:02:49,090 --> 00:02:56,014
gelecekteki koçanı içeren şişkin kısımdır bu, 
dışarıdan sadece mısır ipeği görülür. 

23
00:03:00,392 --> 00:03:03,301
Her ipek,

24
00:03:03,694 --> 00:03:08,152
tozlaşmadan sonra 
koçanda bir tohum üretecek olan bir ovüle bağlıdır. 

25
00:03:12,080 --> 00:03:17,083
Mısır allogamdır, 
yani bir bitki diğerini döller.

26
00:03:18,552 --> 00:03:23,585
Aynı zamanda da anemofildir, 
yani mısır polenlerini 10 km'den uzak mesafelere

27
00:03:23,949 --> 00:03:27,963
taşıyan rüzgar tarafından tozlaştırılır. 

28
00:03:32,618 --> 00:03:36,283
Rüzgarın poleni düzgün bir şekilde
taşıyabilmesi için

29
00:03:36,370 --> 00:03:39,890
mısırların en az 3 sıra olacak şekilde 
gruplar halinde ekilmesi gerekir.

30
00:03:41,367 --> 00:03:43,130
Tek sıra olursa,

31
00:03:43,345 --> 00:03:48,610
tozlaşma zayıf olacak ve 
koçanlar tam olarak dolgunlaşmayacaktır. 

32
00:03:51,149 --> 00:03:55,352
Bazı bölgelerde mısır, 
üretilen yüksek miktarda polenin

33
00:03:55,440 --> 00:03:58,843
cazibesine kapılan 
arılar tarafından da ziyaret edilebilir.

34
00:04:00,349 --> 00:04:05,287
Bir bitki 18 milyona kadar 
polen üretebilir! 

35
00:04:10,749 --> 00:04:17,214
Çapraz tozlaşmayı önlemek için, 
iki çeşit mısır ekilecekse arada en az 3 km mesafe bırakılmalıdır. 

36
00:04:19,927 --> 00:04:26,378
Çalı çit gibi doğal bir bariyer varsa 
bu mesafe 1 km'ye düşürülebilir. 

37
00:04:29,127 --> 00:04:32,058
Ayrıca, zamana yayarak izole etme yöntemini de kullanabilirsiniz.

38
00:04:33,236 --> 00:04:39,643
Aynı bahçeye iki çeşit mısır, 
birkaç hafta arayla ekilir.

39
00:04:40,923 --> 00:04:45,287
Amaç, ilk çeşidin erkek çiçeklerinin 
polenlerini salma zamanının

40
00:04:45,730 --> 00:04:50,385
diğer çeşidin dişi çiçeklerinin 
ortaya çıkışıyla kesişmesini önlemektir.

41
00:04:51,701 --> 00:04:55,461
Aksi takdirde, çeşitler 
kaçınılmaz olarak çapraz tozlaşacaktır. 

42
00:04:56,858 --> 00:05:01,745
Bu teknik için, çeşide bağlı olarak 
55 ila 120 gün arasında değişen

43
00:05:02,276 --> 00:05:07,740
büyüme döngüsünün 
uzunluğunu hesaba katın. 

44
00:05:09,781 --> 00:05:12,574
Üretim alanınızın yakınlarında büyük çaplı

45
00:05:12,850 --> 00:05:17,607
endüstriyel hibrit mısır tarımı yapılmaktaysa, 
yetiştirmekte olduğunuz mısır çeşidinin saflığını korumak neredeyse imkansızdır. 

46
00:05:19,389 --> 00:05:24,778
Bu durumda, elle tozlaşma 
tohum üretimi için tek çözümdür.

47
00:05:26,378 --> 00:05:31,498
Bu yöntem için sıralar birbirinden biraz daha aralıklı ekilmelidir, 
böylece aralarında yürümek için yer kalır. 

48
00:05:34,720 --> 00:05:38,792
Yağmura dayanıklı 
sağlam kağıt poşetlere ihtiyacınız olacaktır. 

49
00:05:42,414 --> 00:05:46,450
Mısır bitkileri 10 ila 14 gün boyunca çiçek açar.

50
00:05:48,814 --> 00:05:52,472
Elle tozlaşma süreci üç gün sürer. 

51
00:05:55,541 --> 00:05:59,825
İlk gün dişi çiçekler 
poşetlere konulacaktır.

52
00:06:01,640 --> 00:06:06,167
Bunu küçük koçanlardan 
ipek çıkmadan hemen önce yapmalısınız;

53
00:06:07,338 --> 00:06:10,450
eğer siz çiçekleri torbalamadan 
önce belirdiyse,

54
00:06:10,610 --> 00:06:11,723
geç kaldınız demektir! 

55
00:06:14,596 --> 00:06:18,850
Öncelikle minik koçanın 
etrafındaki yaprakların tepe kısmı kesilir,

56
00:06:18,980 --> 00:06:22,356
böylece ipekler daha belirgin bir şekilde ortaya çıkacaktır. 

57
00:06:31,498 --> 00:06:36,480
Ardından koçan poşete konur 
ve poşet de tabanından tutturulur. 

58
00:06:42,232 --> 00:06:45,450
Eril çiçekler, anterlerin 
yani erkek organların

59
00:06:45,636 --> 00:06:48,203
mısırın başağının dikey ve yanal saplarından

60
00:06:48,400 --> 00:06:52,363
çıkmaya başladığı 
üçüncü günün sabahı torbalanır. 

61
00:06:58,530 --> 00:07:02,625
Anterler hala yeşilse, 
torbalanma sonrasında gelişmeleri durabilir. 

62
00:07:05,301 --> 00:07:09,830
Torbalamadan önce, 
arılar veya rüzgar tarafından taşınmış olabilecek

63
00:07:09,963 --> 00:07:13,229
diğer çeşitlerin polenlerini bitkilerin üzerinden silkeleyin. 

64
00:07:29,120 --> 00:07:34,349
Torba, sabah dökülen polenleri 
toplayacak şekilde takılmalıdır. 

65
00:07:49,476 --> 00:07:53,956
Çoğu polen, çiy kuruduktan sonra
öğle saatlerinden önce salınır. 

66
00:07:57,280 --> 00:08:00,181
Çiçeklere birkaç kez dokunarak 
polenlerin dökülmesine yardımcı olunur. 

67
00:08:09,410 --> 00:08:14,196
Elle tozlaşma aynı gün 
öğle saatlerinde yapılmalıdır zira öğleden sonra,

68
00:08:14,298 --> 00:08:19,141
torbadaki polen çok fazla ısınabilir 
ve canlılığını yitirebilir. 

69
00:08:20,523 --> 00:08:23,745
Sabahın sonunda, 
günün en sıcak diliminden önce,

70
00:08:24,247 --> 00:08:27,541
polen toplamak için 
farklı torbalar açılır,

71
00:08:37,970 --> 00:08:39,620
polenler karıştırılır

72
00:08:41,512 --> 00:08:44,596
ve ardından bir koçanın üzerini örten torba açılır.

73
00:08:44,741 --> 00:08:50,145
İpek iki gün içinde 
yaklaşık 3 ila 4 cm büyümüş olmalıdır. 

74
00:08:50,480 --> 00:08:56,581
Polen, dışarda kalan ipeğin 
her tarafına bir fırça yardımıyla uygulanır.

75
00:08:57,534 --> 00:09:00,930
Koçan başına yaklaşık bir çay kaşığı polen uygulanması gerekir. 

76
00:09:14,240 --> 00:09:19,360
Torba derhal koçanın etrafına kapatılır 
ancak gelişmesi için yeterli alan bırakılır. 

77
00:09:20,901 --> 00:09:23,912
İpek, birkaç hafta boyunca 
polenlere açıktır.

78
00:09:24,580 --> 00:09:28,305
Bu nedenle koçanların hasada kadar torbalarda bırakılması yerinde olur. 

79
00:09:40,210 --> 00:09:41,460
Yaşam döngüsü

80
00:10:00,900 --> 00:10:05,490
Mısır, bir yıl içinde koçan üreten 
tek yıllık bir bitkidir. 

81
00:10:07,389 --> 00:10:11,207
Tüketim için olan mısır ile 
tohumluk mısır aynı şekilde yetiştirilir.

82
00:10:14,458 --> 00:10:20,101
Genel olarak, yaşadığınız çevreye uygun bir 
mısır çeşidi seçtiğinizden emin olmalısınız.

83
00:10:26,574 --> 00:10:32,167
İyi bir genetik çeşitliliği korumak için 
en az 50 bitki gerekir;

84
00:10:33,090 --> 00:10:35,563
200 bitki yetiştirmek en iyisidir. 

85
00:10:48,720 --> 00:10:53,556
Tohum almak için, çeşit özelliklerine uygun gelişen 
ve seçim kriterlerini karşılayan bitkileri tercih edin:

86
00:10:53,890 --> 00:10:57,580
bu kriterler boyut, renk,

87
00:10:58,283 --> 00:11:01,020
sağlamlık, erkenci ya da geçci olması,

88
00:11:01,860 --> 00:11:05,540
koçanın boyutu 
ve yapraklarla ne kadar iyi sarmalandığıdır. 

89
00:11:18,370 --> 00:11:20,618
Koçanlar bitki üzerinde kuruyabilir. 

90
00:11:23,658 --> 00:11:28,145
Tırnakla bastırdığınızda 
artık çekirdekte iz kalmıyorsa mısır olgunlaşmış demektir. 

91
00:11:32,581 --> 00:11:35,316
Daha sonra koçanlar saptan toplanır. 

92
00:11:41,900 --> 00:11:45,796
Yapraklar koçanlardan sıyrılır ve toplanan mısırlar

93
00:11:46,203 --> 00:11:49,730
kuru ve iyi havalandırılan 
bir yerde saklanır. 

94
00:12:10,036 --> 00:12:15,025
Ayrıca bitkinin tamamı kesilerek 
kapalı bir yerde kurumaya bırakılabilir. 

95
00:12:26,480 --> 00:12:29,956
Tohum alma – Ayırma – Saklama

96
00:12:34,436 --> 00:12:38,880
Tohumluk koçanlar, 
tanelerinin şekline, rengine,

97
00:12:39,018 --> 00:12:43,774
tane sıralarının sayısına, 
dizilimlerine ve dokularına göre seçilir. 

98
00:12:51,230 --> 00:12:53,709
Genetik çeşitliliği korumak için

99
00:12:53,963 --> 00:12:59,003
tohumluk için kullanılacak taneler 
çok sayıda koçandan karışık olarak seçilmelidir.

100
00:13:20,930 --> 00:13:24,596
Ayrıca çekirdeklerin koçanın 
orta kısmından alınması tavsiye edilir. 

101
00:13:31,294 --> 00:13:36,269
Koçanları ovalayarak tohumları çıkarmak için 
eldiven giymeyi ihmal etmeyiniz. 

102
00:13:48,887 --> 00:13:51,781
Poşetin dışına yazıldığında silinme riski bulunduğundan dolayı,

103
00:13:51,898 --> 00:13:58,570
her zaman poşetin içine tür ve çeşit adları ile 
tohumun alındığı yılın yazıldığı bir etiket koyun. 

104
00:14:02,460 --> 00:14:07,170
Tohumları birkaç gün dondurucuda saklamak 
parazit larvalarını öldürecektir. 

105
00:14:09,447 --> 00:14:16,589
Patlak mısır, atdişi mısır ve sert mısır tohumları 
beş yıla kadar filizlenebilir.

106
00:14:17,643 --> 00:14:21,883
Bazı durumlarda, 
bu süre on yıla kadar uzatılabilir. 

107
00:14:22,560 --> 00:14:27,207
Şeker mısır tohumları ise 
üç yıla kadar çimlenebilir. 

108
00:14:28,230 --> 00:14:31,687
Tohumların düşük ısıda saklanmasıyla 
bu süre uzatılabilir. 
