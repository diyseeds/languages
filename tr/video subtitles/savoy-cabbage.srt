﻿1
00:00:09,480 --> 00:00:12,938
Milano lahanası yahut Kıvırcık baş lahana, 
Turpgiller (Brassicaceae) familyasının,

2
00:00:13,510 --> 00:00:18,930
Brassica oleraceae türünün ve 
Sabauda alt türünün bir üyesidir.

3
00:00:20,560 --> 00:00:25,025
Brassica oleracea türü 
ayrıca alabaş,

4
00:00:25,163 --> 00:00:31,396
brokoli, lahana, Brüksel lahanası, 
karalahana ve karnabaharı da içerir. 

5
00:00:37,810 --> 00:00:41,949
Milano lahanaları, 
lahana kadar sıkışık olmayan

6
00:00:44,581 --> 00:00:48,196
bir başa sahip, 
kıvırcık yapraklarıyla tanınır.

7
00:00:50,220 --> 00:00:53,934
İlkbahar ve yaz aylarında yetiştirilen, 
yaprakları ve

8
00:00:54,138 --> 00:00:56,792
başı daha az sıkı olan Milano lahanaları,

9
00:00:58,698 --> 00:01:01,912
depolama için yetiştirilen 
büyük başlı Milano lahanaları

10
00:01:05,745 --> 00:01:10,240
ve başı yeşil ve açık olan 
kışlık Milano lahanaları vardır. 

11
00:01:15,592 --> 00:01:20,340
Oleracea türü 
tüm lahanaların tozlaşması: 

12
00:01:36,120 --> 00:01:40,240
Brassica oleracea türlerinin 
çiçekleri hermafrodittir,

13
00:01:43,090 --> 00:01:47,265
yani hem erkek 
hem de dişi organlara sahiptirler.

14
00:01:50,996 --> 00:01:53,490
Çoğu kendi kendini dölleyemez:

15
00:01:54,101 --> 00:01:59,156
bir bitkinin çiçeklerinden gelen polen 
sadece başka bir bitkiyi dölleyebilir.

16
00:02:02,967 --> 00:02:05,178
Bu nedenle, bitkiler allogam olarak adlandırılır.

17
00:02:06,123 --> 00:02:10,945
İyi bir tozlaşma sağlamak için 
birkaç bitki yetiştirmek daha iyi olacaktır. 

18
00:02:14,501 --> 00:02:17,563
Böcekler tozlaşmanın aracılarıdır.

19
00:02:20,952 --> 00:02:25,403
Bu özellikleri de 
büyük bir doğal genetik çeşitlilik sağlar.

20
00:02:32,356 --> 00:02:36,661
Brassica oleracea türünün 
tüm lahana alt türleri

21
00:02:36,814 --> 00:02:38,690
birbirleriyle çapraz tozlaşabilir.

22
00:02:40,276 --> 00:02:44,923
Bu nedenle farklı lahana türlerini 
tohumluk olarak birbirlerine yakın yetiştirmemelisiniz. 

23
00:02:49,447 --> 00:02:55,192
Saflığı sağlamak için, 
Brassica oleracea türünün

24
00:02:55,476 --> 00:02:58,690
farklı çeşitleri en az 1 km arayla ekilmelidir. 

25
00:03:00,000 --> 00:03:04,378
İki çeşit arasında 
çit gibi doğal bir bariyer varsa

26
00:03:04,472 --> 00:03:07,287
bu mesafe 500 metreye kadar düşürülebilir. 

27
00:03:10,603 --> 00:03:14,680
Çeşitler, sinek telleri kullanıp 
bunların dönüşümlü olarak açılıp kapatılmasıyla

28
00:03:14,763 --> 00:03:18,334
veya kapalı bir sinek telinin içine

29
00:03:23,389 --> 00:03:27,090
böcekli küçük kovanlar 
yerleştirerek de izole edilebilir.

30
00:03:31,440 --> 00:03:32,618
Bu teknik için,

31
00:03:32,843 --> 00:03:36,860
"Tohum üretiminin ABC'si" bölümündeki 
izolasyon teknikleri modülüne bakın. 

32
00:03:50,180 --> 00:03:53,862
Milano lahanasının yaşam döngüsü

33
00:03:54,974 --> 00:03:57,680
Milano lahanası iki yıllık bir bitkidir.

34
00:03:58,705 --> 00:04:03,643
Döngünün ilk yılında, tohumluk olarak da tıpkı 
tüketim amacıyla olduğu gibi yetiştirilir.

35
00:04:08,327 --> 00:04:10,596
Tohumlarını ikinci yıl üretecektir. 

36
00:04:50,720 --> 00:04:56,436
İyi bir genetik çeşitlilik sağlamak için 
10 ila 15 bitki seçmelisiniz.

37
00:04:57,220 --> 00:05:00,843
Milano lahanası tohumları, 
tüm büyüme dönemi boyunca

38
00:05:01,112 --> 00:05:04,465
gözlemlenen 
sağlıklı bitkilerden seçilir.

39
00:05:05,440 --> 00:05:09,980
Bu, çeşidin tüm özelliklerinin 
bilinmesini sağlar. 

40
00:05:10,356 --> 00:05:15,774
İstenen seçim kriterlerini karşılayan 
kuvvetli başları seçmelisiniz:

41
00:05:17,309 --> 00:05:22,865
düzenli ve hızlı büyüme, 
hızlı baş oluşumu,

42
00:05:25,200 --> 00:05:28,660
iyi depolama kapasitesi, erken büyüme,

43
00:05:30,887 --> 00:05:33,310
soğuğa ve hastalıklara karşı direnç vb.

44
00:05:36,138 --> 00:05:41,520
Seçimi etkileyen diğer özellikler
çeşidin tipik şekline sahiplik,

45
00:05:42,080 --> 00:05:45,100
sivri, düz veya yuvarlak bir baş,

46
00:05:46,974 --> 00:05:52,472
kısa bir sap, iyi bir kök sistemi, tat ve renktir. 

47
00:05:55,934 --> 00:06:01,047
Milano lahanası, 
Brassica oleracea'nın diğer türlerine göre

48
00:06:01,483 --> 00:06:06,501
soğuğa çok daha dirençlidir ve 
-15°'ye kadar düşen sıcaklıklara dayanabilir.

49
00:06:06,890 --> 00:06:10,254
Çoğu çeşit kışı açık havada geçirebilir. 

50
00:06:23,310 --> 00:06:26,618
Diğer kışlatma yöntemleri 
ve yaşam döngüsünün ikinci yılı

51
00:06:26,712 --> 00:06:30,340
lahana ile aynıdır.

52
00:07:07,636 --> 00:07:14,340
Tüm Brassica oleracea türlerinin hasat edilmesi, 
ayıklanması, ayrıştırılması ve depolanması.

53
00:07:23,505 --> 00:07:27,250
Tohumların olgunlaştığı, tohum kabuklarının
bej rengine dönmesiyle anlaşılır.

54
00:07:31,483 --> 00:07:34,007
Tohum kabukları çok gevşektir,

55
00:07:34,196 --> 00:07:39,294
yani olgunlaştıklarında çok kolay açılırlar 
ve tohumlarını dağıtırlar. 

56
00:07:47,781 --> 00:07:51,963
Çoğu zaman sapların hepsi 
aynı anda olgunlaşmaz.

57
00:07:52,836 --> 00:07:59,069
Tohum israfını önlemek için, 
hasat her sap olgunlaştıkça yapılabilir.

58
00:08:00,676 --> 00:08:06,298
Tüm bitki, tohumların hepsi 
tamamen olgunlaşmadan da hasat edilebilir. 

59
00:08:09,636 --> 00:08:14,800
Olgunlaşma süreci daha sonra nemsiz, 
iyi havalandırılan

60
00:08:14,950 --> 00:08:16,625
bir yerde kurutularak tamamlanır. 

61
00:08:29,236 --> 00:08:35,374
Tohumları, tohum kabukları 
elle kolayca açılabildiğinde çıkarılmaya hazırdır. 

62
00:08:37,403 --> 00:08:38,690
Tohumları çıkarmak için,

63
00:08:38,800 --> 00:08:44,160
tohum kabukları 
plastik bir örtü veya kalın bir kumaş parçası üzerine yayılır

64
00:08:44,581 --> 00:08:47,461
ve ardından elle dövülür veya ovulur.

65
00:08:50,930 --> 00:08:56,021
Ayrıca bir torbaya koyup 
yumuşak bir yüzeye de vurabilirsiniz. 

66
00:08:57,420 --> 00:09:02,072
Daha büyük miktarlar, üzerlerinde yürünerek 
veya araba sürülerek işlenebilir. 

67
00:09:14,600 --> 00:09:20,000
Kolayca açılmayan tohum kabukları 
muhtemelen iyi çimlenmeyecek,

68
00:09:20,130 --> 00:09:21,796
olgunlaşmamış tohumlar içerir. 

69
00:09:26,581 --> 00:09:27,898
Ayıklama sırasında,

70
00:09:28,130 --> 00:09:32,690
tohumlar önce samanları tutan 
geniş delikli bir elekten geçirilerek

71
00:09:33,025 --> 00:09:34,740
ve ardından tohumları tutan

72
00:09:39,505 --> 00:09:42,043
ancak daha küçük parçaların düşmesine izin veren

73
00:09:42,501 --> 00:09:47,060
başka bir elekten geçirilerek 
çer çöpten arındırılır. 

74
00:09:50,760 --> 00:09:54,610
Son olarak, tohumların üzerine 
üfleyerek

75
00:09:55,040 --> 00:09:59,025
ya da rüzgâr yardımıyla 
samanları temizlemelisiniz. 

76
00:10:14,225 --> 00:10:19,180
Brassica oleracea türlerinin 
tüm tohumları birbirine benzer.

77
00:10:20,174 --> 00:10:24,101
Örneğin lahana ve 
karnabahar tohumlarını

78
00:10:24,400 --> 00:10:26,865
birbirinden ayırmak çok zordur.

79
00:10:27,381 --> 00:10:30,320
Bu nedenle bitkileri ve ardından çıkarılan tohumları

80
00:10:30,589 --> 00:10:34,349
türün adı, çeşidi ve
ekim yılı ile

81
00:10:34,600 --> 00:10:37,643
etiketlemek önemlidir. 

82
00:10:39,076 --> 00:10:43,767
Tohumları birkaç gün dondurucuda saklamak 
parazitleri ortadan kaldırır. 

83
00:10:49,280 --> 00:10:52,429
Lahana tohumları 
5 yıla kadar çimlenebilmektedir.

84
00:10:53,512 --> 00:10:57,200
Ancak bu kapasitelerini 
10 yıla kadar koruyabilirler.

85
00:10:58,880 --> 00:11:02,290
Dondurucuda saklanarak 
bu süre uzatılabilir.

86
00:11:03,243 --> 00:11:09,680
Bir gramında, çeşidine göre 
250 ila 300 tohum bulunur. 
