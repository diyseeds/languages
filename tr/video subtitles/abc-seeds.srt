1
00:00:13,380 --> 00:00:16,340
Hangi tohum ile başlanmalı?

2
00:00:23,640 --> 00:00:26,580
Bahçede tohumluk olarak 
yetiştirilecek olan bitkilerin

3
00:00:26,640 --> 00:00:29,050
kökeni bilinmelidir.

4
00:00:35,540 --> 00:00:39,410
Tohum üretmesi için 
seçilecek olan çeşitlerin

5
00:00:39,760 --> 00:00:44,070
tohum sanayii içerisinde elde edilenlerden 
olmamasına gösterilmelidir.

6
00:00:44,870 --> 00:00:50,550
F1 veya F2 gibi hibrid tohumlar,

7
00:00:50,840 --> 00:00:53,840
GDO’lar ve biyoteknoloji ile üretilmiş tüm tohumlar kaçınılması gerekenler arasındadır. 

8
00:01:00,070 --> 00:01:07,540
F1 veya F2 hibrid tohumlarından elde edilmiş olan bitkiler 
birebir aynı şekilde çoğalmazlar.

9
00:01:08,430 --> 00:01:14,435
Tohumları da kısır olabilir veya neye benzeyeceğinin 
öngörülmesi mümkün olmayan bitkiler verebilir.

10
00:01:23,935 --> 00:01:26,295
Teknolojik kilit kullanan tohum üreticisi çokuluslu şirketler

11
00:01:26,825 --> 00:01:31,550
kendi sattıkları bitki çeşitlerinin 
yeniden üretilmesini engelleyebilir

12
00:01:31,930 --> 00:01:34,975
ve satış üzerindeki tekel konumlarını muhafaza ederler. 

13
00:01:38,995 --> 00:01:43,545
Bu teknik zorunluluklar ile 
tohumların serbestçe çoğaltılmasını

14
00:01:43,745 --> 00:01:46,630
yasaklayan yasal düzenlemeler elele ilerler.

15
00:01:49,390 --> 00:01:54,245
Bu çeşitlerin tümü, bitki mülkiyet hakları 
ve patentler şeklinde

16
00:01:54,945 --> 00:01:58,230
fikri mülkiyet hakları kapsamındadır. 

17
00:02:03,300 --> 00:02:05,910
Kendi tohumlarınızı üretmeye başlamak için,

18
00:02:06,485 --> 00:02:10,425
tohumları açık tozlaşan çeşitlerden 
ve mümkün olduğu ölçüde organik,

19
00:02:10,780 --> 00:02:17,065
biyodinamik veya 
agroekolojik tarımdan temin etmelisiniz. 

20
00:02:38,575 --> 00:02:40,640
Doğa fazlasıyla cömert ve bereketlidir

21
00:02:40,855 --> 00:02:46,165
çoğu zaman size kendi kullanımınız için ihtiyaç duyacağınızdan 
çok daha fazla tohum sağlayacaktır.

22
00:02:57,520 --> 00:03:01,480
İhtiyacınızdan fazla olan tohumları başkalarına verebilir veya takas edebilirsiniz. 
