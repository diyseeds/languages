# Copyright (C) 2021 Maxime Lecoq
# This file is distributed under the GNU General Public License v2 or later.
msgid ""
msgstr ""
"Project-Id-Version: Diyseeds 2.0\n"
"POT-Creation-Date: 2021-07-10T00:00:00+02:00\n"
"PO-Revision-Date: 2022-01-15 15:14+0000\n"
"Last-Translator: Olya Zubyk <olya.zubyk@gmail.com>\n"
"Language-Team: Ukrainian <http://translate.diyseeds.org/projects/"
"diyseeds-org-videos/abc-selection/uk/>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n%10==1 && n%100!=11 ? 0 : n%10>=2 && n"
"%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Weblate 4.8\n"
"WPOT-Type: post-types/video\n"
"X-Domain: wpot\n"
"WPOT-Origin: abc-selection\n"

#. Title of the film
msgctxt "title"
msgid "How to select plants for seed production"
msgstr ""

msgid "The selection process enables you to gradually adapt the plant to the environment, to your needs and wishes."
msgstr ""
"Процес селекції дозволяє вам поступово адаптувати рослини до середовища, "
"ваших потреб і побажань."

msgid "It is important to pay particular attention to plants grown for their seeds, as they will produce the future generations of vegetables and fruits. It is therefore necessary to choose them well and to observe them throughout their development."
msgstr ""
"Дуже важливо піклуватися про насінники під час їх зростання, адже вони "
"дадуть насіння для подальших поколінь овочів і фруктів. Тому дуже важливо "
"ретельно добирати перше насіння і уважно стежити за процесом його зростання."

msgid "Selection carried out only on the basis of the fruit will not reflect all of the characteristics linked to the development of plants. Selection criteria must be precisely defined, as they will determine the choice of plants for seed production."
msgstr ""
"Селекція, проведена лише на основі плодів, не відображатиме всіх "
"характеристик, пов’язаних з розвитком рослин. Критерії відбору повинні бути "
"точно визначені, оскільки вони визначатимуть вибір рослин для виробництва "
"насіння."

msgid "You should first of all consider the criteria specific to the variety, such as resistance to diseases, yield, precocity, as well as more subjective criteria such as taste and appearance. The capacity of plants to adapt to their environment and to cultivation methods should also be taken into account."
msgstr ""
"Перш за все, слід врахувати критерії, характерні для підвиду, такі як "
"стійкість до хвороб, урожайність, скороспілість, а також більш суб’єктивні "
"критерії, такі як смак та зовнішній вигляд. Слід також враховувати здатність "
"рослин адаптуватися до свого середовища та методів вирощування."

msgid "The key thing is to know from the start what aspects you want to give priority to, because over generations the variety will evolve and certain characteristics will become less present or dominant."
msgstr ""
"Найголовніше - знати з самого початку, яким аспектам ви хочете надати "
"пріоритет, адже протягом багатьох поколінь різноманітність буде розвиватися, "
"а певні характеристики ставатимуть менш присутніми чи домінантними."

msgid "Sometimes it is not wise to carry out a selection process. This is the case when there are very few seeds left of a rare variety facing the risk of extinction, as there will not be enough plants produced the first year to enable you to carry out selection."
msgstr ""
"Інколи нерозумно проводити процес відбору. Це той випадок, коли від "
"рідкісного підвиду, якому загрожує зникнення, залишилося дуже мало насіння, "
"оскільки не буде достатньо рослин, вирощених в перший рік, щоб можна було "
"проводити селекцію."

msgid "You can also decide to preserve the variety as it is, without a selection process. In this case you take all of the elements of the variety, even if there is a strong disparity, in order to encourage genetic diversity. This creates a strong capacity for adaptation and provides a reservoir of potentialities, which could provide the basis of future selection."
msgstr ""
"Ви також можете вирішити зберегти підвид таким, який він є, без процесу "
"селекції. У цьому випадку ви берете всі елементи підвиду, навіть якщо є "
"сильна невідповідність, щоб заохотити генетичне різноманіття. Це створює "
"потужну здатність до адаптації та забезпечує резерв потенціалів, що може "
"послужити основою майбутнього відбору."

msgid "It is important to clearly mark the plants grown for seed production so as to distinguish them from those grown for their vegetables. Drawing up a plan of the garden indicating the location of each variety will help you find them if the labels have disappeared."
msgstr ""
"Важливо чітко маркувати рослини, що вирощуються для виробництва насіння, щоб "
"відрізняти їх від тих, що вирощуються для отримання овочів. Складання плану "
"саду із зазначенням розташування кожного сорту допоможе вам знайти їх, якщо "
"етикетки зникли."

msgid "The complete cycle of plants through to their seeds is often longer than that of those grown for their vegetables. For example, you can eat lettuce after two or three months, but its full life cycle through to seed harvest is five to six months. There exist many biennial plants, such as the carrot, which flower and produce their seeds in the second year of growth. The best of all is to reserve a small corner of the garden specifically for seed production."
msgstr ""
"Повний цикл рослин до збору їх насіння часто довший, ніж у тих, що "
"вирощуються для отримання плодів. Наприклад, салат можна їсти через два-три "
"місяці, але його повний життєвий цикл до збирання насіння становить п'ять-"
"шість місяців. Існує багато дворічних рослин, наприклад морква, яка квітне і "
"дає свої насіння на другий рік зростання. Найкраще - зарезервувати невеликий "
"куточок саду спеціально для виробництва насіння."

#. This title will be displayed by search engines like Google. 60 characters max.
msgctxt "seo-title"
msgid "Selection of plants grown for their seeds"
msgstr "Вибір рослин, що вирощуються для отримання насіння"

#. This description will be displayed below the title on search engine results. 160 characters max.
msgctxt "seo-description"
msgid "The selection process enables you to gradually adapt the plant to the environment, to your needs and wishes. Video explanations."
msgstr ""
"Процес вибору дозволяє поступово адаптувати рослину до навколишнього "
"середовища, до ваших потреб та побажань. Відеопояснення."

#. The tags are used by search engines to reference the content. Use commas to separate tags.
msgctxt "seo-tags"
msgid "selection, seeds, seed carrier, environment, adaptation, disappearance, explanations, functioning, learn, ABC, video"
msgstr ""
"відбір, насіння, носій насіння, середовище, адаптація, зникнення, пояснення, "
"функціонування, навчання, азбука, відео"

#. This title will be displayed on Online Video Platforms (OVP) like YouTube or Vimeo.
msgctxt "ovp-title"
msgid "ABC How to select and grow plants intended for seed production"
msgstr ""

#. This description will be displayed on Online Video Platforms (OVP) like YouTube or Vimeo.
msgctxt "ovp-description"
msgid "The selection process enables you to gradually adapt the plant to the environment, to your needs and wishes. It is important to pay particular attention to plants grown for their seeds, as they will produce the future generations of vegetables and fruits. It is therefore necessary to choose them well and to observe them throughout their development. In this video, learn how to select the best plants for seed production depending on the criteria you have chosen."
msgstr ""
