# Copyright (C) 2021 Maxime Lecoq
# This file is distributed under the GNU General Public License v2 or later.
msgid ""
msgstr ""
"Project-Id-Version: Diyseeds 2.0\n"
"POT-Creation-Date: 2021-07-10T00:00:00+02:00\n"
"PO-Revision-Date: 2022-01-14 15:14+0000\n"
"Last-Translator: Olya Zubyk <olya.zubyk@gmail.com>\n"
"Language-Team: Ukrainian <http://translate.diyseeds.org/projects/"
"diyseeds-org-organizations/european-civic-forum/uk/>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n%10==1 && n%100!=11 ? 0 : n%10>=2 && n"
"%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Weblate 4.8\n"
"WPOT-Type: post-types/organization\n"
"WPOT-Origin: the-european-civic-forum\n"
"X-Domain: wpot\n"

#. This title will be displayed into the browser's window title bar.
msgctxt "browser-title"
msgid "The European Civic Forum"
msgstr "Європейський громадянський форум"

#. The slug is the last part of the URL (www.example.org/slug-is-here). Only alphanumerical characters are allowed, use dash to replace space. The slug should be short and nice and it must reflect the content it refers to. It is usually made from the title. If you don't provide it, a slug will be auto-generated.
msgctxt "slug"
msgid "the-european-civic-forum"
msgstr "європейський-громадянський-форум"

msgctxt "name"
msgid "The European Civic Forum"
msgstr "Європейський громадянський форум"

msgctxt "description"
msgid "An international solidarity network active, among others, in the defence of open-pollinated seeds and small-scale farming that respects social rights."
msgstr ""
"Міжнародна мережа солідарності, активна, зокрема, в сфері захисту насіння, "
"отриманого відкритим способом, і дрібних фермерських господарств, які "
"поважають соціальні права."

#. This title will be displayed by search engines like Google. 60 characters max.
msgctxt "seo-title"
msgid "The European Civic Forum, an international solidarity network"
msgstr "Європейський громадянський форум, міжнародна мережа солідарності"

#. This description will be displayed below the title on search engine results. 160 characters max.
msgctxt "seo-description"
msgid "International solidarity network that defends open-pollinated seeds and small-scale agriculture that respects social rights."
msgstr ""
"Міжнародна мережа солідарності, активна в сфері захисту насіння, отриманого "
"відкритим способом, і дрібних фермерських господарств, які поважають "
"соціальні права."

#. The tags are used by search engines to reference the content. Use commas to separate tags.
msgctxt "seo-tags"
msgid "network, international solidarity, seeds, defence, social rights, agriculture"
msgstr ""
"мережа, міжнародна солідарність, насіння, захист, соціальні права, сільське "
"господарство"
