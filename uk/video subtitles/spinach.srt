1
00:00:07,883 --> 00:00:14,676
Spinach belongs to the Chenopodiaceae family
and to the Spinacia oleracea species.

2
00:00:19,316 --> 00:00:24,123
They are annual or biennial plants
cultivated for their leaves.

3
00:00:27,750 --> 00:00:34,890
Certain varieties of spinach are winter crops,
others grow in spring or summer.

4
00:00:39,396 --> 00:00:40,661
Pollination 

5
00:00:47,636 --> 00:00:54,334
Spinach is a dioecious species, meaning that
there are male plants that disseminate pollen

6
00:00:56,749 --> 00:01:00,240
and female plants that bear the fertile seeds. 

7
00:01:02,560 --> 00:01:08,305
The female flowers are discreet
and located at the axil of the branches. 

8
00:01:09,403 --> 00:01:15,847
The male flowers are at the top of the plant
and begin flowering before the female plants. 

9
00:01:19,840 --> 00:01:21,694
Spinach is allogamous,

10
00:01:22,152 --> 00:01:26,305
meaning that cross-fertilisation
takes place between different plants.

11
00:01:28,080 --> 00:01:29,832
It is wind-pollinated. 

12
00:01:34,680 --> 00:01:36,836
It is a “daylength plant”

13
00:01:37,061 --> 00:01:43,963
which means that it will start forming
its flowers once the days last 10 to 14 hours.

14
00:01:50,000 --> 00:01:52,349
Flowering lasts 2 to 3 weeks. 

15
00:01:55,869 --> 00:02:02,581
To avoid cross-fertilisation,
grow two varieties of spinach 1km apart. 

16
00:02:05,680 --> 00:02:12,247
You can reduce this distance to 500m
if there is a natural barrier, such as a hedge. 

17
00:02:14,989 --> 00:02:20,356
There are different methods that enable you
to cultivate two varieties in the same garden.

18
00:02:21,360 --> 00:02:25,447
You can stagger out the times
when you sow out different varieties,

19
00:02:25,800 --> 00:02:29,550
thereby ensuring
that their flowering will not coincide,

20
00:02:30,247 --> 00:02:35,025
but you must make sure that the full seed
to seed cycle can be completed. 

21
00:02:37,320 --> 00:02:40,443
You can also use
the mechanical isolation method,

22
00:02:40,814 --> 00:02:45,345
covering each variety with mosquito nets
that you open alternately.

23
00:02:45,949 --> 00:02:47,934
For details about these methods,

24
00:02:48,058 --> 00:02:53,040
consult the isolation techniques chapter
in the 'ABC of Seed production'. 

25
00:03:01,530 --> 00:03:02,894
Life cycle 

26
00:03:13,789 --> 00:03:15,498
When growing spinach for seeds,

27
00:03:15,585 --> 00:03:19,243
the cultivation techniques
differ according to the variety : 

28
00:03:19,752 --> 00:03:23,483
spring varieties are sown out
early in the season.

29
00:03:24,014 --> 00:03:27,934
They will blossom and produce seeds
in the summer of the same year.

30
00:03:28,138 --> 00:03:32,523
They cannot be cultivated in winter
since they will not survive the cold. 

31
00:03:33,440 --> 00:03:36,370
Winter varieties are sown out in autumn.

32
00:03:36,778 --> 00:03:38,220
They develop in winter,

33
00:03:38,472 --> 00:03:42,080
and blossom and produce seeds
in the following spring. 

34
00:03:56,560 --> 00:04:02,741
It is important to grow 25 to 30 plants
for seeds to ensure good genetic diversity.

35
00:04:21,150 --> 00:04:26,836
Choose healthy plants that correspond
to the variety’s specific characteristics.

36
00:04:34,189 --> 00:04:38,654
A good criteria for winter species
is resistance to the cold

37
00:04:39,570 --> 00:04:45,563
and above all to root asphyxia
which results in the leaves turning yellow.

38
00:04:45,760 --> 00:04:48,305
This is a common phenomenon in winter.

39
00:04:52,247 --> 00:04:56,770
It is better not to harvest too many leaves
from plants grown for seeds. 

40
00:05:19,949 --> 00:05:27,105
The flowering spikes of spinach can grow
to as high as 80 cm but they do not need staking.

41
00:05:31,076 --> 00:05:33,730
The male plants will dry out first.

42
00:05:33,960 --> 00:05:35,861
It is better to uproot them. 

43
00:05:46,880 --> 00:05:51,396
The female flowers can be recognised
by their light sandy colour. 

44
00:06:08,007 --> 00:06:12,501
Cut the flowering spikes
after the dew once the seeds are mature. 

45
00:06:21,270 --> 00:06:26,940
It is best to continue the drying
process in a dry and well ventilated place. 

46
00:06:47,040 --> 00:06:49,660
Extracting - sorting - storing 

47
00:06:55,207 --> 00:06:58,901
Extract the seeds by rubbing the stems
between your hands.

48
00:06:59,738 --> 00:07:01,340
It is better to wear gloves. 

49
00:07:04,952 --> 00:07:08,349
You can also walk on the stems,
or beat them with a stick. 

50
00:07:15,960 --> 00:07:20,160
Sift the seeds through a coarse
sieve that will retain the debris.

51
00:07:31,541 --> 00:07:36,490
Then use a finer sieve that will retain
the seeds and get rid of the dust. 

52
00:07:54,232 --> 00:07:56,060
Finish by winnowing the seeds.

53
00:07:56,625 --> 00:08:01,389
You can either blow on the surface
to get rid of the last debris, or use the wind. 

54
00:08:19,236 --> 00:08:24,780
Always put a label with the name of the variety
and species as well as the year inside the sachet,

55
00:08:25,069 --> 00:08:27,221
as writing on the outside may rub off. 

56
00:08:34,520 --> 00:08:38,916
Leave the seeds in the freezer
a few days to kill parasite larvae. 

57
00:08:42,090 --> 00:08:48,480
Spinach seeds have a germination capacity
of 5 years, sometimes it can last 7 years.

58
00:08:49,650 --> 00:08:52,552
To keep them longer,
store them in the freezer.

59
00:08:54,167 --> 00:08:57,098
One gram contains about 100 seeds. 
