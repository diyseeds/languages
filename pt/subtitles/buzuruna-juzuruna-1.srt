﻿1
00:00:00,000 --> 00:00:05,760
Entrevista com Serge Harfouche de Buzuruna Juzuruna
Maio 2021, Beqaa Valley, Líbano
www.DIYseeds.org
filmes educativos sobre a produção de sementes

2
00:00:05,920 --> 00:00:08,760
O que significa "Buzuruna Juzuruna"?

3
00:00:09,520 --> 00:00:12,100
Significa "nossas sementes, nossas raízes".

4
00:00:12,140 --> 00:00:16,150
E como esse nome representa a associação de vocês?

5
00:00:17,870 --> 00:00:25,170
Nós somos uma fazenda escola que produz sementes camponesas

6
00:00:26,090 --> 00:00:30,570
e também damos cursos.

7
00:00:30,660 --> 00:00:35,520
São nossas duas principais áreas de atuação.

8
00:00:36,220 --> 00:00:39,350
Produzimos sementes

9
00:00:39,530 --> 00:00:42,460
e essas mesmas sementes viram raízes.

10
00:00:42,460 --> 00:00:47,020
A idéia é de se aproximar de uma autonomia camponesa

11
00:00:47,130 --> 00:00:49,380
e de uma soberania alimentar.

12
00:00:49,380 --> 00:00:53,120
Isso tudo parece funcionar bem.

13
00:00:54,080 --> 00:01:04,730
Porque a soberania e a autonomia alimentares são tão importantes no Líbano nesse momento?

14
00:01:05,600 --> 00:01:07,840
É uma urgência.

15
00:01:07,860 --> 00:01:13,600
Estamos vivendo uma crise econômica histórica sem precedentes,

16
00:01:13,600 --> 00:01:15,600
que é extremamente violenta.

17
00:01:15,710 --> 00:01:24,150
Nossa moeda perdeu 10 vezes o seu valor.

18
00:01:24,260 --> 00:01:30,020
Passamos de 1500 libras libanesas para um dólar à 15000 num momento

19
00:01:30,040 --> 00:01:32,880
e agora está estabilizado a mais ou menos 12 000.

20
00:01:32,950 --> 00:01:36,600
É muito aleatório, a gente não sabe quando o mercado vai explodir.

21
00:01:36,680 --> 00:01:37,770
Desde quando estão nessa situação?

22
00:01:37,860 --> 00:01:43,400
A queda começou por volta de 2018

23
00:01:43,510 --> 00:01:54,640
mas sentimos a inflação no mercado a partir do meio de 2019, eu acho.

24
00:01:55,950 --> 00:02:01,260
-Quando isso acontece, qual o impacto na vida das pessoas?

25
00:02:02,600 --> 00:02:07,020
É terrível porque os salários não aumentaram em libras libanesas.

26
00:02:07,060 --> 00:02:12,200
O salário mínimo valia em torno de 400 € por mês.

27
00:02:12,200 --> 00:02:17,350
agora vale 40 €,

28
00:02:17,880 --> 00:02:21,620
mas tudo o que consumimos está mais caro

29
00:02:21,680 --> 00:02:24,750
porque também é indexado ao dólar, está terrível.

30
00:02:24,840 --> 00:02:27,910
Imagina que você compra uma caixa de leite.

31
00:02:27,930 --> 00:02:31,530
Uma caixa de leite que custava 3000 libras libanesas

32
00:02:31,530 --> 00:02:36,480
agora custa 27 ou 25,000,

33
00:02:36,600 --> 00:02:40,200
enquanto você continua recebendo 100.000 por exemplo.

34
00:02:40,200 --> 00:02:44,160
então ao invés de poder comprar várias no mês

35
00:02:44,260 --> 00:02:47,380
você só pode comprar uma.

36
00:02:47,440 --> 00:02:59,180
Como o nosso consumo depende de 90 a 95% da importação,

37
00:02:59,260 --> 00:03:01,580
quando depende de uma moeda estrangeira

38
00:03:01,620 --> 00:03:03,260
-o dólar, no nosso caso-,

39
00:03:03,310 --> 00:03:05,520
tudo fica bem mais caro:

40
00:03:05,570 --> 00:03:08,210
o pão fica mais caro,

41
00:03:08,360 --> 00:03:11,450
o combustível fica mais caro

42
00:03:11,560 --> 00:03:14,660
e os salários são sempre os mesmos.

43
00:03:14,690 --> 00:03:17,720
O que significa que o nível de vida,

44
00:03:17,750 --> 00:03:20,820
a qualidade de vida é fortemente impactada

45
00:03:20,910 --> 00:03:23,090
e o acesso à alimentação

46
00:03:23,110 --> 00:03:26,530
e a segurança alimentar são impactadas.

47
00:03:26,720 --> 00:03:30,370
Daí vem a urgência de produzir suas próprias sementes

48
00:03:30,430 --> 00:03:33,200
ao invés de importar ou de comprar em algum lugar

49
00:03:33,220 --> 00:03:35,490
principalmente se elas são híbridas ou estéreis

50
00:03:35,520 --> 00:03:38,230
e que no ano seguinte tem que comprar de novo.

51
00:03:40,010 --> 00:03:43,980
Precisamos também produzir nossos próprios fertilizantes

52
00:03:44,020 --> 00:03:46,480
de maneira orgânica,

53
00:03:46,530 --> 00:03:49,810
fazer compostagem...

54
00:03:49,890 --> 00:03:51,540
O ??????, como eles dizem.

55
00:03:51,570 --> 00:03:55,290
É o que tentamos fazer nos nossos cursos

56
00:03:55,330 --> 00:03:57,330
e graças à atividade da fazenda.

57
00:03:58,730 --> 00:04:01,460
- Quando você estava na França você nos mostrou

58
00:04:01,470 --> 00:04:04,110
fotos de como,

59
00:04:04,160 --> 00:04:07,630
depois do que você chamou de revolução,

60
00:04:07,640 --> 00:04:10,560
a vida mudou para muita gente,

61
00:04:10,660 --> 00:04:12,610
diversos tipos de pessoas:

62
00:04:12,630 --> 00:04:18,410
estudantes, funcionários,

63
00:04:18,430 --> 00:04:20,730
pessoas de todos os tipos que vem

64
00:04:20,750 --> 00:04:23,360
e que se interessam de repente por tudo isso.

65
00:04:23,630 --> 00:04:24,810
Exatamente.

66
00:04:24,930 --> 00:04:27,530
Parece que a gente estava vindo a coisa vir,

67
00:04:28,180 --> 00:04:33,380
que de alguma maneira nós já estávamos em uma crise sem precedentes.

68
00:04:33,490 --> 00:04:38,210
Por isso que a revolução começou.

69
00:04:38,270 --> 00:04:43,040
Então as pessoas tinham uma vaga consciência

70
00:04:43,040 --> 00:04:47,650
que os anos que estão por vir vão ser bem complicados

71
00:04:47,680 --> 00:04:50,490
e que elas vão ter que se virar de alguma maneira.

72
00:04:50,540 --> 00:04:54,340
Estamos cada vez mais conscientes

73
00:04:54,400 --> 00:05:00,000
que uma economia que não é produtiva não é mais viável,

74
00:05:00,080 --> 00:05:03,130
e a nossa não era uma economia produtiva.

75
00:05:03,460 --> 00:05:05,920
Durante muito tempo, 30, 40 anos,

76
00:05:05,970 --> 00:05:10,250
era realmente uma economia baseada na dívida.

77
00:05:11,090 --> 00:05:15,320
-Então muita gente começou a produzir?

78
00:05:15,460 --> 00:05:17,980
...

79
00:05:18,040 --> 00:05:19,290
Está gravando.

80
00:05:20,430 --> 00:05:23,260
Desculpe, eu estava falando com o meu colega.

81
00:05:23,280 --> 00:05:26,070
-Tem muita gente na fazenda?

82
00:05:26,850 --> 00:05:31,620
Somos 16 adultos e 27 crianças

83
00:05:31,650 --> 00:05:34,160
e agora temos vários amigos visitando,

84
00:05:34,210 --> 00:05:36,000
voluntários, estagiários, ...

85
00:05:36,030 --> 00:05:38,930
No total somos umas 50 pessoas,

86
00:05:39,000 --> 00:05:40,370
Talvez até mais.

87
00:05:40,410 --> 00:05:43,350
-E que tamanho tem a fazenda?

88
00:05:44,940 --> 00:05:47,770
Se contarmos a fazenda e a associação

89
00:05:47,920 --> 00:05:54,140
temos 2 hectares de terra:

90
00:05:54,580 --> 00:05:57,940
18 000 m² cultivados

91
00:05:57,950 --> 00:06:00,500
e 2000 m² para os prédios ,

92
00:06:00,670 --> 00:06:02,980
as estufas

93
00:06:03,070 --> 00:06:06,050
os galinheiros,

94
00:06:06,060 --> 00:06:07,640
e curral para as cabras e ovelhas.

95
00:06:07,710 --> 00:06:09,820
E esse ano a gente conseguiu

96
00:06:09,830 --> 00:06:13,490
um terreno adicional de 70,000 m²,

97
00:06:13,610 --> 00:06:16,600
acho que 7 hectares a mais,

98
00:06:16,700 --> 00:06:19,700
para multiplicar e aumentar a nossa ?????,

99
00:06:19,720 --> 00:06:21,350
principalmente os cereais.

100
00:06:22,280 --> 00:06:24,770
-Mas você não mora na fazenda?

101
00:06:25,610 --> 00:06:27,510
Não exatamente.

102
00:06:27,520 --> 00:06:32,860
Um dos nossos colegas e cofundadores mora na fazenda.

103
00:06:34,400 --> 00:06:36,320
Temos uma construção polivalente

104
00:06:36,340 --> 00:06:39,780
que é uma cozinha profissional,

105
00:06:39,820 --> 00:06:43,090
cozinha para nós, área de estocagem, escola

106
00:06:43,140 --> 00:06:44,350
e apartamento,

107
00:06:44,360 --> 00:06:47,040
um prédio longo.

108
00:06:47,060 --> 00:06:50,120
É o Walid que mora lá com a família dele.

109
00:06:50,260 --> 00:06:52,440
Nossos outros colegas e amigos

110
00:06:52,460 --> 00:06:54,260
moram do outro lado da rua,

111
00:06:54,310 --> 00:06:56,530
e nós estamos um pouco à frente na rua,

112
00:06:56,550 --> 00:06:58,450
a 4 minutos a pé.

113
00:06:59,590 --> 00:07:02,290
-E vocês também produzem um pouco

114
00:07:02,300 --> 00:07:04,400
do que vocês consomem?

115
00:07:04,740 --> 00:07:06,950
Sim!

116
00:07:07,160 --> 00:07:10,070
Compramos muito pouca coisa.

117
00:07:10,110 --> 00:07:13,770
Tudo o que a gente pode produzir, não compramos, nós temos!

118
00:07:14,900 --> 00:07:17,980
Tem coisas como o arroz,

119
00:07:18,010 --> 00:07:20,880
que é um pouco difícil da gente produzir,

120
00:07:20,910 --> 00:07:22,690
esse tipo de coisa.

121
00:07:22,920 --> 00:07:24,120
Mas o resto…

122
00:07:24,130 --> 00:07:27,230
Ano passado a gente teve até um pouco de mel,

123
00:07:27,250 --> 00:07:30,520
mas um predador atacou nossas abelhas,

124
00:07:30,560 --> 00:07:32,600
então só tivemos mel de 3 colméias,

125
00:07:32,620 --> 00:07:34,570
das 11 que tínhamos.

126
00:07:36,830 --> 00:07:39,980
A idéia também é mostrar que é possível.

127
00:07:41,590 --> 00:07:46,920
-E sobre os parceiros, vínculos, rede de contatos

128
00:07:46,940 --> 00:07:50,600
que vocês tem no Líbano e na Síria,

129
00:07:50,610 --> 00:07:54,570
como começou, como está avançando?

130
00:07:54,590 --> 00:07:56,450
Conta um pouco!

131
00:07:57,100 --> 00:08:00,160
Basicamente a idéia principal era produzir sementes

132
00:08:00,180 --> 00:08:03,170
e depois de ensinar a quem se interessasse

133
00:08:03,190 --> 00:08:04,560
em ter sua própria produção.

134
00:08:04,590 --> 00:08:08,450
assim você não precisa recomeçar a cada ano.

135
00:08:08,450 --> 00:08:11,030
É realmente o aprendizado da autonomia

136
00:08:11,040 --> 00:08:12,560
e da independência.

137
00:08:12,580 --> 00:08:16,380
Então, a partir daí, pequenos produtores de sementes

138
00:08:16,400 --> 00:08:17,910
foram para vários lugares ????????????

139
00:08:17,920 --> 00:08:19,710
Alguns voltaram para a Síria

140
00:08:19,710 --> 00:08:22,260
para produzir suas próprias sementes na Síria.

141
00:08:22,280 --> 00:08:25,120
São diversas situações diferentes.

142
00:08:25,150 --> 00:08:29,610
Os amigos que temos acompanhado desde o começo

143
00:08:29,630 --> 00:08:32,020
agora são completamente autônomos,

144
00:08:32,040 --> 00:08:34,970
Eles estão se virando muito bem.

145
00:08:34,980 --> 00:08:36,910
Claro que a gente propõe um acompanhamento,

146
00:08:36,920 --> 00:08:40,350
se precisarem trocar informações ou experiência:

147
00:08:40,360 --> 00:08:42,840
"a gente testou isso", "eles testaram isso".

148
00:08:42,920 --> 00:08:46,690
A idéia é ter o maior número de produtores de sementes

149
00:08:46,690 --> 00:08:49,360
e de bancos de sementes possível

150
00:08:49,390 --> 00:08:51,640
em todo o território libanês

151
00:08:51,690 --> 00:08:55,600
e talvez até na Síria, quem sabe.

152
00:08:57,460 --> 00:09:02,240
-A fronteira ainda está fechada?

153
00:09:03,970 --> 00:09:06,550
Sim, está fechada

154
00:09:06,570 --> 00:09:09,540
e é bem difícil passar

155
00:09:09,610 --> 00:09:12,040
pra pessoas de certas nacionalidades.

156
00:09:12,050 --> 00:09:16,250
Se você não faz parte de uma comitiva diplomática ou do exército

157
00:09:16,400 --> 00:09:17,990
é bem difícil -

158
00:09:18,660 --> 00:09:20,860
melhor nem tentar.

159
00:09:21,700 --> 00:09:23,550
Melhor ficar onde você está.

160
00:09:23,580 --> 00:09:25,730
-Sobre os cursos...

161
00:09:25,920 --> 00:09:30,670
Você participou da tradução de DIYseeds -

162
00:09:34,330 --> 00:09:37,440
você traduziu o site?

163
00:09:38,750 --> 00:09:40,100
... o site em árabe,

164
00:09:40,130 --> 00:09:44,680
e eu revisei os vídeos que foram feitos pour uma outra equipe,

165
00:09:44,820 --> 00:09:47,750
- um trabalhão... -

166
00:09:47,760 --> 00:09:50,090
É muito doido o que eles fizeram, é tão bonito.

167
00:09:50,170 --> 00:09:52,080
É super prático.

168
00:09:52,120 --> 00:09:54,720
Quando você dá um curso

169
00:09:54,780 --> 00:09:57,840
quando você tenta explicar como extrair sementes

170
00:09:57,870 --> 00:10:01,660
ou como respeitar distâncias de segurança

171
00:10:01,670 --> 00:10:03,360
para evitar cruzamentos,

172
00:10:03,380 --> 00:10:05,360
é melhor com as ilustrações,

173
00:10:05,360 --> 00:10:08,140
os desenhos, o vídeo e as explicações.

174
00:10:08,140 --> 00:10:11,550
Além de mostrarmos estes vídeos nos cursos,

175
00:10:11,560 --> 00:10:13,470
mas eles também ficam disponíveis

176
00:10:13,480 --> 00:10:17,000
aos alunos nos telefones deles

177
00:10:17,020 --> 00:10:19,660
-isso quando eles tem internet, mas digamos, eles tem acesso-,

178
00:10:19,680 --> 00:10:23,800
isso permite que as pessoas sejam autônomas.

179
00:10:23,910 --> 00:10:25,680
É ótimo, perfeito.

180
00:10:27,600 --> 00:10:30,770
-E é fácil também pras pessoas da região

181
00:10:31,240 --> 00:10:35,560
pra terem acesso a essas informações, pra usar, pra …

182
00:10:35,640 --> 00:10:36,480
Exatamente!

183
00:10:36,540 --> 00:10:37,730
Facilita as coisas pra eles,

184
00:10:37,760 --> 00:10:40,500
porque é uma coisa leve, simples.

185
00:10:40,520 --> 00:10:44,910
Não é um livrão que você tem que carregar.

186
00:10:44,960 --> 00:10:47,940
Tem muitos agricultores que não sabem ler,

187
00:10:48,000 --> 00:10:50,220
mas eles sabem escutar.

188
00:10:50,260 --> 00:10:52,970
Tem também os desenhos e o próprio vídeo,

189
00:10:53,040 --> 00:10:55,570
que ilustram as ideias,

190
00:10:55,580 --> 00:10:58,770
e fica muito mais simples e acessível,

191
00:10:58,800 --> 00:11:01,830
mais fácil de aplicar com esse tipo de tutorial

192
00:11:01,870 --> 00:11:05,270
do que decifrar um grande livro.

193
00:11:06,000 --> 00:11:11,350
-Como as sementes que vocês produzem são não híbridas,

194
00:11:11,380 --> 00:11:14,580
sementes naturais, reprodutíveis

195
00:11:14,660 --> 00:11:18,030
permite a todos que tenham estas sementes

196
00:11:18,160 --> 00:11:21,380
e tenham acesso aos vídeos de ganhar rapidamente em autonomia.

197
00:11:21,400 --> 00:11:26,560
Você pode nos contar como e em quanto tempo pode funcionar?

198
00:11:26,580 --> 00:11:27,630
Em quanto tempo?

199
00:11:27,650 --> 00:11:30,530
Depende do acesso à terra,

200
00:11:30,560 --> 00:11:32,160
depende do acesso à água,

201
00:11:32,180 --> 00:11:34,310
depende da segurança da região.

202
00:11:34,320 --> 00:11:37,320
Temos amigos que tiveram que se mudar várias vezes

203
00:11:37,390 --> 00:11:39,260
da região em que estavam na Síria.

204
00:11:39,300 --> 00:11:41,170
A cada vez que eles se instalavam

205
00:11:41,180 --> 00:11:42,830
tinham bombardeamentos,

206
00:11:42,840 --> 00:11:44,810
aí eles tinham que se mudar de novo.

207
00:11:44,960 --> 00:11:47,240
O contexto é complicado.

208
00:11:47,320 --> 00:11:51,670
Mas quando você tem as ferramentas básicas,

209
00:11:51,710 --> 00:11:54,660
onde você chega você pode se virar.

210
00:11:54,690 --> 00:11:57,110
Você tem suas sementes, você sabe como plantá-las,

211
00:11:57,130 --> 00:12:00,260
você sabe como reproduzí-las, extrair,

212
00:12:00,290 --> 00:12:01,990
guardar, preservar, etc.

213
00:12:02,020 --> 00:12:04,980
Você é um homem ou mulher livres!

214
00:12:07,140 --> 00:12:08,050
É isso.

215
00:12:08,190 --> 00:12:12,120
-Você vê pessoas que em uma estação,

216
00:12:12,170 --> 00:12:15,180
aprenderam a fazer sementes -

217
00:12:15,580 --> 00:12:19,470
e agora tem suas próprias sementes?

218
00:12:19,520 --> 00:12:20,510
Exatamente.

219
00:12:20,530 --> 00:12:23,240
A maior parte das pessoas com as quais trabalhamos

220
00:12:23,290 --> 00:12:25,350
vem do meio agrícola,

221
00:12:25,370 --> 00:12:26,840
cresceram no meio agrícola,

222
00:12:26,850 --> 00:12:29,000
ou trabalharam com agricultura,

223
00:12:29,020 --> 00:12:30,440
mesmo se convencional.

224
00:12:30,460 --> 00:12:34,350
É mais fácil pra les fazerem a transição.

225
00:12:36,550 --> 00:12:38,500
Antes eles tinham que

226
00:12:38,520 --> 00:12:39,970
comprar sementes

227
00:12:39,970 --> 00:12:42,450
E assim que eles aprendem

228
00:12:42,470 --> 00:12:45,010
que seu destino está nas suas próprias mãos,

229
00:12:45,260 --> 00:12:47,100
porque les só precisavam

230
00:12:47,120 --> 00:12:50,730
das ferramentas adaptadas,

231
00:12:50,880 --> 00:12:53,890
em uma ou duas estações eles têm as própria sementes,

232
00:12:53,930 --> 00:12:55,040
e nós trocamos.

233
00:12:55,080 --> 00:12:56,800
Trabalhamos juntos a longo prazo,

234
00:12:56,820 --> 00:13:00,570
por exemplo alguém produz uma variedade,

235
00:13:00,580 --> 00:13:03,410
outra pessoa produz uma outra, produzimos uma terceira…

236
00:13:03,423 --> 00:13:08,680
pra ter a maior diversidade possível e pronto,

237
00:13:08,720 --> 00:13:10,680
começa a funcionar.

238
00:13:11,760 --> 00:13:16,280
-E como você começou a participar desse projeto?

239
00:13:18,426 --> 00:13:20,280
Por puro acaso.

240
00:13:20,330 --> 00:13:26,600
Eu morava em Beirute e meu colega de apartamento ia na universidade avec Ferdi,

241
00:13:26,693 --> 00:13:30,613
que é um dos cofundadores da associação e de projeto.

242
00:13:32,320 --> 00:13:35,253
Então quando o Ferdi e a Lara voltaram pro Líbano

243
00:13:35,293 --> 00:13:38,253
pra ver como começar o projeto das sementes,

244
00:13:38,290 --> 00:13:41,270
como produzir, onde fazer tudo isso,

245
00:13:41,308 --> 00:13:43,228
o que precisava fazer pra começar,

246
00:13:43,242 --> 00:13:44,927
eles ficaram na nossa casa

247
00:13:44,936 --> 00:13:46,592
durante um mês mais ou menos,

248
00:13:46,602 --> 00:13:48,545
e a gente se entendeu bem imediatamente.

249
00:13:49,727 --> 00:13:51,303
Como eu dizia:

250
00:13:51,336 --> 00:13:54,174
Eu me deixei levar conscientemente!

251
00:13:54,988 --> 00:13:56,898
-E você mudou sua vida?

252
00:13:57,068 --> 00:13:59,101
O que você fazia antes?

253
00:14:00,112 --> 00:14:03,303
Logo antes eu fazia comunicação

254
00:14:03,336 --> 00:14:04,823
pra uma universidade.

255
00:14:04,851 --> 00:14:06,960
Mas na origem eu sou um bibliotecário,

256
00:14:09,520 --> 00:14:12,640
Eu estudei psicologia, literatura,

257
00:14:12,672 --> 00:14:15,868
nada a ver com as sementes!

258
00:14:19,745 --> 00:14:21,520
Mas ajuda pra traduzir,

259
00:14:21,543 --> 00:14:22,922
então é perfeito!

260
00:14:23,275 --> 00:14:24,992
-E você quer continuar?

261
00:14:25,920 --> 00:14:27,637
Sim!

262
00:14:27,661 --> 00:14:29,863
Como a gente estava dizendo mais cedo:

263
00:14:29,890 --> 00:14:33,002
estamos vivendo uma urgência,

264
00:14:34,164 --> 00:14:39,185
Acho que esse trabalho é essencial,

265
00:14:40,014 --> 00:14:41,477
é necessário.

266
00:14:41,515 --> 00:14:47,298
e além do mais enriquecedor em vários aspectos:

267
00:14:47,310 --> 00:14:50,555
no nível social, político também.

268
00:14:50,597 --> 00:14:53,050
É uma atividade extremamente importante

269
00:14:53,072 --> 00:14:54,823
e bonita ao mesmo tempo.

270
00:14:55,247 --> 00:14:58,211
Acho que não vou parar tão cedo!

271
00:14:58,418 --> 00:15:01,797
-Ótimo! Obrigado pela entrevista.

272
00:15:01,960 --> 00:15:03,722
De nada, de nada!

273
00:15:03,755 --> 00:15:06,290
Foi legal conversar com você de novo.

274
00:15:07,620 --> 00:15:08,936
-Espero que a gente se veja de novo.

275
00:15:08,960 --> 00:15:11,134
Estamos te esperando na fazenda!

276
00:15:11,181 --> 00:15:15,811
-Eu adoraria :-) Mas não posso prometer mas…

277
00:15:17,463 --> 00:15:20,560
Quando você quiser, é só avisar,

278
00:15:20,583 --> 00:15:22,672
e "ahla wa sahla" (bem-vindo!)

279
00:15:25,251 --> 00:15:26,409
-Muito obrigado!

280
00:15:26,432 --> 00:15:27,091
Perfeito!

281
00:15:28,120 --> 00:15:35,720
Muito obrigado à Serge Harfouche e a todos de Buzuruna Juzuruna
mais informações: @buzurunajuzuruna (Instagram)
fotos: Charlotte Joubert, Buzuruna Juzuruna, Rabih Yassine
entrevista e edição: Erik D'haese

282
00:15:35,720 --> 00:15:40,320
www.DIYseeds.org
vídeos educativos para a produção de sementes
