﻿1
00:00:10,625 --> 00:00:14,247
Chard and beetroot belong
to the Chenopodiaceae family

2
00:00:14,356 --> 00:00:17,229
and the Beta vulgaris species.

3
00:00:22,021 --> 00:00:29,381
They are biennial plants grown
for their roots, stalks and leaves.

4
00:00:32,981 --> 00:00:35,134
There are five sub-species :

5
00:00:41,003 --> 00:00:44,487
- cicla includes different varieties of chard 

6
00:00:46,276 --> 00:00:50,189
esculenta is made up of vegetable beetroots 

7
00:00:52,945 --> 00:00:56,421
rapa consists of fodder beet varieties 

8
00:00:57,898 --> 00:01:01,723
altissima consists of sugar beet varieties 

9
00:01:03,149 --> 00:01:05,694
the wild chard, called maritima,

10
00:01:05,869 --> 00:01:10,501
can be found on the Atlantic coast
in Europe and as far away as India. 

11
00:01:12,080 --> 00:01:17,374
Certain varieties of beetroot are early,
while others are grown for conservation.

12
00:01:18,480 --> 00:01:23,112
The colour of the roots may vary :
they can be pink, red

13
00:01:23,447 --> 00:01:27,025
and sometimes yellow, light purple or white.

14
00:01:28,109 --> 00:01:35,301
Their shape can also be very different :
some are round, others flat, long or cone-shaped. 

15
00:01:41,680 --> 00:01:45,890
Chard is mainly known for its white
stalks and green leaves,

16
00:01:46,850 --> 00:01:50,530
but they can also be yellow, pink or red.

17
00:01:52,647 --> 00:01:58,800
Some varieties are entirely green and smaller,
resembling the wild species. 

18
00:02:07,360 --> 00:02:08,500
Pollination 

19
00:02:19,621 --> 00:02:23,178
The small flowers of chard
and beetroot are hermaphrodite.

20
00:02:24,763 --> 00:02:28,080
But the stamen,
the male sexual organ of the flower,

21
00:02:28,240 --> 00:02:34,298
releases its pollen before the pistil,
the female sexual organ, is ready to receive it.

22
00:02:36,567 --> 00:02:38,749
They are therefore self-sterile. 

23
00:02:42,487 --> 00:02:44,472
Chard and beet are autogamous,

24
00:02:44,840 --> 00:02:48,109
because they need other plants
to ensure fertilisation. 

25
00:02:53,723 --> 00:02:56,420
They are generally wind-pollinated,

26
00:03:02,174 --> 00:03:03,789
but certain insects,

27
00:03:04,036 --> 00:03:09,912
such as dyptera or hemiptera can also
pollinate chard and beetroot. 

28
00:03:17,061 --> 00:03:20,436
All chards, including
the wild ones cross-pollinate,

29
00:03:20,850 --> 00:03:24,043
as do beetroot, sugar and fodder beets. 

30
00:03:29,170 --> 00:03:36,858
To avoid cross-fertilisation, leave 1 km
between two varieties of Beta vulgaris.

31
00:03:38,625 --> 00:03:45,861
This distance may be reduced to 500m
if there is a natural barrier such as a hedge. 

32
00:03:46,880 --> 00:03:51,600
In regions where beet is cultivated
commercially for its seeds,

33
00:03:52,065 --> 00:03:55,432
ensure a distance of 7km. 

34
00:03:57,156 --> 00:04:00,200
It is also possible to isolate two varieties

35
00:04:00,443 --> 00:04:03,690
by protecting them
under different mosquito nets

36
00:04:03,920 --> 00:04:06,414
that you open on alternate days.

37
00:04:06,836 --> 00:04:08,967
For more information on this technique,

38
00:04:09,280 --> 00:04:15,534
refer to the module on Isolation techniques
in the « ABC of Seed production ». 

39
00:04:28,000 --> 00:04:29,301
Life cycle 

40
00:04:37,541 --> 00:04:41,483
Chard and beet plants grown for seed production

41
00:04:41,796 --> 00:04:45,534
are cultivated in the same way
as those for consumption.

42
00:04:47,927 --> 00:04:51,243
They will produce seeds only in the second year.

43
00:05:29,960 --> 00:05:34,349
In spite of the very large amount
of seeds produced by each plant,

44
00:05:34,698 --> 00:05:39,469
it is important to keep a dozen
plants to ensure genetic diversity. 

45
00:05:43,280 --> 00:05:48,989
Beet are generally harvested in the autumn
at the end of the first year’s cycle.

46
00:05:52,021 --> 00:05:55,992
When uprooting the crop you should
select those for seed production

47
00:05:56,225 --> 00:05:59,316
according to the specific
criteria of the variety,

48
00:05:59,660 --> 00:06:03,360
such as colour, shape, strength of the plant… 

49
00:06:06,334 --> 00:06:12,167
Clean the roots by brushing them, without water,
and cut the leaves above the collar.

50
00:06:18,120 --> 00:06:23,476
Store the roots in a sandbox to protect them
from the frost and light. 

51
00:06:24,763 --> 00:06:33,490
Ideal storing conditions are at a temperature
of 1°C and between 90 and 95 % humidity.

52
00:06:39,090 --> 00:06:43,287
During winter, make sure
that you remove any roots that rot. 

53
00:06:50,770 --> 00:06:56,312
As for chards, in regions with
severe winters and strong frosts,

54
00:06:56,625 --> 00:07:01,018
you will also have to uproot and store
the roots inside in autumn.

55
00:07:02,043 --> 00:07:06,967
You must cut all the leaves at the collar
except those at the centre. 

56
00:07:08,698 --> 00:07:12,167
Without wounding them,
store the roots in a cellar,

57
00:07:12,581 --> 00:07:15,272
in a box with slightly dampened sand. 

58
00:07:22,705 --> 00:07:24,472
Most of the time, however,

59
00:07:24,698 --> 00:07:29,941
you can leave chard in the garden over winter
if they have a well-developed root system.

60
00:07:32,130 --> 00:07:37,454
Mulch them with straw during
particularly cold spells to protect them. 

61
00:07:38,923 --> 00:07:42,901
In milder climates you can also
leave beetroots in the ground,

62
00:07:43,287 --> 00:07:47,112
provided that you sow them late
in August or September.

63
00:07:53,723 --> 00:07:57,636
In spring select the roots for seed production.

64
00:07:58,800 --> 00:08:01,716
You can uncover the collar to examine them

65
00:08:02,109 --> 00:08:07,592
and get rid of those which do not correspond
to the variety’s required characteristics.

66
00:08:08,910 --> 00:08:13,818
This is not, however,
the best method to select seed producing plants. 

67
00:08:17,803 --> 00:08:24,007
It is better to uproot,
select and immediately replant them. 

68
00:08:34,385 --> 00:08:39,020
When replanting chard and beet roots
that have been stored over the winter

69
00:08:39,621 --> 00:08:43,112
you should ensure that the collar
is at ground level.

70
00:08:48,749 --> 00:08:50,705
Water copiously. 

71
00:09:11,505 --> 00:09:16,014
The next steps of the cycle are
the same for chard and beetroots.

72
00:09:17,025 --> 00:09:20,872
The flower stalks will develop to up to 1m 50.

73
00:09:21,890 --> 00:09:24,072
They often need supporting with a stake. 

74
00:09:26,007 --> 00:09:29,127
The seed-forming process may be difficult

75
00:09:29,243 --> 00:09:33,505
or even impossible in regions
where there is little difference

76
00:09:33,600 --> 00:09:36,320
between the lengths of day and night.

77
00:09:37,740 --> 00:09:40,625
They need long summer days to fructify. 

78
00:09:46,487 --> 00:09:51,310
Cut the flower stalks, after the dew,
when the first seeds are mature.

79
00:09:55,621 --> 00:10:00,581
The seeds are in fact clusters
containing 2 to 6 individual seeds. 

80
00:10:11,760 --> 00:10:18,043
It is possible to cut the stalks all in one go,
but you may lose part of the first mature seeds. 

81
00:10:37,563 --> 00:10:41,818
Whatever the case, it is better
to continue drying in a shaded,

82
00:10:42,138 --> 00:10:44,530
dry and well-ventilated area. 

83
00:10:57,800 --> 00:11:01,229
Extracting - sorting - storing

84
00:11:05,090 --> 00:11:09,280
Extract the seeds by rubbing
the stems between your hands.

85
00:11:09,710 --> 00:11:11,774
It is better to wear gloves.

86
00:11:11,963 --> 00:11:16,807
It is also possible to walk on the stems,
or to beat them using a stick. 

87
00:11:19,990 --> 00:11:24,865
To sort them, first use a coarse
sieve that will retain the waste.

88
00:11:27,070 --> 00:11:30,654
Then use a thinner sieve
that will retain the seeds,

89
00:11:30,865 --> 00:11:32,610
but not the fine chaff. 

90
00:11:35,490 --> 00:11:40,596
Finally, winnow them by blowing
on them to get rid of the remaining chaff.

91
00:11:41,450 --> 00:11:44,254
Either do this yourself or use the wind. 

92
00:11:47,250 --> 00:11:50,661
Always put a label with the name
of the variety and species,

93
00:11:50,814 --> 00:11:53,716
as well as the year inside the sachet.

94
00:11:54,270 --> 00:11:57,207
Writing on the outside sometimes rubs off.

95
00:12:03,854 --> 00:12:09,236
Placing the seeds in the freezer
for a few days will kill certain parasites. 

96
00:12:11,941 --> 00:12:16,872
Chard and beet seeds have a germination
capacity of 6 years,

97
00:12:20,138 --> 00:12:22,036
sometimes even 10 years.

98
00:12:24,770 --> 00:12:28,560
One gram of seeds represents about 50 clusters. 
