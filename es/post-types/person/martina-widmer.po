# Copyright (C) 2021 Maxime Lecoq
# This file is distributed under the GNU General Public License v2 or later.
msgid ""
msgstr ""
"Project-Id-Version: Diyseeds 2.0\n"
"POT-Creation-Date: 2021-07-10T00:00:00+02:00\n"
"PO-Revision-Date: 2021-12-21 17:14+0000\n"
"Last-Translator: Luna Saenz <point@mailoo.org>\n"
"Language-Team: Spanish <http://translate.diyseeds.org/projects/"
"diyseeds-org-persons/martina-widmer/es/>\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 4.8\n"
"WPOT-Type: post-types/person\n"
"WPOT-Origin: martina-widmer\n"
"X-Domain: wpot\n"

#. This title will be displayed into the browser's window title bar.
msgctxt "browser-title"
msgid "Martina Widmer"
msgstr "Martina Widmer"

#. The slug is the last part of the URL (www.example.org/slug-is-here). Only alphanumerical characters are allowed, use dash to replace space. The slug should be short and nice and it must reflect the content it refers to. It is usually made from the title. If you don't provide it, a slug will be auto-generated.
msgctxt "slug"
msgid "martina-widmer"
msgstr "martina-widmer"

msgctxt "name"
msgid "Martina Widmer"
msgstr "Martina Widmer"

msgctxt "given-name"
msgid "Martina"
msgstr "Martina"

msgctxt "family-name"
msgid "Widmer"
msgstr "Widmer"

msgctxt "description"
msgid "Martina Widmer lives at the Longo Maï Cooperative in the south of France. She has had experience in market gardening and seed production since the early 2000s in collaboration with Kokopelli. She has organized seed swaps and participated in the variety conservation network of seed producers contributing to Kokopelli's “Semences sans frontières” project. She is involved in the European Civic Forum's international public awareness campaigns on the topic of free access to open-pollinated seeds. She co-directed the film “From seed to seed”."
msgstr ""
"Martina Widmer vive en una cooperativa Longo maï, en el sur de Francia. "
"Tiene experiencia en horticultura y producción de semillas desde principios "
"de la década de 2000 en colaboración con Kokopelli. Ha organizado "
"intercambios de semillas y participado en la red de conservación de "
"variedades de lxs productorxs de semillas que contribuyen al proyecto "
"\"Semences sans frontières\" de Kokopelli. Participa en las campañas "
"internacionales de sensibilización del Foro Cívico Europeo sobre el tema del "
"libre acceso a las semillas de polinización abierta. Ha codirigido la "
"película \"¡Siembra!\"."
