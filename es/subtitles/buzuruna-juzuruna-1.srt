﻿1
00:00:00,000 --> 00:00:05,760
Interview with Serge Harfouche from Buzuruna Juzuruna
May 2021, Beqaa Valley, Lebanon
www.DIYseeds.org
educational films on seed production

2
00:00:05,920 --> 00:00:08,760
-¿Qué significa "Buzuruna Juzuruna"?

3
00:00:09,520 --> 00:00:12,100
Quiere decir "nuestras semillas, nuestras raíces".

4
00:00:12,140 --> 00:00:16,150
-¿Y cómo ese nombre representa vuestra asociación?

5
00:00:17,870 --> 00:00:25,170
Somos una granja escuela que produce semillas campesinas

6
00:00:26,090 --> 00:00:30,570
y también organizamos formaciones.

7
00:00:30,660 --> 00:00:35,520
Esas son nuestras dos actividades principales.

8
00:00:36,220 --> 00:00:39,350
Así que producimos semillas

9
00:00:39,530 --> 00:00:42,460
que, a su vez, se vuelven raíces.

10
00:00:42,460 --> 00:00:47,020
La idea es trabajar por la autonomía campesina

11
00:00:47,130 --> 00:00:49,380
y la soberanía alimentaria.

12
00:00:49,380 --> 00:00:53,120
Todo eso parece sonar bien junto.

13
00:00:54,080 --> 00:01:04,730
-¿Por qué la autonomía alimentaria es tan importante en Líbano en este momento?

14
00:01:05,600 --> 00:01:07,840
Es una emergencia.

15
00:01:07,860 --> 00:01:13,600
Estamos en medio de una crisis económica sin precedentes

16
00:01:13,600 --> 00:01:15,600
que es extremadamente violenta.

17
00:01:15,710 --> 00:01:24,150
Nuestra moneda ha perdido 10 veces su valor.

18
00:01:24,260 --> 00:01:30,020
En un momento dado, pasamos de 1 500 libras libanesas por dólar a 15 000

19
00:01:30,040 --> 00:01:32,880
y ahora se ha más o menos estabilizado a 12 000.

20
00:01:32,950 --> 00:01:36,600
Es muy aleatorio, no sabemos cuándo va a explotar.

21
00:01:36,680 --> 00:01:37,770
-¿Cuanto tiempo hace que ocurrió esto?

22
00:01:37,860 --> 00:01:43,400
El colapso empezó alrededor de 2018

23
00:01:43,510 --> 00:01:54,640
pero vimos la inflación en el mercado desde mediados de 2019, principios de 2020, creo.

24
00:01:55,950 --> 00:02:01,260
-Cuando esto ocurre, ¿qué impacto tiene en la vida de las personas?

25
00:02:02,600 --> 00:02:07,020
Es aterrador, porque los salarios no han aumentado en libras libanesas.

26
00:02:07,060 --> 00:02:12,200
El salario mínimo era de unos 400 euros al mes.

27
00:02:12,200 --> 00:02:17,350
Ahora es de solamente 40 €,

28
00:02:17,880 --> 00:02:21,620
pero todo lo que se consume es más caro

29
00:02:21,680 --> 00:02:24,750
porque también está indexado al dólar, se está volviendo terrible.

30
00:02:24,840 --> 00:02:27,910
Imagina que estás comprando un cartón de leche.

31
00:02:27,930 --> 00:02:31,530
Un cartón de leche que costaba 3 000 libras libanesas

32
00:02:31,530 --> 00:02:36,480
ahora cuesta 27 o 25 000,

33
00:02:36,600 --> 00:02:40,200
mientras que aún te pagan, por ejemplo, 100 000.

34
00:02:40,200 --> 00:02:44,160
Así que en lugar de poder comprar varios cartones al mes

35
00:02:44,260 --> 00:02:47,380
apenas puedes comprar una.

36
00:02:47,440 --> 00:02:59,180
Como nuestra consumo depende en un 90-95% de las importaciones,

37
00:02:59,260 --> 00:03:01,580
que a su vez depende de una moneda extranjera

38
00:03:01,620 --> 00:03:03,260
-en nuestro caso el dólar-,

39
00:03:03,310 --> 00:03:05,520
todo se vuelve mucho más caro:

40
00:03:05,570 --> 00:03:08,210
el pan se vuelve más caro,

41
00:03:08,360 --> 00:03:11,450
el combustible se vuelve más caro

42
00:03:11,560 --> 00:03:14,660
y los salarios siguen siendo los mismos.

43
00:03:14,690 --> 00:03:17,720
Esto significa que el nivel de vida,

44
00:03:17,750 --> 00:03:20,820
la calidad de vida, se lleva un gran golpe

45
00:03:20,910 --> 00:03:23,090
y el acceso a la alimentación

46
00:03:23,110 --> 00:03:26,530
y la seguridad alimentaria se llevan un gran golpe.

47
00:03:26,720 --> 00:03:30,370
Es por eso que es tan urgente producir nuestras propias semillas

48
00:03:30,430 --> 00:03:33,200
en lugar de importarlas o comprarlas en otros lugares

49
00:03:33,220 --> 00:03:35,490
especialmente si son híbridas y estériles

50
00:03:35,520 --> 00:03:38,230
y necesitas volver a comprarlas el año siguiente.

51
00:03:40,010 --> 00:03:43,980
También tenemos que producir nuestros propios fertilizantes

52
00:03:44,020 --> 00:03:46,480
de forma ecológica y limpia,

53
00:03:46,530 --> 00:03:49,810
hacer abono...

54
00:03:49,890 --> 00:03:51,540
El lote, como dicen.

55
00:03:51,570 --> 00:03:55,290
Es lo que intentamos hacer con nuestras formaciones

56
00:03:55,330 --> 00:03:57,330
y gracias a la actividad en la granja.

57
00:03:58,730 --> 00:04:01,460
-Cuando estuviste en Francia nos enseñaste

58
00:04:01,470 --> 00:04:04,110
fotos de cómo,

59
00:04:04,160 --> 00:04:07,630
después de lo que llamaste la revolución,

60
00:04:07,640 --> 00:04:10,560
la vida cambió para un montón de gente,

61
00:04:10,660 --> 00:04:12,610
gente muy diferente:

62
00:04:12,630 --> 00:04:18,410
estudiantes, personas que trabajan en la administración,

63
00:04:18,430 --> 00:04:20,730
gente de todo tipo que viene

64
00:04:20,750 --> 00:04:23,360
y de repente se interesan en todo esto.

65
00:04:23,630 --> 00:04:24,810
Exactamente.

66
00:04:24,930 --> 00:04:27,530
Es como si pudiéramos sentirlo venir,

67
00:04:28,180 --> 00:04:33,380
que en cierto modo, estamos ya viviendo una crisis económica sin precedentes.

68
00:04:33,490 --> 00:04:38,210
Por eso empezó la revolución.

69
00:04:38,270 --> 00:04:43,040
Así que la gente era vagamente consciente

70
00:04:43,040 --> 00:04:47,650
que los años venideros no iban a ser fáciles

71
00:04:47,680 --> 00:04:50,490
y que tendrían que arreglárselas de alguna manera.

72
00:04:50,540 --> 00:04:54,340
Cada vez somos más conscientes

73
00:04:54,400 --> 00:05:00,000
que una economía improductiva no es sostenible,

74
00:05:00,080 --> 00:05:03,130
y que la nuestra no es una economía productiva.

75
00:05:03,460 --> 00:05:05,920
Durante mucho tiempo, 30 o 40 años,

76
00:05:05,970 --> 00:05:10,250
fue una economía realmente basada en la deuda.

77
00:05:11,090 --> 00:05:15,320
-Así que, ¿mucha gente comenzó a producir?

78
00:05:15,460 --> 00:05:17,980
...

79
00:05:18,040 --> 00:05:19,290
Está grabando.

80
00:05:20,430 --> 00:05:23,260
Perdona, hablaba con mi colega.

81
00:05:23,280 --> 00:05:26,070
-¿Hay mucha gente en la granja?

82
00:05:26,850 --> 00:05:31,620
Somos 16 personas adultas, 27 niñxs

83
00:05:31,650 --> 00:05:34,160
y en este momento tenemos muchas visitas de amigxs,

84
00:05:34,210 --> 00:05:36,000
personas voluntarias, becarixs...

85
00:05:36,030 --> 00:05:38,930
En total somos unas 50 personas,

86
00:05:39,000 --> 00:05:40,370
quizás algo más.

87
00:05:40,410 --> 00:05:43,350
-¿Y qué tamaño tiene el lugar?

88
00:05:44,940 --> 00:05:47,770
En la granja con la asociación

89
00:05:47,920 --> 00:05:54,140
tenemos 2 hectáreas de terreno:

90
00:05:54,580 --> 00:05:57,940
18 000 m² de cultivos

91
00:05:57,950 --> 00:06:00,500
y 2000 m² para los locales,

92
00:06:00,670 --> 00:06:02,980
los viveros

93
00:06:03,070 --> 00:06:06,050
y el corral para las gallinas,

94
00:06:06,060 --> 00:06:07,640
las cabras y las ovejas.

95
00:06:07,710 --> 00:06:09,820
Y este año conseguimos

96
00:06:09,830 --> 00:06:13,490
alquilar 70 000 m² más,

97
00:06:13,610 --> 00:06:16,600
unas 7 hectáreas más,

98
00:06:16,700 --> 00:06:19,700
para multiplicar y ampliar nuestra colección,

99
00:06:19,720 --> 00:06:21,350
especialmente de nuestros cereales.

100
00:06:22,280 --> 00:06:24,770
-Pero, ¿no vivís en la granja?

101
00:06:25,610 --> 00:06:27,510
No del todo.

102
00:06:27,520 --> 00:06:32,860
Uno de nuestros colegas y co-fundadores vive en la granja.

103
00:06:34,400 --> 00:06:36,320
Tenemos un edificio multiusos

104
00:06:36,340 --> 00:06:39,780
que es una cocina de transformación,

105
00:06:39,820 --> 00:06:43,090
cocina para nosotrxs, área de almacenamiento, escuela

106
00:06:43,140 --> 00:06:44,350
y apartamento,

107
00:06:44,360 --> 00:06:47,040
un edificio bastante grande.

108
00:06:47,060 --> 00:06:50,120
Es Walid el que vive allí con su familia.

109
00:06:50,260 --> 00:06:52,440
Nuestrxs otrxs colegas y amigxs

110
00:06:52,460 --> 00:06:54,260
viven justo enfrente,

111
00:06:54,310 --> 00:06:56,530
y nosotrxs estamos en la misma calle, un poco más arriba,

112
00:06:56,550 --> 00:06:58,450
a cuatro minutos a pie.

113
00:06:59,590 --> 00:07:02,290
-¿Y también producís un poco de

114
00:07:02,300 --> 00:07:04,400
lo que consumís vosotrxs?

115
00:07:04,740 --> 00:07:06,950
¡Sí!

116
00:07:07,160 --> 00:07:10,070
Compramos muy pocas cosas.

117
00:07:10,110 --> 00:07:13,770
¡Todo lo que podemos producir, no lo compramos, lo tenemos!

118
00:07:14,900 --> 00:07:17,980
Hay cosas, como el arroz,

119
00:07:18,010 --> 00:07:20,880
que son difíciles de producir para nosotrxs,

120
00:07:20,910 --> 00:07:22,690
ese tipo de cosas.

121
00:07:22,920 --> 00:07:24,120
Pero a parte de esas cosas…

122
00:07:24,130 --> 00:07:27,230
El año pasado incluso conseguimos algo de miel,

123
00:07:27,250 --> 00:07:30,520
pero un depredador atacó a nuestras abejas,

124
00:07:30,560 --> 00:07:32,600
así que nos quedamos con sólo 3 colmenas,

125
00:07:32,620 --> 00:07:34,570
de 11.

126
00:07:36,830 --> 00:07:39,980
La idea es también de mostrar que es posible.

127
00:07:41,590 --> 00:07:46,920
-¿Qué hay de lxs socixs, de los vínculos, de la red

128
00:07:46,940 --> 00:07:50,600
que tenéis en el Líbano y en Siria,

129
00:07:50,610 --> 00:07:54,570
cómo se está desarrollando, cómo surgió?

130
00:07:54,590 --> 00:07:56,450
¡Cuéntanos!

131
00:07:57,100 --> 00:08:00,160
Básicamente, la idea fue de, en primer lugar, producir semillas

132
00:08:00,180 --> 00:08:03,170
y luego enseñar a quienes se interesan

133
00:08:03,190 --> 00:08:04,560
como conservar sus propias semillas,

134
00:08:04,590 --> 00:08:08,450
para no tener que continuar produciéndolas nosotrxs cada año.

135
00:08:08,450 --> 00:08:11,030
Se trata realmente de aprender la autonomía

136
00:08:11,040 --> 00:08:12,560
y la independencia.

137
00:08:12,580 --> 00:08:16,380
A partir de entonces, pequeñxs guardianxs de semillas

138
00:08:16,400 --> 00:08:17,910
se extendieron por todas partes.

139
00:08:17,920 --> 00:08:19,710
Algunxs volvieron a Siria

140
00:08:19,710 --> 00:08:22,260
para producir allí sus propias semillas.

141
00:08:22,280 --> 00:08:25,120
Hay muchas situaciones diferentes.

142
00:08:25,150 --> 00:08:29,610
Lxs amigxs a lxs que hemos estado siguiendo desde el principio

143
00:08:29,630 --> 00:08:32,020
ahora son completamente autónomxs.

144
00:08:32,040 --> 00:08:34,970
Les va muy bien.

145
00:08:34,980 --> 00:08:36,910
Por supuesto que hacemos un seguimiento,

146
00:08:36,920 --> 00:08:40,350
si hay necesidad de intercambiar información o experiencia:

147
00:08:40,360 --> 00:08:42,840
"intentamos esto", "probaron aquello".

148
00:08:42,920 --> 00:08:46,690
La idea es tener el mayor número posible

149
00:08:46,690 --> 00:08:49,360
de guardianxs y bancos de semillas

150
00:08:49,390 --> 00:08:51,640
por todo el Líbano

151
00:08:51,690 --> 00:08:55,600
y quizás un día incluso en Siria, quién sabe.

152
00:08:57,460 --> 00:09:02,240
-¿La frontera sigue cerrada?

153
00:09:03,970 --> 00:09:06,550
Sí, está cerrada

154
00:09:06,570 --> 00:09:09,540
y es bastante complicado pasar

155
00:09:09,610 --> 00:09:12,040
para gente de ciertas nacionalidades.

156
00:09:12,050 --> 00:09:16,250
Si no formas parte de una misión diplomática o del ejército

157
00:09:16,400 --> 00:09:17,990
aún es difícil -

158
00:09:18,660 --> 00:09:20,860
es mejor no intentarlo.

159
00:09:21,700 --> 00:09:23,550
Mejor quédate donde estás.

160
00:09:23,580 --> 00:09:25,730
-Acerca de las formaciones...

161
00:09:25,920 --> 00:09:30,670
Has participado en la traducción de DIYseeds -

162
00:09:34,330 --> 00:09:37,440
has traducido la página web?

163
00:09:38,750 --> 00:09:40,100
... la página web en Árabe,

164
00:09:40,130 --> 00:09:44,680
y he revisto los videos que otro equipo hizo,

165
00:09:44,820 --> 00:09:47,750
un trabajo enorme, debo decir.

166
00:09:47,760 --> 00:09:50,090
Es una locura lo que han hecho, es súper bonito.

167
00:09:50,170 --> 00:09:52,080
Y extremadamente práctico.

168
00:09:52,120 --> 00:09:54,720
Cuando impartes una formación

169
00:09:54,780 --> 00:09:57,840
cuando intentas explicar cómo extraer las semillas

170
00:09:57,870 --> 00:10:01,660
o cómo mantener las distancias de seguridad

171
00:10:01,670 --> 00:10:03,360
para evitar la polinización cruzada,

172
00:10:03,380 --> 00:10:05,360
es mucho mejor con las imágenes,

173
00:10:05,360 --> 00:10:08,140
los dibujos, el vídeo y las explicaciones.

174
00:10:08,140 --> 00:10:11,550
No solamente mostramos los vídeos durante las formaciones,

175
00:10:11,560 --> 00:10:13,470
sino que además la gente

176
00:10:13,480 --> 00:10:17,000
puede acceder a ellos directamente en sus teléfonos

177
00:10:17,020 --> 00:10:19,660
-cuando tienen internet, pero eh, es accesible-,

178
00:10:19,680 --> 00:10:23,800
lo que les permite apañárselas muy bien solxs.

179
00:10:23,910 --> 00:10:25,680
Es genial, es perfecto.

180
00:10:27,600 --> 00:10:30,770
-Y para la gente de la región, es fácil

181
00:10:31,240 --> 00:10:35,560
acceder a la información, usarla…

182
00:10:35,640 --> 00:10:36,480
¡Exactamente!

183
00:10:36,540 --> 00:10:37,730
Les facilita las cosas,

184
00:10:37,760 --> 00:10:40,500
porque es algo bastante ligero, sencillo.

185
00:10:40,520 --> 00:10:44,910
No se trata de un libraco que te tienes que llevar a todas partes.

186
00:10:44,960 --> 00:10:47,940
Hay muchxs granjerxs que no saben leer,

187
00:10:48,000 --> 00:10:50,220
pero que entienden cuando lo oyen.

188
00:10:50,260 --> 00:10:52,970
También están los dibujos y el propio vídeo,

189
00:10:53,040 --> 00:10:55,570
que ilustran las palabras mucho mejor,

190
00:10:55,580 --> 00:10:58,770
es mucho más accesible y sencillo,

191
00:10:58,800 --> 00:11:01,830
como tutorial es más fácil de seguir

192
00:11:01,870 --> 00:11:05,270
que ponerse a leer su libraco y descifrar lo que dice.

193
00:11:06,000 --> 00:11:11,350
-Como las semillas que producís no son híbridas,

194
00:11:11,380 --> 00:11:14,580
son semillas naturales, reproducibles,

195
00:11:14,660 --> 00:11:18,030
todxs lxs que tienen esas semillas

196
00:11:18,160 --> 00:11:21,380
y acceso a los vídeos pueden recuperar autonomía rápidamente.

197
00:11:21,400 --> 00:11:26,560
¿Sabes decirnos cómo y con qué rapidez?

198
00:11:26,580 --> 00:11:27,630
¿Con qué rapidez?

199
00:11:27,650 --> 00:11:30,530
Depende del acceso a la tierra,

200
00:11:30,560 --> 00:11:32,160
depende del acceso al agua,

201
00:11:32,180 --> 00:11:34,310
depende de la seguridad de la zona.

202
00:11:34,320 --> 00:11:37,320
Tenemos amigxs que han tenido que mudarse varias veces

203
00:11:37,390 --> 00:11:39,260
de su zona, en Siria.

204
00:11:39,300 --> 00:11:41,170
Cada vez que se instalaban

205
00:11:41,180 --> 00:11:42,830
había bombardeos,

206
00:11:42,840 --> 00:11:44,810
así que han tenido que mudarse una y otra vez.

207
00:11:44,960 --> 00:11:47,240
El contexto en sí mismo es complicado.

208
00:11:47,320 --> 00:11:51,670
Pero cuando tienes las herramientas básicas,

209
00:11:51,710 --> 00:11:54,660
puedes arreglártelas donde sea que acabes.

210
00:11:54,690 --> 00:11:57,110
Tienes tus semillas, sabes cómo cultivarlas,

211
00:11:57,130 --> 00:12:00,260
sabes cómo reproducirlas, cómo extraerlas,

212
00:12:00,290 --> 00:12:01,990
guardarlas, conservarlas, etc.

213
00:12:02,020 --> 00:12:04,980
¡Eres una mujer o un hombre libre!

214
00:12:07,140 --> 00:12:08,050
Eso es todo.

215
00:12:08,190 --> 00:12:12,120
-¿Así que ves gente que, en una temporada,

216
00:12:12,170 --> 00:12:15,180
han aprendido a producir sus semillas -

217
00:12:15,580 --> 00:12:19,470
y que ahora tienen sus propias semillas?

218
00:12:19,520 --> 00:12:20,510
Exactamente.

219
00:12:20,530 --> 00:12:23,240
La mayoría de las personas con las que trabajamos

220
00:12:23,290 --> 00:12:25,350
se criaron con la agricultura,

221
00:12:25,370 --> 00:12:26,840
tienen experiencia en la agricultura,

222
00:12:26,850 --> 00:12:29,000
o han trabajado en granjas,

223
00:12:29,020 --> 00:12:30,440
a menudo en la agricultura convencional.

224
00:12:30,460 --> 00:12:34,350
Es más fácil para ellxs hacer la transición.

225
00:12:36,550 --> 00:12:38,500
En el pasado, han tenido que

226
00:12:38,520 --> 00:12:39,970
comprar las semillas

227
00:12:39,970 --> 00:12:42,450
y desde el segundo en el que aprenden

228
00:12:42,470 --> 00:12:45,010
que su destino está en sus manos,

229
00:12:45,260 --> 00:12:47,100
porque solamente necesitan

230
00:12:47,120 --> 00:12:50,730
las herramientas adecuadas,

231
00:12:50,880 --> 00:12:53,890
después de una o dos temporadas, tienen sus propias semillas,

232
00:12:53,930 --> 00:12:55,040
e intercambiamos.

233
00:12:55,080 --> 00:12:56,800
Trabajamos juntxs a largo plazo,

234
00:12:56,820 --> 00:13:00,570
por ejemplo, si alguien cultiva una variedad,

235
00:13:00,580 --> 00:13:03,410
otrx cultiva otra, nosotrxs producimos la tercera…

236
00:13:03,423 --> 00:13:08,680
para disponer de tanta diversidad como nos es posible, y ya está,

237
00:13:08,720 --> 00:13:10,680
está empezando a funcionar.

238
00:13:11,760 --> 00:13:16,280
- Y tú, ¿cómo te involucraste en todo esto?

239
00:13:18,426 --> 00:13:20,280
Realmente, por casualidad.

240
00:13:20,330 --> 00:13:26,600
Yo vivía en Beirut y mi compañero de piso estaba en la universidad con Ferdi

241
00:13:26,693 --> 00:13:30,613
que es uno de lxs co-fundadorxs de la asociación y del proyecto.

242
00:13:32,320 --> 00:13:35,253
Así que cuando Ferdi y Lara volvieron al Líbano

243
00:13:35,293 --> 00:13:38,253
para ver cómo iniciar el proyecto de las semillas,

244
00:13:38,290 --> 00:13:41,270
cómo producirlas, dónde hacer todo esto,

245
00:13:41,308 --> 00:13:43,228
lo que había que hacer,

246
00:13:43,242 --> 00:13:44,927
se quedaron con nosotrxs

247
00:13:44,936 --> 00:13:46,592
durante aproximadamente un mes,

248
00:13:46,602 --> 00:13:48,545
nos llevamos bien de inmediato y eso fue todo.

249
00:13:49,727 --> 00:13:51,303
Como dije antes:

250
00:13:51,336 --> 00:13:54,174
¡Me dejé embarcar a bordo en plena consciencia!

251
00:13:54,988 --> 00:13:56,898
-¿Y cambiaste de vida?

252
00:13:57,068 --> 00:13:59,101
¿Qué hacías antes?

253
00:14:00,112 --> 00:14:03,303
Justo antes estaba trabajando en la comunicación

254
00:14:03,336 --> 00:14:04,823
para una universidad.

255
00:14:04,851 --> 00:14:06,960
Pero en realidad soy librero,

256
00:14:09,520 --> 00:14:12,640
Estudié psicología, literatura,

257
00:14:12,672 --> 00:14:15,868
¡nada que ver con las semillas!

258
00:14:19,745 --> 00:14:21,520
Pero ayuda a traducir,

259
00:14:21,543 --> 00:14:22,922
¡así que es perfecto!

260
00:14:23,275 --> 00:14:24,992
-¿Y quieres continuar?

261
00:14:25,920 --> 00:14:27,637
¡Sí!

262
00:14:27,661 --> 00:14:29,863
Como decíamos antes:

263
00:14:29,890 --> 00:14:33,002
estamos en medio de una emergencia,

264
00:14:34,164 --> 00:14:39,185
Creo que este trabajo es realmente básico,

265
00:14:40,014 --> 00:14:41,477
es necesario.

266
00:14:41,515 --> 00:14:47,298
Y también es gratificante a muchos niveles:

267
00:14:47,310 --> 00:14:50,555
a nivel social, a nivel político también.

268
00:14:50,597 --> 00:14:53,050
Lo que hacemos es extremadamente importante

269
00:14:53,072 --> 00:14:54,823
y hermoso al mismo tiempo.

270
00:14:55,247 --> 00:14:58,211
¡No creo que vaya a dejarlo pronto!

271
00:14:58,418 --> 00:15:01,797
-¡Genial! Gracias por la entrevista.

272
00:15:01,960 --> 00:15:03,722
¡De nada, de nada!

273
00:15:03,755 --> 00:15:06,290
Ha sido un placer volver a hablar contigo.

274
00:15:07,620 --> 00:15:08,936
-Espero que nos volveremos a ver.

275
00:15:08,960 --> 00:15:11,134
¡Te esperamos en la granja!

276
00:15:11,181 --> 00:15:15,811
-Me encantaría. No puedo prometer nada, pero…

277
00:15:17,463 --> 00:15:20,560
Sólo tienes que avisarnos de que vas a venir, cuando quieras,

278
00:15:20,583 --> 00:15:22,672
y "ahla wa sahla" (bienvenidx!)

279
00:15:25,251 --> 00:15:26,409
-¡Muchísimas gracias!

280
00:15:26,432 --> 00:15:27,091
¡Genial!

281
00:15:28,120 --> 00:15:35,720
Many thanks to Serge Harfouche and everyone at Buzuruna Juzuruna
more information: @buzurunajuzuruna (Instagram)
photos: Charlotte Joubert, Buzuruna Juzuruna, Rabih Yassine
interview & editing: Erik D'haese

282
00:15:35,720 --> 00:15:40,320
www.DIYseeds.org
educational films on seed production
