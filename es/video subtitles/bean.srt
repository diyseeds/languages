﻿1
00:00:09,640 --> 00:00:12,705
Beans are annual plants of the Fabaceae family.

2
00:00:19,320 --> 00:00:21,150
There are several species of beans,

3
00:00:21,270 --> 00:00:27,130
the most common of which are Phaseolus vulgaris
and Phaseolus coccineus.

4
00:00:41,570 --> 00:00:45,180
Phaseolus vulgaris includes different kinds:

5
00:00:45,690 --> 00:00:52,740
such as dwarf beans,
semi-climbing beans and climbing beans.

6
00:00:55,315 --> 00:01:00,180
These two types of crops
include beans grown for their pods

7
00:01:00,480 --> 00:01:06,100
such as green beans or for their seeds,
eaten fresh or dried. 

8
00:01:13,455 --> 00:01:18,100
There are also other species
such as Phaseolus coccineus

9
00:01:18,655 --> 00:01:22,180
or runner beans which are all climbing beans.

10
00:01:23,430 --> 00:01:26,220
They have very beautiful red or white flowers

11
00:01:26,315 --> 00:01:31,500
and are suited temperatures
below 25°C during fructification. 

12
00:01:38,980 --> 00:01:44,025
You can distinguish between
both these two species thanks to the pods.

13
00:01:45,095 --> 00:01:48,315
Phaseolus coccineus has thicker pods.

14
00:01:48,750 --> 00:01:52,995
Those of Phaseolus vulgaris
are thinner and smoother. 

15
00:02:04,125 --> 00:02:05,120
Pollination 

16
00:02:21,325 --> 00:02:27,825
The flowers of the Phaseolus vulgaris species
are hermaphrodite and self-fertilising,

17
00:02:28,295 --> 00:02:32,790
meaning they have male and female
organs in the same flower.

18
00:02:33,125 --> 00:02:34,515
They are autogamous. 

19
00:02:36,060 --> 00:02:38,750
Insects may nonetheless
cause cross-pollination

20
00:02:38,820 --> 00:02:40,530
between different varieties.

21
00:02:41,475 --> 00:02:45,085
This risk is more or less great
depending on the variety. 

22
00:02:48,045 --> 00:02:49,390
To reduce this risk,

23
00:02:49,820 --> 00:02:53,575
you should leave a distance
of at least 5 or 10 meters

24
00:02:53,875 --> 00:02:56,420
between two varieties of dwarf beans.

25
00:02:59,075 --> 00:03:03,695
Leave at least 50 meters between
two varieties of climbing beans. 

26
00:03:05,315 --> 00:03:11,015
And finally leave at least 10 meters
between dwarf and climbing beans. 

27
00:03:17,745 --> 00:03:22,305
To preseve maximum varietal purity
despite a lack of space,

28
00:03:22,590 --> 00:03:27,285
cover each Phaseolus vulgaris variety
with a mosquiito net.

29
00:03:33,635 --> 00:03:39,140
Be careful to place the net before flowering
starts to avoid any cross-pollination. 

30
00:03:44,900 --> 00:03:48,740
The flowers of Phaseolus coccineus
are hermaphrodite,

31
00:03:48,990 --> 00:03:52,680
which means that male and female organs
are within the same flower.

32
00:03:59,865 --> 00:04:07,000
To pollinate, beans with red flowers require
insects such as bees and bumblebees.

33
00:04:09,495 --> 00:04:14,225
Beans with cream-coloured flowers,
on the other hand, can self-pollinate. 

34
00:04:16,010 --> 00:04:20,995
To avoid cross-pollination between
two varieties of Phaseolus coccineus

35
00:04:21,720 --> 00:04:24,390
keep a distance of 500 meters.

36
00:04:25,640 --> 00:04:32,855
This distance can be reduced to 150 meters
if there is a natural barrier such as a hedge. 

37
00:04:33,610 --> 00:04:40,415
Leave a distance of 300m between a coccineus
climbing bean and a vulgaris climbing bean. 

38
00:04:41,335 --> 00:04:47,165
And a 50m distance between a coccineus
climbing bean and a vulgaris dwarf bean. 

39
00:04:53,810 --> 00:04:54,840
Life cycle 

40
00:05:11,980 --> 00:05:16,255
The technique for growing beans for seeds
is the same as for growing them for food.

41
00:05:17,150 --> 00:05:19,440
They require warm soil for sowing. 

42
00:06:01,920 --> 00:06:06,385
For the green bean, it is better
to divide the crop into two sections:

43
00:06:06,920 --> 00:06:09,635
one for food and one for seed production.

44
00:06:15,030 --> 00:06:18,230
Harvesting for food is done
throughout the season,

45
00:06:21,375 --> 00:06:26,235
whilst harvesting the seeds must be done
only when all pods are mature.

46
00:06:29,435 --> 00:06:33,210
If you only keep the pods that mature
at the end of the season,

47
00:06:33,910 --> 00:06:36,685
you will end up with a late-season variety. 

48
00:06:43,455 --> 00:06:46,890
To harvest the seeds,
wait until the pods have dried.

49
00:06:49,380 --> 00:06:53,060
For certain varieties,
including climbing beans,

50
00:06:53,640 --> 00:06:56,320
harvesting time can be spread out. 

51
00:06:58,190 --> 00:07:03,235
For certain species of dwarf beans,
all pods dry at the same time,

52
00:07:04,340 --> 00:07:09,100
meaning that the seed harvest can be done
in one go by cutting all of the plants. 

53
00:07:14,765 --> 00:07:19,800
If the weather is damp and the seed-bearing
plants are not completely dry,

54
00:07:20,390 --> 00:07:23,625
you can put them to dry in a well ventilated shed. 

55
00:07:29,245 --> 00:07:33,340
It is important to protect them
from insects with a net. 

56
00:07:36,140 --> 00:07:40,520
Leave the seeds to dry for two
or three weeks after harvest.

57
00:07:45,015 --> 00:07:48,075
To check they are ready, bite one slightly:

58
00:07:49,120 --> 00:07:52,475
if this leaves no mark, drying is complete. 

59
00:08:00,070 --> 00:08:03,415
Extracting - sorting - storing

60
00:08:14,930 --> 00:08:18,620
For small quantities,
shelling can be done by hand. 

61
00:08:21,480 --> 00:08:25,860
For larger quantities,
you can beat the pods with a stick.

62
00:08:26,915 --> 00:08:28,515
You can also walk on them. 

63
00:08:43,585 --> 00:08:47,130
Once they have been beaten,
you can sieve the beans. 

64
00:08:47,485 --> 00:08:51,810
The sieve will retain the beans and larger waste
which can easily be removed.

65
00:08:57,070 --> 00:09:04,550
To get rid of the remaining smaller waste,
you will need to winnow or ventilate what is left. 

66
00:09:06,140 --> 00:09:08,895
You can ventilate either
by blowing on it yourself

67
00:09:09,305 --> 00:09:13,125
or by using a ventilator or small compressor.

68
00:09:15,925 --> 00:09:21,520
Remove those that are of a different type,
they are a sign of cross-pollination.

69
00:09:24,410 --> 00:09:29,695
Remove also damaged or badly formed beans
and those infested by weavils. 

70
00:09:30,320 --> 00:09:36,280
The bean weavil (Acanthocelides obtectus)
is a small insect

71
00:09:36,650 --> 00:09:40,795
that lays its eggs inside the pod on the plant.

72
00:09:41,475 --> 00:09:46,330
An easy way to get rid of them is to leave
the seeds in the freezer for a few days. 

73
00:09:57,980 --> 00:10:01,960
Always write the name of the species
and the variety,

74
00:10:02,140 --> 00:10:07,845
as well as the year of harvest on a label
and put it inside the bag with the seeds.

75
00:10:08,720 --> 00:10:11,255
Writing on the outside may rub off. 

76
00:10:13,325 --> 00:10:17,615
Putting the bag in the freezer
for a few days kills the larvae of parasites. 

77
00:10:23,235 --> 00:10:26,955
Bean seeds germinate very well for three years.

78
00:10:27,225 --> 00:10:31,665
To prolong their germination capacity,
keep them in the freezer. 
