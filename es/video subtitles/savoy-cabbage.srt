﻿1
00:00:09,480 --> 00:00:12,938
The Savoy cabbage is a member
of the Brassicaceae family,

2
00:00:13,510 --> 00:00:18,930
the Brassica oleraceae species
and the Sabauda subspecies.

3
00:00:20,560 --> 00:00:25,025
The brassica oleracea species
also includes kohlrabi,

4
00:00:25,163 --> 00:00:31,396
broccoli, cabbage, Brussels sprouts,
kale and cauliflower. 

5
00:00:37,810 --> 00:00:41,949
Savoy cabbages are characterized
by curly leaves

6
00:00:44,581 --> 00:00:48,196
that form a head slightly
less compact than cabbages.

7
00:00:50,220 --> 00:00:53,934
There are Savoy cabbages grown
in spring and summer

8
00:00:54,138 --> 00:00:56,792
with leaves and a head that are less tight,

9
00:00:58,698 --> 00:01:01,912
Savoy cabbages
with a very large head for storage

10
00:01:05,745 --> 00:01:10,240
and winter Savoy cabbages
with a head that is green and light. 

11
00:01:15,592 --> 00:01:20,340
Pollination of all of the cabbages
of the Oleracea species: 

12
00:01:36,120 --> 00:01:40,240
The flowers of the Brassica oleracea
species are hermaphrodite

13
00:01:43,090 --> 00:01:47,265
which means that they have both male
and female organs.

14
00:01:50,996 --> 00:01:53,490
Most of them are self-sterile:

15
00:01:54,101 --> 00:01:59,156
the pollen from the flowers of one plant
can only fertilize another plant.

16
00:02:02,967 --> 00:02:05,178
The plants are therefore allogamous.

17
00:02:06,123 --> 00:02:10,945
In order to ensure good pollination
it is better to grow several plants. 

18
00:02:14,501 --> 00:02:17,563
Insects are the vectors of pollination.

19
00:02:20,952 --> 00:02:25,403
These characteristics ensure great
natural genetic diversity.

20
00:02:32,356 --> 00:02:36,661
All of the cabbage sub-species
of the Brassica oleracea species

21
00:02:36,814 --> 00:02:38,690
can cross with each other.

22
00:02:40,276 --> 00:02:44,923
You should therefore not grow different
kinds of cabbage for seeds close to each other. 

23
00:02:49,447 --> 00:02:55,192
To ensure purity, different varieties
of the Brassica oleracea species

24
00:02:55,476 --> 00:02:58,690
should be planted at least 1 km apart. 

25
00:03:00,000 --> 00:03:04,378
This distance can be reduced
to 500 meters if there is a natural barrier

26
00:03:04,472 --> 00:03:07,287
such as a hedge between the two varieties. 

27
00:03:10,603 --> 00:03:14,680
The varieties can also be isolated
by placing small hives

28
00:03:14,763 --> 00:03:18,334
with insects inside a closed mosquito net

29
00:03:23,389 --> 00:03:27,090
or by alternately opening
and closing mosquito nets.

30
00:03:31,440 --> 00:03:32,618
For this technique,

31
00:03:32,843 --> 00:03:36,860
see the module on isolation techniques
in “The ABC of seed production”. 

32
00:03:50,180 --> 00:03:53,862
Life cycle of the Savoy cabbage

33
00:03:54,974 --> 00:03:57,680
Savoy cabbage is a biennial plant.

34
00:03:58,705 --> 00:04:03,643
In the first year of the cycle it is grown for seed
in the same way as for consumption.

35
00:04:08,327 --> 00:04:10,596
It will produce seeds in the second year. 

36
00:04:50,720 --> 00:04:56,436
You should select 10 to 15 plants
to ensure good genetic diversity.

37
00:04:57,220 --> 00:05:00,843
Savoy cabbage seeds are saved
from healthy plants

38
00:05:01,112 --> 00:05:04,465
that have been observed over
the entire period of growth.

39
00:05:05,440 --> 00:05:09,980
This ensures that all the characteristics
of the variety are known. 

40
00:05:10,356 --> 00:05:15,774
You should choose the most vigorous
heads that meet the desired selection criteria :

41
00:05:17,309 --> 00:05:22,865
regular and vigorous growth,
rapid formation of heads,

42
00:05:25,200 --> 00:05:28,660
good storage capacity, precocity,

43
00:05:30,887 --> 00:05:33,310
resistance to cold and to disease.

44
00:05:36,138 --> 00:05:41,520
Other characteristics influencing selection
are the typical shape of the variety,

45
00:05:42,080 --> 00:05:45,100
a pointed, flat or round head,

46
00:05:46,974 --> 00:05:52,472
a short stem, a good root system, taste, colour. 

47
00:05:55,934 --> 00:06:01,047
It is much more resistant to cold
than other species of Brassica oleracea

48
00:06:01,483 --> 00:06:06,501
and can withstand temperatures
as low as -15°.

49
00:06:06,890 --> 00:06:10,254
Most varieties can spend the winter outdoors. 

50
00:06:23,310 --> 00:06:26,618
The other methods of overwintering
and the second year

51
00:06:26,712 --> 00:06:30,340
of the life cycle are the same as for the cabbage.

52
00:07:07,636 --> 00:07:14,340
Harvesting, extracting,
sorting and storing of all Brassica oleracea.

53
00:07:23,505 --> 00:07:27,250
The seeds are mature
when the seed pods turn beige.

54
00:07:31,483 --> 00:07:34,007
The seed pods are very dehiscent,

55
00:07:34,196 --> 00:07:39,294
which means that they open very easily
when mature and disperse their seed. 

56
00:07:47,781 --> 00:07:51,963
Most of the time, the stalks do not
all mature at the same time.

57
00:07:52,836 --> 00:07:59,069
To avoid wasting any seed,
harvesting can take place as each stalk matures.

58
00:08:00,676 --> 00:08:06,298
The entire plant can also be harvested before
all of the seeds have completely matured. 

59
00:08:09,636 --> 00:08:14,800
The ripening process is then completed
by drying them in a dry,

60
00:08:14,950 --> 00:08:16,625
well-ventilated place. 

61
00:08:29,236 --> 00:08:35,374
Cabbage seeds are ready to be removed
when the seed pods can be easily opened by hand. 

62
00:08:37,403 --> 00:08:38,690
To extract the seeds,

63
00:08:38,800 --> 00:08:44,160
the seed pods are spread across
a plastic sheet or thick piece of fabric

64
00:08:44,581 --> 00:08:47,461
and then beaten or rubbed together by hand.

65
00:08:50,930 --> 00:08:56,021
You can also put them in a bag
and beat them against a soft surface. 

66
00:08:57,420 --> 00:09:02,072
Larger quantities can be threshed
by walking or driving on them. 

67
00:09:14,600 --> 00:09:20,000
Seed pods that do not open easily
probably contain immature seeds

68
00:09:20,130 --> 00:09:21,796
that will not germinate well. 

69
00:09:26,581 --> 00:09:27,898
During sorting,

70
00:09:28,130 --> 00:09:32,690
the chaff is removed by first passing
the seeds through a coarse sieve

71
00:09:33,025 --> 00:09:34,740
that retains the chaff

72
00:09:39,505 --> 00:09:42,043
and then by passing them through another sieve

73
00:09:42,501 --> 00:09:47,060
that retains the seeds but allows
smaller particles to fall through. 

74
00:09:50,760 --> 00:09:54,610
Finally, you should winnow them
by blowing on them

75
00:09:55,040 --> 00:09:59,025
or with the help of the wind
so that any remaining chaff is removed. 

76
00:10:14,225 --> 00:10:19,180
All seeds from the Brassica oleracea
species resemble one another.

77
00:10:20,174 --> 00:10:24,101
It is very difficult to distinguish
between, for example,

78
00:10:24,400 --> 00:10:26,865
cabbage and cauliflower seeds.

79
00:10:27,381 --> 00:10:30,320
This is why it is important to label the plants

80
00:10:30,589 --> 00:10:34,349
and then the extracted seeds
with the name of the species,

81
00:10:34,600 --> 00:10:37,643
the variety and the year of cultivation. 

82
00:10:39,076 --> 00:10:43,767
Storing the seeds in the freezer
for several days eliminates any parasites. 

83
00:10:49,280 --> 00:10:52,429
Cabbage seeds are able to germinate
up to 5 years.

84
00:10:53,512 --> 00:10:57,200
However, they may retain
this capacity up to 10 years.

85
00:10:58,880 --> 00:11:02,290
This can be prolonged by storing
them in the freezer.

86
00:11:03,243 --> 00:11:09,680
One gram contains 250 to 300 seeds
depending on the variety. 
