1
00:00:08,475 --> 00:00:11,680
The cardoon is a member of the Asteraceae family

2
00:00:12,015 --> 00:00:17,865
and the Cynara cardunculus species
that is grown for its fleshy stems.

3
00:00:25,460 --> 00:00:31,585
The Cynara genus also includes
the artichoke from the scolymus sub-species,

4
00:00:32,190 --> 00:00:33,955
grown for its floral bud.

5
00:00:38,095 --> 00:00:41,740
It is grown for seed in a similar
manner to the cardoon. 

6
00:00:53,500 --> 00:00:54,475
Pollination 

7
00:01:10,100 --> 00:01:11,495
Like the artichoke,

8
00:01:11,870 --> 00:01:17,670
the cardoon has individual florets
combined into a bluish purple capitulum.

9
00:01:19,585 --> 00:01:22,640
They are hermaphrodite but self-sterile.

10
00:01:24,740 --> 00:01:28,340
Each floret is pollinated by a second floret

11
00:01:28,630 --> 00:01:32,485
that can be situated on the same
capitulum or another one.

12
00:01:36,375 --> 00:01:39,705
The cardoon and the artichoke
are thus allogamous.

13
00:01:40,575 --> 00:01:43,170
They need to be pollinated by insects. 

14
00:01:45,335 --> 00:01:49,160
There is a risk of crossing
between the cardoon and the artichoke

15
00:01:49,470 --> 00:01:53,350
as well as between different
varieties grown in the same garden. 

16
00:01:57,685 --> 00:02:03,190
To avoid cross-pollination,
two varieties should be grown 1 kilometer apart. 

17
00:02:05,735 --> 00:02:08,785
This distance can be reduced to 500 meters

18
00:02:09,020 --> 00:02:12,370
if there is a natural barrier
such as a hedge between them. 

19
00:02:15,790 --> 00:02:20,465
The varieties can also be isolated
by alternately opening

20
00:02:20,555 --> 00:02:22,740
and closing mosquito net cages

21
00:02:23,375 --> 00:02:28,195
or by placing small hives
with insects inside a permanent net cage

22
00:02:29,485 --> 00:02:30,575
(for this technique,

23
00:02:30,815 --> 00:02:35,950
see the module on isolation techniques
in “The ABC of seed production”). 

24
00:02:40,545 --> 00:02:41,625
Life cycle 

25
00:02:47,725 --> 00:02:49,445
In the first year of the cycle,

26
00:02:49,895 --> 00:02:55,085
cardoon plants grown for seed are cultivated
in the same way as those for consumption.

27
00:02:59,160 --> 00:03:01,490
They produce seed in the second year. 

28
00:03:12,295 --> 00:03:18,690
Cardoon seeds are sown in a sheltered
pot in mid-February in cold climates

29
00:03:19,090 --> 00:03:25,025
and in the ground at
the end of April, beginning of May in mild climates. 

30
00:03:28,405 --> 00:03:30,745
To maintain good genetic diversity,

31
00:03:31,235 --> 00:03:34,615
you should select at least
a dozen plants for seed production,

32
00:03:36,165 --> 00:03:40,260
chosen for the desired criteria of the variety. 

33
00:03:43,960 --> 00:03:49,120
Select the cardoon stalks that are the most
vigorous and resistant to cold and to rot,

34
00:03:51,610 --> 00:03:55,540
with beautiful leaves
and fleshy but not stringy ribs,

35
00:03:56,845 --> 00:03:59,045
with or without spines. 

36
00:04:09,155 --> 00:04:13,480
Artichokes are selected for the number
of floral buds they develop,

37
00:04:14,025 --> 00:04:16,520
for their taste and regular growth. 

38
00:04:19,720 --> 00:04:23,475
Cardoon plants that flower
in the first year are removed. 

39
00:04:30,375 --> 00:04:33,415
In autumn, their leaves
are harvested for consumption. 

40
00:04:45,815 --> 00:04:48,270
In regions with very harsh winters,

41
00:04:48,585 --> 00:04:53,345
the roots are dug up before the frost
and stored in a place protected from frost. 

42
00:05:00,165 --> 00:05:02,235
In regions with a milder climate,

43
00:05:02,670 --> 00:05:07,340
the roots of cardoons and artichokes
can remain in the ground during winter. 

44
00:05:12,840 --> 00:05:15,955
In spring, the plants for seed production

45
00:05:16,075 --> 00:05:19,250
that have overwintered
in the cellar are replanted. 

46
00:06:18,365 --> 00:06:22,125
At the end of the summer,
the plants for seed will flower. 

47
00:06:38,810 --> 00:06:40,790
The seeds will be collected in autumn. 

48
00:06:47,435 --> 00:06:48,785
To harvest the seed,

49
00:06:48,950 --> 00:06:54,870
the capitula are cut when small white
and feathery plumes appear at their tips.

50
00:07:02,430 --> 00:07:07,440
The capitula can finish maturing in a dry,
well-ventilated place. 

51
00:07:11,725 --> 00:07:15,040
Extracting – sorting - storing

52
00:07:18,830 --> 00:07:22,795
Once the capitula are dry,
the plumes are removed by hand.

53
00:07:24,020 --> 00:07:27,060
Wear gloves to protect yourself from the spines.

54
00:07:27,860 --> 00:07:31,010
The capitula are then rubbed together
to remove the seeds.

55
00:07:41,120 --> 00:07:47,455
They can also be beaten in a bag put on a soil
that is not too hard using a wooden or plastic mallet.

56
00:07:55,100 --> 00:07:58,260
When sorting, remove large debris by hand

57
00:07:58,345 --> 00:08:02,370
and then blow gently on the seeds
to remove any remaining chaff

58
00:08:02,955 --> 00:08:05,240
to obtain perfectly clean seed. 

59
00:08:15,790 --> 00:08:19,215
Finally, pour the seeds into a plastic bag.

60
00:08:20,090 --> 00:08:24,300
Put a label inside with the name
of the variety, the species,

61
00:08:24,465 --> 00:08:25,910
and the year of production. 

62
00:08:33,510 --> 00:08:38,090
Storing the seeds in the freezer
for several days kills parasite larvae. 

63
00:08:41,260 --> 00:08:45,025
Cardoon seeds are able to germinate
for 7 years on average.

64
00:08:47,370 --> 00:08:52,045
This period of time can be lengthened
if the seeds are stored in a freezer. 

65
00:08:54,025 --> 00:08:58,920
One gram of seed contains around
25 individual seeds. 
