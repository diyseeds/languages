# Copyright (C) 2021 Maxime Lecoq
# This file is distributed under the GNU General Public License v2 or later.
msgid ""
msgstr ""
"Project-Id-Version: Diyseeds 2.0\n"
"POT-Creation-Date: 2021-07-10T00:00:00+02:00\n"
"PO-Revision-Date: 2021-09-12 15:13+0000\n"
"Last-Translator: Ieva Zariņa <ievazarinja@gmail.com>\n"
"Language-Team: Latvian <http://translate.diyseeds.org/projects/"
"diyseeds-org-pages/films/lv/>\n"
"Language: lv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n % 10 == 0 || n % 100 >= 11 && n % 100 <= "
"19) ? 0 : ((n % 10 == 1 && n % 100 != 11) ? 1 : 2);\n"
"X-Generator: Weblate 4.8\n"
"WPOT-Type: post-types/page\n"
"X-Domain: wpot\n"
"WPOT-Origin: films\n"

#. This title will be displayed into the browser's window title bar.
msgctxt "browser-title"
msgid "Diyseeds educational videos"
msgstr "Diyseeds izglītojošās īsfilmas"

#. The slug is the last part of the URL (www.example.org/slug-is-here). Only alphanumerical characters are allowed, use dash to replace space. The slug should be short and nice and it must reflect the content it refers to. It is usually made from the title. If you don't provide it, a slug will be auto-generated.
msgctxt "slug"
msgid "videos"
msgstr "video"

#. Title of a page's section.
msgctxt "section-title"
msgid "From seed to seed: educational films for seed production"
msgstr "''No sēklas līdz sēklai'': izglītojošas filmas par sēklu ražošanu"

msgid "Following the entire plant cycle, from sown seed to harvested seed, is a surprise, curiosity and pleasure. It also makes it possible to perpetuate and revitalize a heritage that has been built up over centuries thanks to close collaboration between humans and nature. As such, it is nobody's prerogative and it is priceless. It is urgent to become part of this dynamic tradition and to perpetuate it, because this is the only way to safeguard and renew the diversity of existing varieties."
msgstr ""
"Sekošana pilnam auga dzīves ciklam (no sēklu sēšanas līdz sēklu ievākšanai) "
"ir brīnums, kas priecē un aizrauj. Tā ir iespēja turpināt un atdzīvināt "
"gadsimtu mantojumu, ko ierosinājusi cilvēku un dabas kopdarbība. Tieši tādēļ "
"- piekļuve sēklām nedrīkst būt ierobežota. Sēklas ir nenovērtējamas. Svarīgi "
"no jauna iesaistīties tradīcijas dinamikā, lai mūsu mantojums neizzustu, jo "
"tikai tādējādi spēsim nosargāt un atjaunot esošo šķirņu daudzveidību."

msgid "This is all the more important in view of the major climate changes observed in recent decades. Knowing how to select and multiply varieties adapted to specific pedoclimatic conditions or to rapid climate changes is a challenge that must not be left in the hands of multinationals."
msgstr ""
"Tradīcijas atjaunotne ir vēl jo izšķirīgāka - pēdējās desmitgadēs novērojamo "
"klimata pārmaiņu apstākļos. Šķirņu selekcijas un pavairošanas zinības, kas "
"pielāgotas specifiskiem augsnes un klimata nosacījumiem vai straujajām "
"klimata pārmaiņām, nav viegli gūstamas un ir sargājamas, tādēļ tās nedrīkst "
"kļūt par multinacionālo rūpnieku ekskluzīvu īpašumu."

msgid "Finally, learning and sharing this elementary and vital know-how makes it possible to regain part of our means of subsistence."
msgstr ""
"Visbeidzot - mācoties un daloties šajās elementārajās, taču nozīmīgajās, "
"praktiskajās iemaņās, mēs atgūsim spēju sevi nodrošināt."

msgid "The idea of making a series of educational films on seed production arose at a time when large events were bringing together thousands of people on the subject of seeds, seed fairs, campaigns against new restrictive European seed legislation, public awareness workshops. All of these actions have revealed the loss of knowledge in this area."
msgstr ""
"Iecere veidot sēklu ražošanai veltītu, izglītojošu filmu sēriju radās laikā, "
"kad tūkstošiem cilvēku sāka apvienoties plašākos, sēklām un sēklu tirdziņiem "
"veltītos projektos, īstenojot kampaņas pret jaunajiem, ierobežojošajiem "
"Eiropas sēklu likumiem un organizējot informatīvus seminārus. Aktivitāšu "
"norisē atklājies zināšanu trūkums sēklu jautājumos."

msgid "After three years of filming, the “From seed to seed” films were released in the autumn of 2015. They are designed to provide as many answers as possible to the questions that everyone faces when becoming involved in seed production. The educational films are divided into two categories: general ABCs and practical manuals for each vegetable."
msgstr ""
"Pēc trim filmēšanas gadiem - 2015. gada rudenī sabiedrību sasniedza ''No "
"sēklas līdz sēklai'' īsfilmu cikls. Īsfilmas veidotas ar nolūku piedāvāt "
"iespējami daudz atbilžu uz jautājumiem, ar kādiem sastopas ikviens sēklu "
"ražotājs. Izglītojošās filmas iedalītas divās kategorijās: pamatprincipu "
"ābece un praktiskās, atsevišķiem dārzeņiem veltītās pamācības."

#. Title of a page's section.
msgctxt "section-title"
msgid "Seed production methods in pictures: technical ABCs"
msgstr "Sēklu ražošanas metodes - attēlos: praktiskā ābece"

msgid "The ABCs are films dedicated to a theoretical approach to questions such as pollination, botanical classification, how to choose the seeds with which to start, the selection of seed-bearing plants. Other more practical films explain the manual pollination of cucurbits, techniques for isolating seed-bearing plants, extracting and sorting seeds. You can navigate from film to film according to your requirements and interests."
msgstr ""
"''Ābeces filmiņas'' pievēršas tādiem tehniskiem jautājumiem kā "
"apputeksnēšana, botāniskā klasifikācija, sējas sēklu izvēle, sēklaugu "
"selekcija. Citas praktiskās īsfilmas skaidro ķirbju dzimtas manuālās "
"apputeksnēšanas gaitu, sēklai paredzēto augu izolēšanas metodes, sēklu "
"ievākšanas un šķirošanas paņēmienus. Filmu secība pielāgojama jūsu prasībām "
"un interesēm!"

#. Title of a page's section.
msgctxt "section-title"
msgid "Videos to learn how to make your own vegetable seeds, step by step"
msgstr "Īsfilmas iepazīstinās ar dārzeņu sēklu ražošanu - soli pa solim"

msgid "Over thirty short films of about ten minutes are each dedicated to a vegetable, with chapters on its diversity, pollination, the life-cycle of the plant up to the harvest, sorting, extraction and conservation of its seeds. On this page you will find all the seed production tutorial videos."
msgstr ""
"Vairāk nekā 30 īsfilmas (ilgums - aptuveni 10 minūtes), no kurām katra "
"veltīta atsevišķam dārzenim, aplūkojot dārzeņa daudzveidību, apputeksnēšanu, "
"auga dzīves ciklu līdz pat ražas novākšanai, kā arī sēklu ievākšanas, "
"šķirošanas un uzglabāšanas procesus. Šajā lapā atradīsiet visus sēklu "
"ražošanas mācību video."

#. Title of a page's section.
msgctxt "section-title"
msgid "Our project to spread knowledge on the production of free seeds needs your support!"
msgstr ""
"Mūsu brīvo sēklu ražošanas zinību izplatīšanas projektam nepieciešams jūsu "
"atbalsts!"

msgid "The films were made in France, in many gardens of seed producers, but all the indications given can be adapted to many other environments and climates. A voice-over accompanies the images in order to give all the explanations needed to understand. You can download the voice-over texts in PDF format, to help you when you start to put the explanations into practice in your garden."
msgstr ""
"Filmas uzņemtas Francijā - vairāku sēklu ražotāju dārzos, taču filmu "
"praktiskās norādes piemērojamas daudzveidīgiem vides un klimatiskajiem "
"apstākļiem. Attēlus papildina ieskaņots teksts, piedāvājot nepieciešamos "
"skaidrojumus. Ieskaņotā teksta rakstiskā versija lejupielādējama PDF "
"formātā, lai sāktu pielietot iegūtās zināšanas praktiski - savā dārziņā."

msgid "This site is the natural extension of the films. The sale of the DVDs is used to finance the uploading, hosting of the site and videos, the translations and the further development of the project. Our goal is simple, obvious and ambitious: to enable everyone, in all countries of the world, to learn how to produce seeds from local open-pollinated varieties, to guarantee a healthy diet and maintain the biodiversity of vegetable crops."
msgstr ""
"Mājas lapa dabiski papildina īsfilmu ciklu. DVD tirdzniecība ļauj finansēt "
"mājas lapas un video augušpielādi un uzturēšanu, kā arī nodrošināt "
"tulkojumus un projekta turpmāku attīstību. Mūsu mērķis ir vienkāršs, skaidrs "
"un ambiciozs - ikvienam cilvēkam (jebkurā pasaules valstī) jābūt pieejamai "
"iespējai mācīties vietēju, dabiski apputeksnētu šķirņu sēklu ražošanas "
"principus, lai iegūtu veselīgu uzturu un rūpētos par dārzeņu kultūru "
"biodaudzveidības saglabāšanu."

msgid "The site is still under construction: we upload the films in each language over time. Many translations are already available on the site. Many more languages are planned. Some are already underway, while others will begin if the necessary funding is found. Support us by sending a donation, buying the DVD or offering to help with the translation work."
msgstr ""
"Mājas lapa nav pabeigta - laika gaitā ieskaņosim filmas jaunās valodās. "
"Mājas lapā jau pieejami tulkojumi vairākās valodās. Esam iecerējuši vēl "
"citus tulkojumus. Daži jau top, bet citiem nepieciešams rast finansējumu. "
"Atbalstiet mūsu darbību - ziedojot, iegādājoties DVD vai iesaistoties "
"tulkošanas projektos!"

msgid "And of course we hope that you will produce beautiful seeds to spread around you!"
msgstr ""
"Protams, ceram - jums izdosies izaudzēt skaistas sēklas, ko izplatīt savā "
"kopienā!"

#. This title will be displayed by search engines like Google. 60 characters max.
msgctxt "seo-title"
msgid "Watch Diyseeds videos"
msgstr "Skatieties Diyseeds īsfilmas"

#. This description will be displayed below the title on search engine results. 160 characters max.
msgctxt "seo-description"
msgid "Find our 40 films to watch free of charge and learn how to produce your own seeds. Classification, cycle, pollination, and conservation of seeds."
msgstr ""
"Skatieties mūsu 40 īsfilmas - bez maksas! Uzziniet, kā ražot sēklas "
"pašrocīgi. Sēklu klasifikācija, dzīves cikls, apputeksnēšana un uzglabāšana."

#. The tags are used by search engines to reference the content. Use commas to separate tags.
msgctxt "seo-tags"
msgid "seeds, video, tutorial, vegetables, learn, classification, cycle, pollination, extraction, sorting, conservation"
msgstr ""
"sēklas, video, instrukcija, dārzeņi, mācīties, klasifikācija, cikls, "
"apputeksnēšana, ievākšana, šķirošana, uzglabāšana"
