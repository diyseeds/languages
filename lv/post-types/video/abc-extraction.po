# Copyright (C) 2021 Maxime Lecoq
# This file is distributed under the GNU General Public License v2 or later.
msgid ""
msgstr ""
"Project-Id-Version: Diyseeds 2.0\n"
"POT-Creation-Date: 2021-07-10T00:00:00+02:00\n"
"PO-Revision-Date: 2022-12-06 19:11+0000\n"
"Last-Translator: Ieva Zariņa <ievazarinja@gmail.com>\n"
"Language-Team: Latvian <http://translate.diyseeds.org/projects/"
"diyseeds-org-videos/abc-extraction/lv/>\n"
"Language: lv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n % 10 == 0 || n % 100 >= 11 && n % 100 <= "
"19) ? 0 : ((n % 10 == 1 && n % 100 != 11) ? 1 : 2);\n"
"X-Generator: Weblate 4.8\n"
"WPOT-Type: post-types/video\n"
"X-Domain: wpot\n"
"WPOT-Origin: abc-extraction\n"

#. Title of the film
msgctxt "title"
msgid "Extraction, drying and sorting of seed"
msgstr "Sēklu ievākšana, žāvēšana un šķirošana"

msgid "The extraction and wet-processing method with fermentation"
msgstr "Sēklu ievākšana un mitrā apstrāde ar fermentāciju"

msgid "Wet-processing with a fermentation process is used for tomatoes and cucumbers. The process of fermentation allows removal of the gelatinous coating surrounding each seed that keeps it dormant."
msgstr ""
"Mitrās fermentācijas apstrāde ir piemērota tomātiem un gurķiem. "
"Fermentācijas process ļauj sēklu atbrīvot no želatīnveida apvalka, kas to "
"ietver, uzturot sēklu neaktīvā stāvoklī."

msgid "The tomatoes or cucumbers are cut in half. Their seeds and juice are extracted with a spoon and placed in a glass jar. A little water can be added if necessary."
msgstr ""
"Tomātus vai gurķus pārgriež uz pusēm. Ar karoti izgrebj sēklas un mīkstumu, "
"kas ievietojami stikla burkā. Ja nepieciešams, pievieno nelielu ūdens "
"daudzumu."

msgid "Seeds should not be saved from damaged or fermented fruit. Each jar is labeled with the name of the species and the variety. The glass jar makes it possible to observe the fermentation process."
msgstr ""
"Sēklas nevajadzētu ievākt no bojātiem vai ierūgušiem augļiem. Burkām "
"pievieno etiķetes ar sugas un šķirnes norādi. Stikla burkā būs vieglāk "
"novērojams fermentācijas process."

msgid "Do not close the jar tightly. Simply cover it and protect it from flies with insect netting and place it in a warm place between 23° and 30° out of direct sunlight. The time required for fermentation varies depending on air temperature and the amount of sugar in the fermentation liquid."
msgstr ""
"Burku neaizvākot pārāk cieši! Burka tiek pārklāta ar kukaiņu tīkliņu, lai "
"pasargātu to no mušām. Burku novieto siltā vietā (23° - 30°C), taču ne "
"tiešos saules staros. Fermentācijas ilgums atkarīgs no gaisa temperatūras un "
"cukura daudzuma šķidrumā."

msgid "Little by little, a white layer of mold forms on the surface that you should mix in several times to ensure more consistent fermentation and to avoid the formation of a too thick layer of mold. Adding a pinch of sugar prevents the growth of harmful molds and activates this process whenever there is not enough flesh."
msgstr ""
"Pamazām virsmu pārklās balta pelējuma kārta, kas vairākkārt iemaisāma "
"šķīdumā, lai fermentācija būtu vienmērīgāka un neveidotos pārāk biezs "
"pelējuma slānis. Šķipsniņa cukura mazinās kaitīga pelējuma attīstības risku, "
"kā arī aktivizēs procesu, ja augļa masas nav gana daudz - dabisko cukuru "
"nodrošināšanai."

msgid "The fermentation process should be closely observed. It may take less than 48 hours on very hot days. If left too long, the seeds now lacking their gelatinous coating will start to germinate and can no longer be used for seed."
msgstr ""
"Fermentācijas norise ir rūpīgi uzraugāma. Karstā laikā tā var ilgt mazāk "
"nekā 48 stundas. Process jāpārtrauc savlaicīgi, lai sēklas, zaudējušas "
"želatīnkārtiņu, nesāktu dīgt. Sadīgušās sēklas nav izmantojamas sēšanai."

msgid "When the seeds break away and fall to the bottom of the jar and the rest of the flesh and skin float to the top, the gelatinous coating has been destroyed and the process is complete. The seeds can now be cleaned. The seeds retained in the sieve are cleaned under a jet of water."
msgstr ""
"Kad sēklas atdalās un nogrimst burkas lejasdaļā, bet mizas un mīkstums "
"uzpeld virspusē, želatīnkārta ir pārstrādāta - process noslēdzies. Sēklas "
"nepieciešams notīrīt. Sēklas tiek aizturētas sietiņā un skalotas zem tekoša "
"ūdens."

msgid "The extraction and wet-processing method without fermentation"
msgstr "Sēklu ievākšana un mitrā apstrāde bez fermentācijas"

msgid "Wet-processing without a fermentation process is used for vegetables with fruits, such as eggplants or aubergines, pumpkins and courgettes or zucchini, melons and watermelons."
msgstr ""
"Mitrā apstrāde bez fermentācijas ir piemērotāka augļus ražojošiem dārzeņiem: "
"baklažāniem, ķirbjiem, kabačiem vai cukīni, melonēm un arbūziem."

msgid "The seeds are removed from the fruit and washed under running water in a colander. If the seeds do not separate easily from the flesh, they can be soaked in water for 12 to 24 hours until the flesh disintegrates and the seeds are released. To avoid fermentation, they should not be put in a warm place. The seeds should then be dried without delay."
msgstr ""
"Sēklas izgrebj no augļa, ievieto sietā un skalo tekošā ūdenī. Ja sēklas "
"viegli neatdalās no augļa masas, tās 12 - 24 stundas mērcējamas ūdenī, līdz "
"augļa masa izjūk, atbrīvojot sēklas. Lai neierosinātu fermentāciju, sēklas "
"nedrīkst turēt siltā vietā. Sēklas nepieciešams nekavējoties izžāvēt."

msgid "Drying"
msgstr "Žāvēšana"

msgid "After wet-processing, the seeds must be dried quickly. They should be dry after a maximum of two days. They are placed on a fine sieve or a plate in a well-ventilated dry place with a temperature of between 23° to 30°."
msgstr ""
"Pēc mitrās apstrādes - sēklas ātri jāizžāvē. Sēklu žāvēšana ilgst ne vairāk "
"kā divas dienas. Sēklas tiek iebērtas smalkā sietā vai šķīvī - labi "
"vēdinātā, sausā vietā, kur temperatūra sasniedz 23° - 30°C."

msgid "Another method for small amounts of seed is to put them on a very absorbent coffee filter on which they do not stick. A maximum of one teaspoon of seeds is placed on each filter. The name of the variety and species is written in permanent ink on each filter. The filters are hung on a clothes rack in a warm, dry, well ventilated place. The seeds should not be exposed to sunlight, nor should they be dried on paper because they will stick together and it will be difficult to remove them."
msgstr ""
"Žāvējot nelielus sēklu apjomus, tās izvietojamas uz absorbējoša kafijas "
"filtra, rūpējoties, lai sēklas nepieliptu. Viens filtrs paredzēts ne vairāk "
"kā tējkarotītei sēklu. Uz katra filtra ar ūdensnoturīgu rakstāmo tiek "
"atzīmēts šķirnes un sugas nosaukums. Filtrus izkar uz veļasauklas - siltā, "
"tumšā, labi vēdinātā vietā. Sēklām nedrīkst piekļūt saulesgaisma. Nežāvējiet "
"sēklas uz papīra - tās salips un būs grūti atdalāmas."

msgid "Remove the seeds and rub them between your hands to separate them from each other."
msgstr ""
"Lai sēklas atdalītu - noņemiet tās no pamatnes un saberzējiet plaukstās."

msgid "Sorting seeds"
msgstr "Sēklu šķirošana"

msgid "There are different ways to sort seeds after extraction. Either wet or dry methods can be used."
msgstr ""
"Pēc ievākšanas - sēklas šķirojamas vairākos veidos. Iespējams izmantot sauso "
"vai mitro metodi."

msgid "Seeds that are not surrounded by flesh such as leeks and onions can be sorted using water."
msgstr ""
"Sēklas, ko neietver mīkstā masa (puravi, sīpoli u.c.), šķirojamas ūdenī."

msgid "A large amount of water is poured into a transparent container and the seeds are dropped inside. The water is stirred several times so that the heavy fertile seeds fall to the bottom of the container. The seeds that remain on the surface along with the chaff are skimmed off with a colander. The water is then poured through a sieve to recover the seeds that have fallen to the bottom. They must be dried immediately. Many seeds that are very light cannot be sorted in this manner."
msgstr ""
"Caurspīdīgā traukā ielej lielu ūdens apjomu. Ūdenī ieber sēklas. Ūdeni "
"vairākkārt apmaisa, lai smagās, auglīgās sēklas nogrimtu trauka lejasdaļā. "
"Virspusē peldošās sēklas un pelavas tiek nosmalstītas ar sietu. Ūdeni lej "
"cauri sietam, lai iegūtu nogrimušās sēklas. Tās tūdaļ jāžāvē. Metode nav "
"piemērota ārkārtīgi vieglām sēkliņām."

msgid "Dry sorting is the most commonly used method."
msgstr "Biežāk lietota ir sausās šķirošanas metode."

msgid "With large seeds that are shelled by hand such as beans, for example, you just have to remove the badly shaped and damaged seeds."
msgstr ""
"Liela izmēra sēklas, kas lobītas ar rokām, (piemēram, pupas) paredz "
"deformēto vai bojāto sēklu manuālu atlasi."

msgid "For all other seeds whose seed heads are beaten or crushed, the chaff must be removed. The seeds are first passed through a very coarse sieve that retains the largest pieces of chaff; the seeds and smaller chaff fall into a bucket."
msgstr ""
"Pārējām sēklām, kuru sēklgalviņas tiek pāršķeltas vai pārspiestas, "
"atsijājamas pelavas. Vispirms sēklas tiek pārsijātas rupjā sietā, kas uztver "
"lielākos atkritumus. Sēklas un sīkākās pelavas nonāk spainī."

msgid "The process is then repeated using a fine sieve that retains the seeds and lets the chaff pass through. The choice of the sieve is critical; it should retain the seeds and let through as much chaff as possible."
msgstr ""
"Procesu atkārto, izmantojot smalkāku sietu, kas pārtver sēklas, bet izvētī "
"pelavas. Sieta izvēle ir būtiska - tam jāaiztur sēklas, izlaižot cauri "
"iespējami daudz pelavu."

msgid "To finish the cleaning, the seeds are poured into a flat container and blown on gently to remove any light chaff."
msgstr ""
"Tīrīšanas noslēgumā - sēklas ieber plakanā tvertnē. Tām viegli uzpūš, lai "
"izvētītu sīkos krikumus."

msgid "The wind can also be used to sort the seeds. A large sheet is spread on the ground. The seeds are poured on top of it and the wind blows away the chaff. The wind must be regular, because strong gusts will blow everything away."
msgstr ""
"Sēklu šķirošanā izmantojams arī vējš. Uz zemes tiek noklāta liela loksne. Uz "
"loksnes tiek izbērtas sēklas. Vējš no tām aizpūš pelavas. Vējam jābūt "
"vienmērīgam - spēcīgas brāzmas aizpūtīs loksni ar visām sēklām."

msgid "A small fan can also be used. A small compressor is also effective with heavy seeds; all others might be blown away."
msgstr ""
"Iespējams izmantot mazu ventilatoru. Smagākām sēklām piemērots arī gaisa "
"kompresors (sīkas sēklas tiktu aizpūstas)."

msgid "No matter what method is used, a small number of seeds are always lost."
msgstr ""
"Lai kāda metode izraudzīta - vienmēr tiek zaudēts neliels sēklu daudzums."

msgid "What’s important is to know to what extent you want to sort the seeds."
msgstr "Svarīgi noteikt, cik smalki sēklas šķirojamas."

msgid "Nature is very generous, and when you start to propagate seeds, you will soon realise that an enormous amount of seed is produced, more than you need for your own garden. Don’t absolutely try and save every seed– there will always be enough."
msgstr ""
"Daba ir dāsna. Iesākot sēklu pavairošanu, drīz atklāsiet - tiks saražots "
"ievērojams sēklu apjoms (vairāk nekā nepieciešams pašu dārzam). Nepūlieties "
"izšķirot ikkatru sēkliņu - vienmēr būs gana tāpat!"

#. This title will be displayed by search engines like Google. 60 characters max.
msgctxt "seo-title"
msgid "Extraction, cleaning, drying and sorting of seeds"
msgstr "Sēklu ievākšana, tīrīšana, žāvēšana un šķirošana"

#. This description will be displayed below the title on search engine results. 160 characters max.
msgctxt "seo-description"
msgid "In this video, see how to extract, clean, dry and sort your seeds using different methods."
msgstr ""
"Īsfilmā noskaidrosiet, kā sēklas ievācamas, tīrāmas, žāvējamas un šķirojamas "
"- izmantojot dažādas metodes."

#. The tags are used by search engines to reference the content. Use commas to separate tags.
msgctxt "seo-tags"
msgid "extract, clean, dry, sort, seeds, video, explanations, learn, ABC"
msgstr "ievākt, tīrīt, žāvēt, šķirot, sēklas, video, pamācība, mācīties, ābece"

#. This title will be displayed on Online Video Platforms (OVP) like YouTube or Vimeo.
msgctxt "ovp-title"
msgid "ABC Extraction, wet-processing, drying and sorting of seeds"
msgstr "Sēklu ievākšanas, mitrās apstrādes, žāvēšanas un šķirošanas ābece"

#. This description will be displayed on Online Video Platforms (OVP) like YouTube or Vimeo.
msgctxt "ovp-description"
msgid "Watch this video to learn how to extract and clean your seeds. Some seeds, like the ones from tomatoes and cucumbers, require a fermentation process. In other cases, water is only used to clean and extract the seeds from the pulp. Seeds can then be sorted with sieves, a fan or even water to keep only the viable seeds and ensure good germination."
msgstr ""
"Skatieties īsfilmu, lai noskaidrotu, kā sēklas ievācamas un tīrāmas! Dažām "
"sēklām, piemēram, tomātu un gurķu sēklām, nepieciešama fermentācija. Citām "
"sēklām ūdens lietojams tikai sēklu tīrīšanas un atdalīšanas nolūkos. Sēklas "
"iespējams šķirot, izmantojot sietus, ventilatoru vai pat ūdeni, lai paturētu "
"tikai auglīgās sēklas un nodrošinātu veiksmīgu dīgšanu."
