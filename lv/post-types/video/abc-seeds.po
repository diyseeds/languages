# Copyright (C) 2021 Maxime Lecoq
# This file is distributed under the GNU General Public License v2 or later.
msgid ""
msgstr ""
"Project-Id-Version: Diyseeds 2.0\n"
"POT-Creation-Date: 2021-07-10T00:00:00+02:00\n"
"PO-Revision-Date: 2022-12-06 19:11+0000\n"
"Last-Translator: Ieva Zariņa <ievazarinja@gmail.com>\n"
"Language-Team: Latvian <http://translate.diyseeds.org/projects/"
"diyseeds-org-videos/abc-seeds/lv/>\n"
"Language: lv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n % 10 == 0 || n % 100 >= 11 && n % 100 <= "
"19) ? 0 : ((n % 10 == 1 && n % 100 != 11) ? 1 : 2);\n"
"X-Generator: Weblate 4.8\n"
"WPOT-Type: post-types/video\n"
"X-Domain: wpot\n"
"WPOT-Origin: abc-seeds\n"

#. Title of the film
msgctxt "title"
msgid "How to choose seeds"
msgstr "Kā īstenot sēklu atlasi"

msgid "It is important to know the origin of the varieties that you are going to cultivate in your garden."
msgstr ""
"Svarīgi noskaidrot to šķirņu izcelsmi, kuras grasāties audzēt savā dārziņā."

msgid "When choosing the varieties from which you plan to produce seeds, you should avoid using ones that have been developed by the big seed industry. So do not use F1 or F2 hybrid seeds, GMOs or all seeds developed through biotechnology."
msgstr ""
"Izvēloties sēklu ražošanai paredzētās šķirnes, ieteicams izvairīties no "
"lielās sēklu industrijas selekcionētajām šķirnēm. Neizmantojiet F1 vai F2 "
"hibrīdsēklas, ĢMO vai biotehnoloģiju ceļā ražotās sēklas."

msgid "Plants grown from F1 and F2 hybrid seeds will not reproduce themselves identically. Their seeds can also be sterile or produce plants with unpredictable characteristics. By technically blocking their varieties, the seed multinationals can prevent gardeners from reproducing their own seeds and thereby keep their monopoly on the market."
msgstr ""
"No F1 un F2 hibrīdsēklām audzētie augi nevairosies identiskā veidā. Sēklas "
"var izrādīties sterilas vai radīt augus ar neparedzamām īpašībām. "
"Pielietojot bloķēšanas paņēmienus - multinacionālie sēklu ražotāji neļauj "
"dārzkopjiem ražot savas sēklas, jo lielie ražotāji vēlas saglabāt tirgus "
"monopolu."

msgid "These technical mechanisms reinforce the legal regulations that prohibit the free reproduction of seeds. All of these varieties are covered by intellectual property rights, such as plant property rights and patents."
msgstr ""
"Minētie tehniskie mehānismi uzspiež juridiskas prasības, aizliedzot brīvu "
"sēklu pavairošanu. Šķirnes pakļautas tādām intelektuālā īpašuma tiesībām kā "
"augu īpašuma tiesības un patenti."

msgid "To begin producing your own seeds, the seeds should be from varieties that are open-pollinated and if possible from organic, biodynamic or agro-ecological agriculture."
msgstr ""
"Sēklu ražošanas \"izejsēklas\" iegūstamas no dabiski apputeksnētām šķirnēm. "
"Vēlams - no bioloģiskām, biodinamiskām vai agroekoloģiskām sistēmām."

msgid "Nature is very generous and will in general provide you with far more seeds than you will need for your own use. The surplus can be given to others or exchanged."
msgstr ""
"Daba ir dāsna. Parasti iegūsiet daudz vairāk sēklu - nekā nepieciešams pašu "
"lietošanai. Pāri palikušās sēklas var dāvināt citiem vai iemainīt."

#. This title will be displayed by search engines like Google. 60 characters max.
msgctxt "seo-title"
msgid "Seeds to start your own seed production"
msgstr "Sēklu izvēle - sēklu ražošanas iesākumam"

#. This description will be displayed below the title on search engine results. 160 characters max.
msgctxt "seo-description"
msgid "The origin of the varieties to be grown as seed plants must be known. Choose reproducible, organic, bio-dynamic or agroecological seeds."
msgstr ""
"Nepieciešams izpētīt sēklai paredzēto augu šķirņu izcelsmi. Izvēlieties "
"pavairojamas, bioloģiski, biodinamiski vai agroekoloģiski audzētas sēklas."

#. The tags are used by search engines to reference the content. Use commas to separate tags.
msgctxt "seo-tags"
msgid "seed carrier, seeds, cultivate, varieties, free, reproducible, organic, bio-dynamic, agroecological, video, ABC, explanations, learn"
msgstr ""
"augi sēklai, sēklas, kultivēt, šķirnes, bezmaksas, pavairojams, bioloģisks, "
"biodinamisks, agroekoloģisks, video, ābece, pamācība, mācīties"

#. This title will be displayed on Online Video Platforms (OVP) like YouTube or Vimeo.
msgctxt "ovp-title"
msgid "ABC How to choose your seeds for seed production"
msgstr "Kā izraudzīties sēklas - sēklu ražošanai (ābece)"

#. This description will be displayed on Online Video Platforms (OVP) like YouTube or Vimeo.
msgctxt "ovp-description"
msgid "Watch this video to learn how to choose what seeds to reproduce. You should avoid using ones that have been developed by the big seed industry. So do not use F1 or F2 hybrid seeds, GMOs or any seed developed through biotechnology. To begin producing your own seeds, the seeds should be from varieties that are open-pollinated and if possible from organic, biodynamic or agro-ecological agriculture. Nature is very generous and will in general provide you with far more seeds than you will need for your own use. The surplus can be given to others or exchanged."
msgstr ""
"Skatieties īsfilmu, lai noskaidrotu, kā atlasīt pavairošanai piemērotākās "
"sēklas! Nekad neizvēlieties lielās sēklu industrijas selekcionētās un "
"ražotās sēklas. Neizmantojiet F1 vai F2 hibrīdsēklas, ĢMO sēklas vai sēklas, "
"kas iegūtas biotehnoloģiju lietojuma ceļā. Lai sāktu sēklu ražošanu, "
"nepieciešams izraudzīties sēklas no dabiski apputeksnētām šķirnēm, kā arī ("
"ja iespējams) - no bioloģiskās, biodinamiskās vai agroekoloģiskās "
"lauksaimniecības sistēmās kultivētu augu sēklām. Daba ir dāsna. Parasti "
"iegūsiet vairāk sēklu, nekā nepieciešams pašu vajadzībām. Liekās sēklas "
"iespējams iedāvināt citiem vai iemainīt pret citām sēklām."
