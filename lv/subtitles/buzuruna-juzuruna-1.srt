﻿1
00:00:00,000 --> 00:00:05,760
Intervija ar ''Buzuruna Juzuruna'' pārstāvi Seržu Harfušu
2021. g. maijs, Bekā ieleja, Libāna
www.DIYseeds.org
izglītojošas filmas par sēklu ražošanu

2
00:00:05,920 --> 00:00:08,760
-Ko īsti nozīmē ''Buzuruna Juzuruna''?

3
00:00:09,520 --> 00:00:12,100
Tas nozīmē - ''mūsu sēklas, mūsu saknes''.

4
00:00:12,140 --> 00:00:16,150
-Un kā nosaukums saistīts ar jūsu apvienību?

5
00:00:17,870 --> 00:00:25,170
Esam skolai piesaistīti zemnieku sēklu audzētāji,

6
00:00:26,090 --> 00:00:30,570
rīkojam arī apmācības.

7
00:00:30,660 --> 00:00:35,520
Tās ir mūsu pamatiniciatīvas.

8
00:00:36,220 --> 00:00:39,350
Mēs ražojam sēklas,

9
00:00:39,530 --> 00:00:42,460
kas izdzen saknes.

10
00:00:42,460 --> 00:00:47,020
Mērķis - iedibināt zemnieku pašnoteikšanos

11
00:00:47,130 --> 00:00:49,380
un pārtikas suverenitāti.

12
00:00:49,380 --> 00:00:53,120
Tas šķiet vērtīgs apvienojums.

13
00:00:54,080 --> 00:01:04,730
-Kādēļ pārtikas autonomija pašlaik ir aktuāla Libānā?

14
00:01:05,600 --> 00:01:07,840
Šeit valda krīze.

15
00:01:07,860 --> 00:01:13,600
Mūs piemeklējusi nepieredzēta ekonomiskā krīze,

16
00:01:13,600 --> 00:01:15,600
kas izpaužas vardarbībā.

17
00:01:15,710 --> 00:01:24,150
Mūsu valūtas vērtība 10-kārt kritusies.

18
00:01:24,260 --> 00:01:30,020
No 1500 Libānas mārciņām par dolāru - kurss nokritās līdz pat 15 000,

19
00:01:30,040 --> 00:01:32,880
pašlaik kurss stabilizējies - pie 12 000.

20
00:01:32,950 --> 00:01:36,600
Valūtas mainība ir neparedzama, nezinām, kas mūs gaida.

21
00:01:36,680 --> 00:01:37,770
-Kad tas notika?

22
00:01:37,860 --> 00:01:43,400
Sabrukums sākās ap 2018. gadu,

23
00:01:43,510 --> 00:01:54,640
taču tirgus inflācija bija vērojama no 2019. g. vidus, 2020. g. sākuma.

24
00:01:55,950 --> 00:02:01,260
-Kad tā notiek - kā tiek ietekmēta vietējo kopienu dzīve?

25
00:02:02,600 --> 00:02:07,020
Tas ir biedējoši, jo atalgojuma līmenis (Libānas mārciņās) nav mainījies.

26
00:02:07,060 --> 00:02:12,200
Minimālā alga sasniedza aptuveni 400 € / mēnesī.

27
00:02:12,200 --> 00:02:17,350
tagad - tikai 40€,

28
00:02:17,880 --> 00:02:21,620
taču visas preces kļuvušas dārgākas,

29
00:02:21,680 --> 00:02:24,750
jo valūta piesaistīta dolāram, un sekas ir dramatiskas.

30
00:02:24,840 --> 00:02:27,910
Iztēlojieties - jūs pērkat piena paciņu.

31
00:02:27,930 --> 00:02:31,530
Piena paka, kas maksāja 3000 Libānas mārciņu,

32
00:02:31,530 --> 00:02:36,480
tagad maksā 27 vai 25 000,

33
00:02:36,600 --> 00:02:40,200
lai gan joprojām saņemat, piemēram, 100 000.

34
00:02:40,200 --> 00:02:44,160
Tādēļ vairs nevarat nopirkt vairākas pakas mēnesī,

35
00:02:44,260 --> 00:02:47,380
knapi pietiek vienai.

36
00:02:47,440 --> 00:02:59,180
Mūsu patēriņš (90-95%) atkarīgs no importa,

37
00:02:59,260 --> 00:03:01,580
bet piesaiste ārvalstu valūtai

38
00:03:01,620 --> 00:03:03,260
(mūsu gadījumā - dolāram)

39
00:03:03,310 --> 00:03:05,520
visu daudzkārt sadārdzina:

40
00:03:05,570 --> 00:03:08,210
maize kļūst dārgāka,

41
00:03:08,360 --> 00:03:11,450
sadārdzinās degviela,

42
00:03:11,560 --> 00:03:14,660
bet algas paliek nemainīgas.

43
00:03:14,690 --> 00:03:17,720
Tas nozīmē - labklājības standarti,

44
00:03:17,750 --> 00:03:20,820
dzīves kvalitāte būtiski krītas,

45
00:03:20,910 --> 00:03:23,090
tāpat cieš arī

46
00:03:23,110 --> 00:03:26,530
pārtikas pieejamība, pārtikas drošība.

47
00:03:26,720 --> 00:03:30,370
Tādēļ nekavējoties jāsāk ražot savas sēklas,

48
00:03:30,430 --> 00:03:33,200
lai nenāktos tās importēt vai iepirkt,

49
00:03:33,220 --> 00:03:35,490
īpaši - ja tās ir sterilas hibrīdsēklas,

50
00:03:35,520 --> 00:03:38,230
kas nākamjā gadā jāpērk atkal.

51
00:03:40,010 --> 00:03:43,980
Jāražo arī pašiem savs mēslojums -

52
00:03:44,020 --> 00:03:46,480
bioloģiskā un tīrā veidā,

53
00:03:46,530 --> 00:03:49,810
jāražo komposts...

54
00:03:49,890 --> 00:03:51,540
Viss nepieciešamais.

55
00:03:51,570 --> 00:03:55,290
Tam arī veltītas mūsu mācību programmas

56
00:03:55,330 --> 00:03:57,330
un saimniecības aktivitātes.

57
00:03:58,730 --> 00:04:01,460
-Kad biji Francijā, tu mums rādīji

58
00:04:01,470 --> 00:04:04,110
attēlus -

59
00:04:04,160 --> 00:04:07,630
pēc, tā dēvētās, revolūcijas,

60
00:04:07,640 --> 00:04:10,560
kad daudziem dzīve bija mainījusies,

61
00:04:10,660 --> 00:04:12,610
dažādiem cilvēkiem:

62
00:04:12,630 --> 00:04:18,410
studentiem, administratīvajiem darbiniekiem,

63
00:04:18,430 --> 00:04:20,730
dažāda dzīvesgājuma ļaudīm,

64
00:04:20,750 --> 00:04:23,360
kas pēkšņi pievērsās sēklu ražošanai.

65
00:04:23,630 --> 00:04:24,810
Tieši tā.

66
00:04:24,930 --> 00:04:27,530
Mēs to itin kā paredzējām -

67
00:04:28,180 --> 00:04:33,380
mūs jau gaidīja nepieredzētas ekonomiskās krīzes priekšnojautas.

68
00:04:33,490 --> 00:04:38,210
Tādēļ arī iesākās revolūcija.

69
00:04:38,270 --> 00:04:43,040
Cilvēki neskaidri aptvēra -

70
00:04:43,040 --> 00:04:47,650
turpmākajos gaidos gaidāmas grūtības,

71
00:04:47,680 --> 00:04:50,490
kuras nāksies kā nebūt pārvarēt.

72
00:04:50,540 --> 00:04:54,340
Arvien vairāk apzināmies -

73
00:04:54,400 --> 00:05:00,000
neproduktīva ekonomika nav ilgtspējīga,

74
00:05:00,080 --> 00:05:03,130
bet mūsu ekonomika nebija produktīva.

75
00:05:03,460 --> 00:05:05,920
Ilgu laiku - 30, 40 gadus -

76
00:05:05,970 --> 00:05:10,250
tā bija parādu ekonomika.

77
00:05:11,090 --> 00:05:15,320
-Un daudzi pievērsās ražošanai?

78
00:05:15,460 --> 00:05:17,980
...

79
00:05:18,040 --> 00:05:19,290
Notiek ieraksts.

80
00:05:20,430 --> 00:05:23,260
Piedodiet, runāju ar kolēģi.

81
00:05:23,280 --> 00:05:26,070
-Vai saimniecībā ir daudz cilvēku?

82
00:05:26,850 --> 00:05:31,620
16 pieaugušie, 27 bērni,

83
00:05:31,650 --> 00:05:34,160
pašlaik pie mums viesojas arī vairāki draugi,

84
00:05:34,210 --> 00:05:36,000
brīvprātīgie, praktikanti...

85
00:05:36,030 --> 00:05:38,930
Kopā - esam kādi 50 cilvēki,

86
00:05:39,000 --> 00:05:40,370
varbūt nedaudz vairāk.

87
00:05:40,410 --> 00:05:43,350
-Un cik liela ir saimniecība?

88
00:05:44,940 --> 00:05:47,770
Ferma (kopā ar apvienību)

89
00:05:47,920 --> 00:05:54,140
aizņem 5 akru platību:

90
00:05:54,580 --> 00:05:57,940
4,5 akri - audzēšanai,

91
00:05:57,950 --> 00:06:00,500
pusakrs - ēkām,

92
00:06:00,670 --> 00:06:02,980
bērnu telpām,

93
00:06:03,070 --> 00:06:06,050
lopkopības aplokiem: cāļiem,

94
00:06:06,060 --> 00:06:07,640
kazām, aitām.

95
00:06:07,710 --> 00:06:09,820
Šogad mums izdevās

96
00:06:09,830 --> 00:06:13,490
izīrēt vēl papildus 70 000 kvadrātmetrus,

97
00:06:13,610 --> 00:06:16,600
aptuveni 17 akrus,

98
00:06:16,700 --> 00:06:19,700
lai pavairotu un paplašinātu krājumus,

99
00:06:19,720 --> 00:06:21,350
īpaši - graudaugus.

100
00:06:22,280 --> 00:06:24,770
-Bet jūs nedzīvojat saimniecībā?

101
00:06:25,610 --> 00:06:27,510
Ne gluži.

102
00:06:27,520 --> 00:06:32,860
Tur dzīvo viens no mūsu kolēģiem un līdzdibinātājiem.

103
00:06:34,400 --> 00:06:36,320
Mums ir multifunkcionāla ēka:

104
00:06:36,340 --> 00:06:39,780
virtuve - produktu apstrādei,

105
00:06:39,820 --> 00:06:43,090
virtuve - pašiem, noliktava, skola,

106
00:06:43,140 --> 00:06:44,350
kā arī dzīvoklis,

107
00:06:44,360 --> 00:06:47,040
diezgan garena ēka.

108
00:06:47,060 --> 00:06:50,120
Šeit dzīvo Valids kopā ar savu ģimeni.

109
00:06:50,260 --> 00:06:52,440
Pārējie draugi un kolēģi

110
00:06:52,460 --> 00:06:54,260
dzīvo pāri ielai,

111
00:06:54,310 --> 00:06:56,530
bet mēs - nedaudz tālāk ieliņā,

112
00:06:56,550 --> 00:06:58,450
4 minūšu gājiens.

113
00:06:59,590 --> 00:07:02,290
-Un jūs ražojat arī pārtiku,

114
00:07:02,300 --> 00:07:04,400
ko paši lietojat uzturā?

115
00:07:04,740 --> 00:07:06,950
Jā!

116
00:07:07,160 --> 00:07:10,070
Iepērkamies reti.

117
00:07:10,110 --> 00:07:13,770
Ko vien varam saražot - nepērkam, mums ir pašiem!

118
00:07:14,900 --> 00:07:17,980
Dažus produktus (rīsus)

119
00:07:18,010 --> 00:07:20,880
mums ir grūti izaudzēt,

120
00:07:20,910 --> 00:07:22,690
tamlīdzīgas preces.

121
00:07:22,920 --> 00:07:24,120
Taču pārējais mums ir…

122
00:07:24,130 --> 00:07:27,230
Pagājušogad ieguvām pat medu,

123
00:07:27,250 --> 00:07:30,520
taču bitēm uzbruka plēsējs,

124
00:07:30,560 --> 00:07:32,600
tāpēc palika 3 stropi -

125
00:07:32,620 --> 00:07:34,570
no 11.

126
00:07:36,830 --> 00:07:39,980
Vēlamies parādīt - tas ir iespējams.

127
00:07:41,590 --> 00:07:46,920
-Kā ar sadarbību, saiknēm, sadraudzību,

128
00:07:46,940 --> 00:07:50,600
ko esat iedibinājuši Libānā un arīdzan Sīrijā,

129
00:07:50,610 --> 00:07:54,570
kā sadarbība attīstās, kā tā sāka veidoties?

130
00:07:54,590 --> 00:07:56,450
Pastāsti mums!

131
00:07:57,100 --> 00:08:00,160
Pamatideja bija iesākt ar sēklu audzēšanu,

132
00:08:00,180 --> 00:08:03,170
lai vēlāk mācītu visus interesentus,

133
00:08:03,190 --> 00:08:04,560
kā saglabāt izaudzētās sēklas,

134
00:08:04,590 --> 00:08:08,450
lai nenāktos tās ražot katru gadu no jauna.

135
00:08:08,450 --> 00:08:11,030
Tās ir pašpietiekamības iemaņas,

136
00:08:11,040 --> 00:08:12,560
neatkarības prasmes.

137
00:08:12,580 --> 00:08:16,380
Vēlāk - sēklu glabātāji

138
00:08:16,400 --> 00:08:17,910
viesojās daudzviet.

139
00:08:17,920 --> 00:08:19,710
Daži atgriezās Sīrijā,

140
00:08:19,710 --> 00:08:22,260
lai tur ražotu savas sēklas.

141
00:08:22,280 --> 00:08:25,120
Katra situācija ir krietni atšķirīga.

142
00:08:25,150 --> 00:08:29,610
Mūsu draugi, kuru darbībai sekojam jau sen,

143
00:08:29,630 --> 00:08:32,020
pašlaik ir pilnībā autonomi.

144
00:08:32,040 --> 00:08:34,970
Viņiem klājas visnotaļ labi.

145
00:08:34,980 --> 00:08:36,910
Turpinām sazināties,

146
00:08:36,920 --> 00:08:40,350
ja nepieciešama informācijas vai pieredzes apmaiņa:

147
00:08:40,360 --> 00:08:42,840
''izmēģinājām'', ''viņi pārbaudīja''.

148
00:08:42,920 --> 00:08:46,690
Mērķis - pulcināt sēklu glabātājus

149
00:08:46,690 --> 00:08:49,360
un dibināt sēklu bankas -

150
00:08:49,390 --> 00:08:51,640
visā Libānā,

151
00:08:51,690 --> 00:08:55,600
varbūt kādreiz pat Sīrijā, kas zina.

152
00:08:57,460 --> 00:09:02,240
-Robeža joprojām slēgta?

153
00:09:03,970 --> 00:09:06,550
Jā, slēgta,

154
00:09:06,570 --> 00:09:09,540
un to šķērsot ir visai grūti,

155
00:09:09,610 --> 00:09:12,040
īpaši - dažu tautību pārstāvjiem.

156
00:09:12,050 --> 00:09:16,250
Ja nepiederi diplomātiskai misijai vai armijai,

157
00:09:16,400 --> 00:09:17,990
ir grūti -

158
00:09:18,660 --> 00:09:20,860
labāk nemēģināt.

159
00:09:21,700 --> 00:09:23,550
Labāk palikt, kur esi.

160
00:09:23,580 --> 00:09:25,730
-Par apmācībām...

161
00:09:25,920 --> 00:09:30,670
Tu piedalījies DIYseeds tulkošanā -

162
00:09:34,330 --> 00:09:37,440
tulkoji mājas lapu?

163
00:09:38,750 --> 00:09:40,100
... tulkoju arābu valodā,

164
00:09:40,130 --> 00:09:44,680
pārskatīju citas komandas veidotos video,

165
00:09:44,820 --> 00:09:47,750
paveikts ļoti daudz.

166
00:09:47,760 --> 00:09:50,090
Pārsteidzoši, ko izdevies radīt, brīnišķīgi.

167
00:09:50,170 --> 00:09:52,080
Un ārkārtīgi lietderīgi.

168
00:09:52,120 --> 00:09:54,720
Organizējot mācības,

169
00:09:54,780 --> 00:09:57,840
cenšoties paskaidrot, kā iegūt sēklas,

170
00:09:57,870 --> 00:10:01,660
kā ievērot atstatumu,

171
00:10:01,670 --> 00:10:03,360
kā novērst svešapputi,

172
00:10:03,380 --> 00:10:05,360
vēlams izmantot attēlus,

173
00:10:05,360 --> 00:10:08,140
ilustrācijas, video un pamācības.

174
00:10:08,140 --> 00:10:11,550
Mēs īsfilmas demonstrējam savos kursos,

175
00:10:11,560 --> 00:10:13,470
taču tās ir pieejamas arī

176
00:10:13,480 --> 00:10:17,000
cilvēkiem - mobilajos tālruņos

177
00:10:17,020 --> 00:10:19,660
-ja vien ir interneta pieslēgums, taču tā ir iespēja,

178
00:10:19,680 --> 00:10:23,800
un cilvēki tādējādi paši var lieliski tikt galā.

179
00:10:23,910 --> 00:10:25,680
Ideāls risinājums.

180
00:10:27,600 --> 00:10:30,770
-Un cilvēkiem jūsu reģionā

181
00:10:31,240 --> 00:10:35,560
ir ērtāk pielietot informāciju…

182
00:10:35,640 --> 00:10:36,480
Tieši tā!

183
00:10:36,540 --> 00:10:37,730
Viss kļūst vienkāršāk,

184
00:10:37,760 --> 00:10:40,500
jo tas nav nekas sarežģīts, apgrūtinošs.

185
00:10:40,520 --> 00:10:44,910
Tā nav bieza grāmata, kas jānēsā līdzi.

186
00:10:44,960 --> 00:10:47,940
Daudzi zemnieki neprot lasīt,

187
00:10:48,000 --> 00:10:50,220
taču saprot dzirdēto.

188
00:10:50,260 --> 00:10:52,970
Turklāt ir zīmējumi un videosižets,

189
00:10:53,040 --> 00:10:55,570
kas vēl veiksmīgāk ilustrē sacīto,

190
00:10:55,580 --> 00:10:58,770
tas ir uztveramāk, skaidrāk,

191
00:10:58,800 --> 00:11:01,830
pamācība, kam viegli sekot,

192
00:11:01,870 --> 00:11:05,270
kas aizstāj biezas grāmatas un apjukumu.

193
00:11:06,000 --> 00:11:11,350
-Jūsu ražotās sēklas nav hibrīdsēklas,

194
00:11:11,380 --> 00:11:14,580
bet dabiskas, pavairojamas,

195
00:11:14,660 --> 00:11:18,030
ikvienam lietojamas sēklas,

196
00:11:18,160 --> 00:11:21,380
kas kopā ar īsfilmām ļauj strauji atgūt pašpietiekamību.

197
00:11:21,400 --> 00:11:26,560
Vai pastāstīsiet - kā, cik ātri tas notiek?

198
00:11:26,580 --> 00:11:27,630
Cik ātri?

199
00:11:27,650 --> 00:11:30,530
Tas atkarīgs no piekļuves zemei,

200
00:11:30,560 --> 00:11:32,160
piekļuves ūdenim,

201
00:11:32,180 --> 00:11:34,310
no drošības līmeņa reģionā.

202
00:11:34,320 --> 00:11:37,320
Mūsu draugiem ir nācies vairākkārt pārcelties -

203
00:11:37,390 --> 00:11:39,260
no mītnes Sīrijā.

204
00:11:39,300 --> 00:11:41,170
Ikreiz, apmetoties jaunā vietā,

205
00:11:41,180 --> 00:11:42,830
sākās uzlidojumi,

206
00:11:42,840 --> 00:11:44,810
tādēļ nācās atkal pārvākties.

207
00:11:44,960 --> 00:11:47,240
Konteksts nav no vienkāršajiem.

208
00:11:47,320 --> 00:11:51,670
Taču - pamatrīki, pamatiemaņas

209
00:11:51,710 --> 00:11:54,660
ļauj atsākt darbību jebkurā vietā.

210
00:11:54,690 --> 00:11:57,110
Jums ir sēklas, jūs protat no tām iegūt augus,

211
00:11:57,130 --> 00:12:00,260
jūs zināt, kā sēklas pavairot, kā tās ievākt,

212
00:12:00,290 --> 00:12:01,990
kā tās saglabāt, uzglabāt utt.

213
00:12:02,020 --> 00:12:04,980
Esat brīvs cilvēks!

214
00:12:07,140 --> 00:12:08,050
Tas arī viss.

215
00:12:08,190 --> 00:12:12,120
-Tātad ir cilvēki, kuri vienā sezonā

216
00:12:12,170 --> 00:12:15,180
iemācījušies ražot sēklas -

217
00:12:15,580 --> 00:12:19,470
un viņiem nu ir pašiem savas sēklas?

218
00:12:19,520 --> 00:12:20,510
Tieši tā.

219
00:12:20,530 --> 00:12:23,240
Vairums cilvēku, ar ko strādājam,

220
00:12:23,290 --> 00:12:25,350
auguši agrārā vidē,

221
00:12:25,370 --> 00:12:26,840
ieguvuši zemkopības iemaņas

222
00:12:26,850 --> 00:12:29,000
vai strādājuši lauku saimniecībā,

223
00:12:29,020 --> 00:12:30,440
nereti - konvencionālā fermā.

224
00:12:30,460 --> 00:12:34,350
Viņi ar pārmaiņām tiek galā vieglāk.

225
00:12:36,550 --> 00:12:38,500
Iepriekš viņi bija spiesti

226
00:12:38,520 --> 00:12:39,970
pirkt sēklas,

227
00:12:39,970 --> 00:12:42,450
taču, līdzko viņi apjauta -

228
00:12:42,470 --> 00:12:45,010
liktenis ir pašu rokās,

229
00:12:45,260 --> 00:12:47,100
jo vajadzēja vien

230
00:12:47,120 --> 00:12:50,730
īstos rīkus,

231
00:12:50,880 --> 00:12:53,890
jau pēc pāris sezonām bija saražotas sēklas,

232
00:12:53,930 --> 00:12:55,040
un mēs mijāmies.

233
00:12:55,080 --> 00:12:56,800
Mēs sadarbojamies ilgtermiņā -

234
00:12:56,820 --> 00:13:00,570
piemēram, kāds audzē vienu šķirni,

235
00:13:00,580 --> 00:13:03,410
bet cits audzē citu šķirni, un mēs - vēl trešo škirni…

236
00:13:03,423 --> 00:13:08,680
galvenais - iegūt maksimālu daudzveidību,

237
00:13:08,720 --> 00:13:10,680
un viss jau notiek.

238
00:13:11,760 --> 00:13:16,280
-Kā tu iesaistījies apvienības darbībā?

239
00:13:18,426 --> 00:13:20,280
Gluži nejauši.

240
00:13:20,330 --> 00:13:26,600
Dzīvoju Beirutā, un mans dzīvokļa biedrs mācījās kopā ar Ferdi -

241
00:13:26,693 --> 00:13:30,613
vienu no projekta un apvienības līdzdibinātājiem.

242
00:13:32,320 --> 00:13:35,253
Kad Ferdi un Lara atgriezās Libānā,

243
00:13:35,293 --> 00:13:38,253
lai mēģinātu iesākt sēklu projektu -

244
00:13:38,290 --> 00:13:41,270
kā ražot, kur to visu paveikt,

245
00:13:41,308 --> 00:13:43,228
kas īsti jāpaveic,

246
00:13:43,242 --> 00:13:44,927
viņi dzīvoja pie mums

247
00:13:44,936 --> 00:13:46,592
aptuveni mēnesi,

248
00:13:46,602 --> 00:13:48,545
un mēs tūdaļ sapratāmies, un tā viss sākās.

249
00:13:49,727 --> 00:13:51,303
Kā jau teicu -

250
00:13:51,336 --> 00:13:54,174
apzināti ļāvu sevi iesaistīt notiekošajā!

251
00:13:54,988 --> 00:13:56,898
-Tava dzīve mainījās?

252
00:13:57,068 --> 00:13:59,101
Ar ko nodarbojies iepriekš?

253
00:14:00,112 --> 00:14:03,303
Pirms tam strādāju komunikāciju jomā -

254
00:14:03,336 --> 00:14:04,823
universitātē.

255
00:14:04,851 --> 00:14:06,960
Vispār esmu bibliotekārs,

256
00:14:09,520 --> 00:14:12,640
studēju psiholoģiju, literatūru,

257
00:14:12,672 --> 00:14:15,868
ar sēklām nesaistīti!

258
00:14:19,745 --> 00:14:21,520
Taču tulkošana noder,

259
00:14:21,543 --> 00:14:22,922
tāpēc labi vien ir!

260
00:14:23,275 --> 00:14:24,992
-Un labprāt turpināsi?

261
00:14:25,920 --> 00:14:27,637
Jā!

262
00:14:27,661 --> 00:14:29,863
Kā minēju iepriekš -

263
00:14:29,890 --> 00:14:33,002
valda ārkārtas situācija,

264
00:14:34,164 --> 00:14:39,185
un mūsu darbs ir nozīmīgs,

265
00:14:40,014 --> 00:14:41,477
nepieciešams.

266
00:14:41,515 --> 00:14:47,298
Tas arī sniedz gandarījumu -

267
00:14:47,310 --> 00:14:50,555
sociālā un politiskā līmenī.

268
00:14:50,597 --> 00:14:53,050
Mūsu veikums ir nozīmīgs

269
00:14:53,072 --> 00:14:54,823
un tajā pašā laikā - skaists.

270
00:14:55,247 --> 00:14:58,211
Nedomāju mest mieru!

271
00:14:58,418 --> 00:15:01,797
-Lieliski! Paldies par interviju!

272
00:15:01,960 --> 00:15:03,722
Lūdzu, lūdzu!

273
00:15:03,755 --> 00:15:06,290
Bija jauki atkal aprunāties.

274
00:15:07,620 --> 00:15:08,936
-Ceru vēl tikties.

275
00:15:08,960 --> 00:15:11,134
Gaidīsim mūsu saimniecībā!

276
00:15:11,181 --> 00:15:15,811
-Labprāt :-) Nevaru gan apsolīt…

277
00:15:17,463 --> 00:15:20,560
Iepriekš paziņo, ja ieradīsies - kad vien vēlies,

278
00:15:20,583 --> 00:15:22,672
un ''ahla wa sahla'' (laipni lūgti)!

279
00:15:25,251 --> 00:15:26,409
-Sirsnīgs paldies!

280
00:15:26,432 --> 00:15:27,091
Jauki!

281
00:15:28,120 --> 00:15:35,720
Pateicamies Seržam Harfušam un ''Buzuruna Juzuruna'' komandai!
sīkāka informācija: @buzurunajuzuruna (Instagram)
fotoattēli: Šarlote Jubēra, ''Buzuruna Juzuruna'', Rabihs Jasīns
intervija & montāža: Ēriks D'ase

282
00:15:35,720 --> 00:15:40,320
www.DIYseeds.org
izglītojošas filmas par sēklu ražošanu
