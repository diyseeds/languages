﻿1
00:00:09,300 --> 00:00:14,146
Lauka pupas piederīgas 
Fabaceae dzimtai un Vicia faba sugai.

2
00:00:16,435 --> 00:00:21,824
Lauka pupas ir viengadīgi, dažādas krāsas un izmēra
sēklu ražai audzēti augi.

3
00:00:23,860 --> 00:00:26,232
No augiem tiek iegūti 
arī jaunie dzinumi.

4
00:00:28,620 --> 00:00:30,848
Pastāv vairāki lauka pupu veidi -

5
00:00:31,232 --> 00:00:42,080
tās tiek audzētas cilvēku patēriņam, 
kā arī lopbarībai. 

6
00:00:53,648 --> 00:00:54,712
Apputeksnēšana 

7
00:01:12,976 --> 00:01:17,880
Lauka pupu ziedi ir hermafrodīti 
un pašapputeksnējoši.

8
00:01:18,888 --> 00:01:24,616
Tas nozīmē - vīrišķie un sievišķie orgāni 
atrodami uz viena zieda un ir savietojami.

9
00:01:25,656 --> 00:01:27,744
Tādēļ augi ir autogāmi. 

10
00:01:31,120 --> 00:01:36,888
Tomēr pastāv kukaiņu ierosinātas
starpšķirņu krustošanās risks. 

11
00:01:43,440 --> 00:01:50,064
Krustošanās biežums (5% - 60%) 
atkarīgs no šķirnēm,

12
00:01:50,376 --> 00:01:53,736
apkārtējās vides 
un dabisku barjeru klātbūtnes. 

13
00:01:54,680 --> 00:02:00,496
Svešappute novēršama - 
dažādas šķirnes audzējot 1 km atstatumā. 

14
00:02:01,064 --> 00:02:04,312
Attālums samazināms 
līdz pāris simtiem metru,

15
00:02:04,624 --> 00:02:07,768
ja pastāv dabisks šķērslis (piemēram, dzīvžogs). 

16
00:02:12,616 --> 00:02:15,400
Šķirnes tīrības nodrošināšanai -

17
00:02:15,664 --> 00:02:18,856
sēklai paredzētos augus 
pārklājiet ar pretkukaiņu tīklu.

18
00:02:24,910 --> 00:02:29,192
Tīkls izvietojams - 
pirms iesākas ziedēšana. 

19
00:02:39,260 --> 00:02:41,128
Dzīves cikls 

20
00:02:57,824 --> 00:03:02,512
Sēklām audzētās lauka pupas 
tiek kultivētas tāpat kā pārtikas ieguvei. 

21
00:03:07,560 --> 00:03:10,144
Augam nepatīk paaugstināta temperatūra -

22
00:03:10,456 --> 00:03:13,728
nenotiek apputeksnēšana, 
mazinās ražība.

23
00:03:17,952 --> 00:03:23,928
Lauka pupas tiek sētas rudens beigās 
(maigā klimatā) vai ziemas beigās,

24
00:03:24,040 --> 00:03:25,816
kad zeme tam gatava. 

25
00:03:47,440 --> 00:03:49,775
Ģenētiskā daudzveidība nodrošināma -

26
00:03:50,192 --> 00:03:54,336
audzējot vismaz 10 sēklas stādus. 

27
00:04:17,856 --> 00:04:22,416
Izvēlieties augus - 
atbilstoši specifiskajiem šķirnes kritērijiem:

28
00:04:22,840 --> 00:04:26,520
auga izmēram, 
zieda krāsai,

29
00:04:29,080 --> 00:04:34,410
pākstu skaitam, sēklu skaitam pākstī,

30
00:04:35,104 --> 00:04:38,232
sēklu izmēram, krāsai un garšai. 

31
00:04:42,680 --> 00:04:46,416
Kamēr augi vēl attīstās - 
izraugieties skaistākos,

32
00:04:46,736 --> 00:04:50,104
veselīgākos, ražīgākos stādus,
no kuriem ievākt sēklas. 

33
00:04:51,448 --> 00:04:55,552
Viens no atlases kritērijiem 
ir arī ražas perioda ilgums. 

34
00:05:00,624 --> 00:05:04,240
Daļu no ražas 
rezervējiet sēklu ieguvei.

35
00:05:04,570 --> 00:05:08,080
Neievāciet pupas, 
kamēr visas nav pilnībā nobriedušas.

36
00:05:13,880 --> 00:05:16,624
Neievāciet pirmās pākstis 
pārtikas patēriņam,

37
00:05:17,050 --> 00:05:23,256
sēklai atstājot tikai pēdējās, 
jo sēklas no pirmajām pākstīm

38
00:05:23,576 --> 00:05:27,064
ļaus saglabāt šķirnes agrīnās īpašības. 

39
00:05:34,336 --> 00:05:35,630
Augsta mitruma apstākļos -

40
00:05:36,072 --> 00:05:38,592
ievāciet sēklas 
pirms pilnbrieda sasniegšanas

41
00:05:39,072 --> 00:05:42,832
un atstājiet kaltēties 
sausā, labi vēdinātā vietā.

42
00:05:52,600 --> 00:05:53,776
Visbiežāk -

43
00:05:53,880 --> 00:05:58,208
augi var žāvēties, 
līdz pākstis kļūst melnas. 

44
00:06:06,520 --> 00:06:11,776
Labākās sēklas atrodamas pirmā brieduma pākstīs - 
tuvāk auga pamatnei. 

45
00:06:13,570 --> 00:06:17,056
Lai pārliecinātos, vai sēklas jau sausas, 
viegli iekodieties.

46
00:06:19,680 --> 00:06:22,960
Ja nepaliek iespiedums - 
sēklas ir izžuvušas. 

47
00:06:33,080 --> 00:06:36,900
Ievākšana - šķirošana - uzglabāšana

48
00:06:46,400 --> 00:06:51,232
Pupas ievācamas - pāksti pēc pāksts 
vai izbraucot pupām cauri ar kombainīti.

49
00:06:52,010 --> 00:06:58,920
Izvietojiet ražu uz mīkstas zemes, 
lai neiebojātu sēklas. 

50
00:07:02,520 --> 00:07:06,976
Pupu šķirošanas procesā 
tiek izlasītas netipiskās sēklas.

51
00:07:07,456 --> 00:07:09,808
Tās radušās krustošanās ceļā.

52
00:07:12,256 --> 00:07:18,656
Tāpat izlasiet bojātas, deformētas
 un smecernieku apsēstas pupas. 

53
00:07:20,368 --> 00:07:26,000
Lauka pupu sēklas bieži kolonizē 
smecernieki (Bruchus rufinamus) -

54
00:07:26,648 --> 00:07:31,088
sīki kukaiņi, kas sadēj oliņas 
zem sēklu apvalkiem. 

55
00:07:32,704 --> 00:07:37,808
No oliņām viegli atbrīvoties - 
dažas dienas uzglabājot sēklas saldētavā. 

56
00:07:44,640 --> 00:07:48,160
Paciņā ievietojama etiķete 
ar sugas un šķirnes nosaukumu,

57
00:07:48,330 --> 00:07:53,720
kā arī ievākšanas gadu
(ārējs uzraksts var nodilt). 

58
00:08:00,224 --> 00:08:05,344
Lauka pupas dīgtspēju 
saglabā 5 - 10 gadus.

59
00:08:06,056 --> 00:08:09,808
Termiņš pagarināms, 
glabājot sēklas zemā temperatūrā. 
