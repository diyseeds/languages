﻿1
00:00:10,625 --> 00:00:14,247
Mangolds un bietes 
piederīgi Chenopodiaceae dzimtai

2
00:00:14,356 --> 00:00:17,229
un Beta vulgaris sugai.

3
00:00:22,021 --> 00:00:29,381
Tie ir divgadīgi augi, no kuriem 
iegūstamas saknes, kāti un lapas.

4
00:00:32,981 --> 00:00:35,134
Pastāv 5 apakšsugas:

5
00:00:41,003 --> 00:00:44,487
cicla ietver vairākas mangolda šķirnes, 

6
00:00:46,276 --> 00:00:50,189
esculenta apakšsugā ietilpst dārza bietes, 

7
00:00:52,945 --> 00:00:56,421
rapa - lopbarības biešu šķirnes, 

8
00:00:57,898 --> 00:01:01,723
altissima - cukurbiešu šķirnes 

9
00:01:03,149 --> 00:01:05,694
un maritima - savvaļas lapu biete,

10
00:01:05,869 --> 00:01:10,501
izplatīta no Atlantijas okeāna piekrastes 
Eiropā līdz pat Indijai. 

11
00:01:12,080 --> 00:01:17,374
Dažas biešu šķirnes ir agrīnas, 
bet citas tiek audzētas konservēšanai.

12
00:01:18,480 --> 00:01:23,112
Sakņu krāsa var atšķirties: 
sārta, sarkana,

13
00:01:23,447 --> 00:01:27,025
reizēm - dzeltena, gaišvioleta vai balta.

14
00:01:28,109 --> 00:01:35,301
Arī forma ir daudzveidīga - no apaļām 
līdz plakanām, garenām vai konusveida bietēm. 

15
00:01:41,680 --> 00:01:45,890
Mangoldu iespējams pazīt 
pēc baltajiem kātiem un zaļajām lapām,

16
00:01:46,850 --> 00:01:50,530
taču krāsa var būt dzeltena, rozā vai sarkana.

17
00:01:52,647 --> 00:01:58,800
Dažas šķirnes ir viscaur zaļas un nelielākas - 
līdzīgas savvaļas sugām. 

18
00:02:07,360 --> 00:02:08,500
Apputeksnēšana 

19
00:02:19,621 --> 00:02:23,178
Sīkie mangolda un biešu ziediņi 
ir hermafrodīti.

20
00:02:24,763 --> 00:02:28,080
Taču putekšņlapa 
(zieda vīrišķais vairošanās orgāns)

21
00:02:28,240 --> 00:02:34,298
atbrīvo putekšņus - pirms auglenīca 
(sievišķais orgāns) tos spēj pieņemt.

22
00:02:36,567 --> 00:02:38,749
Tādēļ ziedi ir pašsterili. 

23
00:02:42,487 --> 00:02:44,472
Mangolds un biete ir autogāmi augi -

24
00:02:44,840 --> 00:02:48,109
apaugļošanai nepieciešami 
vairāki stādi. 

25
00:02:53,723 --> 00:02:56,420
Parasti apputeksnēšanu veic vējš,

26
00:03:02,174 --> 00:03:03,789
taču daži kukaiņi

27
00:03:04,036 --> 00:03:09,912
(piemēram, divspārņi un blaktis) 
var piedalīties apputeksnēšanā. 

28
00:03:17,061 --> 00:03:20,436
Mangolds (un mangolda 
savvaļas apakšsugas), bietes,

29
00:03:20,850 --> 00:03:24,043
cukurbietes un lopu bietes var krustoties. 

30
00:03:29,170 --> 00:03:36,858
Lai novērstu krustošanos, starp divām
Beta vulgaris šķirnēm ievērojams 1 km atstatums.

31
00:03:38,625 --> 00:03:45,861
Attālums samazināms līdz 500 m, 
ja pastāv dabiska barjera (dzīvžogs). 

32
00:03:46,880 --> 00:03:51,600
Reģionos, kur biete tiek audzēta 
komerciālai sēklu ieguvei,

33
00:03:52,065 --> 00:03:55,432
ieteicams 7 km atstatums. 

34
00:03:57,156 --> 00:04:00,200
Divas šķirnes iespējams arī izolēt -

35
00:04:00,443 --> 00:04:03,690
tās pārklājot 
ar nošķirtiem kukaiņu tīkliem,

36
00:04:03,920 --> 00:04:06,414
kas tiek atvērti pamīšus.

37
00:04:06,836 --> 00:04:08,967
Ar tīklu sistēmu iepazīstieties

38
00:04:09,280 --> 00:04:15,534
‘’Sēklu ražošanas ābeces’’ 
izolēšanas metodēm veltītajā modulī. 

39
00:04:28,000 --> 00:04:29,301
Dzīves cikls 

40
00:04:37,541 --> 00:04:41,483
Sēklu ieguvei audzētais mangolds un bietes

41
00:04:41,796 --> 00:04:45,534
kultivējami tāpat 
kā pārtikai audzētie.

42
00:04:47,927 --> 00:04:51,243
Sēklas tiks ražotas tikai otrajā attīstības gadā.

43
00:05:29,960 --> 00:05:34,349
Katrs augs nobriedina 
lielu sēklu daudzumu,

44
00:05:34,698 --> 00:05:39,469
taču svarīgi paturēt vismaz duci augu, 
lai nodrošinātu arī ģenētisko daudzveidību. 

45
00:05:43,280 --> 00:05:48,989
Bietes parasti ievācamas rudenī - 
pirmā cikla gada noslēgumā.

46
00:05:52,021 --> 00:05:55,992
Izrokot dārzeņus, 
sēklu ieguvei atlasiet eksemplārus,

47
00:05:56,225 --> 00:05:59,316
kas atbilst tādiem 
šķirnes kritērijiem

48
00:05:59,660 --> 00:06:03,360
kā krāsa, apveids, auga izturība… 

49
00:06:06,334 --> 00:06:12,167
Notīriet saknes, tās noberžot - bez ūdens. 
Nogrieziet lapas - virs kakliņa.

50
00:06:18,120 --> 00:06:23,476
Glabājiet saknes smiltīm pildītā kastē, 
sargājot no sala un gaismas. 

51
00:06:24,763 --> 00:06:33,490
Piemērotākie uzglabāšanas apstākļi - 
pie 1°C temperatūras; mitrums: 90 - 95%.

52
00:06:39,090 --> 00:06:43,287
Ziemas gaitā aizvāciet 
visas iepuvušās saknes. 

53
00:06:50,770 --> 00:06:56,312
Skarbu ziemu 
un spēcīga sala reģionos -

54
00:06:56,625 --> 00:07:01,018
arī mangoldi izrokami 
un saknes rudenī uzglabājamas telpās.

55
00:07:02,043 --> 00:07:06,967
Nogrieziet lapas - līdz ar kakliņu, 
atstājot tikai vidusdaļā augošās. 

56
00:07:08,698 --> 00:07:12,167
Saudzīgi (netraumējot) - 
uzglabājiet saknes pagrabā,

57
00:07:12,581 --> 00:07:15,272
kastē ar viegli mitrinātām smiltīm. 

58
00:07:22,705 --> 00:07:24,472
Tomēr lielākoties

59
00:07:24,698 --> 00:07:29,941
atstājams ziemošanai dārzā, 
ja tam ir labi attīstīta sakņu sistēma.

60
00:07:32,130 --> 00:07:37,454
Aukstākos periodos - 
pretsala aizsardzībai pārklājiet salmu mulču. 

61
00:07:38,923 --> 00:07:42,901
Maigākā klimatā - 
arī bietes atstājamas augsnē,

62
00:07:43,287 --> 00:07:47,112
ja vien tās iesētas 
vēlu augustā vai septembrī.

63
00:07:53,723 --> 00:07:57,636
Pavasarī atlasāmas saknes - sēklu ražošanai.

64
00:07:58,800 --> 00:08:01,716
Atbīdiet zemi ap kakliņu, 
lai saknes pārbaudītu,

65
00:08:02,109 --> 00:08:07,592
un atbrīvojieties no nederīgām bietēm, 
kas neatbilst šķirnes kritērijiem.

66
00:08:08,910 --> 00:08:13,818
Tomēr tā nav labākā 
sēklaugu selekcijas metode. 

67
00:08:17,803 --> 00:08:24,007
Ieteicamāk - izrakt, atlasīt 
un tūdaļ pārstādīt. 

68
00:08:34,385 --> 00:08:39,020
Pārstādot pagrabā ziemojušo 
mangoldu un bietes,

69
00:08:39,621 --> 00:08:43,112
pārliecinieties, lai kakliņš 
atrastos zemes līmenī.

70
00:08:48,749 --> 00:08:50,705
Dāsni aplaistiet. 

71
00:09:11,505 --> 00:09:16,014
Turpmākie mangolda un biešu 
cikla posmi neatšķiras.

72
00:09:17,025 --> 00:09:20,872
Ziedkāti attīstīsies līdz pat 1,5 m augstumam.

73
00:09:21,890 --> 00:09:24,072
Nereti nepieciešams mietiņa atbalsts. 

74
00:09:26,007 --> 00:09:29,127
Reģionos, kur dienas un nakts garums

75
00:09:29,243 --> 00:09:33,505
īpaši neatšķiras,
sēklu ražošana būs sarežģīta

76
00:09:33,600 --> 00:09:36,320
vai pat neiespējama.

77
00:09:37,740 --> 00:09:40,625
Augļi nobriest garās vasaras dienās. 

78
00:09:46,487 --> 00:09:51,310
Nogrieziet ziedkātus - pēc rasas, 
kad pirmās sēklas jau nobriedušas.

79
00:09:55,621 --> 00:10:00,581
Sēklas veidos čemurus. 
Katrā čemurā: 2 - 6 atsevišķas sēklas. 

80
00:10:11,760 --> 00:10:18,043
Visus kātus var apgriezt vienlaicīgi, 
rēķinoties ar pirmā brieduma sēklu zudumiem. 

81
00:10:37,563 --> 00:10:41,818
Lai kā čemuri tikuši ievākti, 
turpmāk augi žāvējami apēnotā,

82
00:10:42,138 --> 00:10:44,530
sausā un labi vēdinātā vietā. 

83
00:10:57,800 --> 00:11:01,229
Ievākšana - šķirošana - uzglabāšana

84
00:11:05,090 --> 00:11:09,280
Ievāciet sēklas - 
saberzējot kātus starp plaukstām.

85
00:11:09,710 --> 00:11:11,774
Izmantojiet aizsargcimdus.

86
00:11:11,963 --> 00:11:16,807
Stiebrus iespējams arī mīdīt kājām 
vai izkult ar nūju. 

87
00:11:19,990 --> 00:11:24,865
Šķirojot - vispirms pelavas 
tiek atsijātas rupjā sietā.

88
00:11:27,070 --> 00:11:30,654
Tad sijājiet sēklas 
smalkākā sietā,

89
00:11:30,865 --> 00:11:32,610
ļaujot izbirt sīkākām pelavām. 

90
00:11:35,490 --> 00:11:40,596
Pēdīgi - izvētījiet sēklas no smalkām pelavām, 
pār tām pūšot elpu.

91
00:11:41,450 --> 00:11:44,254
Vai izmantojiet vēja iedarbību. 

92
00:11:47,250 --> 00:11:50,661
Maisiņā vienmēr ievietojiet 
etiķeti ar sugas un šķirnes nosaukumu,

93
00:11:50,814 --> 00:11:53,716
kā arī ievākšanas gadu.

94
00:11:54,270 --> 00:11:57,207
Ārējs uzraksts var nodilt.

95
00:12:03,854 --> 00:12:09,236
Dažas dienas uzglabājiet sēklas saldētavā, 
lai nonāvētu parazītus. 

96
00:12:11,941 --> 00:12:16,872
Mangolda un biešu sēklas 
dīgtspēju saglabā 6 gadus,

97
00:12:20,138 --> 00:12:22,036
dažkārt - pat 10 gadus.

98
00:12:24,770 --> 00:12:28,560
1 grams sēklu ievācams no aptuveni 50 čemuriem. 
