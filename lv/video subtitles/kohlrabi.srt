1
00:00:12,320 --> 00:00:18,327
Kolrābji piederīgi Brassicaceae dzimtai, 
Brassica oleracea sugai

2
00:00:20,400 --> 00:00:24,261
un caulorapa gongylodes apakšsugai.

3
00:00:25,949 --> 00:00:31,396
Brassica oleracea sugā 
ietilpst arī brokoļi, kāposti,

4
00:00:32,334 --> 00:00:37,469
Briseles kāposti, lapu kāposti, 
puķkāposti un virziņkāposti. 

5
00:00:42,749 --> 00:00:46,800
Kolrābis ir divgadīgs augs, 
no kura iegūst piebriedušo kātu.

6
00:00:52,550 --> 00:00:57,440
Augs neattīsta daudz lapu. 
Mīkstums saglabājas maigs.

7
00:01:08,167 --> 00:01:12,900
Dažādas šķirnes izšķir 
pēc izmēra un kāta krāsas.

8
00:01:16,880 --> 00:01:20,938
Sastopamas agrīnās 
un uzglabājamās šķirnes. 

9
00:01:28,494 --> 00:01:33,260
Oleracea kāpostu
apputeksnēšana 

10
00:01:49,185 --> 00:01:53,127
Brassica oleracea ziedi 
ir hermafrodīti.

11
00:01:56,021 --> 00:02:00,218
Tas nozīmē - tie apveltīti gan ar vīrišķajiem, 
gan sievišķajiem orgāniem.

12
00:02:03,934 --> 00:02:06,458
Vairums ir pašsterili -

13
00:02:07,040 --> 00:02:12,196
viena auga ziedi spēj apputeksnēt 
tikai citu augu.

14
00:02:15,898 --> 00:02:18,167
Tādi augi ir alogāmi augi.

15
00:02:19,047 --> 00:02:23,956
Veiksmīgai apputeksnēšanai 
ieteicams kultivēt vairākus augus. 

16
00:02:27,432 --> 00:02:30,574
Ziedputekšņus izplata kukaiņi.

17
00:02:33,876 --> 00:02:38,378
Tādējādi tiek dabiski veicināta 
lieliska ģenētiskā daudzveidība.

18
00:02:45,265 --> 00:02:49,621
Visas Brassica oleracea 
kāpostu apakšsugas

19
00:02:49,774 --> 00:02:51,330
var savstarpēji krustoties.

20
00:02:53,200 --> 00:02:57,789
Tādēļ dažādu šķirņu sēklas kāpostus 
nevajadzētu audzēt tiešā tuvumā. 

21
00:03:02,334 --> 00:03:08,087
Šķirnes tīrības nolūkos - 
dažādas Brassica oleracea šķirnes

22
00:03:08,378 --> 00:03:11,607
stādāmas vismaz 1 km atstatumā. 

23
00:03:12,920 --> 00:03:17,258
Attālums samazināms līdz 500 metriem, 
ja starp šķirnēm pastāv

24
00:03:17,330 --> 00:03:20,334
dabiska barjera
(piemēram, dzīvžogs). 

25
00:03:23,520 --> 00:03:25,992
Šķirnes iespējams arī izolēt -

26
00:03:26,312 --> 00:03:31,200
norobežotos kukaiņu tīklos ievietojot
nelielas kukaiņu ligzdiņas

27
00:03:36,283 --> 00:03:39,949
vai tīklus pamīšus 
atverot un aizverot.

28
00:03:44,360 --> 00:03:45,541
Sīkāk -

29
00:03:45,716 --> 00:03:49,770
izolēšanas metodēm veltītajā
‘’Sēklu ražošanas ābeces’’ modulī. 

30
00:04:03,374 --> 00:04:05,374
Kolrābja dzīves cikls 

31
00:04:19,083 --> 00:04:21,149
Pirmajā audzēšanas gadā -

32
00:04:21,483 --> 00:04:24,225
ziemā uzglabājamo sēklaugu šķirnes

33
00:04:24,596 --> 00:04:28,087
kultivējamas tāpat 
kā pārtikas patēriņam. 

34
00:04:29,381 --> 00:04:30,680
Pirmajā gadā

35
00:04:30,909 --> 00:04:37,061
veidosies piebrieduši kāti, kas jāieziemo, 
lai nākamajā gadā augs uzziedētu.

36
00:04:37,520 --> 00:04:43,876
Kāti, kas rudens beigās pārgatavojušies 
vai ieplaisājuši, var nepārziemot.

37
00:04:45,163 --> 00:04:48,660
Tādēļ vairākumā reģionu 
gan agrīnās, gan ieziemojamās

38
00:04:48,850 --> 00:04:52,116
šķirnes sējamas jūnijā vai jūlijā. 

39
00:05:50,276 --> 00:05:56,130
Ģenētisko daudzveidību nodrošinās 
30 rudenī ievākti kolrābji,

40
00:05:56,421 --> 00:06:00,160
no kuriem pavasarī 
iegūstami 10 - 15 stādi.

41
00:06:02,021 --> 00:06:05,869
Sēklu ieguvei atlasāmi 
veselīgi augi,

42
00:06:06,101 --> 00:06:09,069
kas novēroti visos 
attīstības posmos,

43
00:06:09,330 --> 00:06:12,894
lai sekotu šķirnes īpašībām:

44
00:06:14,647 --> 00:06:18,945
kāta kvalitātei, dzīvelīgumam, 
agrīnībai,

45
00:06:19,600 --> 00:06:24,414
noturībai pret slimībām, 
uzglabāšanas kapacitātei,

46
00:06:24,960 --> 00:06:27,760
agrīnumam, aukstumizturībai. 

47
00:06:39,080 --> 00:06:43,483
Sēklu ieguvei izraudzītie augi 
glabājami pagrabā.

48
00:06:44,276 --> 00:06:48,021
Nogrieziet sānu lapas, 
bet atstājiet centrālās lapas. 

49
00:06:54,509 --> 00:06:58,312
Kolrābjus ievieto smiltīs 
vai konteineros.

50
00:07:01,207 --> 00:07:06,029
Ziemas laikā kolrābji 
iepūst retāk nekā kāposti. 

51
00:07:12,167 --> 00:07:14,190
Maiga klimata reģionos -

52
00:07:14,545 --> 00:07:21,592
kolrābji var pārziemot augsnē, 
izturot temperatūras līdz pat -7°C.

53
00:07:23,127 --> 00:07:27,454
Labākas ir sausas ziemas, 
kā arī kāti ar zemāku mitruma sastāvu,

54
00:07:27,934 --> 00:07:30,690
jo tiem raksturīga 
izteiktāka sala izturība. 

55
00:07:40,487 --> 00:07:44,436
Pavasarī, kad vairs nedraud bargs sals,

56
00:07:45,156 --> 00:07:48,180
kolrābjus izņem 
no uzglabāšanas vietas

57
00:07:48,400 --> 00:07:52,858
un pārstāda zemē - 
augsnē ierokot ⅔ no stāda. 

58
00:08:02,560 --> 00:08:06,458
Augi izdzīs ziedkātus 
un atplauks ziedos. 

59
00:08:10,110 --> 00:08:11,694
Lai ziedkāti nesagāztos,

60
00:08:11,810 --> 00:08:15,483
dažkārt tie balstāmi 
ar mietiņiem. 

61
00:08:47,301 --> 00:08:54,232
Brassica oleracea kāpostu
ievākšana, šķirošana un uzglabāšana 

62
00:09:03,127 --> 00:09:06,974
Sēklas ir gatavas - 
pākstīm vēršoties smilškrāsā.

63
00:09:11,170 --> 00:09:13,470
Pākstis viegli pārsprāgst.

64
00:09:13,869 --> 00:09:18,952
Tas nozīmē - nobriedušas pākstis 
viegli atveras un strauji izplata sēklas. 

65
00:09:27,469 --> 00:09:31,476
Lielākoties visi stiebri 
nenobriest vienlaicīgi.

66
00:09:32,440 --> 00:09:36,647
Lai nezaudētu sēklas, 
tās ievācamas atsevišķi -

67
00:09:36,741 --> 00:09:38,690
no katra nobriedušā stiebra.

68
00:09:40,334 --> 00:09:46,007
Iekams nogatavojušās visas sēklas - 
var ievākt arī augu kopumā. 

69
00:09:49,360 --> 00:09:54,509
Briedināšana turpināma - 
žāvējot augu sausā,

70
00:09:54,640 --> 00:09:56,545
labi vēdinātā vietā. 

71
00:10:08,923 --> 00:10:15,040
Kāpostu sēklas ievācamas, 
kad pākstis viegli atveramas ar pirkstiem. 

72
00:10:16,960 --> 00:10:18,341
Lai ievāktu sēklas -

73
00:10:18,480 --> 00:10:23,847
pākstis tiek izbērtas uz plēves 
vai bieza auduma gabala.

74
00:10:24,276 --> 00:10:27,098
Tad pākstis tiek izkultas 
vai saberzētas plaukstās.

75
00:10:30,596 --> 00:10:35,665
Pākstis var arī ievietot maisiņā - 
un izkult pret mīkstu virsmu. 

76
00:10:37,080 --> 00:10:41,774
Lielāki apjomi kuļami - 
mīdot vai pārbraucot pāri. 

77
00:10:54,280 --> 00:10:59,672
Ja pākstis neatveras viegli - 
tajās slēpjas nenobriedušas sēklas,

78
00:10:59,818 --> 00:11:01,614
kam būs raksturīga vāja dīgtspēja. 

79
00:11:06,160 --> 00:11:08,820
Šķirošanas gaitā -

80
00:11:09,003 --> 00:11:12,392
sēklas tiek sijātas
rupjākā sietā,

81
00:11:12,749 --> 00:11:14,218
kas aiztur pelavas.

82
00:11:19,185 --> 00:11:21,861
Tad sēklas tiek sijātas citā sietā,

83
00:11:22,203 --> 00:11:26,720
kas aiztur sēklas, 
bet atsijā smalkākās daļiņas. 

84
00:11:30,509 --> 00:11:34,261
Pēdīgi - sēklas vētījamas, 
pūšot pār tām

85
00:11:34,698 --> 00:11:38,734
vai izmantojot vēja spēku, 
lai aizvāktu pelavu paliekas. 

86
00:11:53,920 --> 00:11:58,850
Visu Brassica oleracea kāpostu sēklas 
ir savstarpēji līdzīgas.

87
00:11:59,854 --> 00:12:03,760
Grūti atšķirt, piemēram,

88
00:12:04,160 --> 00:12:06,720
kāpostu un puķkāpostu sēklas.

89
00:12:07,098 --> 00:12:10,000
Tādēļ svarīgi augus iezīmēt

90
00:12:10,254 --> 00:12:14,094
un iegūtajām sēklām pievienot
etiķeti ar sugas un šķirnes

91
00:12:14,280 --> 00:12:17,447
nosaukumu, kā arī ievākšanas gadu. 

92
00:12:18,756 --> 00:12:23,534
Dažas dienas uzglabājot sēklas saldētavā, 
tiks iznīcināti parazīti. 

93
00:12:28,960 --> 00:12:32,167
Kāpostu sēklu dīgtspēja saglabājas 5 gadus.

94
00:12:33,156 --> 00:12:36,814
Taču dīgšanas kapacitāte 
var ieilgt līdz pat 10 gadiem.

95
00:12:38,640 --> 00:12:41,934
Termiņš pagarināms - 
glabājot sēklas saldētavā.

96
00:12:42,916 --> 00:12:49,294
Vienā gramā ietilpst 250 - 300 sēklas 
(atbilstoši šķirnes īpašībām). 
