1
00:00:10,756 --> 00:00:15,780
Puravi piederīgi Amarylidaceae dzimtai, 
kas savulaik dēvēta - Alliaceae.

2
00:00:16,320 --> 00:00:19,614
Tie ietilpst 
Allium ampeloprasum sugā.

3
00:00:20,970 --> 00:00:27,250
Puravs ir divgadīgs augs, no kura iegūst 
garo, balto kātu, kas attīstās pirmajā gadā.

4
00:00:31,160 --> 00:00:33,170
Pastāv vairākas puravu šķirnes.

5
00:00:35,818 --> 00:00:39,454
Lapas mēdz būt zaļas, zilas vai pat violetas.

6
00:00:44,210 --> 00:00:48,480
Dažas šķirnes audzējamas agri sezonā, 
citas ir vēlīnas.

7
00:00:53,250 --> 00:00:56,458
Sastopamas arī 
salcietīgas puravu šķirnes.

8
00:00:57,389 --> 00:00:59,861
Citas šķirnes pārziemo vāji. 

9
00:01:12,440 --> 00:01:13,680
Apputeksnēšana 

10
00:01:24,669 --> 00:01:29,621
Puravu ziedu galviņas veido 
sīki, hermafrodīti ziediņi,

11
00:01:31,752 --> 00:01:33,970
kas ir pašsterili.

12
00:01:39,047 --> 00:01:41,781
Apaugļošanai nepieciešami kukaiņi.

13
00:01:47,330 --> 00:01:50,065
Tas nozīmē - puravi ir alogāmi augi. 

14
00:01:54,203 --> 00:01:59,105
Kukaiņi var izraisīt 
dažādu puravu šķirņu krustošanos

15
00:01:59,389 --> 00:02:00,792
vai dārza un savvaļas puravu krustošanos.

16
00:02:06,894 --> 00:02:13,854
Taču puravi nekrustojas ar sīpoliem, 
maurlokiem vai melnajiem ķiplokiem. 

17
00:02:18,843 --> 00:02:23,730
Lai nepieļautu krustošanos, 
starp divām šķirnēm ievērojams

18
00:02:23,840 --> 00:02:26,087
vismaz 1 km atstatums. 

19
00:02:29,374 --> 00:02:35,396
Attālums samazināms līdz 400 m, 
ja pastāv dabiska barjera, piemēram, dzīvžogs. 

20
00:02:40,145 --> 00:02:44,669
Šķirnes iespējams izolēt 
arī kukaiņu tīklu sistēmā.

21
00:02:44,909 --> 00:02:49,840
Pārsedziet vienas šķirnes augus ar pastāvīgi 
pārklātu tīklu, kurā ievietota kameņu ligzda.

22
00:02:51,352 --> 00:02:54,283
Vai izmantojiet nošķirtus tīklus 
(katru savai šķirnei),

23
00:02:54,620 --> 00:03:00,356
kas tiek atvērti pamīšus: vienai šķirnei - 
pirmajā dienā, bet otrai šķirnei - nākamajā dienā. 

24
00:03:01,640 --> 00:03:04,130
Ar izolēšanas metodēm sīkāk iepazīstieties

25
00:03:04,280 --> 00:03:07,280
izolēšanas metodēm veltītajā
''Sēklu ražošanas ābeces''

26
00:03:07,643 --> 00:03:09,963
modulī. 

27
00:03:15,156 --> 00:03:16,247
Dzīves cikls 

28
00:03:28,807 --> 00:03:33,934
Pirmajā gadā sēklu puravi tiek kultivēti 
tāpat kā pārtikai audzētie.

29
00:03:38,945 --> 00:03:41,621
Sēklas tiks ražotas otrajā gadā. 

30
00:04:33,578 --> 00:04:37,025
Atbrīvojieties no pirmajā gadā 
uzziedējušiem puraviem.

31
00:04:40,130 --> 00:04:44,341
No to sēklām izdīgušie augi 
arīdzan attīstīs pāragrus ziedus. 

32
00:04:48,120 --> 00:04:51,585
Sēklu puravus iespējams uzglabāt 
vairākos veidos. 

33
00:04:59,178 --> 00:05:04,298
Aukstāku ziemu reģionos - 
pirms sala izrociet augus ar saknēm.

34
00:05:14,240 --> 00:05:17,381
Sēklu ieguvei atlasiet augus,

35
00:05:17,505 --> 00:05:20,887
kas atbilst šķirnes 
specifiskajiem kritērijiem:

36
00:05:21,310 --> 00:05:26,480
agrīnība, 
kāta izmērs un biezums,

37
00:05:27,090 --> 00:05:32,087
lapu krāsa, salcietība 
un pretošanās slimībām.

38
00:05:33,883 --> 00:05:38,407
Atlasiet 30 augus, 
jo daži varētu nepārziemot.

39
00:05:44,632 --> 00:05:49,607
Ievietojiet puravus tvertnē - vēsā telpā, 
kas pasargāta no sala ietekmes.

40
00:05:51,040 --> 00:05:55,730
Ģenētisko daudzveidību 
nodrošinās 20 sēklaugi.

41
00:06:00,000 --> 00:06:05,505
Daži vasaras puravi pārziemos veiksmīgāk, 
ja rīkosieties sekojoši. 

42
00:06:08,967 --> 00:06:12,130
Vienkāršākā metode:
ja laikapstākļi ir gana silti,

43
00:06:12,414 --> 00:06:14,821
atstājiet puravus augsnē, dārzā. 

44
00:06:20,727 --> 00:06:26,040
Pavasara beigās pārstādiet 
telpās ieziemotos puravus.

45
00:06:54,080 --> 00:06:57,890
Apgriežot lapas, 
augs attīstīsies veiksmīgāk. 

46
00:07:36,960 --> 00:07:40,980
Ziedkāti sasniegs 
vismaz 1,50 m garumu.

47
00:07:56,894 --> 00:07:59,163
Tie balstāmi ar mietiņiem. 

48
00:08:18,741 --> 00:08:22,298
Četru nedēļu laikā uzziedēs 
katrs no puravu

49
00:08:22,421 --> 00:08:24,923
zieda galviņas atsevišķajiem ziediņiem.

50
00:08:27,970 --> 00:08:33,570
Periods no ziedēšanas sākuma 
līdz sēklu briedumam mēdz būt visai ilgs. 

51
00:08:37,960 --> 00:08:43,221
Sēklas ir gatavas, kad pākstis izkaltušas - 
un tajās redzamas melnas sēkliņas. 

52
00:08:49,970 --> 00:08:54,967
Lai sēkliņas ievāktu - nogrieziet ziedgalvu 
kopā ar kāta augšdaļu.

53
00:08:55,512 --> 00:09:01,607
Ievietojiet tos auduma maisiņā 
un atstājiet žāvēties - labi vēdinātā, siltā vietā. 

54
00:09:03,716 --> 00:09:09,170
Aukstos un mitros reģionos - vējš vai lietus 
var izraisīt nobriedušo sēklu zudumus,

55
00:09:10,036 --> 00:09:15,418
tādēļ ievāciet augus, kamēr tie vēl gatavojas, 
lai augi turpinātu nobriest sausā vietā. 

56
00:09:28,436 --> 00:09:31,660
Ievākšana - šķirošana - uzglabāšana 

57
00:09:36,727 --> 00:09:39,607
Puravu sēklu ievākšana, 
šķirošana un uzglabāšana

58
00:09:39,767 --> 00:09:41,941
līdzinās sīpolu sēklu apstrādei. 

59
00:09:55,534 --> 00:09:58,310
Vispirms - saberzējiet 
ziedgalviņas plaukstās.

60
00:09:58,443 --> 00:10:00,589
Tad saspaidiet tās ar mīklas veltni. 

61
00:10:05,781 --> 00:10:11,396
Lai atdalītu pielipušās sēklas - 
dažas stundas paturiet tās ledusskapī.

62
00:10:11,789 --> 00:10:13,272
Tad sēklas izbirs vieglāk. 

63
00:10:20,920 --> 00:10:26,225
Šķirošanai izmantojiet smalku sietu, 
kas aizturēs sēklas, bet ne putekļus. 

64
00:10:42,550 --> 00:10:46,298
Visbeidzot - vētījiet sēklas, 
lai atbrīvotos no sīkiem krikumiem.

65
00:10:47,556 --> 00:10:52,843
Novietojiet sēklas vējā, pūtiet pār tām elpu 
vai ieslēdziet nelielu ventilatoru. 

66
00:10:57,018 --> 00:11:00,320
Izvētītās sēklas 
beriet aukstā ūdenī, apmaisiet.

67
00:11:05,381 --> 00:11:08,880
Auglīgās sēklas 
būs smagākas un nogrims.

68
00:11:11,905 --> 00:11:15,054
Aizvāciet tukšās sēklas 
un uzpeldējušos krikumus.

69
00:11:22,814 --> 00:11:24,676
Auglīgās sēklas beriet uz šķīvja.

70
00:11:27,570 --> 00:11:30,574
Kad tās nožuvušas - 
sēklas plūdīs līdzīgi smiltīm. 

71
00:11:38,530 --> 00:11:41,752
Paciņā vienmēr ievietojiet etiķeti 
ar sugas un šķirnes

72
00:11:41,912 --> 00:11:45,200
nosaukumu, kā arī ievākšanas gadu.

73
00:11:45,607 --> 00:11:47,800
Ārējs uzraksts mēdz norīvēties. 

74
00:11:51,120 --> 00:11:55,527
Dažas dienas uzglabājiet sēklas saldētavā, 
lai nonāvētu parazītus. 

75
00:12:02,523 --> 00:12:06,487
Puravu sēklas 
dīgtspēju saglabā 2 gadus.

76
00:12:07,250 --> 00:12:10,240
Dažkārt termiņš ieilgst 
līdz pat 6 gadiem.

77
00:12:13,180 --> 00:12:16,647
Sēklas strauji zaudē 
dīgšanas potenciālu.

78
00:12:17,140 --> 00:12:20,465
Lai termiņu pagarinātu, 
glabājiet sēklas saldētavā. 
