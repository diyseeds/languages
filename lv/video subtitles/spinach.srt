1
00:00:07,883 --> 00:00:14,676
Spināti piederīgi Chenopodiaceae dzimtai 
un Spinacia oleracea sugai.

2
00:00:19,316 --> 00:00:24,123
Tie ir viengadīgi vai divgadīgi augi, 
no kuriem iegūst lapas.

3
00:00:27,750 --> 00:00:34,890
Spinātiem raksturīgas ziemas, 
pavasara un vasaras šķirnes.

4
00:00:39,396 --> 00:00:40,661
Apputeksnēšana 

5
00:00:47,636 --> 00:00:54,334
Spināti ir divmāju augi. Tas nozīmē - 
vīrišķie stādi izplata putekšņus,

6
00:00:56,749 --> 00:01:00,240
bet sievišķie augi 
briedina auglīgās sēklas. 

7
00:01:02,560 --> 00:01:08,305
Sievišķie ziedi ir neuzkrītoši. 
Tie atrodas zaru žāklēs. 

8
00:01:09,403 --> 00:01:15,847
Vīrišķie ziedi atrodami auga galotnē - 
tie atplaukst pirms sievišķajiem ziediem. 

9
00:01:19,840 --> 00:01:21,694
Spināti ir alogāmi -

10
00:01:22,152 --> 00:01:26,305
atsevišķi augoši stādi 
apputeksnē viens otru.

11
00:01:28,080 --> 00:01:29,832
Apputeksnēšanu veic vējš. 

12
00:01:34,680 --> 00:01:36,836
Tas ir ''dienasgaismas'' augs -

13
00:01:37,061 --> 00:01:43,963
augs veidos ziedus, dienasgaismas ilgumam 
sasniedzot 10 - 14 stundas.

14
00:01:50,000 --> 00:01:52,349
Spināti zied 2 - 3 nedēļas. 

15
00:01:55,869 --> 00:02:02,581
Lai novērstu krustošanos, 
divas šķirnes audzējiet 1 km atstatumā. 

16
00:02:05,680 --> 00:02:12,247
Attālums samazināms līdz 500 m, 
ja pastāv dabiska barjera, piemēram, dzīvžogs. 

17
00:02:14,989 --> 00:02:20,356
Lai dārzā kultivētu vairākas spinātu šķirnes, 
pielietojamas sekojošas metodes.

18
00:02:21,360 --> 00:02:25,447
Iespējams šķirnes
iesēt dažādos laikos,

19
00:02:25,800 --> 00:02:29,550
lai nodrošinātu 
nevienlaicīgu ziedēšanu,

20
00:02:30,247 --> 00:02:35,025
taču visi spināti spētu pilnībā attīstīties -
no dīgšanas līdz sēklu gatavībai. 

21
00:02:37,320 --> 00:02:40,443
Mehāniskās izolēšanas metode 
paredz katras šķirnes

22
00:02:40,814 --> 00:02:45,345
pārklāšanu ar kukaiņu tīkliem,
kas tiek pamīšus atvērti.

23
00:02:45,949 --> 00:02:47,934
Ar izolēšanas metodēm

24
00:02:48,058 --> 00:02:53,040
iepazīstieties atbilstošajā
 ‘’Sēklu ražošanas ābeces’’ modulī. 

25
00:03:01,530 --> 00:03:02,894
Dzīves cikls 

26
00:03:13,789 --> 00:03:15,498
Audzējot spinātus sēklai,

27
00:03:15,585 --> 00:03:19,243
kultivēšanas metodes 
saskaņojamas ar šķirni: 

28
00:03:19,752 --> 00:03:23,483
Pavasara šķirnes sēj agri - 
sezonas sākumā.

29
00:03:24,014 --> 00:03:27,934
Augi ziedēs un ražos sēklas 
jau tā paša gada vasarā.

30
00:03:28,138 --> 00:03:32,523
Pavasara šķirnes nevar audzēt ziemā - 
tās nepārcietīs aukstumu. 

31
00:03:33,440 --> 00:03:36,370
Ziemas šķirnes tiek sētas rudenī.

32
00:03:36,778 --> 00:03:38,220
Tās attīstās ziemā,

33
00:03:38,472 --> 00:03:42,080
bet ziedēšana un sēklu ražošana 
norisinās nākamā gada pavasarī. 

34
00:03:56,560 --> 00:04:02,741
Ģenētisko daudzveidību 
nodrošinās 25 - 30 augi.

35
00:04:21,150 --> 00:04:26,836
Izvēlieties veselīgus augus, kas atbilst 
šķirnes specifiskajiem rādītājiem.

36
00:04:34,189 --> 00:04:38,654
Ziemas sugu kritēriji:
salcietība

37
00:04:39,570 --> 00:04:45,563
un, svarīgākais, sakņu spēja izdzīvot
ierobežotas apskābekļotības vidē (nenodzeltēt).

38
00:04:45,760 --> 00:04:48,305
Ziemā asfiksija novērojama bieži.

39
00:04:52,247 --> 00:04:56,770
No sēklai audzētajiem spinātiem 
neievāciet pārāk daudz pārtikas lapu. 

40
00:05:19,949 --> 00:05:27,105
Spinātu ziedošie izaugumi var sasniegt 
pat 80 cm garumu, taču tie nav jāstutē.

41
00:05:31,076 --> 00:05:33,730
Vīrišķie augi izkaltīs pirmie.

42
00:05:33,960 --> 00:05:35,861
Tos ieteicams izrakt. 

43
00:05:46,880 --> 00:05:51,396
Sievišķos ziedus pazīsiet 
pēc gaišā smilškrāsas toņa. 

44
00:06:08,007 --> 00:06:12,501
Kad sēklas nobriedušas - 
pēc rasas nogrieziet ziedkātus. 

45
00:06:21,270 --> 00:06:26,940
Žāvēšanas process turpināms 
sausā un labi vēdinātā vietā. 

46
00:06:47,040 --> 00:06:49,660
Ievākšana - šķirošana - uzglabāšana 

47
00:06:55,207 --> 00:06:58,901
Sēklas izbirs, ja paberzēsiet 
kātus starp plaukstām.

48
00:06:59,738 --> 00:07:01,340
Uzvelciet cimdus! 

49
00:07:04,952 --> 00:07:08,349
Stiebrus iespējams mīdīt kājām 
vai izkult ar nūju. 

50
00:07:15,960 --> 00:07:20,160
Izsijājiet sēklas rupjā sietā, 
kas aizturēs pelavas.

51
00:07:31,541 --> 00:07:36,490
Tad ņemiet smalku sietu, 
kas aizturēs sēklas, bet izsijās putekļus. 

52
00:07:54,232 --> 00:07:56,060
Visbeidzot - sēklas vētījamas.

53
00:07:56,625 --> 00:08:01,389
Pūtiet pār tām elpu, lai atbrīvotos 
no pēdīgajām pelavām, vai atstājiet tās vējā. 

54
00:08:19,236 --> 00:08:24,780
Iepakojumā vienmēr ievietojiet etiķeti ar sugas
un šķirnes nosaukumu, kā arī ievākšanas gadu.

55
00:08:25,069 --> 00:08:27,221
Ārējs uzraksts mēdz nodilt. 

56
00:08:34,520 --> 00:08:38,916
Dažas dienas uzglabājiet sēklas saldētavā, 
lai nonāvētu parazītus. 

57
00:08:42,090 --> 00:08:48,480
Spinātu sēklas dīgtspēju saglabā 5 gadus, 
dažkārt - pat 7 gadus.

58
00:08:49,650 --> 00:08:52,552
Lai termiņu pagarinātu, 
glabājiet sēklas saldētavā.

59
00:08:54,167 --> 00:08:57,098
Vienā gramā ietilpst 
aptuveni 100 sēklas. 
