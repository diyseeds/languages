1
00:00:13,380 --> 00:00:16,340
Kā atlasīt sēklas - sēklu ražošanai?

2
00:00:23,640 --> 00:00:26,580
Svarīgi noskaidrot 
to šķirņu izcelsmi,

3
00:00:26,640 --> 00:00:29,050
kuras grasāties audzēt savā dārziņā.

4
00:00:35,540 --> 00:00:39,410
Izvēloties sēklu ražošanai 
paredzētās šķirnes,

5
00:00:39,760 --> 00:00:44,070
ieteicams izvairīties 
no lielās sēklu industrijas selekcionētajām šķirnēm.

6
00:00:44,870 --> 00:00:50,550
Neizmantojiet F1 vai F2 hibrīdsēklas, ĢMO

7
00:00:50,840 --> 00:00:53,840
vai biotehnoloģiju ceļā ražotās sēklas. 

8
00:01:00,070 --> 00:01:07,540
No F1 un F2 hibrīdsēklām audzētie augi 
nevairosies identiskā veidā.

9
00:01:08,430 --> 00:01:14,435
Sēklas var izrādīties sterilas 
vai radīt augus ar neparedzamām īpašībām.

10
00:01:23,935 --> 00:01:26,295
Pielietojot bloķēšanas paņēmienus -

11
00:01:26,825 --> 00:01:31,550
multinacionālie sēklu ražotāji 
neļauj dārzkopjiem ražot savas sēklas

12
00:01:31,930 --> 00:01:34,975
jo lielie ražotāji vēlas saglabāt tirgus monopolu. 

13
00:01:38,995 --> 00:01:43,545
Minētie tehniskie mehānismi 
uzspiež juridiskas prasības,

14
00:01:43,745 --> 00:01:46,630
aizliedzot brīvu sēklu pavairošanu.

15
00:01:49,390 --> 00:01:54,245
Šķirnes pakļautas 
tādām intelektuālā īpašuma tiesībām

16
00:01:54,945 --> 00:01:58,230
kā augu īpašuma tiesības un patenti. 

17
00:02:03,300 --> 00:02:05,910
Sēklu ražošanas "izejsēklas"

18
00:02:06,485 --> 00:02:10,425
iegūstamas 
no dabiski apputeksnētām šķirnēm.

19
00:02:10,780 --> 00:02:17,065
Vēlams - no bioloģiskām, biodinamiskām 
vai agroekoloģiskām sistēmām. 

20
00:02:38,575 --> 00:02:40,640
Daba ir dāsna.

21
00:02:40,855 --> 00:02:46,165
Parasti iegūsiet daudz vairāk sēklu - 
nekā nepieciešams pašu lietošanai.

22
00:02:57,520 --> 00:03:01,480
Pāri palikušās sēklas var dāvināt citiem vai iemainīt. 
