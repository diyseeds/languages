﻿1
00:00:08,283 --> 00:00:11,140
Selerijas piederīgas Apiaceae dzimtai

2
00:00:11,576 --> 00:00:15,168
un Apium graveolens sugai.

3
00:00:16,680 --> 00:00:22,100
Selerija ir divgadīgs augs, ko kultivē 
lapu, sakņu un kātu ieguvei.

4
00:00:22,980 --> 00:00:28,072
Tiek audzētas trīs apakšsugas:
salātu selerija (var. dulce),

5
00:00:34,220 --> 00:00:38,036
sakņu selerija (var. rapaceum)

6
00:00:42,916 --> 00:00:45,643
un lapu selerija (var. secalinum).

7
00:00:51,178 --> 00:00:54,516
Sastopama arī savvaļas selerija. 

8
00:01:01,340 --> 00:01:02,340
Apputeksnēšana 

9
00:01:15,170 --> 00:01:20,174
Selerijas ziedkopu veido sīku, 
parasti - hermafrodītu ziediņu

10
00:01:20,349 --> 00:01:22,523
saliktais čemurs.

11
00:01:23,105 --> 00:01:27,949
Putekšņlapa (vīrišķais vairošanās orgāns) 
nobriest pirms auglenīcas

12
00:01:28,160 --> 00:01:29,992
(sievišķā vairošanās orgāna). 

13
00:01:35,920 --> 00:01:41,163
Tādēļ zieds nespēj pašapaugļoties.

14
00:01:42,240 --> 00:01:45,905
Ziedi neatplaukst vienlaikus,

15
00:01:46,138 --> 00:01:50,392
tādēļ pašapputeksnēšana iespējama -
viena čemura

16
00:01:51,018 --> 00:01:54,610
vai viena auga divu čemuru ietvaros. 

17
00:01:57,560 --> 00:02:02,254
Viens otru var apputeksnēt 
arī atsevišķi stādi. 

18
00:02:05,592 --> 00:02:09,287
Svešapputi, visbiežāk, 
īsteno kukaiņi.

19
00:02:12,480 --> 00:02:16,100
Ziedoša selerija spēcīgi smaržo

20
00:02:16,232 --> 00:02:21,032
un saražo lielus nektāra apjomus, 
piesaistot vairākas kukaiņu sugas. 

21
00:02:22,152 --> 00:02:25,280
Visas selerijas spēj savstarpēji krustoties.

22
00:02:25,600 --> 00:02:29,520
Selerijas krustojas arī ar 
piekrastes reģionu savvaļas seleriju.

23
00:02:29,723 --> 00:02:33,214
Retos gadījumos - tās krustojas ar pētersīļiem. 

24
00:02:35,585 --> 00:02:37,440
Lai novērstu krustošanos,

25
00:02:37,847 --> 00:02:43,032
divas dažādas seleriju šķirnes 
audzējamas 1 km atstatumā. 

26
00:02:45,840 --> 00:02:50,603
Attālums samazināms līdz 500 m, 
ja starp šķirnēm pastāv

27
00:02:50,756 --> 00:02:54,140
dabiska barjera
(piemēram, dzīvžogs). 

28
00:03:02,770 --> 00:03:09,221
Šķirnes iespējams izolēt - 
pamīšus atverot un aizverot kukaiņu tīklus

29
00:03:10,021 --> 00:03:15,025
vai ievietojot nelielas kukaiņu ligzdas 
noslēgtā tīkla sistēmā.

30
00:03:16,647 --> 00:03:17,730
(Sīkāk -

31
00:03:17,840 --> 00:03:22,305
izolēšanas metodēm veltītajā
‘’Sēklu ražošanas ābeces’’ modulī.) 

32
00:03:27,818 --> 00:03:29,220
Dzīves cikls

33
00:03:40,494 --> 00:03:42,909
Visas seleriju šķirnes ir divgadīgas.

34
00:03:43,498 --> 00:03:45,330
Pirmajā audzēšanas gadā -

35
00:03:45,530 --> 00:03:49,898
sēklas selerija tiek kultivēta
 tāpat kā pārtikas ieguvei.

36
00:03:52,290 --> 00:03:54,727
Sēklas attīstīsies otrajā gadā. 

37
00:04:50,843 --> 00:04:55,890
Seleriju ieziemošana sēklu ieguvei 
iespējama vairākos veidos.

38
00:05:00,203 --> 00:05:04,778
Maiga klimata apstākļos - 
atstājiet selerijas ziemot dārza dobē.

39
00:05:06,276 --> 00:05:11,083
Tās nepieciešams pārsegt 
ar pretsala klājumu vai salmiem.

40
00:05:11,810 --> 00:05:14,203
Pavasarī salmi aizvācami. 

41
00:05:18,160 --> 00:05:21,470
Aukstā klimatā - 
selerijas izceļamas ar saknēm

42
00:05:21,600 --> 00:05:24,160
(pirms iestājas bargāks sals).

43
00:05:30,029 --> 00:05:33,258
Lapas nogrieziet 
dažus centimetrus virs kakliņa.

44
00:05:34,430 --> 00:05:38,334
Jo saknes sausākas, 
jo ilgāk tās uzglabājamas. 

45
00:05:42,290 --> 00:05:44,540
Visi seleriju tipi atlasāmi -

46
00:05:44,640 --> 00:05:48,952
saskaņā ar šķirnes 
specifiskajiem kritērijiem.

47
00:05:58,420 --> 00:06:02,625
Sakņu selerijai: 
krāsa, forma, garša.

48
00:06:15,592 --> 00:06:19,460
Kātu selerijai: 
kātu izmērs un kātu krāsa.

49
00:06:20,007 --> 00:06:24,552
Lapu selerijai: 
lapu skaits un to garša. 

50
00:06:28,560 --> 00:06:32,276
Selerijas (augiem savstarpēji nesaskaroties) 
tiek izvietotas

51
00:06:32,676 --> 00:06:35,236
no sala pasargātā smilšu kastē. 

52
00:06:45,640 --> 00:06:47,207
Ziemas gaitā

53
00:06:47,300 --> 00:06:51,912
saknes rūpīgi pārbaudāmas, 
aizvācot visas iepuvušās. 

54
00:07:12,720 --> 00:07:16,189
Pavasara sākumā 
saknes tiek pārstādītas -

55
00:07:16,487 --> 00:07:19,483
līdzko vairs nav gaidāms bargāks sals.

56
00:07:22,770 --> 00:07:27,061
Pieredze liecina - 
sēklas seleriju dēstīšana cieši līdzās

57
00:07:27,338 --> 00:07:33,207
mazina terciāro čemuru 
(vājākas kvalitātes sēklu) veidošanās risku. 

58
00:07:34,320 --> 00:07:38,872
Ģenētisko daudzveidību nodrošinās 
aptuveni 15 augi.

59
00:07:40,632 --> 00:07:45,730
Jāraugās, lai pēc pārstādīšanas 
neizkalstu augu saknes. 

60
00:08:05,720 --> 00:08:10,436
Selerija veido vairākus čemurus, 
kas nezied vienlaicīgi.

61
00:08:11,316 --> 00:08:15,534
Pirmais (primārais čemurs) 
atrodas galvenā kāta galā. 

62
00:08:17,230 --> 00:08:21,389
Čemuri, kas attīstās no galvenā kāta, 
ir sekundārie čemuri. 

63
00:08:22,210 --> 00:08:28,167
Terciārie čemuri veidojas uz kātiem,
kas atzarojas no sekundārajiem kātiem. 

64
00:08:45,120 --> 00:08:48,356
Ieteicams ievākt primāros čemurus.

65
00:08:48,865 --> 00:08:53,040
Sekundārie čemuri saglabājami 
vien nepieciešamības gadījumā. 

66
00:08:55,600 --> 00:08:58,625
Kad sēklas ir nobriedušas, tās kļūst brūnas.

67
00:08:59,520 --> 00:09:05,105
Sēklas ievācamas - 
kad lielākā daļa primāro čemuru jau ir brūni. 

68
00:09:08,807 --> 00:09:12,254
Gatavās sēklas var izsēties.

69
00:09:13,105 --> 00:09:18,232
Ja laikapstākļi ir vējaini vai lietaini, 
ievāciet čemurus - pirms tie pilnībā nobrieduši.

70
00:09:23,236 --> 00:09:29,432
Turpmāk sēklas žāvējamas 
sausā un labi vēdinātā vietā. 

71
00:09:40,232 --> 00:09:43,156
Ievākšana - šķirošana - uzglabāšana

72
00:09:56,778 --> 00:10:00,720
Ievācot sēklas no čemuriem manuāli, 
lietojiet cimdus. 

73
00:10:11,054 --> 00:10:15,032
Sēklu šķirošanai izmantojams siets, 
kas aizturēs pelavas. 

74
00:10:30,836 --> 00:10:36,930
Sēklas tiek vētītas - pūšot pār tām, 
lai atbrīvotos no atlikušajām pelavām. 

75
00:10:50,421 --> 00:10:53,796
Paciņā vienmēr ievietojiet 
etiķeti ar sugas un šķirnes

76
00:10:53,910 --> 00:11:00,210
nosaukumu, kā arī ievākšanas gadu
(ārējs uzraksts mēdz nodilt). 

77
00:11:07,316 --> 00:11:12,589
Dažas dienas uzglabājiet sēklas saldētavā, 
lai nonāvētu parazītus. 

78
00:11:13,800 --> 00:11:17,447
Seleriju sēklas dīgtspēju var saglabāt līdz 8 gadiem.

79
00:11:18,090 --> 00:11:22,669
Termiņš var tikt pagarināts 
līdz 10 gadiem vai ilgāk.

80
00:11:23,660 --> 00:11:27,432
Tas iespējams - 
uzglabājot sēklas saldētavā. 

81
00:11:28,392 --> 00:11:31,709
Vienā gramā ietilpst aptuveni 2000 sēklu. 

82
00:11:33,425 --> 00:11:36,218
Seleriju sēklu dīgšana nav vienmērīga.

83
00:11:36,900 --> 00:11:40,465
Zināmu periodu 
tās saglabā neaktīvu stāvokli. 
