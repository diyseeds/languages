1
00:00:08,305 --> 00:00:13,752
Zirņi piederīgi Fabaceae dzimtai 
un Pisum sativum sugai.

2
00:00:16,450 --> 00:00:17,934
Tie ir viengadīgi augi.

3
00:00:18,189 --> 00:00:20,327
Pastāv vairākas zirņu šķirnes:

4
00:00:23,403 --> 00:00:27,461
Lobāmi zirņi ar gludām, 
apaļām sēklām -

5
00:00:28,443 --> 00:00:32,981
spēcīgas, agrīnas, 
salcietīgas šķirnes. 

6
00:00:35,156 --> 00:00:39,018
Lobāmi zirņi 
ar rievotām sēklām -

7
00:00:39,170 --> 00:00:43,970
mazāk piemēroti agrīnai sējai, taču veiksmīgāk 
izdzīvo kartas temperatūras vidē. 

8
00:00:48,545 --> 00:00:50,145
Ēdamo pākšu zirņi -

9
00:00:50,981 --> 00:00:55,447
ēdamas ir arī jaunās pākstis, 
iekams attīstījušās sēklas. 

10
00:00:57,556 --> 00:00:59,460
Pastāv arī kāpaļājošās šķirnes,

11
00:00:59,650 --> 00:01:03,963
kas var pārsniegt 
70 cm garumu.

12
00:01:06,734 --> 00:01:08,290
Tās svarīgi 
balstīt ar maikstīm. 

13
00:01:14,920 --> 00:01:20,087
Tāpat sastopami pundurziņi,
kas sniedzas 45 - 70 cm augstumā.

14
00:01:20,920 --> 00:01:22,880
Tos parasti nevajag stutēt. 

15
00:01:25,560 --> 00:01:30,043
Pisum sativum sugā
ietilpst arī lopbarības zirņi. 

16
00:01:38,349 --> 00:01:39,425
Apputeksnēšana 

17
00:01:48,021 --> 00:01:52,580
Zirņu ziedi ir hermafrodīti 
un pašapputeksnējoši.

18
00:01:52,974 --> 00:01:57,636
Tas nozīmē - vīrišķie un sievišķie 
vairošanās orgāni atrodas uz viena zieda

19
00:01:57,774 --> 00:01:59,650
un ir savietojami. 

20
00:02:00,720 --> 00:02:06,094
Tomēr dažādas šķirnes var krustoties -
kukaiņu svešapputes ceļā.

21
00:02:11,403 --> 00:02:15,120
Riska pakāpe izriet no šķirnes, 
vides nosacījumiem

22
00:02:16,196 --> 00:02:18,807
un dabisku šķēršļu klātbūtnes. 

23
00:02:22,930 --> 00:02:29,149
Lai novērstu krustošanos,
divas šķirnes audzējamas 15 m atstatumā. 

24
00:02:31,854 --> 00:02:34,756
Ar dažiem metriem būs gana - 
ja pastāv dabiska barjera,

25
00:02:34,872 --> 00:02:36,538
piemēram, dzīvžogs. 

26
00:02:41,825 --> 00:02:45,178
Ja vienā dārzā 
vēlaties audzēt vairākas šķirnes,

27
00:02:45,350 --> 00:02:48,763
vienu iespējams izolēt
 zem tīkla sprosta.

28
00:02:49,527 --> 00:02:53,134
Izvietojiet tīklu 
pirms iesākusies ziedēšana. 

29
00:03:02,734 --> 00:03:04,232
Dzīves cikls 

30
00:03:14,290 --> 00:03:19,105
Sēklai audzētie zirņi tiek kultivēti 
tāpat kā pārtikas patēriņam.

31
00:03:23,320 --> 00:03:28,836
Daži zirņi sējami pirms ziemas, 
taču parasti zirņus sēj agri pavasarī.

32
00:03:33,120 --> 00:03:35,230
Nesējiet zirņus pārāk vēlu -

33
00:03:35,483 --> 00:03:39,949
ziedi neapputeksnēsies, 
temperatūrai pārsniedzot 30°C. 

34
00:03:55,723 --> 00:04:01,556
Audzējiet vismaz 50 stādus, lai nodrošinātu 
ģenētisko daudzveidību un selekcijas iespējas. 

35
00:04:05,956 --> 00:04:11,061
Attīstības laikā - izvēlieties skaistākos, 
veselīgākos un ražīgākos augus,

36
00:04:11,280 --> 00:04:12,880
pārējos atmetot. 

37
00:04:30,360 --> 00:04:33,880
No sēklu briedināšanai 
izraudzītajiem augiem

38
00:04:34,000 --> 00:04:37,410
pākstis pārtikas ražai 
netiek ievāktas.

39
00:04:42,043 --> 00:04:44,414
Ļaujiet sēklām pilnībā nobriest.

40
00:04:46,960 --> 00:04:50,620
Tādējādi saglabāsies 
šķirnes agrīnības īpašības. 

41
00:04:55,330 --> 00:05:01,214
Lai ievāktu sēklas - 
augi kaltējami turpat dārzā. 

42
00:05:17,410 --> 00:05:20,749
Ja nepieciešams, 
kaltēšanu var pabeigt šķūnītī. 

43
00:05:31,592 --> 00:05:35,156
Lai pārliecinātos, vai sēklas jau sausas, 
viegli iekodieties.

44
00:05:36,029 --> 00:05:39,869
Ja nepaliek iespiedums - 
sēklas ir izžuvušas. 

45
00:05:50,080 --> 00:05:53,345
Ievākšana - šķirošana - uzglabāšana

46
00:05:56,036 --> 00:05:59,730
Pākstis var izlobīt 
vienu pēc otras,

47
00:06:01,980 --> 00:06:04,269
izkult ar nūju

48
00:06:08,734 --> 00:06:09,927
vai mīdīt kājām. 

49
00:06:22,923 --> 00:06:26,472
Pēc tam sietā tiek izsijātas 
lielākās pelavas.

50
00:06:30,380 --> 00:06:32,901
Pavisam lielie krikumi 
izlasāmi ar rokām,

51
00:06:33,294 --> 00:06:36,000
bet smalkākās pelavas 
izsijājamas sietā. 

52
00:06:39,650 --> 00:06:43,301
Tad sēklas vētījamas, 
lai aizvāktu pēdīgās pelavas.

53
00:06:43,380 --> 00:06:51,658
Pār sēklām var pūst elpu, lietot ventilatoru 
vai nelielu gaisa kompresoru. 

54
00:07:04,421 --> 00:07:08,392
Paciņā ievietojama etiķete 
ar sugas un šķirnes nosaukumu,

55
00:07:08,538 --> 00:07:11,250
kā arī ievākšanas gadu.

56
00:07:11,381 --> 00:07:14,020
Ārējs uzraksts var nodilt. 

57
00:07:16,240 --> 00:07:21,803
Zirņu sēklas mēdz apsēst 
smecernieki (Bruchus pisurum) -

58
00:07:22,087 --> 00:07:26,040
nelieli kukaiņi, kas sadēj olas 
zem sēklu miziņām,

59
00:07:26,312 --> 00:07:28,596
kamēr augi vēl attīstās dārzā. 

60
00:07:29,767 --> 00:07:34,600
No oliņām viegli atbrīvoties - 
dažas dienas paturot sēklas saldētavā. 

61
00:07:47,960 --> 00:07:51,861
Zirņu sēklas dīgtspēju 
saglabā 3 - 8 gadus.

62
00:07:57,280 --> 00:08:00,618
Termiņš pagarināms - 
glabājot sēklas saldētavā. 
