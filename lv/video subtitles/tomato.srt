1
00:00:08,950 --> 00:00:16,700
Tomāti piederīgi Solanaceae dzimtai 
un Solanum lycopersicum sugai.

2
00:00:22,440 --> 00:00:28,220
Mērenos reģionos - viengadīgs augs. 
Dažkārt daudzgadīgs (tropu klimatā).

3
00:00:33,340 --> 00:00:37,860
Pastāv liela tomātu daudzveidība 
ar tūkstošiem šķirņu,

4
00:00:38,340 --> 00:00:44,685
kas atšķiras krāsā, apveidā, 
izmērā, garšā, attīstības ilgumā,

5
00:00:44,840 --> 00:00:49,715
agrīnībā, ziemcietībā, 
karstas un mitras vides panestspējā.

6
00:00:58,535 --> 00:01:01,620
Sastopami arī tomāti 
ar nenoteiktu attīstību.

7
00:01:02,205 --> 00:01:05,430
Tas nozīmē - augs turpinās 
veidot arvien jaunus ziedus,

8
00:01:05,580 --> 00:01:08,675
ražai ieilgstot 
garākā periodā. 

9
00:01:20,620 --> 00:01:22,780
Fiksētas attīstības tomātu ziedēšana

10
00:01:23,095 --> 00:01:27,000
koncentrēsies noteiktā, 
īsā periodā.

11
00:01:27,875 --> 00:01:31,010
Arī ražas ievākšana 
tādējādi būs visai neilga. 

12
00:01:39,160 --> 00:01:40,140
Apputeksnēšana

13
00:01:50,290 --> 00:01:52,580
Tomātu ziedi ir hermafrodīti -

14
00:01:54,215 --> 00:01:58,345
vīrišķie un sievišķie vairošanās orgāni 
atrodas uz viena zieda,

15
00:01:58,680 --> 00:02:03,580
un putekšņlapas putekšņi 
var apaugļot tā paša zieda auglenīcu.

16
00:02:03,995 --> 00:02:07,615
Tādēļ zieds uzskatāms 
par pašapaugļojošu. 

17
00:02:08,950 --> 00:02:10,390
Mērenā klimatā -

18
00:02:10,630 --> 00:02:15,415
vairošanās orgāni lielākoties ir pasargāti 
un apslēpti zieda iekšienē.

19
00:02:17,035 --> 00:02:18,695
Svešappute notiek reti. 

20
00:02:26,710 --> 00:02:30,920
Karstā un tropiskā klimatā 
svešappute vērojama biežāk.

21
00:02:33,600 --> 00:02:38,170
Svešapputes potenciālu 
ietekmē auglenīcas garums.

22
00:02:41,415 --> 00:02:46,030
Kukaiņiem vieglāk apputeksnēt auglenīcu, 
kas ir garāka nekā putekšņlapa.

23
00:02:46,140 --> 00:02:50,770
Grūtāk - īsu auglenīcu, kas apslēpta 
starp kopā saaugušām putekšnīcām. 

24
00:02:55,505 --> 00:02:59,380
Lieli, gaļīgi tomāti 
bieži ražo dubultziedus.

25
00:02:59,675 --> 00:03:02,285
Dubultziedi palielina 
krustošanās iespējamību.

26
00:03:06,140 --> 00:03:11,165
Tādēļ svarīgi ievērot 
katras šķirnes ziedu uzbūvi,

27
00:03:11,575 --> 00:03:14,785
kā arī kukaiņu aktivitāšu intensitāti 
jūsu dārzā. 

28
00:03:16,160 --> 00:03:20,950
Krustošanās risks ir mazāks, 
ja dārzā aug citi ziedi,

29
00:03:21,255 --> 00:03:24,370
kuru nektāru īpaši iecienījušas 
bites un kamenes. 

30
00:03:32,900 --> 00:03:36,260
Bezvēja laikā - 
papuriniet tomātu stādus

31
00:03:36,505 --> 00:03:40,225
vairākas reizes dienā, 
lai veicinātu pašapaugļošanos. 

32
00:03:50,575 --> 00:03:52,100
Mērenos reģionos -

33
00:03:52,300 --> 00:03:56,445
lai novērstu tādu šķirņu krustošanos, 
kuru auglenīcas ieslēptas ziedā,

34
00:03:56,615 --> 00:04:02,020
starp šķirnēm ievērojams 
3 m atstatums. 

35
00:04:06,495 --> 00:04:12,780
Starp šķirnēm, kuru garās auglenīcas 
ir redzamas, ieviešams 9 - 12 m atstatums. 

36
00:04:17,060 --> 00:04:23,200
Karstos un tropu reģionos - 
starp šķirnēm nodrošināms 1 km atstatums. 

37
00:04:26,570 --> 00:04:33,385
Attālums samazināms līdz 200 m, 
ja pastāv dabiska barjera, piemēram, dzīvžogs. 

38
00:04:36,105 --> 00:04:39,960
Šķirnes iespējams izolēt 
arī kukaiņu tīkla sprostā.

39
00:04:41,455 --> 00:04:42,620
Sīkāk -

40
00:04:42,905 --> 00:04:46,895
‘’Sēklu ražošanas ābeces’’ 
izolēšanas metodēm veltītajā modulī! 

41
00:04:53,650 --> 00:04:54,820
Dzīves cikls 

42
00:05:17,400 --> 00:05:20,540
Mērenā klimatā -
tomāts ir viengadīgs augs.

43
00:05:24,005 --> 00:05:27,380
Tropu reģionos tomāts 
var izdzīvot vairākus gadus.

44
00:05:36,630 --> 00:05:38,980
Sēklu ieguvei audzētos tomātus

45
00:05:39,120 --> 00:05:42,540
kultivē tāpat 
kā pārtikas patēriņam. 

46
00:06:07,415 --> 00:06:08,980
Līdz agrīno šķirņu

47
00:06:09,260 --> 00:06:13,180
nobriešanai jāgaida 
vismaz 40 dienas -

48
00:06:13,495 --> 00:06:15,395
kopš ziedēšanas sākuma.

49
00:06:24,235 --> 00:06:30,580
Vidējas sezonalitātes vai vēlīnām šķirnēm 
periods var ieilgt pat līdz 60 - 80 dienām. 

50
00:06:37,300 --> 00:06:38,620
Sēklu ieguvei atlasāmi augi,

51
00:06:38,700 --> 00:06:42,010
kas novēroti 
visos attīstības posmos

52
00:06:42,120 --> 00:06:44,780
un kas atbilst izraudzītajiem 
šķirnes kritērijiem. 

53
00:06:48,580 --> 00:06:52,250
Tomātu stādiem jāuzrāda 
vienmērīga un aktīva augšana,

54
00:06:53,010 --> 00:07:00,050
agrīna vai vēlīna augļražība, 
vairāki ziedi ar augstu auglību.

55
00:07:04,015 --> 00:07:06,140
Fiksētās attīstības šķirnēm -

56
00:07:06,550 --> 00:07:10,070
izvēlieties kompaktus augus 
ar īsāku ražas periodu. 

57
00:07:14,080 --> 00:07:18,345
Pagaršojiet arī augli, lai noskaidrotu, 
cik tas salds vai skābens. 

58
00:07:19,905 --> 00:07:24,405
Augļu selekcijas gaitā 
nepieciešams sekot šķirnes īpašībām:

59
00:07:25,155 --> 00:07:31,945
augļa izmēram, mīkstuma krāsai, 
mizas pievilcībai,

60
00:07:32,785 --> 00:07:34,970
kā arī augļa daiviņu skaitam.

61
00:07:36,665 --> 00:07:39,545
Sēklas nav ieteicams iegūt

62
00:07:39,815 --> 00:07:43,630
no ražai novāktiem tomātiem -
nebūs iepazītas

63
00:07:43,720 --> 00:07:45,950
šķirnes īpašības 
visos attīstības posmos. 

64
00:07:49,670 --> 00:07:53,740
Sēklas ievācamas 
no veselīgiem stādiem,

65
00:07:53,925 --> 00:07:56,660
kad augļi 
jau nogatavojušies.

66
00:07:57,415 --> 00:08:00,845
Labāk izvēlēties pirmās vai otrās 
ziedēšanas grupas.

67
00:08:06,230 --> 00:08:09,595
Tomātus iespējams novākt 
arī vēlāk sezonā,

68
00:08:09,745 --> 00:08:12,310
ja augi veiksmīgi 
pretojušies slimībām. 

69
00:08:16,880 --> 00:08:20,810
Šķirnes ģenētiskās daudzveidības 
nodrošināšanai

70
00:08:21,365 --> 00:08:25,150
ieteicams ražai izvēlēties 
6 - 12 dažādus augus,

71
00:08:25,695 --> 00:08:29,215
neievācot augļus no slimiem 
vai bojātiem stādiem. 

72
00:08:32,745 --> 00:08:36,220
Ja augļi nav pilnībā nobrieduši 
uz tomāta stāda

73
00:08:37,070 --> 00:08:40,395
(auksta klimata apstākļos 
vai augstieņu reģionos) -

74
00:08:41,340 --> 00:08:48,525
ievāciet augli un atstājiet to nogatavoties 
siltā vietā, piemēram, siltumnīcā vai uz palodzes. 

75
00:08:56,525 --> 00:08:59,150
Ievākšana - šķirošana - uzglabāšana 

76
00:09:04,430 --> 00:09:08,695
Sēklas ievāciet no gataviem, 
bet neierūgušiem augļiem. 

77
00:09:13,590 --> 00:09:15,380
Nelielam sēklu apjomam -

78
00:09:15,520 --> 00:09:20,220
pārgrieziet augli, ar karoti izgrebiet sēklas 
(kopā ar daļu mīkstuma), ievietojiet stikla burkā

79
00:09:20,620 --> 00:09:22,260
(vai tomātu izspaidiet). 

80
00:09:31,120 --> 00:09:36,140
Lielākam sēklu daudzumam 
(vai ķiršu tomātu un savvaļas tomātu šķirnēm) -

81
00:09:36,690 --> 00:09:39,340
sakapājiet augli 
un to sablenderējiet. 

82
00:09:47,045 --> 00:09:53,500
Katra sēkla ietverta želatīnapvalkā, 
kas aizkavē sēklas dīgšanas iesākumu. 

83
00:09:58,700 --> 00:10:03,980
Lai sēklu atdalītu no apvalka, 
ieteicams izmantot fermentēšanas metodi.

84
00:10:06,940 --> 00:10:12,700
Sīkāk - ‘’Sēklu ražošanas ābeces’’ 
fermentācijai, sēklu apstrādei un selekcijai

85
00:10:13,060 --> 00:10:15,340
veltītajos moduļos. 

86
00:10:33,355 --> 00:10:36,100
Kad sēklas apstrādātas ar ūdeni,

87
00:10:36,540 --> 00:10:40,140
tās tūdaļ izliekamas žāvēties - 
sausā, ēnainā

88
00:10:40,260 --> 00:10:43,180
un labi vēdinātā vietā. 

89
00:10:48,495 --> 00:10:52,940
Nelielus sēklu apjomus 
iespējams žāvēt kafijas filtros.

90
00:10:53,775 --> 00:10:57,660
Filtri lieliski uzsūc mitrumu, 
un sēklas tiem nepielīp.

91
00:10:59,565 --> 00:11:03,740
Katrā filtrā ievietojiet 
ne vairāk kā tējkarotīti sēklu.

92
00:11:05,715 --> 00:11:11,900
Izkariet maisiņus uz veļas auklas - 
sausā, caurvējotā, ēnainā, siltā vietā. 

93
00:11:21,650 --> 00:11:24,060
Sēklas (stikla burkā vai plastmasas maisiņā)

94
00:11:24,380 --> 00:11:27,980
nedrīkst pakļaut karstuma, 
mitruma un gaismas ietekmei.

95
00:11:30,605 --> 00:11:34,140
Iepakojumā ievietojiet etiķeti 
ar sugas un šķirnes nosaukumu,

96
00:11:34,500 --> 00:11:36,260
kā arī ievākšanas gadu. 

97
00:11:43,490 --> 00:11:48,985
Tomātu sēklas dīgtspēju 
saglabā 4 - 6 gadus.

98
00:11:49,880 --> 00:11:53,510
Termiņš pagarināms - 
glabājot sēklas saldētavā. 
