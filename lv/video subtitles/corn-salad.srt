﻿1
00:00:11,220 --> 00:00:15,098
Salātu baldriņi piederīgi 
Caprifoliaceae dzimtai.

2
00:00:17,200 --> 00:00:20,065
Tie ir ziemas un agra pavasara augi.

3
00:00:21,636 --> 00:00:23,890
Pastāv divas kultivētās sugas:

4
00:00:25,120 --> 00:00:27,300
Valerianella locusta

5
00:00:32,916 --> 00:00:35,700
un Valerianella eriocarpa,

6
00:00:36,340 --> 00:00:41,389
kas ir vārīgāka suga un pārsvarā aug 
Itālijā un Francijas dienvidos. 

7
00:00:46,596 --> 00:00:50,007
Eiropā sastopams arī savvaļas baldriņš. 

8
00:01:02,700 --> 00:01:03,789
Apputeksnēšana 

9
00:01:14,487 --> 00:01:21,869
Baldriņš ir autogāms augs - 
hermafrodītie ziedi ir pašapaugļojoši.

10
00:01:22,436 --> 00:01:26,356
Tas nozīmē - vienā ziedā atrodami gan vīrišķie, 
gan sievišķie vairošanās orgāni,

11
00:01:26,574 --> 00:01:27,847
un tie ir savietojami.

12
00:01:29,578 --> 00:01:36,501
Tomēr pastāv šķirņu krustošanās risks - 
kukaiņu apputeksnēšanas ceļā. 

13
00:01:43,607 --> 00:01:48,516
Locusta un Eriocarpa sugas nespēj krustoties,

14
00:01:50,727 --> 00:01:57,110
taču jāuzmanās, jo ar kultivētajām šķirnēm 
mēdz krusoties savvaļas baldriņi. 

15
00:02:02,225 --> 00:02:04,734
Lai nodrošinātu šķirnes tīrību -

16
00:02:05,010 --> 00:02:11,760
starp vienas sugas dažādām šķirnēm 
ievērojams 50 m atstatums.

17
00:02:12,770 --> 00:02:19,738
Attālums samazināms līdz 30 m, 
ja pastāv dabiska barjera (piemēram, dzīvžogs). 

18
00:02:22,960 --> 00:02:24,380
Dzīves cikls

19
00:02:35,941 --> 00:02:41,214
Sēklai audzētie salātu baldriņi 
kultivējami tāpat kā pārtikas ieguvei.

20
00:02:42,014 --> 00:02:44,349
Sējiet agri rudenī.

21
00:02:46,523 --> 00:02:51,527
Baldriņi pārziemos dārzā, bet ziedi un sēklas 
attīstīsies nākamajā pavasarī. 

22
00:03:01,338 --> 00:03:05,832
Ģenētisko daudzveidību 
nodrošinās 50 augi. 

23
00:03:08,989 --> 00:03:13,694
Nenolasiet lapas no sēklu ieguvei 
izraudzītajiem augiem. 

24
00:03:17,585 --> 00:03:21,220
Selekcijas kritēriji: 
ziemcietība,

25
00:03:22,356 --> 00:03:25,260
lapu izmērs, forma un krāsa,

26
00:03:28,203 --> 00:03:33,590
pretestība sēnīšu slimībām,
vēlīna ziedēšana. 

27
00:03:39,469 --> 00:03:44,276
Atbrīvojieties no nepareizi veidotiem augiem, 
kas neatbilst šķirnes īpašībām. 

28
00:04:35,141 --> 00:04:41,670
Sēklas nobriest lēnām - ilgstošā periodā. 
Gatavas - tās viegli izbrist zemē. 

29
00:04:52,740 --> 00:04:55,927
Rūpīgi sekojiet sēklu attīstībai

30
00:04:56,480 --> 00:04:59,883
un nenogaidiet, līdz augs pilnībā izkaltīs.

31
00:05:00,712 --> 00:05:06,720
Kad puse no sēklām nobriedušas, 
iespējams sākt sēklu ievākšanu. 

32
00:05:07,578 --> 00:05:11,912
Sēklu zudumi ievākšanas laikā 
būs mazāki -

33
00:05:12,378 --> 00:05:17,040
zem auga iepriekš paklājot 
plēvi vai papīru. 

34
00:05:17,985 --> 00:05:23,869
Žāvējiet sēklas sausā un labi vēdinātā vietā: 
2 - 3 nedēļas.

35
00:05:27,060 --> 00:05:29,069
Lai augi nepārkarstu,

36
00:05:29,323 --> 00:05:32,552
nežāvējiet tos 
pārāk blīvās kaudzēs.

37
00:05:37,876 --> 00:05:41,636
Vērīgi sekojiet 
augu žāvēšanās procesiem. 

38
00:05:51,447 --> 00:05:54,060
Ievākšana - šķirošana - uzglabāšana

39
00:05:58,196 --> 00:06:02,349
Ievāciet sēklas saulainā dienā - 
saberzējot sausos augus.

40
00:06:04,174 --> 00:06:09,330
Nepūlieties ievākt katru sīkāko sēkliņu, 
jo tās nebūs nobriedušas. 

41
00:06:11,760 --> 00:06:15,970
Šķirojiet sēklas, 
izmantojot dažāda izmēra sietus,

42
00:06:16,378 --> 00:06:20,509
lai vispirms atbrīvotos no rupjākām, 
tad - smalkākām pelavām. 

43
00:06:43,178 --> 00:06:47,280
Sijāšanai seko vētīšana - 
ar vēja palīdzību. 

44
00:07:05,880 --> 00:07:10,007
Sēklas vēl nebūs pieņēmušas 
brieduma nokrāsu

45
00:07:10,356 --> 00:07:12,443
Uzglabāšanas laikā tās kļūs tumšākas. 

46
00:07:14,487 --> 00:07:16,770
2 mēnešus pēc nobriešanas 
sēklas ir neaktīvas,

47
00:07:16,923 --> 00:07:21,549
un to dīgtspēja ir visaugstākā - 
tikai pēc 1 vai 2 gadiem.

48
00:07:22,720 --> 00:07:26,356
Tādēļ sējiet 
iepriekšējos gados ievāktās sēklas. 

49
00:07:27,861 --> 00:07:32,101
Maisiņā ievietojiet etiķeti 
ar sugas un šķirnes nosaukumu,

50
00:07:32,334 --> 00:07:38,021
kā arī ievākšanas gadu
(ārējs uzraksts mēdz nodilt). 

51
00:07:42,720 --> 00:07:47,265
Dažas dienas uzglabājot sēklas saldētavā, 
ies bojā parazīti. 

52
00:07:50,720 --> 00:07:55,120
Salātu baldriņa sēklu dīgtspēja 
saglabājas vidēji 5 gadus.

53
00:08:00,240 --> 00:08:03,447
Termiņš pagarināms, 
uzglabājot sēklas saldētavā. 
