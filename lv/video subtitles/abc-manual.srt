﻿1
00:00:18,460 --> 00:00:21,140
lai izvairītos no nevēlamas svešapputes.

2
00:00:25,380 --> 00:00:28,740
Tāpat iespējams vienā dārzā 
audzēt vairākas,

3
00:00:28,860 --> 00:00:31,220
vienai sugai piederīgas šķirnes,

4
00:00:31,620 --> 00:00:33,380
neieviešot izolēšanas mehānismus.

5
00:00:37,780 --> 00:00:42,940
Izpētīsim izmērā lielāko Cucurbitaceae 
ziedu manuālo apputeksnēšanu. 

6
00:00:45,200 --> 00:00:48,300
Vakarā pirms plānotās apputeksnēšanas -

7
00:00:52,140 --> 00:00:55,410
kas pieder vienai šķirnei
un grasās atvērties.

8
00:01:02,140 --> 00:01:06,700
Tiem raksturīga 
krēmīgi dzeltenīga krāsa,

9
00:01:07,020 --> 00:01:08,540
un tie vēl nebūs atvērušies.

10
00:01:10,740 --> 00:01:13,180
Ziedēšana ilgst tikai vienu dienu. 

11
00:01:19,500 --> 00:01:21,340
kas nav sācis attīstīties. 

12
00:01:23,180 --> 00:01:26,820
Vīrišķo ziedu stiebrs būs slaiks -
bez izaugumiem. 

13
00:01:28,835 --> 00:01:32,620
Tiek atlasīti 
vairāku augu sievišķie ziedi.

14
00:01:32,940 --> 00:01:40,425
Tie tiek aizvērti - izmantojot veļasknaģi,
līplentu vai nelielu aukliņu.

15
00:01:40,900 --> 00:01:44,420
Aukla jāpiesien vaļīgi, lai netraumētu ziedus.

16
00:01:50,200 --> 00:01:55,460
katram sievišķajam ziedam 
piemeklējot 3 vīrišķos ziedus (no dažādiem augiem).

17
00:01:55,560 --> 00:01:57,180
Arī tie tiek aizsprausti.

18
00:01:59,220 --> 00:02:04,020
Tādējādi kukaiņi neiekļūs ziedos - 
rīta agrumā.

19
00:02:12,460 --> 00:02:14,660
tie iezīmējami ar mietiņiem. 

20
00:02:18,850 --> 00:02:22,460
No rīta - vīrišķie ziedi tiek noplūkti

21
00:02:23,030 --> 00:02:25,420
un savienoti ar sievišķajiem ziediem,

22
00:02:25,740 --> 00:02:27,820
kas nolūkoti iepriekšējā vakarā.

23
00:02:29,515 --> 00:02:33,045
Tas jāpaveic - 
krietni pirms dienas tveices.

24
00:02:33,940 --> 00:02:38,265
Citādi putekšņi fermentēsies, 
zaudējot auglību,

25
00:02:44,925 --> 00:02:48,980
Trīs vīrišķo ziedu ziedlapas
tiek uzmanīgi noplūktas.

26
00:02:50,170 --> 00:02:52,220
Sievišķais zieds tiek atknaģēts

27
00:02:52,500 --> 00:02:54,425
un maigi atvērts.

28
00:03:10,535 --> 00:03:14,060
Ja bite nelaikā lūko
ievākt nektāru

29
00:03:14,285 --> 00:03:17,540
no sievišķā zieda,
kas tobrīd tiek apputeksnēts -

30
00:03:17,860 --> 00:03:19,460
ziedu nepieciešams aizvākt.

31
00:03:21,270 --> 00:03:24,480
Bites pārnēsā vairāku augu putekšņus,

32
00:03:24,710 --> 00:03:27,340
tādēļ būs neizbēgama svešappute. 

33
00:03:41,360 --> 00:03:44,100
sievišķie ziedi tiek uzmanīgi aizvērti

34
00:03:44,635 --> 00:03:49,020
un no jauna aizknaģēti 
(gluži kā iepriekšējā vakarā).

35
00:03:52,350 --> 00:03:55,620
Lai ražas laikā atrastu 
manuāli apputeksnētos augļus,

36
00:04:05,380 --> 00:04:08,060
Cucurbitaceae izmēros sīkāko ziedu

37
00:04:08,460 --> 00:04:14,380
manuāla apputeksnēšana 
prasa lielāku rūpību un pacietību.

38
00:04:17,935 --> 00:04:23,660
80% no meloņu sievišķajiem ziediem 
neattīsta augļus,

39
00:04:24,300 --> 00:04:27,940
tādēļ kukaiņu apputeksnēšana ir efektīvāka
nekā manuālā.

40
00:04:35,060 --> 00:04:43,740
Toties arbūzu manuālās apputeksnēšanas potenciāls 
sasniedz 50-75%.

41
00:04:45,460 --> 00:04:48,300
Gurķiem - 85%. 

42
00:04:51,130 --> 00:04:57,680
Citiem augiem (piem., kukurūzai un saulespuķei) 
pielietojamas atšķirīgas metodes.

43
00:04:58,805 --> 00:05:01,900
Iepazīstiet tās dārzenim veltītajā īsfilmā! 

44
00:00:09,500 --> 00:00:12,205
Cucurbitaceae manuāla apputeksnēšana

45
00:00:13,815 --> 00:00:18,140
Cucurbitaceae ziedus
iespējams apputeksnēt manuāli,

46
00:00:21,970 --> 00:00:24,825
Metodes priekšrocība -
tās vienkāršība.

47
00:00:48,900 --> 00:00:51,860
nepieciešams identificēt 
sievišķos un vīrišķos ziedus,

48
00:01:16,140 --> 00:01:19,140
Uz sievišķo ziedu stiebra
atradīsies mazs augļaizmetnis,

49
00:01:46,660 --> 00:01:49,260
Ģenētiskā daudzveidība panākama -

50
00:02:07,565 --> 00:02:11,940
Lai atlasītos ziedus
nākamajā rītā būtu vieglāk atrast -

51
00:02:38,380 --> 00:02:40,135
bet ziedi atkal aizvērsies. 

52
00:03:00,025 --> 00:03:05,940
Trīs vīrišķo ziedu putekšņi 
tiek pievadīti sievišķā zieda drīksnai. 

53
00:03:38,385 --> 00:03:40,980
Manuālās apputeksnēšanas beigās -

54
00:03:56,020 --> 00:04:00,620
auga kāts tiek iezīmēts 
ar etiķeti vai krāsainu aukliņu. 

55
00:04:29,270 --> 00:04:33,540
Veiksmīgs rezultāts sagaidāms 
ne vairāk kā 10-15% gadījumu.
