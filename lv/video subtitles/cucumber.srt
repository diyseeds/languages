﻿1
00:00:09,030 --> 00:00:12,676
Gurķi piederīgi Cucurbitaceae dzimtai

2
00:00:12,820 --> 00:00:16,160
un Cucumis sativus sugai.

3
00:00:16,800 --> 00:00:19,080
Tie iedalāmi piecos tipos:  

4
00:00:20,400 --> 00:00:21,980
- lauka gurķi,

5
00:00:24,300 --> 00:00:26,700
kam mizu klāj sīki dzelksnīši,  

6
00:00:30,900 --> 00:00:34,420
- siltumnīcas gurķi 
ar izteikti gludu mizu, 

7
00:00:38,760 --> 00:00:42,000
- Sikimas gurķi ar sarkanoranžu mizu,

8
00:00:45,900 --> 00:00:50,720
- gurķi ar ļoti sīkiem augļiem, 
kas izmantojami konservēšanā,  

9
00:00:52,880 --> 00:00:54,630
- apaļie gurķi.  

10
00:01:07,680 --> 00:01:08,440
Apputeksnēšana  

11
00:01:20,880 --> 00:01:23,598
Gurķi ir vienmājas augi.

12
00:01:24,400 --> 00:01:28,880
Tas nozīmē - vīrišķie un sievišķie ziedi 
izvietoti uz viena auga.

13
00:01:31,470 --> 00:01:34,770
Sievišķo ziedu sēklotne atrodas zem zieda.

14
00:01:37,900 --> 00:01:42,400
Sēklotne ir mazs gurķīša aizmetnis, 
kas attīstīsies pēc apaugļošanas.

15
00:01:46,150 --> 00:01:51,730
Vīrišķie ziedi parādās pirmie - 
garu stiebru galos.

16
00:01:56,050 --> 00:01:58,900
Ziedi atveras tikai vienu dienu. 

17
00:02:00,280 --> 00:02:05,500
Gurķi var pašapaugļoties - 
sievišķo ziedu apputeksnē

18
00:02:05,700 --> 00:02:09,520
tā paša auga 
vīrišķā zieda putekšņi.

19
00:02:13,570 --> 00:02:15,550
Tomēr izplatītāka ir svešappute.

20
00:02:16,780 --> 00:02:21,250
Ziedus parasti apputeksnē kukaiņi 
(galvenokārt - bites).

21
00:02:29,160 --> 00:02:34,980
Visas Cucumis sativus sugai piederīgās šķirnes 
spēj savstarpēji krustoties.

22
00:02:36,960 --> 00:02:42,090
Taču gurķi nekrustojas ar melonēm, 
arbūziem vai ķirbjiem. 

23
00:02:44,440 --> 00:02:50,620
Lai novērstu krustošanos - 
starp šķirnēm ievērojams 1 km atstatums.

24
00:02:53,360 --> 00:02:58,490
Attālums samazināms līdz 500 m, 
ja pastāv dabiska barjera

25
00:02:58,690 --> 00:02:59,450
(piemēram, dzīvžogs). 

26
00:03:02,450 --> 00:03:06,890
Lai vienā dārzā audzētu vairākas 
sēklas gurķu šķirnes, iespējams pielietot

27
00:03:07,220 --> 00:03:08,510
sekojošas metodes.

28
00:03:11,630 --> 00:03:15,547
Pirmā metode: vienas šķirnes gurķu 
pārklāšana ar tīklu,

29
00:03:16,140 --> 00:03:18,800
kurā ievietota 
neliela kameņu ligzda.

30
00:03:22,220 --> 00:03:26,660
Otra metode - divu šķirņu 
pārklāšana ar atsevišķiem tīkliem.

31
00:03:28,240 --> 00:03:33,500
Pirmajā dienā tiek atvērts vienas šķirnes tīkls, 
bet nākamajā dienā - otras šķirnes tīkls.

32
00:03:34,340 --> 00:03:36,500
Apputeksnēšanu veiks savvaļas kukaiņi.

33
00:03:37,460 --> 00:03:41,570
Raža būs niecīgāka, 
jo daži ziedi netiks apputeksnēti.

34
00:03:47,810 --> 00:03:50,180
Ziedus var apputeksnēt arī pašrocīgi.

35
00:03:50,930 --> 00:03:55,430
Gurķus ir grūtāk apputeksnēt manuāli 
(nekā ķirbjus vai cukīni),

36
00:03:55,940 --> 00:03:58,400
jo gurķu ziediņi ir sīkāki.

37
00:04:01,070 --> 00:04:03,098
Ar izolēšanas metodēm iepazīstieties

38
00:04:03,180 --> 00:04:06,240
‘’Sēklu ražošanas ābeces’’ 
mehāniskajai izolēšanai

39
00:04:06,243 --> 00:04:10,040
un manuālajai apputeksnēšanai 
veltītajās īsfilmās. 

40
00:04:22,500 --> 00:04:23,140
Dzīves cikls  

41
00:04:39,600 --> 00:04:44,220
Sēklu ražošanai audzētie gurķi 
tiek kultivēti

42
00:04:44,340 --> 00:04:45,720
tāpat kā pārtikas ieguvei.

43
00:05:01,590 --> 00:05:06,300
Atlasiet vismaz 6 augus - 
ģenētiskās daudzveidības nodrošināšanai.

44
00:05:07,440 --> 00:05:08,790
Ieteicams audzēt duci. 

45
00:05:33,790 --> 00:05:38,075
Sēklu ražošanas augus izvēlieties rūpīgi -
saskaņā ar šķirnes

46
00:05:38,220 --> 00:05:40,870
specifiskajiem kritērijiem.

47
00:05:42,100 --> 00:05:46,450
Paturiet dzīvelīgus augus, 
kas nobriedinājuši labi attīstītus gurķus.

48
00:05:48,520 --> 00:05:50,110
Atbrīvojieties no slimiem augiem.

49
00:06:04,260 --> 00:06:08,827
Sēklas gurķu briedums 
atšķiras no pārtikas gurķu

50
00:06:08,960 --> 00:06:10,140
gatavības.

51
00:06:10,430 --> 00:06:13,500
Parasti mēs ēdam nenogatavojušos gurķus. 

52
00:06:14,250 --> 00:06:18,352
Sēklu ieguvei paredzētajiem gurķiem
 jāļauj attīstīties

53
00:06:18,560 --> 00:06:20,040
līdz pilnam briedumam.

54
00:06:22,870 --> 00:06:27,040
Gurķim jāsaniedz pilns izmērs. 
Mainīsies arī gurķa krāsa.

55
00:06:34,060 --> 00:06:37,540
Gurķus var ievākt 
arī neilgi pirms nobriešanas.

56
00:06:38,140 --> 00:06:42,160
Novietojiet tos siltā vietā 
un ļaujiet nogatavoties.

57
00:06:42,970 --> 00:06:46,090
Metode veicinās 
sēklu auglīgumu. 

58
00:06:55,790 --> 00:06:59,100
Ievākšana - šķirošana - uzglabāšana  

59
00:07:07,160 --> 00:07:09,920
Sēklu ievākšanai - pāršķeliet gurķi,


60
00:07:12,220 --> 00:07:13,960
izņemiet mīkstumu ar visām sēklām

61
00:07:25,020 --> 00:07:27,520
un ļaujiet masai dažas dienas fermentēties.

62
00:07:32,250 --> 00:07:36,990
Sēklas atbrīvosies 
no viskozā apvalka.

63
00:07:40,340 --> 00:07:44,990
Sīkāka informācija - 
mitrās apstrādes metodēm veltītajā

64
00:07:45,190 --> 00:07:46,790
‘’Sēklu ražošanas ābeces’’ modulī.

65
00:07:53,520 --> 00:07:57,060
Ievietojiet sēklas sietā 
un noskalojiet tekošā ūdenī.

66
00:08:00,850 --> 00:08:03,748
Ieberiet sēklas traukā ar ūdeni,

67
00:08:04,420 --> 00:08:07,300
lai atbrīvotos no sterilām sēklām.

68
00:08:07,630 --> 00:08:12,460
Pilnās, smagākās sēklas nogrims, 
bet tukšās, sterilās sēklas uzpeldēs virspusē. 

69
00:08:16,320 --> 00:08:21,240
Aizvāciet tukšās sēklas. Noskalojiet derīgās,
un žāvējiet tās labi vēdinātā vietā.

70
00:08:26,730 --> 00:08:28,980
Saberzējiet sēklas, lai tās atdalītu.

71
00:08:32,490 --> 00:08:37,920
Sēklas būs pilnībā izžuvušas, 
kad tās liecot lūzīs. 

72
00:08:43,820 --> 00:08:46,060
Paciņā ievietojiet etiķeti 
ar sugas un šķirnes

73
00:08:46,280 --> 00:08:51,180
nosaukumu, 
kā arī ievākšanas gadu.

74
00:08:51,440 --> 00:08:53,960
Ārējs uzraksts mēdz nodilt.

75
00:09:01,310 --> 00:09:05,630
Dažas dienas uzglabājiet sēklas saldētavā,
lai nonāvētu parazītus.

76
00:09:09,790 --> 00:09:15,460
Gurķu sēklas dīgtspēju saglabā 
6 gadus vai ilgāk.

77
00:09:18,340 --> 00:09:21,070
Termiņš pagarināms - 
uzglabājot sēklas saldētavā.

78
00:09:23,110 --> 00:09:27,250
Vienā gramā ietilpst 30 - 40 sēklas. 
