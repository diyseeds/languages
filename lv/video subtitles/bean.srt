﻿1
00:00:09,640 --> 00:00:12,705
Pupas ir viengadīgi Fabaceae dzimtas augi.

2
00:00:19,320 --> 00:00:21,150
Pastāv vairākas pupiņu šķirnes.

3
00:00:21,270 --> 00:00:27,130
Izplatītākās šķirnes:
Phaseolus vulgaris un Phaseolus coccineus.

4
00:00:41,570 --> 00:00:45,180
Phaseolus vulgaris izšķirami vairāki paveidi:

5
00:00:45,690 --> 00:00:52,740
pundurpupas, puskāpaļājošās pupas 
un kāršu pupas.

6
00:00:55,315 --> 00:01:00,180
No minētajām sugām 
tiek iegūtas pākstis

7
00:01:00,480 --> 00:01:06,100
(piemēram, zaļo pupu pākstis)
vai sēklas (svaigas vai kaltētas). 

8
00:01:13,455 --> 00:01:18,100
Pastāv arī citas sugas, 
piemēram, Phaseolus coccineus

9
00:01:18,655 --> 00:01:22,180
jeb daudzziedu pupiņas,
kas arī ir kāpaļājoši augi.

10
00:01:23,430 --> 00:01:26,220
Tām raksturīgi skaisti, sarkani vai balti ziedi.

11
00:01:26,315 --> 00:01:31,500
Augļi nobriest arī pie temperatūras, 
kas zemāka par 25°C. 

12
00:01:38,980 --> 00:01:44,025
Abas sugas iespējams atšķirt pēc pākstīm.

13
00:01:45,095 --> 00:01:48,315
Phaseolus coccineus pākstis ir biezākas.

14
00:01:48,750 --> 00:01:52,995
Phaseolus vulgaris raksturīgas 
gludākas un šaurākas pākstis. 

15
00:02:04,125 --> 00:02:05,120
Apputeksnēšana 

16
00:02:21,325 --> 00:02:27,825
Phaseolus vulgaris ziedi 
ir pašapputeksnējoši un hermafrodīti.

17
00:02:28,295 --> 00:02:32,790
Tas nozīmē - vienā ziedā atrodami 
gan vīrišķie, gan sievišķie orgāni.

18
00:02:33,125 --> 00:02:34,515
Tie ir autogāmi augi. 

19
00:02:36,060 --> 00:02:38,750
Tomēr kukaiņu apputeksnēšana
var izraisīt

20
00:02:38,820 --> 00:02:40,530
starpšķirņu krustošanos.

21
00:02:41,475 --> 00:02:45,085
Riska pakāpe atkarīga no šķirnes. 

22
00:02:48,045 --> 00:02:49,390
Lai novērstu svešapputi -

23
00:02:49,820 --> 00:02:53,575
starp divām pundurpupu šķirnēm 
ievērojams vismaz

24
00:02:53,875 --> 00:02:56,420
5 - 10 m atstatums.

25
00:02:59,075 --> 00:03:03,695
Kāpaļājošo pupiņu starpšķirņu atstatums - 
vismaz 50 metri. 

26
00:03:05,315 --> 00:03:11,015
Starp kāpaļājošajām un pundurpupām 
ievērojams vismaz 10 m attālums. 

27
00:03:17,745 --> 00:03:22,305
Lai panāktu maksimālu šķirnes tīrību - 
ierobežotā platībā,

28
00:03:22,590 --> 00:03:27,285
katra Phaseolus vulgaris šķirne 
pārklājama ar kukaiņu tīklu.

29
00:03:33,635 --> 00:03:39,140
Tīkls izvietojams, iekams sākusies ziedēšana, 
lai izvairītos no krustošanās. 

30
00:03:44,900 --> 00:03:48,740
Phaseolus coccineus ziedi 
ir hermafrodīti.

31
00:03:48,990 --> 00:03:52,680
Tas nozīmē - vīrišķie un sievišķie orgāni 
atrodami uz viena zieda.

32
00:03:59,865 --> 00:04:07,000
Sarkanziedu pupiņu apputeksnēšanai 
nepieciešami tādi kukaiņi kā bites un kamenes.

33
00:04:09,495 --> 00:04:14,225
Toties krēmkrāsas ziedu pupas 
spēj pašapaugļoties. 

34
00:04:16,010 --> 00:04:20,995
Lai divas Phaseolus coccineus šķirnes nekrustotos -

35
00:04:21,720 --> 00:04:24,390
ievērojams 500 m atstatums.

36
00:04:25,640 --> 00:04:32,855
Attālums samazināms līdz 150 m, 
ja pastāv dabiska barjera (piemēram, dzīvžogs). 

37
00:04:33,610 --> 00:04:40,415
Starp coccineus un vulgaris kāršu pupām 
ieteicams 300 m atstatums. 

38
00:04:41,335 --> 00:04:47,165
Tāpat - 50 m atstatums starp 
coccineus kāršu pupām un vulgaris pundurpupām. 

39
00:04:53,810 --> 00:04:54,840
Dzīves cikls 

40
00:05:11,980 --> 00:05:16,255
Sēklu pupas kultivējamas 
tāpat kā pupiņas - pārtikas ieguvei.

41
00:05:17,150 --> 00:05:19,440
Pupas jāsēj siltā augsnē. 

42
00:06:01,920 --> 00:06:06,385
Zaļo pupiņu audzes ieteicams nošķirt -

43
00:06:06,920 --> 00:06:09,635
atdalot pārtikas un sēklu pupas.

44
00:06:15,030 --> 00:06:18,230
Pārtikas pupiņas
nolasāmas sezonas gaitā.

45
00:06:21,375 --> 00:06:26,235
Sēklas ievācamas vienīgi tad, 
kad nogatavojušās visas pākstis.

46
00:06:29,435 --> 00:06:33,210
Paturot tikai 
sezonas beigās nobriedušās pākstis,

47
00:06:33,910 --> 00:06:36,685
iegūsiet vēlās sezonas šķirni. 

48
00:06:43,455 --> 00:06:46,890
Lai ievāktu sēklas - 
pākstīm jāizkalst.

49
00:06:49,380 --> 00:06:53,060
Dažas šķirnes 
(to skaitā - kāpaļājošās pupiņas)

50
00:06:53,640 --> 00:06:56,320
pieļauj ievākšanas perioda pagarinājumu. 

51
00:06:58,190 --> 00:07:03,235
Dažu pundurpupu paveidu pākstis 
izkalst vienlaicīgi.

52
00:07:04,340 --> 00:07:09,100
Tas nozīmē - sēklas ievācamas, 
vienā reizē nogriežot visus augus. 

53
00:07:14,765 --> 00:07:19,800
Ja laikapstākļi ir mitri 
un sēklu augi nav izkaltuši,

54
00:07:20,390 --> 00:07:23,625
tos var izlikt žāvēties - 
labi vēdinātā šķūnītī. 

55
00:07:29,245 --> 00:07:33,340
Svarīgi izmantot tīklu, 
lai sargātu augus no kukaiņiem. 

56
00:07:36,140 --> 00:07:40,520
Pēc ražas sēklas tiek žāvētas 
divas vai trīs nedēļas.

57
00:07:45,015 --> 00:07:48,075
Lai pārliecinātos par gatavību - 
iekodieties sēklā.

58
00:07:49,120 --> 00:07:52,475
Ja nepaliek iespiedums, sēkla izžuvusi. 

59
00:08:00,070 --> 00:08:03,415
Ievākšana - šķirošana - uzglabāšana

60
00:08:14,930 --> 00:08:18,620
Nelieliem apjomiem - 
tīrīšana veicama manuāli. 

61
00:08:21,480 --> 00:08:25,860
Lielākiem apjomiem - 
pākstis izkuļamas ar nūju.

62
00:08:26,915 --> 00:08:28,515
Pākstis var mīdīt arī kājām. 

63
00:08:43,585 --> 00:08:47,130
Kad pākstis pārspiestas, 
nepieciešams pupas izsijāt. 

64
00:08:47,485 --> 00:08:51,810
Sietā paliks pupas un lielākie atkritumi, 
kas izlasāmi ar rokām.

65
00:08:57,070 --> 00:09:04,550
Lai atbrīvotos no sīkāka izmēra pelavām - 
pārpalikumus var sijāt vai izvētīt. 

66
00:09:06,140 --> 00:09:08,895
Vētīšana notiek - 
pūšot pār pupām elpu,

67
00:09:09,305 --> 00:09:13,125
izmantojot ventilatoru 
vai nelielu kompresoru.

68
00:09:15,925 --> 00:09:21,520
Izlasiet neatbilstošu ārēju pazīmju pupas -
tās liecina par starpšķirņu krustošanos.

69
00:09:24,410 --> 00:09:29,695
Tāpat izlasiet bojātas vai deformētas, 
kā arī smecernieku apsēstas pupas. 

70
00:09:30,320 --> 00:09:36,280
Pupu smecernieks (Acanthoscelides obtectus)
 ir neliels kukainis,

71
00:09:36,650 --> 00:09:40,795
kas sadēj oliņas auga pāksts iekšienē.

72
00:09:41,475 --> 00:09:46,330
No oliņām viegli atbrīvoties - 
dažas dienas uzglabājot pupas saldētavā. 

73
00:09:57,980 --> 00:10:01,960
Vienmēr izveidojiet etiķeti 
ar sugas un šķirnes nosaukumu,

74
00:10:02,140 --> 00:10:07,845
kā arī sēklu ievākšanas gadu
un ievietojiet to paciņā kopā ar sēklām.

75
00:10:08,720 --> 00:10:11,255
Ārējs uzraksts mēdz nodilt. 

76
00:10:13,325 --> 00:10:17,615
Dažas dienas uzglabājiet paciņu saldētavā, 
lai nonāvētu parazītus. 

77
00:10:23,235 --> 00:10:26,955
Pupu sēklas 3 gadus saglabā lielisku dīgtspēju.

78
00:10:27,225 --> 00:10:31,665
Lai termiņu paildzinātu, 
glabājiet pupas saldētavā. 
