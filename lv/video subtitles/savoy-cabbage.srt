﻿1
00:00:09,480 --> 00:00:12,938
Virziņkāposti piederīgi 
Brassicaceae dzimtai,

2
00:00:13,510 --> 00:00:18,930
Brassica oleracea sugai 
un sabauda apakšsugai.

3
00:00:20,560 --> 00:00:25,025
Brassica oleracea sugā 
ietilpst arī kolrābji,

4
00:00:25,163 --> 00:00:31,396
brokoļi, kāposti, Briseles kāposti, 
lapu kāposti un puķkāposti. 

5
00:00:37,810 --> 00:00:41,949
Virziņkāpostiem raksturīgas 
krokotas lapas.

6
00:00:44,581 --> 00:00:48,196
Galviņa nav tik blīva 
kā kāpostiem.

7
00:00:50,220 --> 00:00:53,934
Sastopami pavasara virziņkāposti
un vasaras virziņkāposti,

8
00:00:54,138 --> 00:00:56,792
kuru lapas un galviņa 
nav cieši veidotas,

9
00:00:58,698 --> 00:01:01,912
kā arī virziņkāposti ar ļoti lielu galviņu,
kas paredzēti uzglabāšanai,

10
00:01:05,745 --> 00:01:10,240
un ziemas virziņkāposti, 
kuru galviņa ir zaļa un viegla. 

11
00:01:15,592 --> 00:01:20,340
Oleracea kāpostu
apputeksnēšana 

12
00:01:36,120 --> 00:01:40,240
Brassica oleracea ziedi 
ir hermafrodīti.

13
00:01:43,090 --> 00:01:47,265
Tas nozīmē - tiem ir gan vīrišķie,
gan sievišķie orgāni.

14
00:01:50,996 --> 00:01:53,490
Vairums ir pašsterili -

15
00:01:54,101 --> 00:01:59,156
viena auga ziedi spēj apputeksnēt 
tikai citu augu.

16
00:02:02,967 --> 00:02:05,178
Tādi augi ir alogāmi augi.

17
00:02:06,123 --> 00:02:10,945
Veiksmīgai apputeksnēšanai 
ieteicams kultivēt vairākus augus. 

18
00:02:14,501 --> 00:02:17,563
Ziedputekšņus izplata kukaiņi.

19
00:02:20,952 --> 00:02:25,403
Tādējādi tiek dabiski veicināta 
lieliska ģenētiskā daudzveidība.

20
00:02:32,356 --> 00:02:36,661
Visas Brassica oleracea 
kāpostu apakšsugas

21
00:02:36,814 --> 00:02:38,690
var savstarpēji krustoties.

22
00:02:40,276 --> 00:02:44,923
Tādēļ dažādu šķirņu sēklas kāpostus 
nevajadzētu audzēt tiešā tuvumā. 

23
00:02:49,447 --> 00:02:55,192
Šķirnes tīrības nolūkos - 
dažādas Brassica oleracea šķirnes

24
00:02:55,476 --> 00:02:58,690
stādāmas vismaz 1 km atstatumā. 

25
00:03:00,000 --> 00:03:04,378
Attālums samazināms līdz 500 metriem, 
ja starp šķirnēm atrodas dabiska barjera

26
00:03:04,472 --> 00:03:07,287
(piemēram, dzīvžogs). 

27
00:03:10,603 --> 00:03:14,680
Tāpat šķirnes iespējams izolēt 
norobežotos kukaiņu tīklos,

28
00:03:14,763 --> 00:03:18,334
kuros ievietotas 
nelielas kukaiņu ligzdiņas,

29
00:03:23,389 --> 00:03:27,090
vai arī tīklus pamīšus 
atverot un aizverot.

30
00:03:31,440 --> 00:03:32,618
Ar izolēšanas metodēm

31
00:03:32,843 --> 00:03:36,860
iepazīstieties atbilstošajā 
‘’Sēklu ražošanas ābeces’’ modulī. 

32
00:03:50,180 --> 00:03:53,862
Dzīves cikls

33
00:03:54,974 --> 00:03:57,680
Virziņkāposts ir divgadīgs augs.

34
00:03:58,705 --> 00:04:03,643
Pirmajā cikla gadā - sēklai audzētie virziņkāposti 
tiek kultivēti tāpat kā pārtikas ieguvei.

35
00:04:08,327 --> 00:04:10,596
Sēklas attīstīsies otrajā gadā. 

36
00:04:50,720 --> 00:04:56,436
Ģenētisko daudzveidību 
nodrošinās 10 - 15 stādi.

37
00:04:57,220 --> 00:05:00,843
Virziņkāpostu sēklas tiek iegūtas 
no veselīgiem augiem,

38
00:05:01,112 --> 00:05:04,465
kas novēroti visos 
attīstības posmos.

39
00:05:05,440 --> 00:05:09,980
Tādējādi iespējams sekot 
šķirnes specifiskajām īpašībām. 

40
00:05:10,356 --> 00:05:15,774
Izvēlieties spēcīgākās galviņas, 
kas atbilst selekcijas kritērijiem:

41
00:05:17,309 --> 00:05:22,865
vienmērīga, aktīva augšana, 
strauja galviņu veidošanās,

42
00:05:25,200 --> 00:05:28,660
laba uzglabājamība, agrīnība,

43
00:05:30,887 --> 00:05:33,310
ziemcietība, noturība pret slimībām.

44
00:05:36,138 --> 00:05:41,520
Jāņem vērā arī 
šķirnes tipiskā forma,

45
00:05:42,080 --> 00:05:45,100
spicā, plakanā vai apaļā galviņa,

46
00:05:46,974 --> 00:05:52,472
īsais kāts, spēcīgā sakņu sistēma, 
garša un krāsa. 

47
00:05:55,934 --> 00:06:01,047
Virziņkāposti aukstumu (līdz pat -15°C) 
panes labāk

48
00:06:01,483 --> 00:06:06,501
nekā citas 
Brassica oleracea sugas.

49
00:06:06,890 --> 00:06:10,254
Vairākums šķirņu 
var pārziemot dārzā. 

50
00:06:23,310 --> 00:06:26,618
Pārējās ieziemošanas 
un dzīves cikla otrā gada metodes

51
00:06:26,712 --> 00:06:30,340
ir tādas pašas - kā kāpostiem.

52
00:07:07,636 --> 00:07:14,340
Brassica oleracea
ievākšana, šķirošana un uzglabāšana

53
00:07:23,505 --> 00:07:27,250
Sēklas ir gatavas - 
pākstīm vēršoties smilškrāsā.

54
00:07:31,483 --> 00:07:34,007
Pākstis viegli pārsprāgst.

55
00:07:34,196 --> 00:07:39,294
Tas nozīmē - nobriedušas pākstis 
viegli atveras un strauji izplata sēklas. 

56
00:07:47,781 --> 00:07:51,963
Lielākoties visi stiebri 
nenobriest vienlaicīgi.

57
00:07:52,836 --> 00:07:59,069
Lai nezaudētu sēklas, tās ievācamas,
līdzko nobriedis katrs atsevišķais stiebrs.

58
00:08:00,676 --> 00:08:06,298
Iekams nogatavojušās visas sēklas - 
var ievākt arī augu kopumā. 

59
00:08:09,636 --> 00:08:14,800
Briedināšana turpināma - 
žāvējot augu sausā,

60
00:08:14,950 --> 00:08:16,625
labi vēdinātā vietā. 

61
00:08:29,236 --> 00:08:35,374
Kāpostu sēklas ievācamas, 
kad pākstis viegli atveramas ar pirkstiem. 

62
00:08:37,403 --> 00:08:38,690
Lai ievāktu sēklas -

63
00:08:38,800 --> 00:08:44,160
pākstis tiek izbērtas uz plastmasas plēves 
vai bieza auduma gabala.

64
00:08:44,581 --> 00:08:47,461
Tad pākstis tiek izkultas 
vai saberzētas plaukstās.

65
00:08:50,930 --> 00:08:56,021
Pākstis var arī ievietot maisiņā - 
un izkult pret mīkstu virsmu. 

66
00:08:57,420 --> 00:09:02,072
Lielāki apjomi kuļami - 
mīdot vai pārbraucot tiem pāri. 

67
00:09:14,600 --> 00:09:20,000
Ja pākstis neatveras viegli - 
tajās slēpjas nenobriedušas sēklas,

68
00:09:20,130 --> 00:09:21,796
kam būs raksturīga vāja dīgtspēja. 

69
00:09:26,581 --> 00:09:27,898
Šķirošanas gaitā -

70
00:09:28,130 --> 00:09:32,690
sēklas tiek sijātas 
rupjākā sietā,

71
00:09:33,025 --> 00:09:34,740
kas aiztur pelavas.

72
00:09:39,505 --> 00:09:42,043
Tad sēklas tiek sijātas 
citā sietā,

73
00:09:42,501 --> 00:09:47,060
kas aiztur sēklas, 
bet atsijā smalkākās daļiņas. 

74
00:09:50,760 --> 00:09:54,610
Pēdīgi - sēklas vētījamas, 
pūšot pār tām

75
00:09:55,040 --> 00:09:59,025
vai izmantojot vēja spēku, 
lai aizvāktu pelavu paliekas. 

76
00:10:14,225 --> 00:10:19,180
Visu Brassica oleracea kāpostu sēklas 
ir savstarpēji līdzīgas.

77
00:10:20,174 --> 00:10:24,101
Grūti atšķirt, piemēram,

78
00:10:24,400 --> 00:10:26,865
kāpostu un puķkāpostu sēklas.

79
00:10:27,381 --> 00:10:30,320
Tādēļ svarīgi augus iezīmēt

80
00:10:30,589 --> 00:10:34,349
un iegūtajām sēklām 
pievienot etiķeti ar sugas

81
00:10:34,600 --> 00:10:37,643
un šķirnes nosaukumu, 
kā arī ievākšanas gadu. 

82
00:10:39,076 --> 00:10:43,767
Dažas dienas uzglabājot sēklas saldētavā, 
tiks iznīcināti parazīti. 

83
00:10:49,280 --> 00:10:52,429
Kāpostu sēklu dīgtspēja 
saglabājas 5 gadus.

84
00:10:53,512 --> 00:10:57,200
Taču dīgšanas kapacitāte 
var ieilgt līdz pat 10 gadiem.

85
00:10:58,880 --> 00:11:02,290
Termiņš pagarināms - 
glabājot sēklas saldētavā.

86
00:11:03,243 --> 00:11:09,680
Vienā gramā ietilpst 250 - 300 sēklas 
(atbilstoši šķirnes īpašībām). 
