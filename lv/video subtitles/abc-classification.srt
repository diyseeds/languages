﻿1
00:00:08,000 --> 00:00:09,675
Botāniskā klasifikācija

2
00:00:22,555 --> 00:00:27,580
Ikvienam sēklu ražotājam nepieciešamas 
vispārīgas zināšanas botānikā -

3
00:00:27,940 --> 00:00:29,420
augu zinātnē.

4
00:00:37,860 --> 00:00:42,860
Mūsdienās augi tiek klasificēti
saskaņā ar to ziedu, vairošanās orgānu

5
00:00:43,220 --> 00:00:46,140
un augļu anatomiju.

6
00:00:47,460 --> 00:00:50,220
Tos klasificē atbilstoši latīniskajam nosaukumam. 

7
00:00:57,325 --> 00:01:00,780
Latīniskie apzīmējumi 
ir precīzāki,

8
00:01:01,180 --> 00:01:04,420
savukārt ikdienā lietotie nosaukumi 
var radīt apjukumu.

9
00:01:09,285 --> 00:01:13,260
Piemēram, ''ķirbis''
var apzīmēt gluži atšķirīgas sugas:

10
00:01:13,460 --> 00:01:19,740
Cucurbita pepo, Cucurbita moschata
un Cucurbita maxima. 

11
00:01:24,635 --> 00:01:28,700
Īsfilmās tiks izmantoti tikai latīniskie nosaukumi:

12
00:01:30,100 --> 00:01:32,780
dzimta (piemēram, Fabaceae,

13
00:01:34,680 --> 00:01:35,940
Asteraceae,

14
00:01:38,200 --> 00:01:39,280
Solanaceae

15
00:01:42,195 --> 00:01:43,540
vai Cucurbitaceae),

16
00:01:45,765 --> 00:01:48,820
ģints (piemēram, Cucurbita),

17
00:01:49,180 --> 00:01:54,540
suga (piemēram, Cucurbita maxima)
un pēdīgi - šķirne. 

18
00:01:59,665 --> 00:02:04,580
Piemēram, gurķis ir piederīgs 
Cucurbitaceae dzimtai,

19
00:02:05,060 --> 00:02:09,500
Cucumis ģintij 
un Cucumis sativus sugai.

20
00:02:10,820 --> 00:02:13,300
Suga iedalāma vairākās šķirnēs. 

21
00:02:20,735 --> 00:02:23,900
Arī melone ietilpst 
Cucurbitaceae dzimtā

22
00:02:24,340 --> 00:02:26,295
un Cucumis ģintī,

23
00:02:26,850 --> 00:02:30,300
taču piederīga atšķirīgai sugai - Cucumis melo,

24
00:02:30,740 --> 00:02:34,100
kas tāpat iedalāma vairākās šķirnēs. 

25
00:02:39,240 --> 00:02:44,180
Vienas šķirnes augi būs apveltīti 
ar līdzīgām īpašībām:

26
00:02:44,260 --> 00:02:47,620
līdzīgu apveidu, krāsu un izmēru.

27
00:02:49,730 --> 00:02:54,220
Visas vienai sugai piederīgas šķirnes 
var savstarpēji krustoties. 

28
00:02:56,245 --> 00:03:00,820
Dažādām sugām piederīgas šķirnes 
parasti nemēdz krustoties.

29
00:03:01,840 --> 00:03:05,975
Piemēram, gurķis 
nekad nekrustosies ar meloni. 

30
00:03:21,440 --> 00:03:23,255
Tomēr pastāv izņēmumi.

31
00:03:24,475 --> 00:03:28,570
Krustošanās iespējama 
starp botāniski radniecīgām sugām,

32
00:03:29,255 --> 00:03:32,700
piemēram, starp Cucurbita moschata

33
00:03:32,820 --> 00:03:36,180
un Cucurbita argyrosperma indivīdiem. 

34
00:03:39,220 --> 00:03:43,100
Atšķirīgām ģintīm piederīgu augu krustošanās 
nav iespējama. 

35
00:03:52,435 --> 00:03:53,620
Tamlīdzīgas zināšanas

36
00:03:53,695 --> 00:03:58,260
ļaus izraudzīties sēklu ražošanai
piemērotākās kultivēšanas metodes,

37
00:03:58,540 --> 00:04:00,980
izvairoties no svešapputes riska. 
