1
00:00:09,295 --> 00:00:12,180
Melones piederīgas Cucurbitaceae dzimtai

2
00:00:13,775 --> 00:00:16,220
un Cucumis melo sugai.

3
00:00:23,080 --> 00:00:24,460
Tas ir viengadīgs augs,

4
00:00:24,665 --> 00:00:30,340
kas iedalāms vairākos paveidos
ar atšķirīgu formu, krāsu un garšu.

5
00:00:34,830 --> 00:00:40,625
Piemēri: muskusa melones, kantalupes,
cukurmelones

6
00:00:41,425 --> 00:00:44,680
un ziemas melones, 
kas uzglabājamas vairākus mēnešus. 

7
00:01:00,980 --> 00:01:01,980
Apputeksnēšana

8
00:01:20,015 --> 00:01:22,620
Melones ir vienmājas augi -

9
00:01:24,595 --> 00:01:29,080
uz viena auga atrodami 
gan vīrišķie, gan sievišķie ziedi.

10
00:01:29,975 --> 00:01:33,200
Sievišķo ziedu sēklotne 
slēpjas zem zieda.

11
00:01:34,000 --> 00:01:37,420
Tas ir melones aizmetnis, 
kas apaugļots attīstīsies. 

12
00:01:40,955 --> 00:01:44,285
Vīrišķie ziedi atrodas 
garu ziedkātu galos.

13
00:01:49,275 --> 00:01:51,835
Ziedi atveras tikai vienu dienu. 

14
00:01:55,750 --> 00:01:57,660
Melone spēj pašapaugļoties -

15
00:01:58,165 --> 00:02:02,620
sievišķo ziedu var apputeksnēt 
tā paša auga vīrišķā zieda

16
00:02:02,760 --> 00:02:03,960
putekšņi. 

17
00:02:07,765 --> 00:02:10,970
Tomēr izplatītāka ir svešappute.

18
00:02:11,920 --> 00:02:16,250
Meloņu ziedus apputeksnē kukaiņi 
(īpaši - bites).

19
00:02:23,730 --> 00:02:30,145
Visas Cucumis melo šķirnes var krustoties 
savstarpēji un arī ar savvaļas melonēm.

20
00:02:30,465 --> 00:02:37,155
Taču meloņu, gurķu, arbūzu un ķirbju 
savstarpēja krustošanās nav iespējama. 

21
00:02:41,950 --> 00:02:47,725
Lai šķirnes nekrustotos, 
starp tām ievērojams 1 km atstatums. 

22
00:02:49,240 --> 00:02:54,665
Attālums samazināms līdz 400 m, 
ja pastāv dabiska barjera, piemēram, dzīvžogs. 

23
00:02:59,560 --> 00:03:04,460
Vienā dārzā iespējams audzēt 
vairākas sēklai paredzēto

24
00:03:04,645 --> 00:03:06,080
meloņu šķirnes. 

25
00:03:08,205 --> 00:03:14,010
Pirmā metode: viena šķirne tiek pārklāta 
ar tīklu, kurā ievietojama kameņu ligzda. 

26
00:03:25,520 --> 00:03:29,820
Otrā metode: divas šķirnes 
pārklāj katru ar savu tīklu.

27
00:03:30,800 --> 00:03:37,125
Vienā dienā atver vienas šķirnes tīklu, 
bet nākamajā - otras šķirnes tīklu.

28
00:03:39,210 --> 00:03:41,625
Ziedus apputeksnēs savvaļas kukaiņi.

29
00:03:44,270 --> 00:03:49,530
Ražīgums būs pazemināts, 
jo daži ziedi netiks apaugļoti. 

30
00:03:52,590 --> 00:03:55,985
Trešā metode: 
ziedu manuāla apputeksnēšana.

31
00:03:58,080 --> 00:04:00,940
Melones apputeksnēt ir grūtāk 
nekā cukīni vai ķirbjus,

32
00:04:01,195 --> 00:04:03,220
jo meloņu ziedi ir ļoti sīki,

33
00:04:03,565 --> 00:04:06,895
turklāt grūti notvert 
ziedēšanas brīdi.

34
00:04:08,035 --> 00:04:12,820
80% meloņu sievišķo ziedu 
nobirst neattīstījušies.

35
00:04:13,970 --> 00:04:17,540
Manuāla apputeksnēšana 
nav tik efektīva kā kukaiņu veiktā.

36
00:04:22,430 --> 00:04:28,390
Tikai 10-15% no manuāli apputeksnētajiem 
ziediem izveidos augļus. 

37
00:04:35,305 --> 00:04:38,740
Visas trīs metodes 
sīkāk raksturotas

38
00:04:38,880 --> 00:04:42,100
‘’Sēklu ražošanas ābeces’’ 
izolēšanas paņēmieniem

39
00:04:42,465 --> 00:04:47,055
un manuālajai apputeksnēšanai 
veltītajos moduļos. 

40
00:04:51,100 --> 00:04:52,260
Dzīves cikls 

41
00:05:09,315 --> 00:05:13,740
Sēklai audzētās melones tiek kultivētas
 tāpat kā pārtikas ieguvei.

42
00:05:43,035 --> 00:05:47,180
Temperatūras prasības 
ir atbilstošas šķirnei.

43
00:05:48,890 --> 00:05:52,130
Ģenētiskās daudzveidības nodrošināšanai 
ieteicams audzēt

44
00:05:52,220 --> 00:05:54,290
vismaz 6 augus.

45
00:05:55,840 --> 00:05:57,755
Vislabāk - audzēt duci. 

46
00:06:01,755 --> 00:06:04,860
Rūpīgi atlasiet 
sēklu ieguvei paredzētos augus -

47
00:06:05,040 --> 00:06:09,685
saskaņā ar šķirnes 
specifiskajiem kritērijiem: agrīnība,

48
00:06:10,240 --> 00:06:11,100
dzīvelīgums,

49
00:06:12,645 --> 00:06:13,845
augļu skaits,

50
00:06:15,280 --> 00:06:19,415
spēja augt zem atklātas debess 
mērena klimata apstākļos,

51
00:06:20,960 --> 00:06:23,500
augļa mīkstuma garša un saldenums.

52
00:06:26,330 --> 00:06:27,830
Atbrīvojieties no neveseliem augiem. 

53
00:06:39,085 --> 00:06:42,820
Melones sēklu gatavības pakāpe 
viegli nosakāma -

54
00:06:43,410 --> 00:06:46,260
auglim jābūt gana nobriedušam,
 lai to ēstu. 

55
00:06:51,975 --> 00:06:54,980
Ievākšana - šķirošana - uzglabāšana

56
00:07:00,835 --> 00:07:04,340
Lai ievāktu sēklas, 
pārgrieziet meloni uz pusēm.

57
00:07:05,265 --> 00:07:07,820
Sēklas izgrebjamas ar karoti.

58
00:07:09,545 --> 00:07:11,380
Pārējo meloni iespējams gardi notiesāt. 

59
00:07:17,615 --> 00:07:24,420
Noskalojiet sēklas ūdenī 
un atstājiet žāvēties ēnainā vietā. 

60
00:07:30,330 --> 00:07:34,110
Kad sēklas būs izžuvušas - 
tās liecot lūzīs. 

61
00:07:46,965 --> 00:07:50,300
Iepakojumā vienmēr ievietojiet 
etiķeti ar sugas un šķirnes

62
00:07:50,370 --> 00:07:54,980
nosaukumu, kā arī ievākšanas gadu
(ārējs uzraksts mēdz nodilt). 

63
00:08:02,380 --> 00:08:06,780
Dažas dienas uzglabājiet sēklas saldētavā, 
lai nonāvētu parazītus. 

64
00:08:12,065 --> 00:08:15,730
Meloņu sēklas dīgtspēju saglabā 
vidēji 5 gadus,

65
00:08:15,950 --> 00:08:18,740
taču tās mēdz dīgt arī pēc 10 gadiem. 
