1
00:00:13,460 --> 00:00:16,836
Rāceņi piederīgi 
Brassicaceae dzimtai,

2
00:00:17,396 --> 00:00:21,221
Brassica rapa sugai 
un rapa apakšsugai.

3
00:00:24,167 --> 00:00:28,152
Brassica rapa sugā ietilpst 
daudzskaitlīgas apakšsugas:

4
00:00:32,200 --> 00:00:37,780
pakčojs, komatsuna 
jeb japāņu sinepjspināti,

5
00:00:38,880 --> 00:00:41,150
raab brokoļi 
un daudzi citi dārzeņi. 

6
00:00:47,112 --> 00:00:53,660
Visām rāceņu šķirnēm raksturīga 
strauja attīstība un formas daudzveidība.

7
00:00:54,520 --> 00:00:56,560
Atšķiras arī krāsa.

8
00:01:03,070 --> 00:01:04,781
Atbilstoši šķirnei -

9
00:01:05,083 --> 00:01:10,000
saknes mēdz plesties augsnes virskārtā
vai ir nedaudz / daļēji ietiekušās zemē.

10
00:01:12,567 --> 00:01:17,280
Sastopamas agrīnās 
un uzglabājamās šķirnes. 

11
00:01:22,094 --> 00:01:23,214
Apputeksnēšana 

12
00:01:30,509 --> 00:01:37,003
Vairums Brassica rapa sugas šķirņu apveltītas
ar koši dzelteniem, hermafrodītiem ziediem,

13
00:01:37,454 --> 00:01:39,680
kas nav pašsavietojami -

14
00:01:40,312 --> 00:01:43,236
auga ziedputekšņi ir auglīgi,

15
00:01:43,469 --> 00:01:47,323
taču spēj apputeksnēt 
tikai cita auga ziedus. 

16
00:01:53,585 --> 00:01:58,487
Tādēļ rāceņi ir alogāmi augi, 
ko apputeksnē kukaiņi. 

17
00:02:05,229 --> 00:02:07,796
Tas nozīmē - rāceņu šķirnes

18
00:02:07,900 --> 00:02:11,476
nepieciešams audzēt nodalīti 
no visām Ķīnas kāpostu,

19
00:02:11,960 --> 00:02:16,778
pakčoju, japāņu sinepjspinātu, 
raab brokoļu šķirnēm

20
00:02:17,003 --> 00:02:20,072
un citām Brassica rapa apakšsugām. 

21
00:02:23,520 --> 00:02:26,110
Lai nodrošinātu šķirnes tīrību -

22
00:02:26,392 --> 00:02:31,069
divas dažādas šķirnes audzējamas 
vismaz 1 km atstatumā. 

23
00:02:33,840 --> 00:02:37,054
Attālums samazināms līdz 500 m,

24
00:02:37,396 --> 00:02:41,614
ja pastāv dabiska barjera, 
piemēram, dzīvžogs. 

25
00:02:47,770 --> 00:02:50,181
Tāpat šķirnes iespējams izolēt -

26
00:02:50,414 --> 00:02:53,956
pamīšus atverot un aizverot 
šķirnēm pārklātus kukaiņu tīklus

27
00:02:55,752 --> 00:03:00,014
vai ievietojot nelielas kukaiņu ligzdas 
slēgtā tīkla sistēmā.

28
00:03:04,680 --> 00:03:05,701
Sīkāk -

29
00:03:05,883 --> 00:03:10,174
‘’Sēklu ražošanas ābeces’’ 
izolēšanas metodēm veltītajā modulī! 

30
00:03:17,607 --> 00:03:18,940
Dzīves cikls

31
00:03:35,316 --> 00:03:39,985
Dažas agrīnās pavasara šķirnes 
uzskatāmas par viengadīgiem augiem.

32
00:03:46,560 --> 00:03:49,861
Tomēr vairums rāceņu šķirņu 
ir divgadīgas.

33
00:03:50,520 --> 00:03:51,803
Mērenā klimatā -

34
00:03:51,910 --> 00:03:55,127
tās sējamas āra augsnē 
(jūlija vidū).

35
00:03:55,250 --> 00:03:58,894
Sēklas attīstīsies tikai otrajā 
kultivēšanas gadā. 

36
00:04:10,283 --> 00:04:13,032
Sēklu ieguvei audzētie rāceņi

37
00:04:13,163 --> 00:04:16,180
kultivējami tāpat 
kā pārtikas patēriņam. 

38
00:04:48,850 --> 00:04:53,540
Sēklu ražošanai 
izvēlieties veselīgus augus,

39
00:04:53,912 --> 00:04:56,749
kas novēroti 
visos attīstības posmos,

40
00:04:56,860 --> 00:05:05,061
lai sekotu šķirnes rādītājiem: dzīvelīgums, 
agrīnība, noturība pret slimībām. 

41
00:05:13,214 --> 00:05:15,540
Rudenī, ievācot ražu,

42
00:05:15,876 --> 00:05:20,756
atlasiet aptuveni 30 veselīgus augus,
kam ir pienācīga forma, krāsa un izmērs.

43
00:05:29,534 --> 00:05:31,980
Neizvēlieties augus 
ar pārāk lielām saknēm -

44
00:05:32,567 --> 00:05:35,716
tās ziemas laikā mēdz pūt. 

45
00:05:43,098 --> 00:05:47,083
Nenogrieziet ne saknes, 
ne lapu pamatni pie kakliņa. 

46
00:05:53,230 --> 00:05:55,463
Bargāku ziemu reģionos -

47
00:05:55,563 --> 00:06:01,789
rāceņus glabā smilšu kastēs, 
sausā pagrabā (vēlams - ar klona grīdu).

48
00:06:04,196 --> 00:06:08,830
Pagraba temperatūrai vajadzētu pieturēties 
no 0°C līdz vairākiem grādiem virs nulles. 

49
00:06:17,825 --> 00:06:18,900
Ziemas gaitā

50
00:06:19,316 --> 00:06:23,700
saknes regulāri pārbaudiet, 
aizvācot visas iepuvušās. 

51
00:06:32,280 --> 00:06:36,196
Dažas šķirnes ar blīvāku mīkstumu 
ir salcietīgas -

52
00:06:36,400 --> 00:06:38,530
tās pārziemos augsnē.

53
00:06:41,745 --> 00:06:45,090
Maigāka klimata reģionos 
(bez barga sala) -

54
00:06:45,505 --> 00:06:48,232
visas rāceņu šķirnes 
var ziemot dārzā.

55
00:06:49,265 --> 00:06:53,963
No vieglām salnām 
augus aizsargās

56
00:06:54,269 --> 00:06:56,523
pretsala pārklājums 
vai 10 cm zemes slānis. 

57
00:06:59,636 --> 00:07:03,127
Saknes tiek atlasītas pavasarī - 
ražas laikā.

58
00:07:11,956 --> 00:07:13,700
Nogrieziet lapas virs kakliņa

59
00:07:22,940 --> 00:07:26,232
un pārstādiet rāceņus, 
ierokot zemē 2/3 no saknes.

60
00:07:27,660 --> 00:07:29,570
Pārstādītās saknes dāsni jālaista. 

61
00:07:37,367 --> 00:07:40,501
No rāceņiem, kas ziemā 
uzglabāti iekštelpās,

62
00:07:40,920 --> 00:07:45,636
aizvācamas iebojātās saknes
(pārstādiet tikai veselos rāceņus).

63
00:07:57,120 --> 00:07:59,229
Arīdzan - bagātīgi aplaistiet. 

64
00:08:12,410 --> 00:08:19,430
Rāceņi sasniegs vismaz 1 m garumu. 
Tie balstāmi ar mietiņiem. 

65
00:08:46,981 --> 00:08:50,940
Rāceņu sēklu ievākšana, 
šķirošana un uzglabāšana

66
00:08:51,083 --> 00:08:55,054
norisinās tāpat kā Brassica oleraceae 
sugas kāpostiem. 

67
00:09:01,876 --> 00:09:06,123
Sēklas ir gatavas - 
pākstīm vēršoties smilškrāsā.

68
00:09:07,810 --> 00:09:09,360
Pākstis viegli pāršķeļas.

69
00:09:09,563 --> 00:09:14,494
Nobriedušas pākstis viegli atveras 
un strauji izplata sēklas.

70
00:09:24,596 --> 00:09:28,305
Lielākoties - visi kāti
nenogatavojas vienlaicīgi.

71
00:09:29,880 --> 00:09:35,200
Lai nezaudētu sēklas, tās ievāc - 
nobriestot katram stiebram atsevišķi.

72
00:09:37,620 --> 00:09:42,810
Pirms nogatavojušās visas sēklas - 
var ievākt arī augu kopumā. 

73
00:09:46,421 --> 00:09:48,000
Briedināšana turpināma -

74
00:09:48,240 --> 00:09:52,007
žāvējot augu sausā, 
labi vēdinātā vietā,

75
00:09:52,087 --> 00:09:53,556
kam nepiekļūst saules gaisma. 

76
00:10:05,985 --> 00:10:08,661
Rāceņu sēklas iespējams ievākt,

77
00:10:08,916 --> 00:10:12,000
kad pākstis 
viegli pārspiežamas pirkstos. 

78
00:10:15,127 --> 00:10:16,458
Lai ievāktu sēklas -

79
00:10:16,567 --> 00:10:21,970
pākstis izklāj pār plastmasas plēvi 
vai biezu auduma gabalu.

80
00:10:22,378 --> 00:10:25,112
Tad pākstis izkuļ vai saberzē plaukstās.

81
00:10:28,240 --> 00:10:33,258
Pākstis iespējams ievietot maišelī - 
un izkult pret mīkstu virsmu. 

82
00:10:35,105 --> 00:10:42,152
Lielāki apjomi kuļami - 
mīdot vai pārbraucot tiem pāri. 

83
00:10:52,360 --> 00:10:57,723
Ja pākstis neatveras viegli, 
tajās slēpjas nenobriedušas sēklas,

84
00:10:57,898 --> 00:10:59,738
kam būs raksturīga 
vāja dīgtspēja. 

85
00:11:04,341 --> 00:11:10,334
Šķirošanas gaitā - sēklas tiek sijātas 
rupjākā sietā,

86
00:11:10,843 --> 00:11:12,320
kas aiztur pelavas.

87
00:11:17,243 --> 00:11:19,970
Tad sēklas tiek sijātas citā sietā,

88
00:11:20,276 --> 00:11:24,820
kas aiztur sēklas, 
bet atsijā smalkākās daļiņas. 

89
00:11:28,520 --> 00:11:32,465
Pēdīgi - sēklas vētījamas, 
pūšot pār tām

90
00:11:32,792 --> 00:11:36,938
vai izmantojot vēja iedarbību, 
lai aizvāktu pelavu paliekas. 

91
00:11:55,469 --> 00:11:59,469
Svarīgi maisiņā ievietot etiķeti 
ar sugas un šķirnes nosaukumu,

92
00:11:59,890 --> 00:12:05,498
kā arī ievākšanas gadu
(ārējs uzraksts mēdz nodilt). 

93
00:12:09,800 --> 00:12:14,938
Dažas dienas uzglabājot sēklas saldētavā, 
tiks iznīcināti parazīti. 

94
00:12:18,770 --> 00:12:21,810
Rāceņu sēklu dīgtspēja 
saglabājas vismaz 6 gadus.

95
00:12:28,196 --> 00:12:31,098
Termiņš pagarināms - 
glabājot sēklas saldētavā. 
