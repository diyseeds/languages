1
00:00:10,920 --> 00:00:16,378
Pastinaki piederīgi Apiaceae dzimtai 
un Pastinaca sativa sugai.

2
00:00:17,672 --> 00:00:20,356
Divgadīgo augu kultivē sakņu ieguvei.

3
00:00:25,534 --> 00:00:27,898
Lielākoties pastinaki ir balti,

4
00:00:28,174 --> 00:00:33,352
taču dažādas šķirnes pielāgojušās 
atšķirīgiem augsnes un klimata nosacījumiem.

5
00:00:43,020 --> 00:00:44,020
Apputeksnēšana 

6
00:00:58,523 --> 00:01:03,534
Pastinaku ziedkopa ir čemurs, 
kas sastāv no sīkiem ziediņiem,

7
00:01:03,621 --> 00:01:05,380
kas, visbiežāk, ir hermafrodīti.

8
00:01:07,280 --> 00:01:13,563
Putekšņlapa (vīrišķais vairošanās orgāns) 
nobriest pirms auglenīcas

9
00:01:13,840 --> 00:01:15,294
(sievišķā vairošanās orgāna). 

10
00:01:17,803 --> 00:01:22,443
Tādēļ zieds nespēj 
veikt pašappaugļošanos.

11
00:01:23,469 --> 00:01:26,901
Tomēr ziedi neatveras vienlaicīgi,

12
00:01:27,345 --> 00:01:30,756
tādēļ pašappute iespējama 
vienā čemurā

13
00:01:32,320 --> 00:01:35,003
vai starp viena auga
dažādiem čemuriem. 

14
00:01:37,389 --> 00:01:41,454
Viens otru apaugļot 
var arī atšķirīgu augu čemuri. 

15
00:01:43,636 --> 00:01:49,221
Pastinaks ir alogāms augs, 
kuru galvenokārt apputeksnē kukaiņi,

16
00:01:49,723 --> 00:01:55,498
īpaši - Diptera un Lepidoptera 
kārtu pārstāvji,

17
00:01:56,080 --> 00:01:58,058
kā arī bizbizmārītes,

18
00:01:58,443 --> 00:02:02,509
kuras pievilina pastinaku čemuri, 
kas nereti klāti ar laputīm.

19
00:02:05,418 --> 00:02:08,385
Pastinaki krustojas ar savvaļas pastinakiem. 

20
00:02:11,098 --> 00:02:13,230
Lai novērstu krustošanos,

21
00:02:13,825 --> 00:02:18,130
pastinaku šķirnes audzējamas 
vismaz 300 m atstatumā. 

22
00:02:22,180 --> 00:02:26,740
Attālums samazināms līdz 100 m, 
ja pastāv dabiska barjera,

23
00:02:26,829 --> 00:02:29,425
piemēram, dzīvžogs. 

24
00:02:31,960 --> 00:02:37,127
Izolēšanas nolūkos - divas šķirnes 
pārklājamas ar nošķirtiem tīkliem,

25
00:02:37,832 --> 00:02:40,552
no kuriem viens atverams, 
kamēr otrs paliek aizvērts

26
00:02:40,872 --> 00:02:42,312
(pamīšus - ik pārdienu).

27
00:02:44,036 --> 00:02:48,480
Iespējams vienu šķirni pārklāt 
ar slēgtu kukaiņu tīklu,

28
00:02:48,930 --> 00:02:51,003
sprostā ievietojot kameņu ligzdu.

29
00:02:52,843 --> 00:02:54,225
(Sīkāka informācija -

30
00:02:54,436 --> 00:02:58,436
izolēšanas metodēm veltītajā 
‘’Sēklu ražošanas ābeces’’ modulī.) 

31
00:03:04,020 --> 00:03:05,170
Dzīves cikls 

32
00:03:16,007 --> 00:03:21,054
Sēklai audzētie pastinaki 
tiek kultivēti tāpat kā pārtikas ieguvei,

33
00:03:22,443 --> 00:03:26,603
taču sēklas nogatavosies 
tikai cikla otrajā gadā.

34
00:03:48,080 --> 00:03:55,083
Sezonas sākumā sēklu dīgšanas process 
var ieilgt - līdz 3 nedēļām. 

35
00:04:20,552 --> 00:04:21,883
Kad sēklas nobriedušas,

36
00:04:22,014 --> 00:04:28,014
sēklu pastinaki ieziemojami 
dažādos veidos. 

37
00:04:31,730 --> 00:04:34,312
Rudenī saknes iespējams izrakt.

38
00:04:40,880 --> 00:04:45,570
Saknes jāatlasa - 
atbilstoši šķirnes kritērijiem:

39
00:04:45,949 --> 00:04:48,378
krāsa, forma, dzīvelīgums.

40
00:04:49,112 --> 00:04:54,661
Saknes notīra, nelietojot ūdeni.
Lapas nogriež virs kakliņa.

41
00:04:55,149 --> 00:04:58,923
Saknes neilgu laiku tiek žāvētas 
zem atklātas debess.

42
00:05:02,349 --> 00:05:04,429
Turpmāk tās uzglabājamas pagrabā.

43
00:05:18,276 --> 00:05:21,301
Ziemā saknes 
regulāri pārbaudāmas,

44
00:05:21,541 --> 00:05:24,174
aizvācot visas iepuvušās. 

45
00:05:27,883 --> 00:05:31,120
Visvienkāršāk - 
atstāt pastinakus dārza augsnē.

46
00:05:32,589 --> 00:05:35,083
Pastinaki ir vieni no izturīgākajiem augiem.

47
00:05:35,316 --> 00:05:39,374
Tie pārziemo zemē - 
arī barga sala reģionos.

48
00:05:40,152 --> 00:05:43,294
Sals pat uzlabo auga garšu. 

49
00:05:44,720 --> 00:05:48,330
Kad bargs sals vairs nav gaidāms, 
izrociet augus

50
00:05:48,480 --> 00:05:51,060
un atlasiet piemērotākos - 
sēklu ražošanai.

51
00:05:55,280 --> 00:05:57,709
Pārstādiet pastinakus pavasara sākumā.

52
00:05:58,640 --> 00:06:01,850
Ierociet tos, lai kakliņš 
būtu vienā līmenī ar zemi

53
00:06:02,014 --> 00:06:07,687
vai nedaudz zem augsnes virskārtas
(atstatums starp augiem: 60 - 90 cm). 

54
00:06:13,381 --> 00:06:16,392
Tuvu stādītiem pastinakiem 
veidojas mazāk terciāro čemuru,

55
00:06:16,560 --> 00:06:21,781
kam raksturīgas 
vājākas kvalitātes sēklas. 

56
00:06:26,043 --> 00:06:28,923
Ģenētiskās daudzveidības uzturēšanai 
nepieciešami

57
00:06:29,025 --> 00:06:30,981
vismaz 15 - 20 augi. 

58
00:07:05,330 --> 00:07:09,098
Sēklai audzētie pastinaki ir iespaidīgi augi,

59
00:07:09,410 --> 00:07:12,203
kas ražīgākās sezonās 
var sasniegt 2 m garumu.

60
00:07:16,116 --> 00:07:18,780
Tos var nākties balstīt ar mietiņu. 

61
00:07:23,876 --> 00:07:29,003
Pastinaki veido vairākus čemurus,
kas nezied vienlaicīgi.

62
00:07:31,927 --> 00:07:37,170
Pirmais (primārais) čemurs 
atrodas galvenā kāta galā. 

63
00:07:39,520 --> 00:07:44,494
Sekundārie čemuri 
attīstās no galvenā kāta. 

64
00:07:46,690 --> 00:07:50,829
Terciārie čemuri izaug 
no sekundārajiem kātiem.

65
00:07:53,127 --> 00:07:58,705
Ieteicams ievākt primāros čemurus, 
jo tie saražo labākās sēklas.

66
00:07:59,556 --> 00:08:03,900
Sekundāros čemurus ievāc 
tikai nepieciešamības gadījumā. 

67
00:08:30,218 --> 00:08:33,170
Čemurus nogriež 
kopā ar ziedkāta augšgalu,

68
00:08:33,352 --> 00:08:35,985
kad sāk izbirt 
pirmās nobriedušās sēklas.

69
00:08:37,360 --> 00:08:41,752
Tos var nogriezt pirms nobriešanas, 
jo sēklām raksturīga strauja izsēšanās.

70
00:08:51,781 --> 00:08:56,821
Turpmāk čemuri žāvējami 
sausā un labi vēdinātā vietā. 

71
00:09:05,272 --> 00:09:08,203
Ievākšana - šķirošana - uzglabāšana

72
00:09:16,400 --> 00:09:19,287
Sēklas no čemuriem izlasa manuāli.

73
00:09:20,101 --> 00:09:24,654
Izmantojiet cimdus, 
jo sēkas izdala ēteriskās eļļas,

74
00:09:24,760 --> 00:09:27,469
kas tiešā saules gaismā 
var radīt čūlas. 

75
00:09:31,854 --> 00:09:37,832
Sēklu tīrīšanai vispirms lietojams 
rupjš siets, kas aizturēs pelavas.

76
00:09:39,898 --> 00:09:44,930
ad sēklas tiek sijātas smalkākā sietā, 
kas aiztur sēklas, bet laiž cauri putekļus. 

77
00:09:57,000 --> 00:10:00,530
Iepakojumā vienmēr ievietojiet 
etiķeti ar sugas un šķirnes nosaukumu,

78
00:10:00,640 --> 00:10:05,345
kā arī ievākšanas gadu 
(ārējs uzraksts mēdz nodilt). 

79
00:10:10,400 --> 00:10:14,683
Dažas dienas uzglabājot sēklas saldētavā, 
ies bojā parazīti. 

80
00:10:18,060 --> 00:10:21,796
Pastinaku sēklas 
dīgtspēju saglabā tikai 1 gadu,

81
00:10:22,552 --> 00:10:25,512
taču termiņš pagarināms - 
sēklas glabājot saldētavā. 

82
00:10:26,698 --> 00:10:31,418
Vienā gramā ietilpst 
aptuveni 220 atsevišķas sēkliņas. 
