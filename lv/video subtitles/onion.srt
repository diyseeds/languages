1
00:00:09,820 --> 00:00:13,340
Parastais sīpols piederīgs 
Amaryllidaceae dzimtai,

2
00:00:13,775 --> 00:00:15,655
kas savulaik dēvēta - 
Alliaceae dzimta.

3
00:00:15,975 --> 00:00:18,610
Sīpols ietilpst Allium cepa sugā.

4
00:00:22,395 --> 00:00:26,430
Tas ir divgadīgs augs, 
kas pirmajā gadā attīsta sīpolu.

5
00:00:27,745 --> 00:00:31,090
Otrajā gadā augs uzzied 
un briedina sēklas.

6
00:00:35,585 --> 00:00:38,460
Dažas šķirnes ir agrīnas, 
citas - vēlās.

7
00:00:38,900 --> 00:00:42,550
Dažas uzglabājamas ilgstoši, 
citas - īslaicīgāk.

8
00:00:48,330 --> 00:00:54,910
Atbilstoši šķirnes īpatnībām - sīpoliem 
raksturīga atšķirīga forma, krāsa un garša.

9
00:01:01,780 --> 00:01:02,600
Apputeksnēšana

10
00:01:20,310 --> 00:01:25,540
Sīpolu ziedkopas veido 
sīki, hermafrodīti ziediņi,

11
00:01:27,590 --> 00:01:30,030
no kuriem katrs atsevišķi 
ir pašsterils.

12
00:01:34,520 --> 00:01:37,320
Ziedu putekšņus izplata kukaiņi.

13
00:01:39,330 --> 00:01:41,995
Tas nozīmē - 
sīpoli ir alogāmi augi.

14
00:01:43,960 --> 00:01:50,020
Kukaiņu svešappute izraisa 
šķirņu krustošanās risku.

15
00:01:57,180 --> 00:01:59,420
Lai to novērstu -

16
00:01:59,555 --> 00:02:04,160
starp divām šķirnēm ievērojiet 
vismaz 1 km atstatumu.

17
00:02:08,230 --> 00:02:14,270
Attālums samazināms līdz 200 m, 
ja pastāv dabiska barjera, piemēram, dzīvžogs.

18
00:02:18,665 --> 00:02:23,635
Šķirnes iespējams izolēt 
arī kukaiņu tīklu sistēmā.

19
00:02:25,185 --> 00:02:30,605
Pārsedziet vienas šķirnes augus ar pastāvīgu tīklu, 
kurā ievietota kameņu ligzda.

20
00:02:32,160 --> 00:02:35,770
Vai izmantojiet nošķirtus tīklus 
(katru savai šķirnei),

21
00:02:36,405 --> 00:02:41,200
kas tiek atvērti pamīšus: 
vienai šķirnei - pirmajā, otrai - otrā dienā.

22
00:02:42,945 --> 00:02:45,740
Ar izolēšanas metodēm 
sīkāk iepazīstieties

23
00:02:46,010 --> 00:02:48,900
specializētajā 
‘’Sēklu ražošanas ābeces’’

24
00:02:49,090 --> 00:02:51,000
modulī.

25
00:03:00,870 --> 00:03:02,020
Dzīves cikls

26
00:03:13,220 --> 00:03:19,560
Sēklai audzētos sīpolus kultivē 
tāpat kā ziemas uzglabāšanai.

27
00:03:23,600 --> 00:03:27,150
Sīpoli ražos sēklas 
tikai otrajā attīstības gadā.

28
00:03:35,115 --> 00:03:39,495
Sēklas sīpoli veiksmīgāk audzējami, 
sējot sēklas -

29
00:03:40,370 --> 00:03:46,585
nevis stādot sīpolus vai pumpursīpoliņus, 
jo tie mēdz izziedēt pārāk agri sezonā.

30
00:04:03,565 --> 00:04:08,315
Sīpoli, kas uzziedējuši pirmajā gadā, 
aizvācami -

31
00:04:09,155 --> 00:04:14,460
no tādu augu sēklām izaugušie sīpoli 
arī attīstīs pāragrus ziedus.

32
00:04:20,015 --> 00:04:26,305
Lai augs pirmajā gadā attīstītu sīpolu, 
nepieciešami silti laikapstākļi un garas dienas.

33
00:04:34,025 --> 00:04:37,535
Augi ievācami, 
kad sīpols nogatavojies.

34
00:04:46,740 --> 00:04:51,310
Lai pavasarī būtu iespējams 
pārstādīt 15 - 20 sīpolus,

35
00:04:51,860 --> 00:04:57,970
paturiet 20 - 30 no iepriekšējā rudens ražas, 
jo ne visi sīpoli veiksmīgi pārziemos.

36
00:05:02,495 --> 00:05:06,720
Atlasiet sīpolus 
saskaņā ar šķirnes kritērijiem:

37
00:05:06,990 --> 00:05:09,660
apveids, krāsa, izmērs utt.

38
00:05:13,035 --> 00:05:17,030
Augiem jābūt veselīgiem - 
ar skaistu, neieplīsušu mizu.

39
00:05:21,025 --> 00:05:25,800
Atbrīvojieties no neviendabīgiem 
un iešķeltiem sīpoliem.

40
00:05:30,700 --> 00:05:36,575
Atstājiet sīpolus žāvēties (10 - 12 dienas) 
siltā, labi vēdinātā vietā.

41
00:05:37,600 --> 00:05:42,540
Neieskrambājiet sīpolus, jo tādi augi 
ziemā būs pakļauti puves riskam.

42
00:05:46,340 --> 00:05:52,195
Ziemas uzglabāšanai sīpoli novietojami 
aukstā un labi vēdinātā vietā.

43
00:05:56,810 --> 00:06:02,175
Ziemas gaitā veiciet regulāras pārbaudes, 
izlasot bojātos sīpolus.

44
00:06:06,840 --> 00:06:12,630
Šķirnēm raksturīgs zināms neaktivitātes periods
(dažām - ilgāks nekā citām).

45
00:06:13,335 --> 00:06:17,880
Sīpoli aktivizēsies, 
temperatūrai sasniedzot 12°C.

46
00:06:21,150 --> 00:06:23,380
Nākamā pavasara sākumā

47
00:06:23,895 --> 00:06:29,040
stādiet sīpolus 20 cm atstatumā
(neierokot pārāk dziļi).

48
00:07:42,680 --> 00:07:48,160
Izdīgs 1 - 3 kāti, kas var sasniegt 
vismaz 1 m garumu.

49
00:07:51,735 --> 00:07:55,675
Svarīgi kātus stutēt, 
lai tie nesagāztos.

50
00:07:57,915 --> 00:07:59,900
Četru nedēļu laikā

51
00:08:00,165 --> 00:08:04,620
uzziedēs katrs sīpola ziedkopas 
atsevišķais ziediņš.

52
00:08:06,075 --> 00:08:09,020
Periods no ziedēšanas sākuma

53
00:08:09,310 --> 00:08:11,390
līdz pilnam sēklu briedumam ir visai ilgs.

54
00:08:14,720 --> 00:08:19,420
Sēklas ir gatavas, kad pākstis izkaltušas - 
un tajās redzamas melnas sēkliņas.

55
00:08:28,230 --> 00:08:32,470
Lai ievāktu sēklas - 
nogrieziet ziedgalviņu līdz ar kāta augšgalu.

56
00:08:33,335 --> 00:08:39,220
Ievietojiet auduma maisiņā un atstājiet žāvēties - 
siltā, labi vēdinātā vietā.

57
00:08:43,780 --> 00:08:45,540
Aukstos un mitros reģionos -

58
00:08:45,800 --> 00:08:52,310
ievāciet sīpolus agri, lai nezaudētu sēklas,
kuru izbiršanu veicinās vējš vai lietus.

59
00:08:53,650 --> 00:08:57,940
Lai to paveiktu, izrociet visu augu -
iekams sēklas nogatavojušās.

60
00:08:58,410 --> 00:09:00,615
Ļaujiet sēklām nobriest 
sausā vietā.

61
00:09:06,925 --> 00:09:09,620
Ievākšana - šķirošana - uzglabāšana

62
00:09:22,460 --> 00:09:25,260
Vispirms - 
saberzējiet ziedgalviņas plaukstās,

63
00:09:25,335 --> 00:09:27,650
tad saspaidiet, 
izmantojot mīklas veltni.

64
00:09:42,615 --> 00:09:47,310
Sēklu šķirošanai izmantojiet smalku sietu, 
kas aizturēs sēklas, bet ne putekļus.

65
00:10:02,790 --> 00:10:05,890
Visbeidzot - vētījiet sēklas, 
lai atbrīvotos no sīkām pelavām.

66
00:10:06,305 --> 00:10:11,450
Novietojiet sēklas vējā, pūtiet pār tām elpu 
vai ieslēdziet nelielu ventilatoru.

67
00:10:17,230 --> 00:10:20,620
Izvētītās sēklas 
beriet aukstā ūdenī, apmaisiet.

68
00:10:26,270 --> 00:10:29,855
Auglīgās sēklas būs smagākas 
un nogrims.

69
00:10:33,625 --> 00:10:36,885
Aizvāciet tukšās sēklas 
un uzpeldējušos krikumus.

70
00:10:44,540 --> 00:10:46,430
Auglīgās sēklas beriet uz šķīvja.

71
00:10:49,715 --> 00:10:52,625
Kad tās izžuvušas -
 plūdīs līdzīgi smiltīm.

72
00:10:59,880 --> 00:11:03,015
Paciņā vienmēr ievietojiet etiķeti 
ar sugas un šķirnes nosaukumu,

73
00:11:03,080 --> 00:11:09,530
kā arī ievākšanas gadu
(ārējs uzraksts mēdz nodilt).

74
00:11:20,040 --> 00:11:25,340
Dažas dienas uzglabājiet sēklas saldētavā, 
lai nonāvētu parazītus.

75
00:11:26,480 --> 00:11:29,940
Sīpolu sēklas dīgtspēju 
saglabā 2 gadus.

76
00:11:30,290 --> 00:11:33,180
Termiņš dažkārt ieilgst līdz 4 - 5 gadiem.

77
00:11:33,855 --> 00:11:37,340
Sēklas strauji zaudē 
dīgšanas kapacitāti.

78
00:11:37,815 --> 00:11:41,230
Lai to pagarinātu, 
glabājiet sēklas saldētavā.
