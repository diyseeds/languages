﻿1
00:00:07,700 --> 00:00:12,140
Sēklu ražošanai paredzēto augu selekcija un audzēšana

2
00:00:18,565 --> 00:00:24,090
Selekcija nodrošina iespēju 
augus pakāpeniski pielāgot vides prasībām,

3
00:00:24,650 --> 00:00:26,430
kā arī jūsu vēlmēm un vajadzībām.

4
00:00:27,140 --> 00:00:32,625
Sēklu ieguvei kultivētajiem augiem 
svarīgi pievērst īpašu uzmanību,

5
00:00:33,795 --> 00:00:38,280
jo no tiem tiks ražotas 
jaunas augļu un dārzeņu paaudzes.

6
00:00:40,560 --> 00:00:43,780
Tādēļ izvēle jāveic rūpīgi,

7
00:00:44,095 --> 00:00:47,180
novērojot augus - 
visā to attīstības gaitā. 

8
00:00:52,725 --> 00:00:56,215
Izvēle, kas izriet 
vienīgi no augļa īpašībām,

9
00:00:56,825 --> 00:01:01,800
neatspoguļos visus 
auga attīstībā iesaistītos faktorus. 

10
00:01:04,425 --> 00:01:07,795
Nepieciešams apsvērt selekcijas kritērijus -

11
00:01:08,380 --> 00:01:11,795
tie palīdzēs atlasīt 
labākos augus sēklu ražošanai. 

12
00:01:13,860 --> 00:01:18,640
Vispirms izvērtējami 
tādi šķirnes rādītāji

13
00:01:19,270 --> 00:01:24,270
kā pretestība slimībām, ražīgums, agrīnība

14
00:01:24,695 --> 00:01:29,535
un subjektīvāki kritēriji, 
piemēram, garša un apveids.

15
00:01:43,890 --> 00:01:47,000
Jāņem vērā arī augu spēja 
pielāgoties videi

16
00:01:47,195 --> 00:01:51,035
un kultivēšanas metodēm. 

17
00:01:54,175 --> 00:01:59,515
Svarīgi laikus apjaust, 
kuri rādītāji būs prioritārie,

18
00:02:00,160 --> 00:02:03,695
jo, mainoties paaudzēm, 
norisinās šķirnes attīstība -

19
00:02:03,895 --> 00:02:07,850
atsevišķas īpašības var kļūt 
mazāk izteiktas vai dominējošas. 

20
00:02:13,340 --> 00:02:17,465
Dažkārt selekcijas process 
nav ieteicams.

21
00:02:17,770 --> 00:02:22,450
Piemērs - izzušanas riskam pakļauts 
retas šķirnes augs,

22
00:02:22,740 --> 00:02:24,885
kam saglabājušās

23
00:02:25,120 --> 00:02:28,235
vien nedaudzas sēklas, 
pirmajā gadā nesaražos gana daudz stādu,

24
00:02:28,305 --> 00:02:30,400
lai būtu īstenojams selekcijas process. 

25
00:02:35,765 --> 00:02:41,100
Tāpat iespējams saglabāt šķirni nemainīgu - 
neveicot selekcijas atlasi.

26
00:02:41,470 --> 00:02:45,140
Tiek pieņemti 
visi šķirnes nosacījumi,

27
00:02:45,660 --> 00:02:51,220
pat ja rezultāti ir dažādoti
(ģenētiskās daudzveidības veicināšans nolūkos).

28
00:02:52,195 --> 00:02:55,240
Daudzveidība nodrošina izteiktāku adaptivitāti,

29
00:02:55,670 --> 00:02:58,785
kā arī ģenētisko potenciālu

30
00:02:58,955 --> 00:03:01,775
(nākotnē īstenojamai selekcijai). 

31
00:03:09,170 --> 00:03:12,870
Dārza ietvaros svarīgi 
skaidri nošķirt sēklu ieguves nolūkos

32
00:03:14,590 --> 00:03:18,270
un dārzeņu ražas ieguves nolūkos 
kultivētos augus. 

33
00:03:20,885 --> 00:03:25,595
Ieteicams izveidot dārza plānu, 
norādot katras šķirnes atrašanās vietu,

34
00:03:25,940 --> 00:03:29,150
kas ļaus augus vieglāk atrast, 
ja nozudīs pievienotās etiķetes. 

35
00:03:31,095 --> 00:03:35,920
Sēklu ieguves augu pilnais attīstības cikls 
mēdz būt ilgāks -

36
00:03:36,125 --> 00:03:38,505
nekā augu attīstība pārtikas ieguvei.

37
00:03:39,215 --> 00:03:42,865
Piemēram, lapu salāti 
ēdami jau pēc 2-3 mēnešiem,

38
00:03:43,110 --> 00:03:48,060
taču salātu pilnais cikls (līdz sēklu novākšanai) 
ilgst 5-6 mēnešus.

39
00:04:05,450 --> 00:04:09,180
Vairāki divgadīgie augi 
(piemēram, burkāni)

40
00:04:09,570 --> 00:04:13,910
zied un briedina sēklas 
tikai otrajā augšanas gadā. 

41
00:05:00,780 --> 00:05:06,660
Ieteicams atvēlēt nelielu dārza stūrīti - 
īpaši sēklu ražošanai. 
