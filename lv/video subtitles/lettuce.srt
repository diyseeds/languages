﻿1
00:00:10,210 --> 00:00:13,300
Salāti pieder Asteraceae dzimtai

2
00:00:13,700 --> 00:00:15,980
un Lactuca sativa sugai,

3
00:00:16,460 --> 00:00:19,550
kas iedalāma četros pamattipos:

4
00:00:21,505 --> 00:00:26,300
- Galviņsalāti (to skaitā - 
sviesta galviņsalāti ar gludām,

5
00:00:26,420 --> 00:00:28,060
viegli krokotām lapām

6
00:00:30,905 --> 00:00:35,860
un Batāvijas salāti ar sulīgām, 
vairāk vai mazāk krokotām lapām),

7
00:00:38,845 --> 00:00:43,060
- Romiešu salāti ar iegarenu apveidu 
un garām lapām, 

8
00:00:45,975 --> 00:00:49,100
- Lapu salāti, 
kas neveido ‘’serdi’’,

9
00:00:49,540 --> 00:00:53,620
(dažām šķirnēm raksturīgas lapas 
ar izteikti krokotiem galiņiem), 

10
00:00:58,260 --> 00:01:03,660
- Sparģeļu salāti - 
kultivēti galvenokārt Āzijā,

11
00:01:04,180 --> 00:01:07,700
audzēti sulīgo kātu 
un maigo lapu ieguvei. 

12
00:01:13,995 --> 00:01:16,580
Svarīga salātu īpašība

13
00:01:16,805 --> 00:01:20,400
ir spēja pielāgoties 
klimatam un gadalaikiem.

14
00:01:28,140 --> 00:01:31,985
Vairākas šķirnes ir īpaši izturīgas - 
ziemas sala vai tveices apstākļos,

15
00:01:32,880 --> 00:01:35,820
un tām raksturīga lēna augļaizmešanās. 

16
00:01:39,440 --> 00:01:42,460
Dažas šķirnes ir piemērotas
 jebkuram klimatam. 

17
00:01:50,510 --> 00:01:51,540
Apputeksnēšana 

18
00:02:00,395 --> 00:02:03,250
Salātu ziedu sauc par galviņu.

19
00:02:07,010 --> 00:02:10,060
Augs ir hermafrodīts un pašapputeksnējošs -

20
00:02:11,450 --> 00:02:15,820
vīrišķie un sievišķie vairošanās orgāni 
atrodas uz viena zieda

21
00:02:16,890 --> 00:02:18,580
un ir savietojami.

22
00:02:20,540 --> 00:02:23,300
Tādēļ augs ir autogāms. 

23
00:02:26,800 --> 00:02:31,820
Kukaiņi var izraisīt 
dažādu šķirņu krustošanos.

24
00:02:35,095 --> 00:02:38,035
Jo karstāks klimats - 
jo lielāks krustošanās risks. 

25
00:02:44,640 --> 00:02:46,260
Dažos pasaules reģionos

26
00:02:46,615 --> 00:02:50,980
arī savvaļas šķirnes var krustoties 
ar kultivētajiem salātiem. 

27
00:02:58,400 --> 00:02:59,660
Lai novērstu krustošanos,

28
00:03:00,010 --> 00:03:05,140
divas salātu šķirnes audzējamas 
vismaz vairāku metru atstatumā

29
00:03:05,365 --> 00:03:06,740
(mērenā klimatā)

30
00:03:07,205 --> 00:03:10,155
un vēl attālāk viena no otras - 
karsta klimata reģionos. 

31
00:03:12,500 --> 00:03:16,755
Šķirnes iespējams izolēt 
ar kukaiņu tīklu sprostiem.

32
00:03:17,540 --> 00:03:18,590
Sīkāk -

33
00:03:18,635 --> 00:03:24,140
‘’Sēklu ražošanas ābeces’’
 izolēšanas metodēm veltītajā modulī! 

34
00:03:32,845 --> 00:03:36,940
Stādīšanu iespējams plānot,

35
00:03:37,105 --> 00:03:39,595
lai šķirnes neziedētu vienlaicīgi.

36
00:03:41,680 --> 00:03:42,700
Tomēr

37
00:03:42,945 --> 00:03:47,915
sēklas jāsēj gana agri sezonā, 
lai augs pagūtu saražot sēklas. 

38
00:04:02,815 --> 00:04:03,940
Dzīves cikls 

39
00:04:32,855 --> 00:04:37,630
Sēklai audzētos salātus 
kultivē tāpat kā pārtikas ieguvei. 

40
00:04:53,290 --> 00:04:59,100
Ģenētiskās daudzveidības uzturēšanai 
nepieciešams vismaz ducis augu. 

41
00:05:02,990 --> 00:05:05,940
Rūpīgi jāizraugās augi,

42
00:05:06,080 --> 00:05:10,340
kas atbilst specifiskajiem 
šķirnes kritērijiem: forma,

43
00:05:10,915 --> 00:05:13,540
krāsa vai augšanas sezona.

44
00:05:14,520 --> 00:05:19,265
Ja atlase netiek veikta, 
laika gaitā zudīs šķirnes īpašības. 

45
00:05:21,005 --> 00:05:23,530
Galviņsalātu lapas veido blīvu ‘’serdeni’’.

46
00:05:24,345 --> 00:05:27,880
Batāvijas salātu lapas 
ir kraušķīgas, krokainas. 

47
00:05:28,160 --> 00:05:29,780
Ziemas salātus

48
00:05:29,950 --> 00:05:33,660
(kā liecina nosaukums) 
audzē gada aukstajā periodā -

49
00:05:34,050 --> 00:05:36,660
augi attīsta sēklas nākamajā pavasarī.

50
00:05:37,205 --> 00:05:40,815
Tādējādi ziemas salāti 
saglabā salcietības spēju. 

51
00:05:43,955 --> 00:05:48,185
Aizvāciet augus, kas neatbilst 
specifiskajām šķirnes īpašībām.

52
00:05:56,960 --> 00:06:00,380
Tāpat neizvēlieties salātus, 
kas pāragri izziedējuši,

53
00:06:00,560 --> 00:06:03,465
neveicot pilnu attīstības ciklu.

54
00:06:03,850 --> 00:06:06,525
Tādu augu sēklas ražos nīkulīgus stādus. 

55
00:06:10,190 --> 00:06:15,220
Dažas salātu šķirnes ar grūtībām 
izspraucina ziedkātu cauri galviņai,

56
00:06:15,690 --> 00:06:18,405
īpaši - ja galviņa ir ārkārtīgi blīva.

57
00:06:19,480 --> 00:06:24,540
Augiem iespējams palīdzēt - 
uzmanīgi iegriežot atveri galviņas virspusē

58
00:06:24,820 --> 00:06:27,660
(neievainojot trauslo kāta dīgstu).

59
00:06:40,120 --> 00:06:45,345
Tāpat (pa vienai vien) var nošķērēt lapas, 
kas ietver salāta ‘’serdeni’’.

60
00:06:55,740 --> 00:06:58,900
Mitros laikapstākļos 
lapām ir tendence iepūt.

61
00:06:59,805 --> 00:07:02,620
Ja tā noticis - lapas aizvācamas. 

62
00:07:52,320 --> 00:07:55,705
Ziedošs salātaugs 
var sasniegt vidēji 1 m garumu.

63
00:07:56,000 --> 00:07:59,540
Salāti stutējami ar mietiņu - 
individuāli vai grupās. 

64
00:08:05,160 --> 00:08:07,500
Atbilstoši vides nosacījumiem -

65
00:08:07,680 --> 00:08:13,260
sēklas veidojas 12 - 24 dienu laikā 
pēc ziedēšanas.

66
00:08:16,740 --> 00:08:20,020
Salātu ziedgalviņas izzied pakāpeniski,

67
00:08:20,675 --> 00:08:24,220
tādēļ visas sēklas 
nenobriest vienlaikus.

68
00:08:28,975 --> 00:08:34,260
Uz viena auga vienlaikus var būt atrodami
pumpuri, ziedi un sēklas. 

69
00:08:41,050 --> 00:08:45,420
Sēklu brieduma pakāpe nosakāma, 
ievācot apvītušu ziedkopu

70
00:08:45,740 --> 00:08:49,005
un saspiežot to 
starp īkšķi un rādītājpirkstu.

71
00:08:49,490 --> 00:08:53,740
Ja sēklas brīvi neatdalās 
no galviņas,

72
00:08:53,910 --> 00:08:55,380
tām vēl jāgatavojas. 

73
00:08:57,380 --> 00:09:02,500
Kad sēklas viegli izbirst no galviņas, 
tās iespējams ievākt. 

74
00:09:06,190 --> 00:09:09,640
Labākās sēklas atrodamas 
uz salātauga galvenā kāta. 

75
00:09:19,715 --> 00:09:23,815
Laikapstākļi būtiski ietekmē 
salātu sēklu ražu.

76
00:09:24,040 --> 00:09:27,580
Lietains, mitrs laiks 
kavēs sēklu veidošanos.

77
00:09:27,940 --> 00:09:33,070
Pārmērīgs mitrums var mazināt 
auga spēju pretoties sēnīšu slimībām. 

78
00:09:39,975 --> 00:09:44,605
Dažos reģionos - sēklaugi kultivējami 
no laikapstākļiem pasargātā vidē. 

79
00:09:48,615 --> 00:09:52,070
Sēklas iespējams ievākt trīs veidos. 

80
00:09:53,520 --> 00:09:59,955
Var ievākt pirmās nobriedušās galviņas, 
nodrošinot nelielāku ražas apjomu. 

81
00:10:00,445 --> 00:10:03,700
Zem galviņas novietojams

82
00:10:03,850 --> 00:10:08,300
spainis, maiss vai pārklājs,
pār kuru tiek izpurinātas krītošās sēkliņas. 

83
00:10:13,340 --> 00:10:17,740
Var nogaidīt, kamēr nogatavosies 
vismaz puse no galviņām,

84
00:10:18,040 --> 00:10:22,700
tad ziedkātus nogriezt 
un ievietot lielā audekla maisā.

85
00:10:28,640 --> 00:10:33,580
Maisu izkar no vides ietekmes pasargātā,
labi vēdinātā, sausā vietā.

86
00:10:34,580 --> 00:10:37,620
Sēklas turpinās nobriest uz auga. 

87
00:10:46,315 --> 00:10:49,740
Ja nogatavošanās periodā 
ir nelāgi laikapstākļi -

88
00:10:50,155 --> 00:10:53,725
trešā iespēja paredz 
augu izrakšanu ar saknēm.

89
00:10:54,355 --> 00:10:59,500
Ap saknēm tiek aptīts maiss, 
lai augsne un akmentiņi nesajauktos ar sēklām.

90
00:11:00,020 --> 00:11:04,355
Augi tiek izkārti ar ziedkopām lejup - 
sausā, labi vēdinātā vietā.

91
00:11:05,970 --> 00:11:08,725
Tādējādi turpināsies 
sēklu nobriešanas procesi. 

92
00:11:14,865 --> 00:11:17,740
Ievākšana - šķirošana - uzglabāšana

93
00:11:23,665 --> 00:11:27,900
Ziedkātiem jābūt pilnībā izžuvušiem - 
iekams tiek ievāktas sēklas.

94
00:11:29,235 --> 00:11:31,500
Galviņu saberzē plaukstās -

95
00:11:31,785 --> 00:11:34,535
vairums sēklu izbirs pašas.

96
00:11:38,525 --> 00:11:42,140
Neapputeksnēti ziedi saražos tukšas sēklas.

97
00:11:48,630 --> 00:11:53,470
Ziedkātus iespējams arīdzan izkult - 
ievietojot tos lielākā tvertnē. 

98
00:11:58,150 --> 00:12:02,780
Sēklu šķirošanai izmanto 
dažāda smalkuma sietus. 

99
00:12:16,525 --> 00:12:20,535
Visbeidzot - sēklas tiek vētītas, 
lai atsijātu atlikušās pelavas.

100
00:12:21,500 --> 00:12:23,260
Sēklas izber uz šķīvja

101
00:12:23,390 --> 00:12:29,020
vai ievieto vētījamā grozā un tām pāri 
pūš elpu, lai izvētītu vieglākas pelavas. 

102
00:12:30,940 --> 00:12:32,505
Vētīt var arīdzan vēsmiņā.

103
00:12:33,230 --> 00:12:37,420
Ieberiet sēklas tvertnē, 
kas novietota ārā uz zemes.

104
00:12:37,545 --> 00:12:39,300
Pelavas aizpūtīs vējš.

105
00:12:40,745 --> 00:12:44,460
Zem tvertnes paklājiet plēvi - 
ja gadās stiprākas brāzmas. 

106
00:12:53,450 --> 00:12:56,260
Sēklas tiek iebērtas plastmasas maisiņā,

107
00:12:56,670 --> 00:13:01,580
kurā ievietojama etiķete, norādot
sugas un šķirnes nosaukumu,

108
00:13:01,900 --> 00:13:04,870
ievākšanas gadu 
un audzēšanas vietu. 

109
00:13:08,535 --> 00:13:13,005
Dažas dienas uzglabājot sēklas saldētavā, 
ies bojā parazīti. 

110
00:13:14,700 --> 00:13:17,980
Salātu sēklas dīgtspēju saglabā 
vidēji 5 gadus -

111
00:13:18,210 --> 00:13:21,700
vai pat līdz 9 gadiem (un ilgāk), 
ja tiek glabātas saldētavā.

112
00:13:26,430 --> 00:13:32,260
Nepiemērotos uzglabāšanas apstākļos - 
salātu sēklas drīz vien kļūst nederīgas.

113
00:13:34,550 --> 00:13:39,750
Labs sēklu augs saražos 
10 - 15 gramus sēklu. 
