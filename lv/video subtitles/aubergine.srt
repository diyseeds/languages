1
00:00:07,720 --> 00:00:11,940
Baklažāns piederīgs 
Solanaceae dzimtai

2
00:00:12,290 --> 00:00:15,060
un Solanum melongena sugai.

3
00:00:20,265 --> 00:00:25,540
Tas ir daudzgadīgs augs - tropu zemēs, 
bet viengadīgs - mērenos reģionos.

4
00:00:37,350 --> 00:00:42,100
Pastāv plaša baklažānu izmēru, 
apveidu un krāsu daudzveidība.

5
00:00:55,840 --> 00:00:56,740
Apputeksnēšana 

6
00:01:15,800 --> 00:01:20,060
Baklažāna zieds ir hermafrodīts 
un pašapaugļojošs.

7
00:01:21,180 --> 00:01:25,700
Tas nozīmē - vīrišķie un sievišķie orgāni 
atrodas uz viena zieda,

8
00:01:26,255 --> 00:01:27,855
turklāt ir savietojami.

9
00:01:32,980 --> 00:01:34,930
Tādēļ baklažāns ir autogāms augs.

10
00:01:39,505 --> 00:01:42,995
Tomēr iespējama starpšķirņu krustošanās.

11
00:01:44,065 --> 00:01:47,460
Krustošanās biežums 
atkarīgs no vides

12
00:01:47,705 --> 00:01:50,380
un apputeksnējošo kukaiņu skaita. 

13
00:01:53,580 --> 00:01:57,220
Audzējot baklažānus vietās, 
kas pasargātas no vēja,

14
00:01:57,515 --> 00:02:02,740
apputeksnēšana veicināma, 
tos regulāri papurinot ziedēšanas laikā. 

15
00:02:06,990 --> 00:02:08,940
Lai novērstu svešapputi,

16
00:02:09,350 --> 00:02:14,185
dažādas baklažānu šķirnes 
audzējamas 100 m atstatumā.

17
00:02:16,690 --> 00:02:22,580
Attālums samazināms līdz 50 m, 
ja pastāv dabiska barjera, piemēram, dzīvžogs.

18
00:02:24,840 --> 00:02:30,020
Tropu klimatā - 
ieteicams ievērot līdz pat 1 km atstatumu. 

19
00:02:32,935 --> 00:02:36,885
Iespējams pielietot arī kukaiņu tīklu -
mehāniskai izolācijai.

20
00:02:38,550 --> 00:02:42,790
Ielūkojieties ‘’Sēklu ražošanas ābeces’’ 
izolēšanas metodēm veltītajā modulī! 

21
00:02:55,100 --> 00:02:56,540
Dzīves cikls 

22
00:03:11,045 --> 00:03:16,620
Sēklai audzētos baklažānus 
kultivē tāpat kā pārtikas ieguvei.

23
00:03:27,875 --> 00:03:30,060
Ģenētiskās daudzveidības nodrošināšanai

24
00:03:30,235 --> 00:03:34,020
audzējami 6 - 12 augi 
no katras šķirnes. 

25
00:03:37,390 --> 00:03:42,005
Baklažānu attīstībai 
nepieciešams pastiprināts siltums.

26
00:03:44,810 --> 00:03:49,340
Sējas laiks precīzi pielāgojams datumam, 
kad augs tiks pārstādīts ārā. 

27
00:04:18,605 --> 00:04:23,815
Kad zieds atvēries, 
auglis nogatavosies 60 - 100 dienu laikā

28
00:04:23,950 --> 00:04:28,420
(atbilstoši šķirnes specifikai).

29
00:04:29,980 --> 00:04:33,895
Taču atcerieties - 
sēklas vēl nebūs pilnībā nobriedušas! 

30
00:04:38,465 --> 00:04:44,380
Sēklu ieguvei tiek atlasīti 
veselīgi un dzīvelīgi augi,

31
00:04:44,720 --> 00:04:48,100
kas iepriekš novēroti - 
visā to attīstības gaitā. 

32
00:04:51,625 --> 00:04:56,460
Izraugoties augus, 
svarīgi faktori ir vienmērīga un aktīva attīstība,

33
00:04:57,025 --> 00:04:58,500
daudzi ziedi,

34
00:04:58,960 --> 00:05:04,940
labi veidoti augļi un veiksmīga pielāgošanās 
aukstiem klimata apstākļiem. 

35
00:05:05,710 --> 00:05:09,555
Augļu ziņā - 
izvēlieties garšīgākos,

36
00:05:09,940 --> 00:05:13,260
kā arī šķirnei atbilstošākos (forma, izmērs,

37
00:05:14,010 --> 00:05:16,260
mīkstuma un mizas krāsa,

38
00:05:16,830 --> 00:05:21,990
augļa rūgtenums, mizas biezums). 

39
00:05:25,195 --> 00:05:29,380
Neatlasiet sēklas augus 
no pārtikai novāktajiem baklažāniem -

40
00:05:29,750 --> 00:05:34,780
nebūs novērojamas auga īpašības 
visos attīstības posmos. 

41
00:05:40,025 --> 00:05:43,120
Sēklu ražošanai nav piemēroti slimi augi. 

42
00:05:48,120 --> 00:05:53,660
Pilnībā nobrieduši augļi 
kļūst mīksti un maina krāsu.

43
00:05:55,510 --> 00:06:00,055
Baltie baklažāni top dzelteni, 
bet violetie - brūni.

44
00:06:02,125 --> 00:06:05,900
Ja augļi nav paguvuši 
pilnībā nobriest uz auga,

45
00:06:06,220 --> 00:06:11,345
tos var nogatavināt koka kastēs -
vēsā un sausā vietā. 

46
00:06:16,285 --> 00:06:19,180
Ievākšana - šķirošana - uzglabāšana

47
00:06:24,170 --> 00:06:25,685
Sēklas ievācamas

48
00:06:25,940 --> 00:06:29,145
no pilnībā nobriedušiem, 
neierūgušiem augļiem. 

49
00:06:34,050 --> 00:06:36,560
Sēklas iegūstamas divos veidos:

50
00:06:40,660 --> 00:06:42,080
Nelielai ražai -

51
00:06:42,340 --> 00:06:44,250
sagrieziet baklažānus četrās daļās

52
00:06:46,875 --> 00:06:48,720
un izgrebiet sēklas ar nazi.

53
00:06:56,080 --> 00:07:02,230
Lielākai ražai - nomizojiet, sakapājiet baklažānus 
kubiciņos, ievietojiet tos burkā ar ūdeni. 

54
00:07:08,905 --> 00:07:11,460
Dažas sekundes sablenderējiet.

55
00:07:16,310 --> 00:07:19,530
Derīgās sēklas 
nogrims trauka lejasdaļā.

56
00:07:27,185 --> 00:07:30,420
Ar sietu atdaliet augļa gaļīgo masu, 
mizas atliekas

57
00:07:30,785 --> 00:07:33,620
un neattīstītās sēklas.

58
00:07:37,535 --> 00:07:41,695
Tad izlasiet derīgās sēklas 
un noskalojiet tās sietā - zem tekoša ūdens.

59
00:07:43,725 --> 00:07:47,340
Svarīgi sēklas apžāvēt - 
ne ilgāk kā divas dienas.

60
00:07:50,215 --> 00:07:52,460
Izklājiet sēklas smalkā sietā

61
00:07:52,805 --> 00:07:58,140
vai uz šķīvja - siltā, sausā, 
caurvējotā vai labi vēdinātā vietā

62
00:07:59,540 --> 00:08:02,380
(23° - 30°C). 

63
00:08:04,580 --> 00:08:07,740
Nelielam sēklu apjomam 
lietojami kafijas filtri,

64
00:08:08,110 --> 00:08:12,020
jo tie lieliski uzsūc mitrumu - 
un sēklas tiem nepielīp.

65
00:08:15,525 --> 00:08:19,740
Filtrā ieteicams ievietot 
ne vairāk kā vienu tējkaroti sēklu.

66
00:08:20,210 --> 00:08:25,645
Izkariniet sainīšus uz veļasauklas - 
sausā, vēdinātā un apēnotā vietā. 

67
00:08:35,455 --> 00:08:37,465
Izveidojiet etiķeti ar šķirnes,

68
00:08:37,695 --> 00:08:45,790
sugas un ievākšanas gada norādēm,
ieviejotiet to paciņā.

69
00:08:47,095 --> 00:08:49,930
Ārējs uzraksts mēdz nodilt. 

70
00:08:57,285 --> 00:09:01,100
Dažas dienas saldētavā 
nonāvēs parazītu kūniņas.

71
00:09:04,900 --> 00:09:09,710
Baklažānu dīgtspēja 
saglabājas 3 - 6 gadus.

72
00:09:13,470 --> 00:09:16,275
Termiņa pagarināšanai -
 glabājiet sēklas saldētavā. 
