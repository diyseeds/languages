﻿1
00:00:09,840 --> 00:00:14,960
Sēklu ievākšana, mitrā apstrāde,
žāvēšana un šķirošana

2
00:00:23,560 --> 00:00:28,520
Sēklu ievākšana 
un mitrā apstrāde ar fermentāciju

3
00:00:34,680 --> 00:00:38,040
Mitrās fermentācijas apstrāde

4
00:00:38,200 --> 00:00:41,240
ir piemērota tomātiem un gurķiem.

5
00:00:41,840 --> 00:00:46,880
Fermentācijas process ļauj sēklu atbrīvot
no želatīnveida apvalka,

6
00:00:46,960 --> 00:00:49,760
kas to ietver, 
uzturot sēklu neaktīvā stāvoklī.

7
00:00:54,480 --> 00:00:58,000
Tomātus vai gurķus pārgriež uz pusēm.

8
00:00:59,080 --> 00:01:05,680
Ar karoti izgrebj sēklas un mīkstumu, 
kas ievietojami stikla burkā.

9
00:01:06,680 --> 00:01:09,760
Ja nepieciešams, pievieno nedaudz ūdens.

10
00:01:11,480 --> 00:01:15,480
Sēklas nevajadzētu ievākt 
no bojātiem vai ierūgušiem augļiem.

11
00:01:17,280 --> 00:01:21,640
Burkām pievieno etiķetes 
ar sugas un šķirnes norādi.

12
00:01:23,080 --> 00:01:27,320
Stikla burkā būs vieglāk novērojams 
fermentācijas process.

13
00:01:28,000 --> 00:01:30,240
Burku neaizvākot pārāk cieši!

14
00:01:31,240 --> 00:01:36,000
Burka tiek pārklāta ar tīkliņu,
lai pasargātu to no mušām.

15
00:01:36,640 --> 00:01:43,680
Burku novieto siltā vietā (23° - 30°C), 
taču ne tiešos saules staros.

16
00:01:49,720 --> 00:01:55,240
Fermentācijas ilgums atkarīgs 
no gaisa temperatūras

17
00:01:55,320 --> 00:01:58,320
un cukura daudzuma šķīdumā.

18
00:02:00,440 --> 00:02:05,160
Pamazām virsmu pārklās 
balta pelējuma kārta,

19
00:02:05,640 --> 00:02:07,880
kas vairākkārt iemaisāma šķīdumā,

20
00:02:08,200 --> 00:02:10,840
lai fermentācija būtu vienmērīgāka

21
00:02:11,520 --> 00:02:15,160
un neveidotos 
pārāk biezs pelējuma slānis.

22
00:02:17,400 --> 00:02:21,960
Šķipsniņa cukura mazinās 
kaitīga pelējuma attīstības risku,

23
00:02:22,560 --> 00:02:26,360
kā arī aktivizēs procesu, 
ja augļa masā trūkst dabisko cukuru.

24
00:02:31,800 --> 00:02:35,000
Fermentācijas norise 
ir rūpīgi uzraugāma.

25
00:02:35,720 --> 00:02:39,360
Karstā laikā tā var ilgt 
mazāk nekā 48 stundas.

26
00:02:40,080 --> 00:02:45,040
Process jāpārtrauc savlaicīgi, 
lai sēklas, zaudējušas želatīnkārtiņu,

27
00:02:45,120 --> 00:02:48,560
nesāktu dīgt -
sadīgušās sēklas nav izmantojamas sēšanai.

28
00:02:52,480 --> 00:02:55,960
Kad sēklas atdalās
un nogrimst burkas lejasdaļā,

29
00:02:56,080 --> 00:02:59,080
bet mizas un mīkstums 
uzpeld virspusē,

30
00:02:59,640 --> 00:03:03,720
želatīnkārta ir pārstrādāta -
un process ir noslēdzies.

31
00:03:06,240 --> 00:03:07,880
Sēklas nepieciešams notīrīt.

32
00:03:10,960 --> 00:03:14,800
Sēklas tiek aizturētas sietiņā
un skalotas zem tekoša ūdens.

33
00:03:28,640 --> 00:03:32,360
Sēklu ievākšana 
un mitrā apstrāde bez fermentācijas

34
00:03:35,280 --> 00:03:38,560
Mitrā apstrāde bez fermentācijas

35
00:03:39,000 --> 00:03:41,920
ir piemērotāka augļražojošiem dārzeņiem:

36
00:03:42,040 --> 00:03:44,600
baklažāniem,

37
00:03:45,560 --> 00:03:48,280
ķirbjiem, kabačiem vai cukīni,

38
00:03:49,120 --> 00:03:50,840
melonēm un arbūziem.

39
00:03:54,640 --> 00:03:56,720
Sēklas izgrebj no augļa,

40
00:03:57,200 --> 00:04:00,480
ievieto sietā 
un skalo tekošā ūdenī.

41
00:04:03,520 --> 00:04:06,840
Ja sēklas viegli neatdalās 
no augļa masas,

42
00:04:07,360 --> 00:04:11,920
tās 12 - 24 stundas 
mērcējamas ūdenī,

43
00:04:12,560 --> 00:04:15,960
līdz augļa masa izjūk, 
atbrīvojot sēklas.

44
00:04:17,080 --> 00:04:21,760
Lai neierosinātu fermentāciju, 
sēklas nedrīkst turēt siltā vietā.

45
00:04:22,520 --> 00:04:25,160
Sēklas nepieciešams 
nekavējoties izžāvēt.

46
00:04:34,240 --> 00:04:35,520
Žāvēšana

47
00:04:40,200 --> 00:04:43,720
Pēc mitrās apstrādes - 
sēklas ātri jāizžāvē.

48
00:04:45,720 --> 00:04:48,640
Sēklu žāvēšana ilgst 
ne vairāk kā divas dienas.

49
00:04:50,680 --> 00:04:56,320
Sēklas tiek iebērtas smalkā sietā vai šķīvī - 
labi vēdinātā, sausā vietā,

50
00:04:56,800 --> 00:05:00,520
kur temperatūra sasniedz 23° - 30°C.

51
00:05:06,720 --> 00:05:09,640
Žāvējot nelielus sēklu apjomus,

52
00:05:10,080 --> 00:05:13,680
tās izvietojamas 
uz absorbējoša kafijas filtra,

53
00:05:13,960 --> 00:05:15,840
rūpējoties, lai sēklas nepieliptu.

54
00:05:16,600 --> 00:05:21,200
Viens filtrs paredzēts 
ne vairāk kā tējkarotītei sēklu.

55
00:05:23,640 --> 00:05:26,800
Uz katra filtra ar ūdensnoturīgu rakstāmo

56
00:05:27,040 --> 00:05:30,360
tiek atzīmēts 
šķirnes un sugas nosaukums.

57
00:05:32,000 --> 00:05:34,520
Filtrus izkar uz veļasauklas -

58
00:05:34,600 --> 00:05:38,040
siltā, tumšā, labi vēdinātā vietā.

59
00:05:38,840 --> 00:05:41,560
Sēklām nedrīkst piekļūt saulesgaisma.

60
00:05:42,160 --> 00:05:44,280
Nežāvējiet sēklas uz papīra -

61
00:05:44,600 --> 00:05:46,080
tās salips

62
00:05:46,760 --> 00:05:48,840
un būs grūti atdalāmas.

63
00:05:51,960 --> 00:05:54,920
Lai sēklas atdalītu -
noņemiet tās no pamatnes

64
00:05:55,200 --> 00:05:57,120
un saberzējiet plaukstās.

65
00:06:18,240 --> 00:06:20,040
Sēklu šķirošana

66
00:06:30,680 --> 00:06:33,560
Pēc ievākšanas - 
sēklas šķirojamas vairākos veidos.

67
00:06:34,720 --> 00:06:37,400
Iespējams izmantot sauso vai mitro metodi.

68
00:06:41,280 --> 00:06:46,080
Sēklas, ko neietver mīkstā masa 
(puravi, sīpoli u.c.),

69
00:06:46,520 --> 00:06:48,200
šķirojamas ūdenī.

70
00:06:49,800 --> 00:06:54,240
Caurspīdīgā traukā 
ielej lielu ūdens apjomu.

71
00:06:54,720 --> 00:06:56,480
Ūdenī ieber sēklas.

72
00:06:58,880 --> 00:07:01,240
Ūdeni vairākkārt apmaisa,

73
00:07:01,760 --> 00:07:06,520
lai smagās, auglīgās sēklas 
nogrimtu trauka lejasdaļā.

74
00:07:07,320 --> 00:07:10,720
Virspusē peldošās sēklas un pelavas

75
00:07:11,360 --> 00:07:13,160
tiek nosmalstītas ar sietu.

76
00:07:18,040 --> 00:07:20,600
Ūdeni lej cauri sietam,

77
00:07:20,680 --> 00:07:23,440
lai iegūtu nogrimušās sēklas.

78
00:07:24,360 --> 00:07:26,280
Tās tūdaļ jāžāvē.

79
00:07:28,200 --> 00:07:32,760
Metode nav piemērota 
ārkārtīgi vieglām sēkliņām.

80
00:07:36,480 --> 00:07:39,880
Biežāk lietota 
ir sausās šķirošanas metode.

81
00:07:59,600 --> 00:08:02,240
Liela izmēra sēklas, kas lobītas ar rokām,

82
00:08:02,320 --> 00:08:03,880
(piemēram, pupas)

83
00:08:04,320 --> 00:08:07,600
paredz deformēto vai bojāto sēklu 
manuālu atlasi.

84
00:08:12,840 --> 00:08:16,240
Pārējām sēklām, 
kuru sēklgalviņas tiek pāršķeltas vai pārspiestas,

85
00:08:17,320 --> 00:08:19,080
atsijājamas pelavas.

86
00:08:21,720 --> 00:08:24,800
Vispirms sēklas 
tiek pārsijātas rupjā sietā,

87
00:08:25,280 --> 00:08:27,760
kas uztver lielākos atkritumus.

88
00:08:31,440 --> 00:08:34,560
Sēklas un sīkākās pelavas 
nonāk spainī.

89
00:08:36,120 --> 00:08:39,320
Procesu atkārto, 
izmantojot smalkāku sietu,

90
00:08:39,760 --> 00:08:43,360
kas pārtver sēklas, 
bet izvētī pelavas.

91
00:08:50,640 --> 00:08:52,800
Sieta izvēle ir būtiska -

92
00:08:53,200 --> 00:08:57,160
tam jāaiztur sēklas, 
izlaižot cauri iespējami daudz pelavu.

93
00:09:06,240 --> 00:09:07,640
Tīrīšanas noslēgumā -

94
00:09:07,960 --> 00:09:10,360
sēklas ieber plakanā tvertnē.

95
00:09:10,440 --> 00:09:13,600
Tām viegli uzpūš, 
lai izvētītu sīkos krikumus.

96
00:09:17,760 --> 00:09:20,160
Sēklu šķirošanā 
izmantojams arī vējš.

97
00:09:20,520 --> 00:09:22,320
Uz zemes tiek noklāta liela loksne.

98
00:09:23,960 --> 00:09:28,160
Uz loksnes tiek izbērtas sēklas.
Vējš no tām aizpūš pelavas.

99
00:09:30,000 --> 00:09:32,600
Vējam jābūt vienmērīgam,
jo spēcīgas brāzmas

100
00:09:32,680 --> 00:09:33,880
aizpūtīs loksni ar visām sēklām.

101
00:09:34,480 --> 00:09:36,400
Iespējams izmantot mazu ventilatoru.

102
00:09:41,680 --> 00:09:46,080
Smagākām sēklām 
piemērots arī gaisa kompresors

103
00:09:46,560 --> 00:09:48,320
(sīkas sēklas tiktu aizpūstas).

104
00:09:54,400 --> 00:09:56,520
Lai kāda metode izraudzīta -

105
00:09:56,760 --> 00:09:59,480
vienmēr tiek zaudēts neliels sēklu daudzums.

106
00:09:59,800 --> 00:10:04,760
Svarīgi noteikt, 
cik smalki sēklas šķirojamas.

107
00:10:32,320 --> 00:10:35,600
Daba ir dāsna. 
Iesākot sēklu pavairošanu,

108
00:10:35,800 --> 00:10:39,320
drīz atklāsiet - 
tiks saražots ievērojams sēklu apjoms

109
00:10:39,880 --> 00:10:41,640
(vairāk nekā nepieciešams pašu dārzam).

110
00:10:43,280 --> 00:10:45,760
Nepūlieties izšķirot ikkatru sēkliņu -

111
00:10:46,120 --> 00:10:47,320
vienmēr būs gana tāpat!
