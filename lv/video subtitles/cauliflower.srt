﻿1
00:00:12,010 --> 00:00:15,660
Puķkāposti un brokoļi piederīgi 
Brassicaceae dzimtai

2
00:00:16,065 --> 00:00:18,985
un Brassica oleracea sugai.

3
00:00:20,470 --> 00:00:25,500
Puķkāposti iekļaujami 
botrytis var. botrytis apakšsugā,

4
00:00:25,745 --> 00:00:31,580
savukārt brokoļi ietilpst 
botrytis var. italica apakšsugā.

5
00:00:31,990 --> 00:00:35,950
Brassica oleracea sugā 
ietilpst arī kolrābji,

6
00:00:36,080 --> 00:00:40,580
kāposti, Briseles kāposti, 
lapu kāposti un virziņkāposti. 

7
00:00:42,035 --> 00:00:47,780
Puķkāposti un brokoļi ir viengadīgi augi, 
taču ziemas šķirnes mēdz būt divgadīgas.

8
00:00:49,380 --> 00:00:55,340
Tos audzē galviņu (embrionālo, mīksto čemuru, 
no kuriem attīstīsies ziedi) ieguvei.

9
00:00:57,225 --> 00:00:59,740
Pastāv agrīnas un vēlīnas šķirnes.

10
00:01:00,220 --> 00:01:02,960
Puķkāposti var būt balti vai violeti,

11
00:01:04,910 --> 00:01:07,380
bet brokoļi - zaļi vai violeti.

12
00:01:09,265 --> 00:01:12,515
Romiešu brokoļi ir dzeltenzaļi. 

13
00:01:18,175 --> 00:01:22,820
Oleracea kāpostu
apputeksnēšana: 

14
00:01:38,840 --> 00:01:42,740
Brassica oleracea ziedi 
ir hermafrodīti.

15
00:01:45,680 --> 00:01:49,815
Tie apveltīti gan ar vīrišķajiem, 
gan sievišķajiem orgāniem.

16
00:01:53,590 --> 00:01:56,100
Vairums ir pašsterili -

17
00:01:56,730 --> 00:02:01,765
viena auga ziedi 
spēj apputeksnēt tikai citu augu.

18
00:02:05,585 --> 00:02:07,800
Tādi augi ir alogāmi augi.

19
00:02:08,720 --> 00:02:13,455
Veiksmīgai apputeksnēšanai 
ieteicams kultivēt vairākus augus. 

20
00:02:17,115 --> 00:02:20,130
Ziedputekšņus izplata kukaiņi.

21
00:02:23,555 --> 00:02:27,930
Tādējādi tiek dabiski veicināta 
lieliska ģenētiskā daudzveidība.

22
00:02:34,945 --> 00:02:40,875
Visas Brassica oleracea kāpostu apakšsugas 
var savstarpēji krustoties.

23
00:02:42,865 --> 00:02:47,455
Tādēļ dažādu šķirņu sēklas kāpostus 
nevajadzētu audzēt tiešā tuvumā. 

24
00:02:52,025 --> 00:02:53,460
Šķirnes tīrības nolūkos -

25
00:02:53,760 --> 00:02:57,700
dažādas Brassica oleracea šķirnes

26
00:02:58,065 --> 00:03:01,170
stādāmas vismaz 1 km atstatumā. 

27
00:03:02,600 --> 00:03:05,380
Attālums samazināms līdz 500 metriem,

28
00:03:05,650 --> 00:03:09,820
ja starp šķirnēm atrodas dabiska barjera 
(piemēram, dzīvžogs). 

29
00:03:13,190 --> 00:03:15,640
Šķirnes iespējams izolēt -

30
00:03:15,985 --> 00:03:20,740
norobežotos kukaiņu tīklos, 
kuros ievietotas kukaiņu ligzdiņas,

31
00:03:25,985 --> 00:03:29,695
vai arī tīklus pamīšus 
atverot un aizverot.

32
00:03:34,115 --> 00:03:35,155
Sīkāk -

33
00:03:35,440 --> 00:03:39,600
izolēšanas metodēm veltītajā
''Sēklu ražošanas ābeces’’ modulī. 

34
00:03:48,350 --> 00:03:51,380
Puķkāpostu un brokoļu dzīves cikls 

35
00:04:09,935 --> 00:04:12,100
Maiga klimata reģionos -

36
00:04:12,235 --> 00:04:16,525
brokoļi un puķkāposti 
kultivējami kā divgadīgi augi.

37
00:04:18,340 --> 00:04:22,260
Tie sējami vasarā. 
Augi pārziemos augsnē,

38
00:04:22,585 --> 00:04:26,410
bet nākamajā pavasarī 
veidos galviņas un ziedus.

39
00:04:27,065 --> 00:04:31,100
Sēklas būs ievācamas 
otrā gada vasarā. 

40
00:04:34,795 --> 00:04:41,300
Tomēr puķkāposti un brokoļi 
ir Brassica oleracea sugas izņēmums,

41
00:04:41,940 --> 00:04:45,420
jo tie spēj vairoties 
jau pirmajā cikla gadā.

42
00:04:45,995 --> 00:04:49,385
Lai iegūtu sēklas
jau pirmajā rudenī -

43
00:04:49,680 --> 00:04:53,710
tie sējami siltā, no vides ietekmes 
pasargātā vietā, iespējami agri

44
00:04:53,910 --> 00:04:55,780
(janvārī vai februārī).

45
00:05:12,090 --> 00:05:14,020
Martā vai aprīļa sākumā -

46
00:05:14,360 --> 00:05:16,415
augi pārstādāmi āra augsnē,

47
00:05:16,640 --> 00:05:20,495
pārsedzot tos 
ar pretsala pārklāju. 

48
00:05:46,675 --> 00:05:50,380
Sēklas iegūstamas 
no veseliem, dzīvelīgiem augiem,

49
00:05:50,680 --> 00:05:53,915
kas novēroti visos attīstības posmos.

50
00:05:54,275 --> 00:05:58,005
Tādējādi iespējams sekot 
šķirnes īpašībām:

51
00:06:00,540 --> 00:06:02,640
vienmērīgai un aktīvai augšanai.

52
00:06:07,050 --> 00:06:10,300
Puķkāpostiem jāveido ciešas galviņas,

53
00:06:10,675 --> 00:06:13,380
kuras sargā bagātīgi sazēlušas lapas.

54
00:06:17,670 --> 00:06:22,950
Brokoļiem jāveido viena galviņa 
vai vairāki dzinumi.

55
00:06:27,965 --> 00:06:34,020
Brokoļiem svarīga ilgstoša pumpurošanās
pirms ziedēšanas, noturība pret slimībām. 

56
00:06:35,655 --> 00:06:41,135
Sēklu ražošanai izraugieties 15 stādus, 
lai nodrošinātu ģenētisko daudzveidību. 

57
00:06:43,395 --> 00:06:47,830
Kad puķkāpostu galviņas izveidojušās, 
tās var iebojāt mitrums.

58
00:06:47,975 --> 00:06:50,710
Sargājiet augus no lietus - 
pārslejot nelielu jumtiņu.

59
00:06:52,630 --> 00:06:57,095
Ja galviņas daļas skārusi puve,
 tās izgriežamas ar nazi.

60
00:07:06,120 --> 00:07:09,615
Puķkāposti neveido 
laterālus ziedkātus.

61
00:07:11,840 --> 00:07:14,030
Tādēļ nekad nenogrieziet visu galviņu. 

62
00:07:39,315 --> 00:07:43,300
Puķkāposti un brokoļi 
ziedēs, vēlākais, jūlijā,

63
00:07:44,010 --> 00:07:50,175
lai tiktu īstenots nogatavināšanās periods, 
kas mēdz būt visai ilgs. 

64
00:08:03,200 --> 00:08:09,940
Brassica oleracea
ievākšana, šķirošana un uzglabāšana

65
00:08:19,060 --> 00:08:22,750
Sēklas ir gatavas - 
pākstīm vēršoties smilškrāsā.

66
00:08:27,055 --> 00:08:29,500
Pākstis viegli pārsprāgst.

67
00:08:29,800 --> 00:08:34,780
Tas nozīmē - nobriedušas pākstis 
viegli atveras un strauji izplata sēklas. 

68
00:08:43,395 --> 00:08:47,455
Lielākoties stiebri nenobriest vienlaicīgi.

69
00:08:48,415 --> 00:08:54,425
Lai nezaudētu sēklas, tās ievācamas atsevišķi - 
no katra nobriedušā stiebra.

70
00:08:56,230 --> 00:09:01,875
Iekams nogatavojušās visas sēklas - 
var ievākt arī augu kopumā. 

71
00:09:05,420 --> 00:09:12,240
Briedināšana turpināma - 
žāvējot augu sausā, labi vēdinātā vietā. 

72
00:09:24,845 --> 00:09:30,890
Kāpostu sēklas ievācamas, 
kad pākstis viegli atveramas ar pirkstiem. 

73
00:09:32,995 --> 00:09:34,260
Lai ievāktu sēklas -

74
00:09:34,438 --> 00:09:37,700
pākstis tiek izbērtas uz plastmasas plēves

75
00:09:38,220 --> 00:09:43,010
vai bieza auduma gabala;
tad pākstis tiek izkultas vai saberzētas plaukstās.

76
00:09:46,500 --> 00:09:51,455
Pākstis var arī ievietot maisiņā -
 un izkult pret mīkstu virsmu. 

77
00:09:52,960 --> 00:09:57,640
Lielāki apjomi kuļami - 
mīdot vai pārbraucot pāri. 

78
00:10:10,255 --> 00:10:15,550
Ja pākstis neatveras viegli - 
tajās slēpjas nenobriedušas sēklas,

79
00:10:15,720 --> 00:10:17,440
kam būs raksturīga vāja dīgtspēja. 

80
00:10:22,185 --> 00:10:23,500
Šķirošanas gaitā -

81
00:10:23,710 --> 00:10:28,245
sēklas tiek sijātas rupjākā sietā,

82
00:10:28,680 --> 00:10:30,140
kas aiztur pelavas.

83
00:10:35,065 --> 00:10:37,820
Tad sēklas tiek sijātas citā sietā,

84
00:10:38,100 --> 00:10:42,940
kas aiztur sēklas, 
bet atsijā smalkākās daļiņas. 

85
00:10:46,405 --> 00:10:48,540
Pēdīgi - sēklas vētījamas,

86
00:10:49,015 --> 00:10:52,140
pūšot pār tām vai izmantojot vēja spēku,

87
00:10:52,290 --> 00:10:54,700
lai aizvāktu pelavu paliekas. 

88
00:11:09,825 --> 00:11:14,900
Visas Brassica oleracea kāpostu sēklas 
ir savstarpēji līdzīgas.

89
00:11:15,790 --> 00:11:19,595
Grūti atšķirt, piemēram,

90
00:11:20,065 --> 00:11:22,470
kāpostu un puķkāpostu sēklas.

91
00:11:23,020 --> 00:11:26,020
Tādēļ svarīgi augus iezīmēt

92
00:11:26,140 --> 00:11:29,940
un iegūtajām sēklām pievienot etiķeti
ar sugas un šķirnes

93
00:11:30,220 --> 00:11:33,140
nosaukumu, kā arī ievākšanas gadu. 

94
00:11:34,600 --> 00:11:39,415
Dažas dienas uzglabājot sēklas saldētavā, 
tiks iznīcināti parazīti. 

95
00:11:44,875 --> 00:11:47,940
Kāpostu sēklu dīgtspēja 
saglabājas 5 gadus.

96
00:11:49,090 --> 00:11:52,665
Taču dīgšanas kapacitāte 
var ieilgt līdz pat 10 gadiem.

97
00:11:54,550 --> 00:11:57,695
Termiņš pagarināms - 
glabājot sēklas saldētavā.

98
00:11:58,855 --> 00:12:05,205
Vienā gramā ietilpst 250 - 300 sēklas 
(atbilstoši šķirnes īpašībām). 
