﻿1
00:00:11,040 --> 00:00:14,075
Kāposti piederīgi 
Brassicaceae dzimtai,

2
00:00:14,570 --> 00:00:17,565
Brassica oleracea sugai

3
00:00:19,715 --> 00:00:21,860
un capitata apakšsugai.

4
00:00:22,845 --> 00:00:27,095
Brassica oleracea sugā 
ietilpst arī kolrābji,

5
00:00:27,710 --> 00:00:33,725
brokoļi, Briseles kāposti, lapu kāposti, 
puķkāposti un virziņkāposti. 

6
00:00:38,300 --> 00:00:43,800
Var atšķirties kāpostu krāsa 
(zaļa, balta vai sarkana)

7
00:00:44,915 --> 00:00:48,920
un galviņas apveids 
(izvirzīts vai apaļš).

8
00:00:49,985 --> 00:00:55,490
Kāpostiem raksturīgas gludas lapas, 
kas veido blīvu galviņu. 

9
00:01:06,560 --> 00:01:11,145
Oleracea kāpostu
apputeksnēšana: 

10
00:01:27,190 --> 00:01:31,130
Brassica oleracea ziedi 
ir hermafrodīti.

11
00:01:34,035 --> 00:01:38,030
Tas nozīmē - tie apveltīti gan ar vīrišķajiem,
gan sievišķajiem orgāniem.

12
00:01:41,960 --> 00:01:44,460
Vairums ir pašsterili -

13
00:01:45,065 --> 00:01:50,075
viena auga ziedi 
spēj apputeksnēt tikai citu augu.

14
00:01:53,930 --> 00:01:56,075
Tādi augi ir alogāmi augi.

15
00:01:57,085 --> 00:02:01,865
Veiksmīgai apputeksnēšanai 
ieteicams kultivēt vairākus augus. 

16
00:02:05,475 --> 00:02:08,490
Ziedputekšņus izplata kukaiņi.

17
00:02:11,895 --> 00:02:16,250
Tādējādi tiek dabiski veicināta 
lieliska ģenētiskā daudzveidība.

18
00:02:23,295 --> 00:02:27,615
Visas Brassica oleracea
kāpostu apakšsugas

19
00:02:27,785 --> 00:02:29,340
var savstarpēji krustoties.

20
00:02:31,235 --> 00:02:35,855
Tādēļ dažādu šķirņu sēklas kāpostus 
nevajadzētu audzēt tiešā tuvumā. 

21
00:02:40,370 --> 00:02:46,035
Šķirnes tīrības nolūkos - 
dažādas Brassica oleracea šķirnes

22
00:02:46,420 --> 00:02:49,645
stādāmas vismaz 1 km atstatumā. 

23
00:02:50,960 --> 00:02:53,780
Attālums samazināms līdz 500 metriem,

24
00:02:54,020 --> 00:02:58,210
ja starp šķirnēm atrodas dabiska barjera 
(piemēram, dzīvžogs). 

25
00:03:01,560 --> 00:03:06,720
Šķirnes iespējams izolēt - 
norobežotos kukaiņu tīklos,

26
00:03:06,955 --> 00:03:09,155
kuros ievietotas kukaiņu ligzdiņas.

27
00:03:14,310 --> 00:03:18,000
vai arī tīklus 
pamīšus atverot un aizverot.

28
00:03:22,485 --> 00:03:23,605
Ar izolēšanas metodēm

29
00:03:23,770 --> 00:03:28,090
iepazīstieties atbilstošajā 
‘’Sēklu ražošanas ābeces’’ modulī. 

30
00:03:41,380 --> 00:03:43,380
Kāposta dzīves cikls 

31
00:03:58,620 --> 00:04:02,220
Kāposti ir divgadīgi augi. 
Sēklai audzētie kāposti

32
00:04:02,475 --> 00:04:04,985
tiek kultivēti tāpat kā pārtikas ieguvei.

33
00:04:07,115 --> 00:04:08,270
Pirmajā gadā

34
00:04:08,610 --> 00:04:12,195
augs veidos galviņu, 
kas pārziemos

35
00:04:12,500 --> 00:04:14,415
un ziedēs nākamajā gadā. 

36
00:04:22,055 --> 00:04:25,735
Sējas laiks atkarīgs 
no klimata apstākļiem,

37
00:04:25,935 --> 00:04:29,730
ieziemošanas metodes 
un šķirnes agrīnuma. 

38
00:05:24,900 --> 00:05:27,200
Lai kāpostgalvas pārziemotu -

39
00:05:27,390 --> 00:05:30,400
sējiet maija vidū vai jūnija sākumā

40
00:05:30,930 --> 00:05:33,530
(dažas šķirnes - pat vēlāk).

41
00:05:34,065 --> 00:05:38,996
Tad rudenī galvas 
nepārgatavosies un neieplaisās. 

42
00:05:42,923 --> 00:05:46,981
Mazākas un ciešākas galviņas 
pārziemos veiksmīgāk. 

43
00:05:50,952 --> 00:05:57,294
Pirmajā gadā saglabājami vismaz 30 stādi,
lai 10-15 no tiem izturētu ziemu.

44
00:06:06,196 --> 00:06:08,661
Sēklas ievāc no veselīgiem augiem,

45
00:06:08,792 --> 00:06:12,036
kas novēroti visos 
attīstības posmos,

46
00:06:12,196 --> 00:06:15,450
lai sekotu to īpašībām. 

47
00:06:21,800 --> 00:06:23,992
Tiek izraudzītas spēcīgākās galviņas -

48
00:06:24,065 --> 00:06:27,352
saskaņā ar šķirnes īpašībām

49
00:06:27,498 --> 00:06:29,330
un selekcijas kritērijiem:

50
00:06:30,160 --> 00:06:34,560
vienmērīga un aktīva augšana, 
blīvu galviņu attīstība,

51
00:06:34,865 --> 00:06:40,734
uzglabāšanas kapacitāte, 
agrīnība un salcietība. 

52
00:06:43,563 --> 00:06:48,276
Pārziemošana ir riskantākais 
sēklu ražošanas posms.

53
00:06:49,680 --> 00:06:54,996
Augu ziemošanai izstrādātas vairākas metodes -
saskaņā ar klimatu,

54
00:06:55,200 --> 00:06:59,018
attīstības cikla posmiem 
un pieejamo infrastruktūru. 

55
00:07:11,854 --> 00:07:14,200
Skarbāku ziemu reģionos -

56
00:07:14,523 --> 00:07:20,072
rudens beigās 
tiek ievākts viss augs ar saknēm.

57
00:07:21,949 --> 00:07:25,949
Ārējās lapas tiek norautas, 
lai paliktu vienīgi

58
00:07:26,130 --> 00:07:28,414
ciešās, stingrās, galviņu veidojošās lapas.

59
00:07:36,392 --> 00:07:39,578
Galvām jābūt sausām - bez augsnes daļiņām. 

60
00:07:44,080 --> 00:07:48,254
Reģionos ar zemāku gaisa mitrumu -

61
00:07:48,850 --> 00:07:52,552
augi uzglabājami 
zemes klona pagrabos. 

62
00:07:57,560 --> 00:08:04,196
Reģionos ar augstu gaisa mitrumu - 
augi glabājami neaizsalstošā telpā vai bēniņos.

63
00:08:05,607 --> 00:08:10,007
Temperatūra telpā 
nedrīkst ilgstoši nokristies zem 0°C,

64
00:08:10,145 --> 00:08:17,140
lai gan kāposti var īslaicīgi 
pārciest -5°C sala periodus. 

65
00:08:19,280 --> 00:08:22,800
Ieziemotos kāpostus 
nepieciešams ‘’pieskatīt’’.

66
00:08:23,870 --> 00:08:29,440
Ārējām lapām var kaitēt 
pelēkā puve (Botrytis cinerea).

67
00:08:30,320 --> 00:08:33,120
Tādas lapas aizvācamas - 
līdz ar iepuvušajām auga daļām.

68
00:08:33,338 --> 00:08:37,149
Bojātās vietas dezinficējamas 
ar koksnes pelniem. 

69
00:08:40,370 --> 00:08:45,905
Dažas no izturīgākajām 
vai maigāku ziemu reģionos audzētām šķirnēm

70
00:08:46,349 --> 00:08:48,930
var pārziemot augsnē. 

71
00:08:52,843 --> 00:08:56,829
Maigākā klimatā - 
kāpostus var uzglabāt zemē.

72
00:08:57,265 --> 00:09:00,814
Kāposti ar visām saknēm 
tiek ievietoti dziļos tuneļos,

73
00:09:01,000 --> 00:09:04,400
nedaudz piepacelti 
un pārklāti ar augsni.

74
00:09:08,203 --> 00:09:10,552
Augi nedrīkst savstarpēji saskarties.

75
00:09:11,180 --> 00:09:15,200
Ja iestājas sals, 
zeme pārsedzama ar stikla paneli,

76
00:09:15,483 --> 00:09:17,890
lopu mēsliem vai kritušām lapām. 

77
00:09:28,705 --> 00:09:33,789
Pavasarī aizsargslānis tiek aizvākts, 
bet augus nepārstāda.

78
00:09:34,276 --> 00:09:37,985
Tie izsprauksies cauri augsnes slānim 
un uzziedēs. 

79
00:09:47,040 --> 00:09:51,090
Cita metode paredz 
sakņu saglabāšanu bez galviņām,

80
00:09:51,454 --> 00:09:52,647
kas var tikt apēstas.

81
00:09:53,207 --> 00:09:56,203
Vasaras beigās (sausākā periodā) -

82
00:09:56,552 --> 00:10:00,014
galvas tiek nogrieztas pie pamatnes ieslīpā leņķī.

83
00:10:00,980 --> 00:10:03,854
Patur tikai kātu un saknes. 

84
00:10:07,963 --> 00:10:12,363
Kātu un saknes žāvē vairākas dienas, 
dezinficējot ar koksnes pelniem.

85
00:10:13,563 --> 00:10:18,145
Puves riska novēršanai - 
iegriezumu ieziež ar potēšanas vasku. 

86
00:10:23,818 --> 00:10:28,123
Tādējādi nodrošināma pārziemošana,

87
00:10:28,334 --> 00:10:33,127
taču bez galvām uzglabātie kāti 
ražos mazāk (zemākas kvalitātes) sēklu.

88
00:10:35,054 --> 00:10:38,058
Augs neziedēs no kāta viduča,

89
00:10:38,378 --> 00:10:42,072
kur veidojas skaistākie sēklu stiebri 
ar labākajām sēklām. 

90
00:10:47,360 --> 00:10:52,029
Pagrabā vai bēniņos 
pārziemojuši sēklas augi

91
00:10:52,530 --> 00:10:56,021
tiek pārstādīti 
cikla otrā gada pavasarī -

92
00:10:56,247 --> 00:10:57,927
martā vai aprīlī. 

93
00:11:01,680 --> 00:11:08,327
Augus ierok zemē, ievērojot 60 cm atstatumu. 
Galviņu virsmas paliek zemes līmenī.

94
00:11:09,360 --> 00:11:11,890
Augi dzīs jaunas saknes.

95
00:11:14,596 --> 00:11:18,269
Pēc pārstādīšanas 
un sakņu dzīšanas laikā -

96
00:11:18,480 --> 00:11:21,112
kāpostus nepieciešams dāsni aplaistīt. 

97
00:11:22,960 --> 00:11:26,283
Lai ziedkāti 
straujāk izsprauktos no galviņām -

98
00:11:26,560 --> 00:11:32,910
galviņas virspusē ar nazi 
dažkārt ieteicams veikt

99
00:11:33,170 --> 00:11:34,327
krustveida iegriezumu.

100
00:11:34,720 --> 00:11:39,980
Tam jābūt 3 - 6 cm dziļam 
(atbilstoši galviņas izmēram).

101
00:11:41,541 --> 00:11:46,843
Sargieties neiegriezt kāposta pamatnē, 
no kuras augs sēklu kāti.

102
00:11:48,865 --> 00:11:55,098
Ja ziedkāts neparādās - 
iegriezumu var veikt atkārtoti. 

103
00:12:11,440 --> 00:12:14,792
Centrālais ziedkāts 
ražos labākās sēklas.

104
00:12:17,032 --> 00:12:22,400
Vājākus sekundāros kātus var nogriezt, 
lai vidus ziedkopa attīstītos veiksmīgāk

105
00:12:22,523 --> 00:12:27,905
un viss auga spēks 
tiktu virzīts sēklu briedināšanai. 

106
00:13:03,250 --> 00:13:06,632
Ziedkāti var sasniegt 
2 m garumu,

107
00:13:07,076 --> 00:13:09,629
tādēļ katrs augs 
balstāms ar mietiņu.

108
00:13:09,956 --> 00:13:14,232
Mietiņi nostiprināmi, jo kāti, veidojoties sēklām, 
mēdz kļūt ļoti smagi. 

109
00:13:35,992 --> 00:13:42,821
Brassica oleracea
ievākšana, šķirošana un uzglabāšana 

110
00:13:51,832 --> 00:13:55,636
Sēklas ir gatavas - 
pākstīm vēršoties smilškrāsā.

111
00:13:59,810 --> 00:14:02,392
Pākstis viegli pārsprāgst.

112
00:14:02,560 --> 00:14:07,665
Tas nozīmē - nobriedušas pākstis 
viegli atveras un strauji izplata sēklas. 

113
00:14:16,138 --> 00:14:20,218
Lielākoties visi stiebri 
nenobriest vienlaicīgi.

114
00:14:21,207 --> 00:14:27,410
Lai nezaudētu sēklas, tās ievācamas atsevišķi - 
no katra nobriedušā stiebra.

115
00:14:28,920 --> 00:14:34,900
Iekams nogatavojušās visas sēklas - 
var ievākt arī augu kopumā. 

116
00:14:37,992 --> 00:14:43,178
Briedināšana turpināma - 
žāvējot augu sausā,

117
00:14:43,320 --> 00:14:45,403
labi vēdinātā vietā. 

118
00:14:57,621 --> 00:15:03,781
Kāpostu sēklas ievācamas, 
kad pākstis viegli atveramas ar pirkstiem. 

119
00:15:05,730 --> 00:15:07,130
Lai ievāktu sēklas -

120
00:15:07,258 --> 00:15:12,574
pākstis tiek izbērtas uz plastmasas plēves 
vai bieza auduma gabala.

121
00:15:12,952 --> 00:15:15,789
Tad pākstis tiek izkultas vai saberzētas plaukstās.

122
00:15:19,200 --> 00:15:24,429
Pākstis var arī ievietot maisiņā - 
un izkult pret mīkstu virsmu. 

123
00:15:25,672 --> 00:15:30,589
Lielāki apjomi kuļami - 
tos mīdot vai pārbraucot tiem pāri. 

124
00:15:42,960 --> 00:15:48,312
Ja pākstis neatveras viegli - 
tajās slēpjas nenobriedušas sēklas,

125
00:15:48,509 --> 00:15:50,480
kam būs raksturīga vāja dīgtspēja. 

126
00:15:54,830 --> 00:15:56,232
Šķirošanas gaitā -

127
00:15:56,480 --> 00:16:01,003
sēklas vispirms tiek sijātas 
rupjākā sietā,

128
00:16:01,345 --> 00:16:03,220
kas aiztur pelavas.

129
00:16:07,832 --> 00:16:10,538
Tad sēklas tiek sijātas citā sietā,

130
00:16:10,850 --> 00:16:15,716
kas aiztur sēklas, 
bet atsijā smalkākās daļiņas. 

131
00:16:19,170 --> 00:16:24,865
Pēdīgi - sēklas vētījamas, 
pūšot pār tām vai izmantojot vēja spēku,

132
00:16:25,040 --> 00:16:27,636
lai aizvāktu pelavu paliekas. 

133
00:16:42,603 --> 00:16:47,745
Visas Brassica oleracea kāpostu sēklas 
ir savstarpēji līdzīgas.

134
00:16:48,545 --> 00:16:55,258
Grūti atšķirt, piemēram, 
kāpostu un puķkāpostu sēklas.

135
00:16:55,760 --> 00:16:58,821
Tādēļ svarīgi iezīmēt augus

136
00:16:58,930 --> 00:17:02,734
un iegūtajām sēklām pievienot
etiķeti ar sugas un šķirnes

137
00:17:03,010 --> 00:17:06,232
nosaukumu, kā arī ievākšanas gadu. 

138
00:17:07,447 --> 00:17:12,261
Dažas dienas uzglabājot sēklas saldētavā, 
tiks iznīcināti parazīti. 

139
00:17:17,665 --> 00:17:20,850
Kāpostu sēklu dīgtspēja saglabājas 5 gadus.

140
00:17:21,854 --> 00:17:25,607
Taču dīgšanas kapacitāte 
var ieilgt līdz pat 10 gadiem.

141
00:17:27,338 --> 00:17:30,727
Termiņš pagarināms - 
glabājot sēklas saldētavā.

142
00:17:31,629 --> 00:17:38,196
Vienā gramā ietilpst 250 - 300 sēklas 
(atbilstoši šķirnes īpašībām). 
