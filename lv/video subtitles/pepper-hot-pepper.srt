1
00:00:12,100 --> 00:00:16,780
Saldie un čili pipari 
piederīgi Solanaceae dzimtai.

2
00:00:18,065 --> 00:00:22,390
Capsicum ģintī 
ietilpst 5 kultivētās sugas:

3
00:00:22,940 --> 00:00:34,205
Capsicum baccatum, Capsicum chinense, 
Capsicum frutescens un Capsicum pubescens.

4
00:00:35,580 --> 00:00:38,860
Vairums kultivēto šķirņu

5
00:00:39,155 --> 00:00:42,060
piederīgas Capsicum annuum sugai -

6
00:00:46,185 --> 00:00:51,060
un tādu šķirņu ir tūkstošiem, 
to skaitā: saldie pipari,

7
00:00:53,300 --> 00:00:56,980
viegli asie čili 
un asie čili pipari.

8
00:00:58,915 --> 00:01:04,090
Pipariem raksturīga izmēra, 
formas un krāsas daudzveidība.

9
00:01:07,435 --> 00:01:08,340
Apputeksnēšana

10
00:01:21,440 --> 00:01:26,045
Piparu ziedi ir pašapaugļojoši 
un hermafrodīti.

11
00:01:26,185 --> 00:01:31,980
Tas nozīmē - vīrišķie un sievišķie vairošanās orgāni 
atrodas uz viena zieda un ir savietojami.

12
00:01:35,380 --> 00:01:37,615
Tādi augi ir autogāmi augi.

13
00:01:40,220 --> 00:01:43,380
Taču ziedus var apputeksnēt arī kukaiņi,

14
00:01:43,440 --> 00:01:47,490
piemēram, bites un kamenes. 

15
00:01:51,565 --> 00:01:54,120
Augus spēcīgi ietekmē 
temperatūras maiņas.

16
00:01:55,350 --> 00:02:02,575
Ja naktī temperatūra ir pārāk augsta (29°C) 
vai pārāk zema (5°C),

17
00:02:02,740 --> 00:02:06,540
ziedi neattīstīsies - 
tie netiks apputeksnēti.

18
00:02:07,500 --> 00:02:11,765
Tā rezultātā, pipari saražos maz sēklu 
vai neražos sēklas vispār.

19
00:02:12,235 --> 00:02:15,015
Temperatūra ietekmē 
augļa izmēru.

20
00:02:22,370 --> 00:02:29,020
Veiksmīgu apaugļošanu nodrošina 
nakts temperatūras no 12° - 16°C. 

21
00:02:32,910 --> 00:02:35,100
Pašsaputes veicināšanai -

22
00:02:35,420 --> 00:02:39,320
ziedēšanas laikā 
augus iespējams regulāri papurināt. 

23
00:02:42,140 --> 00:02:46,825
Visas Capsicum ģints sugas 
var savstarpēji krustoties

24
00:02:47,510 --> 00:02:50,840
(izņemot Capsicum pubescens sugu). 

25
00:02:58,215 --> 00:03:02,935
Lai novērstu šķirņu krustošanos 
mērena klimata apstākļos -

26
00:03:03,490 --> 00:03:06,050
ievērojiet 100 m atstatumu. 

27
00:03:07,550 --> 00:03:13,910
Attālums samazināms līdz 50 m, 
ja pastāv dabiska barjera (piemēram, dzīvžogs). 

28
00:03:18,080 --> 00:03:22,925
Tropu klimatā - 
atstatumam jāsasniedz 1 km. 

29
00:03:23,600 --> 00:03:26,780
Vai 500 m - ja ir dzīvžogs. 

30
00:03:31,485 --> 00:03:37,505
Kukaiņu ierosinātas krustošanās novēršanai -
izolējiet augus ar tīkliem

31
00:03:38,300 --> 00:03:42,120
(tuneļu sistēmās 
vai slēgtā tīkla sprostā).

32
00:03:44,470 --> 00:03:47,840
Taču pipariem nepieciešams 
daudz gaismas.

33
00:03:48,780 --> 00:03:53,140
Pārāk cieši un ierobežojoši tīkli 
kavēs augu attīstību,

34
00:03:53,680 --> 00:03:56,290
mazinot augļu un sēklu ražīgumu.

35
00:03:59,505 --> 00:04:04,815
Sīkāk - ‘’Sēklu ražošanas ābeces’’ 
izolēšanas metodēm veltītajā modulī. 

36
00:04:14,300 --> 00:04:15,340
Dzīves cikls 

37
00:04:31,505 --> 00:04:34,500
Pipari lielākoties ir viengadīgi augi,

38
00:04:36,880 --> 00:04:40,180
taču dažas sugas tropu klimatā 
mēdz būt arī daudzgadīgas.

39
00:04:45,395 --> 00:04:48,510
Auga attīstībai 
nepieciešama tveice. 

40
00:04:54,820 --> 00:04:59,340
Sēklai audzētie pipari tiek kultivēti 
tāpat kā pārtikas ieguvei. 

41
00:05:10,220 --> 00:05:15,865
Ģenētisko daudzveidību nodrošinās 
6 - 12 katras šķirnes pipari. 

42
00:05:31,315 --> 00:05:36,490
Kad ziedi atplaukuši, 
auglis nobriedīs patēriņam -

43
00:05:36,780 --> 00:05:41,185
aptuveni 60 - 100 dienu laikā
(atbilstoši šķirnes īpašībām). 

44
00:05:55,005 --> 00:05:57,390
Sēklu ražošanai atlasītajiem augiem

45
00:05:57,520 --> 00:05:59,940
jābūt veselīgiem un dzīvelīgiem,

46
00:06:00,075 --> 00:06:03,215
un tiem jātiek novērotiem 
visos auga attīstības posmos, 

47
00:06:03,645 --> 00:06:06,760
lai pārliecinātos par atbilstību 
selekcijas kritērijiem. 

48
00:06:09,035 --> 00:06:14,685
Augu attīstībai jābūt vienmērīgai 
un spēcīgai, augiem jāveido vairāki ziedi,

49
00:06:15,155 --> 00:06:19,190
kas tiek veiksmīgi apaugļoti, 
savukārt zari nedrīkst viegli lūzt. 

50
00:06:19,590 --> 00:06:22,860
Pārbaudiet augļu garšu,

51
00:06:23,095 --> 00:06:29,820
šķirnes tipisko apveidu, izmēru, 
mīkstuma, kā arī mizas krāsu un biezumu. 

52
00:06:31,655 --> 00:06:35,430
Neievāciet sēklas no pārtikas ražai 
ievāktajiem pipariem -

53
00:06:35,960 --> 00:06:39,580
tādējādi nebūs novērojamas 
šķirnes īpašības

54
00:06:39,650 --> 00:06:41,915
visos attīstības posmos. 

55
00:06:46,300 --> 00:06:49,775
Lai ievāktu sēklas, 
nogaidiet pilnu briedumu.

56
00:06:52,575 --> 00:06:57,660
Zaļie augļi kļūs sarkani, 
oranži, brūni vai dzelteni.

57
00:06:59,785 --> 00:07:04,900
Bāldzeltenie augļi kļūs 
tumšdzelteni, sarkani vai oranži. 

58
00:07:06,395 --> 00:07:10,025
Nobriedušās sēklas būs dzeltenas. 

59
00:07:12,760 --> 00:07:15,275
Neievāciet sēklas 
no negataviem augļiem -

60
00:07:15,405 --> 00:07:18,650
to dīgtspēja 
būs krietni vājāka. 

61
00:07:22,875 --> 00:07:27,380
Sēklas ieteicams ievākt 
no pirmajiem nobriedušajiem augļiem.

62
00:07:29,880 --> 00:07:34,215
Vēlākas gatavības augļu sēklām
 raksturīga vājāka dīgtspēja.

63
00:07:37,065 --> 00:07:39,955
Neievāciet sēklas 
no neveseliem augiem. 

64
00:07:49,570 --> 00:07:52,460
Ievākšana - šķirošana - uzglabāšana

65
00:08:02,205 --> 00:08:03,090
Ievērojiet!

66
00:08:03,590 --> 00:08:09,180
Aso čili sēklas svarīgi ievākt 
labi vēdinātā vietā,

67
00:08:09,935 --> 00:08:14,105
ja iespējams - ārtelpās, 
lai izvairītos no kapsaicīna noplūdēm.

68
00:08:15,525 --> 00:08:19,840
Kapsaicīns var ierosināt acu, 
rīkles un deguna kairinājumu.

69
00:08:24,750 --> 00:08:29,440
Nepieciešami arī biezi gumijas cimdi 
un aizsargacenes. 

70
00:08:34,945 --> 00:08:38,830
Pārgrieziet piparu uz pusēm. 
Izgrebiet sēklas ar nazi.

71
00:08:41,440 --> 00:08:47,305
Ievietojiet tās traukā ar ūdeni - 
tukšās sēklas uzpeldēs virspusē.

72
00:08:53,360 --> 00:08:54,815
Nosmeliet tās ar sietu. 

73
00:09:06,210 --> 00:09:10,320
Derīgās sēklas skalojiet sietā 
zem tekoša ūdens. 

74
00:09:14,095 --> 00:09:17,860
Sēklas žāvējamas 
aptuveni divas dienas.

75
00:09:26,195 --> 00:09:27,255
Ieberiet sēklas

76
00:09:27,480 --> 00:09:31,725
smalkā sietā vai šķīvī;
novietojiet šķīvi sausā,

77
00:09:32,010 --> 00:09:37,555
caurvējotā, siltā vietā 
(no 23° līdz 30°C). 

78
00:09:43,190 --> 00:09:48,735
Nelieli sēklu apjomi 
žāvējami kafijas filtros.

79
00:09:49,310 --> 00:09:53,090
Filtri lieliski uzsūc mitrumu, 
turklāt sēklas tiem nepielīp.

80
00:09:54,120 --> 00:09:58,575
Katrā filtrā ievietojiet 
tikai tējkarotīti sēklu.

81
00:09:59,020 --> 00:10:00,840
Uz filtra ar marķieri pierakstiet

82
00:10:01,160 --> 00:10:04,935
sugas un šķirnes nosaukumu.

83
00:10:06,675 --> 00:10:13,805
Izkariniet maisiņus uz veļasauklas - 
sausā, caurvējotā, ēnainā, siltā vietā. 

84
00:10:15,600 --> 00:10:18,230
Nepakļaujiet sēklas saules ietekmei.

85
00:10:18,915 --> 00:10:22,130
Nežāvējiet sēklas uz papīra, 
kam tās pielips. 

86
00:10:30,945 --> 00:10:33,525
Paciņā ievietojiet etiķeti

87
00:10:33,780 --> 00:10:38,420
ar sugas un šķirnes nosaukumu,
kā arī ievākšanas gadu.

88
00:10:39,390 --> 00:10:42,010
Ārējs uzraksts mēdz nodilt. 

89
00:10:46,670 --> 00:10:50,740
Dažas dienas saldētavā 
nonāvēs parazītu kūniņas. 

90
00:10:53,855 --> 00:10:59,415
Čili un piparu sēklas 
dīgtspēju saglabā 3 - 6 gadus.

91
00:11:04,380 --> 00:11:07,785
Lai termiņu pagarinātu, 
glabājiet sēklas saldētavā. 
