1
00:00:09,565 --> 00:00:14,960
Burkāni piederīgi Apiaceae dzimtai 
un Daucus carota sugai.

2
00:00:15,565 --> 00:00:17,510
Tos audzē sakņu ieguvei.

3
00:00:21,600 --> 00:00:23,545
Pastāv divas apakšsugas:

4
00:00:25,410 --> 00:00:30,860
- Rietumu burkāns (Carota sativus),
kas parasti ir divgadīgs, 

5
00:00:35,530 --> 00:00:42,060
- un austrumu burkāns (Carota atrorubens), 
viengadīgs augs. 

6
00:00:48,315 --> 00:00:56,250
Dažas šķirnes ir agrīnas, 
bet citas tiek audzētas uzglabāšanai.

7
00:00:58,440 --> 00:01:06,025
Saknes variējas no gaišām līdz tumšām
(dzeltenas, oranžas, sarkanas vai violetas).

8
00:01:06,455 --> 00:01:09,220
Arī forma mēdz atšķirties. 

9
00:01:15,015 --> 00:01:15,985
Apputeksnēšana 

10
00:01:26,685 --> 00:01:32,645
Burkānu ziedkopas veido sīku, 
lielākoties - hermafrodītu

11
00:01:33,055 --> 00:01:35,270
ziediņu čemurus.

12
00:01:36,875 --> 00:01:39,940
Putekšņlapa (vīrišķais vairošanās orgāns)

13
00:01:41,945 --> 00:01:43,850
nobriest pirms auglenīcas

14
00:01:44,260 --> 00:01:45,960
(sievišķā vairošanās orgāna).

15
00:01:47,885 --> 00:01:51,700
Atsevišķs zieds nav spējīgs 
veikt pašapaugļošanos.

16
00:01:53,955 --> 00:01:57,210
Taču ziedi neatplaukst vienlaicīgi,

17
00:01:59,935 --> 00:02:03,585
tādēļ pašapaugļošanās 
iespējama vienā čemurā

18
00:02:04,165 --> 00:02:07,225
vai viena auga divos čemuros. 

19
00:02:08,660 --> 00:02:13,290
Apaugļošana iespējama arī 
starp dažādu augu čemuriem. 

20
00:02:14,750 --> 00:02:20,510
Tādēļ burkāns ir alogāms augs, 
ko parasti apputeksnē kukaiņi.

21
00:02:22,660 --> 00:02:26,760
Pastāv atšķirīgu šķirņu
savstarpējās krustošanās risks. 

22
00:02:44,090 --> 00:02:47,225
Burkāns var krustoties 
arī ar savvaļas burkānu,

23
00:02:47,560 --> 00:02:52,905
kas sastopams daudzos pasaules reģionos 
un kura gēni ir dominējoši

24
00:02:53,305 --> 00:02:56,115
(kā visām savvaļas sugām). 

25
00:02:57,395 --> 00:03:00,600
Savvaļas burkānu viegli atpazīt -

26
00:03:00,985 --> 00:03:05,620
ziedkopas vidū atrodams 
sīks, melns ziediņš. 

27
00:03:06,860 --> 00:03:08,770
Krustošanās novēršama -

28
00:03:09,150 --> 00:03:13,650
divas burkānu šķirnes audzējot 
aptuveni 1 km atstatumā. 

29
00:03:16,420 --> 00:03:19,840
Attālums samazināms līdz 500 m,

30
00:03:20,330 --> 00:03:25,635
ja starp šķirnēm pastāv dabiska barjera 
(piemēram, dzīvžogs). 

31
00:03:28,225 --> 00:03:30,365
Šķirnes iespējams arī izolēt -

32
00:03:30,585 --> 00:03:34,425
pamīšus atverot un aizverot 
kukaiņu tīklus

33
00:03:40,790 --> 00:03:46,065
vai slēgtā tīklā ievietojot 
nelielas kukaiņu ligzdas.

34
00:03:47,330 --> 00:03:48,560
(Ar izolācijas metodēm

35
00:03:48,855 --> 00:03:54,480
iepazīstieties 
‘’Sēklu ražošanas ābeces’’ modulī.) 

36
00:04:01,930 --> 00:04:04,060
Burkāna dzīves cikls 

37
00:04:12,695 --> 00:04:17,455
Austrumu burkānu šķirnes 
lielākoties ir viengadīgas -

38
00:04:17,535 --> 00:04:20,830
ja tās tiek audzētas periodā, 
kad dienas ir garas.

39
00:04:22,870 --> 00:04:25,485
Sēklas nogatavosies jau pirmajā gadā. 

40
00:04:31,215 --> 00:04:36,755
Rietumu burkānu sēklu ražošanai 
nepieciešami divi gadi.

41
00:04:37,090 --> 00:04:41,830
Pirmajā gadā 
veidosies sakne un lapu cers. 

42
00:04:43,910 --> 00:04:47,840
Burkāniem nepieciešams 
jarovizācijas periods (ziema).

43
00:04:48,400 --> 00:04:52,030
Otrajā gadā burkāni ziedēs 
un briedinās sēklas. 

44
00:04:54,405 --> 00:04:58,890
Agrīnās burkānu šķirnes 
tiek sētas iespējami savlaicīgi,

45
00:04:59,365 --> 00:05:03,585
lai augi nebūtu pārāk nogatavojušies 
ziemas uzglabāšanai.

46
00:05:04,055 --> 00:05:08,810
Citādi nākamajā pavasarī 
tiem būs grūti ataugt. 

47
00:05:39,670 --> 00:05:41,085
Atbilstoši reģiona klimatam -

48
00:05:41,255 --> 00:05:46,080
sēklas burkāni tiek ieziemoti 
dažādos veidos.

49
00:05:46,835 --> 00:05:50,435
Vienkāršākā metode - 
burkāni atstājami dārza augsnē,

50
00:05:50,555 --> 00:05:51,975
ja to pieļauj klimata apstākļi.

51
00:05:55,140 --> 00:06:00,120
Salmu slānis būs pietiekama aizsardzība 
pret vieglu salu. 

52
00:06:02,020 --> 00:06:06,820
Aukstāka klimata reģionos 
ar lielāku sala risku -

53
00:06:07,540 --> 00:06:12,410
pirms ziemas iestāšanās augi jāizrok 
un jāglabā no sala pasargātā vietā. 

54
00:06:14,720 --> 00:06:18,040
Augi, kas uzziedējuši pirmajā gadā, 
netiek paturēti,

55
00:06:18,485 --> 00:06:20,980
jo tādu augu sēklām piemīt tendence

56
00:06:21,150 --> 00:06:24,645
katrā ražā 
izziedēt arvien agrāk.

57
00:06:29,305 --> 00:06:33,340
Tāpat nav lietojami burkāni 
ar zaļu kakliņu

58
00:06:33,795 --> 00:06:36,355
vairākām saknēm, ieplīsušu mizu. 

59
00:06:39,810 --> 00:06:45,390
Saknes tiek tīrītas bez ūdens palīdzības. 
Lapas tiek nogrieztas virs kakliņa.

60
00:06:46,855 --> 00:06:50,575
Burkānus īslaicīgi žāvē 
zem atklātas debess. 

61
00:07:20,175 --> 00:07:24,940
Selekcija norit saskaņā 
ar šķirnes specifiskajiem kritērijiem:

62
00:07:25,715 --> 00:07:31,155
krāsu, apveidu, audzelību, uzglabājamību.

63
00:07:38,910 --> 00:07:42,655
Tāpat svarīgi sekot 
garšas īpašībām,

64
00:07:43,080 --> 00:07:46,960
jo - pat vienai šķirnei piederīgi burkāni 
var garšot gluži atšķirīgi. 

65
00:07:51,340 --> 00:07:54,845
Lai pagaršotu - nogrieziet burkāna galiņu.

66
00:07:55,865 --> 00:07:59,175
Tad saknes tiek dezinficētas ar koksnes pelniem. 

67
00:08:05,180 --> 00:08:08,375
Atlasītās saknes tiek ievietotas 
smilšu kastē,

68
00:08:08,505 --> 00:08:13,240
kas pasargāta no sala, 
vai sastatītas vertikāli - koka kastēs.

69
00:08:19,295 --> 00:08:26,230
Ideāli uzglabāšanas apstākļi:
 1°C, pie 90 - 95% mitruma līmeņa. 

70
00:08:35,010 --> 00:08:38,450
Ziemas gaitā 
saknes regulāri pārbaudāmas,

71
00:08:38,760 --> 00:08:40,985
atlasot visas iepuvušās. 

72
00:09:10,735 --> 00:09:14,115
Saknes tiek pārstādītas 
pavasara sākumā -

73
00:09:14,500 --> 00:09:17,450
kolīdz nav gaidāms bargāks sals.

74
00:09:17,800 --> 00:09:22,555
Jārūpējas, 
lai pārstādītās saknes neizkalstu.

75
00:09:23,230 --> 00:09:28,355
Pārstādītās saknes pie gaismas 
jāradina pakāpeniski, sargājot no tiešas saules. 

76
00:09:31,055 --> 00:09:37,650
Dažos reģionos - kultivētie burkāni 
zied vienlaikus ar savvaļas burkāniem.

77
00:09:37,920 --> 00:09:42,165
Lai tie nekrustotos, 
jāpārvirza ziedēšanas periods.

78
00:09:42,510 --> 00:09:43,635
To paturot prātā,

79
00:09:43,735 --> 00:09:48,735
ziemas beigās sēklu ieguvei paredzētās saknes 
stādāmas podiņos -

80
00:09:49,115 --> 00:09:54,770
lecektī, labi apgaismotā vietā, 
kur tās neskars sals.

81
00:10:15,080 --> 00:10:18,295
Līdzko tas iespējams - 
saknes tiek pārstādītas ārā. 

82
00:10:18,920 --> 00:10:24,660
Ģenētiskās daudzveidības uzturēšanai 
nepieciešami vismaz 30 augi,

83
00:10:25,785 --> 00:10:29,825
taču ieteicams paturēt 50 - 100 augus.

84
00:10:51,830 --> 00:10:55,015
Augšanas gaitā 
burkāni stutējami ar mietiņiem. 

85
00:10:59,080 --> 00:11:03,930
Burkāns veido vairākus čemurus, 
kas nezied vienlaicīgi.

86
00:11:04,750 --> 00:11:09,905
Pirmais (primārais) čemurs 
atrodas galvenā kāta galā. 

87
00:11:11,400 --> 00:11:15,575
Sekundārie čemuri 
attīstās no galvenā kāta. 

88
00:11:21,715 --> 00:11:25,270
Terciārie čemuri izaug 
no sekundārajiem kātiem. 

89
00:11:30,535 --> 00:11:34,190
Čemuri nobriest 
ilgākā laika periodā,

90
00:11:34,475 --> 00:11:36,700
tādēļ ievācami pakāpeniski. 

91
00:11:39,775 --> 00:11:45,370
Ieteicams ievākt primāros čemurus, 
jo tie ražo labākās sēklas.

92
00:11:45,665 --> 00:11:49,520
Sekundāros čemurus ievāc 
tikai nepieciešamības gadījumā. 

93
00:11:52,505 --> 00:11:56,030
Čemurus nogriež kopā ar kāta augšgalu,

94
00:11:56,315 --> 00:11:59,055
kad sāk izbirt pirmās gatavās sēklas.

95
00:11:59,465 --> 00:12:04,890
Sēklas mēdz izsēties augsnē, 
tādēļ sliktos laikapstākļos augi apgriežami agrāk. 

96
00:12:20,280 --> 00:12:25,980
Aukstos reģionos - septembrī 
augi izrokami ar visām saknēm. 

97
00:12:40,025 --> 00:12:45,205
Turpmāk veicams žāvēšanas process - 
sausā un labi vēdinātā vietā. 

98
00:12:47,545 --> 00:12:51,990
Sēklas palēnām nobriedīs, 
augam žāvējoties. 

99
00:12:58,655 --> 00:13:01,515
Ievākšana - šķirošana - uzglabāšana

100
00:13:07,415 --> 00:13:10,395
Sēklas no čemuriem izlasa ar rokām.

101
00:13:10,775 --> 00:13:16,095
Nepieciešami cimdi, 
jo sēklas klātas ar adatiņām vai akotu.

102
00:13:16,640 --> 00:13:20,600
Paberzējot sēklas sietā - 
akots norīvēsies. 

103
00:13:24,780 --> 00:13:29,645
Sēklu šķirošanai vispirms izmantojams 
siets, kas aizturēs pelavas.

104
00:13:35,710 --> 00:13:41,260
Tad sēklas tiek ievietotas smalkākā sietā,
 kas atsijās putekļus. 

105
00:13:58,120 --> 00:14:04,290
Pēdīgi sēklas tiek vētītas.
Atlikušās pelavas var izlasīt arī ar rokām. 

106
00:14:15,700 --> 00:14:19,960
Paciņā vienmēr ievietojiet etiķeti 
ar sugas un šķirnes nosaukumu,

107
00:14:20,260 --> 00:14:25,075
kā arī ievākšanas gadu
(ārējs uzraksts var nodilt). 

108
00:14:29,145 --> 00:14:34,445
Dažas dienas uzglabājiet sēklas saldētavā, 
lai nonāvētu parazītus. 

109
00:14:40,065 --> 00:14:44,095
Burkānu sēklu dīgtspēja 
saglabājas līdz pat 5 gadiem.

110
00:14:44,530 --> 00:14:48,475
Dažkārt termiņš pagarināms 
līdz 10 gadiem.

111
00:14:49,560 --> 00:14:53,680
Termiņš paildzināms - 
glabājot sēklas saldētavā.

112
00:14:55,555 --> 00:15:01,495
Burkānu sēklas mēdz būt neaktīvas - 
trīs mēnešus pēc to ievākšanas.

113
00:15:04,660 --> 00:15:11,810
Vienā gramā (ar akotiem) 
ietilpst aptuveni 700 - 800 sēklu. 
