1
00:00:09,015 --> 00:00:13,610
Arbūzs ir viengadīgs augs, 
kas ietilpst Cucurbitaceae dzimtā

2
00:00:15,800 --> 00:00:18,680
un Citrullus lanatus sugā.

3
00:00:20,260 --> 00:00:22,590
Sastopami trīs arbūzu pamatveidi:

4
00:00:25,690 --> 00:00:27,370
saldie arbūzi,

5
00:00:29,995 --> 00:00:31,700
ievārījuma arbūzi

6
00:00:34,680 --> 00:00:40,700
un Āfrikā kultivētie arbūzi, no kuru sēklām 
iegūst eļļu un kuru mīkstums ir rūgtens. 

7
00:00:46,420 --> 00:00:47,405
Apputeksnēšana 

8
00:01:08,380 --> 00:01:11,005
Arbūzs ir vienmājnieks.

9
00:01:11,105 --> 00:01:15,220
Tas nozīmē - vīrišķie un sievišķie ziedi 
atrodas uz viena auga.

10
00:01:15,915 --> 00:01:18,525
Ziedi atplaukst 
tikai vienu dienu. 

11
00:01:20,950 --> 00:01:24,400
Sievišķo ziedu sēklotne 
paslēpta zem zieda.

12
00:01:25,310 --> 00:01:29,590
Tā ir miniatūrs arbūziņš, 
kas attīstīsties pēc apputeksnēšanās. 

13
00:01:31,730 --> 00:01:34,960
Vīrišķie ziedi atrodas 
garu kātu galos. 

14
00:01:43,815 --> 00:01:46,230
Arbūzs var pašapaugļoties -

15
00:01:46,360 --> 00:01:52,465
sievišķos ziedus apaugļo 
tā paša auga vīrišķā zieda putekšņi. 

16
00:01:58,280 --> 00:02:01,775
Tomēr izplatītāka ir svešappute.

17
00:02:02,725 --> 00:02:07,395
Arbūzu ziedus apputeksnē kukaiņi, 
galvenokārt - bites.

18
00:02:12,315 --> 00:02:15,435
Visas arbūzu šķirnes 
spēj krustoties savstarpēji

19
00:02:15,750 --> 00:02:17,810
un arī ar savvaļas arbūziem..

20
00:02:19,200 --> 00:02:24,545
Taču arbūzi nekrustojas 
ar gurķiem, melonēm vai ķirbjiem. 

21
00:02:30,385 --> 00:02:36,095
Lai novērstu krustošanos, 
starp šķirnēm ievērojams 1 km atstatums. 

22
00:02:38,970 --> 00:02:45,535
Attālums samazināms līdz 400 m, 
ja pastāv dabiska barjera, piemēram, dzīvžogs. 

23
00:02:50,155 --> 00:02:52,320
Pastāv vairākas metodes, 
kas ļauj iegūt sēklas

24
00:02:52,395 --> 00:02:56,295
no dažādu šķirņu arbūziem, 
kas audzēti tiešā tuvumā. 

25
00:02:58,560 --> 00:03:04,365
Pirmā metode: viena šķirne pārklājama 
ar kukaiņu tīklu, kurā ievietota kameņu ligzda. 

26
00:03:13,915 --> 00:03:18,680
Otrā metode: divas šķirnes 
pārklājamas ar atsevišķiem tīkliem,

27
00:03:19,320 --> 00:03:25,460
vienu atverot - kamēr otrs ir noslēgts
(pamīšus - ik pārdienas).

28
00:03:25,805 --> 00:03:27,930
Apputeksnēšanu veiks savvaļas kukaiņi.

29
00:03:28,280 --> 00:03:33,970
Tomēr sēklu ražība būs vājāka, 
jo daži ziedi netiks apputeksnēti. 

30
00:03:40,865 --> 00:03:44,375
Trešā metode: 
ziedu manuāla apputeksnēšana.

31
00:03:45,960 --> 00:03:48,755
Process ir sarežģītāks 
nekā ķirbjiem vai cukīni.

32
00:03:51,665 --> 00:03:58,530
Arbūzu ziedi ir sīkāki, 
turklāt grūti notvert ziedēšanas brīdi.

33
00:04:02,215 --> 00:04:07,320
Manuālās apputeksnēšanas efektivitāte 
sasniedz 50% - 75%.

34
00:04:09,575 --> 00:04:13,180
Ja zieds nav ticis apputeksnēts, 
tas nokalst. 

35
00:04:15,840 --> 00:04:19,310
Trīs metodes 
sīkāk raksturotas

36
00:04:19,400 --> 00:04:22,525
‘’Sēklu ražošanas ābeces’’ 
mehāniskās izolēšanas paņēmieniem

37
00:04:22,800 --> 00:04:27,065
un manuālajai apputeksnēšanai
veltītajos moduļos. 

38
00:04:37,475 --> 00:04:38,620
Dzīves cikls 

39
00:04:57,090 --> 00:04:58,980
Sēklai audzētie arbūzi

40
00:04:59,395 --> 00:05:02,820
tiek kultivēti tāpat 
kā pārtikas patēriņam.

41
00:05:12,075 --> 00:05:17,270
Arbūzu izcelsme meklējama Āfrikā -
to dīgšanai un attīstībai nepieciešams siltums.

42
00:05:24,250 --> 00:05:29,500
Ģenētisko daudzveidību 
nodrošinās vismaz 6 augi.

43
00:05:29,645 --> 00:05:31,650
Ieteicams audzēt duci. 

44
00:05:56,805 --> 00:05:59,660
Rūpīgi atlasiet 
sēklai paredzētos arbūzus -

45
00:05:59,725 --> 00:06:02,930
saskaņā ar šķirnes 
specifiskajiem kritērijiem,

46
00:06:03,350 --> 00:06:10,795
piemēram, agrīnību, augļu skaitu, 
garšu un cukuru sastāvu. 

47
00:06:14,020 --> 00:06:16,635
Pārbaudiet noturību pret slimībām. 

48
00:06:16,985 --> 00:06:20,970
Paturiet augus, kas labi attīstījušies.
Atbrīvojieties no slimiem augiem. 

49
00:06:24,680 --> 00:06:29,690
Arbūza sēklu brieduma pakāpe 
ir viegli nosakāma -

50
00:06:30,415 --> 00:06:33,860
auglim jābūt gatavam 
un lietojamam uzturā. 

51
00:06:45,940 --> 00:06:49,660
Ievākšana - šķirošana - uzglabāšana

52
00:06:56,820 --> 00:06:59,775
Lai ievāktu sēklas, 
pāršķeliet arbūzu,

53
00:07:01,545 --> 00:07:05,230
sagrieziet to šķēlēs 
un izgrebiet sēklas ar nazi. 

54
00:07:07,345 --> 00:07:08,495
Ja tuvumā ir bērni,

55
00:07:08,560 --> 00:07:11,655
viņi noteikti līksmi izknibinās 
palikušās sēkliņas. 

56
00:07:13,840 --> 00:07:16,240
Noskalojiet sēklas ūdenī. 

57
00:07:20,320 --> 00:07:25,260
Lai atbrīvotos no sterilām sēklām, 
ieberiet sēklas traukā ar ūdeni.

58
00:07:27,560 --> 00:07:33,140
Auglīgās sēklas nogrims, 
bet tukšās sēklas uzpeldēs virspusē. 

59
00:07:59,430 --> 00:08:01,090
Atstājiet sēklas žāvēties 
ēnainā vietā. 

60
00:08:03,300 --> 00:08:07,750
Iebakstot nagu labi izžuvušā sēklā, 
nepaliks nospiedums. 

61
00:08:11,755 --> 00:08:15,230
Vienmēr maisiņā ievietojiet 
etiķeti ar sugas un šķirnes

62
00:08:15,345 --> 00:08:20,960
nosaukumu, kā arī ievākšanas gadu
(ārējs uzraksts mēdz nodilt). 

63
00:08:25,655 --> 00:08:29,040
Dažas dienas saldētavā nonāvēs parazītus. 

64
00:08:32,540 --> 00:08:36,940
Arbūzu sēklu dīgtspēja 
saglabājas vidēji 5 gadus,

65
00:08:37,290 --> 00:08:39,115
taču var ilgt 
līdz pat 10 gadiem.

66
00:08:43,655 --> 00:08:47,130
Termiņš pagarināms, 
glabājot sēklas saldētavā. 
