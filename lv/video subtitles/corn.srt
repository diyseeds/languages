1
00:00:11,520 --> 00:00:16,996
Kukurūza (gluži kā vairākums graudzāļu) 
piederīga Poaceae dzimtai.

2
00:00:17,160 --> 00:00:19,774
Augs ietilpst Zea mays sugā.

3
00:00:21,716 --> 00:00:23,643
Pastāv sekojoši kukurūzas veidi:

4
00:00:24,650 --> 00:00:27,912
- Cukurkukurūza (Zea mays zaccharata) -

5
00:00:28,952 --> 00:00:34,630
ēdama neapstrādāta, kamēr vālītes 
nav nobriedušas (vārīta ūdenī vai grilēta).

6
00:00:36,960 --> 00:00:39,520
Izžūstot - sēklas saraujas.

7
00:00:46,000 --> 00:00:48,618
Piemērota audzēšanai vēsākos reģionos. 

8
00:00:53,280 --> 00:00:57,156
- Zobkukurūza (Zea mays indentata) -

9
00:00:57,640 --> 00:01:04,094
sēklu veido cieti saturošs kodols, 
ko ietver blīvāks, stiklains slānis.

10
00:01:05,690 --> 00:01:09,323
Cieti saturošais viducis nobriestot atkāpjas,

11
00:01:09,680 --> 00:01:13,505
veidojot zobam līdzīgo apveidu
(tādēļ - ''zobkukurūza'').

12
00:01:15,050 --> 00:01:20,647
Šķirne izmantojama 
miltu ražošanā un lopbarībā. 

13
00:01:31,992 --> 00:01:35,476
- Kramkukurūza (Zea mays indurata) -

14
00:01:37,934 --> 00:01:43,178
atšķirībā no zobkukurūzas, 
kodolam raksturīgs zemāks cietes sastāvs

15
00:01:43,629 --> 00:01:50,000
un izteiktāks stiklainais slānis, 
tādēļ no sēklas parasti ražo kukurūzas mannu. 

16
00:01:56,494 --> 00:02:05,614
- Plīsējkukurūza (Zea mays microsperma vai everta) -
kulinārijas procesā sēklas pārsprāgst (popkorns). 

17
00:02:22,100 --> 00:02:23,100
Apputeksnēšana 

18
00:02:29,185 --> 00:02:30,836
Kukurūza ir vienmājniece -

19
00:02:31,200 --> 00:02:35,192
vīrišķie un sievišķie ziedi 
atrodas uz viena auga.

20
00:02:37,832 --> 00:02:42,603
Vīrišķais zieds (pušķītis) 
izvietots stublāja galā,

21
00:02:44,450 --> 00:02:48,116
bet sievišķais zieds - 
stublāja viducī,

22
00:02:49,090 --> 00:02:56,014
vālītes iedīgļa pietūkušajā izvirzījumā
(redzami vien zīdainie matiņi). 

23
00:03:00,392 --> 00:03:03,301
Katrs matiņš savienots ar sēklaizmetni,

24
00:03:03,694 --> 00:03:08,152
kas pēc apaugļošanās 
attīstīs vienu vālītes graudiņu. 

25
00:03:12,080 --> 00:03:17,083
Kukurūza ir alogāms augs - 
viens stāds apputeksnē otru.

26
00:03:18,552 --> 00:03:23,585
Tā ir arīdzan anemofīls augs - 
kukurūzu apputeksnē vējš,

27
00:03:23,949 --> 00:03:27,963
kas izplata putekšņus vairāk nekā 10 km attālumā. 

28
00:03:32,618 --> 00:03:36,283
Kukurūza stādāma grupās 
(vismaz 3 rindās),

29
00:03:36,370 --> 00:03:39,890
lai vējš spētu 
pienācīgi izplatīt putekšņus.

30
00:03:41,367 --> 00:03:43,130
Ja rinda ir tikai viena -

31
00:03:43,345 --> 00:03:48,610
apputeksnēšana noritēs vāji 
un vālītes ražas laikā nebūs pilnībā nobriedušas. 

32
00:03:51,149 --> 00:03:55,352
Dažos reģionos 
kukurūzas bagātīgie putekšņu apjomi

33
00:03:55,440 --> 00:03:58,843
var piesaistīt arī bites.

34
00:04:00,349 --> 00:04:05,287
Viens augs spēj saražot 
līdz pat 18 miljoniem ziedputekšņu graudiņu! 

35
00:04:10,749 --> 00:04:17,214
Lai novērstu krustošanos - 
divas šķirnes stādāmas vismaz 3 km atstatumā. 

36
00:04:19,927 --> 00:04:26,378
Attālums samazināms līdz 1 km, 
ja pastāv dabiska barjera (piemēram, dzīvžogs). 

37
00:04:29,127 --> 00:04:32,058
Pielietojama arī temporālās izolācijas metode.

38
00:04:33,236 --> 00:04:39,643
Divas kukurūzas šķirnes tiek sētas vienā dārzā - 
ievērojot dažu nedēļu intervālu.

39
00:04:40,923 --> 00:04:45,287
Mērķis: izvairīties no vienas šķirnes 
vīrišķo ziedu putekšņu izplatīšanās

40
00:04:45,730 --> 00:04:50,385
laikā, kad parādās 
otrās šķirnes sievišķie ziedi.

41
00:04:51,701 --> 00:04:55,461
Citādi šķirnes 
neizbēgami krustosies. 

42
00:04:56,858 --> 00:05:01,745
Lai šķirnes temporāli izolētu,
jāņem vērā augšanas cikla ilgums

43
00:05:02,276 --> 00:05:07,740
(no 155 līdz 120 dienām) - 
atbilstoši šķirnes īpašībām. 

44
00:05:09,781 --> 00:05:12,574
Gandrīz neiespējami pasargāt savu šķirni

45
00:05:12,850 --> 00:05:17,607
no netālu audzētām 
industriālās kukurūzas hibrīdšķirnēm. 

46
00:05:19,389 --> 00:05:24,778
Šādos apstākļos - 
ieteicama manuāla apputeksnēšana.

47
00:05:26,378 --> 00:05:31,498
Starp rindām ievērojams lielāks atstatums, 
lai pietiktu vietas, kur pārvietoties. 

48
00:05:34,720 --> 00:05:38,792
Nepieciešami cieti, 
ūdensizturīgi papīra maisiņi. 

49
00:05:42,414 --> 00:05:46,450
Kukurūza zied 10 - 14 dienas.

50
00:05:48,814 --> 00:05:52,472
Manuālai apputeksnēšanai nepieciešamas 3 dienas. 

51
00:05:55,541 --> 00:05:59,825
Pirmajā dienā 
tiek apklāti sievišķie ziedi.

52
00:06:01,640 --> 00:06:06,167
Tas paveicams - iekams no mazajām vālītēm 
parādījušies matiņi.

53
00:06:07,338 --> 00:06:10,450
Ja matiņi jau redzami, 
papīra maisiņus

54
00:06:10,610 --> 00:06:11,723
vairs nav vērts lietot. 

55
00:06:14,596 --> 00:06:18,850
Vispirms tiek apgrieztas 
mizveida lapiņas ap sīko vālīti,

56
00:06:18,980 --> 00:06:22,356
lai atklātu skatam jaunos matiņus. 

57
00:06:31,498 --> 00:06:36,480
Vālīti pārklāj ar maisiņu, 
ko cieši piestiprina vālītes pamatnei. 

58
00:06:42,232 --> 00:06:45,450
Vīrišķos ziedus apklāj 
trešās dienas rītā,

59
00:06:45,636 --> 00:06:48,203
kad putekšnīcas (vīrišķie orgāni)

60
00:06:48,400 --> 00:06:52,363
sāk parādīties no stublāja vertikālā 
un laterālajiem jeb sānu atzariem. 

61
00:06:58,530 --> 00:07:02,625
Ja putekšnīcas vēl zaļas, 
maisiņi apturēs to attīstību. 

62
00:07:05,301 --> 00:07:09,830
Pirms apklāšanas - papuriniet augus,
lai atbrīvotos no bišu vai vēja izplatītajiem,

63
00:07:09,963 --> 00:07:13,229
citu šķirņu saražotajiem putekšņiem. 

64
00:07:29,120 --> 00:07:34,349
Maisiņš piestiprināms, 
lai uzkrātu rīta cēlienā atbrīvotos putekšņus. 

65
00:07:49,476 --> 00:07:53,956
Putekšņu visvairāk - 
kad rasa apžuvusi, pirms pusdienas laika. 

66
00:07:57,280 --> 00:08:00,181
Putekšņus nobirdina - 
ziedus papliķējot. 

67
00:08:09,410 --> 00:08:14,196
Manuālā apputeksnēšana veicama 
tās pašas dienas pusdienlaikā,

68
00:08:14,298 --> 00:08:19,141
jo pēcpusdienā putekšņi maisiņā 
būs pārāk sakarsuši un zaudējuši auglību. 

69
00:08:20,523 --> 00:08:23,745
Rīta beigās - 
pirms pieņēmusies dienas tveice -

70
00:08:24,247 --> 00:08:27,541
tiek atvērti 
putekšņu ievākšanas maisiņi.

71
00:08:37,970 --> 00:08:39,620
Putekšņi tiek sajaukti kopā.

72
00:08:41,512 --> 00:08:44,596
Tiek atsegts vālīti klājošais maisiņš.

73
00:08:44,741 --> 00:08:50,145
Divu dienu laikā 
matiņi būs paaugušies 3 - 4 cm. 

74
00:08:50,480 --> 00:08:56,581
Putekšņus uzklāj ar otiņu - 
visā redzamo matiņu garumā.

75
00:08:57,534 --> 00:09:00,930
Vienai vālītei nepieciešama tējkarote putekšņu. 

76
00:09:14,240 --> 00:09:19,360
Maisiņu ap vālīti tūlīt noslēdz, 
atstājot gana vietas vālītes attīstībai. 

77
00:09:20,901 --> 00:09:23,912
Matiņi būs apaugļojami 
vairākas nedēļas.

78
00:09:24,580 --> 00:09:28,305
Maisiņus no vālītēm nenoņem - 
līdz pat ražas laikam. 

79
00:09:40,210 --> 00:09:41,460
Dzīves cikls

80
00:10:00,900 --> 00:10:05,490
Kukurūza ir viengadīgs augs - 
raža nobriest gada laikā. 

81
00:10:07,389 --> 00:10:11,207
Sēklas augus kultivē 
tāpat kā pārtikas ieguvei.

82
00:10:14,458 --> 00:10:20,101
Svarīgi izvēlēties vides nosacījumiem 
piemērotāko kukurūzas šķirni.

83
00:10:26,574 --> 00:10:32,167
Ģenētiskās daudzveidības nodrošināšanai 
nepieciešami vismaz 50 augi.

84
00:10:33,090 --> 00:10:35,563
Ieteicams audzēt 200 augus. 

85
00:10:48,720 --> 00:10:53,556
Izvēlieties augus, kas pareizi attīstījušies 
un atbilst selekcijas kritērijiem:

86
00:10:53,890 --> 00:10:57,580
izmērs, krāsa,

87
00:10:58,283 --> 00:11:01,020
dzīvelīgums, attīstība,

88
00:11:01,860 --> 00:11:05,540
vālītes izmērs 
un vālītes ietvars. 

89
00:11:18,370 --> 00:11:20,618
Vālītes var izžūt uz auga. 

90
00:11:23,658 --> 00:11:28,145
Kukurūza nobriedusi, 
kad graudiņā vairs nevar iespiest nagu. 

91
00:11:32,581 --> 00:11:35,316
Tad vālītes nolasa no stublāja. 

92
00:11:41,900 --> 00:11:45,796
Vālītes mizlapas atliec, 
lai vālīte kļūtu redzama.

93
00:11:46,203 --> 00:11:49,730
Vālītes uzglabā sausā, 
labi vēdinātā vietā. 

94
00:12:10,036 --> 00:12:15,025
Iespējams nogriezt visu augu 
un žāvēt to šķūnītī. 

95
00:12:26,480 --> 00:12:29,956
Ievākšana - šķirošana - uzglabāšana

96
00:12:34,436 --> 00:12:38,880
Vālītes atlasa - 
pēc graudu formas, krāsas,

97
00:12:39,018 --> 00:12:43,774
graudu rindu skaita, 
struktūras un tekstūras īpašībām. 

98
00:12:51,230 --> 00:12:53,709
Sēklai paredzētos graudus

99
00:12:53,963 --> 00:12:59,003
izraugās no plaša vālīšu klāsta - 
ģenētiskās daudzveidības uzturēšanai.

100
00:13:20,930 --> 00:13:24,596
Ieteicams izvēlēties graudus 
tuvāk vālītes vidusdaļai. 

101
00:13:31,294 --> 00:13:36,269
Ievāciet sēklas, saberzējot vālītes. 
Izmantojiet cimdus! 

102
00:13:48,887 --> 00:13:51,781
Paciņā ievietojiet etiķeti 
ar sugas un šķirnes nosaukumu,

103
00:13:51,898 --> 00:13:58,570
kā arī ievākšanas gadu
(ārējs uzraksts mēdz nodilt). 

104
00:14:02,460 --> 00:14:07,170
Dažas dienas uzglabājot sēklas saldētavā, 
ies bojā parazīti. 

105
00:14:09,447 --> 00:14:16,589
Plīsējkukurūzas, zobkukurūzas 
un kramkukurūzas sēklas spēj dīgt 5 gadus.

106
00:14:17,643 --> 00:14:21,883
Dažkārt termiņš pagarināms 
līdz pat 10 gadiem. 

107
00:14:22,560 --> 00:14:27,207
Cukurkukurūzas sēklu dīgtspēja 
ilgst līdz 3 gadiem. 

108
00:14:28,230 --> 00:14:31,687
Termiņš pagarināms, 
sēklas uzglabājot saldētavā. 
