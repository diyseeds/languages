﻿1
00:00:12,995 --> 00:00:16,680
Briseles kāposti piederīgi 
Brassicaceae dzimtai,

2
00:00:17,340 --> 00:00:22,210
Brassica oleracea sugai
un gemmifera apakšsugai.

3
00:00:26,030 --> 00:00:30,215
Brassica oleracea sugā 
ietilpst arī kolrābji,

4
00:00:30,675 --> 00:00:37,130
brokoļi, kāposti, lapu kāposti, 
puķkāposti un virziņkāposti. 

5
00:00:42,530 --> 00:00:45,832
Auksta vai mērena klimata reģionos -

6
00:00:45,950 --> 00:00:48,770
Briseles kāposti 
ir rudens un ziemas dārzeņi.

7
00:00:51,030 --> 00:00:56,825
Lapu pamatnē veidojas nelieli dīgsti, 
kas spēj pārciest ļoti zemu temperatūru. 

8
00:01:05,760 --> 00:01:10,315
Oleracea kāpostu
apputeksnēšana: 

9
00:01:26,400 --> 00:01:30,285
Brassica oleracea ziedi
ir hermafrodīti.

10
00:01:33,250 --> 00:01:37,460
Tas nozīmē - vienā ziedā
ir gan vīrišķie, gan sievišķie orgāni.

11
00:01:41,155 --> 00:01:43,650
Vairums ir pašsterili -

12
00:01:44,265 --> 00:01:49,250
augu var apputeksnēt 
tikai cita auga ziedi.

13
00:01:53,140 --> 00:01:55,315
Tādi augi ir alogāmi augi.

14
00:01:56,285 --> 00:02:01,020
Lai apputeksnēšana būtu veiksmīga,
ieteicams audzēt vairākus augus. 

15
00:02:04,665 --> 00:02:07,615
Apputeksnēšanu veic kukaiņi.

16
00:02:11,095 --> 00:02:15,450
Tādējādi tiek dabiski uzturēta
pienācīga ģenētiskā daudzveidība.

17
00:02:22,510 --> 00:02:28,405
Visas Brassica oleracea kāpostu apakšsugas
var savstarpēji krustoties.

18
00:02:30,330 --> 00:02:34,920
Tādēļ dažādus sēklas kāpostu paveidus
nevajadzētu audzēt tuvu vienu otram. 

19
00:02:39,565 --> 00:02:40,990
Ģenētiskās tīrības nolūkos -

20
00:02:41,330 --> 00:02:48,695
dažādas Brassica oleracea sugas šķirnes
vajadzētu audzēt vismaz 1 km atstatumā. 

21
00:02:50,160 --> 00:02:53,025
Attālums samazināms līdz 500 metriem,

22
00:02:53,215 --> 00:02:57,485
ja starp šķirnēm pastāv dabiska barjera,
piemēram, dzīvžogs. 

23
00:03:00,745 --> 00:03:05,960
Tāpat šķirnes iespējams izolēt - 
ievietojot nelielas kukaiņu ligzdas

24
00:03:06,165 --> 00:03:08,340
noslēgtā kukaiņu tīklā

25
00:03:13,530 --> 00:03:17,285
vai pamīšus tīklus atverot un aizverot.

26
00:03:21,680 --> 00:03:22,830
Meklējiet pamācību

27
00:03:22,985 --> 00:03:27,270
''Sēklu ražošanas ābeces''
izolēšanas metodēm veltītajā modulī. 

28
00:03:38,710 --> 00:03:40,940
Briseles kāpostu dzīves cikls

29
00:03:56,615 --> 00:03:59,415
Briseles kāposts ir divgadīgs augs.

30
00:04:00,210 --> 00:04:04,260
Rudenī un ziemā nobriedīs
pārtikā lietojamās galviņas.

31
00:04:04,820 --> 00:04:08,100
Ziedkāti veidosies 
nākamā gada pavasarī.

32
00:04:14,360 --> 00:04:18,730
Augi sēklai tiek kultivēti 
tāpat kā pārtikas ieguvei.

33
00:04:19,840 --> 00:04:22,095
Tos sēj maijā vai jūnijā. 

34
00:04:45,350 --> 00:04:51,425
Sēklu ražošanai izraugieties 15 stādus, 
lai nodrošinātu ģenētisko daudzveidību. 

35
00:04:55,495 --> 00:04:58,065
Sēklu ievākšanai piemēroti veselīgi augi,

36
00:04:58,190 --> 00:05:01,200
kas novēroti visos attīstības posmos.

37
00:05:04,320 --> 00:05:08,595
Tādējādi tiek pārbaudītas šķirnes īpašības:

38
00:05:09,075 --> 00:05:13,440
regulāru galviņu veidošanās 
visa stiebra garumā,

39
00:05:15,035 --> 00:05:18,840
kompaktums, galviņu krāsa, forma

40
00:05:19,770 --> 00:05:22,390
un garša (ne-rūgta),

41
00:05:23,010 --> 00:05:27,010
ziemcietība, ražība 
un auga izmērs. 

42
00:05:28,495 --> 00:05:34,070
Pirmajā gadā augi var sasniegt 
60 - 80 cm garumu. 

43
00:05:36,135 --> 00:05:40,235
Rudenī ievācamas galviņas - 
visa stublāja garumā,

44
00:05:40,690 --> 00:05:43,915
izņemot galviņas, kas atrodas pašā stublāja augšgalā. 

45
00:05:46,690 --> 00:05:51,355
Briseles kāposti ir aukstumizturīgāki 
nekā lielie kāposti.

46
00:05:53,410 --> 00:05:57,415
Ziemas šķirnes 
var palikt augsnē līdz pavasarim.

47
00:05:59,980 --> 00:06:04,080
Ja nepieciešams, 
pārsedziet augus ar pretsala pārklāju. 

48
00:06:13,985 --> 00:06:19,020
Otrajā gadā - 
stublāji sasniegs 1,5 metru augstumu.

49
00:06:19,800 --> 00:06:21,135
Lai tie nesagāztos,

50
00:06:21,585 --> 00:06:25,670
dažkārt augus nepieciešams 
balstīt ar mietiņiem. 

51
00:06:27,690 --> 00:06:31,785
Stiebra augšpusi iespējams nogriezt, 
lai veicinātu ziedēšanas procesu. 

52
00:07:17,840 --> 00:07:24,640
Brassica oleracea sugas augu
ievākšana, šķirošana un uzglabāšana 

53
00:07:33,675 --> 00:07:37,525
Sēklas ir gatavas - 
pākstīm vēršoties smilškrāsā.

54
00:07:41,670 --> 00:07:43,980
Pākstis viegli pārsprāgst.

55
00:07:44,385 --> 00:07:49,310
Tas nozīmē - nobriedušas pākstis 
viegli atveras un strauji izplata sēklas. 

56
00:07:57,965 --> 00:08:02,000
Lielākoties - stiebri nenobriest vienlaicīgi.

57
00:08:03,025 --> 00:08:09,000
Lai nezaudētu sēklas, tās ievācamas atsevišķi - 
no katra nobriedušā stiebra.

58
00:08:10,835 --> 00:08:16,460
Iekams nogatavojušās visas sēklas - 
var ievākt arī augu kopumā. 

59
00:08:19,940 --> 00:08:25,045
Briedināšana turpināma - 
žāvējot augu sausā,

60
00:08:25,150 --> 00:08:26,970
labi vēdinātā vietā. 

61
00:08:39,465 --> 00:08:45,445
Kāpostu sēklas ievācamas,
kad pākstis viegli atveramas ar pirkstiem. 

62
00:08:47,570 --> 00:08:48,890
Lai ievāktu sēklas -

63
00:08:49,040 --> 00:08:54,375
pākstis tiek izbērtas uz plastmasas plēves 
vai bieza auduma gabala.

64
00:08:54,785 --> 00:08:57,710
Tad pākstis tiek izkultas vai saberzētas plaukstās.

65
00:09:01,125 --> 00:09:05,840
Pākstis var arī ievietot maisiņā - 
un izkult pret mīkstu virsmu. 

66
00:09:07,600 --> 00:09:12,275
Lielāki apjomi kuļami - 
tos mīdot vai pārbraucot tiem pāri. 

67
00:09:24,800 --> 00:09:30,090
Ja pākstis neatveras viegli - 
tajās slēpjas nenobriedušas sēklas,

68
00:09:30,310 --> 00:09:32,060
kam būs raksturīga vāja dīgtspēja. 

69
00:09:36,765 --> 00:09:37,995
Šķirošanas gaitā -

70
00:09:38,310 --> 00:09:41,395
sēklas tiek sijātas rupjākā sietā,

71
00:09:41,580 --> 00:09:44,605
kas aiztur pelavas.

72
00:09:49,685 --> 00:09:52,265
Tad sēklas tiek sijātas citā sietā,

73
00:09:52,715 --> 00:09:57,380
kas aiztur sēklas, 
bet atsijā smalkākās daļiņas. 

74
00:10:01,020 --> 00:10:04,930
Pēdīgi - sēklas vētījamas, 
pūšot pār tām

75
00:10:05,210 --> 00:10:09,245
vai izmantojot vēja spēku, 
lai aizvāktu pelavu paliekas. 

76
00:10:24,425 --> 00:10:29,380
Visas Brassica oleracea kāpostu sēklas 
ir savstarpēji līdzīgas.

77
00:10:30,390 --> 00:10:37,015
Grūti atšķirt, piemēram, 
kāpostu un puķkāpostu sēklas.

78
00:10:37,605 --> 00:10:40,500
Tādēļ svarīgi iezīmēt augus
un iegūtajām sēklām

79
00:10:40,775 --> 00:10:44,550
pievienot etiķeti 
ar sugas un šķirnes nosaukumu,

80
00:10:44,835 --> 00:10:47,855
kā arī ievākšanas gadu. 

81
00:10:49,275 --> 00:10:54,035
Dažas dienas uzglabājot sēklas saldētavā, 
tiek iznīcināti parazīti. 

82
00:10:59,480 --> 00:11:02,505
Kāpostu sēklu dīgtspēja saglabājas 5 gadus.

83
00:11:03,695 --> 00:11:07,395
Taču dīgšanas kapacitāte 
var ieilgt līdz pat 10 gadiem.

84
00:11:09,145 --> 00:11:12,405
Termiņš pagarināms - 
glabājot sēklas saldētavā.

85
00:11:13,450 --> 00:11:19,625
Vienā gramā ietilpst 250 - 300 sēklas 
(atbilstoši šķirnes īpašībām). 
