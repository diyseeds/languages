1
00:00:15,994 --> 00:00:19,620
Sēkla rodas - 
apputeksnēšanas ceļā apaugļota

2
00:00:19,780 --> 00:00:21,660
sēklaizmetņa transformācijas rezultātā.

3
00:00:23,994 --> 00:00:25,820
Sēklu veido dīglis,

4
00:00:27,620 --> 00:00:35,500
ko ietver barības vielu rezerves,
un tās aizsargā dīgļapvalks.

5
00:00:38,538 --> 00:00:42,260
Rezervju apjoms 
atkarīgs no auga sugas.

6
00:00:53,621 --> 00:01:00,180
Dažādu augu sēklu ārējais veidols 
var ievērojami atšķirties

7
00:01:00,448 --> 00:01:03,660
pēc formas, krāsas, izmēra un struktūras.

8
00:01:05,728 --> 00:01:09,780
No sēklām dīgst 
veselīgi un dzīvelīgi augi,

9
00:01:10,060 --> 00:01:15,180
kas savukārt ražo jaunas sēklas - 
turpinot auga dzīvības ciklu.

10
00:01:21,904 --> 00:01:27,146
Sēklas var izsēties 
tuvāk vai tālāk no mātesauga.

11
00:01:30,474 --> 00:01:33,860
Atkarīgi no sugas 
un vides apstākļiem -

12
00:01:34,060 --> 00:01:38,260
sēklas var nokrist zemē 
(mātesauga pakājē)

13
00:01:38,580 --> 00:01:42,620
vai tikt izplatītas attālāk, 
ja sēklas iznēsā vējš

14
00:01:42,860 --> 00:01:46,460
vai dzīvnieki, 
kuru spalvā tās ieķērušās.

15
00:01:52,293 --> 00:01:55,060
Mērķis - izplatīties, 
cik tālu vien iespējams.

16
00:01:56,260 --> 00:02:02,380
Vējš, ūdens, kukaiņi, putni un daudzi citi dzīvnieki

17
00:02:02,700 --> 00:02:06,021
var neapzināti nodrošināt 
sēklu izplatību.

18
00:02:07,861 --> 00:02:10,660
Sēklām piemīt pārsteidzoša spēja nogaidīt -

19
00:02:10,940 --> 00:02:16,896
dažkārt ļoti ilgi, līdz vides apstākļi 
kļūst piemēroti turpmākai attīstībai.

20
00:02:17,306 --> 00:02:20,380
Šo fenomenu sauc par miera periodu.

21
00:02:24,597 --> 00:02:29,900
Sēklu organiskais miera periods pārtrūkst - 
dažādu stimulu rezultātā.

22
00:02:39,340 --> 00:02:43,520
Dažām sēklām jāizkļūst cauri 
dzīvnieku zarnu traktam,

23
00:02:44,026 --> 00:02:47,740
kur tās aktivizē 
dzīvnieku gremošanas enzīmi.

24
00:02:48,133 --> 00:02:53,260
Citas sēklas stimulē 
fermentācijas procesi vai sala ietekme.

25
00:02:56,053 --> 00:03:01,500
Tādēļ arī dārzkopis dažkārt centīsies 
atdarināt dabas procesus,

26
00:03:02,060 --> 00:03:04,500
lai pārtrauktu sēklu neaktīvo periodu.

27
00:03:05,082 --> 00:03:11,820
Sējot nepieciešams gādāt 
dīgšanai labvēlīgus apstākļus:

28
00:03:12,580 --> 00:03:15,820
mitruma līmenis, temperatūra,

29
00:03:16,260 --> 00:03:19,460
gaismas intensitāte, dziļums augsnē,

30
00:03:20,074 --> 00:03:23,380
konkrētajai šķirnei 
piemērotākā sējas sezona.

31
00:03:24,160 --> 00:03:25,589
Sēklu dzīves ilgums

32
00:03:26,186 --> 00:03:29,620
ir laika periods, 
kurā tās spēj iesākt dīgšanu,

33
00:03:30,180 --> 00:03:31,780
un tas atkarīgs no sugas.

34
00:03:32,160 --> 00:03:37,540
Piemēram, pastinaku sēklas 
spēj pilnībā izdīgt tikai gadu pēc ievākšanas.

35
00:03:38,060 --> 00:03:41,900
Savukārt cigoriņu sēklas 
var sadīgt pat pēc 10 gadiem.

36
00:03:42,380 --> 00:03:46,260
Kad termiņš iztecējis - 
dīgtspēja mazinās.

37
00:03:48,298 --> 00:03:52,420
Lai pārbaudītu 
konkrētu sēklu auglīgumu,

38
00:03:52,740 --> 00:03:55,420
iespējams veikt dīgšanas testu.

39
00:03:55,700 --> 00:03:58,340
Saskaitiet izsētās sēklas.

40
00:03:59,340 --> 00:04:01,900
Tad saskaitiet 
veiksmīgi izaugušos augus.

41
00:04:03,900 --> 00:04:08,060
Ievērosiet - jo vecākas sēklas, 
jo vājāka to dīgtspēja.

42
00:04:13,946 --> 00:04:19,140
Sēklu dzīves ilgumu ietekmē 
arī žāvēšanas un uzglabāšanas apstākļi.

43
00:04:22,282 --> 00:04:26,220
Svarīgi pārliecināties, 
lai sēklas būtu pilnīgi sausas.

44
00:04:27,088 --> 00:04:30,580
Sēklas uzglabājamas 
aukstā un sausā vidē,

45
00:04:31,300 --> 00:04:34,940
kam nepiekļūst daudz gaismas
un raksturīga noteikta temperatūra.

46
00:04:36,860 --> 00:04:41,260
Karsti un mitri uzglabāšanas apstākļi 
var negatīvi ietekmēt sēklu kvalitāti.

47
00:04:43,248 --> 00:04:47,140
Tāpat sekojiet 
sīko sēklēdāju-kukaiņu aktivitātēm.

48
00:04:48,320 --> 00:04:53,740
Kukaiņu aktivitātes ierobežojamas - 
dažas dienas paturot sēklas saldētavā.

49
00:04:56,050 --> 00:04:58,042
Saldēšana ir veiksmīga 
uzglabāšanas metode.

50
00:04:58,469 --> 00:05:05,980
Jebkuras sēklas (hermētiskā iepakojumā) 
spēj izturēt ledusaukstumu (-18°C).

51
00:05:08,300 --> 00:05:10,220
Tiek pagarināts sēklu mūžs.

52
00:05:16,600 --> 00:05:18,741
Lai saglabātu sēklu audzelību,

53
00:05:19,180 --> 00:05:22,780
tās gan nepieciešams 
kultivēt regulāri.

54
00:05:25,061 --> 00:05:28,900
Sēklas iemanto spēju adaptēties 
strauji mainīgos

55
00:05:29,060 --> 00:05:30,740
vides un klimata apstākļos.
