1
00:00:08,475 --> 00:00:11,680
Lapu artišoki piederīgi Asteraceae dzimtai

2
00:00:12,015 --> 00:00:17,865
un Cynara cardunculus sugai, 
ko audzē gaļīgo stiebru ieguvei.

3
00:00:25,460 --> 00:00:31,585
Cynara ģintī ietilpst arī 
scolymus apakšsugas artišoki,

4
00:00:32,190 --> 00:00:33,955
no kuriem ievāc ziedpumpurus.

5
00:00:38,095 --> 00:00:41,740
Sēklu ieguves nolūkos -
tos audzē līdzīgi kā lapu artišokus. 

6
00:00:53,500 --> 00:00:54,475
Apputeksnēšana 

7
00:01:10,100 --> 00:01:11,495
Gluži kā artišokiem -

8
00:01:11,870 --> 00:01:17,670
arī lapu artišoku atsevišķie ziediņi 
apvienoti zilvioletās ziedgalviņās.

9
00:01:19,585 --> 00:01:22,640
Ziedi ir hermafrodīti, taču pašsterili.

10
00:01:24,740 --> 00:01:28,340
Katru ziediņu apputeksnē cits ziediņš -

11
00:01:28,630 --> 00:01:32,485
no tās pašas vai citas ziedkopas.

12
00:01:36,375 --> 00:01:39,705
Lapu artišoki un parastie artišoki 
ir alogāmi augi.

13
00:01:40,575 --> 00:01:43,170
Apputeksnēšanu veic kukaiņi. 

14
00:01:45,335 --> 00:01:49,160
Lapu artišoki un artišoki, 
kā arī dažādas, vienā dārzā audzētas

15
00:01:49,470 --> 00:01:53,350
artišoku šķirnes 
var savstarpēji krustoties. 

16
00:01:57,685 --> 00:02:03,190
Krustošanās novēršama - 
divas šķirnes audzējot 1 km atstatumā. 

17
00:02:05,735 --> 00:02:08,785
Attālums samazināms līdz 500 m,

18
00:02:09,020 --> 00:02:12,370
ja pastāv dabiska barjera 
(piemēram, dzīvžogs). 

19
00:02:15,790 --> 00:02:20,465
Šķirnes iespējams arī izolēt - 
pamīšus atverot un aizverot

20
00:02:20,555 --> 00:02:22,740
kukaiņu tīklsprostus

21
00:02:23,375 --> 00:02:28,195
vai ievietojot nelielas kukaiņu ligzdas 
slēgtā tīkla sprostā.

22
00:02:29,485 --> 00:02:30,575
Ar izolēšanas metodēm

23
00:02:30,815 --> 00:02:35,950
iepazīstieties 
“Sēklu razošanas ābeces’’ modulī. 

24
00:02:40,545 --> 00:02:41,625
Dzīves cikls 

25
00:02:47,725 --> 00:02:49,445
Pirmajā cikla gadā -

26
00:02:49,895 --> 00:02:55,085
sēklai audzētie lapu artišoki 
tiek kultivēti tāpat kā pārtikai ieguvei.

27
00:02:59,160 --> 00:03:01,490
Sēklas attīstīsies tikai otrajā gadā. 

28
00:03:12,295 --> 00:03:18,690
Lapu artišoku sēklas tiek sētas 
no vides ietekmes pasargātā podiņā

29
00:03:19,090 --> 00:03:25,025
(februāra vidū - aukstā klimatā)
vai augsnē (aprīļa beigās / maija sākumā - maigā klimatā). 

30
00:03:28,405 --> 00:03:30,745
Ģenētiskās daudzveidības nodrošināšanai

31
00:03:31,235 --> 00:03:34,615
izvēlieties vismaz duci 
sēklu ražošanas augu,

32
00:03:36,165 --> 00:03:40,260
kas atbilst izvirzītajiem šķirnes kritērijiem. 

33
00:03:43,960 --> 00:03:49,120
Atlasiet stādus, kas ir dzīvelīgākie, 
aukstumizturīgākie un pretojas puvei,

34
00:03:51,610 --> 00:03:55,540
kam ir skaistas lapas un gaļīgas, 
taču ne stiegrainas dzīslas

35
00:03:56,845 --> 00:03:59,045
(ar vai bez dzelksnīšiem). 

36
00:04:09,155 --> 00:04:13,480
Artišokus izvēlas 
atbilstoši attīstīto ziedpumpuru skaitam,

37
00:04:14,025 --> 00:04:16,520
garšai un augšanas vienmērīgumam. 

38
00:04:19,720 --> 00:04:23,475
Pirmajā gadā saziedējušie stādi 
tiek aizvākti. 

39
00:04:30,375 --> 00:04:33,415
Rudenī tiek nolasītas lapas - 
pārtikas patēriņam. 

40
00:04:45,815 --> 00:04:48,270
Skarbāku ziemu reģionos -

41
00:04:48,585 --> 00:04:53,345
saknes tiek izraktas pirms sala 
un uzglabātas vietā, kas pasargāta no aukstuma. 

42
00:05:00,165 --> 00:05:02,235
Maigāka klimata reģionos -

43
00:05:02,670 --> 00:05:07,340
lapu artišoku un artišoku saknes 
var pārziemot augsnē. 

44
00:05:12,840 --> 00:05:15,955
Pavasarī - pagrabā ziemojušie,

45
00:05:16,075 --> 00:05:19,250
sēklai paredzētie augi 
tiek pārstādīti. 

46
00:06:18,365 --> 00:06:22,125
Vasaras beigās 
sēklaugi uzziedēs. 

47
00:06:38,810 --> 00:06:40,790
Sēklas ievācamas rudenī. 

48
00:06:47,435 --> 00:06:48,785
Lai iegūtu sēklas -

49
00:06:48,950 --> 00:06:54,870
galviņa tiek nogriezta, kad galos parādījušās 
sīkās, baltās, plūksnainās spalviņas.

50
00:07:02,430 --> 00:07:07,440
Galviņa var turpināt gatavināties - 
sausā, labi vēdinātā vietā. 

51
00:07:11,725 --> 00:07:15,040
Ievākšana - šķirošana - uzglabāšana

52
00:07:18,830 --> 00:07:22,795
Kad galviņas izžuvušas, 
ar rokām tiek nolasītas spalviņas.

53
00:07:24,020 --> 00:07:27,060
Izmantojiet cimdus, 
lai sargātos no dzelksnīšiem.

54
00:07:27,860 --> 00:07:31,010
Galviņas saberzē kopā,
lai izkristu sēklas.

55
00:07:41,120 --> 00:07:47,455
Tās var arī ievietot maišelī un izkult 
ar koka vai plastmasas āmurīti - uz mīkstas zemes.

56
00:07:55,100 --> 00:07:58,260
Šķirojot - ar rokām izlasiet lielākās pelavas.

57
00:07:58,345 --> 00:08:02,370
Tad viegli uzpūtiet sēklām, 
lai izvētītu pārējās pelavas

58
00:08:02,955 --> 00:08:05,240
un iegūtu ideāli tīras sēklas. 

59
00:08:15,790 --> 00:08:19,215
Visbeidzot - ieberiet sēklas plastmasas maisiņā.

60
00:08:20,090 --> 00:08:24,300
Pievienojiet etiķeti 
ar sugas un šķirnes nosaukumu,

61
00:08:24,465 --> 00:08:25,910
kā arī ievākšanas gadu. 

62
00:08:33,510 --> 00:08:38,090
Dažas dienas uzglabājot sēklas saldētavā, 
ies bojā parazīti. 

63
00:08:41,260 --> 00:08:45,025
Lapu artišoku sēklu dīgtspēja
 saglabājas vidēji 7 gadus.

64
00:08:47,370 --> 00:08:52,045
Termiņš pagarināms - 
glabājot sēklas saldētavā. 

65
00:08:54,025 --> 00:08:58,920
Vienā gramā ietilpst aptuveni 
25 atsevišķas sēkliņas. 
