1
00:00:09,440 --> 00:00:14,305
Ķirbji, cukīni un kabači piederīgi 
Cucurbitaceae dzimtai.

2
00:00:16,960 --> 00:00:21,260
Vairums ir viengadīgi augi, 
kas iedalāmi vairākās sugās:

3
00:00:26,260 --> 00:00:29,950
Cucurbita pepo sugā 
ietilpst cukīni,

4
00:00:31,305 --> 00:00:32,600
patisoni,

5
00:00:33,950 --> 00:00:35,105
eļļas ķirbji

6
00:00:37,300 --> 00:00:39,500
un vairāki dekoratīvie ķirbji.

7
00:00:43,660 --> 00:00:49,435
Tie lielākoties ir krūmveida - 
ar durstīgām, nereti robainām lapām.

8
00:00:50,525 --> 00:00:52,395
Arī kāti ir durstīgi.

9
00:00:55,480 --> 00:00:56,970
Sēklas ir izliektas -

10
00:00:57,255 --> 00:01:01,775
viendabīgi pelēkbaltā krāsā 
ar baltu maliņu. 

11
00:01:05,160 --> 00:01:12,340
 Cucurbita maxima sugā ietilpst savvaļas ķirbji, 
kas sastopami Argentīnā un Bolīvijā.

12
00:01:13,520 --> 00:01:19,985
Sugai raksturīgas garas, bet trauslas, 
pūkainas stīgas, kas vijas pa zemi. 

13
00:01:23,010 --> 00:01:29,970
Lapas ir lielas un matainas, 
nekad nav izteikti robainas, ieapaļām malām.

14
00:01:32,095 --> 00:01:36,765
Ziedkāts vienmēr noapaļots 
un līdzinās korķim.

15
00:01:38,800 --> 00:01:42,110
Ziedkāts mēdz šķelties.
Tas ir biezāks nekā lapu kāti.

16
00:01:46,120 --> 00:01:50,330
Sēklas nereti klāj 
viegli noņemams apvalks.

17
00:01:51,060 --> 00:01:53,785
Tās ir ovālas, 
parasti - uzblīdušas. 

18
00:01:57,590 --> 00:02:03,160
Cucurbita moshata sugai pieder augi,
 kas vienmēr stīgo pa zemi.

19
00:02:03,360 --> 00:02:06,980
Stīgas ir ļoti garas, matainas 
un vijas līkločiem.

20
00:02:11,755 --> 00:02:14,250
Lapas - balti raibumotas

21
00:02:14,575 --> 00:02:18,790
ar spiciem galiem 
un nedaudz ierobotām malām.

22
00:02:20,265 --> 00:02:24,285
Ziedkāts ir ciets, matains 
un visai stūrains.

23
00:02:25,520 --> 00:02:29,955
Sēklas ir smilšu krāsā, 
iegarenas, ar tumšbēšu maliņu.

24
00:02:32,565 --> 00:02:35,850
Suga attīstās pie augstākas temperatūras 
nekā citi ķirbji.

25
00:02:37,500 --> 00:02:40,615
Aukstā klimatā auglis 
ne vienmēr nogatavosies. 

26
00:02:53,100 --> 00:02:54,085
Apputeksnēšana 

27
00:03:01,335 --> 00:03:03,990
Ķirbju sugas ir vienmājnieces -

28
00:03:04,100 --> 00:03:08,220
vīrišķie un sievišķie ziedi 
atrodas uz viena auga. 

29
00:03:09,170 --> 00:03:12,380
Sievišķo ziedu sēklotne 
atrodas zem ziedlapām.

30
00:03:12,985 --> 00:03:16,330
Tā atgādina sīku ķirbīti, 
kas vēlāk sāks attīstīties.

31
00:03:17,720 --> 00:03:21,220
Vīrišķie ziedi veidojas 
garu ziedkātu galos.

32
00:03:23,940 --> 00:03:26,455
Ziedi atveras tikai vienu dienu. 

33
00:03:32,170 --> 00:03:34,825
Ķirbji ir pašauglīgi -

34
00:03:35,285 --> 00:03:39,660
sievišķos ziedus var apaugļot 
tā paša auga vīrišķā zieda

35
00:03:39,855 --> 00:03:41,270
putekšņi.

36
00:03:46,760 --> 00:03:52,120
Tomēr vienai šķirnei piederīgu, 
atsevišķu augu vidū

37
00:03:52,990 --> 00:03:54,855
izplatītāka ir svešappute.

38
00:04:01,675 --> 00:04:05,765
Ķirbju ziedus apputeksnē kukaiņi, 
parasti - bites. 

39
00:04:14,325 --> 00:04:20,175
Starpsugu krustošanās 
novērojama vienīgi

40
00:04:20,310 --> 00:04:26,520
starp Cucurbita argyrosperma 
un Cucurbita moschata sugām.

41
00:04:28,630 --> 00:04:32,405
Reti - ar savvaļas 
Cucurbita pepo sugu. 

42
00:04:35,915 --> 00:04:40,405
Tādēļ lielākoties divas ķirbju sugas 
iespējams audzēt tiešā tuvumā -

43
00:04:40,830 --> 00:04:43,380
bez krustošanās riska. 

44
00:04:44,260 --> 00:04:45,675
Tomēr svarīgi ievērot

45
00:04:45,855 --> 00:04:51,745
1 km atstatumu starp divām 
(vienai sugai piederīgām) šķirnēm. 

46
00:04:52,825 --> 00:04:58,030
Attālums samazināms līdz 500 m, 
ja pastāv dabiska barjera, piemēram, dzīvžogs. 

47
00:05:03,530 --> 00:05:07,500
Lai iegūtu sēklas no vienā dārzā audzētiem, 
dažādu šķirņu ķirbjiem,

48
00:05:07,580 --> 00:05:08,900
pielietojamas vairākas metodes. 

49
00:05:12,635 --> 00:05:15,765
Pirmā metode: viena šķirne 
pārklājama ar kukaiņu tīklu,

50
00:05:16,145 --> 00:05:18,875
kurā ievietota kameņu ligzda.

51
00:05:25,225 --> 00:05:27,915
Paņēmiens grūti izpildāms 
stīgojošajām šķirnēm,

52
00:05:28,145 --> 00:05:30,570
jo tās strauji aizpilda tīklu,

53
00:05:31,325 --> 00:05:34,060
kavējot kukaiņu 
brīvu pārvietošanos. 

54
00:05:38,095 --> 00:05:42,805
Cita metode: divu šķirņu pārklāšana 
ar atdalītiem tīklu sprostiem.

55
00:05:43,840 --> 00:05:47,405
Viens tīkls tiek atvērts, 
kamēr otrs ir aizvērts -

56
00:05:48,050 --> 00:05:49,975
pamīšus, ik pārdienas.

57
00:05:50,865 --> 00:05:52,955
Apputeksnēšanu veiks 
savvaļas kukaiņi. 

58
00:06:06,385 --> 00:06:09,400
Ziedus var apputeksnēt 
arī pašrocīgi.

59
00:06:11,780 --> 00:06:16,055
Tas ir diezgan viegli - 
ziedi ir lieli un labi saskatāmi.

60
00:06:19,870 --> 00:06:22,735
Visi trīs risinājumi
sīkāk aplūkoti

61
00:06:22,950 --> 00:06:25,875
mehāniskās izolēšanas metodēm
un manuālajai apputeksnēšanai

62
00:06:26,070 --> 00:06:30,290
veltītajos ‘’Sēklu audzēšanas ābeces’’
moduļos.

63
00:06:40,580 --> 00:06:41,580
Dzīves cikls 

64
00:07:06,860 --> 00:07:12,430
Sēklai audzētie ķirbji tiek kultivēti 
tāpat kā pārtikas patēriņam. 

65
00:07:29,810 --> 00:07:33,125
Ģenētiskās daudzveidības nodrošināšanai 
paturiet vismaz 6 augus.

66
00:07:36,310 --> 00:07:38,145
Vēlams - izaudzējiet duci. 

67
00:08:09,330 --> 00:08:12,255
Rūpīgi atlasiet 
sēklai paredzētos augus -

68
00:08:12,325 --> 00:08:15,550
saskaņā ar šķirnes 
specifiskajām īpašībām,

69
00:08:17,700 --> 00:08:20,455
piemēram, stīgu izplatību. 

70
00:08:22,560 --> 00:08:28,785
Pārbaudiet augļa formu un izmēru, 
mīkstuma garšu un struktūru.

71
00:08:29,560 --> 00:08:32,375
Izvēlieties ķirbjus, 
kas veiksmīgi uzglabājami. 

72
00:08:32,640 --> 00:08:35,460
Vērojiet arī noturību pret slimībām. 

73
00:08:36,110 --> 00:08:40,985
Sēklai audzētie ķirbji nogatavojas 
vienlaikus ar pārtikai kultivētajiem,

74
00:08:41,465 --> 00:08:44,255
kas parasti tiek patērēti - 
kad pilnībā nobrieduši.

75
00:08:45,050 --> 00:08:49,295
Izņēmums - dažas
Cucurbita pepo šķirnes,

76
00:08:49,675 --> 00:08:54,510
piemēram, patisoni un cukīni, 
kas tiek ēsti, iekams nogatavojušies. 

77
00:08:56,215 --> 00:09:00,980
Cukīni (tāpat kā ķirbji) nobriedināmi 
līdz krāsas maiņai,

78
00:09:01,150 --> 00:09:05,570
pilnam izmēram, kā arī izkaltušam 
un stingram ziedkātam.

79
00:09:06,170 --> 00:09:08,855
Sēklas iegūstamas,
kad cukīni miza būs sacietējusi. 

80
00:09:13,875 --> 00:09:15,395
Ievāciet augus

81
00:09:16,060 --> 00:09:21,445
un glabājiet tos siltā vietā,
vismaz mēnesi ļaujot turpināt gatavoties.

82
00:09:24,640 --> 00:09:28,270
Tādējādi tiks veicināta 
sēklu auglība. 

83
00:09:33,780 --> 00:09:36,515
Ievākšana - šķirošana - uzglabāšana

84
00:09:42,100 --> 00:09:44,580
Lai ievāktu sēklas, 
pāršķeliet ķirbi

85
00:09:47,530 --> 00:09:50,125
un izgrebiet sēklas ar karoti.

86
00:09:50,375 --> 00:09:52,435
Neizgrebiet pārāk daudz mīkstuma.

87
00:09:57,140 --> 00:09:58,660
Noskalojiet ūdenī.

88
00:10:07,350 --> 00:10:11,765
Dažu šķirņu sēklas 
grūti atdalāmas no mīkstuma.

89
00:10:12,265 --> 00:10:16,430
Izmērcējiet sēklas un mīkstumu 
istabas temperatūras ūdenī -

90
00:10:16,900 --> 00:10:19,130
atdaliet sēklas nākamajā dienā. 

91
00:10:30,405 --> 00:10:36,795
Žāvējiet sēklas temperatūrā no 22- 25°C, 
labi vēdinātā vietā. 

92
00:10:39,675 --> 00:10:42,455
Lai pārliecinātos, vai sēkla izžuvusi,
 ielieciet to.

93
00:10:42,660 --> 00:10:44,975
Ja sēkla lūst - 
tā ir gana sausa. 

94
00:11:02,500 --> 00:11:05,215
Paciņā ievietojiet etiķeti 
ar sugas un šķirnes

95
00:11:05,415 --> 00:11:10,215
nosaukumu, kā arī ievākšanas gadu
(ārējs uzraksts mēdz nodilt). 

96
00:11:16,320 --> 00:11:21,910
Ieteicams dažas dienas sēklas uzglabāt 
saldētavā, lai nonāvētu parazītus. 

97
00:11:27,240 --> 00:11:32,485
Ķirbju un cukīni sēklu dīgtspēja 
saglabājas vidēji 6 gadus,

98
00:11:32,975 --> 00:11:34,930
taču var ieilgt līdz 10 gadiem.

99
00:11:38,510 --> 00:11:42,035
Termiņš pagarināms - 
glabājot sēklas saldētavā. 
