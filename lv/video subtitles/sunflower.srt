1
00:00:10,990 --> 00:00:16,940
Saulespuķes piederīgas Asteraceae dzimtai 
un Helianthus annuus sugai.

2
00:00:20,970 --> 00:00:26,500
Viengadīgais augs tiek kultivēts, lai iegūtu 
ziedkopas vai sēklas, no kurām tiek spiesta eļļa.

3
00:00:29,745 --> 00:00:32,020
Pastāv vairākas saulespuķu šķirnes.

4
00:00:33,925 --> 00:00:37,780
Tikai viena daudzgadīgā šķirne 
attīsta ēdamu bumbuli -

5
00:00:38,020 --> 00:00:41,820
topinambūru 
(Helianthus tuberosus). 

6
00:00:54,895 --> 00:00:55,980
Apputeksnēšana 

7
00:01:05,150 --> 00:01:08,580
Saulespuķes ziedu 
dēvē par galviņu.

8
00:01:09,660 --> 00:01:12,380
Galviņu veido daudzi ziediņi,

9
00:01:13,155 --> 00:01:17,340
kas zied viens pēc otra - 
sākot no ārmalas.

10
00:01:20,960 --> 00:01:23,100
Katrs zieds ir hermafrodīts.

11
00:01:24,240 --> 00:01:26,420
Vispirms parādās vīrišķie orgāni,

12
00:01:31,435 --> 00:01:34,460
kas vienas dienas laikā 
atbrīvo putekšņus.

13
00:01:38,275 --> 00:01:44,060
Zieds mainās, un parādās sievišķais orgāns, 
lai pieņemtu putekšņus.

14
00:01:58,180 --> 00:02:01,300
Ziedus apputeksnē kukaiņi

15
00:02:01,700 --> 00:02:06,540
(parasti - bites un kamenes), izplatot 
ziedputekšņus no viena ziediņa pie otra. 

16
00:02:13,725 --> 00:02:19,180
Saulespuķes parasti apputeksnē viena otru, 
jo vairums šķirņu nav pašapaugļojošas.

17
00:02:25,050 --> 00:02:31,060
Tas nozīmē - viena auga ziediņus 
var apputeksnēt tikai cita auga ziedi.

18
00:02:32,080 --> 00:02:34,940
Apputeksnēšanas veicināšanai 
vienkopus audzējamas

19
00:02:35,190 --> 00:02:36,945
vairākas saulespuķes. 

20
00:02:42,720 --> 00:02:45,900
Dažas šķirnes 
ir pašsavietojamas.

21
00:02:46,510 --> 00:02:51,100
Tas nozīmē - ziediņus var apputeksnēt 
citi ziedi no tās pašas ziedkopas. 

22
00:02:57,290 --> 00:03:00,100
Visas saulespuķu šķirnes krustojas.

23
00:03:04,705 --> 00:03:09,620
Dažos reģionos aug savvaļas saulespuķes, 
kas var apaugļot kultivētos augus.

24
00:03:13,505 --> 00:03:16,020
Tas pats attiecināms uz topinambūru,

25
00:03:16,267 --> 00:03:18,660
kas (bioloģiskā ziņā) 
ir radniecīga suga. 

26
00:03:24,815 --> 00:03:27,700
Lai novērstu šķirņu krustošanos,

27
00:03:28,145 --> 00:03:31,570
divas saulespuķu šķirnes 
audzējamas 1 km atstatumā.

28
00:03:33,980 --> 00:03:37,140
Attālums samazināms līdz 700 m,

29
00:03:37,420 --> 00:03:40,900
ja starp šķirnēm pastāv dabiska barjera, 
piemēram, dzīvžogs. 

30
00:03:46,475 --> 00:03:49,740
Ja dārzs atrodas tuvu 
plašam saulespuķu laukam

31
00:03:50,440 --> 00:03:54,560
vai arī - ja vēlaties šaurā platībā 
audzēt vairākas šķirnes,

32
00:03:54,985 --> 00:03:58,060
ziedus nāksies apputeksnēt manuāli,

33
00:03:58,700 --> 00:04:00,980
lai pasargātu 
katras šķirnes tīrību. 

34
00:04:11,905 --> 00:04:15,100
Manuāla apputeksnēšana 
nav sarežģīta.

35
00:04:25,065 --> 00:04:30,100
Ietiniet katru ziedkopu 
blīvā un ūdensizturīgā papīra maisiņā -

36
00:04:30,435 --> 00:04:32,380
pirms ziedi atplaukuši.

37
00:05:05,845 --> 00:05:10,420
Kad tie jau zied, noņemiet maisiņus 
no diviem augiem, kas atrodas līdzās.

38
00:05:17,190 --> 00:05:20,140
Vienlaikus sekojiet 
bitēm un kamenēm,

39
00:05:20,180 --> 00:05:23,860
kas centīsies apputeksnēt 
neaizklātos ziedus.

40
00:05:28,540 --> 00:05:31,035
Viegli paberzējiet saulespuķu galviņas 
vienu pret otru.

41
00:05:42,975 --> 00:05:45,860
Kad tas paveikts, 
ziedus atkal pārklājiet.

42
00:05:49,235 --> 00:05:55,060
Ziedēšana ilgst 5 - 10 dienas, tādēļ process
minētajā periodā jāatkārto katru dienu.

43
00:06:10,470 --> 00:06:13,860
Atstājiet maisiņu pār ziedu, 
kamēr pienāk laiks ievākt sēklas. 

44
00:06:28,470 --> 00:06:29,820
Dzīves cikls

45
00:06:49,200 --> 00:06:51,500
Sējas sēklai audzētās saulespuķes

46
00:06:51,790 --> 00:06:55,940
kultivējamas tāpat kā pārtikas sēklu 
vai ziedkopu ieguvei. 

47
00:07:19,590 --> 00:07:21,820
Ģenētisko daudzveidību nodrošinās

48
00:07:22,180 --> 00:07:25,620
vismaz 10 
sēklai audzētas saulespuķes.

49
00:07:35,440 --> 00:07:38,180
Rūpīgi atlasiet sēklu ieguves augus -

50
00:07:38,395 --> 00:07:41,940
saskaņā ar šķirnes 
specifiskajiem kritērijiem:

51
00:07:44,130 --> 00:07:45,540
garumu,

52
00:07:47,635 --> 00:07:48,740
ziedkopas izmēru

53
00:07:49,500 --> 00:07:53,900
un krāsu, kā arī sēklu kvalitāti. 

54
00:07:54,635 --> 00:07:59,020
Sēklas attīstās pakāpeniski, 
sākot no ziedkopas ārmalas,

55
00:07:59,260 --> 00:08:01,140
tad - arvien tuvāk centram.

56
00:08:05,940 --> 00:08:08,780
Ievāciet saulespuķes, 
kad ziedkopa pilna sēklām

57
00:08:09,145 --> 00:08:11,060
un ziedlapas sākušas birt.

58
00:08:14,370 --> 00:08:16,535
Putni iecienījuši saulespuķu sēklas,

59
00:08:16,810 --> 00:08:22,185
tādēļ negaidiet, kamēr augs pilnībā izkaltīs, 
lai to nogrieztu, citādi sēklas jau būs nozudušas! 

60
00:08:48,145 --> 00:08:52,390
Saberzējiet ziedkopas, lai novāktu 
izkaltušos ziediņus - tie nobirs zemē.

61
00:09:09,550 --> 00:09:13,940
Apgrieziet arī ārējās ziedlapas, 
lai ziedkopas veiksmīgāk izžūtu.

62
00:09:20,710 --> 00:09:23,020
Novietojiet ziedkopas sausā, 
labi vēdinātā vietā -

63
00:09:23,295 --> 00:09:27,340
ar sēklām uz augšu, lai novērstu 
pelējumu vai fermentāciju. 

64
00:09:42,410 --> 00:09:44,980
Ievākšana - šķirošana - uzglabāšana

65
00:09:51,425 --> 00:09:54,420
Saberzējot ziedkopas - 
izbirs sēklas.

66
00:10:03,865 --> 00:10:08,660
Varat novietot ziedkopas uz metāla režģa 
pāri spainim - un tad tās saberzēt. 

67
00:10:15,080 --> 00:10:19,420
Atstājiet sēklas žāvēties 
sausā un labi vēdinātā vietā. 

68
00:10:25,585 --> 00:10:29,590
Lai pārliecinātos, vai sēkla izžuvusi, 
salieciet to.

69
00:10:30,155 --> 00:10:33,260
Ja tā lūst - 
sēkla gatava uzglabāšanai. 

70
00:10:36,650 --> 00:10:40,405
Visbeidzot - vētījiet sēklas, 
lai atbrīvotos no sīkām pelavām.

71
00:10:41,240 --> 00:10:46,420
Ieberiet sēklas šķīvī vai vētījamā grozā 
un pūtiet pār tām elpu.

72
00:10:47,440 --> 00:10:49,305
Tiks izvētīti smalkākie atkritumi. 

73
00:11:03,545 --> 00:11:05,310
Ieberiet sēklas paciņā,

74
00:11:05,420 --> 00:11:11,380
kurā ievietota etiķete ar sugas un šķirnes 
nosaukumu, kā arī ievākšanas gadu. 

75
00:11:15,440 --> 00:11:19,700
Dažas dienas uzglabājiet sēklas saldētavā, 
lai nonāvētu parazītus. 

76
00:11:25,280 --> 00:11:29,220
Saulespuķu sēklas 
dīgtspēju saglabā vidēji 7 gadus.

77
00:11:32,040 --> 00:11:35,340
Termiņš pagarināms - 
glabājot sēklas saldētavā. 
