﻿1
00:00:09,980 --> 00:00:13,163
Cigoriņi piederīgi Asteraceae dzimtai.

2
00:00:13,950 --> 00:00:20,261
Divgadīgajam augam izšķir divas sugas:
Cichorium endivia

3
00:00:21,163 --> 00:00:23,330
un Cichorium intybus.

4
00:00:28,021 --> 00:00:33,505
- Cichorium endivia iedalāma divās galvenajās 
apakšsugās, kas audzētas lapu ieguvei: 

5
00:00:37,636 --> 00:00:40,327
- krokainā endīvija (var. crispum)

6
00:00:41,185 --> 00:00:44,960
- un platlapu (var. latifolium)
endīvija. 

7
00:00:49,280 --> 00:00:53,454
Otrā sugā (Cichorium intybus) ietilpst:

8
00:00:53,818 --> 00:01:01,374
- rūgtie cigoriņi (var. foliosum) ar lielām, 
sarkanām, raibām vai zaļām lapām,

9
00:01:02,705 --> 00:01:07,090
- sativum tipa cigoriņi, 
no kuriem iegūst beļģu endīviju, 

10
00:01:08,443 --> 00:01:12,203
- kā arī šķirnes - 
sakņu cukura ražošanai,

11
00:01:12,465 --> 00:01:15,352
vai grauzdējamu sakņu ieguvei 
(kafijas aizstājēji). 

12
00:01:18,807 --> 00:01:22,945
Intybus sugā ietilpst arī 
Eiropas savvaļas cigoriņi. 

13
00:01:32,640 --> 00:01:33,730
Apputeksnēšana 

14
00:01:49,214 --> 00:01:54,618
Cigoriņu ziedkopa sastāv no ziediņiem, 
kuru krāsa variējas

15
00:01:54,792 --> 00:02:00,450
no zilas līdz zilvioletai, 
un ziedkopu dēvē par galviņu.

16
00:02:01,403 --> 00:02:04,530
Abu sugu ziedi ir hermafrodīti. 

17
00:02:06,647 --> 00:02:12,909
Cichorium endivia ziedi ir pašapaugļojoši, 
taču lielākoties - alogāmi.

18
00:02:13,410 --> 00:02:17,105
Tādēļ ziedu apputeksnēšanai 
nav nepieciešami kukaiņi. 

19
00:02:17,432 --> 00:02:21,480
Cichorium intybus ziedi 
ir pašsterili -

20
00:02:21,941 --> 00:02:24,618
tiem vajadzīgi apputeksnētāji-kukaiņi. 

21
00:02:26,203 --> 00:02:30,600
Šķirnes mēdz krustoties 
vienas sugas ietvaros -

22
00:02:30,858 --> 00:02:33,054
kukaiņu klātbūtnes ietekmē.

23
00:02:35,221 --> 00:02:38,974
Sugu īpatnību rezultātā -

24
00:02:39,301 --> 00:02:43,280
Cichorium endivia var apputeksnēt 
Cichorium intybus,

25
00:02:43,738 --> 00:02:45,900
bet ne pretēji. 

26
00:02:55,080 --> 00:02:56,810
Lai novērstu krustošanos -

27
00:02:56,996 --> 00:03:01,709
divas cigoriņu šķirnes a
udzējamas vismaz 500 m atstatumā. 

28
00:03:03,447 --> 00:03:06,741
Attālums samazināms līdz 250 m,

29
00:03:07,010 --> 00:03:11,214
ja pastāv dabiska barjera 
(piemēram, dzīvžogs). 

30
00:03:13,207 --> 00:03:18,320
Lai nenotiktu krustošanās, 
Cichorium endivia šķirnes iespējams audzēt

31
00:03:18,640 --> 00:03:22,101
zem slēgtiem kukaiņu tīkliem, 
jo tās ir pašapaugļojošas. 

32
00:03:24,894 --> 00:03:26,560
Cichorium intybus

33
00:03:26,814 --> 00:03:30,749
pieprasa pamīšus atveramu un aizveramu 
tīklu sistēmu,

34
00:03:31,105 --> 00:03:35,498
kā norādīts ‘’Sēklu ražošanas ābeces’’ 
izolēšanas metodēm veltītajā modulī.

35
00:03:39,178 --> 00:03:42,960
Svarīgi pārliecināties, 
lai tuvumā neaugtu

36
00:03:43,054 --> 00:03:47,229
rūgtā vai krokainā endīvija, 
kā arī savvaļas cigoriņi.

37
00:03:47,934 --> 00:03:50,560
Tie var krustoties 
ar sēklas augiem. 

38
00:03:55,200 --> 00:03:56,540
Dzīves cikls 

39
00:04:14,567 --> 00:04:16,901
Cigoriņš ir divgadīgs augs.

40
00:04:19,180 --> 00:04:21,992
Sēklas attīstās tikai otrajā gadā. 

41
00:04:37,520 --> 00:04:39,130
Pirmajā cikla gadā -

42
00:04:39,265 --> 00:04:44,356
sēklai paredzētie augi tiek kultivēti 
tāpat kā pārtikas ieguvei. 

43
00:05:05,963 --> 00:05:11,380
Rūpīgi atlasiet augus - 
saskaņā ar šķirnes kritērijiem:

44
00:05:11,970 --> 00:05:16,260
forma, izmērs, lapu krāsa

45
00:05:18,480 --> 00:05:19,927
un labi veidotas galviņas. 

46
00:05:27,440 --> 00:05:28,705
Beļģu endīvijām

47
00:05:28,780 --> 00:05:33,192
svarīgi izraudzīties labi attīstītas saknes, 
kā arī labi veidotas un ciešas galviņas. 

48
00:05:39,047 --> 00:05:41,790
Cigoriņi, kas sēklas saražojuši pirmajā gadā,

49
00:05:42,036 --> 00:05:46,036
nav īstenojuši dabisko attīstības ciklu 
un ir atmetami.

50
00:05:47,600 --> 00:05:51,316
Turpmākos gados tādi augi 
mēdz briedināt sēklas arvien agrāk,

51
00:05:51,490 --> 00:05:54,952
un tā nav vēlama lapu dārzeņu iezīme. 

52
00:06:00,916 --> 00:06:06,283
Veiksmīgu ģenētisko daudzveidību 
nodrošinās 10 - 15 augi. 

53
00:06:19,978 --> 00:06:24,814
Rudenī - lapas ievācamas pārtikai, 
bet saknes saglabājamas sēklai. 

54
00:06:33,323 --> 00:06:38,720
Maigāka klimata reģionos - 
cigoriņu saknes var pārziemot augsnē. 

55
00:06:43,105 --> 00:06:45,323
Skarbāka klimata vidē -

56
00:06:45,563 --> 00:06:48,850
sēklai paredzētie augi 
ieziemojami vairākos veidos. 

57
00:06:53,760 --> 00:06:56,140
Pirms iestājusies ziema - 
saknes tiek izraktas,

58
00:06:57,883 --> 00:07:00,700
apgrieztas dažus centimetrus 
virs kakliņa

59
00:07:14,000 --> 00:07:18,363
un uzglabātas mitrās smiltīs 
vai pagrabā, podiņos. 

60
00:07:20,480 --> 00:07:28,254
Ideālais mitruma līmenis: 80%. 
Temperatūra: 0° - 4°C. 

61
00:07:30,094 --> 00:07:36,880
Cigoriņus var sēt arī vēlu rudenī - 
aukstā siltumnīcā vai dārzā (maigā klimatā).

62
00:07:38,290 --> 00:07:42,203
Ja līdz ziemai izveidojušās 
nelielas rozetes -

63
00:07:42,520 --> 00:07:45,149
cigoriņi veiksmīgāk pārcietīs salu. 

64
00:07:46,887 --> 00:07:50,341
Pavasarī augs attīstīs galviņu, 
bet uzziedēs vasarā. 

65
00:07:55,374 --> 00:08:00,210
Pavasarī - pagrabā glabātās saknes 
tiek pārstādītas dārzā.

66
00:08:01,020 --> 00:08:03,323
Iepuvušās saknes tiek aizvāktas.

67
00:08:21,290 --> 00:08:24,101
Pēc pārstādīšanas - dāsni laistīt. 

68
00:08:52,610 --> 00:08:58,640
Pamazām augi uzziedēs, veidojot ļoti garus 
ziedkātus, kas balstāmi ar mietiņiem. 

69
00:09:25,480 --> 00:09:30,967
Cigoriņu sēklas ievācamas -
tām nobriestot (periods var ieilgt).

70
00:09:38,036 --> 00:09:41,854
Negaidiet, kamēr augs 
pilnībā izkaltīs,

71
00:09:42,020 --> 00:09:45,243
jo vairums sēklu 
jau būs izbirušas. 

72
00:09:54,840 --> 00:09:57,876
Iespējams ievākt visu galviņu.

73
00:09:58,760 --> 00:10:02,400
Sēklas turpinās gatavoties 
uz novāktā auga. 

74
00:10:05,825 --> 00:10:10,981
Sēklu žāvēšana turpināma 
sausā, labi vēdinātā vietā. 

75
00:10:18,580 --> 00:10:22,290
Ievākšana - šķirošana - uzglabāšana

76
00:10:28,101 --> 00:10:31,680
Ievācot sēklas, 
augiem jābūt pilnībā izžuvušiem.

77
00:10:32,414 --> 00:10:36,080
Cigoriņu sēklas nereti grūti izlobīt 
no smalkās pāksts,

78
00:10:36,465 --> 00:10:39,287
bet citkārt tās izbirst - bez liekas piepūles. 

79
00:10:44,298 --> 00:10:47,380
Sausās pākstis 
tiek intensīvi berzētas plaukstās

80
00:10:47,570 --> 00:10:48,792
vai izkultas ar nūju. 

81
00:10:52,458 --> 00:10:54,341
Ja sēklas neizbirst,

82
00:10:54,509 --> 00:11:00,545
iespējams lietot mehāniskas metodes: 
mīklas rulli, traktora pārbraucienu vai āmuru. 

83
00:11:04,843 --> 00:11:09,680
Pākstis, kas neatveras, 
apstrādājamas arī sekojošā veidā...

84
00:11:11,320 --> 00:11:15,220
Dažas sekundes - sēklas tiek maltas 
kafijas vai dārzeņu blenderī.

85
00:11:15,469 --> 00:11:17,060
Rezultāts ir lielisks,

86
00:11:19,700 --> 00:11:24,247
taču jāseko blenderēšanas ilgumam, 
citādi sēklas pārvērtīsies pulverī. 

87
00:11:30,430 --> 00:11:37,294
Sēklu šķirošanai, kā arī zariņu, stiebru un putekļu 
atsijāšanai izmanto dažāda izmēra sietiņus. 

88
00:11:43,949 --> 00:11:47,920
Visbeidzot - sēklas tiek vētītas, 
lai atbrīvotos no pārpalikušām kripatām.

89
00:11:49,243 --> 00:11:54,116
Viegli pūtiet pār sēklām - 
uz šķīvja vai vētījamā grozā. 

90
00:11:57,520 --> 00:12:00,414
Sausās sēklas ieber 
ūdensnecaurlaidīgā maisiņā.

91
00:12:01,876 --> 00:12:05,105
Paciņā ievieto etiķeti 
ar sugas un šķirnes nosaukumu,

92
00:12:05,294 --> 00:12:08,443
kā arī ievākšanas gadu. 

93
00:12:13,112 --> 00:12:18,225
Dažas dienas uzglabājot sēklas saldētavā, 
ies bojā parazīti. 

94
00:12:19,381 --> 00:12:23,490
Cigoriņu sēklu dīgtspēja saglabājas 
vidēji vismaz 5 gadus.

95
00:12:23,980 --> 00:12:28,123
Termiņš pagarināms - 
glabājot sēklas saldētavā. 
