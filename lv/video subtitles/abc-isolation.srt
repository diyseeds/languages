﻿1
00:00:08,650 --> 00:00:10,380
Izolēšanas metodes

2
00:00:17,140 --> 00:00:19,620
Svešapputes riska apstākļos -

3
00:00:19,685 --> 00:00:22,620
lai nekrustotos vienas sugas
dažādu šķirņu augi -

4
00:00:23,420 --> 00:00:25,700
šķirnes izolējamas,

5
00:00:25,850 --> 00:00:30,100
lai saglabātu katras sugas 
specifiskās īpašības.

6
00:00:30,690 --> 00:00:33,750
Šķirņu izolēšanai 
piemērojamas vairākas metodes.

7
00:00:39,485 --> 00:00:41,180
Telpiskā izolācija 

8
00:00:45,240 --> 00:00:51,820
Vienkāršākā metode - 
platībā kultivēt tikai vienu sugas šķirni.

9
00:00:52,820 --> 00:00:56,300
Dažādas šķirnes audzējamas 
gana attālu,

10
00:00:56,490 --> 00:01:00,955
lai novērstu starpšķirņu 
krustošanās risku.

11
00:01:02,045 --> 00:01:05,230
Katram dārzenim norādīts ieteicamais atstatums.

12
00:01:06,670 --> 00:01:11,100
Attālums atkarīgs arī no vides faktoriem 
un apputeksnēšanas veida.

13
00:01:13,750 --> 00:01:18,580
Ja apputeksnēšanu veic kukaiņi, 
pārsimt metru būs pietiekams atstatums. 

14
00:01:20,780 --> 00:01:24,020
Ja apputeksnēšanu veic vējš,

15
00:01:24,500 --> 00:01:29,020
starp šķirnēm ievērotajam attālumam 
jābūt lielākam. 

16
00:01:29,325 --> 00:01:33,420
Biezs dzīvžogs 
var būtiski mazināt

17
00:01:33,780 --> 00:01:36,660
vēja un kukaiņu nesto putekšņu izplatību. 

18
00:01:37,820 --> 00:01:43,020
Ja sēklu ieguves augu tuvumā 
būs daudz ziedaugu,

19
00:01:43,140 --> 00:01:48,540
ziedi piesaistīs kukaiņus, 
novēršot to uzmanību no sēklas augu ziediem. 

20
00:01:59,600 --> 00:02:01,140
Temporālā izolācija 

21
00:02:01,880 --> 00:02:07,940
Viena dārzeņa dažādu šķirņu 
kultivēšanas posmus iespējams ''nobīdīt''.

22
00:02:08,770 --> 00:02:11,660
Ja augi nezied vienlaikus -

23
00:02:11,940 --> 00:02:14,220
nav iespējama arī svešappute.

24
00:02:20,090 --> 00:02:25,300
Piemēram, divas salātu šķirnes 
var audzēt vienā dārza placītī.

25
00:02:25,880 --> 00:02:31,725
Agrīnie (agrāk iesētie) salāti uzziedēs - 
kamēr vēlīnākā šķirne tikai sāks attīstību.

26
00:02:32,940 --> 00:02:38,915
Protams, svarīgi iepazīt augu ciklus 
un noskaidrot to ziedēšanas laiku. 

27
00:02:41,975 --> 00:02:43,820
Mehāniskā izolācija 

28
00:02:45,550 --> 00:02:50,960
Kukaiņu tīkli ir nenovērtējami palīgi 
drošai sēklu ražošanai.

29
00:02:52,560 --> 00:02:58,220
Tīkla smalkums saskaņojams 
ar kukaiņu veidu un izolēšanas stratēģiju:

30
00:02:59,045 --> 00:03:04,620
vai tīkls paredzēts kukaiņu atvairīšanai 
vai paturēšanai auga tuvumā.

31
00:03:07,320 --> 00:03:09,975
Tīkliem ir daudzējāds pielietojums. 

32
00:03:11,670 --> 00:03:14,035
Atsevišķa zieda pārklāšana

33
00:03:14,390 --> 00:03:18,340
paredzēta hermafrodītiem 
un pašapputes ziediem -

34
00:03:18,795 --> 00:03:21,230
neliela sēklu apjoma ieguvei.

35
00:03:22,295 --> 00:03:26,700
Neatvērušos ziedu 
pārklāj ar smalku tīklu,

36
00:03:26,910 --> 00:03:30,340
kas ir aizsiets, lai ziedam nepiekļūtu kukaiņi.

37
00:03:34,585 --> 00:03:37,700
Kad zieds novītis, 
tīkla sprosts tiek noņemts,

38
00:03:37,830 --> 00:03:40,590
lai dārzenis attīstītos netraucēti.

39
00:03:42,075 --> 00:03:46,050
Neaizmirstiet iezīmēt augli, 
kas tiek glabāts sēklu ieguvei. 

40
00:03:55,890 --> 00:04:02,585
Iespējams izmantot arī lielākus tīklapvalkus, 
kas ietver zaru vai visu augu kopumā.

41
00:04:04,800 --> 00:04:09,260
Vispirms epieciešams 
nolasīt jau atvērušos ziedus,

42
00:04:09,730 --> 00:04:14,060
jo pastāv risks - 
kukaiņi tos paguvuši apputeksnēt. 

43
00:04:19,900 --> 00:04:22,780
Lielāku sēklu apjomu ļaus iegūt

44
00:04:22,860 --> 00:04:26,380
veiksmīgi izveidota 
tīklu sprostu un tuneļu sistēma. 

45
00:05:29,540 --> 00:05:32,180
Vienā tīkla sprosta sistēmā sagrupējami

46
00:05:32,320 --> 00:05:35,940
hermafrodītie un pašapaugļojošie augi,

47
00:05:36,060 --> 00:05:37,620
lai sargātu tos no kukaiņiem. 

48
00:05:40,540 --> 00:05:45,700
Alogāmiem augiem 
nepieciešama apputeksnētāju klātbūtne,

49
00:05:46,340 --> 00:05:51,820
tādēļ tunelī tiek ievietota kameņu ligzda,
lai kamenes augus apputeksnētu.

50
00:05:54,610 --> 00:05:58,620
Šādā veidā iespējams audzēt 
tikai vienu sugas šķirni.

51
00:05:59,085 --> 00:06:02,020
Lai ligzdu ievietošana būtu izdevīga,

52
00:06:02,230 --> 00:06:06,420
ieteicams veidot plašu tuneli
ar daudziem sēklas augiem. 

53
00:06:08,050 --> 00:06:10,540
Pamīšus tīklsprostu sistēmas

54
00:06:10,780 --> 00:06:14,740
piemērojamas vienas sugas,
bet dažādu šķirņu alogāmo augu

55
00:06:14,900 --> 00:06:18,980
sēklu audzēšanai - 
viena dārza ietvaros.

56
00:06:19,940 --> 00:06:23,220
Piemērs - 
divas dažādas cukīni šķirnes.

57
00:06:24,120 --> 00:06:25,900
Ja netiek izmantota kameņu ligzda,

58
00:06:26,350 --> 00:06:30,980
viena sugas šķirne audzējama 
zem viena tīkla tuneļa,

59
00:06:31,580 --> 00:06:36,520
bet otra (tās pašas sugas šķirne) - 
citā tīkla tunelī.

60
00:06:38,570 --> 00:06:40,060
Ziediem atplaukstot -

61
00:06:40,720 --> 00:06:43,780
tuneļi atverami pamīšus, ik pārdienas.

62
00:06:44,880 --> 00:06:51,610
Pirmajā dienā tiek atvērts pirmais tunelis, 
bet otrais tunelis paliek noslēgts.

63
00:06:52,810 --> 00:06:55,335
Nākamajā dienā - pretēji.

64
00:06:56,990 --> 00:06:59,800
Tuneļi atverami 
mazā rīta gaismiņā,

65
00:07:00,190 --> 00:07:02,860
jo vasarās kukaiņu aktivitātes 
iesākas agri.

66
00:07:03,225 --> 00:07:07,380
Tuneļi aizverami vēlu, 
kad augos vairs nav dzirdama sanēšana.

67
00:07:08,575 --> 00:07:09,740
Metode ļauj

68
00:07:10,030 --> 00:07:12,835
apputeksnēt mazāku 
sievišķo ziedu skaitu

69
00:07:13,030 --> 00:07:15,805
un iegūt mazāk augļu.

70
00:07:17,140 --> 00:07:19,900
Tomēr daba ir dāsna -

71
00:07:20,215 --> 00:07:23,980
tiks iegūti pietiekami sēklu krājumi
turpmākajiem sējas gadiem. 
