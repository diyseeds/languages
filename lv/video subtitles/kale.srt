﻿1
00:00:08,654 --> 00:00:11,840
Lapu kāposti piederīgi Brassicaceae dzimtai,

2
00:00:12,480 --> 00:00:20,072
Brassica oleracea sugai 
un acephala apakšsugai.

3
00:00:20,760 --> 00:00:26,894
Brassica oleracea sugā 
ietilpst arī kolrābji, brokoļi,

4
00:00:27,665 --> 00:00:33,454
kāposti, Briseles kāposti, 
puķkāposti un virziņkāposti. 

5
00:00:36,850 --> 00:00:40,220
Lapu kāposti ir kāpostu tips, 
kas neveido galviņu,

6
00:00:41,440 --> 00:00:45,796
bet gan centrālo kātu, 
gar kuru attīstās lapas.

7
00:00:49,160 --> 00:00:51,774
Dažām šķirnēm raksturīgi vairāki kāti. 

8
00:00:58,240 --> 00:01:00,552
Augs ir rudens un ziemas dārzenis.

9
00:01:01,840 --> 00:01:04,690
Vairums šķirņu ir salcietīgas.

10
00:01:05,156 --> 00:01:09,760
Sals maina lapu garšu - 
cietēm pārvēršoties cukuros.

11
00:01:12,385 --> 00:01:16,567
Tomēr ilgstoši sala periodi 
var nonāvēt augus. 

12
00:01:20,363 --> 00:01:24,940
Lapu salātiem ir daudz paveidu, 
kam raksturīgs atšķirīgs garums

13
00:01:25,287 --> 00:01:28,923
(no 40 cm - 1,5 m),

14
00:01:29,505 --> 00:01:32,930
gluda vai krokota lapu struktūra,

15
00:01:33,949 --> 00:01:40,530
kā arī atšķirīga krāsa (no gaišzaļas 
vai dzeltenzaļas - līdz gaiši vai tumši violetai).

16
00:01:47,461 --> 00:01:50,340
Lapu kāpostus izmanto 
sīklopu barībai,

17
00:01:55,287 --> 00:01:57,410
un arī pārtikas patēriņam.

18
00:02:00,420 --> 00:02:05,861
No tiem ražo amatniecības preces - 
spieķus vai pat jumtu sijas. 

19
00:02:12,080 --> 00:02:16,654
Oleracea kāpostu
apputeksnēšana 

20
00:02:32,640 --> 00:02:36,581
Brassica oleracea ziedi 
ir hermafrodīti.

21
00:02:39,549 --> 00:02:43,825
Tas nozīmē - tie apveltīti gan ar vīrišķajiem, 
gan sievišķajiem orgāniem.

22
00:02:47,505 --> 00:02:49,980
Vairums ir pašsterili -

23
00:02:50,625 --> 00:02:55,825
viena auga ziedi spēj apputeksnēt 
tikai citu augu.

24
00:02:59,469 --> 00:03:01,723
Tādi augi ir alogāmi augi.

25
00:03:02,589 --> 00:03:07,425
Veiksmīgai apputeksnēšanai 
ieteicams kultivēt vairākus augus. 

26
00:03:11,010 --> 00:03:14,080
Ziedputekšņus izplata kukaiņi.

27
00:03:17,418 --> 00:03:21,956
Tādējādi tiek dabiski veicināta 
lieliska ģenētiskā daudzveidība.

28
00:03:28,829 --> 00:03:33,163
Visas Brassica oleracea
kāpostu apakšsugas

29
00:03:33,323 --> 00:03:34,858
var savstarpēji krustoties.

30
00:03:36,680 --> 00:03:41,403
Tādēļ dažādu šķirņu sēklas kāpostus 
nevajadzētu audzēt tiešā tuvumā. 

31
00:03:45,890 --> 00:03:47,338
Šķirnes tīrībai -

32
00:03:47,658 --> 00:03:55,090
dažādas Brassica oleracea šķirnes 
stādāmas vismaz 1 km atstatumā. 

33
00:03:56,450 --> 00:04:00,850
Attālums samazināms līdz 500 metriem, 
ja starp šķirnēm atrodas dabiska barjera

34
00:04:00,938 --> 00:04:03,876
(piemēram, dzīvžogs). 

35
00:04:07,069 --> 00:04:09,600
Šķirnes iespējams izolēt -

36
00:04:09,876 --> 00:04:14,654
norobežotos kukaiņu tīklos
ievietojot nelielas kukaiņu ligzdiņas,

37
00:04:19,890 --> 00:04:23,687
vai arī tīklus pamīšus 
atverot un aizverot.

38
00:04:27,920 --> 00:04:29,141
Sīkāk -

39
00:04:29,330 --> 00:04:33,425
izolēšanas metodēm veltītajā
‘’Sēklu ražošanas ābeces’’ modulī. 

40
00:04:45,580 --> 00:04:55,485
Lapu kāpostu dzīves cikls 

41
00:05:03,134 --> 00:05:09,163
Lapu kāposts ir divgadīgs augs, 
kas pirmajā gadā attīsta kātus ar lapām,

42
00:05:09,890 --> 00:05:14,058
bet nākamajā pavasarī - ziedus
(sēklas tiek ražotas vasarā). 

43
00:05:28,989 --> 00:05:34,152
Sēklai audzētos lapu kāpostus 
kultivē tāpat kā pārtikas patēriņam.

44
00:05:38,130 --> 00:05:43,381
Ģenētisko daudzveidību 
nodrošinās 10 - 15 augi. 

45
00:05:59,760 --> 00:06:02,356
Sēklu ievākšanai jāizraugās veselīgi augi,

46
00:06:02,440 --> 00:06:05,440
kas novēroti visos 
attīstības posmos,

47
00:06:05,658 --> 00:06:08,603
lai sekotu šķirnes īpašībām:

48
00:06:09,410 --> 00:06:17,490
izmēram, krāsai, dzīvelīgumam, 
agrīnībai, noturībai pret slimībām,

49
00:06:19,890 --> 00:06:25,236
lapu izplatībai visa kāta garumā, 
aukstumizturībai. 

50
00:06:28,516 --> 00:06:30,872
Lapu kāposts var pārziemot augsnē.

51
00:06:53,614 --> 00:06:58,727
Otrā gada pavasarī augs uzziedēs 
un vasarā ražos sēklas. 

52
00:07:16,065 --> 00:07:22,780
Brassica oleracea kāpostu
ievākšana, šķirošana un uzglabāšana

53
00:07:31,905 --> 00:07:35,643
Sēklas ir gatavas - 
pākstīm vēršoties smilškrāsā.

54
00:07:39,912 --> 00:07:42,458
Pākstis viegli pārsprāgst.

55
00:07:42,632 --> 00:07:47,665
Tas nozīmē - nobriedušas pākstis 
viegli atveras un strauji izplata sēklas. 

56
00:07:56,232 --> 00:08:00,240
Lielākoties stiebri 
nenobriest vienlaicīgi.

57
00:08:01,294 --> 00:08:07,381
Lai nezaudētu sēklas, tās ievācamas 
atsevišķi - no katra nobriedušā stiebra.

58
00:08:09,083 --> 00:08:14,807
Iekams nogatavojušās visas sēklas - 
var ievākt arī augu kopumā. 

59
00:08:18,080 --> 00:08:25,098
Briedināšana turpināma - 
žāvējot augu sausā, labi vēdinātā vietā. 

60
00:08:37,694 --> 00:08:43,890
Kāpostu sēklas ievācamas, 
kad pākstis viegli atveramas ar pirkstiem. 

61
00:08:45,840 --> 00:08:47,090
Lai ievāktu sēklas -

62
00:08:47,240 --> 00:08:52,618
pākstis tiek izbērtas uz plastmasas plēves 
vai bieza auduma gabala.

63
00:08:53,010 --> 00:08:55,832
Tad pākstis izkuļ
vai saberzē plaukstās.

64
00:08:59,345 --> 00:09:04,363
Pākstis var arī ievietot maisiņā - 
un izkult pret mīkstu virsmu. 

65
00:09:05,767 --> 00:09:10,618
Lielāki apjomi kuļami - 
mīdot vai pārbraucot pāri. 

66
00:09:23,040 --> 00:09:28,370
Ja pākstis neatveras viegli - 
tajās slēpjas nenobriedušas sēklas,

67
00:09:28,560 --> 00:09:30,167
kam būs raksturīga vāja dīgtspēja. 

68
00:09:35,010 --> 00:09:36,240
Šķirošanas gaitā -

69
00:09:36,574 --> 00:09:41,207
sēklas tiek sijātas 
rupjākā sietā,

70
00:09:41,512 --> 00:09:43,047
kas aiztur pelavas.

71
00:09:47,941 --> 00:09:50,654
Tad sēklas tiek sijātas citā sietā,

72
00:09:50,938 --> 00:09:55,500
kas aiztur sēklas, 
bet atsijā smalkākās daļiņas. 

73
00:09:59,272 --> 00:10:03,025
Pēdīgi - sēklas vētījamas, 
pūšot pār tām

74
00:10:03,454 --> 00:10:07,556
vai izmantojot vēja spēku, 
lai aizvāktu pelavu paliekas. 

75
00:10:22,676 --> 00:10:27,610
Visas Brassica oleracea kāpostu sēklas 
ir savstarpēji līdzīgas.

76
00:10:28,647 --> 00:10:35,360
Grūti atšķirt, piemēram, 
kāpostu un puķkāpostu sēklas.

77
00:10:35,847 --> 00:10:40,836
Tādēļ svarīgi augus iezīmēt 
un iegūtajām sēklām

78
00:10:41,040 --> 00:10:46,109
pievienot etiķeti ar sugas un šķirnes 
nosaukumu, kā arī ievākšanas gadu. 

79
00:10:47,440 --> 00:10:52,240
Dažas dienas uzglabājot sēklas saldētavā, 
tiks iznīcināti parazīti. 

80
00:10:57,716 --> 00:11:00,960
Kāpostu sēklu dīgtspēja 
saglabājas 5 gadus.

81
00:11:01,934 --> 00:11:05,592
Taču dīgšanas kapacitāte 
var ieilgt līdz pat 10 gadiem.

82
00:11:07,310 --> 00:11:10,669
Termiņš pagarināms - 
glabājot sēklas saldētavā.

83
00:11:11,694 --> 00:11:17,905
Vienā gramā ietilpst 250 - 300 sēklas 
(atbilstoši šķirnes īpašībām). 
