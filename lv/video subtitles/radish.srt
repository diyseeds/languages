1
00:00:11,480 --> 00:00:15,290
Redīsi piederīgi 
Brassicaceae dzimtai

2
00:00:15,527 --> 00:00:17,992
un Raphanus sativus sugai.

3
00:00:19,301 --> 00:00:23,694
Pastāv mazie redīsi 
(Raphanus sativus sativus)

4
00:00:24,152 --> 00:00:28,138
un lielie redīsi 
(Raphanus sativus niger)

5
00:00:32,501 --> 00:00:36,116
ar vasaras, rudens 
un ziemas šķirnēm. 

6
00:00:37,190 --> 00:00:40,865
Mazie redīsi parasti ir 
balti, sārti vai sarkani.

7
00:00:41,803 --> 00:00:45,003
Pastāv arī dzeltenas, 
pelēkas, violetas

8
00:00:45,156 --> 00:00:49,069
un melnas šķirnes 
ar vairāk vai mazāk iegarenām saknēm.

9
00:00:50,720 --> 00:00:56,298
Lielie redīsi ir balti, sārti, violeti 
vai melni ar apaļām

10
00:00:56,410 --> 00:00:59,425
vai iegarenām, 
dažāda garuma saknēm. 

11
00:01:09,229 --> 00:01:10,363
Apputeksnēšana 

12
00:01:18,894 --> 00:01:26,327
Redīsu ziedi ir hermafrodīti - tiem ir gan vīrišķie, 
gan sievišķie vairošanās orgāni.

13
00:01:30,276 --> 00:01:32,778
Lielākā daļa ir pašsterili -

14
00:01:34,996 --> 00:01:39,803
viena auga ziedputekšņi 
var apaugļot tikai citu augu.

15
00:01:40,887 --> 00:01:45,570
Veiksmīgas apputeksnēšanas nolūkos 
audzējami vairāki augi. 

16
00:01:47,621 --> 00:01:50,145
Tādēļ redīsi ir alogāmi augi.

17
00:01:50,385 --> 00:01:53,221
Ziedputekšņus izplata kukaiņi.

18
00:01:57,905 --> 00:02:00,116
Redīsu ziedi ir balti vai violeti.

19
00:02:00,400 --> 00:02:03,163
Ziedi ražo nektāru 
un pievilina bites. 

20
00:02:09,614 --> 00:02:13,207
Visas redīsu šķirnes 
spēj savstarpēji krustoties.

21
00:02:13,469 --> 00:02:17,818
Krusoties var pat abas apakšsugas: 
sativus un niger. 

22
00:02:19,643 --> 00:02:22,072
Lai saglabātu šķirnes tīrību,

23
00:02:22,312 --> 00:02:26,727
divas šķirnes audzējamas 
vismaz 1 km atstatumā. 

24
00:02:27,403 --> 00:02:32,080
Attālums samazināms līdz 500 m, 
ja starp šķirnēm pastāv dabiska barjera,

25
00:02:32,283 --> 00:02:34,829
piemēram, dzīvžogs. 

26
00:02:38,734 --> 00:02:41,250
Tāpat šķirnes iespējams izolēt,

27
00:02:41,432 --> 00:02:45,112
pamīšus atverot 
un aizverot kukaiņu tīklus

28
00:02:51,163 --> 00:02:56,058
vai slēgtā tīklā ievietojot 
nelielas kukaiņu ligzdas.

29
00:02:57,934 --> 00:02:58,974
Sīkāk -

30
00:02:59,214 --> 00:03:03,338
izolēšanas metodēm veltītajā 
‘’Sēklu ražošanas ābeces’’ modulī! 

31
00:03:12,654 --> 00:03:14,700
Dzīves cikls 

32
00:03:19,003 --> 00:03:21,578
Mazie redīsi ir viengadīgi augi.

33
00:03:21,709 --> 00:03:24,858
Sējot agri 
(martā vai aprīlī),

34
00:03:25,280 --> 00:03:28,938
vasaras beigās 
iegūsiet nobriedušas sēklas. 

35
00:04:09,760 --> 00:04:12,741
Lai sēklai atlasītu 15 - 20 augus,

36
00:04:13,170 --> 00:04:18,196
izvelciet no zemes 50 - 100 redīsus 
un rūpīgi pārbaudiet tos.

37
00:04:36,200 --> 00:04:40,385
Izraudzītos redīsus pārstādiet, 
pilnībā pārklājot ar augsni -

38
00:04:40,785 --> 00:04:46,058
25 cm atstatumā vienu no otra;
30 cm - starp rindām.

39
00:04:51,720 --> 00:04:53,243
Tad redīsus dāsni aplaista. 

40
00:04:59,200 --> 00:05:02,138
Lielie redīsi ir divgadīgi augi.

41
00:05:03,992 --> 00:05:07,032
Sēklu ražošanai - 
tie sējami vasarā.

42
00:05:09,294 --> 00:05:14,567
Redīsi pārlaidīs ziemu un nākamajā gadā 
veidos ziedus un sēklas. 

43
00:05:17,963 --> 00:05:22,050
Rudens beigās 
lielos redīsus izrok un atlasa. 

44
00:05:26,080 --> 00:05:28,632
Sēklas iegūst no veselīgiem redīsiem,

45
00:05:28,720 --> 00:05:33,461
kas novēroti visos attīstības posmos,
lai sekotu šķirnes kritērijiem:

46
00:05:33,934 --> 00:05:40,967
izmērs, krāsa, dzīvelīgums, 
agrīnība, noturība pret slimībām.

47
00:05:41,258 --> 00:05:43,180
Redīsi nedrīkst 
pāragri attīstīt ziedkātus.

48
00:05:46,421 --> 00:05:50,407
Mīkstumam jābūt maigam - 
bez caurumiem, spīvākam vai saldākam. 

49
00:05:58,400 --> 00:06:01,350
Lapas nogriež, neievainojot kakliņu.

50
00:06:01,578 --> 00:06:06,334
Saknes uzglabājamas mitrās smiltīs 
vai īpašos plastmasas maisiņos.

51
00:06:09,418 --> 00:06:14,218
Saknes ieziemojamas pagrabā 
vai vēsā vietā, ko neskar sala ietekme.

52
00:06:20,007 --> 00:06:25,607
Ziemas gaitā saknes regulāri pārbaudiet, 
atmetot iepuvušās. 

53
00:06:53,076 --> 00:06:58,669
Pavasarī redīsus pārstāda dārzā - 
25 cm atstatumā vienu no otra

54
00:06:59,352 --> 00:07:03,847
(30 cm - starp rindām),
tad dāsni aplaista.

55
00:07:41,770 --> 00:07:47,549
Sēklaugi var sasniegt līdz pat 2 m garumu. 
Tie jābalsta ar mietiņiem. 

56
00:07:55,207 --> 00:07:58,618
Sēklas ir gatavas, kad pākstis 
pieņēmušas smilškrāsas toni. 

57
00:08:02,305 --> 00:08:05,760
Parasti visi kāti 
nenobriest vienlaikus.

58
00:08:06,756 --> 00:08:08,550
Lai nezaudētu sēklas,

59
00:08:08,858 --> 00:08:13,025
augi ievācami pakāpeniski - 
līdzko nobriedis katrs atsevišķais kāts.

60
00:08:16,378 --> 00:08:21,512
Iespējams ievākt visu augu kopumā, 
kamēr sēklas vēl negatavas. 

61
00:08:26,189 --> 00:08:30,349
Briedināšanas process turpināsies, 
augus novietojot sausā,

62
00:08:30,440 --> 00:08:32,130
labi vēdinātā vietā. 

63
00:08:46,523 --> 00:08:50,060
Ievākšana - šķirošana - uzglabāšana

64
00:08:52,370 --> 00:08:54,800
Redīsu sēklas ievācamas,

65
00:08:55,090 --> 00:08:58,450
kad pākstis viegli atveramas ar pirkstiem. 

66
00:09:02,740 --> 00:09:07,629
Lai ievāktu sēklas, 
saspaidiet pākstis ar mīklas veltni.

67
00:09:13,338 --> 00:09:18,901
Pākstis var arī ievietot maisā, 
un maisu izkult pret mīkstāku virsmu.

68
00:09:20,060 --> 00:09:23,600
Lielus sēklu apjomus iespējams izkult, 
tos mīdot kājām

69
00:09:23,800 --> 00:09:26,334
vai pārbraucot tiem ar transportu. 

70
00:09:29,250 --> 00:09:34,560
Pākstis, kas grūti atveramas, 
būs pilnas ar negatavām,

71
00:09:34,734 --> 00:09:36,429
vājas dīgtspējas sēklām. 

72
00:09:40,705 --> 00:09:41,992
Sēklas šķirojot,

73
00:09:42,189 --> 00:09:47,941
vispirms rupjā sietā 
tiek aizturētas pelavas.

74
00:09:55,272 --> 00:09:57,774
Tad sēklas tiek sijātas otrā sietā,

75
00:09:57,890 --> 00:10:02,247
kas aizturēs sēklas, 
bet izsijās smalkākas pelavu daļiņas. 

76
00:10:21,134 --> 00:10:24,101
Pēdīgi - sēklas vētījamas, 
pār tām pūšot elpu

77
00:10:24,261 --> 00:10:28,596
ai paturot sēklas vējā, 
lai atbrīvotos no visām pelavām. 

78
00:10:39,170 --> 00:10:42,123
Paciņā vienmēr ievietojiet etiķeti
ar sugas un šķirnes nosaukumu,

79
00:10:42,349 --> 00:10:48,029
kā arī ievākšanas gadu 
(ārējs uzraksts mēdz nodilt). 

80
00:10:55,940 --> 00:11:00,800
Dažas dienas uzglabājot sēklas 
saldētavā, tiks iznīcināti parazīti. 

81
00:11:02,840 --> 00:11:06,727
Redīsu sēklas dīgtspēju 
saglabā 5 - 10 gadus.

82
00:11:07,636 --> 00:11:10,945
Termiņš pagarināms, 
sēklas glabājot saldētavā. 
