1
00:00:08,756 --> 00:00:14,283
Fenhelis piederīgs Apiaceae dzimtai 
un Foeniculum vulgaris sugai.

2
00:00:16,501 --> 00:00:22,196
Fenhelis var būt viengadgs, divgadīgs, 
bet dažkārt - daudzgadīgs augs.

3
00:00:24,230 --> 00:00:25,920
Pastāv divi fenheļa paveidi:

4
00:00:28,007 --> 00:00:31,820
Florences fenheļiem raksturīgs sīpols,

5
00:00:34,340 --> 00:00:36,880
bet garšaugu fenheļiem sīpola nav -

6
00:00:37,360 --> 00:00:43,032
tos audzē lapu un sēklu ieguvei, 
kas lietojamas medicīnā vai aromatizēšanai. 

7
00:00:44,603 --> 00:00:47,410
Pastāv agrīnās un vēlīnās šķirnes.

8
00:00:50,807 --> 00:00:57,178
Fenhelis ir fotoperiodisks augs - 
ilgāks dienasgaismas periods veicina ziedēšanu. 

9
00:01:07,818 --> 00:01:08,778
Apputeksnēšana 

10
00:01:28,538 --> 00:01:33,636
Fenheļa ziedkopu veido čemurs, 
kas sastāv no sīkiem, dzelteniem,

11
00:01:33,934 --> 00:01:35,890
parasti - hermafrodītiem ziediņiem.

12
00:01:38,349 --> 00:01:44,465
Putekšņlapa (vīrišķais vairošanās orgāns) 
nobriest pirms auglenīcas (sievišķā orgāna). 

13
00:01:50,138 --> 00:01:54,712
Tādēļ zieds 
nespēj pašapaugļoties.

14
00:01:56,029 --> 00:01:59,636
Tā kā ziedi 
neatplaukst vienlaicīgi -

15
00:02:00,174 --> 00:02:03,940
pašappute ir iespējama 
viena čemura

16
00:02:04,247 --> 00:02:07,018
vai viena auga 
dažādu čemuru ietvaros. 

17
00:02:11,032 --> 00:02:15,760
Viens otru apaugļo 
arī atsevišķi augi. 

18
00:02:19,440 --> 00:02:25,018
Fenhelis ir alogāms augs, 
ko lielākoties apputeksnē kukaiņi.

19
00:02:32,320 --> 00:02:36,240
Pastāv dažādu šķirņu 
savstarpējās krustošanās risks. 

20
00:02:39,156 --> 00:02:41,512
Fenhelis var krustoties 
arī ar savvaļas fenheli,

21
00:02:41,883 --> 00:02:44,778
kas izplatīts 
dažos pasaules reģionos. 

22
00:02:50,269 --> 00:02:52,043
Lai novērstu krustošanos,

23
00:02:52,261 --> 00:02:55,890
divas fenheļa šķirnes 
audzējamas 1 km atstatumā. 

24
00:03:00,581 --> 00:03:06,821
Attālums samazināms līdz 500 m, 
ja pastāv dabiska barjera (piemēram, dzīvžogs). 

25
00:03:14,610 --> 00:03:20,420
Šķirnes iespējams izolēt - 
pamīšus atverot un aizverot kukaiņu tīklus

26
00:03:26,152 --> 00:03:30,660
vai slēgtā tīklā ievietojot 
nelielas kukaiņu ligzdas.

27
00:03:31,389 --> 00:03:32,523
Sīkākas norādes:

28
00:03:32,680 --> 00:03:38,189
‘’Sēklu ražošanas ābeces’’ 
izolēšanas metodēm veltītajā modulī. 

29
00:03:44,276 --> 00:03:45,272
Dzīves cikls

30
00:03:55,200 --> 00:03:57,629
Fenhelis-garšaugs neveido sīpolu.

31
00:03:59,054 --> 00:04:01,396
Parasti tas ir daudzgadīgs augs.

32
00:04:08,727 --> 00:04:13,825
Iesēts pavasarī - tas zied 
un ražo sēklas jau pirmajā gadā.

33
00:04:14,210 --> 00:04:18,501
Lieliskā sala izturība 
ļauj fenhelim pārziemot augsnē.

34
00:04:19,352 --> 00:04:23,687
Sēklu apjoms būs lielāks - 
otrajā audzēšanas gadā. 

35
00:04:29,120 --> 00:04:32,341
Sīpola fenhelis kultivējams vairākos veidos. 

36
00:04:34,727 --> 00:04:39,258
Maigā klimatā - pavasara šķirnes 
audzējamas kā viengadīgas.

37
00:04:39,563 --> 00:04:42,370
Tās sēj agri pavasarī (martā),

38
00:04:42,712 --> 00:04:46,458
lai pirms ziedēšanas 
pagūtu aizmesties sīpols.

39
00:04:53,047 --> 00:04:58,996
Sēklai kultivētais sīpola fenhelis 
uzskatāms par divgadīgu augu. 

40
00:04:59,556 --> 00:05:05,876
Vasaras šķirnes sēj pēc 20. jūnija, 
dienām sākot kļūt īsākām.

41
00:05:06,000 --> 00:05:08,683
Tādējādi augi neuzziedēs pārāk agri.

42
00:05:09,461 --> 00:05:12,749
Līdz ziemai tie pagūs 
attīstīt sīpolu.

43
00:05:14,072 --> 00:05:17,316
Ziedēšana un sēklu ražošana 
sekos otrajā gadā. 

44
00:05:43,578 --> 00:05:46,974
Sēklai tiek izraudzīti 
tikai tādi augi, kam raksturīgas

45
00:05:47,127 --> 00:05:50,138
tipiskās šķirnes īpašības un attīstīti sīpoli.

46
00:05:55,258 --> 00:05:57,781
Pārāk agri uzziedējuši augi tiek aizvākti. 

47
00:06:04,487 --> 00:06:06,850
Sēklu ieguvei izvēlieties sīpolus,

48
00:06:06,952 --> 00:06:10,101
kas atbilst šķirnes kritērijiem:

49
00:06:11,127 --> 00:06:13,905
krāsa, forma, dzīvelīgums. 

50
00:06:28,654 --> 00:06:32,472
Aukstā klimatā - 
sīpoli zemē var sasalt.

51
00:06:33,090 --> 00:06:36,380
Tādēļ nogrieziet lapas 
virs pēdējā pumpura

52
00:06:36,698 --> 00:06:40,887
un ievietojiet sīpolus 
burkā vai podiņā.

53
00:06:41,170 --> 00:06:44,298
Saknes apklāj ar zemi,
lai sargātu no sala un pārmērīga mitruma. 

54
00:06:53,614 --> 00:06:56,312
Sekojiet sīpolu 
ziemošanas gaitai,

55
00:06:56,981 --> 00:07:00,080
atlasot visus iepuvušos.

56
00:07:05,620 --> 00:07:11,236
Sīpoli tiek pārstādīti pavasarī, 
kad bargs sals vairs nav gaidāms. 

57
00:07:30,900 --> 00:07:36,334
Ģenētisko daudzveidību 
nodrošinās 15 - 20 augi. 

58
00:07:40,094 --> 00:07:44,596
Maigā klimatā - 
sīpola fenhelis var pārziemot augsnē.

59
00:07:45,854 --> 00:07:49,694
Tas turpinās augt nākamā gada 
pavasarī - un uzziedēs. 

60
00:08:27,890 --> 00:08:32,225
Fenheļa augu garie ziedkāti 
mēdz apgāzties.

61
00:08:33,054 --> 00:08:34,283
Tos nepieciešams stutēt. 

62
00:08:35,440 --> 00:08:41,367
Čemuri tiek nogriezti kopā ar kāta daļu - 
kad sāk izbirt pirmās gatavās sēklas.

63
00:08:45,927 --> 00:08:47,750
Tos var nogriezt arī agrāk -

64
00:08:48,443 --> 00:08:53,120
sēklu nobriešana turpināsies
žāvēšanās laikā.

65
00:08:56,218 --> 00:09:01,745
Turpmāk sēklas žāvējamas 
sausā un labi vēdinātā vietā. 

66
00:09:10,080 --> 00:09:12,756
Ievākšana - šķirošana - uzglabāšana

67
00:09:19,614 --> 00:09:22,967
Saberzējiet čemurus plaukstās, 
lai izbirtu sēklas. 

68
00:09:34,167 --> 00:09:39,323
Šķirošanai vispirms izmantojiet rupjus sietus, 
kas aizturēs pelavas.

69
00:09:45,578 --> 00:09:50,400
Tad sēklas tveramas smalkākā sietā, 
kam cauri izkļūs putekļi. 

70
00:10:06,600 --> 00:10:09,620
Pēdīgi - vētījiet sēklas, tām uzpūšot.

71
00:10:09,890 --> 00:10:12,225
Sēklas attīrīsies no pelavu paliekām. 

72
00:10:15,505 --> 00:10:19,450
Vienmēr maisiņā ievietojiet etiķeti 
ar sugas un šķirnes nosaukumu,

73
00:10:19,556 --> 00:10:24,378
kā arī ievākšanas gadu
(ārējs uzraksts mēdz nodilt).

74
00:10:27,687 --> 00:10:32,138
Dažas dienas uzglabājiet sēklas saldētavā, 
lai nonāvētu parazītus. 

75
00:10:35,400 --> 00:10:39,345
Fenheļa sēklas dīgtspēju 
saglabā 4 gadus.

76
00:10:39,520 --> 00:10:42,778
Dažkārt dīgtspēja nezūd līdz pat 7 gadiem.

77
00:10:43,345 --> 00:10:46,916
Termiņš pagarināms - 
glabājot sēklas saldētavā.

78
00:10:47,578 --> 00:10:50,560
Vienā gramā ietilpst 
aptuveni 300 sēklas. 
