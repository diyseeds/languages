# Copyright (C) 2021 Maxime Lecoq
# This file is distributed under the GNU General Public License v2 or later.
msgid ""
msgstr ""
"Project-Id-Version: Diyseeds 2.0\n"
"POT-Creation-Date: 2021-07-10T00:00:00+02:00\n"
"PO-Revision-Date: 2021-12-15 10:14+0000\n"
"Last-Translator: Roberto Sole <rebelsun@riseup.net>\n"
"Language-Team: Italian <http://translate.diyseeds.org/projects/"
"diyseeds-org-videos/abc-seeds/it/>\n"
"Language: it\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 4.8\n"
"WPOT-Type: post-types/video\n"
"X-Domain: wpot\n"
"WPOT-Origin: abc-seeds\n"

#. Title of the film
msgctxt "title"
msgid "How to choose seeds"
msgstr ""

msgid "It is important to know the origin of the varieties that you are going to cultivate in your garden."
msgstr ""
"È importante conoscere l'origine delle varietà che vanno coltivate nell'orto "
"come porta semi."

msgid "When choosing the varieties from which you plan to produce seeds, you should avoid using ones that have been developed by the big seed industry. So do not use F1 or F2 hybrid seeds, GMOs or all seeds developed through biotechnology."
msgstr ""
"Bisogna evitare di utilizzare varietà provenienti dall'industria sementiera "
"per coltivare i porta semi. Cio' riguarda i semi denominati ibridi F1 oppure "
"F2, gli OMG e tutte le sementi provenienti dalle biotecnologie."

msgid "Plants grown from F1 and F2 hybrid seeds will not reproduce themselves identically. Their seeds can also be sterile or produce plants with unpredictable characteristics. By technically blocking their varieties, the seed multinationals can prevent gardeners from reproducing their own seeds and thereby keep their monopoly on the market."
msgstr ""
"Le piante ottenute da semi ibridi F1 o F2 non si replicano in modo identico "
"e i loro semi possono essere sterili o produrre piante con caratteristiche "
"imprevedibili. Con questo blocco tecnologico i sementieri multinazionali "
"impediscono di riprodurre le loro varietà, mantenendo quindi il loro "
"monopolio sul mercato."

msgid "These technical mechanisms reinforce the legal regulations that prohibit the free reproduction of seeds. All of these varieties are covered by intellectual property rights, such as plant property rights and patents."
msgstr ""
"Questi metodi tecnologici rinforzano il divieto giuridico di riprodurre "
"liberamente i semi. Infatti, tutte queste varietà sono coperte da diritti di "
"proprietà intellettuale con brevetti."

msgid "To begin producing your own seeds, the seeds should be from varieties that are open-pollinated and if possible from organic, biodynamic or agro-ecological agriculture."
msgstr ""
"Per cominciare la propria produzione di semente, i semi devono essere "
"riproducibili liberamente e possibilmente provenienti da agricoltura "
"biologica, biodinamica oppure agroecologiche."

msgid "Nature is very generous and will in general provide you with far more seeds than you will need for your own use. The surplus can be given to others or exchanged."
msgstr ""
"La natura essendo generosa, produrrà molte più sementi del necessario. "
"L'eccedenza servirà per scambi e doni."

#. This title will be displayed by search engines like Google. 60 characters max.
msgctxt "seo-title"
msgid "Seeds to start your own seed production"
msgstr "Semi per cominciare la propria produzione di semente"

#. This description will be displayed below the title on search engine results. 160 characters max.
msgctxt "seo-description"
msgid "The origin of the varieties to be grown as seed plants must be known. Choose reproducible, organic, bio-dynamic or agroecological seeds."
msgstr ""
"E' importante conoscere l'origine delle varietà che vanno coltivate "
"nell'orto come porta semi. Si scelgono sementi riproducibili, da agricoltura "
"biologica, biodinamica oppure agroecologiche."

#. The tags are used by search engines to reference the content. Use commas to separate tags.
msgctxt "seo-tags"
msgid "seed carrier, seeds, cultivate, varieties, free, reproducible, organic, bio-dynamic, agroecological, video, ABC, explanations, learn"
msgstr ""
"Porta semi, semente, coltivare, varietà, liberi, riproducibili, biologica, "
"biodinamica, agroecologiche, video, ABC, spiegazioni, imparare"

#. This title will be displayed on Online Video Platforms (OVP) like YouTube or Vimeo.
msgctxt "ovp-title"
msgid "ABC How to choose your seeds for seed production"
msgstr ""

#. This description will be displayed on Online Video Platforms (OVP) like YouTube or Vimeo.
msgctxt "ovp-description"
msgid "Watch this video to learn how to choose what seeds to reproduce. You should avoid using ones that have been developed by the big seed industry. So do not use F1 or F2 hybrid seeds, GMOs or any seed developed through biotechnology. To begin producing your own seeds, the seeds should be from varieties that are open-pollinated and if possible from organic, biodynamic or agro-ecological agriculture. Nature is very generous and will in general provide you with far more seeds than you will need for your own use. The surplus can be given to others or exchanged."
msgstr ""
