# Copyright (C) 2021 Maxime Lecoq
# This file is distributed under the GNU General Public License v2 or later.
msgid ""
msgstr ""
"Project-Id-Version: Diyseeds 2.0\n"
"POT-Creation-Date: 2021-07-10T00:00:00+02:00\n"
"PO-Revision-Date: 2021-08-10 07:56+0000\n"
"Language: it\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 4.5.1\n"
"WPOT-Type: post-types/page\n"
"WPOT-Origin: contact\n"
"X-Domain: wpot\n"

#. This title will be displayed into the browser's window title bar.
msgctxt "browser-title"
msgid "Diyseeds: contacts and credits"
msgstr ""

#. The slug is the last part of the URL (www.example.org/slug-is-here). Only alphanumerical characters are allowed, use dash to replace space. The slug should be short and nice and it must reflect the content it refers to. It is usually made from the title. If you don't provide it, a slug will be auto-generated.
msgctxt "slug"
msgid "contacts-credits"
msgstr ""

#. Title of a page's section.
msgctxt "section-title"
msgid "Contact Diyseeds"
msgstr ""

msgid "Address"
msgstr ""

msgid "Ergonomy/graphic design"
msgstr ""

msgid "Website developer/communication"
msgstr ""

msgctxt "person-name"
msgid "Maxime Lecoq"
msgstr ""

msgctxt "person-name"
msgid "Claire Lamure"
msgstr ""

#. Title of a page's section.
msgctxt "section-title"
msgid "Translators"
msgstr ""

msgctxt "translation-version"
msgid "English:"
msgstr ""

msgctxt "translation-version"
msgid "French:"
msgstr ""

msgctxt "translation-version"
msgid "Spanish:"
msgstr ""

msgctxt "translation-version"
msgid "Arabic:"
msgstr ""

msgctxt "translation-version"
msgid "Portuguese:"
msgstr ""

msgctxt "translation-version"
msgid "Italian:"
msgstr ""

msgctxt "translation-version"
msgid "German:"
msgstr ""

msgctxt "translation-version"
msgid "Hungarian:"
msgstr ""

msgctxt "translation-version"
msgid "Dutch:"
msgstr ""

msgctxt "translation-version"
msgid "Latvian:"
msgstr ""

msgctxt "translation-version"
msgid "Russian:"
msgstr ""

msgctxt "translation-version"
msgid "Ukrainian:"
msgstr ""

msgctxt "translation-version"
msgid "Turkish:"
msgstr ""

msgctxt "person-name"
msgid "Nicholas Bell"
msgstr ""

msgctxt "person-name"
msgid "Tatiana Bielousova"
msgstr ""

msgctxt "person-name"
msgid "Erik D'haese"
msgstr ""

msgctxt "person-name"
msgid "Manuel Delafoulhouze"
msgstr ""

msgctxt "person-name"
msgid "Judit Fehér"
msgstr ""

msgctxt "person-name"
msgid "József Hegyesi"
msgstr ""

msgctxt "person-name"
msgid "Borbála Lipka"
msgstr ""

msgctxt "person-name"
msgid "Serge Harfouche"
msgstr ""

msgctxt "person-name"
msgid "Philip Rizk"
msgstr ""

msgctxt "person-name"
msgid "Luna Saenz"
msgstr ""

msgctxt "person-name"
msgid "Bediz Yilmaz"
msgstr ""

msgctxt "person-name"
msgid "Ieva Zariņa"
msgstr ""

msgctxt "person-name"
msgid "Olya Zubyk"
msgstr ""

#. Title of a page's section.
msgctxt "section-title"
msgid "Newsletter"
msgstr ""

#. This title will be displayed by search engines like Google. 60 characters max.
msgctxt "seo-title"
msgid "Diyseeds contact information"
msgstr ""

#. This description will be displayed below the title on search engine results. 160 characters max.
msgctxt "seo-description"
msgid "We can be contacted by email and post. You can also find us on social networks. Subscribe to our newsletter."
msgstr ""

#. The tags are used by search engines to reference the content. Use commas to separate tags.
msgctxt "seo-tags"
msgid "contacts, contact details, credits, email, address, newsletter, webmaster, developer, ergonomy, design, translators"
msgstr ""
