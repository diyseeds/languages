﻿1
00:00:09,030 --> 00:00:12,676
Cucumber is part of the Cucurbitaceae family

2
00:00:12,820 --> 00:00:16,160
and belongs to the Cucumis sativus species.

3
00:00:16,800 --> 00:00:19,080
It is divided into five types:  

4
00:00:20,400 --> 00:00:21,980
- open field cucumber

5
00:00:24,300 --> 00:00:26,700
whose skin is covered with small thorns  

6
00:00:30,900 --> 00:00:34,420
- greenhouse cucumbers with a very smooth skin 

7
00:00:38,760 --> 00:00:42,000
- 'sikkim' type cucumbers with a red-orange skin

8
00:00:45,900 --> 00:00:50,720
- cucumbers with very small fruit for processing,
such as gerkins  

9
00:00:52,880 --> 00:00:54,630
- round-shaped cucumbers.  

10
00:01:07,680 --> 00:01:08,440
Pollination  

11
00:01:20,880 --> 00:01:23,598
The cucumber is a monoecious plant,

12
00:01:24,400 --> 00:01:28,880
meaning that it has both male
and female flowers on the same plant.

13
00:01:31,470 --> 00:01:34,770
Female flowers have an ovary under the flower.

14
00:01:37,900 --> 00:01:42,400
It is in fact a mini cucumber
that will develop after pollination.

15
00:01:46,150 --> 00:01:51,730
Male flowers are the first to appear
and are at the end of long stems.

16
00:01:56,050 --> 00:01:58,900
The flowers only open during one day. 

17
00:02:00,280 --> 00:02:05,500
Cucumbers can be self-fertilised,
meaning that a female flower can be

18
00:02:05,700 --> 00:02:09,520
fertilised by pollen from a male flower
of the same plant.

19
00:02:13,570 --> 00:02:15,550
But cross pollination is more common.

20
00:02:16,780 --> 00:02:21,250
Insects and mainly bees
pollinate cucumber flowers.

21
00:02:29,160 --> 00:02:34,980
All varietes of the Cucumis sativus
species cross-pollinate between each other.

22
00:02:36,960 --> 00:02:42,090
Cucumbers do not, however,
cross with melons, watermelons or squashes. 

23
00:02:44,440 --> 00:02:50,620
To avoid cross-pollination,
keep a distance of 1km between two varieties.

24
00:02:53,360 --> 00:02:58,490
You can reduce this distance to 500m
if there is a natural barrier,

25
00:02:58,690 --> 00:02:59,450
such as a hedge. 

26
00:03:02,450 --> 00:03:06,890
There are several methods to produce
seeds from different varieties of cucumber

27
00:03:07,220 --> 00:03:08,510
grown in the same garden.

28
00:03:11,630 --> 00:03:15,547
One of them is to cover an entire variety
with a net

29
00:03:16,140 --> 00:03:18,800
and to place
a small bumble bee hive inside.

30
00:03:22,220 --> 00:03:26,660
Another is to cover two varieties
in different nets :

31
00:03:28,240 --> 00:03:33,500
open one while the other is closed on one day,
and alternate the next day.

32
00:03:34,340 --> 00:03:36,500
Let the wild insects do their work.

33
00:03:37,460 --> 00:03:41,570
The production will be smaller because
certain flowers will not be pollinated.

34
00:03:47,810 --> 00:03:50,180
You can also pollinate the flowers manually.

35
00:03:50,930 --> 00:03:55,430
It is more delicate to do this with cucumbers
than with squashes or zucchini

36
00:03:55,940 --> 00:03:58,400
as cucumber flowers are a lot smaller.

37
00:04:01,070 --> 00:04:03,098
These three methods are described

38
00:04:03,180 --> 00:04:06,240
in the modules
on mechanical isolation techniques

39
00:04:06,243 --> 00:04:10,040
and on manual pollination
in the ABC of seed production. 

40
00:04:22,500 --> 00:04:23,140
Life cycle  

41
00:04:39,600 --> 00:04:44,220
Cucumbers grown for seed production
are cultivated in the same way

42
00:04:44,340 --> 00:04:45,720
as those for consumption.

43
00:05:01,590 --> 00:05:06,300
Keep at least 6 plants to ensure good
genetic diversity.

44
00:05:07,440 --> 00:05:08,790
Ideally, grow a dozen. 

45
00:05:33,790 --> 00:05:38,075
Take great care to select the plants
you keep for seeds in accordance

46
00:05:38,220 --> 00:05:40,870
with the specific characteristics
of the variety.

47
00:05:42,100 --> 00:05:46,450
You should keep vigorous plants which
have produced well-developed cucumbers.

48
00:05:48,520 --> 00:05:50,110
Get rid of sick plants.

49
00:06:04,260 --> 00:06:08,827
The maturity of cucumbers for seed
production is not the same

50
00:06:08,960 --> 00:06:10,140
as for consumption.

51
00:06:10,430 --> 00:06:13,500
In fact, we generally eat unripe cucumbers. 

52
00:06:14,250 --> 00:06:18,352
To produce seeds, it is important to
let the cucumber develop

53
00:06:18,560 --> 00:06:20,040
until it is fully mature.

54
00:06:22,870 --> 00:06:27,040
It must have grown to its full size
and its colour must have changed.

55
00:06:34,060 --> 00:06:37,540
You can also harvest the fruit shortly
before maturity.

56
00:06:38,140 --> 00:06:42,160
In this case, place it in a warm area
and let it ripen.

57
00:06:42,970 --> 00:06:46,090
This technique increases the fertility
of the seeds. 

58
00:06:55,790 --> 00:06:59,100
Extracting - sorting - storing  

59
00:07:07,160 --> 00:07:09,920
To extract the seeds, open the cucumber,


60
00:07:12,220 --> 00:07:13,960
remove the pulp with the seeds

61
00:07:25,020 --> 00:07:27,520
and let the mixture ferment for a few days.

62
00:07:32,250 --> 00:07:36,990
This fermentation will remove the viscous
envelope that surrounds the seeds.

63
00:07:40,340 --> 00:07:44,990
For more information, refer to the module
on wet-processing techniques

64
00:07:45,190 --> 00:07:46,790
in the ABC of seed production.

65
00:07:53,520 --> 00:07:57,060
You should then clean the seeds under
running water using a sieve.

66
00:08:00,850 --> 00:08:03,748
To get rid of the empty sterile seeds,

67
00:08:04,420 --> 00:08:07,300
place all the seeds in a container full of water.

68
00:08:07,630 --> 00:08:12,460
The full, heavier seeds will sink to
the bottom and the empty ones will float. 

69
00:08:16,320 --> 00:08:21,240
Remove the empty ones, rince the good ones
and dry them in a well-ventilated area.

70
00:08:26,730 --> 00:08:28,980
Rub them together to separate them.

71
00:08:32,490 --> 00:08:37,920
To be sure that the seeds are fully dry,
they should break if you try to bend them. 

72
00:08:43,820 --> 00:08:46,060
Write a label with the name
of the species and variety

73
00:08:46,280 --> 00:08:51,180
as well as the year of harvesting
and leave it inside the package,

74
00:08:51,440 --> 00:08:53,960
as writing on the outside may rub off.

75
00:09:01,310 --> 00:09:05,630
Leave the seeds in the freezer
for a few days to kill any parasites.

76
00:09:09,790 --> 00:09:15,460
Cucumber seeds have a germination capacity
of 6 years, sometimes longer.

77
00:09:18,340 --> 00:09:21,070
This can be prolonged by storing them
in a freezer.

78
00:09:23,110 --> 00:09:27,250
One gram contains 30 to 40 seeds. 
