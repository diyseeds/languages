1
00:00:15,994 --> 00:00:19,620
A seed is the result
of the transformation of the ovule

2
00:00:19,780 --> 00:00:21,660
fertilised through pollination.

3
00:00:23,994 --> 00:00:25,820
It is made up of an embryo

4
00:00:27,620 --> 00:00:35,500
surrounded by a reserve of food
protected by a skin called a tegument.

5
00:00:38,538 --> 00:00:42,260
The size of this food reserve
depends on the species.

6
00:00:53,621 --> 00:01:00,180
The appearance of seeds can greatly differ,
their shape, size, colour

7
00:01:00,448 --> 00:01:03,660
and texture changes according to the plant.

8
00:01:05,728 --> 00:01:09,780
From these seeds healthy
and vigorous plants will spring forth

9
00:01:10,060 --> 00:01:15,180
which will in their turn produce new seeds,
thus perpetuating the cycle of life.

10
00:01:21,904 --> 00:01:27,146
The seeds will be disseminated over smaller
or greater distances from the mother plant.

11
00:01:30,474 --> 00:01:33,860
Depending on the species
and the specific conditions,

12
00:01:34,060 --> 00:01:38,260
the seeds can simply fall to the ground
at the foot of the mother plant,

13
00:01:38,580 --> 00:01:42,620
or they can be dispersed
much further afield by the wind

14
00:01:42,860 --> 00:01:46,460
or thanks to animals
in whose fur the seeds get attached.

15
00:01:52,293 --> 00:01:55,060
The aim is to spread as far as possible.

16
00:01:56,260 --> 00:02:02,380
Wind, water, insects, birds and many other animals

17
00:02:02,700 --> 00:02:06,021
can be involuntary vectors
of this dissemination.

18
00:02:07,861 --> 00:02:10,660
Seeds have an extraordinary ability to wait,

19
00:02:10,940 --> 00:02:16,896
sometimes for a very long time, until the external
conditions are suitable for their development.

20
00:02:17,306 --> 00:02:20,380
This phenomenon is called dormancy.

21
00:02:24,597 --> 00:02:29,900
Seeds will come out of this dormancy period
thanks to different kinds of stimulus.

22
00:02:39,340 --> 00:02:43,520
Some seeds have to pass
through the intestinal tract of animals

23
00:02:44,026 --> 00:02:47,740
where they are activated
by the animal’s digestive enzymes.

24
00:02:48,133 --> 00:02:53,260
Others are stimulated
by a fermentation process or by a frost.

25
00:02:56,053 --> 00:03:01,500
The gardener will therefore sometimes
seek to imitate certain natural phenomena

26
00:03:02,060 --> 00:03:04,500
in order to end the dormancy of seeds.

27
00:03:05,082 --> 00:03:11,820
When sowing you should ensure that all of
the conditions facilitating germination are met :

28
00:03:12,580 --> 00:03:15,820
the amount of water, the temperature,

29
00:03:16,260 --> 00:03:19,460
the level of light, the depth in the ground,

30
00:03:20,074 --> 00:03:23,380
the appropriate season
for the specific variety.

31
00:03:24,160 --> 00:03:25,589
The lifespan of seeds,

32
00:03:26,186 --> 00:03:29,620
that is the length of time
that they are able to germinate,

33
00:03:30,180 --> 00:03:31,780
depends on the species.

34
00:03:32,160 --> 00:03:37,540
Parsnip seeds, for example, only retain
their full germination capacity for one year.

35
00:03:38,060 --> 00:03:41,900
Chicory seeds, on the other hand,
remain valid for ten years.

36
00:03:42,380 --> 00:03:46,260
Once this period is past,
their germination rate will fall.

37
00:03:48,298 --> 00:03:52,420
If you want to test the fertility rate
of certain seeds,

38
00:03:52,740 --> 00:03:55,420
you can carry out a germination test.

39
00:03:55,700 --> 00:03:58,340
You count the number of seeds that you sow

40
00:03:59,340 --> 00:04:01,900
and then the number of plants
that effectively grow.

41
00:04:03,900 --> 00:04:08,060
You will see that the older
the seeds are the less they germinate.

42
00:04:13,946 --> 00:04:19,140
The lifespan of seeds is also influenced
by the drying and storage conditions.

43
00:04:22,282 --> 00:04:26,220
First of all you must ensure
that the seeds are perfectly dry.

44
00:04:27,088 --> 00:04:30,580
They must then be stored
in a cold and dry environment

45
00:04:31,300 --> 00:04:34,940
with little light
and with minimal variation of temperature,

46
00:04:36,860 --> 00:04:41,260
because hot and humid conditions
can adversely affect seed quality.

47
00:04:43,248 --> 00:04:47,140
You should also watch out
for the small insects that eat the seeds.

48
00:04:48,320 --> 00:04:53,740
These are easily eliminated
by leaving the seeds several days in the freezer.

49
00:04:56,050 --> 00:04:58,042
Freezing is a good storage method

50
00:04:58,469 --> 00:05:05,980
because all seeds can resist
in airtight bags at icy temperatures of -18°.

51
00:05:08,300 --> 00:05:10,220
This prolongs their lifespan.

52
00:05:16,600 --> 00:05:18,741
To maintain seed vitality,

53
00:05:19,180 --> 00:05:22,780
it is however necessary
to cultivate seeds regularly.

54
00:05:25,061 --> 00:05:28,900
This will ensure that they adapt
to rapidly evolving environmental

55
00:05:29,060 --> 00:05:30,740
and climatic conditions.
