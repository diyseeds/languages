﻿1
00:00:00,000 --> 00:00:05,760
Interview mit Serge Harfouche vom Verein Buzuruna Juzuruna
Mai 2021, Beqaatal, Libanon
www.DIYseeds.org
Lehrfilme zur Samengärtnerei

2
00:00:05,920 --> 00:00:08,760
-Was heisst Buzurna Juzurna?

3
00:00:09,520 --> 00:00:12,100
Es heisst "Unsere Samen, unsere Wurzeln".

4
00:00:12,140 --> 00:00:16,150
-Warum habt ihr diesen Namen für euer Projekt gewählt?

5
00:00:17,870 --> 00:00:25,170
Wir sind eine Schulgärtnerei für die Produktion von bäuerlichem Saatgut

6
00:00:26,090 --> 00:00:30,570
und wir bieten Trainings dazu an.

7
00:00:30,660 --> 00:00:35,520
Das sind unsere zwei Hauptaktivitäten.

8
00:00:36,220 --> 00:00:39,350
Also produzieren wir Samen

9
00:00:39,530 --> 00:00:42,460
die wiederum zu Wurzeln werden.

10
00:00:42,460 --> 00:00:47,020
Das Ziel ist das Erreichen von kleinbäuerlicher Autonomie

11
00:00:47,130 --> 00:00:49,380
und von Ernährungssouveränität.

12
00:00:49,380 --> 00:00:53,120
All das passt gut zusammen.

13
00:00:54,080 --> 00:01:04,730
-Warum ist Ernährungssouveränität im Libanon derzeit so wichtig?

14
00:01:05,600 --> 00:01:07,840
Es ist dringender denn je.

15
00:01:07,860 --> 00:01:13,600
Wir sind mitten in einer beispiellosen ökonomischen Krise,

16
00:01:13,600 --> 00:01:15,600
die äusserst gewaltvoll ist.

17
00:01:15,710 --> 00:01:24,150
Unsere Währung hat das 10-fache ihres Wertes verloren.

18
00:01:24,260 --> 00:01:30,020
Von 1.500 libanesischen Pfund pro Dollar ist der Wert auf 15.000 gefallen

19
00:01:30,040 --> 00:01:32,880
und hat sich nun mehr oder weniger bei 12.000 Pfund eingependelt.

20
00:01:32,950 --> 00:01:36,600
Es ist sehr unvorhersehbar, wir wissen nicht, wann es explodiert.

21
00:01:36,680 --> 00:01:37,770
-Seit wann passiert das?

22
00:01:37,860 --> 00:01:43,400
Der Kollaps hat 2018 angefangen

23
00:01:43,510 --> 00:01:54,640
aber die Inflation auf dem Markt hat Mitte 2019, Anfang 2020 Fahrt aufgenommen, denke ich.

24
00:01:55,950 --> 00:02:01,260
-Welchen Einfluss haben diese Geschehnisse auf das Leben der Menschen?

25
00:02:02,600 --> 00:02:07,020
Es ist beängstigend, denn die Löhne in libanesischen Pfund sind kein Stück gestiegen.

26
00:02:07,060 --> 00:02:12,200
Der Mindestlohn war um die 400 € wert.

27
00:02:12,200 --> 00:02:17,350
Jetzt liegt er bei 40 €,

28
00:02:17,880 --> 00:02:21,620
aber alles was du konsumierst ist viel teurer

29
00:02:21,680 --> 00:02:24,750
weil der Libanesische Pfund an den Dollar gekoppelt ist, wird es immer schlimmer.

30
00:02:24,840 --> 00:02:27,910
Stell dir vor, du kaufst eine Packung Milch.

31
00:02:27,930 --> 00:02:31,530
Eine Packung Milch, die 3000 Libanesische Pfund gekostet hat,

32
00:02:31,530 --> 00:02:36,480
kostet nun 27.000 oder 25.000 Pfund,

33
00:02:36,600 --> 00:02:40,200
während du zum Beipiel noch immer 100.000 Pfund verdienst.

34
00:02:40,200 --> 00:02:44,160
Anstatt also mehrere Packungen im Laufe des Monats kaufen zu können

35
00:02:44,260 --> 00:02:47,380
kannst du dir kaum mehr eine einzige leisten.

36
00:02:47,440 --> 00:02:59,180
Da unsere Versorgung zu 90-95% auf Importen basiert,

37
00:02:59,260 --> 00:03:01,580
und so also von anderen Währungen abhängt

38
00:03:01,620 --> 00:03:03,260
-dem Dollar in unserem Fall-,

39
00:03:03,310 --> 00:03:05,520
wird alles immer teurer:

40
00:03:05,570 --> 00:03:08,210
Brot wird teurer,

41
00:03:08,360 --> 00:03:11,450
Treibstoffe werden teurer

42
00:03:11,560 --> 00:03:14,660
während die Löhne gleich bleiben.

43
00:03:14,690 --> 00:03:17,720
Das heisst, dass der Lebensstandard,

44
00:03:17,750 --> 00:03:20,820
die Lebensqualität stark darunter leiden

45
00:03:20,910 --> 00:03:23,090
und der Zugang zu Nahrung

46
00:03:23,110 --> 00:03:26,530
und Ernährungssicherheit noch stärker getroffen werden.

47
00:03:26,720 --> 00:03:30,370
Deshalb ist es so wichtig, dass wir unser eigenes Saatgut machen

48
00:03:30,430 --> 00:03:33,200
anstatt es zu importieren oder von irgendwo her zu kaufen,

49
00:03:33,220 --> 00:03:35,490
insbesondere wenn es sich um hybride und sterile Samen handelt,

50
00:03:35,520 --> 00:03:38,230
die du jedes Jahr wieder kaufen musst.

51
00:03:40,010 --> 00:03:43,980
Wir müssen auch unseren eigenen landwirtschaftlichen Betriebsmittel produzieren

52
00:03:44,020 --> 00:03:46,480
auf biologische und saubere Weise,

53
00:03:46,530 --> 00:03:49,810
und Komposte herstellen...

54
00:03:49,890 --> 00:03:51,540
Das ganze Paket eben.

55
00:03:51,570 --> 00:03:55,290
Das versuchen wir durch Schulungen zu vermitteln

56
00:03:55,330 --> 00:03:57,330
und durch die Aktivitäten auf dem Hof.

57
00:03:58,730 --> 00:04:01,460
-Als du in Frankreich warst, hast du uns

58
00:04:01,470 --> 00:04:04,110
Fotos davon gezeigt,

59
00:04:04,160 --> 00:04:07,630
wie sich das Leben für viele Menschen verändert hat,

60
00:04:07,640 --> 00:04:10,560
seit der Revolution, wie du es nennst;

61
00:04:10,660 --> 00:04:12,610
sehr verschiedene Menschen:

62
00:04:12,630 --> 00:04:18,410
Student*innen, Menschen die in der Verwaltung arbeiten,

63
00:04:18,430 --> 00:04:20,730
Menschen mit den verschiedensten Hintergründen kommen auf einmal zu euch

64
00:04:20,750 --> 00:04:23,360
und interessieren sich für all das.

65
00:04:23,630 --> 00:04:24,810
Genau.

66
00:04:24,930 --> 00:04:27,530
Es kommt mir so vor, als hätten wir das schon gespürt,

67
00:04:28,180 --> 00:04:33,380
dass wir uns bereits in einer beispiellosen Wirtschaftskrise befinden.

68
00:04:33,490 --> 00:04:38,210
Das ist der Grund, warum die Revolution begann.

69
00:04:38,270 --> 00:04:43,040
Also hatten die Menschen eine diffuse Ahnung

70
00:04:43,040 --> 00:04:47,650
dass die kommenden Jahre nicht einfach sein würden

71
00:04:47,680 --> 00:04:50,490
und dass sie sich darauf würden einstellen müssen.

72
00:04:50,540 --> 00:04:54,340
Es gibt ein wachsendes Bewusstsein dafür

73
00:04:54,400 --> 00:05:00,000
dass eine Wirtschaft, die nicht produktiv ist, nicht nachhaltig ist,

74
00:05:00,080 --> 00:05:03,130
und unsere Wirschaft war nicht produktiv.

75
00:05:03,460 --> 00:05:05,920
Seit langem, seit 30, 40 Jahren,

76
00:05:05,970 --> 00:05:10,250
ist unsere Wirtschaft schuldenbasiert.

77
00:05:11,090 --> 00:05:15,320
-Also haben viele Leute angefangen, selbst etwas zu produzieren?

78
00:05:15,460 --> 00:05:17,980
...

79
00:05:18,040 --> 00:05:19,290
Es nimmt auf.

80
00:05:20,430 --> 00:05:23,260
Entschuldige, ich habe kurz mit einer Kollegin geredet.

81
00:05:23,280 --> 00:05:26,070
-Seid ihr viele auf dem Hof?

82
00:05:26,850 --> 00:05:31,620
Wir sind 16 Erwachsene, 27 Kinder,

83
00:05:31,650 --> 00:05:34,160
und momentan besuchen uns viele Freund*innen,

84
00:05:34,210 --> 00:05:36,000
Freiwillige, Praktikant*innen, ...

85
00:05:36,030 --> 00:05:38,930
Im Grossen und Ganzen sind wir circa 50 Menschen,

86
00:05:39,000 --> 00:05:40,370
vielleicht etwas mehr.

87
00:05:40,410 --> 00:05:43,350
-Und wie gross ist euer Gelände?

88
00:05:44,940 --> 00:05:47,770
Auf dem Hof mit dem Verein

89
00:05:47,920 --> 00:05:54,140
haben wir insgesamt 2 Hektar:

90
00:05:54,580 --> 00:05:57,940
18.000 m² Fläche im Anbau

91
00:05:57,950 --> 00:06:00,500
und 2000 m² Gebäude,

92
00:06:00,670 --> 00:06:02,980
die Anzucht

93
00:06:03,070 --> 00:06:06,050
und ein Auslauf für die Hühner,

94
00:06:06,060 --> 00:06:07,640
Ziegen und Schafe.

95
00:06:07,710 --> 00:06:09,820
Dieses Jahr haben wir es geschafft

96
00:06:09,830 --> 00:06:13,490
zusätzliche 7 Hektar zu pachten,

97
00:06:13,610 --> 00:06:16,600
ungefähr 17 Acre,

98
00:06:16,700 --> 00:06:19,700
um unsere Saatgutsammlung zu erhalten und zu vergrössern,

99
00:06:19,720 --> 00:06:21,350
insbesondere das Getreide.

100
00:06:22,280 --> 00:06:24,770
-Aber ihr lebt nicht auf dem Hof?

101
00:06:25,610 --> 00:06:27,510
Nicht wirklich.

102
00:06:27,520 --> 00:06:32,860
Einer unserer Kollegen und Mitbegründer lebt auf dem Hof.

103
00:06:34,400 --> 00:06:36,320
Wir haben ein Multifunktionsgebäude

104
00:06:36,340 --> 00:06:39,780
das sowohl Einmachküche,

105
00:06:39,820 --> 00:06:43,090
Küche für uns, Lagerhalle, Schule

106
00:06:43,140 --> 00:06:44,350
und Wohnraum ist,

107
00:06:44,360 --> 00:06:47,040
ein langes Gebäude.

108
00:06:47,060 --> 00:06:50,120
Walid lebt dort mit seiner Familie.

109
00:06:50,260 --> 00:06:52,440
Unsere anderen Kolleg*innen und Freunde

110
00:06:52,460 --> 00:06:54,260
leben direkt gegenüber vom Hof,

111
00:06:54,310 --> 00:06:56,530
und wir leben ein Stück weiter die Strasse rauf,

112
00:06:56,550 --> 00:06:58,450
4 Minuten zu Fuss.

113
00:06:59,590 --> 00:07:02,290
-Und produziert ihr hier auch einen Teil

114
00:07:02,300 --> 00:07:04,400
eures Konsums selbst?

115
00:07:04,740 --> 00:07:06,950
Ja!

116
00:07:07,160 --> 00:07:10,070
Wir kaufen sehr wenig ein.

117
00:07:10,110 --> 00:07:13,770
Was auch immer wir selbst herstellen können, machen wir hier - wir kaufen es nicht!

118
00:07:14,900 --> 00:07:17,980
Es gibt aber Sachen wie Reis,

119
00:07:18,010 --> 00:07:20,880
die wir nur schwer hier anbauen können,

120
00:07:20,910 --> 00:07:22,690
solche Dinge eben.

121
00:07:22,920 --> 00:07:24,120
Aber jenseits davon…

122
00:07:24,130 --> 00:07:27,230
Letztes Jahr haben wir es sogar geschafft, Honig zu produzieren,

123
00:07:27,250 --> 00:07:30,520
aber ein Raubtier hat unsere Bienen angegriffen,

124
00:07:30,560 --> 00:07:32,600
deshalb haben wir nur noch 3 Bienenstöcke,

125
00:07:32,620 --> 00:07:34,570
von 11.

126
00:07:36,830 --> 00:07:39,980
Die Idee ist auch, einfach zu zeigen, was alles möglich ist.

127
00:07:41,590 --> 00:07:46,920
-Wie steht es um Partnerschaften und Netzwerke

128
00:07:46,940 --> 00:07:50,600
die ihr im Libanon und auch in Syrien habt,

129
00:07:50,610 --> 00:07:54,570
wie entwickeln sie sich, wie kam es dazu?

130
00:07:54,590 --> 00:07:56,450
Erzähl uns davon!

131
00:07:57,100 --> 00:08:00,160
Generell war die Anfangsidee die, Saatgut zu produzieren

132
00:08:00,180 --> 00:08:03,170
und dann Leute darin auszubilden, die es interessiert

133
00:08:03,190 --> 00:08:04,560
ihr eigenes Saatgut zu produzieren,

134
00:08:04,590 --> 00:08:08,450
damit nicht alle jedes Saatgut jedes Jahr reproduzieren müssen.

135
00:08:08,450 --> 00:08:11,030
Es geht wirklich um das Erlernen von Autonomie

136
00:08:11,040 --> 00:08:12,560
und Unabhängigkeit.

137
00:08:12,580 --> 00:08:16,380
Ausgehend davon sind Saatgutretter*innen

138
00:08:16,400 --> 00:08:17,910
an viele Orte gegangen.

139
00:08:17,920 --> 00:08:19,710
Einige gingen zurück nach Syrien

140
00:08:19,710 --> 00:08:22,260
um ihr eigenes Saatgut zu produzieren.

141
00:08:22,280 --> 00:08:25,120
Es gibt eine große Vielfalt an Situationen.

142
00:08:25,150 --> 00:08:29,610
Unsere Freund*innen, die wir seit Beginn begleiteten

143
00:08:29,630 --> 00:08:32,020
sind mittlerweile komplett selbstständig.

144
00:08:32,040 --> 00:08:34,970
Sie kommen sehr gut für sich zurecht.

145
00:08:34,980 --> 00:08:36,910
Natürlich begleiten wir einander weiterhin,

146
00:08:36,920 --> 00:08:40,350
wenn es einen Bedarf nach Information und Austausch gibt:

147
00:08:40,360 --> 00:08:42,840
"wir versuchten dies", "wir testeten das".

148
00:08:42,920 --> 00:08:46,690
Die Idee ist so viele Saatgutretter*innen und

149
00:08:46,690 --> 00:08:49,360
Saatgutbanken wie möglich zu haben

150
00:08:49,390 --> 00:08:51,640
überall im Libanon

151
00:08:51,690 --> 00:08:55,600
und vielleicht eines Tages sogar in Syrien, wer weiß.

152
00:08:57,460 --> 00:09:02,240
- Ist die Grenze noch geschlossen?

153
00:09:03,970 --> 00:09:06,550
Ja, sie ist geschlossen

154
00:09:06,570 --> 00:09:09,540
und es ist ziemlich kompliziert für Leute

155
00:09:09,610 --> 00:09:12,040
bestimmter Nationalitäten, sie zu passieren.

156
00:09:12,050 --> 00:09:16,250
Wenn du nicht Teil einer diplomatischen Mission oder der Armee bist

157
00:09:16,400 --> 00:09:17,990
ist es ganz schön hart -

158
00:09:18,660 --> 00:09:20,860
es ist besser, es gar nicht zu versuchen.

159
00:09:21,700 --> 00:09:23,550
Es ist grad besser, zu bleiben wo du bist.

160
00:09:23,580 --> 00:09:25,730
- Zu den Schulungen...

161
00:09:25,920 --> 00:09:30,670
Du hast dich an den Übersetzungen von DIYseeds beteiligt -

162
00:09:34,330 --> 00:09:37,440
und du hast die Website übersetzt?

163
00:09:38,750 --> 00:09:40,100
... die Webseite auf Arabisch,

164
00:09:40,130 --> 00:09:44,680
und ich habe die Videos überprüft, die von anderen Teams gemacht wurden,

165
00:09:44,820 --> 00:09:47,750
verdammt viel Arbeit muss ich sagen.

166
00:09:47,760 --> 00:09:50,090
Es ist beeindruckend, was sie erschaffen haben, es ist superschön.

167
00:09:50,170 --> 00:09:52,080
Und extrem praktisch.

168
00:09:52,120 --> 00:09:54,720
Wenn du eine Schulung gibst

169
00:09:54,780 --> 00:09:57,840
in der du versuchst, die Entnahme von Saatgut zu erklären

170
00:09:57,870 --> 00:10:01,660
oder wie Sicherheitsabstände eingehalten werden

171
00:10:01,670 --> 00:10:03,360
um Kreuzungen zu vermeiden,

172
00:10:03,380 --> 00:10:05,360
sind die Illustrationen sehr hilfreich,

173
00:10:05,360 --> 00:10:08,140
die Zeichnungen, die Videos und die Erklärungen.

174
00:10:08,140 --> 00:10:11,550
Wir zeigen diese Filme nicht nur während der Schulungen,

175
00:10:11,560 --> 00:10:13,470
sondern sie sind auch erreichbar

176
00:10:13,480 --> 00:10:17,000
über die Smartphones der Menschen

177
00:10:17,020 --> 00:10:19,660
-wenn sie Internet haben, aber hey, das ist schon zugänglich-,

178
00:10:19,680 --> 00:10:23,800
das erlaubt Leuten selbstständig mit Saatgut zu arbeiten.

179
00:10:23,910 --> 00:10:25,680
Es ist genial, es ist perfekt.

180
00:10:27,600 --> 00:10:30,770
- Und ist es auch einfach für Menschen auf dem Land

181
00:10:31,240 --> 00:10:35,560
an diese Informationen zu kommen, sie zu nutzen, …

182
00:10:35,640 --> 00:10:36,480
Genau!

183
00:10:36,540 --> 00:10:37,730
Es ist einfach für sie,

184
00:10:37,760 --> 00:10:40,500
weil es leicht und simpel gehalten ist.

185
00:10:40,520 --> 00:10:44,910
Es ist kein dickes Buch, was du mit dir rumtragen musst.

186
00:10:44,960 --> 00:10:47,940
Es gibt viele Bäuer*innen, die nicht lesen können,

187
00:10:48,000 --> 00:10:50,220
aber sie verstehen, was sie da hören.

188
00:10:50,260 --> 00:10:52,970
Noch dazu gibt es die Zeichnungen und das Video selbst,

189
00:10:53,040 --> 00:10:55,570
das illustriert die Worte viel besser,

190
00:10:55,580 --> 00:10:58,770
es ist wesentlich zugänglicher und einfach,

191
00:10:58,800 --> 00:11:01,830
als Tutorial ist es einfacher zu folgen

192
00:11:01,870 --> 00:11:05,270
als das dicke Buch vorzuholen und zu entziffern.

193
00:11:06,000 --> 00:11:11,350
- Da ihr nur samenfestes Saatgut produziert,

194
00:11:11,380 --> 00:11:14,580
natürliche Samen, die sich reproduzieren lassen,

195
00:11:14,660 --> 00:11:18,030
können alle, die diese Samen haben

196
00:11:18,160 --> 00:11:21,380
und Zugang zu den Videos haben, schnell Autonomie zurückerlangen.

197
00:11:21,400 --> 00:11:26,560
Kannst du uns erzählen wie, und wie schnell das gehen kann?

198
00:11:26,580 --> 00:11:27,630
Wie schnell?

199
00:11:27,650 --> 00:11:30,530
Das hängt vom Zugang zu Land ab,

200
00:11:30,560 --> 00:11:32,160
es hängt vom Zugang zu Wasser ab,

201
00:11:32,180 --> 00:11:34,310
es hängt von der Sicherheitslage in der Region ab.

202
00:11:34,320 --> 00:11:37,320
Wir haben Freund*innen, die mehrmals umziehen mussten

203
00:11:37,390 --> 00:11:39,260
von ihrem Land in Syrien.

204
00:11:39,300 --> 00:11:41,170
Jedes mal wenn sie sich niederließen

205
00:11:41,180 --> 00:11:42,830
gab es Bombardierungen,

206
00:11:42,840 --> 00:11:44,810
also mussten sie wieder und wieder weiterziehen.

207
00:11:44,960 --> 00:11:47,240
Die Situation selbst ist schon kompliziert.

208
00:11:47,320 --> 00:11:51,670
Aber wenn du ein paar grundlegende Werkzeuge hast,

209
00:11:51,710 --> 00:11:54,660
kannst du zurechtkommen, egal wo du hinkommst.

210
00:11:54,690 --> 00:11:57,110
Du hast dein Saatgut, du weißt wie du es kultivierst,

211
00:11:57,130 --> 00:12:00,260
du weißt, wie du es vermehrst, es entnimmst,

212
00:12:00,290 --> 00:12:01,990
es sicherst, es aufbewahrst, etc.

213
00:12:02,020 --> 00:12:04,980
Du bist ein*e freie*r Mann oder Frau!

214
00:12:07,140 --> 00:12:08,050
Das ist es.

215
00:12:08,190 --> 00:12:12,120
-Also begegnest du Leuten, die innerhalb einer Saison

216
00:12:12,170 --> 00:12:15,180
lernen, Saatgut zu produzieren -

217
00:12:15,580 --> 00:12:19,470
sodass sie auf einmal ihre eigenen Samen haben?

218
00:12:19,520 --> 00:12:20,510
Genau.

219
00:12:20,530 --> 00:12:23,240
Die meisten Menschen mit denen wir arbeiten

220
00:12:23,290 --> 00:12:25,350
sind in einer Landwirtschaft aufgewachsen,

221
00:12:25,370 --> 00:12:26,840
oder mit einem landwirtschaftlichen Hintergrund

222
00:12:26,850 --> 00:12:29,000
oder sie haben in der Landwirtschaft gearbeitet,

223
00:12:29,020 --> 00:12:30,440
meist auf konventionellen Betrieben.

224
00:12:30,460 --> 00:12:34,350
Es ist einfach für sie, einen Wandel zu vollführen.

225
00:12:36,550 --> 00:12:38,500
Früher mussten sie

226
00:12:38,520 --> 00:12:39,970
Saatgut kaufen

227
00:12:39,970 --> 00:12:42,450
aber in dem Moment, in dem sie verstanden

228
00:12:42,470 --> 00:12:45,010
dass ihr Schicksal in ihren Händen lag,

229
00:12:45,260 --> 00:12:47,100
und sie einfach das richtige

230
00:12:47,120 --> 00:12:50,730
Werkzeug brauchten,

231
00:12:50,880 --> 00:12:53,890
da hatten sie nach ein oder zwei Saisons ihr eigenes Saatgut,

232
00:12:53,930 --> 00:12:55,040
und wir tauschen uns aus.

233
00:12:55,080 --> 00:12:56,800
Wir arbeiten auf lange Sicht zusammen,

234
00:12:56,820 --> 00:13:00,570
zum Beispiel produziert eine*r Saatgut einer bestimmten Sorte,

235
00:13:00,580 --> 00:13:03,410
wer anders produziert eine andere, wir produzieren die dritte …

236
00:13:03,423 --> 00:13:08,680
um so viel Diversität wie möglich zu erreichen und dann

237
00:13:08,720 --> 00:13:10,680
fängt es an zu funktionieren.

238
00:13:11,760 --> 00:13:16,280
-Und du, wie kam es dazu, dass du dich hier einbringst?

239
00:13:18,426 --> 00:13:20,280
Tatsächlich durch Zufall.

240
00:13:20,330 --> 00:13:26,600
Ich habe in Beirut gewohnt und mein Mitbewohner war in der Uni mit Ferdi,

241
00:13:26,693 --> 00:13:30,613
einer der Mitbegründer des Vereins und des Projekts.

242
00:13:32,320 --> 00:13:35,253
Als Ferdi und Lara in den Libanon zurückkamen

243
00:13:35,293 --> 00:13:38,253
um zu überlegen, wie sie das Saatgutprojekt starten sollten,

244
00:13:38,290 --> 00:13:41,270
wie zu produzieren, wo all das zu machen,

245
00:13:41,308 --> 00:13:43,228
was getan werden musste,

246
00:13:43,242 --> 00:13:44,927
sie sind bei uns untergekommen

247
00:13:44,936 --> 00:13:46,592
für ungefähr einen Monat

248
00:13:46,602 --> 00:13:48,545
es ist sofort der Funke übergesprungen.

249
00:13:49,727 --> 00:13:51,303
Wie ich schon sagte:

250
00:13:51,336 --> 00:13:54,174
Ich habe mich bewusst mit an Bord ziehen lassen!

251
00:13:54,988 --> 00:13:56,898
-Und du hast dein Leben umgekrempelt?

252
00:13:57,068 --> 00:13:59,101
Was hast du davor gemacht?

253
00:14:00,112 --> 00:14:03,303
Direkt davor habe ich in der Öffentlichkeitsabeit gearbeitet

254
00:14:03,336 --> 00:14:04,823
für eine Universität.

255
00:14:04,851 --> 00:14:06,960
Aber eigentlich bin ich Bibliothekar,

256
00:14:09,520 --> 00:14:12,640
Ich habe Psychologie studiert, Literatur,

257
00:14:12,672 --> 00:14:15,868
überhaupt nichts mit Saatgut!

258
00:14:19,745 --> 00:14:21,520
Aber es hilft bei den Übersetzungen,

259
00:14:21,543 --> 00:14:22,922
also ist es perfekt!

260
00:14:23,275 --> 00:14:24,992
-Und hast du Lust, weiterzumachen?

261
00:14:25,920 --> 00:14:27,637
Ja!

262
00:14:27,661 --> 00:14:29,863
Wie ich gerade schon gesagt hatte:

263
00:14:29,890 --> 00:14:33,002
wir sind mitten im Krisennotstand,

264
00:14:34,164 --> 00:14:39,185
ich denke das ist wirklich eine essentielle Arbeit,

265
00:14:40,014 --> 00:14:41,477
es ist notwendig.

266
00:14:41,515 --> 00:14:47,298
Und es ist so bereichernd auf mehreren Ebenen:

267
00:14:47,310 --> 00:14:50,555
auf der sozialen Ebene, auf der politischen Ebene auch.

268
00:14:50,597 --> 00:14:53,050
Was wir machen ist extrem wichtig

269
00:14:53,072 --> 00:14:54,823
und gleichzeitig schön.

270
00:14:55,247 --> 00:14:58,211
Ich glaube nicht, dass ich demnächst damit aufhören werde!

271
00:14:58,418 --> 00:15:01,797
-Super! Vielen Dank für das Interview.

272
00:15:01,960 --> 00:15:03,722
Gerne, sehr gerne!

273
00:15:03,755 --> 00:15:06,290
Es hat mich sehr gefreut, wieder mit dir zu reden.

274
00:15:07,620 --> 00:15:08,936
-Ich hoffe wir sehen uns wieder.

275
00:15:08,960 --> 00:15:11,134
Wir erwarten dich auf unserem Hof!

276
00:15:11,181 --> 00:15:15,811
-Ich würde sehr gerne kommen :) Ich kann nichts versprechen, aber…

277
00:15:17,463 --> 00:15:20,560
Wann auch immer du kommen willst, sag uns einfach bescheid,

278
00:15:20,583 --> 00:15:22,672
und "ahla wa sahla" (willkommen!)

279
00:15:25,251 --> 00:15:26,409
-Vielen Dank!

280
00:15:26,432 --> 00:15:27,091
Perfekt!

281
00:15:28,120 --> 00:15:35,720
Vielen Dank an Serge Harfouche und alle Freunde von Buzuruna Juzuruna
mehr Informationen unter: @buzurunajuzuruna (Instagram)
Fotos: Charlotte Joubert, Buzuruna Juzuruna, Rabih Yassine
Interview & Schnitt: Erik D'haese

282
00:15:35,720 --> 00:15:40,320
www.DIYseeds.org
Lehrfilme zur samengärtnerei
