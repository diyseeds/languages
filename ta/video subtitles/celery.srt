﻿1
00:00:08,283 --> 00:00:11,140
Celery belongs to the Apiaceae family

2
00:00:11,576 --> 00:00:15,168
and to the Apium graveolens species.

3
00:00:16,680 --> 00:00:22,100
It is a biennial plant cultivated
for its leaves, roots and stalks.

4
00:00:22,980 --> 00:00:28,072
Three subspecies are grown:
stalk celery dulce,

5
00:00:34,220 --> 00:00:38,036
celeriac or root celery rapaceum,

6
00:00:42,916 --> 00:00:45,643
and leaf celery secalinum.

7
00:00:51,178 --> 00:00:54,516
The wild variety of celery is called smallage. 

8
00:01:01,340 --> 00:01:02,340
Pollination 

9
00:01:15,170 --> 00:01:20,174
The inflorescence of the celery
is an umbel composed of small flowers

10
00:01:20,349 --> 00:01:22,523
that are usually hermaphrodite.

11
00:01:23,105 --> 00:01:27,949
The stamen, the male sexual organ,
matures before the pistil,

12
00:01:28,160 --> 00:01:29,992
the female sexual organ. 

13
00:01:35,920 --> 00:01:41,163
Self-fertilization does not therefore
occur within the same flower.

14
00:01:42,240 --> 00:01:45,905
Yet since the flowers do not bloom
at the same time,

15
00:01:46,138 --> 00:01:50,392
self-fertilization is possible
within the same umbel

16
00:01:51,018 --> 00:01:54,610
or between two umbels on the same plant. 

17
00:01:57,560 --> 00:02:02,254
Fertilization also occurs between
the umbels of different plants. 

18
00:02:05,592 --> 00:02:09,287
Insects are the main agents
of cross-pollination.

19
00:02:12,480 --> 00:02:16,100
Flowering celery gives off a very strong scent

20
00:02:16,232 --> 00:02:21,032
and produces an abundance
of nectar that attracts many insects. 

21
00:02:22,152 --> 00:02:25,280
All kinds of celery can cross with each other.

22
00:02:25,600 --> 00:02:29,520
They can also cross with wild celery
found in coastal areas.

23
00:02:29,723 --> 00:02:33,214
On rare occasions they can cross with parsley. 

24
00:02:35,585 --> 00:02:37,440
To avoid cross-pollination,

25
00:02:37,847 --> 00:02:43,032
two different varieties of celery should be
grown about one kilometer apart. 

26
00:02:45,840 --> 00:02:50,603
This distance can be reduced to 500 meters
if a natural barrier

27
00:02:50,756 --> 00:02:54,140
such as a hedge exists between the varieties. 

28
00:03:02,770 --> 00:03:09,221
The varieties can also be isolated
by alternately opening and closing insect nets

29
00:03:10,021 --> 00:03:15,025
or by placing small hives
with insects inside a closed insect net

30
00:03:16,647 --> 00:03:17,730
(for this technique,

31
00:03:17,840 --> 00:03:22,305
see the module on isolation techniques
in "The ABC of seed production "). 

32
00:03:27,818 --> 00:03:29,220
Life cycle

33
00:03:40,494 --> 00:03:42,909
All celery varieties are biennial.

34
00:03:43,498 --> 00:03:45,330
In the first year of cultivation,

35
00:03:45,530 --> 00:03:49,898
celery for seed is grown in the same way
as celery for consumption.

36
00:03:52,290 --> 00:03:54,727
They will produce seeds in the second year. 

37
00:04:50,843 --> 00:04:55,890
There are different methods for storing celery
for seed production over the winter.

38
00:05:00,203 --> 00:05:04,778
In mild climates you can leave
them where they are in the garden.

39
00:05:06,276 --> 00:05:11,083
Nevertheless, they should be protected
with a frost blanket or straw.

40
00:05:11,810 --> 00:05:14,203
The straw should be removed in spring. 

41
00:05:18,160 --> 00:05:21,470
In cold climates you should
uproot the celery plants

42
00:05:21,600 --> 00:05:24,160
before the severe winter frosts.

43
00:05:30,029 --> 00:05:33,258
Cut back the leaves to a few
centimetres from the collar.

44
00:05:34,430 --> 00:05:38,334
The less water the roots contain,
the longer they can be stored. 

45
00:05:42,290 --> 00:05:44,540
All types of celery should be selected

46
00:05:44,640 --> 00:05:48,952
in accordance with the characteristics
specific to the variety.

47
00:05:58,420 --> 00:06:02,625
For root celery: colour, shape, taste.

48
00:06:15,592 --> 00:06:19,460
For stalk celery :
the size of the stalks and their colour.

49
00:06:20,007 --> 00:06:24,552
For leaf celery : the abundance
of the leaves and their taste. 

50
00:06:28,560 --> 00:06:32,276
Then they are placed,
without their touching each other,

51
00:06:32,676 --> 00:06:35,236
in a sandbox protected from frost. 

52
00:06:45,640 --> 00:06:47,207
Over the course of the winter,

53
00:06:47,300 --> 00:06:51,912
the roots should be checked carefully
and any rotten ones should be removed. 

54
00:07:12,720 --> 00:07:16,189
The roots are then replanted
at the beginning of spring,

55
00:07:16,487 --> 00:07:19,483
once the risk of hard frosts has passed.

56
00:07:22,770 --> 00:07:27,061
It would seem that planting celery plants
for seed close to each other

57
00:07:27,338 --> 00:07:33,207
reduces the number of tertiary umbels,
whose seeds are of poor quality. 

58
00:07:34,320 --> 00:07:38,872
About fifteen plants are necessary
to ensure good genetic diversity.

59
00:07:40,632 --> 00:07:45,730
Care should be taken that the roots
do not dry out once they are replanted. 

60
00:08:05,720 --> 00:08:10,436
Celery produces several umbels
that do not all bloom at the same time.

61
00:08:11,316 --> 00:08:15,534
The first, the primary umbel,
is found at the top of the main stem. 

62
00:08:17,230 --> 00:08:21,389
Those that develop from the main stem
are called secondary umbels. 

63
00:08:22,210 --> 00:08:28,167
The tertiary umbels form on the stems
that branch off the secondary stems. 

64
00:08:45,120 --> 00:08:48,356
It is preferable to harvest the primary umbels.

65
00:08:48,865 --> 00:08:53,040
The secondary umbels should be
harvested only if necessary. 

66
00:08:55,600 --> 00:08:58,625
Celery seeds are mature when they turn brown.

67
00:08:59,520 --> 00:09:05,105
Seeds should be harvested when
most of the primary umbels start to turn brown. 

68
00:09:08,807 --> 00:09:12,254
When they are mature,
the seeds fall to the ground.

69
00:09:13,105 --> 00:09:18,232
If the weather is windy or rainy,
harvest them before they are completely mature.

70
00:09:23,236 --> 00:09:29,432
In any case, they should continue
to be dried in a dry and well ventilated place. 

71
00:09:40,232 --> 00:09:43,156
Extracting - sorting - storing

72
00:09:56,778 --> 00:10:00,720
You should wear gloves to extract
the seeds from the umbels by hand. 

73
00:10:11,054 --> 00:10:15,032
To sort the seeds you should use
sieves that retain the chaff. 

74
00:10:30,836 --> 00:10:36,930
The seeds should then be winnowed by blowing
on them so that any remaining chaff is removed. 

75
00:10:50,421 --> 00:10:53,796
Always include a label
with the name of the variety,

76
00:10:53,910 --> 00:11:00,210
the species and the year in the bag
as writing on the bag can be rubbed off. 

77
00:11:07,316 --> 00:11:12,589
Storing the seeds in the freezer
for several days kills certain parasite larvae. 

78
00:11:13,800 --> 00:11:17,447
Celery seeds can germinate for up to eight years.

79
00:11:18,090 --> 00:11:22,669
Sometimes this can be extended
to ten years or even longer.

80
00:11:23,660 --> 00:11:27,432
This can be further prolonged
by storage in a freezer. 

81
00:11:28,392 --> 00:11:31,709
One gram contains around 2000 seeds. 

82
00:11:33,425 --> 00:11:36,218
Germination of celery seeds can be erratic.

83
00:11:36,900 --> 00:11:40,465
It appears that they remain dormant
for a certain period. 
