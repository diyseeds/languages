1
00:00:09,820 --> 00:00:13,340
The common onion belongs
to the Amarylidaceae family,

2
00:00:13,775 --> 00:00:15,655
which used to be called Alliaceae.

3
00:00:15,975 --> 00:00:18,610
It is part of the allium cepa species.

4
00:00:22,395 --> 00:00:26,430
It is a biennial plant
that produces a bulb in the first year.

5
00:00:27,745 --> 00:00:31,090
During the second year,
it flowers and then produces seeds.

6
00:00:35,585 --> 00:00:38,460
Some varieties are early, others late,

7
00:00:38,900 --> 00:00:42,550
certain can be stored for a long period,
others for less.

8
00:00:48,330 --> 00:00:54,910
Depending on the variety, onions can be
very different in shape, colour and taste.

9
00:01:01,780 --> 00:01:02,600
Pollination

10
00:01:20,310 --> 00:01:25,540
The flower heads of onions
are made up of small hermaphrodite flowers

11
00:01:27,590 --> 00:01:30,030
that are individually self-sterile.

12
00:01:34,520 --> 00:01:37,320
They need insects to fertilise each other.

13
00:01:39,330 --> 00:01:41,995
This means that onions are allogamous.

14
00:01:43,960 --> 00:01:50,020
There is therefore the risk that insects
cause cross pollination between varieties.

15
00:01:57,180 --> 00:01:59,420
To avoid crossing between varieties,

16
00:01:59,555 --> 00:02:04,160
do not plant two varieties
of onion less than 1km apart.

17
00:02:08,230 --> 00:02:14,270
You can reduce this distance to 200m
if there is a natural barrier, such as a hedge.

18
00:02:18,665 --> 00:02:23,635
It is also possible to use mosquito
nets to isolate each variety.

19
00:02:25,185 --> 00:02:30,605
You can cover one variety with a permanent net
and put a bumble bee hive inside.

20
00:02:32,160 --> 00:02:35,770
Or you can cover two varieties
with different nets:

21
00:02:36,405 --> 00:02:41,200
open one while the other is closed
on one day, and alternate the next day.

22
00:02:42,945 --> 00:02:45,740
These methods are described in greater detail

23
00:02:46,010 --> 00:02:48,900
in the module
on ‘mechanical isolation techniques’

24
00:02:49,090 --> 00:02:51,000
in the ABC of Seed production.

25
00:03:00,870 --> 00:03:02,020
Life cycle

26
00:03:13,220 --> 00:03:19,560
Onions for seed production are cultivated
in the same way as those for storing in the winter.

27
00:03:23,600 --> 00:03:27,150
They will produce seeds only
in the second year of growth.

28
00:03:35,115 --> 00:03:39,495
To produce seed-bearing plants,
it is better to sow seeds,

29
00:03:40,370 --> 00:03:46,585
rather than planting onion sets or bulblets
as these tend to bloom too early in the season.

30
00:04:03,565 --> 00:04:08,315
Each onion that produces flowers
in the first year must be removed

31
00:04:09,155 --> 00:04:14,460
as the seeds taken from these plants
will often also produce flowers too soon.

32
00:04:20,015 --> 00:04:26,305
The common onion needs high temperatures
and long days to form a bulb in its first year.

33
00:04:34,025 --> 00:04:37,535
Onions must be harvested
when the bulb is well formed.

34
00:04:46,740 --> 00:04:51,310
To make sure you can replant
15 to 20 bulbs in spring,

35
00:04:51,860 --> 00:04:57,970
keep between 20 and 30 the previous autumn,
as there are often losses during winter.

36
00:05:02,495 --> 00:05:06,720
Select the bulbs according
to the variety's specific criteria,

37
00:05:06,990 --> 00:05:09,660
such as shape, colour and size.

38
00:05:13,035 --> 00:05:17,030
They must also be healthy
and have a beautiful unbroken skin.

39
00:05:21,025 --> 00:05:25,800
Get rid of bulbs that are not homogenous,
and those that are divided.

40
00:05:30,700 --> 00:05:36,575
Let the bulbs dry for 10 to 12 days
in a warm and well-ventilated area.

41
00:05:37,600 --> 00:05:42,540
Be careful not to bruise the bulbs
as this could cause them to rot in winter.

42
00:05:46,340 --> 00:05:52,195
To store the bulbs in winter, it is crucial
to put them in a cold and ventilated place.

43
00:05:56,810 --> 00:06:02,175
Throughout the winter, regularly check
and remove bulbs that are damaged.

44
00:06:06,840 --> 00:06:12,630
Each variety has its own dormancy period,
some are longer than others

45
00:06:13,335 --> 00:06:17,880
but all end when the temperature reaches 12°C.

46
00:06:21,150 --> 00:06:23,380
At the beginning of the following spring,

47
00:06:23,895 --> 00:06:29,040
plant the bulbs 20cm apart
and don't bury them too deep.

48
00:07:42,680 --> 00:07:48,160
One to three stalks will develop
and can grow to 1m or more.

49
00:07:51,735 --> 00:07:55,675
It is important to stake them
to prevent them from falling.

50
00:07:57,915 --> 00:07:59,900
Over a period of four weeks

51
00:08:00,165 --> 00:08:04,620
each individual flower
within the onion flower head will blossom.

52
00:08:06,075 --> 00:08:09,020
The period lasting
from the beginning of blossoming

53
00:08:09,310 --> 00:08:11,390
to full seed maturity is long.

54
00:08:14,720 --> 00:08:19,420
The seeds are ready when the seed pods
have dried and reveal black seeds.

55
00:08:28,230 --> 00:08:32,470
To harvest the seeds,
cut the flower heads with the top of the stem.

56
00:08:33,335 --> 00:08:39,220
Place them in a woven bag and let them dry
in a warm and ventilated place.

57
00:08:43,780 --> 00:08:45,540
In cold and wet regions,

58
00:08:45,800 --> 00:08:52,310
you can harvest them early to avoid losing
the seeds since rain or wind can make them fall.

59
00:08:53,650 --> 00:08:57,940
In this case, uproot the whole plants
before the seeds are ready

60
00:08:58,410 --> 00:09:00,615
and let them mature in a dry place.

61
00:09:06,925 --> 00:09:09,620
Extracting- sorting- storing

62
00:09:22,460 --> 00:09:25,260
First of all, rub the flower heads
between your hands

63
00:09:25,335 --> 00:09:27,650
and then crush them using a rolling pin.

64
00:09:42,615 --> 00:09:47,310
To sort the seeds, use a fine sieve
to retain the seeds but not the dust.

65
00:10:02,790 --> 00:10:05,890
Finish by winnowing the seeds
to remove the light debris.

66
00:10:06,305 --> 00:10:11,450
You can place them in the wind,
blow on top of them or use a small ventilator.

67
00:10:17,230 --> 00:10:20,620
Then pour the seeds in cold water and stir.

68
00:10:26,270 --> 00:10:29,855
The fertilised seeds are heavier,
so they will sink.

69
00:10:33,625 --> 00:10:36,885
Remove the empty ones and the debris that float.

70
00:10:44,540 --> 00:10:46,430
Dry the good seeds on a plate.

71
00:10:49,715 --> 00:10:52,625
Once they are dry, they will run like sand.

72
00:10:59,880 --> 00:11:03,015
Always put a label with the name
of the variety and species

73
00:11:03,080 --> 00:11:09,530
as well as the year of harvesting inside
the package, as writing on the outside may rub off.

74
00:11:20,040 --> 00:11:25,340
Leave the seeds in the freezer for a few days
to kill any parasite larvae.

75
00:11:26,480 --> 00:11:29,940
Onion seeds have a germination
capacity of two years.

76
00:11:30,290 --> 00:11:33,180
This can sometimes stretch to four to five years.

77
00:11:33,855 --> 00:11:37,340
As these seeds
quickly lose their germination capacity,

78
00:11:37,815 --> 00:11:41,230
to lengthen this period keep them
in the freezer.
