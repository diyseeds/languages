﻿1
00:00:08,654 --> 00:00:11,840
Kale is a member of the Brassicaceae family,

2
00:00:12,480 --> 00:00:20,072
the Brassica oleracea species
and the acephala subspecies.

3
00:00:20,760 --> 00:00:26,894
The brassica oleracea species also
includes kohlrabi, broccoli,

4
00:00:27,665 --> 00:00:33,454
cabbage, Brussels sprouts,
cauliflower and the Savoy Cabbage. 

5
00:00:36,850 --> 00:00:40,220
Kale is a type of cabbage that does not form a head

6
00:00:41,440 --> 00:00:45,796
and usually has a central stalk along
which leaves develop.

7
00:00:49,160 --> 00:00:51,774
There are varieties that have several branches. 

8
00:00:58,240 --> 00:01:00,552
It is an autumn and winter vegetable.

9
00:01:01,840 --> 00:01:04,690
Most varieties are resistant to the frost

10
00:01:05,156 --> 00:01:09,760
which changes the taste of the leaves
by transforming starch into sugar.

11
00:01:12,385 --> 00:01:16,567
Ongoing periods of hard frost can,
however, destroy the plants. 

12
00:01:20,363 --> 00:01:24,940
There are a great variety of kales
that are distinguished by their height,

13
00:01:25,287 --> 00:01:28,923
ranging from 40 cm to 1.5 m,

14
00:01:29,505 --> 00:01:32,930
the structure of their smooth or curly leaves

15
00:01:33,949 --> 00:01:40,530
and their colour, ranging from light green
or yellow green to light or dark purple.

16
00:01:47,461 --> 00:01:50,340
Kale is used as fodder for small animals

17
00:01:55,287 --> 00:01:57,410
as well as for human consumption,

18
00:02:00,420 --> 00:02:05,861
or for craft products such as walking
sticks or even for roof rafters. 

19
00:02:12,080 --> 00:02:16,654
Pollination of all of the cabbages
of the Oleracea species: 

20
00:02:32,640 --> 00:02:36,581
The flowers of the Brassica oleracea
species are hermaphrodite

21
00:02:39,549 --> 00:02:43,825
which means that they have both male
and female organs.

22
00:02:47,505 --> 00:02:49,980
Most of them are self-sterile:

23
00:02:50,625 --> 00:02:55,825
the pollen from the flowers of one plant
can only fertilize another plant.

24
00:02:59,469 --> 00:03:01,723
The plants are therefore allogamous.

25
00:03:02,589 --> 00:03:07,425
In order to ensure good pollination
it is better to grow several plants. 

26
00:03:11,010 --> 00:03:14,080
Insects are the vectors of pollination.

27
00:03:17,418 --> 00:03:21,956
These characteristics ensure great
natural genetic diversity.

28
00:03:28,829 --> 00:03:33,163
All of the cabbage sub-species
of the Brassica oleracea species

29
00:03:33,323 --> 00:03:34,858
can cross with each other.

30
00:03:36,680 --> 00:03:41,403
You should therefore not grow different kinds
of cabbage for seeds close to each other. 

31
00:03:45,890 --> 00:03:47,338
To ensure purity,

32
00:03:47,658 --> 00:03:55,090
different varieties of the Brassica oleracea
species should be planted at least 1 km apart. 

33
00:03:56,450 --> 00:04:00,850
This distance can be reduced to 500 meters
if there is a natural barrier

34
00:04:00,938 --> 00:04:03,876
such as a hedge between the two varieties. 

35
00:04:07,069 --> 00:04:09,600
The varieties can also be isolated

36
00:04:09,876 --> 00:04:14,654
by placing small hives with insects
inside a closed mosquito net

37
00:04:19,890 --> 00:04:23,687
or by alternately opening
and closing mosquito nets.

38
00:04:27,920 --> 00:04:29,141
For this technique,

39
00:04:29,330 --> 00:04:33,425
see the module on isolation techniques
in “The ABC of seed production”. 

40
00:04:45,580 --> 00:04:55,485
Life cycle of kale 

41
00:05:03,134 --> 00:05:09,163
Kale is a biennial plant that forms stems
with leaves in the first year of cultivation

42
00:05:09,890 --> 00:05:14,058
and then flowers in spring,
producing seeds in the summer. 

43
00:05:28,989 --> 00:05:34,152
Kale plants for seed are grown
in the same way as those for consumption.

44
00:05:38,130 --> 00:05:43,381
You should select 10 to 15 plants
to ensure good genetic diversity. 

45
00:05:59,760 --> 00:06:02,356
Producing kale seed requires healthy plants

46
00:06:02,440 --> 00:06:05,440
that have been observed
throughout the growth period

47
00:06:05,658 --> 00:06:08,603
to identify the characteristics of the variety:

48
00:06:09,410 --> 00:06:17,490
size, colour, vigour, rapid growth,
resistance to diseases,

49
00:06:19,890 --> 00:06:25,236
leaves along the entire stem, resistance to cold. 

50
00:06:28,516 --> 00:06:30,872
It can remain in the ground during winter.

51
00:06:53,614 --> 00:06:58,727
In the second year, they flower in spring
and produce seed in summer. 

52
00:07:16,065 --> 00:07:22,780
Harvesting, extracting, sorting
and storing of all Brassica oleracea

53
00:07:31,905 --> 00:07:35,643
The seeds are mature
when the seed pods turn beige.

54
00:07:39,912 --> 00:07:42,458
The seed pods are very dehiscent,

55
00:07:42,632 --> 00:07:47,665
which means that they open very easily
when mature and disperse their seed. 

56
00:07:56,232 --> 00:08:00,240
Most of the time, the stalks do not
all mature at the same time.

57
00:08:01,294 --> 00:08:07,381
To avoid wasting any seed,
harvesting can take place as each stalk matures.

58
00:08:09,083 --> 00:08:14,807
The entire plant can also be harvested
before all of the seeds have completely matured. 

59
00:08:18,080 --> 00:08:25,098
The ripening process is then completed
by drying them in a dry, well-ventilated place. 

60
00:08:37,694 --> 00:08:43,890
Cabbage seeds are ready to be removed
when the seed pods can be easily opened by hand. 

61
00:08:45,840 --> 00:08:47,090
To extract the seeds,

62
00:08:47,240 --> 00:08:52,618
the seed pods are spread across
a plastic sheet or thick piece of fabric

63
00:08:53,010 --> 00:08:55,832
and then beaten or rubbed together by hand.

64
00:08:59,345 --> 00:09:04,363
You can also put them in a bag
and beat them against a soft surface. 

65
00:09:05,767 --> 00:09:10,618
Larger quantities can be threshed
by walking or driving on them. 

66
00:09:23,040 --> 00:09:28,370
Seed pods that do not open easily
probably contain immature seeds

67
00:09:28,560 --> 00:09:30,167
that will not germinate well. 

68
00:09:35,010 --> 00:09:36,240
During sorting,

69
00:09:36,574 --> 00:09:41,207
the chaff is removed by first passing
the seeds through a coarse sieve

70
00:09:41,512 --> 00:09:43,047
that retains the chaff

71
00:09:47,941 --> 00:09:50,654
and then by passing them through another sieve

72
00:09:50,938 --> 00:09:55,500
that retains the seeds
but allows smaller particles to fall through. 

73
00:09:59,272 --> 00:10:03,025
Finally, you should winnow them
by blowing on them

74
00:10:03,454 --> 00:10:07,556
or with the help of the wind
so that any remaining chaff is removed. 

75
00:10:22,676 --> 00:10:27,610
All seeds from the Brassica oleracea
species resemble one another.

76
00:10:28,647 --> 00:10:35,360
It is very difficult to distinguish between,
for example, cabbage and cauliflower seeds.

77
00:10:35,847 --> 00:10:40,836
This is why it is important to label
the plants and then the extracted seeds

78
00:10:41,040 --> 00:10:46,109
with the name of the species,
the variety and the year of cultivation. 

79
00:10:47,440 --> 00:10:52,240
Storing the seeds in the freezer
for several days eliminates any parasites. 

80
00:10:57,716 --> 00:11:00,960
Cabbage seeds are
able to germinate up to 5 years.

81
00:11:01,934 --> 00:11:05,592
However, they may retain
this capacity up to 10 years.

82
00:11:07,310 --> 00:11:10,669
This can be prolonged
by storing them in the freezer.

83
00:11:11,694 --> 00:11:17,905
One gram contains 250 to 300 seeds
depending on the variety. 
